EDOS
====
[![Code Climate](https://codeclimate.com/github/IceDragon200/EDOS.png)](https://codeclimate.com/github/IceDragon200/EDOS)

## Introduction
Years in the works, finally I got tired of sitting down on the code, and have
decided to share it with the world.

The code still has remnants of RMVX/A and other bits and pieces.
Eventually all the code will be re-written and old bits removed or improved.

The code cannot be executed as is without StarRuby (using my repo), RM-SRRI, and
RM-Rice (unreleased)

## Code Structure
Eventually I will write this
