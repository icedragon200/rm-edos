# death - row.11
cols = 6
index cols * 11 # set to first index
import 'partial/emo_init'
2.times do
  rest 0.1 # rest for 1 milli-second
  succ     # advance 1 index
end
import 'partial/hold'