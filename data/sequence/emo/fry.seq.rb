# fry - row.16
cols = 6
index cols * 16 # set to first index
import 'partial/emo_init'
5.times do
  rest 0.1 # rest for 1 milli-second
  succ     # advance 1 index
end
import 'partial/hold'