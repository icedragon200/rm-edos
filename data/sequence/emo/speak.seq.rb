# speak - row.17
cols = 6
index cols * 17 # set to first index
import 'partial/emo_init'
4.times do
  rest 0.1 # rest for 1 milli-second
  succ     # advance 1 index
end
import 'partial/hold'