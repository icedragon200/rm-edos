# speak - row.17
cols = 6
index cols * 17 # set to first index
import 'partial/emo_init'
repeat do
  4.times do
    rest 0.1 # rest for 1 milli-second
    succ     # advance 1 index
  end
  rest 0.1 # rest for 1 milli-second
  index cols * 17 # set to first index
end