#
# EDOS/lib/automation/comp.rb
#   by IceDragon
require_relative 'comp/ajar'
require_relative 'comp/colorizer'
require_relative 'comp/contents_fade'
require_relative 'comp/fade'
require_relative 'comp/move'
require_relative 'comp/move_offset'
require_relative 'comp/move_offset_osc'
require_relative 'comp/moveto'
require_relative 'comp/moveto_offset'
require_relative 'comp/moveto_offset_osc'
require_relative 'comp/resize'
require_relative 'comp/toner'
require_relative 'comp/zoom'