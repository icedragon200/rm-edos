#
# EDOS/lib/automation/comp/ajar.rb
#   by IceDragon
# @param [Integer]
# @target [Integer] openness
module Automation
  class Ajar < BaseEased

    def update_value(target, v)
      target.openness = v
    end

    type :ajar

  end
end