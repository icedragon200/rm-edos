#
# EDOS/lib/config/cache-mod.rb
#   by IceDragon
EDOS::Loader.config.add("Cache Runtime Modifications", 1000) do
  bmp = Cache.system('menu-icons/menu-icons')
  bmp.blend_fill_rect(bmp.rect, Palette['droid_light_ui_dis'], :dst_mask)
end