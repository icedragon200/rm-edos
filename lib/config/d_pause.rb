EDOS::Loader.config.add("Daemon::DPause Config", 100) do

class Daemon::DPause

# USER_CONFIG

  PAUSE_BUTTON = :F6        # // Default Y button

  # Pause Audio Config
  PAUSE_AUDIO_FADE    = true
  PAUSE_VOL_FADE_TIME = 120 # // In frames
  PAUSE_VOL_RATE      = 0.6 # // 60% of the original volume

  # Pause Text Config
  PAUSE_TEXT       = true
  PAUSE_TEXT_STR   = 'Pause'
  PAUSE_TEXT_ALIGN = 1      # // 0 - Flush Left, 1 - Center, 2 - Flush Right
  PAUSE_TEXT_ANIM  = 1      # // 0 - None, 1 - Flick, 2 - Pulse
  PAUSE_TEXT_ANIM_TIME = 30 # // Animation Timing

  # Pause Text Font Config
  PAUSE_FONT = Font.new     # // Font object :D
  PAUSE_FONT.size      = 96
  PAUSE_FONT.name      = Font.default_name
  PAUSE_FONT.bold      = false
  PAUSE_FONT.italic    = true
  PAUSE_FONT.shadow    = false
  PAUSE_FONT.outline   = false
  PAUSE_FONT.color     = Color.new(255, 255, 255, 255)
  PAUSE_FONT.out_color = Color.new(0, 0, 0, 0)

  # Pause Dimming Config
  PAUSE_DIMMING       = true
  PAUSE_DIMMING_TRANS = false
  PAUSE_DIM_ALPHA     = 128 # // How dark should the pause state be (0...255) 255 being BLACK

  # Pause Misc
  PAUSE_PLAYTIME_ADVANCES = false # // Should the playtime still advance when paused?

# /USER_CONFIG

end

end