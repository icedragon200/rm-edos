#
# EDOS/lib/#config/DScreenFilm.rb
#   by IceDragon (mistdragon100)
#   dc 06/07/2013
#   dm 06/07/2013
# vr 1.0.0
EDOS::Loader.config.add("Daemon::DScreenFilm Config", 100) do

class Daemon::DScreenFilm

# USER_CONFIG
  RECORD_DIRNAME = "record"
  RECORD_BUTTON  = :F10
  RECORD_REALTIME = true # saves the files when idling
  @@film_interval = 15

end

end