EDOS::Loader.config.add("Daemon::DScreenshot Config", 100) do

class Daemon::DScreenshot

# USER_CONFIG
  DIRNAME = "media/screenshots"
  FILENAME_SPF = "shot%s.png" # %s will be replaced by the Daemon for tests

  SCREENSHOT_BUTTON = :F11
  PROMPT_SCREENSHOT = false # TODO
  SCREENSHOT_BACKGROUND_COLOR = Color.new(0, 0, 0, 255)

end

end