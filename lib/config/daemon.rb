EDOS::Loader.config.add("Daemon", 101) do
  server = EDOS.daemon_server = MACL::DaemonServer.new
  server.set_active
  server.log = EDOS.log

  Daemon::DPanel.new
  Daemon::DClock.new
  #Daemon::DDemoplay.new
  #Daemon::DIdle.new
  #Daemon::DMouseSparks.new
  Daemon::DAlarm.new        # Alarm
  Daemon::DNotify.new       # Notifications
  Daemon::DLoopRender.new   # F2 Loop Render
  Daemon::DStepDrawing.new  # F3 StepDrawing
  Daemon::DZSweep.new       # F5 Z Sweeper
  Daemon::DPause.new        # F6 Pause
  Daemon::DScriptLoader.new # F8 script loading menu
  Daemon::DScreenFilm.new   # F10 Frame recording
  Daemon::DScreenshot.new   # F11 Screenshots
  Daemon::DTimerTask.new    # crond-esque tasks, based on frame counts
  #Daemon::DWhatsPlaying.new

  server.init_daemons
end

EDOS::Loader.unload.add("Daemon", 101) do
  if server = EDOS.daemon_server
    server.unload
  end
end