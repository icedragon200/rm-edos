#
# EDOS/lib/#config/drawext-merio.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 03/07/2013
#   dm 03/07/2013
# vr 1.0.0
#   Merio DrawExtension loader
EDOS::Loader.config.add("DrawExt::gauge_palettes", 5) do
  DrawExt.init_gauge_palettes
end
EDOS::Loader.config.add("DrawExt::Merio::init", 6) do
  DrawExt::Merio.init
end