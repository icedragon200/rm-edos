#!/usr/bin/env ruby
# src/#config/font.rb
# vr 1.0

# Font (Settings)
# Delayed loading using Linara's on load function
EDOS::Loader.config.add("Font", 5) do

  Font.default.size      = 22 #24
  Font.default.bold      = false
  Font.default.italic    = false
  Font.default.shadow    = false
  Font.default.outline   = false
  Font.default.color     = Color.new(255, 255, 255, 255)
  Font.default.out_color = Color.new(  0,   0,   0, 128)

  # SRRI Extended Font Control
  Font.default.shadow_color = Font.default.out_color.dup
  Font.default.antialias = true

  Font.default.shadow_conf = [7, 1]

  Font.default.exconfig = {
    flip_shadow_color: false,
    flip_outline_color: false
  }

  Font.default.name      =#["AGENCYR.TTF"]
                          #["ARIAL.TTF"]
                          #["Aero Matics Regular.ttf"]
                          #["Avenir-Book.otf"]
                          #["CODE Bold.otf"]
                          #["CourierNew.ttf"]
                          #["Crisp.ttf"]
                          #["DejaVuSansMono-Bold.ttf"]
                          #["Eurosti.TTF"]
                          #["LucidaGrande.ttf"]
                          #["MankSans-Medium.ttf"]
                          #["MesloLGL-Regular.ttf"]
                          #["Microgramma Bold.ttf"]
                          #["Prime Regular.otf"]
                          #["ProggySmall.ttf"]
                           ["Roboto-Regular.ttf"]
                          #["TitilliumText25L003.otf"]
                          #["Ubuntu-B.ttf"]
                          #["Ubuntu-R.ttf"]
                          #["VL-Gothic-Regular.ttf"]
                          #["VL-PGothic-Regular.ttf"]
                          #["VeraMono.ttf"]
                          #["Verdana.ttf"]
                          #["akashi.ttf"]
                          #["calibri.ttf"]
                          #["general_enclosed_foundicons.ttf"]
                          #["general_foundicons.ttf"]
                          #["homizio-regular.ttf"]
                          #["luximr.ttf"]
                          #["saxmono.ttf"]
                          #["segoepr.ttf"]
                          #["segoeui.ttf"]
                          #["segoeuil.ttf"]
                          #["typicons-regular-webfont.ttf"]

end