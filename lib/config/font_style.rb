#
# EDOS/lib/#config/font_style.rb
# vr 1.0.0
EDOS::Loader.config.add("Font Styles", 20) do
module Mixin::FontStyle

  px_font = nil #["visitor2.ttf"]
  px_font_size = 18 #16
  px_antialias = true

  simple_black =
  new_style('simple_black',
              name:    px_font,
              color:   Palette['black'],
              shadow:  false,
              outline: false,
              size:    px_font_size
            )

  new_style('simple_white',
            simple_black.merge(color: Palette['white']))

  num = new_style('text_gauge',
              name:        px_font,
              color:       Palette['white'],
              outline:     false,
              shadow:      true,
              antialias:   px_antialias,
              shadow_conf: [8, 1],
              size:        px_font_size
            )

  new_style('num_fmt', num.merge(name: ["Microgramma Bold.ttf"]))

  new_style('text_help',
              name:    px_font,
              color:   Palette['black'],
              outline: false,
              shadow:  false,
              size:    px_font_size
            )

  new_style('button_white',
              color:   Palette['white'],
              shadow:  false,
              italic:  false,
              outline: true,
              bold:    true,
              size:    Font.default_size - 8
            )

  ##
  # System Fonts
  sys_name = 'sys_%s'
  mer_name = 'merio_%s'
  hstrs    = Metric.scales.keys
  shades   = ['light', 'dark']
  states   = ['enb', 'dis']

  hstrs.each do |hstr|
    shades.each do |shade|
      states.each do |state|
        #new_style(sys_name % "#{shade}_#{hstr}_#{state}",
        #            name:      nil,
        #            outline:   false,
        #            shadow:    false,
        #            size:      ->(f) { Metric.ui_font_size(hstr) },
        #            color:     Palette["droid_#{shade}_ui_#{state}"],
        #            antialias: true
        #          )
        new_style(mer_name % "#{shade}_#{hstr}_#{state}",
                    name:      ->(f) { DrawExt::Merio.font_name },
                    outline:   false,
                    shadow:    false,
                    size:      ->(f) { DrawExt::Merio.ui_font_size(hstr) },
                    color:     ->(f) { DrawExt::Merio.txt_palette["#{shade}_ui_#{state}"] }
                    #antialias: true
                  )
      end
    end
  end

  new_style('merio_default', name: ->(f) { DrawExt::Merio.font_name },
                             size: ->(f) { DrawExt::Merio.ui_font_size(:default) })

  new_style("cedar_text", name:  "segoeui.ttf",
                          color: Palette['white'],
                          size:  18)

  EDOS.try_log do |log|
    log.puts("Available Font Styles: %d" % @@style.size)
  end

end
end