#
# EDOS/lib/#config/graphics.rb
#   by IceDragon
EDOS::Loader.config.add("Graphics#frame_rate", 1) do
  module Graphics
    self.frame_rate = 60
  end
end