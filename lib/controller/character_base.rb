module Controller
  class CharacterBase

    include Mixin::Activatable

    attr_accessor :character

    def initialize(character)
      @character = character
    end

    def update
      @character.move_in_direction(Input.dir8) if active?
      @character.jump if Input.press?(:v)
    end

  end
end