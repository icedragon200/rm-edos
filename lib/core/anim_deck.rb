class AnimDeck

  def initialize(object, *properties)
    @object = object
    @deck = Hash[properties.map do |sym|
      [sym, MACL::Tween2.new(1, :linear, [0,0]).stop]
    end]
  end

  def setup(property, tickmax, easer, pair)
    tween = @deck[property]
    tween.setup(tickmax, easer, pair)
    self
  end

  def busy?
    @deck.values.any?(&:active?)
  end

  def update
    @deck.each do |key, tween|
      tween.update && @object.send(sym + "=", tween.value)
    end
  end

end

class AnimDeck::Sprite < AnimDeck

  def initialize(sprite)
    super(sprite, :x, :y, :z, :ox, :oy, :width, :height,
                  :opacity, :bush_opacity, :bush_depth,
                  :zoom_x, :zoom_y, :angle)
  end

end