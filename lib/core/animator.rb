class Animator

  class Builder

    attr_reader :_list
    protected :_list

    def initialize
      @_list = []
      @_depth = 0
    end

    def to_animator
      animator = Animator.new
      animator.setup(@_list.dup)
      return animator
    end

    def render
      @_list
    end

    def _cmd(sym, *params)
      @_list << [@_depth, sym, params]
    end

    def _depth_succ
      @_depth += 1
    end

    def _depth_pred
      @_depth -= 1
    end

    def _import(data)
      instance_eval(data); self
    end

    def import(filename)
      @_list.concat(Cache::Sequence.normal(filename)._list)
    end

    def set(name, *values)
      _cmd(:set, name, values)
    end

    def index(n)
      _cmd(:index, n.to_i)
    end

    def modulo(n)
      _cmd(:modulo, n.to_i)
    end

    def succ(n=1)
      _cmd(:succ, n.to_i)
    end

    def pred(n=1)
      _cmd(:pred, n.to_i)
    end

    def rest(n)
      _cmd(:rest, n.to_f)
    end

    def repeat
      _cmd(:repeat)
      _depth_succ
      yield
      _depth_pred
      _cmd(:end)
    end

    def cancel
      _cmd(:cancel)
    end

    def self.build(data)
      new._import(data)
    end

  end

  attr_reader :index
  attr_reader :env
  attr_reader :_list_index
  attr_accessor :frame_rate

  def initialize
    @frame_rate = Graphics.frame_rate
    clear
  end

  def clear
    ### internals
    @_list = nil
    @_list_index = 0
    @_depth = 0
    @_depth_flags = [[]]
    @_depth_vars = [{}]
    @_rest = 0
    ### externals
    @index = 0
    @env = {}
  end

  def setup(list)
    clear
    @_list = list
  end

  def depth_succ
    @_depth += 1
    @_depth_flags.push([])
    @_depth_vars.push({})
  end

  def depth_pred
    @_depth -= 1
    @_depth_flags.pop
    @_depth_vars.pop
  end

  def depth_vars
    @_depth_vars[@_depth]
  end

  def depth_flags
    @_depth_flags[@_depth]
  end

  def exec_command(cmd)
    dp, op, params = *cmd
    case op
    when :set
      key, value = *params
      @env[key] = value
    when :index # set the index
      @index = params.first
    when :modulo # modulo the index
      @index %= params.first
    when :succ # increment index
      @index += params.first
    when :pred # decrement index
      @index -= params.first
    when :rest # sleep for a period
      @_rest = (params.first * @frame_rate).to_i
    when :repeat
      depth_succ
      depth_flags << :repeat
      depth_vars[:line] = @_list_index
    when :cancel
      depth_pred
    when :end
      if depth_flags.include?(:repeat)
        @_list_index = depth_vars[:line]
      else
        depth_flags.delete(:repeat)
      end
    end
  end

  def current_line
    @_list[@_list_index]
  end

  def current_line_depth
    current_line ? current_line[0] : @_depth
  end

  def done?
    @_list.size < @_list_index
  end

  def update
    if @_rest > 0
      @_rest -= 1
    else
      if @_list && !done?
        while !done?
          if current_line_depth > @_depth
            @_list_index += 1 until current_line_depth == @_depth || @_list.size < @_list_index
          end
          exec_command(current_line)
          @_list_index += 1
          break if @_rest > 0
        end
      end
    end
  end

end