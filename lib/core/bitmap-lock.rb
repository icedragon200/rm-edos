#
# EDOS/core/core-ext/bitmap-lock.rb
#
module EDOS
  ###
  ##
  # ::check_bitmap(bmp)
  def self.check_bitmap(bmp)
    if !bmp.is_a?(Bitmap)
      raise TypeError, "expected a Bitmap but recieved a #{bmp.class}"
    elsif bmp.disposed?
      raise BitmapError, "cannot modify disposed Bitmap #{bmp}"
    end
  end

  module Lockable

    class LockedObject < RuntimeError ; end

    attr_writer :locked

    def locked
      @locked ||= 0
    end

    def locked?
      locked > 0
    end

    def check_lock
      raise LockedObject, "#{self} is locked" if locked?
    end

    def lock
      self.locked += 1
      if block_given?
        yield self
        unlock
      end
    end

    def unlock
      self.locked -= 1
    end

  end
end

class Bitmap

  include EDOS::Lockable

  alias :unlocked_dispose :dispose
  def dispose
    check_lock
    unlocked_dispose
  end

end

class StarRuby::Texture

  include EDOS::Lockable

  alias :unlocked_dispose :dispose
  def dispose
    check_lock
    unlocked_dispose
  end

end