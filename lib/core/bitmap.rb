#
# EDOS/lib/core/bitmap.rb
#   by IceDragon
class Bitmap

  class BackBufferBitmap < Bitmap
  end

  include DrawExt::Include

  @@__back_buffer__ = nil

  ##
  # used to interface with the DrawExt::Include functions
  # @return [Bitmap]
  def bitmap
    self
  end

  ##
  # Returns the merio context for this Bitmap
  # @return [DrawExt::Merio::DrawContext]
  def merio
    if !@merio || !@merio.bitmap.equal?(self)
      @merio = DrawExt::Merio::DrawContext.new(self)
    end
    yield @merio if block_given?
    return @merio
  end

  ##
  # Copies a sample/selection of the Bitmap and returns it
  # @overload subsample(rect)
  #   @param [Rect] rect
  # @overload subsample(x, y, width, height)
  #   @param [Integer] x
  #   @param [Integer] y
  #   @param [Integer] width
  #   @param [Integer] height
  # @return [Bitmap]
  def subsample(*args)
    case args.size
    when 1
      r = Convert.Rect(args.first)
    when 4
      r = Convert.Rect(args)
    else
      raise ArgumentError,
            "wrong argument number #{args.size} (expected 1, or 4 arguments)"
    end
    nr = self.rect.subsample(r)
    return nil if nr.empty?
    Bitmap.new(nr.width, nr.height).tap { |o| o.blt(0, 0, self, nr.rect) }
  end

  ##
  # DrawExt::draw_text patch
  alias :draw_text_str :draw_text
  def draw_text(*args)
    case args.size
    when 2, 3
      if args[1].is_a?(DrawExt::Text)
        return DrawExt.draw_text(self, *args)
      end
    when 5, 6
      if args[4].is_a?(DrawExt::Text)
        r = Rect(args[0, 4])
        return DrawExt.draw_text(self, r, *args[4, 2])
      end
    end
    draw_text_str(*args)
  end

  def self.back_buffer(w=Graphics.width, h=Graphics.height)
    if(@@__back_buffer__.nil? || @@__back_buffer__.disposed? ||
       @@__back_buffer__.width < w || @@__back_buffer__.height < h)
      @@__back_buffer__.dispose if @@__back_buffer__ && !@@__back_buffer__.disposed?
      @@__back_buffer__ = BackBufferBitmap.new(w, h)
    end
    return @@__back_buffer__
  end

  def self.clear_back_buffer
    @@__back_buffer__.each do |bmp|
      bmp.dispose if bmp && !bmp.disposed?
    end
    @@__back_buffer__.clear
  end

  ## SRRI only function
  # ::render_text(Font font, String text)
  def self.render_text(font, text, align=1)
    rect = Rect.new(0, 0, *font.text_size(text))
    #rect.expand!(anchor: 5, amount: 4)
    bmp = Bitmap.new(rect.width, rect.height)
    bmp.font.set(font)
    bmp.draw_text(rect, text, align)
    return bmp
  end

end

module Cache
  class CacheBitmap < Bitmap

    attr_accessor :cache_key

  end
end