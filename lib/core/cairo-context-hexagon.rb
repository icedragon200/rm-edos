#
# EDOS/core/core-ext/cairo.rb
#   by IceDragon
# Swiped from: http://www.hasenkopf2000.net/wiki/page/2d-graphics-cairo/
# Ported it to ruby
module Cairo
  class Context
    module Hexagon

      def hexagon(x, y, d1, d2=nil)
        sr3 = Math.sqrt(3)
        d2 ||= d1
        qd1 = d1 / 4.0
        #hd1 = d1 / 2.0
        qd2 = d2 / 4.0
        hd2 = d2 / 2.0
        line_to(x + sr3 * qd1, y + qd2)
        line_to(x, y + hd2)
        line_to(x - sr3 * qd1, y + qd2)
        line_to(x - sr3 * qd1, y - qd2)
        line_to(x, y - hd2)
        line_to(x + sr3 * qd1, y - qd2)
        close_path
      end

    end
    include Hexagon
  end
end