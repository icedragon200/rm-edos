#
# EDOS/lib/core/camera.rb
#   by IceDragon
class Camera

  attr_accessor :position
  attr_accessor :follow_target

  def initialize
    @position = Vector3.new(0, 0, 0)
    @follow_target = nil
  end

  def screen_half_width
    Screen.content.width / 2
  end

  def screen_half_height
    Screen.content.height / 2
  end

  def x
    @position.x
  end

  def y
    @position.y
  end

  def z
    @position.z
  end

  def x=(n)
    @position.x = n
  end

  def y=(n)
    @position.y = n
  end

  def z=(n)
    @position.z = n
  end

  def update
    update_follow if @follow_target
  end

  def update_follow
    position.x = @follow_target.screen_x - screen_half_width
    position.y = @follow_target.screen_y - screen_half_height
    position.z = @follow_target.screen_z
  end

  def mouse_position
    Mouse.position + @position.to_vector2
  end

end