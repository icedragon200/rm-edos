#
# EDOS/lib/class/color.rb
# vr 1.0.2
class Color

  include Mixin::ColorEx
  include MACL::Mixin::ColorMath

  attr_accessor :name

  def to_tone
    r, g, b = *self.to_a_na
    gray = 0
    Tone.new(r, g, b, gray)
  end unless method_defined?(:to_tone)

  def opaque!
    a = self.alpha.to_f / 255.0
    self.red   = self.red   * a
    self.green = self.green * a
    self.blue  = self.blue  * a
    self.alpha = 255
    self
  end

  def opaque
    dup.opaque!
  end

  def self.random
    new(rand(0x100), rand(0x100), rand(0x100), 0x100)
  end

  def hex_s
    "%06x" % to_rgb24
  end

  def to_yaml
    { red: red, green: green, blue: blue, alpha: alpha }.to_yaml
  end

end