#
# EDOS/lib/core/convert.rb
#   by IceDragon
module Convert
  class << self

    ##
    # converts given arg to a Boolean
    def Boolean(obj)
      !!obj
    end

    ##
    # converts given args to a Vector2F, if the arguments are invalid
    # an ArgumentError is raised
    def Vector2(*args)
      args, = args if args.size == 1
      return StarRuby::Vector2F.cast(args)
    end

    ##
    # converts given args to a Vector3F, if the arguments are invalid
    # an ArgumentError is raised
    def Vector3(*args)
      args, = args if args.size == 1
      return StarRuby::Vector3F.cast(args)
    end

    ##
    # converts given args to a Point2, if the arguments are invalid
    # an ArgumentError is raised
    def Point2(*args)
      Point2.cast(args.size < 2 ? args.first : args)
    end

    ##
    # converts given args to a Point3, if the arguments are invalid
    # an ArgumentError is raised
    def Point3(*args)
      Point3.cast(args.size < 2 ? args.first : args)
    end

    ##
    # converts given args to a Size2, if the arguments are invalid an
    # ArgumentError is raised
    def Size2(*args)
      Size2.tcast(args.size < 2 ? args.first : args)
    end

    ##
    # converts given args to a Size3, if the arguments are invalid an
    # ArgumentError is raised
    def Size3(*args)
      Size3.tcast(args.size < 2 ? args.first : args)
    end

    ##
    # converts given args to a Rect, if the arguments are invalid an
    # ArgumentError is raised
    def Rect(*args)
      return Rect.cast(*args)
    end

    ##
    # converts given args to a Color, if the arguments are invalid an
    # ArgumentError is raised
    def Color(*args)
      return Color.cast(*args)
    end

    ##
    # converts given args to a Tone, if the arguments are invalid an
    # ArgumentError is raised
    def Tone(*args)
      return Tone.cast(*args)
    end

    ##
    # converts given obj to a MACL::Palette, if the Object is invalid a
    # TypeError is raised
    def Palette(obj)
      case obj
      when MACL::Palette  then obj
      when String, Symbol then DrawExt.gauge_palettes[obj]
      when Hash           then DrawExt::DrawExtPalette.new.import(obj)
      else
        raise TypeError,
              "wrong argument type (#{obj.class}) expected MACL::Palette, String, Symbol or Hash"
      end
    end

  end
end