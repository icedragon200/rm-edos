class Direction

  attr_accessor :dir

  def initialize(dir)
    @dir = dir
  end

  def to_i
    @dir
  end

  def to_angle
    case @dir
    when 1 then 225
    when 2 then 180
    when 3 then 135
    when 4 then 270
    when 6 then 90
    when 7 then 315
    when 8 then 0
    when 9 then 45
    else        0
    end
  end

  def to_radian
    case @dir
    when 1 then 3.9269908169872414
    when 2 then 3.141592653589793
    when 3 then 2.356194490192345
    when 4 then 4.71238898038469
    when 6 then 1.5707963267948966
    when 7 then 5.497787143782138
    when 8 then 0.0
    when 9 then 0.7853981633974483
    else        0
    end
  end

  def inverse_int
    case @dir
    when 1 then 9
    when 2 then 8
    when 3 then 7
    when 4 then 6
    when 5 then 5
    when 6 then 4
    when 7 then 3
    when 8 then 2
    when 9 then 1
    else        0
    end
  end

  def inverse
    Direction.new(inverse_int)
  end

  def to_vector2
    case @dir
    when 1 then Vector2.new(-1,  1)
    when 2 then Vector2.new( 0,  1)
    when 3 then Vector2.new( 1,  1)
    when 4 then Vector2.new(-1,  0)
    when 6 then Vector2.new( 1,  0)
    when 7 then Vector2.new(-1, -1)
    when 8 then Vector2.new( 0, -1)
    when 9 then Vector2.new( 1, -1)
    else        Vector2.new( 0,  0)
    end
  end

  def to_row8_i
    case @dir
    when 1 then 4
    when 2 then 0
    when 3 then 5
    when 4 then 1
    when 6 then 2
    when 7 then 7
    when 8 then 3
    when 9 then 6
    else        0
    end
  end

  def to_row4_i
    case @dir
    when 1 then 0
    when 2 then 0
    when 3 then 0
    when 4 then 1
    when 6 then 2
    when 7 then 3
    when 8 then 3
    when 9 then 3
    else        0
    end
  end

  alias :-@ :inverse

end