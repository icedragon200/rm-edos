#
# EDOS/lib/core/Game.rb
#   by IceDragon
#   dc 13/05/2013
#   dm 13/05/2013
# vr 1.0.0
class Game

  @@current = nil

  def initialize
    @@current = self
  end

  def newgame
    create_objects
  end

  def create_objects

  end

  def make_save_contents
    contents = {}

    contents
  end

  def extract_save_contents(contents)

  end

  def make_save_header
    header = {}

    header
  end

  def pre_save

  end

  def post_save

  end

  def pre_load

  end

  def post_load

  end

  def self.current
    @@current
  end

end