#
# EDOS/lib/core/input.rb
#   by IceDragon
module Input

  ##
  # srri repeat rate
  def self.sr_repeat_rate
    @sr_repeat_rate ||= Metric::Time.sec_to_frame(0.1666666666)
  end

  def self.dir4_vector2
    case dir8
    when 2 then Vector2.new( 0, -1)
    when 4 then Vector2.new(-1,  0)
    when 6 then Vector2.new( 1,  0)
    when 8 then Vector2.new( 0,  1)
    else        Vector2.new( 0,  0)
    end
  end

  def self.dir8_vector2
    case dir8
    when 1 then Vector2.new(-1,  1)
    when 2 then Vector2.new( 0,  1)
    when 3 then Vector2.new( 1,  1)
    when 4 then Vector2.new(-1,  0)
    when 6 then Vector2.new( 1,  0)
    when 7 then Vector2.new(-1, -1)
    when 8 then Vector2.new( 0, -1)
    when 9 then Vector2.new( 1, -1)
    else        Vector2.new( 0,  0)
    end
  end

end