#
# EDOS/lib/core/kernel.rb
#   by IceDragon
module Kernel

  ##
  # loads a ruby file as an anonymous module
  def load_module(filename)
    content = File.read(filename)
    Module.new do
      module_eval content
      extend self
    end
  end unless method_defined?(:load_module)

end