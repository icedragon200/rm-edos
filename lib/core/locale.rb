#
# EDOS/lib/core/locale.rb
#   by IceDragon
class Locale

  ##
  # @return [String] name of this Locale
  attr_reader :name

  ##
  # @param [String] name
  def initialize(name)
    @name = name
    @data = Hash.new
  end

  ##
  # @param [String] locname
  # @return [String]
  def [](locname)
    @data.fetch(locname)
  end

  ##
  # @param [String] locname
  # @param [String] translated
  # @return [Void]
  def []=(locname, translated)
    @data[locname.dup.freeze] = translated.dup.freeze
  end

end