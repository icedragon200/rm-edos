#
# EDOS/lib/core/macl-palette.rb
#   by IceDragon
module MACL
  class Palette

    ##
    # set_color_abs(String|Symbol osym, Color color)
    alias :set_color_abs_wo_name :set_color_abs
    def set_color_abs(sym, color)
      color.name = sym
      set_color_abs_wo_name(sym, color)
    end

    ##
    # Exports the Palette as a .png file
    # @param [String] filename
    def export_as_png(filename)
      color_count = entries.size
      cols = 12
      block_size = Size2.new(Metric.ui_element * 2, Metric.ui_element * 2)
      image_size = block_size * [cols, color_count / cols + (color_count % cols).min(1)]

      bmp = Bitmap.new(*image_size)
      entries.each_with_index do |(k, c), i|
        # Color rect
        r = Rect.new((i % cols) * block_size.width, (i / cols) * block_size.height,
                     *block_size)
        # Name rect
        r2 = r.dup
        r2.height = Metric.ui_element_sml
        r.height -= r2.height
        r2.y = r.y2
        bmp.merio.font_config(:light, :small, :enb)
        bmp.merio.draw_fill_rect(r, c)
        bmp.merio.draw_fill_rect(r2, Palette['_droid_dark_ui_enb'])
        bmp.draw_text(r2, k, 1)
      end
      bmp.texture.save_png(filename)
      bmp.dispose
      return self
    end

  end
end