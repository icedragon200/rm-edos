#
# EDOS/lib/core/macl-surface.rb
#   by IceDragon
module MACL
  module Mixin
    module Surface2

      include EDOS::Type::Surface2

      def pos
        return Convert.Point2(x, y)
      end

      def pos2
        return Convert.Point2(x2, y2)
      end

    end
    module Surface3

      include EDOS::Type::Surface3

    end
  end
end