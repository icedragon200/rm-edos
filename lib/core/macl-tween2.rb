module MACL
  class Tween2

    def start
      reset_tick
      self
    end

    def stop
      self.tick = @tickmax - reset_tick
      self
    end

  end
end