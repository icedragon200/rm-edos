#
# EDOS/lib/core/module.rb
#   by IceDragon
class Module

  ##
  # similar to attr_writer, but the resultant method will not modify
  # the attribute
  # @param [Symbol] symbols
  def attr_null_writer(*symbols)
    symbols.each { |sym| define_method(sym.to_s + "=") { |arg| } }
    symbols
  end

end