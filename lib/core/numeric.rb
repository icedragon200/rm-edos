#
# EDOS/lib/core/numeric.rb
#   by IceDragon
class Numeric

  ##
  # snatched from the Ruby Gosu ChipmunkIntegration example
  def radians_to_vec2
    CP::Vec2.new(Math::cos(self), Math::sin(self))
  end

  def lerp(trg, delta)
    self + (trg - self) * delta
  end

  def lerp_step(trg, step)
    if trg > self
      [self + step, trg].min
    elsif trg < self
      [self - step, trg].max
    else
      self
    end
  end

end