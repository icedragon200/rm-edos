#
# EDOS/lib/core/plane.rb
#  by IceDragon
class Plane

  include Mixin::VisPos
  include MACL::Mixin::Surface2Bang

  class PlaneError < RGSSError
    #
  end

  def dispose_bitmap
    self.bitmap.dispose
    self.bitmap = nil
  end

  def dispose_bitmap_safe
    dispose_bitmap if self.bitmap && !self.bitmap.disposed?
  end

  def dispose_all
    dispose_bitmap_safe
    dispose
  end

  attrs = [:x, :y, :width, :height]
  attrs += [:x2, :y2] # MACL::Surface

  attrs.each do |sym|
    module_eval(%Q(
      def #{sym}
        return (viewport || Graphics).rect.#{sym}
      end

      def #{sym}=(n)
        unless viewport
          raise(PlaneError,
                "changes to a \#{self.class} without a Viewport is not allowed")
        end
        viewport.rect.#{sym} = n
      end
    ))
  end

  def rect
    return (self.viewport || Graphics).rect
  end

  def update
    #
  end

end