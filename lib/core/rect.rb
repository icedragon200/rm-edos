#
# EDOS/lib/core/rect.rb
#   by IceDragon
class Rect

  include MACL::Mixin::Surface2

  attr_writer :math_mode

  def to_h
    { x: x, y: y, width: width, height: height }
  end unless method_defined?(:to_h)

  def to_yaml
    to_h.to_yaml
  end

  ### math
  def math_mode
    @math_mode ||= :all
  end

  def math_params(other, mm = math_mode)
    ox, oy, ow, oh = nil, nil, nil, nil
    case other
    when EDOS::Type::Size2
      ow, oh = *other
      mm = :size if mm == :all
      return self if mm == :pos
    when EDOS::Type::Surface2
      ox, oy, ow, oh = *other
    when EDOS::Type::Vector2
      ox, oy = *other
      mm = :pos if mm == :all
      return self if mm == :size
    when Numeric
      ox = oy = ow = oh = other
    else
      raise TypeError,
            "wrong argument type #{other.class} " \
            "(expected Numeric, " \
            "EDOS::Type::Size2, " \
            "EDOS::Type::Surface2 " \
            "or EDOS::Type::Vector2)"
    end
    return mm, ox, oy, ow, oh
  end

  def add!(other)
    mm, ox, oy, ow, oh = math_params(other)
    if mm == :pos || mm == :all
      self.x      += ox
      self.y      += oy
    end
    if mm == :size || mm == :all
      self.width  += ow
      self.height += oh
    end
    return self
  end

  def sub!(other)
    mm, ox, oy, ow, oh = math_params(other)
    if mm == :pos || mm == :all
      self.x      -= ox
      self.y      -= oy
    end
    if mm == :size || mm == :all
      self.width  -= ow
      self.height -= oh
    end
    return self
  end

  def mul!(other)
    mm, ox, oy, ow, oh = math_params(other)
    if mm == :pos || mm == :all
      self.x      *= ox
      self.y      *= oy
    end
    if mm == :size || mm == :all
      self.width  *= ow
      self.height *= oh
    end
    return self
  end

  def div!(other)
    mm, ox, oy, ow, oh = math_params(other)
    if mm == :pos || mm == :all
      self.x      /= ox
      self.y      /= oy
    end
    if mm == :size || mm == :all
      self.width  /= ow
      self.height /= oh
    end
    return self
  end

  def add(other)
    return dup.add!(other)
  end

  def sub(other)
    return dup.sub!(other)
  end

  def mul(other)
    return dup.mul!(other)
  end

  def div(other)
    return dup.div!(other)
  end

  unless method_defined?(:square!)
    def square!
      self.height = self.width = [self.width, self.height].min
      return self
    end

    def square
      dup.square!
    end
  end

  alias :+ :add
  alias :- :sub
  alias :* :mul
  alias :/ :div

end