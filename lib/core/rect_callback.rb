#
# EDOS/lib/core/rect_callback.rb
#   by IceDragon
class RectCallback < Rect

  include MACL::Mixin::Callback

  def snapshot
    @snapshot = to_a
  end

  def snapshot_changed?
    @snapshot != to_a
  end

  def set(*args, &block)
    snapshot
    super(*args,&block)
    try_callback(:set) if snapshot_changed?
  end

  def empty(*args, &block)
    snapshot
    super(*args, &block)
    try_callback(:empty) if snapshot_changed?
  end

  def x=(n)
    snapshot
    super(n)
    try_callback(:x) if snapshot_changed?
  end

  def y=(n)
    snapshot
    super(n)
    try_callback(:y) if snapshot_changed?
  end

  def width=(n)
    snapshot
    super(n)
    try_callback(:width) if snapshot_changed?
  end

  def height=(n)
    snapshot
    super(n)
    try_callback(:height) if snapshot_changed?
  end

end