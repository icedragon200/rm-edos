#
# EDOS/lib/core/set.rb
#   by IceDragon
class Set

  def concat(array)
    array.each do |obj|
      self << obj
    end
  end unless method_defined?(:concat)

end