#
# EDOS/lib/class/sprite.rb
#   by IceDragon
#
# Sprite (EDOS Xtension)
# vr 2.0.2
class Sprite

  include Mixin::VisPos
  ### mixin
  include MACL::Mixin::Surface2Bang
  include Mixin::Visibility
  include Mixin::SpaceBoxBody
  include Mixin::WindowClient
  include Mixin::Automatable
  include Mixin::GetStateHash

  def move(x, y, z=0)
    self.x += x
    self.y += y
    self.z += z
  end

  def moveto(x, y, z=self.z)
    self.x, self.y, self.z = x, y, z
  end

  def cx=(x)
    self.x = x - self.width / 2.0
  end unless method_defined?(:cx=)

  def cy=(y)
    self.y = y - self.height / 2.0
  end unless method_defined?(:cy=)

  ##
  # width=(Integer n)
  def width=(n)
    src_rect.width = n
  end unless method_defined?(:width=)

  ##
  # height=(Integer n)
  def height=(n)
    src_rect.height = n
  end unless method_defined?(:height=)

  ##
  # set_oxy
  def set_oxy(ox=0, oy=0)
    self.ox, self.oy = ox, oy
    self
  end

  ##
  # z2
  def z2
    return z
  end

  ##
  # z2=(Integer new_z2)
  def z2=(new_z2)
    self.z = new_z2
  end

  ##
  # anchor_oxy!(ACNHOR ops)
  def anchor_oxy!(ops)
    anchor = ops[:anchor] || 5
    surface = ops[:surface] || Rect.new(0, 0, self.width, self.height)

    orect = Rect.new(0, 0, 1, 1)
    orect.align_to!(anchor: anchor, surface: surface)

    set_oxy(orect.x, orect.y)

    return nil
  end

  #### experimental
  def visx
    super + (viewport ? viewport.rect.x : 0)
  end

  def visy
    super + (viewport ? viewport.rect.y : 0)
  end

  def visx2
    super + (viewport ? viewport.rect.x : 0)
  end

  def visy2
    super + (viewport ? viewport.rect.y : 0)
  end

  ##
  # Automation
  alias :no_automate_initialize :initialize
  alias :no_automate_dispose :dispose
  alias :no_automate_update :update

  def initialize(*args, &block)
    no_automate_initialize(*args, &block)
    init_automations
  end

  def dispose
    dispose_automations
    no_automate_dispose
  end

  def update
    no_automate_update
    update_automations
  end

  ##
  # Clean up
  def dispose_bitmap
    self.bitmap.dispose
    self.bitmap = nil
  end

  ##
  # dispose_bitmap_safe
  def dispose_bitmap_safe
    dispose_bitmap unless self.bitmap.disposed? if self.bitmap
  end

  ##
  # dispose_all
  def dispose_all
    dispose_bitmap_safe
    dispose
  end

  ##
  # GetStateHash
  ##
  # @return [Hash<Symbol, Object>]
  def get_state_h
    super.merge(
      x: x, y: y, z: z, ox: ox, oy: oy,
      zoom_x: zoom_x, zoom_y: zoom_y,
      angle: angle, blend_type: blend_type,
      bush_depth: bush_depth, bush_opacity: bush_opacity,
      wave_speed: wave_speed, wave_amp: wave_amp,
      wave_phase: wave_phase, wave_length: wave_length,
      opacity: opacity,
      visible: visible,
      src_rect: src_rect.dup,
      color: color.dup, tone: tone.dup
    )
  end

  ##
  # @param [Hash<Symbol, Object>] hsh
  def set_state_h(hsh)
    hsh.each { |k, v| send(k.to_s+"=", v) }
    #self.x        = hsh[:x]
    #self.y        = hsh[:y]
    #self.z        = hsh[:z]
    #self.ox       = hsh[:ox]
    #self.oy       = hsh[:oy]
    #self.src_rect = hsh[:src_rect]
    #self.opacity  = hsh[:opacity]
    #self.visible  = hsh[:visible]
  end

  ##
  # Tone Flash
  alias :real_tone :tone
  alias :real_tone= :tone=
  alias :no_tone_flash_update :update

  def update(*args, &block)
    update_tone_flash
    no_tone_flash_update(*args, &block)
  end

  def tone
    @tone_abs ||= real_tone
  end

  def tone=(ntone)
    @tone_abs = ntone
    unless @_tone_flash
      self.real_tone = @tone_abs
    end
  end

  def tone_flash(ftone, duration)
    @_tone_flash = Convert.Tone(ftone)
    @_tone_flash_duration = @_tone_flash_duration_max = duration
  end

  def update_tone_flash
    if @_tone_flash
      self.real_tone = tone + (@_tone_flash * (@_tone_flash_duration.to_f / @_tone_flash_duration_max))
      @_tone_flash_duration -= 1
      if @_tone_flash_duration < 0
        @_tone_flash = nil
        @_tone_flash_duration = nil
        @_tone_flash_duration_max = nil
        self.real_tone = tone
      end
    end
  end

  private :update_tone_flash

end