#
# EDOS/lib/core/star_ruby-vector3f.rb
#   by IceDragon
module StarRuby
  class Vector3F

    include EDOS::Type::Vector3

    def to_vector2
      Vector2F.new(x, y)
    end

  end
end