#
# EDOS/lib/core/star_ruby-vector3i.rb
#   by IceDragon
module StarRuby
  class Vector3I

    include EDOS::Type::Vector3

    def to_vector2
      Vector2I.new(x, y)
    end

  end
end