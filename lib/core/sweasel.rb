class Sweasel

  attr_reader :value, :target_value

  def initialize(*args)
    case args.size
    when 1
      arg, = *args
      case arg
      when Hash
        value, time, easer = arg.fetch(:value), arg.fetch(:time), (arg[:easer] || :linear)
      when Array
        value, time, easer = *args
      else
        raise TypeError, "expected Array or Hash, but recieved #{arg}"
      end
    when 3
      value, time, easer = *args
    else
      raise ArgumentError, "expected 1 or 3 args but recieved #{args.size}"
    end
    @time = 0
    @timemax = time
    @src_value = @value = @target_value = value
    setup_easer(easer)
  end

  def rechase
    @src_value = @value
    @time = @timemax
  end

  ##
  # setup_easer(Object obj)
  def setup_easer(obj)
    @easer =  case obj
              when Symbol      then MACL::Easer.get_easer(obj)
              when MACL::Easer then obj
              else raise(TypeError,
                         "Expected type Easer or Symbol but received #{obj}")
              end
  end

  def target_value=(new_value)
    if @target_value != new_value
      @target_value = new_value
      rechase
    end
  end

  def update
    if @time > 0
      @value = @easer.ease(1 - (@time / @timemax.to_f), @src_value, @target_value, 1.0)
      @time -= 1
    end
  end

  def busy?
    @time > 0
  end

end