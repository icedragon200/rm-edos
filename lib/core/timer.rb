#
# EDOS/lib/core/timer.rb
#   by IceDragon
class Timer

  include Mixin::Activatable

  attr_reader :time
  attr_accessor :mtime
  attr_accessor :active

  def initialize(t)
    @time = @mtime = t.to_i
    @active = true
  end

  def zero
    @time = 0
    return self
  end

  def idle?
    active? && @time < 0
  end

  def rate
    return [[1 - (@time.to_f / @mtime), 0].max, 1].min
  end

  def set(new_time)
    @mtime = new_time.ot_i
    reset
  end

  def reset
    @time = @mtime
    return self
  end

  def update
    @time -= 1 if !idle? if active?
  end

end