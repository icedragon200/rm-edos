#
# EDOS/lib/core/tone.rb
#   by IceDragon
class Tone

  include MACL::Mixin::ToneMath

  attr_accessor :name

  def alpha_r
    return self.gray / 255.0
  end

  def lerp!(trg_tone, r)
    super(trg_tone, r)
    self.gray  = self.gray  + (trg_tone.gray  - self.gray)  * r
    return self
  end

  def to_yaml
    { red: red, green: green, blue: blue, gray: gray }.to_yaml
  end

end