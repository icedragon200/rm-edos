class Vector < MACL::Vector ; end
class Vector2 < StarRuby::Vector2F

  def abs
    vec = dup
    vec.x = vec.x.abs
    vec.y = vec.y.abs
    return vec
  end unless method_defined?(:abs)

  def lerp(trg, delta)
    self + (trg - self) * delta
  end

  def lerp_step!(trg, step)
    step = Convert.Vector2(step) unless step.is_a?(EDOS::Type::Vector2)
    if trg.x > self.x    then self.x = [self.x + step.x, trg.x].min
    elsif trg.x < self.x then self.x = [self.x - step.x, trg.x].max
    end
    if trg.y > self.y    then self.y = [self.y + step.y, trg.y].min
    elsif trg.y < self.y then self.y = [self.y - step.y, trg.y].max
    end
    self
  end

  def lerp_step(*args)
    dup.lerp_step!(*args)
  end

end
class Vector3 < StarRuby::Vector3F

  def abs
    vec = dup
    vec.x = vec.x.abs
    vec.y = vec.y.abs
    vec.z = vec.z.abs
    return vec
  end unless method_defined?(:abs)

  def lerp(trg, delta)
    self + (trg - self) * delta
  end

  def lerp_step!(trg, step)
    step = Convert.Vector3(step) unless step.is_a?(EDOS::Type::Vector3)
    if trg.x > self.x    then self.x = [self.x + step.x, trg.x].min
    elsif trg.x < self.x then self.x = [self.x - step.x, trg.x].max
    end
    if trg.y > self.y    then self.y = [self.y + step.y, trg.y].min
    elsif trg.y < self.y then self.y = [self.y - step.y, trg.y].max
    end
    if trg.z > self.z    then self.z = [self.z + step.z, trg.z].min
    elsif trg.z < self.z then self.z = [self.z - step.z, trg.z].max
    end
    self
  end

  def lerp_step(*args)
    dup.lerp_step!(*args)
  end

end
class Vector4 < MACL::Vector4 ; end