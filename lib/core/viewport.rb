#
# EDOS/lib/core/viewport.rb
#   by IceDragon
class Viewport

  attrs = [:x, :y, :width, :height]
  #attrs += [:x2, :y2] # MACL Surface

  attrs.each do |sym|
    module_eval(%Q(
      def #{sym}
        return self.rect.#{sym}
      end

      def #{sym}=(n)
        self.rect.#{sym} = n
      end
    ))
  end

  include MACL::Mixin::Surface2Bang

end