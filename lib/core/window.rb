#
# EDOS/lib/class/window.rb
#   by IceDragon
class Window

  include MACL::Mixin::Surface2Bang

  ### redraw* is a WindowAddons function
  #
  def _redraw!
    _redraw
  end

  def _redraw
  end

end