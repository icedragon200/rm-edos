#
# EDOS/lib/daemon/DAlarm.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.0.0
class Daemon::DAlarm < MACL::Daemon

  ### instance_attributes
  attr_accessor :disabled

  def init
    @ticks = 0
  end

  ##
  # disabled?
  def disabled?
    @disabled
  end

  ##
  # update
  def update
    return if disabled?
    time = Time.now
    if @last_minute != m = time.min
      @last_minute = m
      msg = "Time is now #{time.strftime("%R")}"
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:time, msg])
    end
    @ticks += 1
  end

end