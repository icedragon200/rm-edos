#
# EDOS/lib/daemon/DClock.rb
#   dc ??/??/2012
#   dm 29/03/2013
# vr 1.0.0
class Daemon::DClock < MACL::Daemon

  ##
  # init
  def init
    @clock_sprite = Sprite::MerioTextBox.new(nil, 96, Metric.ui_element_sml)
    @clock_sprite.use_background = false
    @clock_sprite.merio_font_size = :micro
    @clock_sprite.x = 0
    @clock_sprite.y = 0
    @clock_sprite.z = 0xAFFF
    @clock_sprite.align_to!(anchor: 3, surface: Screen.panel)
    @last_time = Time.now
    @tone = Palette['droid_blue'].to_tone
  end

  ##
  # update
  def update
    if @last_time.sec != (n = Time.now).sec
      @last_time = n
      @clock_sprite.set_text(n.strftime('%I:%M:%S %p'), 2)
    end
    mx, my = *Mouse.position
    if @clock_sprite.contains?(mx, my)
      @clock_sprite.tone.set(@tone)
    else
      @clock_sprite.tone.set(0, 0, 0, 0)
    end
    @clock_sprite.update
  end

end