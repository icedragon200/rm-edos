#
# EDOS/lib/daemon/DDemoplay.rb
#   by IceDragon
#   dc ??/??/2012
#   dm 29/03/2013
# vr 1.0.0
class Daemon::DDemoplay < MACL::Daemon

  attr_reader :count, :countmax

  def init
    @count = 0
    # // 5 Minutes
    @countmax = Graphics.frame_rate * 60 * 1.0
    @exe = false
  end

  def rate
    @count / @countmax
  end

  def update
    return if @exe
    @count += 1
    exe! if @count >= @countmax
    #puts "#@count/#@countmax"
  end

  def exe!
    @exe = true
    color_clay       = Palette['clay']
    color_clay_light = Palette['clay-light']

    sp = Sprite.new(nil)

    bmp = sp.bitmap  = Bitmap.new((Graphics.width * 0.85).to_i, 128)
    bmp.fill_rect(bmp.rect, color_clay)
    bmp.fill_rect(bmp.rect.contract(anchor: 5, amount: 4), color_clay_light)
    bmp.fill_rect(bmp.rect.contract(anchor: 5, amount: 5), color_clay)
    bmp.font.name = Font.default_name.dup
    bmp.font.size = Font.default_size
    rect = bmp.rect.dup
    rect.height = 32
    bmp.draw_text(rect, "Your demo time has expired", 1)
    rect.y = sp.height - rect.height
    bmp.draw_text(rect, "Press the C button to return to the title menu", 1)
    bmp.font.name = ['ARIAL.TTF'] + bmp.font.name
    bmp.font.size = 54
    bmp.draw_text(bmp.rect, "Thank You For Playing", 1)

    sp.x = (Graphics.width - sp.width) / 2
    sp.y = (Graphics.height - sp.height) / 2
    sp.z = 10000
    sp.opacity = 0
    t = 0
    time_fadein = 90.0
    #Daemon::Idle.stop!
    loop do
      Main.update
      sp.update
      if sp.opacity < 255
        t += 1
        sp.opacity = ((t / time_fadein) ** 4) * 255
      end
      if Input.trigger?(:C)
        exit
      end
    end
  end

end