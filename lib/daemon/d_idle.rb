#
# EDOS/lib/daemon/DIdle.rb
#   dc 06/09/2012
#   dm 06/09/2012
# vr 1.0.1
class Daemon::DIdle < MACL::Daemon

  attr_accessor :idle
  attr_reader :idle_threshold

  def init
    @paused = false
    @idle = 0
    @idle_threshold = Graphics.frame_rate * 60 * 0.15 # // Idle after 1.5 minutes
    create_idle_sprite()
  end

  def stop!
    @idle = 0
    pause()
  end

  def idle_rate
    @idle / @idle_threshold.to_f
  end

  def idle?
    @idle >= @idle_threshold
  end

  def update
    @idle += 1 unless @paused
    update_idle()
  end

  def update_idle
    @idle = 0 if Input.any_input? unless @paused
    n = 255.0 / Graphics.frame_rate
    if idle?
      @idle_sprite.opacity += n if @idle_sprite.opacity < 255
    else
      @idle_sprite.opacity -= n if @idle_sprite.opacity > 0
    end
  end

  def create_idle_sprite
    @idle_sprite = Sprite.new(nil)
    bmp = @idle_sprite.bitmap = Bitmap.new(Graphics.width, Graphics.height)
    bmp.fill_rect(bmp.rect, Color.new(0, 0, 0, 198))
    @idle_sprite.opacity = 0
    @idle_sprite.z = 0xBFFF
  end

  def dispose_idle_sprite
    @idle_sprite.bitmap.dispose
    @idle_sprite.dispose
    @idle_sprite = nil
  end

end