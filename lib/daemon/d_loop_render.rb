#
# EDOS/lib/daemon/DLoopRender.rb
#   by IceDragon
#   dc 03/07/2013
#   dm 03/07/2013
# vr 1.0.0
#   This daemon is kinda useless, all it does is trap the game in a screen loop
#   which means it bypasses all logical updates and only re-renders the
#   active drawable objects over and over again like a normal screen update
class Daemon::DLoopRender < MACL::Daemon

  def init
    @trapped = false
  end

  def update
    lock! if !@trapped && Input.trigger?(:F2)
  end

  def lock!
    @trapped = true
    EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:loop_render, "Loop Render: Enabled"])
    loop do
      Main.update
      break if Input.trigger?(:F2, :ESC)
    end
    EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:loop_render, "Loop Render: Disabled"])
    @trapped = false
  end

end