#
# EDOS/lib/daemon/DMouseSparks.rb
# vr 1.0.0
class Daemon::DMouseSparks < MACL::Daemon

  def init
    @halted = false
    @bitmap = Bitmap.new(Graphics.width, Graphics.height)
    @particle_sys = Palila::ParticleSystem.new
    @vec = @particle_sys.location

    @particle_count = 256 / 2
    @particle_size = Rect.new(0, 0, 2, 2)

    @sp = Sprite.new
    @sp.z = 0xFFFF
    @sp.bitmap = @bitmap
  end

  def update
    if(Mouse.left_click?)#(@vec.x != Mouse.x || @vec.y != Mouse.y)
      x = @vec.x = Mouse.x
      y = @vec.y = Mouse.y

      x = 0 if x < 0
      y = 0 if y < 0

      col = Graphics.starruby.screen[x, y]
      col = ColorTool.invert_color(col)

      @particle_count.times do
        @particle_sys.add_particle(Particle::MouseSpark).color = col
      end
    end

    if @particle_sys.particles?
      @bitmap.clear
      @particle_sys.update_particles do |particle|
        @particle_size.x = particle.x
        @particle_size.y = particle.y

        r = particle.life / particle.lifespan.to_f
        #col = @color_list[((1 - r) * @color_list.size).to_i]

        col = particle.color
        col.alpha = r * 255

        @bitmap.fill_rect(@particle_size, col)
      end
      @bitmap.blur
    end
  end

  def halt!
    @halted = true
  end

  def unhalt!
    @halted = false
  end

end