#
# EDOS/lib/daemon/DNotify.rb
#   by IceDragon
#   dc 27/04/2013
#   dm 19/06/2013
# vr 1.1.0
#   CHANGELOG
#     vr 1.1.0 [19/06/2013]
#       Changed server model from Aviary to EDOS::Server
class Daemon::DNotify < MACL::Daemon

  ##
  # init
  def init
    @notify_sp = Sprite.new
    @notify_sp.z = 0xFFFF
    @buffer = []
    EDOS.server.listen_to(self, EDOS::CHANNEL_EDOS_FUN)
    EDOS.server.listen_to(self, EDOS::CHANNEL_EDOS_NOTE)
    @ticks = 0
  end

  ##
  # busy? -> Boolean
  def busy?
    @ticks > 0
  end

  ##
  # receive_data(EDOS::Server::Channel from, Object* data)
  def receive_data(from, data)
    @buffer.push(data)
  end

  ## TEMP
  # eventually I need to setup a unload system for daemons
  def dispose
    super
    @notify_sp.dispose_all
  end

  ##
  # update
  def update
    if !@buffer.empty? && !busy?
      @ticks = (Graphics.frame_rate * 1).to_i
      refresh_notify
    end
    @notify_sp.update

    if @ticks > 0
      @ticks -= 1
    end

    if @ticks > 0 && @notify_sp.opacity < 255
      @notify_sp.opacity += 256 / 15.0
    elsif @ticks == 0 && @notify_sp.opacity > 0
      @notify_sp.opacity -= 256 / 15.0
    end
  end

  ##
  # refresh_notify
  def refresh_notify
    data  = @buffer.shift.dup
    flag  = data.shift

    font = DrawExt::Merio.new_font(:dark, :medium, :enb)

    if flag == :birthday
      whom = data.shift
      age  = data.shift
      whom = whom.call if whom.is_a?(Proc)
      str  = "#{whom.name} is now #{age} years old"
    elsif flag == :time
      str  = data.shift
    else
      warn("#{self.class.name} | unhandled flag: #{flag}")
      str = data.shift
    end

    #w, h = font.text_size(str)
    #w += Metric.contract * 2
    #w = Graphics.width
    w, h = Screen.panel.width, Screen.panel.height
    @notify_sp.dispose_bitmap_safe
    @notify_sp.bitmap = Bitmap.new(w, h)

    ## Drawing Code
    bmp   = @notify_sp.bitmap
    rect  = bmp.rect
    rect2 = rect.contract(anchor: 5, amount: Metric.contract)
    bmp.clear
    #DrawExt::Merio.draw_light_rect(bmp, bmp.rect)
    bmp.font.set(font)
    bmp.draw_text(rect2, str)
    @notify_sp.opacity = 0
    @notify_sp.align_to!(anchor: 7, surface: Screen.panel)
  end

end