#
# EDOS/lib/daemon/DPanel.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.0.0
class Daemon::DPanel < MACL::Daemon

  ##
  # init
  def init
    @panel_sp = Sprite.new(nil)
    @panel_sp.bitmap = Bitmap.new(Screen.panel.width, Screen.panel.height)
    @panel_sp.z = 1
    @panel_sp.align_to!(anchor: 2, surface: Screen.panel)
    @panel_sp.bitmap.merio.draw_light_rect(@panel_sp.bitmap.rect)
  end

  ##
  # update
  def update
    @panel_sp.update
  end

end
#Daemon.register("dpanel", "1.0.1")