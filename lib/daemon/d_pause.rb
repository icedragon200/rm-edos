#
# src/daemon/dpause.rb
# vr 1.0

##
# class DPause
#
# Pause State Daemon
class Daemon::DPause < MACL::Daemon

  def self.hold_playtime
    last_frame_count = Graphics.frame_count
    yield
    Graphics.frame_count = last_frame_count
  end

  def init
    @paused = false
    @halted = false
  end

  def enter_pause_state
    @paused = true
    @last_frame_count = Graphics.frame_count

    pause_frame_count = 0

    # Graphics
    if PAUSE_DIMMING
      dim_sprite = Sprite.new
      dim_sprite.z = 0x2FFA
      dim_sprite.bitmap = Bitmap.new(Graphics.width, Graphics.height)
      dim_sprite.bitmap.fill_rect(
        dim_sprite.bitmap.rect, Color.new(0, 0, 0, 255))
      dim_sprite.opacity = PAUSE_DIMMING_TRANS ? 255 : PAUSE_DIM_ALPHA
    end

    # Pause Text
    if PAUSE_TEXT
      sp_pause_text = Sprite.new
      sp_pause_text.bitmap = Bitmap.new(Graphics.width, Graphics.height)
      sp_pause_text.bitmap.font = PAUSE_FONT.dup
      sp_pause_text.bitmap.draw_text(
        sp_pause_text.bitmap.rect, PAUSE_TEXT_STR, 1)
      sp_pause_text.z = 0x2FFB
    end

    # Audio
    if PAUSE_AUDIO_FADE
      bgm = RPG::BGM.last
      org_volume = bgm.volume
      fadeout_time = fadeout_time_max = PAUSE_VOL_FADE_TIME.to_f
    end

    # Main Loop
    loop do
      fade_rate = fadeout_time / fadeout_time_max
      if PAUSE_DIMMING and PAUSE_DIMMING_TRANS
        dim_sprite.opacity = 255 - (255 * ((PAUSE_DIM_ALPHA / 255.0) *
          (1 - fade_rate)))
      end

      # Audio Fade
      if fadeout_time > 0
        fadeout_time -= 1
        bgm.volume = org_volume - (org_volume *
          PAUSE_VOL_RATE * (1 - fade_rate))
        bgm.play
      end if PAUSE_AUDIO_FADE

      case PAUSE_TEXT_ANIM
      when 0 # None
      when 1 # Flick
        sp_pause_text.visible =
          pause_frame_count % (PAUSE_TEXT_ANIM_TIME * 2) > PAUSE_TEXT_ANIM_TIME
      when 2 # Pulse
        n = pause_frame_count % (PAUSE_TEXT_ANIM_TIME * 2)
        if n > PAUSE_TEXT_ANIM_TIME
          r = (PAUSE_TEXT_ANIM_TIME - (n - PAUSE_TEXT_ANIM_TIME)) /
            PAUSE_TEXT_ANIM_TIME.to_f
        else
          r = n / PAUSE_TEXT_ANIM_TIME.to_f
        end
        sp_pause_text.opacity = 255 * r
      end

      Main.update
      break if Input.trigger?(PAUSE_BUTTON)

      pause_frame_count += 1
    end

    # Audio
    if PAUSE_AUDIO_FADE
      bgm.volume = org_volume
      bgm.play
    end

    # Graphics
    if PAUSE_DIMMING
      dim_sprite.bitmap.dispose
      dim_sprite.dispose
    end

    # Pause Text
    if PAUSE_TEXT
      sp_pause_text.bitmap.dispose
      sp_pause_text.dispose
    end

    @paused = false

    Graphics.frame_count = @last_frame_count unless PAUSE_PLAYTIME_ADVANCES

    return true
  end

  def update
    return if @halted
    enter_pause_state if !@paused && Input.trigger?(PAUSE_BUTTON)
  end

  def halt!
    @halted = true
  end

  def unhalt!
    @halted = false
  end

end