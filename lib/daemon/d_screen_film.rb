#
# EDOS/lib/daemon/dscreenfilm.rb
#   by IceDragon
#   dc 10/03/2013
#   dm 10/03/2013
# vr 1.0.0
class Daemon::DScreenFilm < MACL::Daemon

  def init
    @recording = false
    @buffer = []
    @frames = 0
    Dir.mkdir(RECORD_DIRNAME) unless Dir.exist?(RECORD_DIRNAME)
  end

  def clear_frame_cache
    Dir.mkdir(RECORD_DIRNAME) unless Dir.exist?(RECORD_DIRNAME)
    files = Dir.glob(File.join(RECORD_DIRNAME, "*.png"))
    for filename in files
      File.delete(filename)
    end
  end

  def save_frame(index, texture)
    Dir.mkdir(RECORD_DIRNAME) unless Dir.exist?(RECORD_DIRNAME)
    filename = File.join(RECORD_DIRNAME, "%07d.png" % index)
    if texture.save(filename)
      p "#{self.class.name} | Saved frame: #{index} as #{filename}"
    end
  end

  def clear_buffer
    for i, texture in @buffer
      texture.dispose
    end
    @buffer.clear
  end

  def start_record
    msg = "#{self.class.name} | Started recording"
    EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:daemon, msg])
    clear_frame_cache
    clear_buffer
    @frames = 0
    @frame_index = 0
  end

  def stop_record
    msg = "#{self.class.name} | Stopped recording"
    EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:daemon, msg])
    for i, texture in @buffer
      save_frame(i, texture)
    end
    clear_buffer
  end

  def update
    if @recording
      if @frames % @@film_interval == 0
        @buffer.push([@frame_index, Graphics.starruby.screen.dup])
        @frame_index += 1
      elsif RECORD_REALTIME
        i, t = @buffer.shift
        if i && t
          save_frame(i, t)
          t.dispose
        end
      end
      @frames += 1
    end
    if Input.trigger?(RECORD_BUTTON)
      @recording = !@recording
      @recording ? start_record : stop_record
    end
  end

end