#
# EDOS/lib/daemon/dscreenshot.rb
#   by IceDragon
#   dc 10/03/2013
#   dm 10/03/2013
# vr 1.1.0
#   CHANGELOG
#     Added SCREENSHOT_BACKGROUND_COLOR
class Daemon::DScreenshot < MACL::Daemon

  def init
    @index = 0
    Dir.mkdir(DIRNAME) unless Dir.exist?(DIRNAME)
  end

  def save_screenshot(texture)
    Dir.mkdir(DIRNAME) unless Dir.exist?(DIRNAME)
    @index = 0
    fn_spf = FILENAME_SPF % "%04d"
    filename = nil
    (filename = File.join(DIRNAME, fn_spf % @index); @index += 1) until filename && !File.exist?(filename)

    new_texture = StarRuby::Texture.new(texture.width, texture.height)
    new_texture.fill(SCREENSHOT_BACKGROUND_COLOR)
    new_texture.render_texture(texture, 0, 0, blend_type: :alpha)
    if new_texture.save(filename)
      yield filename if block_given?
    end
    new_texture.dispose
  end

  def update
    if Input.trigger?(SCREENSHOT_BUTTON)
      save_screenshot(Graphics.starruby.screen) do |filename|
        EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:screenshot, "Screenshot saved to %s" % filename])
      end
    end
  end

end