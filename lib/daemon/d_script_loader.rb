#
# EDOS/lib/daemon/DScriptLoader.rb
#   by IceDragon
#   dc ??/??/2012
#   dm 03/05/2013
# vr 1.0.0
class Daemon::DScriptLoader < MACL::Daemon

  def init

  end

  def update
    pop_script_loader if !@loading_script && Input.trigger?(:F8)
  end

  def pop_script_loader
    @loading_script = true

    @window = Window::ScriptLoader.new

    on_ok = proc do
      filename = @window.current_item
      @window.close
      begin
        str = File.read(filename)
        #eval(str)
        RubyVM::InstructionSequence.compile(str).eval
      rescue(Exception) => ex
        Sound.play_buzzer
        puts("Error Ocurred while executing script: %s %s" % [filename, ex.inspect])
        puts(ex.backtrace.join("\n"))
      end
    end

    on_cancel = proc do
      @window.close
    end

    @window.set_handler(:ok, on_ok)
    @window.set_handler(:cancel, on_cancel)
    @window.align_to!(anchor: 5) # center the window
    @window.activate.select(0)
    @window.z = 256

    loop do
      Main.update

      @window.update
      break unless @window.active
    end

    @loading_script = false
    return self
  ensure
    @window.dispose if @window and !@window.disposed?
    @window = nil
  end

end