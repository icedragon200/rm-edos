#
# EDOS/lib/daemon/DStepDrawing.rb
#   by IceDragon
#   dc 15/06/2013
#   dm 15/06/2013
# vr 1.0.0
class Daemon::DStepDrawing < MACL::Daemon

  def update
    return if @step_draw
    step_draw if Input.trigger?(:f3)
  end

  def step_draw
    @step_draw = true
    objects = Graphics.canvas.drawable
    old_state = Hash[objects.map { |o| [o, o.visible] }]
    objects.each_with_object(false, &:visible=)
    index = 0
    old_index = -1

    loop do
      Main.update
      if Input.trigger?(:UP) || Input.trigger?(:LEFT)
        index = (index - 1) % objects.size
      elsif Input.trigger?(:DOWN) || Input.trigger?(:RIGHT)
        index = (index + 1) % objects.size
      end
      if Input.trigger?(:B)
        break
      end
      if old_index != index
        objects[old_index].visible = false
        old_index = index
        objects[index].visible = true
        obj = objects[index]
        p "index: %d | %s" % [index, obj]
      end
    end
    old_state.each_pair do |obj, vis|
      obj.visible = vis
    end
    @step_draw = false
  end

end