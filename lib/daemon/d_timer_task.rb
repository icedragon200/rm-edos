#
# EDOS/lib/daemon/DTimerTask.rb
#   by IceDragon
#   dc ??/??/2012
#   dm 03/05/2013
# vr 1.0.0
class Daemon::DTimerTask < MACL::Daemon
  class Task

    def initialize(func=nil, &block)
      @block = func || block
    end

    def update
      @time -= 1
      if @time <= 0 and !@executed
        @executed = true
        @block.()
      end
    end

    def done?
      @time <= 0
    end

    def update?
      update
      return !done?
    end

  end

  def init
    @tasks = []
  end

  def add(task)
    @tasks.push(task)
    self
  end

  def remove(task)
    @tasks.delete(task)
    self
  end

  def new_task(*args, &block)
    add(Task.new(*args, &block))
  end

  def update
    @tasks.select!(&:update?) unless @tasks.empty?
  end

  alias :task :new_task

end