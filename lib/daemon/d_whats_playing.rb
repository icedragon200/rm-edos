#
# src/daemon/dwhatsplaying.rb
#
class Daemon::DWhatsPlaying < MACL::Daemon

  def init
    @play_sprite = Sprite::TextStrip.new(nil, 256, 16)

    @play_sprite.x = Graphics.width - @play_sprite.width
    @play_sprite.z = 0xBFFF

    @last_bgm = nil
    @time = 0
  end

  def update
    @time -= 1 if @time > 0

    if @last_bgm != (n = RPG::BGM.last)
      name = n.name
      @play_sprite.text  = name.empty? ? "Nuthins Playing" : "Now playing: #{name}"
      @play_sprite.align = 1
      @play_sprite.refresh
      @last_bgm = n
      @time = Graphics.frame_rate * 6
    end

    if @time > 0
      @play_sprite.opacity += 255.0 / Graphics.frame_rate if @play_sprite.opacity < 255
    else
      @play_sprite.opacity -= 255.0 / Graphics.frame_rate if @play_sprite.opacity > 0
    end
  end

end