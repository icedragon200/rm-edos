#
# EDOS/lib/daemon/DZSweep.rb
#   by IceDragon
#   dc 15/06/2013
#   dm 15/06/2013
# vr 1.0.0
class Daemon::DZSweep < MACL::Daemon

  class SpritePosition < Sprite

    attr_reader :parent

    def initialize(viewport, id, parent)
      super(viewport)
      @id = id
      @parent = parent
      self.bitmap = Bitmap.new(24 * 7, 16)
      self.oy = self.bitmap.height
      self.bitmap.merio.font_config(:light, :micro, :enb)
    end

    def dispose
      dispose_bitmap_safe
      super
    end

    def update
      super
      px2 = @parent.x2
      py2 = @parent.y2
      if px2 != @last_x || py2 != @last_y
        refresh
        @last_x = px2
        @last_y = py2
      end
      self.x = [[px2, 0].max, Graphics.width - width].min
      self.y = [[py2, 0].max, Graphics.height - height].min
    end

    def refresh
      self.bitmap.clear
      px = @parent.x
      py = @parent.y
      pz = @parent.z
      px2 = @parent.x2
      py2 = @parent.y2
      v = @parent.viewport ? '=' : '.'
      self.bitmap.merio.draw_dark_rect(self.bitmap.rect)
      self.bitmap.draw_text(self.bitmap.rect, "[#{@id}]#{v}|#{pz}| #{px},#{py},#{px2},#{py2}", 1)
    end

  end

  class SpriteOutline < Sprite

    attr_reader :parent
    attr_accessor :mode

    def initialize(viewport, parent)
      super(viewport)
      @parent = parent
      @mode = :vis
    end

    def dispose
      dispose_bitmap_safe
      super
    end

    def weight
      1
    end

    def outline_color
      case @mode
      when :vis
        Palette['droid_blue_light']
      when :rel
        Palette['droid_red_light']
      when :size
        Palette['droid_yellow_light']
      end
    end

    def outline_rect
      case @mode
      when :vis
        if bmp = @parent.respond_to?(:bitmap) && @parent.bitmap
          rw = bmp.width
          rh = bmp.height
        else
          rw = @parent.width
          rh = @parent.height
        end
        Rect.new(@parent.x, @parent.y, rw, rh)
      when :rel
        Rect.new(@parent.x, @parent.y, @parent.width, @parent.height)
      when :size
        Rect.new(@parent.visx, @parent.visy, @parent.width, @parent.height)
      end
    end

    def update_bitmap
      r = outline_rect
      if !self.bitmap || self.bitmap.disposed? ||
       (bitmap.width != r.width || bitmap.height != r.height)
        dispose_bitmap_safe
        self.bitmap = Bitmap.new(r.width + weight * 2, r.height + weight * 2)
        self.ox = weight
        self.oy = weight
        refresh
      end
    end

    def update_position
      r = outline_rect
      self.x = r.x
      self.y = r.y
    end

    def update
      update_bitmap
      update_position
      super
    end

    def refresh
      self.bitmap.clear
      align = 2
      DrawExt.draw_rect_outline(bitmap, bitmap.rect, outline_color, weight, align)
    end

  end

  def update
    return if @zsweep
    zsweep if Input.trigger?(:f5)
  end

  def zsweep
    @zsweep = true
    objects = ObjectSpace.each_object(SRRI::Interface::IZOrder).to_a
    objects.reject! { |o| o.is_a?(Viewport) || o.disposed? }
    old_state = Hash[objects.map { |o| [o, o.visible] }]
    objects.each_with_object(false, &:visible=)
    zs = objects.map { |o| (v=o.viewport) ? v.z : o.z }.uniq.sort

    sp = Sprite.new(nil)
    sp.z = 999999
    sp.bitmap = Bitmap.new(Graphics.width, Metric.ui_element)

    index = 0
    old_index = -1
    active_objects = []
    draw_individual_names = false
    enabled_canvas = true
    pos_sprites = []
    outline_sprites = []

    redraw_canvas = -> do
      outline_sprites.each(&:dispose)
      outline_sprites.clear
      active_objects.each_with_index do |obj, i|
        next if obj.disposed?
        outline_sprites << (spo = SpriteOutline.new(nil, obj))
        spo.mode = :vis
        spo.z = sp.z - 2
        outline_sprites << (spo = SpriteOutline.new(nil, obj))
        spo.mode = :rel
        spo.z = sp.z - 3
        outline_sprites << (spo = SpriteOutline.new(nil, obj))
        spo.mode = :size
        spo.z = sp.z - 4

        if draw_individual_names
          tx_rect = r.dup
          if tx_rect.width < tx_rect.height || tx_rect.width < Graphics.width / 3
            tx_rect.width = Graphics.width
            tx_rect.x2 = tx_rect.x
            tx_rect.x = 0 if tx_rect.x < 0
          end

          b = canvas.bitmap
          b.merio.font_config(:light, :h1, :enb)
          b.font.outline = true
          b.draw_text(tx_rect, i, MACL::Surface::ANCHOR_NW)
          b.merio.font_config(:light, :small, :enb)
          b.font.outline = true
          b.draw_text(tx_rect, obj.class.name, MACL::Surface::ANCHOR_SE)
        else
          #STDERR.puts("%03d: %s | %s" % [i, obj.class, obj.made_by[0]])
          STDERR.puts("%03d: %s" % [i, obj.class])
        end
      end
    end

    loop do
      Main.update
      if Input.trigger?(:UP) || Input.trigger?(:LEFT)
        index = (index - 1) % zs.size
      elsif Input.trigger?(:DOWN) || Input.trigger?(:RIGHT)
        index = (index + 1) % zs.size
      elsif Input.trigger?(:C)
        enabled_canvas = true
        redraw_canvas.()
      elsif Input.trigger?(:A)
        enabled_canvas = false
        canvas.bitmap.clear
      elsif Input.trigger?(:B) || Input.trigger?(:f5)
        break
      end
      if old_index != index
        old_index = index
        tz = zs[index]
        active_objects.clear
        pos_sprites.each(&:dispose)
        pos_sprites.clear

        objects.each do |obj|
          if obj.viewport ? obj.viewport.z == tz : obj.z == tz
            obj.visible = true
            active_objects << obj
          else
            obj.visible = false
          end
        end

        active_objects.each_with_index do |sp, i|
          pos_sprites << (spo = SpritePosition.new(nil, i, sp))
          spo.z = sp.z - 1
        end

        sp.bitmap.clear
        text_rect = sp.bitmap.rect.contract(anchor: 5, amount: Metric.contract)
        #DrawExt::Merio.draw_dark_rect(sp.bitmap, sp.bitmap.rect)
        sp.bitmap.merio.font_config(:light, :default, :enb)
        sp.bitmap.font.outline = true
        sp.bitmap.draw_text(text_rect, "Current Z: {z: %s}" % tz)
        sp.align_to!(anchor: 5)

        if enabled_canvas
          redraw_canvas.()
        end
      end
      pos_sprites.each(&:update)
      outline_sprites.each(&:update)
    end

    outline_sprites.each(&:dispose)
    pos_sprites.each(&:dispose)
    sp.dispose_all
    old_state.each_pair do |obj, vis|
      obj.visible = vis
    end
    pos_sprites.clear
    outline_sprites.clear
    @zsweep = false
  end

end