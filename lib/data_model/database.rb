module Mythryl
  class Database < Hash

    attr_reader :name

    def initialize(name, entries={})
      @name = name
      super(entries)
    end

  end
end