module Mythryl
  module DataModel
    class BaseItem < BaseModel
    end
  end
end
require_relative 'base_item/feature'
module Mythryl
  module DataModel
    class BaseItem

      ##
      # @return [Array<BaseItem::Feature>]
      field :features, type: Array,   default: proc {[]}
      field :icon_id,  type: Integer, default: 0
      field :type_id,  type: Integer, default: 0

    end
  end
end