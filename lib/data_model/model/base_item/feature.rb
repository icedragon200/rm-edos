module Mythryl
  module DataModel
    class BaseItem
      class Feature < BaseModel

        ##
        # @return [Integer]
        field :data_id, type: Integer, default: 0
        ##
        # @return [Integer]
        field :param_1, type: Integer, default: 0
        ##
        # @return [Integer]
        field :param_2, type: Integer, default: 0
        ##
        # @return [Float]
        field :param_3, type: Float, default: 0.0
        ##
        # @return [Float]
        field :param_4, type: Float, default: 0.0

      end
    end
  end
end