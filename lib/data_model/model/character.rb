module Mythryl
  module DataModel
    class Character < BaseModel

      field :filename, type: String,  default: proc {""}
      field :hue,      type: Integer, default: 0

    end
  end
end