module Mythryl
  module DataModel
    class EquipItem < UsableItem

      field :equip_type_id, type: Integer, default: 0

    end
  end
end