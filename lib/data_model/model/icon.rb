module Mythryl
  module DataModel
    class Icon < BaseModel

      field :index,      type: Integer, default: 0
      field :iconset_id, type: Integer, default: 0

    end
  end
end