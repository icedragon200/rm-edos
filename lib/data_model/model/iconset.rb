module Mythryl
  module DataModel
    class Iconset < BaseModel

      field :filename, type: String, default: proc {""}
      field :params,   type: Hash,   default: proc {{}}

    end
  end
end