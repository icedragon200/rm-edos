#
#
# Locale is responsible for localization / vocab of the game
# its internals are a hash with a master key chain and a set strings
# with the proper name
module Mythryl
  module DataModel
    class Locale < BaseModel

      ##
      # @return Hash<String, Hash<String, String>>
      field :locale, type: Hash, default: proc {{}}

    end
  end
end