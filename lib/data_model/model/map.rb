module Mythryl
  module DataModel
    class Map < BaseModel

      field :width,       type: Integer,         default: 0
      field :height,      type: Integer,         default: 0
      field :data,        type: Mythryl::Matrix, default: proc { |k, m| k.new(m.width, m.height) }
      field :shadow_data, type: Mythryl::Matrix, default: proc { |k, m| k.new(m.width, m.height) }

    end
  end
end