#
# EDOS/lib/data_model/model/name.rb
#   by IceDragon
module Mythryl
  module DataModel
    class Name < BaseModel

      field :first,  type: String, default: proc {""}
      field :middle, type: String, default: proc {""}
      field :last,   type: String, default: proc {""}

    end
  end
end