#
#
# The Recipe model defines the Crafting recipes found in the game
module Mythryl
  module DataModel
    class Recipe < BaseModel

      field :ingredients, type: Array, default: proc {[]}

    end
  end
end