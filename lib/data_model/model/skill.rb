module Mythryl
  module DataModel
    class Skill < UsableItem

      field :skill_type_id, type: Integer, default: 0

    end
  end
end