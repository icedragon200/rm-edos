module Mythryl
  module DataModel
    class UsableItem < BaseItem

      field :usable_type_id, type: Integer, default: 0

    end
  end
end
require_relative 'usable_item/effect'