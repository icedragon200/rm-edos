#
# module/drawext/core.rb
#
module DrawExt

  @flags = []

  def self.flags
    return @flags[-1]
  end

  def self.flag_set(key, value)
    (@flags[-1] ||= {})[key] = value
  end

  def self.flag_clear(key)
    if fl = flags
      fl.delete(key)
    end
  end

  def self.flag?(key)
    flgs = flags
    return false unless flgs
    return flgs.key?(key)
  end

  def self.flag(key)
    flgs = flags
    return nil unless flgs
    return flgs[key]
  end

  def self.restore
    @flags.pop
  end

  def self.snapshot
    flgs = flags || {}
    @flags.push(flgs.dup)
    if block_given?
      yield self
      restore
    end
  end

end

#require_relative 'core/exper'
require_relative 'core/blend'
require_relative 'core/border'
require_relative 'core/box'
require_relative 'core/distortion'
require_relative 'core/draw_icon'
require_relative 'core/gauge'
require_relative 'core/gauge_specia'
require_relative 'core/outline'
require_relative 'core/padded_rect'
require_relative 'core/repeat'
require_relative 'core/slant'
require_relative 'core/special'
require_relative 'core/subtractive'
require_relative 'core/text'