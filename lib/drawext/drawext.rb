require_relative 'core'
require_relative 'include'
require_relative 'helpers'
require_relative 'styler'
require_relative 'palette'
require_relative 'gauge_palette'
require_relative 'merio'

EDOS::Loader.config.add("DrawExt-Const", 4) do
  require_relative 'const'
end