#
# EDOS/core/edos.rb
#   by IceDragon
#
# EDOS/lib/module/edos.rb
#   by IceDragon
#   dc 27/04/2013
#   dm 27/04/2013
# vr 0.0.1
require_relative 'edos/type'
require_relative 'edos/bit_control'
require_relative 'edos/server'

module EDOS
  module Loader
    class << self

      include MACL::Mixin::Log

      attr_accessor :config
      attr_accessor :unload

      def log=(new_log)
        super(new_log)
        @config.log = new_log
        @unload.log = new_log
      end

      def init
        @config = MACL::Linara.new
        @unload = MACL::Linara.new
      end

      def run_config
        @config.run
        #@config = nil # cleanup
      end

      def run_unload
        @unload.run
        #@unload = nil
      end

    end
    init
  end

  extend MACL::Mixin::Log

  edos_mask = BitControl.char_mask("EDOS") << 8
  CHANNEL_EDOS_NULL = edos_mask | 0x00
  CHANNEL_EDOS_SYS  = edos_mask | 0x01 # System message channel
  CHANNEL_EDOS_TOOL = edos_mask | 0x02 # Tooltip channel
  CHANNEL_EDOS_NOTE = edos_mask | 0x03 # Notice message channel
  CHANNEL_EDOS_FUN  = edos_mask | 0x10 # pointless nonsense channel

  #p map_bits(edos_mask, 8, 5).map { |a| a.to_i.chr }
  #p map_bits(CHANNEL_EDOS_FUN, 8, 5).map { |a| a.to_i.chr }

  def self.address
    CHANNEL_EDOS_SYS
  end

  def self.server
    @server
  end

  def self.init
    @server = Server.new("EDOS")
    @server.add_channel(CHANNEL_EDOS_NULL, "null")
    @server.add_channel(CHANNEL_EDOS_SYS,  "system")
    @server.add_channel(CHANNEL_EDOS_TOOL, "tooltip")
    @server.add_channel(CHANNEL_EDOS_NOTE, "notification")
    @server.add_channel(CHANNEL_EDOS_FUN,  "fun")
  end

  def self.on_startup
    #birthday_flag = :birthday
    #dat = [birthday_flag, -> { $game.actors[1] }, 20]
    #@server.request(address).set_data(dat).send_to(CHANNEL_EDOS_FUN).dispatch
  end

  class << self

    attr_accessor :daemon_server

  end

end