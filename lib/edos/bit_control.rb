#
# EDOS/lib/module/EDOS/bit_control.rb
#   by IceDragon
#   dc 27/04/2013
#   dm 27/04/2013
# vr 0.0.1
module EDOS
  module BitControl

    def self.map_bits(int, bits, length)
      length.times.map do |i|
        (int >> (i * bits)) & ((2 ** bits) - 1)
      end.reverse
    end

    def self.char_mask(chars)
      chars.bytes.inject(0) { |r, byte| (r | byte) << 8 } >> 8
    end

    def self.reverse_bits(src, bits, length)
      r = []
      bit_mask = (2 ** bits) - 1
      length.times do |i|
        r.push(src & bit_mask)
        src = src >> bits
      end
      return r.inject(0) { |r, i| (r | i) << bits } >> bits
    end

  end
end