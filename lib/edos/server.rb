#
# EDOS/lib/module/edos/server.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.1.0
module EDOS

  class AgentError < Exception
  end

  class Agent

    attr_reader :server, :destination, :from
    attr_accessor :data, :fail_count

    def initialize(server)
      @server = server
      @data          = nil
      @from          = nil
      @destination   = nil
      @send_callback = nil
      @fail_count    = 0
    end

    def request_from(from)
      raise(AgentError,
            "I've already been dispatched from #@from") if @from
      @from = from
      self
    end

    def set_data(new_data)
      @data = new_data
      self
    end

    def send_to(to, &callback)
      @destination = to
      @send_callback = callback
      self
    end

    def dispatch
      @server.recieve(self)
      nil
    end

    def on_success
      @send_callback.call if @send_callback
      self
    end

  end

  class Server

    class Channel
      ### mixin
      module Listener

        def receive_data(sender, data)
          ### overwrite in subclass
        end

      end

      ### instance_attributes
      attr_accessor :id
      attr_accessor :name
      attr_accessor :server
      attr_reader :listeners
      attr_reader :log

      ##
      # initialize(Object* id)
      def initialize(id)
        @id = id
        @name = "Unknown"
        @server = nil
        @listeners = []
        @blocked = false # not accepting anymore sends?
        @closed = false  # not accepting anymore listeners?
        @log = []
        @keep_log = true
      end

      def add_listener(listener)
        return false if @closed
        @listeners.push(listener) unless @listeners.include?(listener)
        return true
      end

      def remove_listener(listener)
        return false unless @listeners.include?(listener)
        @listeners.delete(listener)
        return true
      end

      def send_data(data)
        return false if @blocked
        @log.push(data)
        @log.shift until @log.size <= 20
        for listener in @listeners
          listener.receive_data(self, data)
        end
        return true
      end

      def ping
        send_data("PING #@id")
      end

    end

    class CallbackListener

      include Channel::Listener

      attr_accessor :func

      def initialize(func=nil, &block)
        @func = func || block
      end

      def receive_data(sender, data)
        @func.(sender, data) if @func
      end

    end

    ### instance_attributes
    attr_accessor :name
    attr_reader :channels

    def initialize(name="Unknown")
      @name = name
      @retries = 3
      @agents = []
      @blocked = {
        request: [],
        recieve: []
      }
      @channels = {}
    end

    def add_channel(channel_id, name="Unknown")
      if !@channels.has_key?(channel_id)
        channel = @channels[channel_id] = Channel.new(channel_id)
        channel.name = name
        channel.server = self
      end
    end

    def send_data(channel_id, data)
      if channel = @channels[channel_id]
        channel.send_data(data)
      end
    end

    def ping(channel_id=nil)
      if channel_id
        if channel = @channels[channel_id]
          channel.ping
        end
      else
        @channels.each(&:ping)
      end
    end

    def listen_to(listener, channel_id)
      if channel = @channels[channel_id]
        channel.add_listener(listener)
      end
    end

    def stop_listening_to(listener, channel_id)
      if channel = @channels[channel_id]
        channel.remove_listener(listener)
      end
    end

    def block_request(from)
      @blocked[:request].push(from) unless @blocked[:request].include?(from)
    end

    def block_recieve(from)
      @blocked[:recieve].push(from) unless @blocked[:recieve].include?(from)
    end

    def block_request?(from)
      @blocked[:request].include?(from)
    end

    def block_recieve?(from)
      @blocked[:recieve].include?(from)
    end

    def request_for(from)
      if block_request?(from)
        return nil
      else
        agent = Agent.new(self)
        agent.request_from(from)
        return agent
      end
    end

    def recieve(agent)
      if agent.server != self
        raise(AgentError,
              "this agent %s does not belong to this server" % agent)
      end
      unless block_recieve?(agent.destination)
        @agents << agent
      end
    end

    def update
      update_agents unless @agents.empty?
    end

    def update_agents
      return if @agents.empty?
      dead_agents = []
      @agents.each do |agent|
        to   = agent.destination
        dat  = agent.data
        from = agent.from
        if send_data(to, dat) #try_send_data(to, from, to, dat)
          agent.on_success
          dead_agents.push(agent)
        else
          agent.fail_count += 1
          dead_agents.push(agent) if agent.fail_count > @retries
        end
      end
      unless dead_agents.empty?
        for agent in dead_agents
          @agents.delete(agent)
        end
      end
    end

    alias :request :request_for

  end

end