#
# EDOS/lib/edos/type.rb
#   by IceDragon
# The EDOS::Type module serves no purpose but for class indentification.
# A Type module is included into the class of choice, EDOS then uses
# its own EDOS::Type to match internal objects.
module EDOS
  module Type

    ### Vector
    module Vector
    end
    module Vector2
      include Vector
    end
    module Vector3
      include Vector
    end

    ### Size
    module Size
    end
    module Size2
      include Size
    end
    module Size3
      include Size
    end

    ### Surface
    module Surface
    end
    module Surface2
      include Surface
    end
    module Surface3
      include Surface
    end

  end
end