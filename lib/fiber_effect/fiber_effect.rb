require_relative 'base/effect_base'
require_relative 'effect/fade'
require_relative 'effect/half_slice'
require_relative 'effect/unfold'