class Game::Character

  attr_reader :character_name
  attr_reader :character_index
  attr_reader :character_hue
  attr_reader :position
  attr_reader :frame_index
  attr_reader :frame_cols
  attr_reader :frame_size
  attr_reader :name
  attr_accessor :emo_id
  attr_accessor :aftershadow_timeout
  attr_accessor :aftershadow_frequency

  attr_accessor :foostep_flag

  attr_reader :hp
  attr_reader :mhp

  attr_reader :mp
  attr_reader :mmp

  def initialize
    @position        = Vector3.new(0, 0, 0)
    @target_position = Vector3.new(0, 0, 0)
    @force           = Vector3.new(0, 0, 0)
    @force_bleed     = Vector3.new(0.98, 0.98, 0.98) * 1.5
    @frame_index = 0
    @frame_cols  = 4
    @frame_size  = Size2.new(40, 40)
    @animators = {
      walking: Cache::Sequence.animation("walking_4frm").to_animator,
      idling:  Cache::Sequence.animation("idling_4frm").to_animator,
      jumping: Cache::Sequence.animation("idling_4frm").to_animator
    }
    @animators.each_value.each_with_object(45, &:frame_rate=)
    @direction = Direction.new(5)
    @animator = @animators[:idling]
    @emo_id = nil
    @aftershadow_timeout = nil
    @aftershadow_frequency = 5
    @hp = @mhp = 20
    @mp = @mmp = 10
    init_name
    init_character
  end

  def hp=(new_hp)
    @hp = [new_hp, 0].max
  end

  def mhp=(new_hp)
    @mhp = [new_hp, 1].max
  end

  def mp=(new_mp)
    @mp = [new_mp, 0].max
  end

  def mmp=(new_mp)
    @mmp = [new_mp, 0].max
  end

  def hp_rate
    @hp / @mhp.to_f
  end

  def mp_rate
    @mmp > 0 ? @mp / @mmp.to_f : 0.0
  end

  def init_name
    @name = Game::EntityName.new
  end

  def init_character
    @character_name  = ""
    @character_index = 0
    @character_hue   = 0
  end

  def change_character(character_name, character_index, character_hue=0)
    @character_name  = character_name
    @character_index = character_index
    @character_hue   = character_hue
  end

  def x
    @position.x
  end

  def y
    @position.y
  end

  def z
    @position.z
  end

  def screen_ox
    @frame_size.width / 2
  end

  def screen_oy
    @frame_size.height
  end

  def screen_x
    x + screen_ox
  end

  def screen_y
    y + screen_oy - z
  end

  def screen_z
    100
  end

  def screen_shadow_position
    Vector3.new(x + screen_ox, y + screen_oy + 4, 99)
  end

  def screen_shadow_scale
    s = (z + 32) / 32.0
    Vector2.new(s, s)
  end

  def screen_shadow_opacity
    (255 - (z / 64.0) * 255.0).max(0)
  end

  def x=(n)
    @position.x = n
  end

  def y=(n)
    @position.y = n
  end

  def z=(n)
    @position.z = n
  end

  def move_velocity
    4
  end

  def jump_strength
    15
  end

  def move!(x, y, z=0)
    @target_position.x += x
    @target_position.y += y
    @target_position.z += z
  end

  def moveto!(x=nil,y=nil,z=nil)
    @target_position.x = x if x
    @target_position.y = y if y
    @target_position.z = z if z
  end

  def jump!
    @force.z = jump_strength
  end

  def move(*args)
    return if moving?
    move!(*args)
  end

  def moveto(*args)
    return if moving?
    moveto!(*args)
  end

  def move_in_direction(dir)
    return if dir == 5 || dir == 0
    @direction.dir = dir
    vec = @direction.to_vector2 * move_velocity
    move(vec.x, vec.y, 0)
  end

  def jump
    return if jumping?
    jump!
  end

  def moving?
    return @position.x != @target_position.x ||
           @position.y != @target_position.y
  end

  def jumping?
    return @position.z != @target_position.z
  end

  def clear_pre_flags
    @foostep_flag = false
  end

  def clear_post_flags
  end

  def update
    clear_pre_flags
    if jumping?
      @animator = @animators[:jumping]
    elsif moving?
      @animator = @animators[:walking]
    else
      @animator = @animators[:idling]
    end
    @position.add!(@force)
    @force.lerp_step!(Vector3.zero, @force_bleed)
    move_value = 4
    @position.lerp_step!(@target_position, move_value)
    @animator.update
    @frame_index = @direction.to_row8_i * @frame_cols + @animator.index
    clear_post_flags
  end

  private :init_name
  private :init_character

end