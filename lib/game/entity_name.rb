class Game::EntityName

  attr_accessor :first
  attr_accessor :middle
  attr_accessor :last
  attr_accessor :nick
  attr_accessor :name_mode

  def initialize
    @first  = ''
    @middle = ''
    @last   = ''
    @nick   = ''
    @name_mode = :first
  end

  def to_s(mode=nil)
    case mode || @name_mode
    when :full             then "#{@first} #{@middle} #{@last}"
    when :middle_initial   then "#{@first} #{@middle[0]}. #{@last}"
    when :last_first       then "#{@last}, #{@first}"
    when :first            then @first
    when :last             then @last
    when :nick             then @nick
    else                        ''
    end
  end

end
