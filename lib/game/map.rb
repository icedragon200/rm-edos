class Game::Map

  attr_reader :mod_id
  attr_reader :mod_changes

  attr_reader :characters
  attr_reader :map_data
  attr_reader :flash_data
  attr_reader :shadow_data

  attr_accessor :world

  def initialize(world)
    @world = world
  end

  def setup(map)
    @map         = map
    @mod_changes = []
    @mod_id      = 0
    setup_map_data
    setup_shadow_data
    setup_flash_data
    setup_characters
  end

  def setup_map_data
    @map_data    = @map.data
  end

  def setup_shadow_data
    @shadow_data = @map.shadow_data
  end

  def setup_flash_data
    @flash_data  = Mythryl::Matrix.new(@map.width, @map.height)
  end

  def setup_characters
    @characters = []
  end

  private def modify
    @mod_changes << yield
    @mod_id += 1
  end

  def add_character(character)
    modify do
      @characters << character
      :character
    end
  end

  def update
    @characters.each(&:update)
  end

  def width
    @map.width
  end

  def height
    @map.height
  end

end