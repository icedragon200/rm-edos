class Game::World

  attr_reader :gravity
  attr_reader :ticks

  def initialize
    @gravity = 0.98
    @ticks = 0
  end

  def update
    @ticks += 1
  end

end