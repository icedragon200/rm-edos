#
# EDOS/lib/hazel2/shell.rb
#   by IceDragon
module Hazel
  class Shell

    VERSION = "3.0.0".freeze

  end
end
require_relative 'shell/error'
require_relative 'shell/shell'
require_relative 'shell/shell-vx'
require_relative 'shell/addons'
require_relative 'shell/decorations'
require_relative 'shell/base'