#
#
#
EDOS::Loader.config.add("IEI", 1) do
  require_relative 'IEI/core.rb'
  IEI::Core.log = EDOS.log
  require_relative 'IEI/magic_equip.rb'
  require_relative 'IEI/magic_learn.rb'
  require_relative 'IEI/arts.rb'
  require_relative 'IEI/costere.rb'
  require_relative 'IEI/elements.rb'
  require_relative 'IEI/exectine.rb'
  require_relative 'IEI/key_n_value.rb'
  require_relative 'IEI/note_eval.rb'
  require_relative 'IEI/scopius.rb'
  require_relative 'IEI/tracker.rb'

  require_relative 'IEI/gallery.rb'
  require_relative 'IEI/antilag.rb'
end