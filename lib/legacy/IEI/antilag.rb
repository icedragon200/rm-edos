# [IEI]Antilag
require_relative 'lib/antilag'

class REI::BattlerUnit

  def on_screen?
    self.screen_x.between?(-32, Graphics.width + 32) and
     self.screen_y.between?(-32, Graphics.height + 32)
  end

end
