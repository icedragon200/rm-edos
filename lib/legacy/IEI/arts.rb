# [IEI]Arts System
require_relative 'lib/arts-system'

module IEI
  module ArtsSystem

    def self.mk_objs
      @arts.clear

      # // How to create a new art? Look below
      art = new_art(0)
      art.icon.index = 0
      art.name = '----------'
      art.description = 'Does Absolutely Nothing'
      art.features.concat([Feature.new(0, 0, 0.0)])
      # // And finally you can start making your own arts here
      # // If your having difficulty with the Features try getting the Features4Dummies XD
      # // >_> Now if your smart you'll figure out a way to manage your 'arts'

      # Increases movement, defense, and prevents knock-back
      art = new_art(1)
      art.icon.index = 172
      art.name = "Footwork"
      art.description = ''
      art.features.concat([MkFeature.def_r(1.1)])

      # Decreases magic cost, and lowers cast time
      art = new_art(2)
      art.icon.index = 228
      art.name = 'Rune'
      art.description = ''
      art.features.concat([MkFeature.mp_cost_r(0.75)])

      # Increases weapon effectiveness
      art = new_art(3)
      art.icon.index = 121
      art.name = 'Stance'
      art.description = ''
      art.features.concat([MkFeature.mat_dam_r(1.1)])

      # Identify all items, enemies, objects, increases hit ratio
      art = new_art(4)
      art.icon.index = 359
      art.name = 'Optics'
      art.description = ''
      art.features.concat([MkFeature.hit_r(1.05)])

      # Higher natural Hp recovery rate, endurance ability (prevents 1ko's and may survive from a lethal attack)
      art = new_art(5)
      art.icon.index = 323
      art.name = 'Survival'
      art.description = ''
      art.features.concat([MkFeature.hp_regen_r(0.07)])

      # Increases speed, magic can be used even when silenced
      art = new_art(6)
      art.icon.index = 159
      art.name = 'Ninjutsu'
      art.description = ''
      art.features.concat([MkFeature.agi_r(1.1)])

      # (Re)Set and Disarm Traps, ablility to spot traps in a room
      art = new_art(7)
      art.icon.index = 267
      art.name = 'Mechanic'
      art.description = ''
      #art.features.concat([MkFeature.agi_r(1.1)])

      # Enables AOC(Area of Control) effect, prevents fear and confusion
      art = new_art(8)
      art.icon.index = 143
      art.name = 'Control'
      art.description = ''
      #art.features.concat([MkFeature.agi_r(1.1)])

      # Prevents paralasis, increases evasion rate
      art = new_art(9)
      art.icon.index = 286
      art.name = 'Nimble'
      art.description = ''
      art.features.concat([MkFeature.eva_r(1.1)])

      # //
      # Increases Attack, Lowers Defense
      art = new_art(10)
      art.icon.index = 398
      art.name = 'Risk'
      art.description = ''
      art.features.concat([MkFeature.atk_r(1.1), MkFeature.def_r(0.9)])

      # Increases Defense, Lowers Agility
      art = new_art(11)
      art.icon.index = 509
      art.name = 'Phalanx'
      art.description = ''
      art.features.concat([MkFeature.def_r(1.1), MkFeature.agi_r(0.9)])

      # Lowers Magic Cost Greatly, Lowers Magic Effectiveness
      art = new_art(12)
      art.icon.index = 365
      art.name = 'Dust Mage'
      art.description = ''
      art.features.concat([MkFeature.mp_cost_r(0.3), MkFeature.mat_dam_r(0.5)])

      # Prevents Counter Attacks, lowers attack
      art = new_art(13)
      art.icon.index = 488
      art.name = 'Loose Grip'
      art.description = ''
      art.features.concat([MkFeature.atk_r(0.9)])

      # Prevents Charm, sleep, confusion, lowers magic defense, and lowers Agility
      art = new_art(14)
      art.icon.index = 460
      art.name = 'Focused'
      art.description = ''
      art.features.concat([MkFeature.mdf_r(0.8), MkFeature.agi_r(0.8)])

      # Increases gold rate, decreases exp rate
      art = new_art(15)
      art.icon.index = 285
      art.name = 'Scavenge'
      art.description = ''
      art.features.concat([MkFeature.exp_gain_r(0.7)])

      # Increases exp rate, decreases gold rate
      art = new_art(16)
      art.icon.index = 230
      art.name = 'Scholar'
      art.description = ''
      art.features.concat([MkFeature.exp_gain_r(1.3)])

      # //
      # Increases Magic Learn Rate sharply, lowers all parameters (except Hp & Mp)
      art = new_art(17)
      art.icon.index = 232
      art.name = 'Pen & Paper'
      art.description = ''
      art.features.concat((2...6).map { |i| MkFeature.param_r(i, 0.9) })

      # Increases Exp Rate, Increases Magic Learn Rate, no gold is gained from defeated enemies
      art = new_art(18)
      art.icon.index = 469
      art.name = 'Discipline'
      art.description = ''
      art.features.concat([MkFeature.exp_gain_r(1.15)])

      $data_arts = @arts
    end

  end
end
require_relative 'arts/elements'