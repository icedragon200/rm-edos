# [IEI]BitmapEx
#require 'C:/Lib/IEI/edos_lib/BitmapEx'
=begin
sprites = Array.new(3) { Sprite.new }
sprites[0].bitmap = Cache.character("Baron_8D").dup
sprites[1].bitmap = sprites[0].bitmap.dup
palette = sprites[0].bitmap.palletize()
sprites[2].bitmap = Bitmap.new(palette.size,1)
sprites[2].zoom_x = sprites[2].zoom_y = 4.0
sprites[1].src_rect.set(sprites[0].src_rect.set(0,0,96,128))
sprites[0].salign!(anchor: 4)
sprites[1].salign!(anchor: 5)
sprites[2].salign!(anchor: 7)
palette.each_with_index{|c,i|sprites[2].bitmap.set_pixel(i,0,c)}
=end
=begin
sprites = Array.new(3) { Sprite.new }
sprites[0].bitmap = Bitmap.new(*Graphics.rect.xto_a(:width,:height))
rects = sprites[0].bitmap.rect.split_by(2,2)
sprites[0].bitmap.fill_rect(rects[0], Palette['sys_red1'])
sprites[0].bitmap.fill_rect(rects[1], Palette['sys_green1'])
sprites[0].bitmap.fill_rect(rects[2], Palette['sys_blue1'])
sprites[0].bitmap.fill_rect(rects[3], Palette['sys_orange1'])
timethen = Time.now
palette = sprites[0].bitmap.palletize()
puts "Palette Time:"
timenow = Time.now
puts timethen
puts timenow
puts timenow - timethen
# // Draw
sprites[2].bitmap = Bitmap.new(palette.size,1)
palette.each_with_index{|c,i|sprites[2].bitmap.set_pixel(i,0,c)}

timethen = Time.now
sprites[0].bitmap.rrecolor(
  [Palette['sys_red1'],Palette['sys_green1'],Palette['sys_blue1'],Palette['sys_orange1']],
  [Palette['white'],Palette['gray16'],Palette['brown1'],Palette['paper2']]
)
puts "Recolor Time:"
timenow = Time.now
puts timethen
puts timenow
puts timenow - timethen
#Main.init
Mouse.init
loop do
  Main.update
  sprites.each{|s|s.update}
end
=end
