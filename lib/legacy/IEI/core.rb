# [IEI]Core
require_relative 'lib/core'

module IEI::Core
  module Mixin
    module InitCore

      def pre_init_iei
        # // Nothing here but use Whitespace
      end

      def init_iei
        # // Nothing here but use Whitespace
      end

      def post_init_iei
        # // Nothing here but use Whitespace
      end

      def do_init_iei
        pre_init_iei  # // . x . Like, make arrays and stuff
        init_iei      # // @.@ Dirty Initializes and stuff
        post_init_iei # // =w= Cleaning up
      end

    end
  end
end

#-inject gen_class_header "Game::Unit"
class Game::Unit

  include IEI::Core::Mixin::InitCore

  alias iei_gmp_initialize initialize
  def initialize
    pre_init_iei
    iei_gmp_initialize
    init_iei
    post_init_iei
  end

end
