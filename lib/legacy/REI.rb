# REI
module REI

end
use_v2 = false

if use_v2
  # REI2
  require_relative 'REI2/rei'
  require_relative 'REI2/mixin'
  require_relative 'REI2/component'
  require_relative 'REI2/entity'
  require_relative 'REI2/entity_system'
  require_relative 'REI2/unit'
  require_relative 'REI2/ai'
  require_relative 'REI2/squad'
  require_relative 'REI2/team'
  require_relative 'REI2/system'
else
  # REI
  dir = File.dirname(__FILE__)
  Dir.glob(File.join(dir, "REI", "*.rb")).sort.each do |fn|
    require fn
  end
end