dir = File.dirname(__FILE__)
require File.join(dir, 'mixins')
require File.join(dir, 'drawext')
require File.join(dir, 'drawshare')
Dir.glob(File.join(dir, 'artist', '*.rb')).each do |fn|
  require fn
end
