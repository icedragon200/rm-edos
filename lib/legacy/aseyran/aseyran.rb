#
# EDOS/src/aseyran/aseyran.rb
#   by IceDragon
module Aseyran
  class Card

    @@cards = []

    attr_accessor :id, :meta,
                  :cost, :name, :element,
                  :mana, :effects

    def new_with_id
      card = new
      @@cards.push(card)
      card.id = @@cards.size
      return card
    end

  end

  class CreatureCard < Card

    attr_accessor :hp, :mp, :def, :atk, :equip_slots

  end

  class EquipCard < Card

    attr_accessor :added_effects, :removed_effects

  end

  class MagicCard < Card

    attr_accessor :mp_cost

  end
end