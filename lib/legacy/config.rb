#
# EDOS/core/config.rb
#   dm 30/03/2013
require_relative 'resolution'

module SRRI

  @@config.replace(
    title: "EDOS - Core",
    cursor: false,
    fullscreen: false,
    vsync: false
    #vsync: true
  )

end

class Game
  VERSION = "0.1.3-alpha".freeze
end
