#
# EDOS/lib/core/input.rb
#   by IceDragon
module Input

  def self.sr_repeat_rate
    @sr_repeat_rate ||= Metric::Time.sec_to_frame(0.1666666666)
  end

  ##
  # ::trigger_any?(keys)
  def self.trigger_any?(*keys)
    keys.any? { |k| trigger?(k) }
  end

  ##
  # ::trigger_all?(keys)
  def self.trigger_all?(*keys)
    keys.all? { |k| trigger?(k) }
  end

  ##
  # ::repeat_any?(keys)
  def self.repeat_any?(*keys)
    keys.any? { |k| repeat?(k) }
  end

  ##
  # ::repeat_all?(keys)
  def self.repeat_all?(*keys)
    keys.all? { |k| repeat?(k) }
  end

  ##
  # ::press_any?(keys)
  def self.press_any?(*keys)
    keys.any? { |k| press?(k) }
  end

  ##
  # ::press_all?(keys)
  def self.press_all?(*keys)
    keys.all? { |k| press?(k) }
  end

  ##
  # ::mtrigger_any?(keys)
  def self.mtrigger_any?(*keys)
    keys.any? { |k| mtrigger?(k) }
  end

  ##
  # ::mtrigger_all?(keys)
  def self.mtrigger_all?(*keys)
    keys.all? { |k| mtrigger?(k) }
  end

  ##
  # ::mrepeat_any?(keys)
  def self.mrepeat_any?(*keys)
    keys.any? { |k| mrepeat?(k) }
  end

  ##
  # ::mrepeat_all?(keys)
  def self.mrepeat_all?(*keys)
    keys.all? { |k| mrepeat?(k) }
  end

  ##
  # ::mpress_any?(keys)
  def self.mpress_any?(*keys)
    keys.any? { |k| mpress?(k) }
  end

  ##
  # ::mpress_all?(keys)
  def self.mpress_all?(*keys)
    keys.all? { |k| mpress?(k) }
  end

end