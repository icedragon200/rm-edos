#
# EDOS/src/database.rb
#   by IceDragon
#   dc 19/05/2013
#   dm 26/05/2013
class Database

  VERSION = "1.0.0".freeze

  def initialize
    @data = {}
  end

  def self.construct(*args, &block)
    Constructor.add_callback(*args, &block)
  end

  def self.run_with_constructor(sym)
    constructor = Constructor.new
    Constructor.try_callback(sym, constructor)
    return constructor
  end

  module Lookup

    def self.by_tags(objs, *tags)
      objs.compact.select { |obj| (tags & obj.tags) == tags }
    end

  end

end

[
  'database/helper',
  'database/constructor',
  'database/db_header',
  'database/db_header_ref',

  'database/actors',
  'database/classes',
  'database/skills',
  'database/items',
  'database/weapons',
  'database/armors',
  'database/enemies',
  'database/troops',
  'database/states',
  'database/animations',
  'database/tilesets',
  'database/commonevents',
  'database/system',
  'database/mapinfos',

  'database/startup',
  'database/temp',
  'database/traps',
  'database/crafts',
  'database/spirits',

  'database/db_tail'
].each do |s|
  require_relative s
end
