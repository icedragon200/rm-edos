#==============================================================================#
# ■ Database.actors
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0
#         Added:
#           Entity 0 (Dummy)
#           Entity 1 (Baron)
#           Entity 2 (Elli)
#           Entity 3 (Chika)
#           Entity 4 (Spyet)
#           Entity 5 (Icaria)
#           Entity 6 (Nekome)
#           Entity 7 (Len)
#           Entity 8 (Cyrus)
#
#==============================================================================#
class Database
  #Entity = RPG::Entity
  def self.armor_features(n)
    fet = []
    fet << MkFeature.armor_type( Helper.armor_type_id(:shield) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:helmet) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:body) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:accessory) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:arm) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:leg) )
    fet << MkFeature.armor_type( Helper.armor_type_id(:crest) )
    case(n)
    when :male
      fet << MkFeature.armor_type( Helper.armor_type_id(:helmetM) )
      fet << MkFeature.armor_type( Helper.armor_type_id(:bodyM) )
      fet << MkFeature.armor_type( Helper.armor_type_id(:accessoryM) )
    when :female
      fet << MkFeature.armor_type( Helper.armor_type_id(:helmetF) )
      fet << MkFeature.armor_type( Helper.armor_type_id(:bodyF) )
      fet << MkFeature.armor_type( Helper.armor_type_id(:accessoryF) )
    end
    fet
  end
def self.build_actors()
  @actors = []
#==============================================================================#
# ◙ Entity 0 (Dummy)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 0
  act.element_id      = 0
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 1 (Baron)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 1
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Baron"
  act.nickname        = act.name
  act.class_id        = 1
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Baron_8D"
  act.character_index = 0
  act.face_name       = "Baron"
  act.face_index      = 0
  act.portrait_name   = "Baron"
  act.equips          = [  Helper.weapon_id(:spears, 1),  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:light)
  act.features += armor_features(:male)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 2 (Elli)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 2
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Ellisia"
  act.nickname        = act.name
  act.class_id        = 2
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Elli"
  act.character_index = 0
  act.face_name       = "Elli"
  act.face_index      = 0
  act.portrait_name   = "Elli"
  act.equips          = [  Helper.weapon_id(:brushes, 1),  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:dark)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 3 (Chika)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 3
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Chika"
  act.nickname        = act.name
  act.class_id        = 3
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Chika"
  act.character_index = 0
  act.face_name       = "Chika"
  act.face_index      = 0
  act.portrait_name   = "Chika"
  act.equips          = [  Helper.weapon_id(:swords, 1),  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:earth)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 4 (Spyet)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 4
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Spyet"
  act.nickname        = act.name
  act.class_id        = 4
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Spyet_8d"
  act.character_index = 0
  act.face_name       = "Spyet"
  act.face_index      = 0
  act.portrait_name   = "Spyet"
  act.equips          = [  Helper.weapon_id(:bows, 1),  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:wind)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 5 (Icaria)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 5
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Icaria"
  act.nickname        = act.name
  act.class_id        = 5
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Icaria"
  act.character_index = 0
  act.face_name       = "Icaria"
  act.face_index      = 0
  act.portrait_name   = "Icaria"
  act.equips          = [  Helper.weapon_id(:spears, 1),  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:fire)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 6 (Nekome)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 6
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Nekome"
  act.nickname        = act.name
  act.class_id        = 6
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Nekome"
  act.character_index = 0
  act.face_name       = "Nekome"
  act.face_index      = 0
  act.portrait_name   = "Nekome"
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:water)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 7 (Len)
#==============================================================================#
  act                 = Entity.new
  act.initialize_add()
  act.id              = 7
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Leonora"
  act.nickname        = act.name
  act.class_id        = 7
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Len"
  act.character_index = 0
  act.face_name       = "Len"
  act.face_index      = 0
  act.portrait_name   = "Len"
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:light)
  act.features += armor_features(:female)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 8 (Cyrus)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 8
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Cyrus"
  act.nickname        = act.name
  act.class_id        = 8
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Cyrus"
  act.character_index = 0
  act.face_name       = "Cyrus"
  act.face_index      = 0
  act.portrait_name   = "Cyrus"
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:wind)
  act.features += armor_features(:male)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 9 (Vesper)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 9
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Vesper"
  act.nickname        = act.name
  act.class_id        = 9
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Vesper"
  act.character_index = 0
  act.face_name       = "Vesper"
  act.face_index      = 0
  act.portrait_name   = "Vesper"
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:fire)
  act.features += armor_features(:male)
  @actors[act.id]     = act
#==============================================================================#
# ◙ Entity 10 (Cogan)
#==============================================================================#
  act                 = Entity.new()
  act.initialize_add()
  act.id              = 10
  act.icon.index      = 0
  act.description     = ''
  act.note            = ''
  act.name            = "Cogan"
  act.nickname        = act.name
  act.class_id        = 9
  act.initial_level   = 1
  act.max_level       = LEVELCAP
  act.character_name  = "Cogan"
  act.character_index = 0
  act.face_name       = "Cogan"
  act.face_index      = 0
  act.portrait_name   = "Cogan"
  act.equips          = [  0,  0,  0,  0,  0]
  act.features        = []
  act.element_id      = element_id(:earth)
  act.features += armor_features(:male)
  @actors[act.id]     = act
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@actors.size
    @actors[i].id = i
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
