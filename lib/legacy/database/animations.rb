#==============================================================================#
# ■ Database.animations
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#
#==============================================================================#
class Database
  #Animation = RPG::Animation
  AnimationScale = 0.55 #0.6
def self.build_animations()
  @animations = []#load_data("data/animations.rvdata2")
  #(0: pattern, 1: X-coordinate, 2: Y-coordinate, 3: zoom level,
  # 4: angle of rotation, 5: horizontal flip, 6: opacity, 7: blending mode
  @animations.each do |a| next unless a
    a.initialize_add()
    a.frames.each do |f|
      for x in 0...f.cell_data.xsize
        f.cell_data[x, 1] *= AnimationScale
        f.cell_data[x, 2] *= AnimationScale
        f.cell_data[x, 3] *= AnimationScale
      end
    end
  end

  @animations.each do |a|
    next unless a

    a.animation1_name.downcase!
    a.animation1_name.gsub!(" ", "")
    a.animation2_name.downcase!
    a.animation2_name.gsub!(" ", "")
  end

end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
