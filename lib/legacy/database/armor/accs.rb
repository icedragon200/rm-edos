# // 02/29/2012
# // 02/29/2012
class Database
def self.mk_armors8()
  armors = []
  armor_sym = :accessory
  equip_sym = :accessory
#==============================================================================#
# ◙ Armor (Accessory)(Rope)
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 5
  armor.name         = "Rope"
  armor.icon.index   = 0
  armor.description  = 'It holds your clothes together'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,1,0,0,0,1]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.02)
  armor.features << MkFeature.luk_r(1.02)
  armors[armor.id] = armor
#==============================================================================#
# ◙ Armor (Accessory)(Belt)
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 6
  armor.name         = "Belt"
  armor.icon.index   = 0
  armor.description  = 'Better than rope, and looks stylish'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,2,0,0,0,1]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.03)
  armor.features << MkFeature.luk_r(1.03)
  armors[armor.id] = armor
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  Database::Helper.adjust_armors(armors,armor_sym,equip_sym)
end
end
