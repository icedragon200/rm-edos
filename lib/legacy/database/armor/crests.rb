# // 02/29/2012
# // 02/29/2012
class Database
def self.mk_armors20()
  armors = []
  armor_sym = :crest
  equip_sym = :crest
#==============================================================================#
# ◙ Armor (Crest)()
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 1
  armor.name         = "Sharpened"
  armor.icon.index   = 0
  armor.description  = 'Raises Attack'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,3,0,0,0,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.atk_r(1.02)
  armors[armor.id] = armor
#==============================================================================#
# ◙ Armor (Crest)()
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 2
  armor.name         = "Plated"
  armor.icon.index   = 0
  armor.description  = 'Raises Defense'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,3,0,0,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.02)
  armors[armor.id] = armor
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  Database::Helper.adjust_armors(armors,armor_sym,equip_sym)
end
end
