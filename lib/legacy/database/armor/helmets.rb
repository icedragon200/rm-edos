# // 02/29/2012
# // 02/29/2012
class Database
def self.mk_armors2()
  armors = []
  armor_sym = :helmet
  equip_sym = :helmet
#==============================================================================#
# ◙ Armor (Helmet)(Bronze)
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 5
  armor.name         = "Bronze"
  armor.icon.index   = 0
  armor.description  = 'A very inexpensive helmet, easy to make, easy to break'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,1,0,1,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.03)
  armor.features << MkFeature.mdf_r(1.02)
  armors[armor.id] = armor
#==============================================================================#
# ◙ Armor (Helmet)(Iron)
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 6
  armor.name         = "Iron"
  armor.icon.index   = 0
  armor.description  = 'Its a standard, even guards wear it!'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,2,0,2,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.04)
  armor.features << MkFeature.mdf_r(1.03)
  armors[armor.id] = armor
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  Database::Helper.adjust_armors(armors,armor_sym,equip_sym)
end
end
