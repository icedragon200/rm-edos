# // 02/29/2012
# // 02/29/2012
class Database
def self.mk_armors1()
  armors = []
  armor_sym = :shield
  equip_sym = :shield
#==============================================================================#
# ◙ Armor (Shield)(Wooden)
#==============================================================================#
  armor = Armor.new()
  armor.initialize_add()
  armor.id           = 5
  armor.name         = "Wooden"
  armor.icon.index   = 502
  armor.description  = 'Made from 80% wood, and 20% scraps you could find'
  armor.features     = []
  armor.note         = ""
  armor.etype_id     = 0
  armor.params       = [0,0,0,2,0,0,0,0]
  armor.atype_id     = 0
  armor.features << MkFeature.def_r(1.05)
  armors[armor.id] = armor
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  Database::Helper.adjust_armors(armors,armor_sym,equip_sym)
end
end
