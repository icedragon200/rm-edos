#==============================================================================#
# ■ Database.classes
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           Class 0 (Dummy)
#           Class 1 (Lance)
#           Class 2 (Artist)
#           Class 3 (Swordsmaster)
#           Class 4 (Archer)
#           Class 5 (Dancer)
#           Class 6 (Ninja)
#           Class 7 (Pandera)
#           Class 8 (Gunner)
#
#==============================================================================#
class Database
  # // To be added
  # Envoy, Templar - Lancer X Classes
  #
  #Class = RPG::Class
def self.build_classes()
  @_class_param_blocks = []
  @classes = []
#==============================================================================#
# ◙ Class 0 (Dummy)
#==============================================================================#
  @classes[0] = Class.new()
#==============================================================================#
# ◙ Class 1 (Lancer)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 1
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(spears: 0, swords: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      18 + level * 7 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 4 / 3.8
    when 3 # // Defense
      5 + level * 4 / 3.8
    when 4 # // Magic Attack
      5 + level * 3 / 2.4
    when 5 # // Magic Defense
      5 + level * 3 / 2.4
    when 6 # // Agility
      5 + level * 3 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 2 (Artist)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 2
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(brushes: 0, staves: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      14 + level * 6 / 3.6
    when 1 # // Max Mp
      5 + level * 5 / 2.8
    when 2 # // Attack
      5 + level * 2 / 3.8
    when 3 # // Defense
      5 + level * 3 / 3.8
    when 4 # // Magic Attack
      5 + level * 5 / 2.4
    when 5 # // Magic Defense
      5 + level * 4 / 2.4
    when 6 # // Agility
      5 + level * 3 / 2.0
    when 7 # // Luck
      5 + level * 5 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 3 (Swordsmaster)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 3
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(swords: 0, spears: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      24 + level * 12 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 4 / 3.8
    when 3 # // Defense
      5 + level * 2 / 3.8
    when 4 # // Magic Attack
      5 + level * 3 / 2.4
    when 5 # // Magic Defense
      5 + level * 4 / 2.4
    when 6 # // Agility
      5 + level * 4 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 4 (Archer)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 4
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(bows: 0, knives: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      18 + level * 10 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 3 / 3.8
    when 3 # // Defense
      5 + level * 3 / 3.8
    when 4 # // Magic Attack
      5 + level * 4 / 2.4
    when 5 # // Magic Defense
      5 + level * 3 / 2.4
    when 6 # // Agility
      5 + level * 5 / 2.2
    when 7 # // Luck
      5 + level * 5 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 5 (Arcaness)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 5
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(staves: 0, brushes: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      14 + level * 6 / 3.6
    when 1 # // Max Mp
      5 + level * 6 / 2.8
    when 2 # // Attack
      5 + level * 2 / 3.8
    when 3 # // Defense
      5 + level * 2 / 3.8
    when 4 # // Magic Attack
      5 + level * 5 / 2.4
    when 5 # // Magic Defense
      5 + level * 5 / 2.4
    when 6 # // Agility
      5 + level * 3 / 2.0
    when 7 # // Luck
      5 + level * 5 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 6 (Ninja)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 6
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(knives: 0, bows: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      14 + level * 6 / 3.6
    when 1 # // Max Mp
      5 + level * 5 / 2.8
    when 2 # // Attack
      5 + level * 2 / 3.8
    when 3 # // Defense
      5 + level * 2 / 3.8
    when 4 # // Magic Attack
      5 + level * 4 / 2.4
    when 5 # // Magic Defense
      5 + level * 4 / 2.4
    when 6 # // Agility
      5 + level * 5 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 7 (Pandera)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 7
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(hammers: 0, guns: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      20 + level * 11 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 4 / 3.8
    when 3 # // Defense
      5 + level * 5 / 3.8
    when 4 # // Magic Attack
      5 + level * 2 / 2.4
    when 5 # // Magic Defense
      5 + level * 2 / 2.4
    when 6 # // Agility
      5 + level * 3 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 8 (Technic)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 8
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(guns: 0, hammers: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      20 + level * 10 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 5 / 3.8
    when 3 # // Defense
      5 + level * 4 / 3.8
    when 4 # // Magic Attack
      5 + level * 2 / 2.4
    when 5 # // Magic Defense
      5 + level * 2 / 2.4
    when 6 # // Agility
      5 + level * 4 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Class 9 (Reaper)
#==============================================================================#
  cls              = Class.new()
  cls.initialize_add()
  cls.id           = 9
  cls.name         = @class_name[cls.id]
  cls.icon.index   = 0
  cls.description  = ''
  cls.features     = []
  cls.wtype_pref   = Database::Helper.mk_wtype_pref(scythes: 0, swords: 10)
  cls.note         = ""
  cls.exp_params   = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      20 + level * 10 / 3.6
    when 1 # // Max Mp
      5 + level * 3 / 2.8
    when 2 # // Attack
      5 + level * 5 / 3.8
    when 3 # // Defense
      5 + level * 4 / 3.8
    when 4 # // Magic Attack
      5 + level * 2 / 2.4
    when 5 # // Magic Defense
      5 + level * 2 / 2.4
    when 6 # // Agility
      5 + level * 4 / 2.0
    when 7 # // Luck
      5 + level * 4 / 2.4
    else
      0
    end)
  end
  cls.features << MkFeature.target_r( 1.0 )
  cls.features << MkFeature.hit_r( 0.95 )
  cls.features << MkFeature.evasion_r( 0.05 )
  cls.features << MkFeature.critical_r( 0.04 )
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Enemy Classes
#/============================================================================\#
# ● Enemies are basically the same as actors, they have experience, can gain
#   levels, have classes and can use equipment
#\============================================================================/#
  cls = Class.new()
  cls.id = 51
  cls.name = "Amobe"
  cls.exp_params = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      10 + level * 5 / 2.8
    when 1 # // Max Mp
      5 + level * 2 / 2.8
    when 2 # // Attack
      5 + level * 5 / 2.4
    when 3 # // Defense
      5 + level * 5 / 2.4
    when 4 # // Magic Attack
      5 + level * 5 / 2.4
    when 5 # // Magic Defense
      5 + level * 5 / 2.4
    when 6 # // Agility
      5 + level * 5 / 2.4
    when 7 # // Luck
      5 + level * 5 / 2.4
    else
      0
    end)
  end
  @classes[cls.id] = cls
#==============================================================================#
# ◙ Trap Classes
#/============================================================================\#
# ● Traps are still the same as actors and enemies
#\============================================================================/#
  cls = Class.new()
  cls.id = 101
  cls.name = "Trap"
  cls.exp_params = Exp_Parameters.call()
  @_class_param_blocks[cls.id] = ->(parameter, level) do
    Integer(case parameter
    when 0 # // Max Hp
      999 #+ level
    when 1 # // Max Mp
      999 #+ level
    when 2 # // Attack
      5 + level * 3 / 2.4
    when 3 # // Defense
      5 + level * 3 / 2.4
    when 4 # // Magic Attack
      5 + level * 3 / 2.4
    when 5 # // Magic Defense
      5 + level * 3 / 2.4
    when 6 # // Agility
      5 + level * 3 / 2.0
    when 7 # // Luck
      5 + level * 3 / 2.4
    else
      0
    end)
  end
  @classes[cls.id] = cls
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  all_weapon_types = -> do
    Database::Helper.all_wtype_ids.map do |i|
      MkFeature.equip_wtype(i)
    end
  end
  for i in 0...@classes.size
    if cls=@classes[i]
      cls.id = i
      cls.rebuild_parameter_table(
        LEVELCAP+1, &@_class_param_blocks[i] )
      cls.features.concat(all_weapon_types.())
      cls.features << MkFeature.hp_regen_r(0.07) # // Regen 7% O:
      cls.features += (1..7).to_a.collect { |i| MkFeature.stype_add(i) }

      #cls.learnings << RPG::Class::Learning.new().set( 1, 15, '' )
      #cls.learnings << RPG::Class::Learning.new().set( 1, 16, '' )
      #cls.learnings << RPG::Class::Learning.new().set( 1, 20, '' )
      #cls.learnings << RPG::Class::Learning.new().set( 1, 21, '' )
      #cls.learnings << RPG::Class::Learning.new().set( 1, 22, '' )
      #cls.learnings << RPG::Class::Learning.new().set( 1, 23, '' )
    end
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
