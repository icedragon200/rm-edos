#
# EDOS/src/db/constructor.rb
#   by IceDragon
#   dc 28/05/2013
#   dm 28/05/2013
class Database
  class Constructor

    include RPG
    extend MACL::Mixin::Callback

    def initialize
      @stack = [[]]
    end

    def stack!
      @stack.push([])
      self
    end

    def collapse!
      @stack = [@stack.flatten]
    end

    def collapse
      @stack.flatten
    end

    def result
      @stack[-1]
    end

    def helper
      Database::Helper
    end

    def new_for_stack(klass, *args)
      obj = klass.new(*args)
      yield obj if block_given?
      result.push(obj)
      return obj
    end

    def new_entity(*args, &block)
      new_for_stack(Entity, *args, &block)
    end

    def new_weapon(*args, &block)
      new_for_stack(Weapon, *args, &block)
    end

    def new_armor(*args, &block)
      new_for_stack(Armor, *args, &block)
    end

    def new_skill(*args, &block)
      new_for_stack(Skill, *args, &block)
    end

    def new_item(*args, &block)
      new_for_stack(Item, *args, &block)
    end

  end
end
