#==============================================================================#
# ■ Database.crafts
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/03/2012
# // • Data Modified : 01/03/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/03/2012 V1.0
#==============================================================================#
class Database
  #Craft = RPG::Craft
  CrfNil_ID = 0
  CrfItm_ID = 1
  CrfWep_ID = 2
  CrfArm_ID = 3
  def self.mk_cnt()
    [CrfNil_ID, 0]
  end
  def self.mk_cit(id)
    [CrfItm_ID, id]
  end
  def self.mk_cwt(id)
    [CrfWep_ID, id]
  end
  def self.mk_cat(id)
    [CrfArm_ID, id]
  end
  def self.item_to_material(item)
    return mk_cnt if item.nil? || item.id.zero?
    case item
    when RPG::Weapon, ExDatabase::Weapon
      mk_cwt(item.id)
    when RPG::Armor, ExDatabase::Armor
      mk_cat(item.id)
    when RPG::Item
      mk_cit(item.id)
    else
      mk_cnt
    end
  end
  def self.material_to_item(i)
    case i[0]
    when CrfWep_ID
      $data_weapons[i[1]]
    when CrfArm_ID
      $data_armors[i[1]]
    when CrfItm_ID
      $data_items[i[1]]
    else
      nil
    end
  end
  def self.mk_recipe(*arras)
    arras.pad(6){mk_cnt()}.sort
  end
  def self.load_recipe_list()
    @craft_recipes = {}
    @crafts.each { |c| @craft_recipes[c.recipe_items] = c if c }
  end
  def self.craft_recipes
    @craft_recipes
  end
def self.build_crafts
  @crafts = []
#==============================================================================#
# ◙ Craft 0 (Dummy)
#==============================================================================#
  craft              = Craft.new()
  craft.id           = 0
  craft.name         = "No Item"
  craft.icon.index   = 239
  craft.recipe_items = mk_recipe(*([mk_cnt]*6))
  craft.result_items = []
  craft.requirement  = 0
  @crafts[craft.id]  = craft
#==============================================================================#
# ◙ Craft 1 (Elixir I)
#==============================================================================#
  craft              = Craft.new()
  craft.id           = 1
  craft.name         = "Elixir I"
  craft.icon.index   = 215
  craft.recipe_items = mk_recipe(mk_cit(1), mk_cit(2))
  craft.result_items = [mk_cit(7)]
  craft.requirement  = 0
  @crafts[craft.id]  = craft
#==============================================================================#
# ◙ Craft 2 (Elixir II)
#==============================================================================#
  craft              = Craft.new()
  craft.id           = 2
  craft.name         = "Elixir II"
  craft.icon.index   = 216
  craft.recipe_items = mk_recipe(*([mk_cit(1)]*2)+([mk_cit(2)]*2))
  craft.result_items = [mk_cit(8)]
  craft.requirement  = 0
  @crafts[craft.id]  = craft
#==============================================================================#
# ◙ Craft 3 (Elixir III)
#==============================================================================#
  craft              = Craft.new()
  craft.id           = 3
  craft.name         = "Elixir III"
  craft.icon.index   = 218
  craft.recipe_items = mk_recipe(*([mk_cit(1)]*3)+([mk_cit(2)]*3))
  craft.result_items = [mk_cit(9)]
  craft.requirement  = 0
  @crafts[craft.id]  = craft
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@crafts.size
    next unless @crafts[i]
    @crafts[i].id = i
  end
end
end
#$hashh = {}
#av = [:weapon, :item, :armor]
#160.times { |i|
#  ar = [[av.sample, rand(50)], [av.sample, rand(50)], [av.sample, rand(50)], [av.sample, rand(50)], [av.sample, rand(50)], [av.sample, rand(50)]]
#  ar.each { |a|
#    $hashh[a] = [av.sample, rand(50)]
#  }
#}
#puts $hashh.size
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
