# // 02/02/2012
# // 02/02/2012
class Database

  include RPG
  MAX_ITEM_NUMBER  = 255

  WEAPON_LEVELCAP = 256
  ARMOR_LEVELCAP  = 256
  LEVELCAP        = 256
  TRAP_LEVELCAP   = 256

  BUILD_SET_SYMBOLS = [:actors, :classes, :skills, :items, :weapons, :armors,
                       :enemies, :troops, :states, :animations, :tilesets,
                       :common_events, :system, :mapinfos,
                       :startup, :temp, :traps, :crafts, :spirit_classes]

  ahash = BUILD_SET_SYMBOLS.map{ |s|
    [s, s.to_s.downcase + ".rvdata2"]
  }
  BUILD_FILENAMES = Hash[ahash]

  # // .__. House keeping...
  @current_db_verions = {
    # // Core
    stringTable:    "0.001a",
    # // Default
    actors:         "0.001c",
    classes:        "0.001a",
    skills:         "0.002a",
    items:          "0.001a",
    weapons:        "0.001a",
    armors:         "0.001a",
    enemies:        "0.001b",
    troops:         "0.001a",
    states:         "0.001a",
    animations:     "0.002a",
    tilesets:       "0.001a",
    common_events:   "0.001a",
    system:         "0.001a",
    # // EX
    startup:        "0.001a",
    temp:           "0.001a",
    traps:          "0.001a",
    crafts:         "0.001a",

    spirit_classes: "0.001a"
  }
  # // >_> Just remake the entire database every startup
  @current_db_verions.keys.each { |k| @current_db_verions[k] = nil }
  @db_header = {}
  @build_set = {}
  include Mixin::RouteCommands

  Exp_Parameters = proc { |bs, cr, infa, infb| [bs || 10, cr || 7, infa || 10, infb || 10] }

  @class_name = {}
  # // Envoy      , Templar   # // X-Classes
  @class_name[1] = "Fighter"  # // (8< YEAH DO SUMTHING @_@
  # // Archwitch , >,>
  @class_name[2] = "Witch"    # // 8) Ahhhhh 0:
  # // Swordstress, >,>
  @class_name[3] = "Squiress" # // :) Sounds cool? D8<
  @class_name[4] = "Archer"   # // O: Simple
  # // Siren, >,>
  @class_name[5] = "Arcaness" # // . x . It likes Arcane + ess (well it is XD)
  # // Shinobi, >,>
  @class_name[6] = "Ninja"    # // @_@ Couldn't find a replacement...
  # // @__@ No idea
  @class_name[7] = "Pandera"  # // o3o Original
  # // @__@ Once again
  @class_name[8] = "Technic"  # // o3o ORIGINAL
  # // @__@ Once again
  @class_name[9] = "Reaper"   # // Change >_>....

  @iconset_name = {
    materials: "iconset", #"Iconset_Ingridients"
    elements:  "iconset_elements", #
    costs:     "iconset_elements", #
  }

  @element_icon_index = {
    fire:    1, #
    water:   2, #
    earth:   3, #
    wind:    4, #
    light:   5, #
    dark:    6, #
    hp:      7, #
    mp:      8, #
    wt:      9, #
  }

  @element_id = @element_icon_index.dup

  @element_sym= @element_id.invert

  @element_icon_index2 = (0..9).to_a # // Array Map . x .
  @element_icon_type_offset = {
    symbol:  0, #
    skill:   16, #
    cost:    32, #
    art:     48, #
  }

  @cost_icons = {
    hp:  @element_icon_type_offset[:cost] + 7, #
    mp:  @element_icon_type_offset[:cost] + 8, #
    wt:  @element_icon_type_offset[:cost] + 9, #
  }

  @param_id = {
    mhp:  0, #
    mmp:  1, #
    atk:  2, #
    def:  3, #
    mat:  4, #
    mdf:  5, #
    agi:  6, #
    luk:  7  #
  }

  def self.param_id(sym)
    @param_id[sym]
  end

  def self.cost_icon(type)
    return @cost_icons[type]
  end
  def self.element_id(sym)
    @element_id[sym]
  end
  def self.element_icon_by_id(n)
    @element_icon_index2[n]
  end

  def self.set_element_icon(n, obj=nil, type=:symbol)
    return @iconset_name[:elements], @element_icon_index[n] if(obj.nil?())
    obj.icon.iconset_name = @iconset_name[:elements]
    obj.icon.index = @element_icon_index[n] + @element_icon_type_offset[type]
    obj
  end

  def self.element2stype(id)
    Helper.stype_id(@element_sym[id])
  end
  #def self.skill_id(sym)
  #  @skill_id[sym]
  #end
  def self.iconset_name(name)
    @iconset_name[name]
  end

  @rogue = {
    attack:  1, #
    guard:   2, #
    nudge:   3, #
    skip:    4, #
    tame:    5, #
    skill:   10, #
    item:    11, #
    status:  12, #
    hotkeys: 13, #
    equip:   14, #
    list:    20, #
    minimap: 21, #
    options: 22, #
    save:    23, #
  }
  def self.rogue_sym2id(sym);@rogue[sym];end

  @rogue_icon = {
    @rogue[:attack]  => 134,
    @rogue[:guard]   => 139,
    @rogue[:skip]    => 448,
    @rogue[:nudge]   => 11,
    @rogue[:tame]    => 122,
    @rogue[:item]    => 261,
    @rogue[:skill]   => 136,
    @rogue[:status]  => 233,
    @rogue[:equip]   => 170,
    @rogue[:options] => 337,
    @rogue[:hotkeys] => 143,
    @rogue[:save]    => 372,
    @rogue[:minimap] => 231,
    @rogue[:list]    => 234,
  }
  def self.rogue_icon(id);@rogue_icon[id];end

=begin
  # OLD                        //[mhp,mmp,atk,def,spi,agi,dex,res]
    base_swords:   [  0,  0,  5,  2,  2,  2,  2,  2], #
    base_spears:   [  0,  0,  8,  1,  1,  1,  3,  1], #
    base_scythes:  [  0,  0, 10,  0,  3,  0,  0,  2], #
    base_brushes:  [  0,  0,  3,  0,  5,  1,  2,  4], #
    base_hammers:  [  0,  0, 12,  3,  1, -3,  1,  1], #
    base_bombs:    [  0,  0,  6,  2,  2,  2,  5, -2], #
    base_fans:     [  0,  0,  5,  2,  2,  1,  1,  1], #
    base_bows:     [  0,  0, 10, -1,  1,  2,  2,  1], #
    base_staves:   [  0,  0, -3,  1, 10,  1,  1,  5], #
    base_claws:    [  0,  0,  4, -2,  1,  6,  5,  1], #
    base_knives:   [  0,  0,  3,  3,  1,  4,  3,  1], #
    base_guns:     [  0,  0, 11,  0,  0,  0,  4,  0], #
    base_chains:   [  0,  0,  3,  2,  2,  3,  3,  2], #

    swords:        [  0,  0,  1,  1,  1,  1,  1,  1], #
    spears:        [  0,  0,  2,  1,  0,  1,  2,  0], #
    scythes:       [  0,  0,  2,  0,  2,  0,  0,  2], #
    brushes:       [  0,  0,  1,  0,  2,  0,  1,  2], #
    hammers:       [  0,  0,  3,  2,  0,  0,  1,  0], #
    bombs:         [  0,  0,  1,  1,  0,  0,  3,  1], #
    fans:          [  0,  0,  1,  2,  1,  1,  0,  1], #
    bows:          [  0,  0,  2,  0,  0,  2,  2,  0], #
    staves:        [  0,  0,  1,  0,  3,  0,  0,  2], #
    claws:         [  0,  0,  2,  0,  1,  1,  1,  1], #
    knives:        [  0,  0,  1,  0,  0,  3,  2,  0], #
    guns:          [  0,  0,  2,  1,  0,  1,  2,  0], #
    chains:        [  0,  0,  1,  1,  1,  1,  1,  1], #
=end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
