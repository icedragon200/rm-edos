#==============================================================================#
# ■ Database.enemies
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           Enemy 0 (Dummy)
#           Enemy 1 (Slime)
#
#==============================================================================#
class Database
  #Enemy = RPG::Enemy
def self.build_enemies()
  @enemies = Array.new( 8+1 ) { RPG::Enemy.new() }
  @enemies.each { |e| e.initialize_add() }
#==============================================================================#
# ◙ Enemy 0 (Dummy)
#==============================================================================#
  enemy = Enemy.new()
  enemy.initialize_add()
  @enemies[0] = enemy
#==============================================================================#
# ◙ Enemy 1 (Slime)
#==============================================================================#
  enemy = Enemy.new()
  enemy.initialize_add()
  enemy.name = "Slime"
  enemy.nickname = ""
  enemy.class_id = 51
  enemy.max_level = LEVELCAP
  enemy.character_name = "$slime"
  enemy.character_index = 0
  enemy.face_name = ""
  enemy.face_index = 0
  enemy.equips = [0,0,0,0,0]
  enemy.description =
%Q(The cannon fodder of RPGs, these little ones,
are weak alone, and annoying in a group.
They have a tendency to follow the player around.)

  @enemies[1] = enemy
#==============================================================================#
# ◙ Enemy 2 (King Slime)
#==============================================================================#
  enemy = Enemy.new()
  enemy.initialize_add()
  enemy.name = "King Slime"
  enemy.nickname = ""
  enemy.class_id = 51
  enemy.max_level = LEVELCAP
  enemy.character_name = "$slimeking_a"
  enemy.character_index = 0
  enemy.face_name = ""
  enemy.face_index = 0
  enemy.equips = [0,0,0,0,0]
  @enemies[2] = enemy
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@enemies.size
    @enemies[i].id = i
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
