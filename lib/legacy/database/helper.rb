#
# EDOS/src/db/helper.rb
#   by IceDragon
#   dc 28/05/2013
#   dm 28/05/2013
# vr 1.0.0
class Database
  module Helper

    @occasion = {
      always:  0, #
      battle:  1, #
      menu:    2, #
      never:   3, #
    }

    @hit_type = {
      certain:   0, #
      physical:  1, #
      magical:   2, #
    }

    @damage_type = {
      none:    0, #
      hp_dam:  1, #
      mp_dam:  2, #
      hp_rec:  3, #
      mp_rec:  4, #
      hp_abs:  5, #
      mp_abs:  6, #
    }

    @item_id_off = {
      materials:  100, #
    }

    @skill_id_off = {
      builtin:  0,
      extended: 1,
      fire:     2, #
      water:    4, #
      earth:    6, #
      wind:     8, #
      light:   10, #
      dark:    12,  #
    }

    @weapon_id_off = {
      spears:   0, #
      brushes:  1, #
      swords:   2, #
      bows:     3, #
      staves:   4, #
      chains:   5, #
      hammers:  6, #
      guns:     7, #
      scythes:  8  #
    }

    @weapon_icon_index = {
      spears:   0, #
      brushes:  1, #
      swords:   2, #
      bows:     3, #
      staves:   4, #
      knives:   5, #
      hammers:  6, #
      guns:     7, #
      scythes:  8, #
    }

    @weapon_type_id = {
      spears:   1, #
      brushes:  2, #
      swords:   3, #
      bows:     4, #
      staves:   5, #
      knives:   6, #
      hammers:  7, #
      guns:     8, #
      scythes:  9, #
      #knives:  10, #
      #knives:  11, #
      #knives:  12, #
    }

    @armor_id_off = {
      shield:      0  , # // Shield
      helmet:      10 , # // Genderless
      helmetM:     20 , # // Males only
      helmetF:     30 , # // Females only
      body:        50 , # // Genderless
      bodyM:       60 , # // Males only
      bodyF:       70 , # // Females only
      accessory:   100 , # // Genderless
      accessoryM:  110 , # // Males only
      accessoryF:  120 , # // Females only
      arm:         150 , # // Genderless
      leg:         160 , # // Genderless
      crest:       180   # // Genderless
    }

    @armor_icon_index = {
      shield:      (16*1)-1, #
      helmet:      (16*5)-1, #
      helmetM:     (16*6)-1, #
      helmetF:     (16*7)-1, #
      body:        (16*9)-1, #
      bodyM:       (16*10)-1, #
      bodyF:       (16*11)-1, #
      accessory:   (16*13)-1, #
      accessoryM:  (16*14)-1, #
      accessoryF:  (16*15)-1, #
      arm:         (16*17)-1, #
      armM:        (16*18)-1, #
      armF:        (16*19)-1, #
      leg:         (16*21)-1, #
      legM:        (16*22)-1, #
      legF:        (16*23)-1, #
      crest:       (16*25)-1  #
    }

    @armor_type_id = {
      shield:     1, #
      helmet:     2, #
      helmetM:    3, #
      helmetF:    4, #
      body:       5, #
      bodyM:      6, #
      bodyF:      7, #
      accessory:  8, #
      accessoryM: 9, #
      accessoryF: 10, #
      arm:        11, #
      leg:        12, #
      crest:      13, #
    }

    @equip_type_id = {
      weapon:     0, #
      shield:     1, #
      helmet:     2, #
      body:       3, #
      accessory:  4, #
      arm:        5, #
      leg:        6, #
      crest:      7, #
    }

    @default_atype_pref = {

    }

    # 100 AKA No pref
    # The lower the pref the more likely the weapon will be selected in
    # optimization
    @default_wtype_pref = {
      spears:  100,
      brushes: 100,
      swords:  100,
      bows:    100,
      staves:  100,
      knives:  100,
      hammers: 100,
      guns:    100,
      scythes: 100,
    }

    @growth_class = {
    #              //[mhp,mmp,atk,def,mat,mdf,agi,lck]
      normal:        [  0,  0,  2,  0,  2,  0,  0,  0], #
      magic:         [  0,  0,  1,  0,  3,  0,  0,  0], #
      swift:         [  0,  0,  1,  0,  1,  0,  2,  0], #
      heay:          [  0,  0,  3,  0,  1,  0,  0,  0], #
      dual:          [  0,  0,  2,  0,  1,  0,  1,  0], #

    # // Limit (New-8) (Old-15)
      base_spears:   [  0,  0,  9,  0,  1,  0, -2,  0], #[  0,  0,  8,  1,  1,  1,  3,  1]
      base_brushes:  [  0,  0,  2,  0,  5,  0,  0,  1], #[  0,  0,  3,  0,  5,  1,  2,  4]
      base_swords:   [  0,  0,  6,  0,  2,  0,  0,  0], #[  0,  0,  5,  2,  2,  2,  2,  2]
      base_bows:     [  0,  0,  8,  0,  0,  0,  0,  0], #[  0,  0, 10, -1,  1,  2,  2,  1]
      base_fans:     [  0,  0,  4,  0,  4,  0,  0,  0], #[  0,  0,  5,  2,  2,  1,  1,  1]
      base_chains:   [  0,  0,  5,  0,  3,  0,  0,  0], #[  0,  0,  3,  2,  2,  3,  3,  2]
      base_hammers:  [  0,  0, 12,  0,  1,  0, -5,  0], #[  0,  0, 12,  3,  1, -3,  1,  1]
      base_guns:     [  0,  0, 15, -2,  0, -2, -1, -2], #[  0,  0, 11,  0,  0,  0,  4,  0]
      base_scythes:  [  0,  0, 10,  0,  0, -2,  0,  0], #[  0,  0, 10,  0,  3,  0,  0,  2]

      base_bombs:    [  0,  0, 12,  0,  0,  0,  0,  0], #[  0,  0,  6,  2,  2,  2,  5, -2]
      base_staves:   [  0,  2,  4, -4,  8,  0, -2,  0], #[  0,  0, -3,  1, 10,  1,  1,  5]
      base_claws:    [  0,  0,  4,  0,  1,  0,  1,  0], #[  0,  0,  4, -2,  1,  6,  5,  1]
      base_knives:   [  0,  0,  4,  0,  2,  0,  2,  0], #[  0,  0,  3,  3,  1,  4,  3,  1]

      spears:        [  0,  0,  2,  0,  1,  0, -1,  0], #[  0,  0,  2,  1,  0,  1,  2,  0]
      brushes:       [  0,  0,  0,  0,  1,  0,  0,  1], #[  0,  0,  1,  0,  2,  0,  1,  2]
      swords:        [  0,  0,  1,  0,  1,  0,  0,  0], #[  0,  0,  1,  1,  1,  1,  1,  1]
      bows:          [  0,  0,  1,  0,  0,  0,  0,  0], #[  0,  0,  2,  0,  0,  2,  2,  0]
      fans:          [  0,  0,  1,  0,  1,  0,  0,  0], #[  0,  0,  1,  2,  1,  1,  0,  1]
      chains:        [  0,  0,  1,  0,  1,  0,  0,  0], #[  0,  0,  1,  1,  1,  1,  1,  1]
      hammers:       [  0,  0,  2,  0,  0,  0, -1,  0], #[  0,  0,  3,  2,  0,  0,  1,  0]
      guns:          [  0,  0,  2,  0,  0,  0, -1,  0], #[  0,  0,  2,  1,  0,  1,  2,  0]
      scythes:       [  0,  0,  2,  0,  0,  0,  0,  0], #[  0,  0,  2,  0,  2,  0,  0,  2]

      bombs:         [  0,  0,  2,  0,  0,  0,  0,  0], #[  0,  0,  1,  1,  0,  0,  3,  1]
      staves:        [  0,  0,  0,  0,  2,  0,  0,  0], #[  0,  0,  1,  0,  3,  0,  0,  2]
      claws:         [  0,  0,  1,  0,  0,  0,  1,  0], #[  0,  0,  2,  0,  1,  1,  1,  1]
      knives:        [  0,  0,  1,  0,  0,  0,  1,  0], #[  0,  0,  1,  0,  0,  3,  2,  0]
    }

    @skill_type_id = {
      normal:  1, #
      fire:    2, #
      water:   3, #
      earth:   4, #
      wind:    5, #
      light:   6, #
      dark:    7, #
    # // o3o
      hp:  8, #
      mp:  9, #
      wt:  10, #
    }

    @scope = {
      none:            0, #
      one_foe:         1,
      one_enemy:       1, #
      one_friend:      7,
      one_ally:        7, #
      one_friend_dead: 8,
      one_ally_dead:   8, #
      one_friend_koed: 9,
      one_ally_koed:   9, #
      user:            11,
      self:            11, #
    # // NYI
      global:           20, #
      foe_team:         21,
      enemy_team:       21, #
      foe_team_alive:   22,
      enemy_team_alive: 22, #
      foe_team_dead:    23,
      enemy_team_dead:  23, #
      ally_team:        31,
      user_team:        31, #
      ally_team_alive:  32,
      user_team_alive:  32, #
      ally_team_dead:   33,
      user_team_dead:   33, #
      everyone:         40, #
      everyone_alive:   41, #
      everyone_dead:    42 #
    }

    def self.occasion(sym)
      @occasion[sym]
    end

    def self.hit_type(sym)
      @hit_type[sym]
    end

    def self.damage_type(sym)
      @damage_type[sym]
    end

    def self.scope(sym)
      @scope[sym]
    end

    def self.equip_type_id(id)
      @equip_type_id[id]
    end

    def self.armor_type_id(id)
      @armor_type_id[id]
    end

    def self.weapon_type_id(id)
      @weapon_type_id[id]
    end

    def self.item_type_id(id)
      @item_type_id[id]
    end

    def self.skill_type_id(sym)
      @skill_type_id[sym]
    end

    def self.all_atype_ids
      @armor_type_id.values
    end

    def self.all_wtype_ids
      @weapon_type_id.values
    end

    def self.all_stype_ids
      @skill_type_id.values
    end

    def self.armor_id(sym, offset=0)
      @armor_id_off[sym] + offset
    end

    def self.weapon_id(sym, offset=0)
      @weapon_id_off[sym] + offset
    end

    def self.skill_id(sym, offset=0)
      @skill_id_off[sym] + offset
    end

    def self.item_id(sym, offset=0)
      @item_id_off[sym] + offset
    end

    def self.etype_id(sym)
      @equip_type_id[sym]
    end

    def self.params_from_growth(*growths)
      result = [0]*8
      (growths.map { |gn| @growth_class[gn] }).each do |g|
        g.each_with_index { |s, i| result[i] += s }
      end
      result
    end

    def self.adjust_armor(item, armor_sym, equip_sym)
      iof = armor_id(armor_sym) # // ID offset
      item.icon = armor_icon(armor_sym, item.id)
      item.id = iof + item.id
      item.atype_id = armor_type_id(armor_sym)
      item.etype_id = equip_type_id(equip_sym)
      item
    end

    def self.adjust_armors(items, armor_sym, equip_sym)
      items.each { |i| adjust_armor(i, armor_sym, equip_sym) if i }
      items
    end

    def self.weapon_parameter(weapon, parameter, level)
      n = params_from_growth(*weapon.growths)[parameter]
      n = weapon.params[parameter] + (level * n * 1.0)
      n.to_i
    end

    def self.adjust_weapon(item, weapon_sym)
      iof = @weapon_id_off[weapon_sym] # // ID offset
      item.icon.index = @weapon_icon_index[weapon_sym] + item.id
      item.id = iof + item.id
      item.wtype_id = @weapon_type_id[weapon_sym]
      item
    end

    def self.adjust_weapons(items, weapon_sym)
      items.compact.each { |i| adjust_weapon(i, weapon_sym) }
      items
    end

    def self.weapon_icon(type, offset)
      RPG::BaseItem::Icon.new(@weapon_icon_index[type] + offset)
    end

    def self.armor_icon(type, offset)
      RPG::BaseItem::Icon.new(@armor_icon_index[type] + offset)
    end

    def self.mk_atype_pref(hsh)
      Hash[@default_atype_pref.merge(hsh).map { |(sym, v)| [atype_id(sym), v] }]
    end

    def self.mk_wtype_pref(hsh)
      Hash[@default_wtype_pref.merge(hsh).map { |(sym, v)| [wtype_id(sym), v] }]
    end

    def self.hash_offset(hsh, offset)
      hsh.replace(Hash[hsh.map { |(k, v)| [k, v * offset] }])
    end

    hash_offset(@skill_id_off, 10)
    hash_offset(@weapon_id_off, 10)
    hash_offset(@weapon_icon_index, 16)

    class << self
      alias :atype_id :armor_type_id
      alias :wtype_id :weapon_type_id
      alias :stype_id :skill_type_id
      alias :scope_id :scope
    end

  end
end
