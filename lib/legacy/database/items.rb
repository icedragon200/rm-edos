#==============================================================================#
# ■ Database.items
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           Item 0 (Dummy)
#           Item 1 (<unnamed>)
#
#==============================================================================#
class Database
  #@_ref_items = load_data( "Items.rvdata2" )
  #File.open("Items.log", "w+") { |f|
  #  for i in 0...@_ref_items.size
  #    f.puts @_ref_items[i].inspect if @_ref_items[i]
  #  end
  #}
  #Item = RPG::Item
def self.build_items()
  @items = []
#==============================================================================#
# ◙ Item 0 (Dummy)
#==============================================================================#
  item              = Item.new()
  item.initialize_add()
  item.id           = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Potion)
#==============================================================================#
  item              = Item.new()
  item.initialize_add()
  item.id           = 1
  item.name         = "Lred"
  item.icon.index   = 212
  item.description  = "A very basic potion, restores 20 HP"
  item.features     = []
  item.note         = ""
  item.scope        = Helper.scope_id(:one_ally)
  item.occasion     = 0
  item.speed        = 0
  item.success_rate = 100
  item.repeats      = 1
  item.tp_gain      = 0
  item.hit_type     = 0
  item.animation_id = 2
  item.itype_id     = 0
  item.price        = 0
  item.consumable   = true
  item.effects << MkEffect.recover_hp(0.0, 20.0)
  item.ai_tags      = [:heal_hp]
  item.element_id = element_id(:light)
  item.atk_range.range = 1
  item.atk_range.minrange = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Potion)
#==============================================================================#
  item              = Item.new()
  item.initialize_add()
  item.id           = 2
  item.name         = "Lreden"
  item.icon.index   = 212
  item.description  = "A very basic potion, restores 50 HP"
  item.features     = []
  item.note         = ""
  item.scope        = Helper.scope_id(:one_ally)
  item.occasion     = 0
  item.speed        = 0
  item.success_rate = 100
  item.repeats      = 1
  item.tp_gain      = 0
  item.hit_type     = 0
  item.animation_id = 2
  item.itype_id     = 0
  item.price        = 0
  item.consumable   = true
  item.effects << MkEffect.recover_hp(0.0, 50.0)
  item.ai_tags      = [:heal_hp]
  item.element_id = element_id(:light)
  item.atk_range.range = 1
  item.atk_range.minrange = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Smoo)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id           = 6
  item.name         = "Smoo"
  item.icon.index   = 213
  item.description  = "Magical~, restores 10 SP"
  item.features     = []
  item.note         = ""
  item.scope        = Helper.scope_id(:one_ally)
  item.occasion     = 0
  item.speed        = 0
  item.success_rate = 100
  item.repeats      = 1
  item.tp_gain      = 0
  item.hit_type     = 0
  item.animation_id = 2
  item.itype_id     = 0
  item.price        = 0
  item.consumable   = true
  item.effects << MkEffect.recover_mp(0.0, 10.0)
  item.ai_tags      = [:heal_mp]
  item.element_id = element_id(:light)
  item.atk_range.range = 1
  item.atk_range.minrange = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Smootuu)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id           = 7
  item.name         = "Smootuu"
  item.icon.index   = 213
  item.description  = "Magical~, restores 20 SP"
  item.features     = []
  item.note         = ""
  item.scope        = Helper.scope_id(:one_ally)
  item.occasion     = 0
  item.speed        = 0
  item.success_rate = 100
  item.repeats      = 1
  item.tp_gain      = 0
  item.hit_type     = 0
  item.animation_id = 2
  item.itype_id     = 0
  item.price        = 0
  item.consumable   = true
  item.effects << MkEffect.recover_mp(0.0, 20.0)
  item.ai_tags      = [:heal_mp]
  item.element_id = element_id(:light)
  item.atk_range.range = 1
  item.atk_range.minrange = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Elixir I)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id           = 9
  item.name         = "Elixir I"
  item.icon.index   = 215
  item.description  = "Lred & Smoo, recovers 20% hp and 20% sp"
  item.features     = []
  item.note         = ""
  item.scope        = Helper.scope_id(:one_ally)
  item.occasion     = 0
  item.speed        = 0
  item.success_rate = 100
  item.repeats      = 1
  item.tp_gain      = 0
  item.hit_type     = 0
  item.animation_id = 2
  item.itype_id     = 0
  item.price        = 0
  item.consumable   = true
  item.effects << MkEffect.recover_hp(0.2, 0.0)
  item.effects << MkEffect.recover_mp(0.2, 0.0)
  item.ai_tags      = [:heal_hp, :heal_mp]
  item.atk_range.range = 1
  item.atk_range.minrange = 0
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Rock)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id                = Helper.item_id(:materials, 1)
  item.name              = "Rock"
  item.icon.iconset_name = @iconset_name[:materials]
  item.icon.index        = 350
  item.description       = "It is, what it is"
  item.features          = []
  item.note              = ""
  item.scope             = Helper.scope_id(:none)
  item.occasion          = 0
  item.speed             = 0
  item.success_rate      = 100
  item.repeats           = 1
  item.tp_gain           = 0
  item.hit_type          = 0
  item.animation_id      = 0
  item.itype_id          = 0
  item.price             = 0
  item.consumable        = false
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Block)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id                = Helper.item_id(:materials, 2)
  item.name              = "Block"
  item.icon.iconset_name = @iconset_name[:materials]
  item.icon.index        = 351
  item.description       = "Huge block of material D:"
  item.features          = []
  item.note              = ""
  item.scope             = Helper.scope_id(:none)
  item.occasion          = 0
  item.speed             = 0
  item.success_rate      = 100
  item.repeats           = 1
  item.tp_gain           = 0
  item.hit_type          = 0
  item.animation_id      = 0
  item.itype_id          = 0
  item.price             = 0
  item.consumable        = false
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Fur)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id                = Helper.item_id(:materials, 3)
  item.name              = "Fur"
  item.icon.iconset_name = @iconset_name[:materials]
  item.icon.index        = 323
  item.description       = "Warm and cozy O:"
  item.features          = []
  item.note              = ""
  item.scope             = Helper.scope_id(:none)
  item.occasion          = 0
  item.speed             = 0
  item.success_rate      = 100
  item.repeats           = 1
  item.tp_gain           = 0
  item.hit_type          = 0
  item.animation_id      = 0
  item.itype_id          = 0
  item.price             = 0
  item.consumable        = false
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Mermerald)
#==============================================================================#
  item = Item.new()
  item.initialize_add()
  item.id                = Helper.item_id(:materials, 4)
  item.name              = "Mermerald"
  item.icon.iconset_name = @iconset_name[:materials]
  item.icon.index        = 347
  item.description       = "The basis to magic items"
  item.features          = []
  item.note              = ""
  item.scope             = Helper.scope_id(:none)
  item.occasion          = 0
  item.speed             = 0
  item.success_rate      = 100
  item.repeats           = 1
  item.tp_gain           = 0
  item.hit_type          = 0
  item.animation_id      = 0
  item.itype_id          = 0
  item.price             = 0
  item.consumable        = false
  @items[item.id] = item
#==============================================================================#
# ◙ Item (Wood)
#==============================================================================#
  item = Item.new()
  item.initialize_add
  item.id                = Helper.item_id(:materials, 5)
  item.name              = "Wood"
  item.icon.iconset_name = @iconset_name[:materials]
  item.icon.index        = 331
  item.description       = "From trees"
  item.features          = []
  item.note              = ""
  item.scope             = Helper.scope_id(:none)
  item.occasion          = 0
  item.speed             = 0
  item.success_rate      = 100
  item.repeats           = 1
  item.tp_gain           = 0
  item.hit_type          = 0
  item.animation_id      = 0
  item.itype_id          = 0
  item.price             = 0
  item.consumable        = false
  @items[item.id] = item
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@items.size
    next unless @items[i]
    @items[i].id = i
    @items[i].id_offset = 0
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
