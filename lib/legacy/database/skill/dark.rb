# // 02/24/2012
# // 02/24/2012
class Database
def self.mk_skills6() # // Dark
  skills = []
  element = :dark
#==============================================================================#
# ◙ Skill (Bleackur)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Blekur"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "A simple shadow spell, a must know for beginners"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Yaskvoid)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Yaskvoid"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "The shadow void"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Bakoid)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Bakoid"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Even larger void"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Obscran)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Obscran"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Creeping into your mind"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Bane)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Bane"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Shivers"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Chaon)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Chaon"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Shivers"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:enemy_team)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Drakraos)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Drakraos"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Guardian Spirit Drakraos"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user_team)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Yami)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Yami"
  skill.icon.index        = 98 # // Doesn't Matter >_>
  skill.description       = "Guardian Spirit Drakraos"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:global)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "a.mat * 5 - b.mdf * 4"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  skills.each { |obj| obj.tags << "elemental" if obj }
  adjust_skills(skills, element)
end
end
