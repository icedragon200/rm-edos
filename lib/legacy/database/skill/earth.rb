# // 02/24/2012
# // 02/24/2012
class Database
def self.mk_skills3() # // Earth
  skills = []
  element = :earth
#==============================================================================#
# ◙ Skill (Crush)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Crush"
  skill.icon.index        = 98
  skill.description       = "Crush!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  # //
  skill.atk_range.range = 2
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Blockard)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Blockard"
  skill.icon.index        = 98
  skill.description       = "Large solid block"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Terraki)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Terraki"
  skill.icon.index        = 98
  skill.description       = "Summon Terraki, rain down diamond shards"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Worenike)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Worenike"
  skill.icon.index        = 98
  skill.description       = "Summon the old Worenike tree, roots wrap around targets"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Dregurak)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Dregurak"
  skill.icon.index        = 98
  skill.description       = "Release mud from the ground!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Druban)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Druban"
  skill.icon.index        = 98
  skill.description       = "Wrapping in the earth for 5 turns"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_ally)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 17
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skill.effects << MkEffect.add_buff(param_id(:def), 5)
  # //
  skill.atk_range.range = 2
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Naturak)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Naturak"
  skill.icon.index        = 98
  skill.description       = "Worenike's roots heal some wounds"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_ally)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 17
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "(a.mat * 0.9).to_i"
  skill.damage.variance   = 10
  skill.damage.critical   = false
  skill.effects << MkEffect.add_buff(param_id(:mdf), 5)
  skill.atk_range.range = 3
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Gaiadrom)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Gaiadrom"
  skill.icon.index        = 98
  skill.description       = "Guardian Spirit Gaiadrom"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:global)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 17
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Valdine.E)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 9
  skill.name              = "Valdine.E"
  skill.icon.index        = 98
  skill.description       = "Guardian Spirit Valdine"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user_team)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 17
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  skill.effects << MkEffect.add_buff(param_id(:def), 5)
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  skills.each { |obj| obj.tags << "elemental" if obj }
  adjust_skills(skills,element)
end
end
