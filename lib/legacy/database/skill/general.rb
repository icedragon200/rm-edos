# // 02/24/2012
# // 02/24/2012
class Database
def self.mk_skills0() # // General
  skills = []
  #element = :none
#==============================================================================#
# ◙ Skill 1 (Attack)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Attack"
  skill.icon.index        = 134
  skill.description       = "Something you will do VERY often"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_foe)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:physical)
  skill.animation_id      = -1
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = -1
  skill.damage.formula    = "a.atk * 2 - b.def"#"6d2 * a.atk - 2d4 * b.def"#"a.atk * 2 - b.def"
  skill.damage.variance   = 20
  skill.damage.critical   = true
  skill.effects << RPG::UsableItem::Effect.new( 21, 0, 1.0, 0.0 )
  skill.atk_range.range = 1
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 2 (Ace Attack)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Ace Attack"
  skill.icon.index        = 134
  skill.description       = ""
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_foe)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = -1
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = -1
  skill.damage.formula    = "a.atk * 2 - b.def"
  skill.damage.variance   = 20
  skill.damage.critical   = true
  skill.effects << RPG::UsableItem::Effect.new( 21, 0, 1.0, 0.0 )
  skill.atk_range.range = 1
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 6 (Guard)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Guard"
  skill.icon.index        = 139
  skill.description       = "When the going gets rough, and you cant get going"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 2000
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 0
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << RPG::UsableItem::Effect.new( 21, 3, 1.0, 0.0 )
  skill.atk_range.range = 0
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 8 (Random Warp)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "R.Warp"
  skill.icon.index        = 116
  skill.description       = ""
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_foe)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 0
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << RPG::UsableItem::Effect.new( 41, 103, 0.0, 0.0 )
  skill.atk_range.range = 0
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 9 (Skip)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 9
  skill.name              = "Skip"
  skill.icon.index        = 116
  skill.description       = "Nothing to do? Skip your turn then"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 0
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  #skill.effects << RPG::UsableItem::Effect.new( 21, 3, 1.0, 0.0 )
  skill.atk_range.range = 0
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 10 (Move)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 10
  skill.name              = "Move"
  skill.icon.index        = 467
  skill.description       = ""
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user)
  skill.occasion          = Helper.occasion(:battle)
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 0
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << RPG::UsableItem::Effect.new( 41, 101, 0, 0 )
  skill.atk_range.range = 0
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 11 (Nudge)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 11
  skill.name              = "Nudge"
  skill.icon.index        = 11
  skill.description       = ""
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_friend_koed)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 0
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  #skill.element_id = element_id(:light)
  skill.effects << MkEffect.rem_state(1,1.0)
  skill.atk_range.range = 1
  skill.atk_range.minrange = 1
  @skills[skill.id] = skill
#==============================================================================#
# ◙ Skill 12 (Tame)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 12
  skill.name              = "Tame"
  skill.icon.index        = 122
  skill.description       = ""
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_foe)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 4
  skill.stype_id          = Helper.skill_type_id(:normal)
  skill.mp_cost           = 0
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << RPG::UsableItem::Effect.new( 41, 102, 0, 0 )
  skill.atk_range.range = 1
  skill.atk_range.minrange = 1
  skills[skill.id] = skill
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  skills
  #adjust_skills(skills,element)
end
end
