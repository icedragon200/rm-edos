# // 02/24/2012
# // 02/24/2012
class Database
def self.mk_skills5() # // Light
  skills = []
  element = :light
#==============================================================================#
# ◙ Skill (Dawjec)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 1
  skill.name              = "Dawjec"
  skill.icon.index        = 98
  skill.description       = "Basic Light Magic"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 25
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Reradi)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 2
  skill.name              = "Reradi"
  skill.icon.index        = 98
  skill.description       = "Light punish my enemies!"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Brigh)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 3
  skill.name              = "Brigh"
  skill.icon.index        = 98
  skill.description       = "Dont stare"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_enemy)
  skill.occasion          = Helper.occasion(:battle) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:magical)
  skill.animation_id      = 2
  skill.stype_id          = Helper.skill_type_id(element)
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_dam)
  skill.damage.element_id = element_id(element)
  skill.damage.formula    = "3 * a.mat / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Mehura)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 4
  skill.name              = "Mehura"
  skill.icon.index        = 112
  skill.description       = "Small blessings save the day"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_friend)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 4
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "(a.mat * 1.1).to_i"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 2
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv1)
#==============================================================================#
# ◙ Skill (Dimehura)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 5
  skill.name              = "Dimehura"
  skill.icon.index        = 112
  skill.description       = "More blessings save the day"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_friend)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "a.mat * 12"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 3
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv2)
#==============================================================================#
# ◙ Skill (Fehura)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 6
  skill.name              = "Fehura"
  skill.icon.index        = 112
  skill.description       = "Large blessings save the day"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_friend)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "a.mat * 20"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.atk_range.range = 4
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv3)
#==============================================================================#
# ◙ Skill (Liire)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 7
  skill.name              = "Liire"
  skill.icon.index        = 112
  skill.description       = "Gift of blessings"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:one_friend)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "b.mhp"
  skill.damage.variance   = 0
  skill.damage.critical   = false
  #skill.effects << MkEffect.recover_hp(1.0,0.0)
  skill.atk_range.range = 2
  skill.atk_range.minrange = 0
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Xenura)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 8
  skill.name              = "Xenura"
  skill.icon.index        = 112
  skill.description       = "Summons Xenura"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user_team)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:hp_rec)
  skill.damage.element_id = 0
  skill.damage.formula    = "b.mhp / 2"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  #skill.effects << MkEffect.recover_hp(1.0,0.0)
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ Skill (Chaon.L)
#==============================================================================#
  skill                   = Skill.new()
  skill.initialize_add()
  skill.id                = 9
  skill.name              = "Chaon.L"
  skill.icon.index        = 112
  skill.description       = "Summons Chaon"
  skill.features          = []
  skill.note              = ""
  skill.scope             = Helper.scope(:user_team)
  skill.occasion          = Helper.occasion(:always) #1
  skill.speed             = 0
  skill.success_rate      = 100
  skill.repeats           = 1
  skill.tp_gain           = 0
  skill.hit_type          = Helper.hit_type(:certain)
  skill.animation_id      = 2
  skill.stype_id          = 0
  skill.mp_cost           = 3
  skill.tp_cost           = 0
  skill.message1          = ""
  skill.message2          = ""
  skill.required_wtype_id1= 0
  skill.required_wtype_id2= 0
  skill.damage.type       = Helper.damage_type(:none)
  skill.damage.element_id = 0
  skill.damage.formula    = "0"
  skill.damage.variance   = 20
  skill.damage.critical   = false
  skill.effects << MkEffect.rem_state(2, 1.0)
  skills[skill.id] = skill
  add_skill_to_groups(skill,element,:lv4)
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  skills.each { |obj| obj.tags << "elemental" if obj }
  adjust_skills(skills,element)
end
end
