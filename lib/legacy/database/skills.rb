#==============================================================================#
# ■ Database.skills
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           Skill 0 (Dummy)
#           Skill 1 (Attack)
#
#==============================================================================#
class Database
  #@_ref_skills = load_data( "Skills.rvdata2" )
  #File.open("Skills.log", "w+") { |f|
  #  for i in 0...@_ref_skills.size
  #    f.puts @_ref_skills[i].inspect if @_ref_skills[i]
  #  end
  #}
  #Skill = RPG::Skill
  DEF_ATTACK_SKILL_ID = 1
  DEF_GUARD_SKILL_ID  = 6
  DEF_SKIP_SKILL_ID   = 9
  # // JUMPS
  # // (GENERAL_SKILLS)
  # // (FIRE_SKILLS)
  # // (WATER_SKILLS)
  # // (EARTH_SKILLS)
  # // (WIND_SKILLS)
  # // (LIGHT_SKILLS)
  # // (DARK_SKILLS)
  def self.adjust_skills(items, element)
    items.compact.each { |i| adjust_skill(i, element) }
    items
  end

  def self.adjust_skill(item,element)
    iof = Helper.skill_id(element) # // ID offset
    set_element_icon(element,item,:skill)
    item.id        += iof
    item.element_id = element_id(element)
    item.stype_id   = Helper.stype_id(element)
    item.tags << element.to_s
    item.tags.uniq!
    item
  end

  def self.add_skills(items)
    items.compact.each {|i|@skills[i.id]=i}
  end

  #def self.set_skill_ids(skills,element)
  #  skills.each_with_index{|s,i|@skill_id[(element.to_s+i.to_s).to_sym]=s.id if s}
  #  skills
  #end

  @skill_group = {}

  def self.add_skill_to_groups(skill, *groups)
    groups.each do |g|
      @skill_group[g] ||= Set.new
      @skill_group[g].add(skill)
    end
  end

  def self.find_skills_by_groups(*groups)
    groups.inject(@skills){|r,g|r&@skill_group[g].to_a}
  end

  def self.build_skills()
    @skills = []
  #==============================================================================#
  # ◙ Skill 0 (Dummy)
  #==============================================================================#
    skill                   = Skill.new()
    skill.initialize_add()
    skill.id                = 0
    @skills[skill.id] = skill
  # // (GENERAL_SKILLS)
    add_skills mk_skills0()
  # // (FIRE_SKILLS)
    add_skills mk_skills1()
  # // (WATER_SKILLS)
    add_skills mk_skills2()
  # // (EARTH_SKILLS)
    add_skills mk_skills3()
  # // (WIND_SKILLS)
    add_skills mk_skills4()
  # // (LIGHT_SKILLS)
    add_skills mk_skills5()
  # // (DARK_SKILLS)
    add_skills mk_skills6()
  #==============================================================================#
  # ◙ REMAP
  #==============================================================================#
    for i in 0...@skills.size
      @skills[i].id = i if @skills[i]
    end
  end

end

require_relative 'skill/general'
require_relative 'skill/fire'
require_relative 'skill/water'
require_relative 'skill/earth'
require_relative 'skill/wind'
require_relative 'skill/light'
require_relative 'skill/dark'

__END__
-Skill Types-
# --------------------- #
~WeaponClass~
Spear
Sword
Scythe
Bow
Brush
Staff
Bomb
Hammer
Knuckle
Knife
Fan
Gun
Chain

# --------------------- #
~Element~
Fire
Ice
Lightning
Ground
Water
Air
Light
Dark

# --------------------- #
~CORE~
Core
Core-Branch

# --------------------- #
~SkT~
Healing
Support
BadStatus
Summon

# --------------------- #
~Class~
Lancemanship
Artwork
Swordsmanship
Hunter
Dance
Ninjutsu
Pantech
Gunmanship

# --------------------- #
~Subclass~
Footwork
Shadow (Arch-Key)
NightHunter
Nature
Pheonix
-------------
Rai-Fist
MartialArt


-SkillList-
# --------------------- #
~Class~
>>Lancemanship
>>Artwork
>>Swordsmanship
>>Hunter
>>Dance
>>Ninjutsu
>>Pantech
>>Gunmanship

# --------------------- #
~Subclass~
>>Footwork
Leg Sweep
Templar
Guillotine Kick

>>Shadow (Arch-Key)
Shadow Cutter
Shadow Flame
Shadow Bind
Shadow Spike
Shadow Radar
Dusty Roullete

>>NightHunter
Blood Overdrive
Night Intentions
Rend

>>Nature
Sleep Pollen
Stun Spore

>>Pheonix
Pheonix Revive
Pheonix Heal

>>-------------
>>Rai-Fist
>>MartialArt

(O'.')=O Q('.'O) Boxing
~Random(Words/)
endem
slyka
vija
doru
duria
aio
gen
xin
sode
tas
basa
rei
rai
led
mied
vdraq
im
ti
red
ve
ry
ti
red

Lo-Arg

Fire       >> Spark
Ice        >> Frost
Lightning  >> Shock
Water      >> Rain
Earth      >> Ground(ed)
Air        >> Atmos
Light      >> ??
Dark       >> ??

~Sword (Words)
Gladius (Latin: Sword)

~Spear (Words)
Telum (Latin: Spear)

#-------------------------------------------------------#

~Fire (Words)
Ash
Ardor (Heat, Love, Passion)
Discharge
Fire
Cinder
Fervor (Excessive heat, aroused) *Careful with this
Blaze
Burn
Balefire
Flame
Ignite
Incite (Stir up)
Inflame
Spark
Burst
Erupt
Explode
Conflagration
Combust
Inferno
Hot
Heat
Kindle
Ignis
Zeal (Fervor, all over again)

~Fire (Expression)
Burn off

~Fire (Objects)
Lighter
Stove
Hell
Pheonix

~Fire (Names)
(Chaos)  Blascite
(Dance)  Blazardor
(Spell)  Cindlebust
(Spell)  Inflamite
(Spell)  Combusplode
(Spell)  Sparkball   (Projectile)
(Summon) Inflamari
(Support)Cinfer

(Lance) Ferlancen
(Brush) Flastro
(Sword) Bladflam
(Bow)   Arferno
(Fan)   Faflam
(Chain) Lashcite
(Bomb)  Sparomb
(Gun)   Flashot
#-------------------------------------------------------#

~Ice (Words)
Artic
Glacial
Gelid
Ice
Chill
Frost
Freeze
Cold
Icicle
Icy

~Ice (Objects)
Refridgerator
Subzero
0 Degrees
IceCube

~Ice (Names)
(Chaos)  Glacaos
(Dance)  Fronade
(Spell)  Fricite
(Spell)  Artinite
(Spell)  Fridic
(Spell)  Chillisile (Projectile)
(Summon) Frostina / Frostena
(Support)Chicle

(Lance) Froslance
(Brush) Coldstroke
(Sword) Frosblam
(Bow)   Glacirrow
(Fan)   Swieeze
(Chain) Fetill
(Bomb)  Gelomb
(Gun)   Icish

#-------------------------------------------------------#

~Lightning (Words)
Levin
Thunderbolt
Lightning
Strike
Bright
Shock
Bolt
Jolt
Xenon
Aura
Tesla
Static
Ohm

~Lightning (Expression)
Chain Lightning

~Lightning (Objects)

~Lightning (Names)
(Chaos)  Shoas
(Dance)  Levaltz
(Spell)  Stribolt
(Spell)  Xenite
(Spell)  Tesock
(Spell)  Ohmesla (Projectile)
(Summon) Xenolto
(Support)Joltura

(Lance) Listryke
(Brush) Shobu
(Sword) Gladatter
(Bow)   Arcike
(Fan)   Xenfan
(Chain) Kimiarch
(Bomb)  Teslomb
(Gun)   Joltound

#-------------------------------------------------------#

~Earth (Words)
Earth
Dirt
Rocks
Stone
Diamond
Tree
Dreg (Anti-Liquid, small residue in a liquid)
Grind
Block
Floor
Spike
Tierra (Spanish: Earth)
Terra  (Latin: Land)

~Earth (Expression)
Rock Fall
Rock Slide

~Earth (Objects)

~Earth (Names)
(Chaos)   Dregturak
(Dance)   Renawaltz
(Spell)   Diaburst
(Spell)   Blockard
(Spell)   Trestone
(Spell)   Tierlance (Projectile)
(Summon)  Gaiadrom
(Support) Naturak

(Lance) Worenike
(Brush) Terrabrush
(Sword) TeraGlade
(Bow)   Flexspi
(Fan)   Terlamna
(Chain) Uminku
(Bomb)  Erumpo
(Gun)   Gekaigu

#-------------------------------------------------------#

~Water (Words)
Water
Wet
Wash
Flush
Rinse
Aqua
Clean
Bathe
Sea
Tide
Wave
River
Bukakkeru (Splash Liquid)

~Water (Expression)

~Water (Objects)

~Water (Names)
(Chaos)   Drench
(Dance)   Seren
(Spell)   Tidespla
(Spell)   Seaer
(Spell)   Oceve
(Spell)   Wublu (Projectile)
(Summon)  Valdine
(Support) Flushine

(Lance)  Tidance
(Brush)  Flubrush
(Sword)  Umiglade
(Bow)    Aquyumi
(Fan)    Mizufan
(Chain)  Rinksea
(Bomb)   Bomsave
(Gun)    Liquround

#-------------------------------------------------------#

~Wind (Words)
Wind
Vent
Air
Slash
Hurricane
Storm
Typhoon
Gust
Coil
Twister
Blast
Breeze
Cloud

~Wind (Expression)
Blow Away

~Wind (Objects)

~Wind (Names)
(Chaos)   Dredclone
(Dance)   Kazeste
(Spell)   Breephoo
(Spell)   Guster
(Spell)   Aaitakaze (Irie Wind)
(Spell)   Coilorm (Projectile)
(Summon)  Dovun
(Support) Airia

(Lance)   Gustlan
(Brush)   Strophoo
(Sword)   Espadust
(Bow)     Yumeeze
(Fan)     Fanstrom
(Chain)   Kazewhip
(Bomb)    Bloud
(Gun)     Roucane

#-------------------------------------------------------#

~Light (Words)
Light
Bright
Illuminate
Glow
Shine
Radiant
Discharge
Dawn

~Light (Expression)

~Light (Objects)

~Light (Names)
(Chaos)   Purlumate
(Dance)   Radi
(Spell)   Brigh
(Spell)   Glumin
(Spell)   Disadian
(Spell)   Dawjec (Projectile)
(Summon)  Xenura (Zenaura)
(Support) Reradi

(Lance)   Dawnspear
(Brush)   Lumush
(Sword)   Spasine
(Bow)     Yuawn
(Fan)     Rafine
(Chain)   Linkight
(Bomb)    Blime
(Gun)     Rolawn

#-------------------------------------------------------#

~Dark (Words)
Shroud
Cloud
Dark
Void
Black
Gloom
Obscure
Bleak
Sullen
Dusk
Yami
Chaos

~Dark (Expression)

~Dark (Objects)

~Dark (Names)
(Chaos)   Gloonack
(Dance)   Voidance
(Spell)   Chaon
(Spell)   Obscran
(Spell)   Yaskvoid
(Spell)   Bleackur (Projectile)
(Summon)  Drakraos
(Support) Clovvert

(Lance)   Dusklance
(Brush)   Drastroke
(Sword)   Chaspada
(Bow)     Bleyumi
(Fan)     Voiswir
(Chain)   Sullvip
(Bomb)    Yaamoud
(Gun)     Blacround

#-------------------------------------------------------#

~Slash (Words)
Slash
Cut
Break
Shatter
Slice
Chop
Crush
Strike
Mow

~Slash (Expression)

~Slash (Objects)

~Slash (Names)

#-------------------------------------------------------#

~Pierce (Words)
Pierce
Stick
Stab
Drill
Perforate
Bore
Penetrate
Permeate
Prick
Impale

~Pierce (Expression)

~Pierce (Objects)

~Pierce (Names)

#-------------------------------------------------------#

~Bow (Words)
Bow
Bend
Arc
Arch
Curve
Crook
Loop
Kink
Ansa (Loop)

~Bow (Expression)

~Bow (Objects)

~Bow (Names)

#-------------------------------------------------------#

~Brush (Words)
Brush
Broom
Brushwood
Besom
Bush
Tail
Graze

~Brush (Expression)

~Brush (Objects)

~Brush (Names)

#-------------------------------------------------------#

~Chain (Words)
Chain
String
Network
Rope
Tie
Fetter
Sequence

~Chain (Expression)

~Chain (Objects)

~Chain (Names)

#-------------------------------------------------------#

~Cure (Words)
Heal
Cure
Recover
Life
Raise
Ressurrect


~Cure (Expression)

~Cure (Objects)

~Cure (Names)
(Spell) Mehura (Heal)
(Spell) Liire (Raise)
