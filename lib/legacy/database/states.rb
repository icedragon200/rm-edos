#==============================================================================#
# ■ Database.states
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           State 0 (Dummy)
#           State 1 (Ko)
#
#==============================================================================#
class Database
  #@_ref_states = load_data( "States.rvdata2" )
  #File.open("States.log", "w+") { |f|
  #  for i in 0...@_ref_states.size
  #    f.puts @_ref_states[i].inspect if @_ref_states[i]
  #  end
  #}
  #State = RPG::State
def self.build_states()
  @states = []
#==============================================================================#
# ◙ State 0 (Dummy)
#==============================================================================#
  @states[0] = State.new()
#==============================================================================#
# ◙ State 1 (Ko)
#==============================================================================#
  state                       = State.new()
  state.initialize_add()
  state.id                    = 1
  state.name                  = "Ko"
  state.priority              = 100
  state.icon.index            = 17
  state.restriction           = 4
  state.auto_removal_timing   = 0
  state.min_turns             = 1
  state.max_turns             = 1
  state.chance_by_damage      = 100
  state.steps_to_remove       = 100
  state.remove_by_walking     = false
  state.remove_at_battle_end  = false
  state.remove_by_damage      = false
  state.remove_by_restriction = false
  state.features << MkFeature.exp_gain_r(0.0)
  @states[state.id] = state
#==============================================================================#
# ◙ State 2 (Poison)
#==============================================================================#
  state                       = State.new()
  state.initialize_add()
  state.id                    = 2
  state.name                  = "Poison"
  state.priority              = 65
  state.icon.index            = 18
  state.restriction           = 0
  state.auto_removal_timing   = 0
  state.min_turns             = 1
  state.max_turns             = 1
  state.chance_by_damage      = 100
  state.steps_to_remove       = 100
  state.remove_by_walking     = false
  state.remove_at_battle_end  = false
  state.remove_by_damage      = false
  state.remove_by_restriction = false
  state.features << MkFeature.hp_regen_r(-1.0)
  @states[state.id] = state
#==============================================================================#
# ◙ State 3 (Guard)
#==============================================================================#
  state                       = State.new()
  state.initialize_add()
  state.id                    = 3
  state.name                  = "Guard"
  state.priority              = 0
  state.icon.index            = 160 #0
  state.restriction           = 0
  state.auto_removal_timing   = 2
  state.min_turns             = 1
  state.max_turns             = 1
  state.chance_by_damage      = 100
  state.steps_to_remove       = 100
  state.remove_by_walking     = false
  state.remove_at_battle_end  = true
  state.remove_by_damage      = false
  state.remove_by_restriction = true
  state.features << RPG::BaseItem::Feature.new( 62, 1, 0 )
  @states[state.id] = state
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@states.size
    @states[i].id = i if @states[i]
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
