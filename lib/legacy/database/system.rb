#==============================================================================#
# ■ Database.system
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/06/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0
#==============================================================================#
class Database
def self.build_system()
  @system = RPG::System.new()
  @system.initialize_add()
  @system.japanese = false
  @system.currency_unit = "ERI"

  @system.terms.params[0]   = 'Max HP'
  @system.terms.params[1]   = 'Max SP'
  @system.terms.params[2]   = 'Strength'
  @system.terms.params[3]   = 'Vitality'
  @system.terms.params[4]   = 'Inteligence'
  @system.terms.params[5]   = 'Mentality'
  @system.terms.params[6]   = 'Agility'
  @system.terms.params[7]   = 'Luck'
  @system.terms.params_a[0] = 'MHP'#'Mhp'
  @system.terms.params_a[1] = 'MSP'#'Msp'
  @system.terms.params_a[2] = 'STR'#'Atk'
  @system.terms.params_a[3] = 'VIT'#'Def'
  @system.terms.params_a[4] = 'INT'#'Mat'
  @system.terms.params_a[5] = 'MEN'#'Mdf'
  @system.terms.params_a[6] = 'AGI'#'Agi'
  @system.terms.params_a[7] = 'LUK'#'Luck'

  # // E-Types
  @system.terms.etypes[Database::Helper.etype_id(:weapon)]   = 'Weapon'
  @system.terms.etypes[Database::Helper.etype_id(:shield)]   = 'Shield'
  @system.terms.etypes[Database::Helper.etype_id(:helmet)]   = 'Head'
  @system.terms.etypes[Database::Helper.etype_id(:body)]     = 'Body'
  @system.terms.etypes[Database::Helper.etype_id(:accessory)]= 'Accessory'
    # // (Ex)
  @system.terms.etypes[Database::Helper.etype_id(:arm)]      = 'Arms'
  @system.terms.etypes[Database::Helper.etype_id(:leg)]      = 'Legs'
  @system.terms.etypes[Database::Helper.etype_id(:crest)]    = 'Crest'

  # // Commands
  @system.terms.commands[0] = 'Fight'
  @system.terms.commands[1] = 'Escape'
  @system.terms.commands[2] = 'Attack'
  @system.terms.commands[3] = 'Guard'
  @system.terms.commands[4] = 'Item'
  @system.terms.commands[5] = 'Magic'
  @system.terms.commands[6] = 'Equip'
  @system.terms.commands[7] = 'Status'
  @system.terms.commands[8] = 'Formation'
  @system.terms.commands[9] = 'Save'
  @system.terms.commands[10]= 'Game End'
  @system.terms.commands[12]= 'Weapon'
  @system.terms.commands[13]= 'Armor'
  @system.terms.commands[14]= 'Important' # // Important Items
  @system.terms.commands[15]= 'Change'    # // Change Equip
  @system.terms.commands[16]= 'Optimize'  # // Best Equipment
  @system.terms.commands[17]= 'Clear'     # // Remove Equipment
  @system.terms.commands[18]= 'New Game'
  @system.terms.commands[19]= 'Continue'
  @system.terms.commands[20]= 'Shutdown'
  @system.terms.commands[21]= 'To Title'
  @system.terms.commands[22]= 'Cancel'

  # // 03/04/2012
  @system.terms.rogue[rogue_sym2id(:attack)]  = @system.terms.commands[2]
  @system.terms.rogue[rogue_sym2id(:guard)]   = @system.terms.commands[3]
  @system.terms.rogue[rogue_sym2id(:nudge)]   = 'Nudge'
  @system.terms.rogue[rogue_sym2id(:skip)]    = 'Skip'
  @system.terms.rogue[rogue_sym2id(:tame)]    = 'Tame'
  @system.terms.rogue[rogue_sym2id(:skill)]   = @system.terms.commands[5]
  @system.terms.rogue[rogue_sym2id(:item)]    = @system.terms.commands[4]
  @system.terms.rogue[rogue_sym2id(:status)]  = @system.terms.commands[7]
  @system.terms.rogue[rogue_sym2id(:hotkeys)] = 'Hotkeys'
  @system.terms.rogue[rogue_sym2id(:equip)]   = @system.terms.commands[6]
  @system.terms.rogue[rogue_sym2id(:list)]    = 'List'
  @system.terms.rogue[rogue_sym2id(:minimap)] = 'Minimap'
  @system.terms.rogue[rogue_sym2id(:options)] = 'Options'
  @system.terms.rogue[rogue_sym2id(:save)]    = @system.terms.commands[9]

  # // 02/23/2012
  @system.elements[0]       = ""
  @system.elements[element_id(:fire)]   = 'Flare'
  @system.elements[element_id(:water)]  = 'Aqua'
  @system.elements[element_id(:earth)]  = 'Land'
  @system.elements[element_id(:wind)]   = 'Atmos'
  @system.elements[element_id(:light)]  = 'Holy'
  @system.elements[element_id(:dark)]   = 'Shadow'
  # //
  @system.elements[element_id(:hp)]   = 'HP'
  @system.elements[element_id(:mp)]   = 'MP'
  @system.elements[element_id(:wt)]   = 'WT'

  @system.skill_types[0]    = nil
  @system.skill_types[Database::Helper.stype_id(:normal)] = 'Normal'
  @element_id.keys.each do |key|
    stype_id = Database::Helper.stype_id(key)
    @system.skill_types[stype_id]= @system.elements[@element_id[key]]
  end
  #@system.window_tone.set( -34, 64, 34 )
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
