#==============================================================================#
# ■ Database.tilesets
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/08/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#
#     ♣ 12/08/2011 V1.0
#         Added:
#           Tileset 0 (Default)
#==============================================================================#
class Database
  @passages = {}
  @passages[:impassable] = 0x000F
  @passages[:passable]   = 0x0000
  #Tileset = RPG::Tileset
def self.build_tilesets()
  #@tilesets = load_data("Data/Tilesets.rvdata2")
  @tilesets = []

#==============================================================================#
# ◙ Tileset 0 (Default)
#==============================================================================#
  tls = Tileset.new()
  tls.name             = "Outside"
  tls.tileset_names[0] = "Outside_A1"
  tls.tileset_names[1] = "Outside_A2"
  tls.tileset_names[2] = "Outside_A3"
  tls.tileset_names[3] = "Outside_A4"
  tls.tileset_names[4] = "Outside_A5"
  tls.tileset_names[5] = "Outside_B"
  tls.tileset_names[6] = "Outside_C"
  tls.flags[1536] = @passages[:impassable]
  @tilesets[0] = tls
#==============================================================================#
# ◙ Tileset 1 (Dungeon)
#==============================================================================#
  tls = Tileset.new()
  tls.name             = "Dungeon"
  tls.tileset_names[0] = "Dungeon_A1"
  tls.tileset_names[1] = "Dungeon_A2"
  tls.tileset_names[3] = "Dungeon_A4"
  tls.tileset_names[4] = "Dungeon_A5"
  tls.tileset_names[5] = "Dungeon_B"
  tls.tileset_names[6] = "Dungeon_C"
  tls.flags[1536] = @passages[:impassable]
=begin
  (TileManager.sym_autotile("floor5").to_a +
  TileManager.sym_autotile("floor6").to_a +
  TileManager.sym_autotile("floor7").to_a +
  TileManager.sym_autotile("floor13").to_a +
  TileManager.sym_autotile("floor14").to_a +
  TileManager.sym_autotile("floor15").to_a +
  TileManager.sym_autotile("floor21").to_a +
  TileManager.sym_autotile("floor22").to_a +
  TileManager.sym_autotile("floor29").to_a +
  TileManager.sym_autotile("floor30").to_a
  ).each { |i|
    tls.flags[i] = @passages[:impassable]
  }
=end
  t = 2048 + (48 * 20)
  for i in t...(t+48) ; tls.flags[i] = @passages[:impassable] ; end
  @tilesets[1] = tls
  @tilesets[2] = tls
  @tilesets[3] = tls
  @tilesets[4] = tls
  @tilesets.each { |tls| tls.tileset_names.each(&:downcase!) if tls }
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
