#==============================================================================#
# ■ Database.traps
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/18/2011
# // • Data Modified : 12/18/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/18/2011 V1.0
#==============================================================================#
class Database
  #Trap = RPG::Rogue::Trap
def self.build_traps()
  @traps = []
  button_route = proc {
    mvrt           = RPG::MoveRoute.new
    mvrt.repeat    = false
    mvrt.skippable = false
    mvrt.wait      = true
    lstmv = mvrt.list.pop
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_OFF )
    mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_DOWN )
    mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_LEFT )
    mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_RIGHT )
    mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_UP )
    mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_ON )
    mvrt.list << lstmv
    mvrt
  }
  finish_route = proc {
    mvrt           = RPG::MoveRoute.new
    mvrt.repeat    = false
    mvrt.skippable = false
    mvrt.wait      = true
    lstmv = mvrt.list.pop
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_OFF )
    mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_DOWN )
    mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_ON )
    mvrt.list << lstmv
    mvrt
  }
  spike_route = proc { |finish=false|
    mvrt           = RPG::MoveRoute.new
    mvrt.repeat    = false
    mvrt.skippable = false
    mvrt.wait      = true
    lstmv = mvrt.list.pop
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_OFF )
    if finish
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_UP )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_RIGHT )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_LEFT )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_DOWN )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    else
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_DOWN )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_LEFT )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [53] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_RIGHT )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [1] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_UP )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
    end
    mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_ON )
    mvrt.list << lstmv
    mvrt
  }
  null_route = proc {
    mvrt           = RPG::MoveRoute.new
    mvrt.repeat    = false
    mvrt.skippable = false
    mvrt.wait      = true
    mvrt
  }
  random_trap_route = proc { |finish=false|
    if finish
      mvrt           = null_route.call() #spike_route.call( true )
      lstmv = mvrt.list.pop
      mvrt.list << RPG::MoveCommand.new( ROUTE_SCRIPT, ["restore_trap()"] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_OFF )
      mvrt.list << RPG::MoveCommand.new( ROUTE_TURN_DOWN )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_DIR_FIX_ON )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [30] )
      mvrt.list << lstmv
    else
      mvrt           = button_route.call()
      lstmv = mvrt.list.pop
      mvrt.list << RPG::MoveCommand.new( ROUTE_SCRIPT, ["set_random_trap()"] )
      mvrt.list << RPG::MoveCommand.new( ROUTE_WAIT, [3] )
      mvrt.list << lstmv
    end
    mvrt
  }
#==============================================================================#
# ◙ Trap 0 (Dummy)
#==============================================================================#
  @traps[0] = Rogue::Trap.new()
#==============================================================================#
# ◙ Trap 1 (Heal I Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 1
  trap.name               = "Heal I Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 5
  trap.item_id            = 1
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 2 (Mana I Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 2
  trap.name               = "Mana I Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 5
  trap.item_id            = 6
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 3 (Meteor Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 3
  trap.name               = "Meteor Trap"
  trap.level              = 5
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 7
  trap.skill_id           = 22
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 4 (Spike Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 4
  trap.name               = "Spike Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 6
  trap.skill_id           = 2
  trap.trigger_limit      = -1
  trap.trigger_move_route = spike_route.call()
  trap.finish_move_route  = spike_route.call( true )
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 5 (Warp Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 5
  trap.name               = "Warp Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 5
  trap.skill_id           = 8
  trap.trigger_limit      = -1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = spike_route.call( true )
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 6 (Smog Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 6
  trap.name               = "Smog Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 3
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 5
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = spike_route.call( true )
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 7 (Freeze Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 7
  trap.name               = "Freeze Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 0
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = -1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = spike_route.call( true )
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 8 (Stone Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 8
  trap.name               = "Stone Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 2
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 9 (Bomb Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 9
  trap.name               = "Bomb Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 4
  trap.skill_id           = 15 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
parameters = ["Max HP", "Max SP", "Attack", "Defense", "Mag. Attack", "Mag. Defense", "Agility", "Luck"]
8.times do |i|
#==============================================================================#
# ◙ Trap 10+i (Stat- Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 10+i
  trap.name               = "#{parameters[i]}- Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 6
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 18+i (Stat+ Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 18+i
  trap.name               = "#{parameters[i]}+ Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 7
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
end
#==============================================================================#
# ◙ Trap 26 (Alarm Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 26
  trap.name               = "Alarm Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 3
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 27 (Fortress Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 27
  trap.name               = "Fortress Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 4
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 28 (Poison Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 28
  trap.name               = "Poison Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 2
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 29 (Curse Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 29
  trap.name               = "Curse Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet01(Wood)"
  trap.character_index    = 2
  trap.skill_id           = 8 # // Change
  trap.trigger_limit      = 1
  trap.trigger_move_route = button_route.call()
  trap.finish_move_route  = null_route.call()
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ Trap 30 (Random Trap)
#==============================================================================#
  trap                    = Rogue::Trap.new()
  trap.id                 = 30
  trap.name               = "Random Trap"
  trap.level              = 1
  trap.max_level          = TRAP_LEVELCAP
  trap.class_id           = 101
  trap.character_name     = "!TrapSet02(Wood)"
  trap.character_index    = 1
  trap.skill_id           = 8
  trap.trigger_limit      = -1
  trap.trigger_move_route = random_trap_route.call()
  trap.finish_move_route  = random_trap_route.call( true )
  @traps[trap.id]         = trap
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  for i in 0...@traps.size
    @traps[i].id = i if @traps[i]
  end
end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
