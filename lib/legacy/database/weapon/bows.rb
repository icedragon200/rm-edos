# // 02/29/2012
# // 02/29/2012
Database.construct(:weapons) do |ct|
  ct.stack!
  wep_sym = :bows
#==============================================================================#
# ◙ Weapon (Bow)(Shimis)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 1
    weapon.name         = "Shimis"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_bows)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:normal, :bows]
  end
#==============================================================================#
# ◙ Weapon (Bow)(Spineyard)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 2
    weapon.name         = "Spineyard"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_bows)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:magic, :bows]
  end
#==============================================================================#
# ◙ Weapon (Bow)(Archenel)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 3
    weapon.name         = "Archenel"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_bows)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:swift, :bows]
  end
#==============================================================================#
# ◙ Weapon (Bow)(Stadive)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 4
    weapon.name         = "Stadive"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_bows)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:heay, :bows]
  end
#==============================================================================#
# ◙ Weapon (Bow)(Acaed)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 5
    weapon.name         = "Acaed"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_bows)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:dual, :bows]
  end
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  ct.helper.adjust_weapons(ct.result, wep_sym)
  ct.result.compact.each do |weapon|
    weapon.atk_range.code     = MkRange::RANGE_DIAMOND
    weapon.atk_range.range    = 5
    weapon.atk_range.minrange = 3
  end
end
