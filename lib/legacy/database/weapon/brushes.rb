# // 02/29/2012
# // 02/29/2012
Database.construct(:weapons) do |ct|
  ct.stack!
  wep_sym = :brushes
#==============================================================================#
# ◙ Weapon (Brush)(Rumi)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 1
    weapon.name         = "Rumi"
    weapon.icon.index   = 0
    weapon.description  = 'A small artist brush'
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_brushes)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:normal, :brushes]
  end
#==============================================================================#
# ◙ Weapon (Brush)(Ruubri)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 2
    weapon.name         = "Ruubri"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_brushes)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:magic, :brushes]
  end
#==============================================================================#
# ◙ Weapon (Brush)(Sharoom)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 3
    weapon.name         = "Sharoom"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_brushes)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:swift, :brushes]
  end
#==============================================================================#
# ◙ Weapon (Brush)(Yeerust)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 4
    weapon.name         = "Yeerust"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_brushes)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:heay, :brushes]
  end
#==============================================================================#
# ◙ Weapon (Brush)(Swush)
#==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 5
    weapon.name         = "Swush"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_brushes)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:dual, :brushes]
  end
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  ct.helper.adjust_weapons(ct.result, wep_sym)
end
