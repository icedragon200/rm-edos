# // 02/29/2012
# // 02/29/2012
Database.construct(:weapons) do |ct|
  ct.stack!
  wep_sym = :knives

  ct.new_weapon do |weapon|
    weapon.id           = 1
    weapon.name         = "Knife"
    weapon.icon.index   = 0
    weapon.description  = 'Common pocket knife'
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_knives)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:normal, :knives]
  end

  ct.helper.adjust_weapons(ct.result, wep_sym)
end
