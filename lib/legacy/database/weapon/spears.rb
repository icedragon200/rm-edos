#
#
#
Database.construct(:weapons) do |ct|
  ct.stack!
  wep_sym = :spears
  #==============================================================================#
  # ◙ Weapon (Spear)(Partisan)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 1
    weapon.name         = "Partisan"
    weapon.icon.index   = 0
    weapon.description  = 'A very difficult spear'
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_spears)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:normal, :spears]
  end
  #==============================================================================#
  # ◙ Weapon (Spear)(Mermald)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 2
    weapon.name         = "Mermald"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_spears)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:magic, :spears]
  end
  #==============================================================================#
  # ◙ Weapon (Spear)(Hedgas)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 3
    weapon.name         = "Hedgas"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_spears)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:swift, :spears]
  end
  #==============================================================================#
  # ◙ Weapon (Spear)(Larzear)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 4
    weapon.name         = "Larzear"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_spears)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:heay, :spears]
  end
  #==============================================================================#
  # ◙ Weapon (Spear)(Disperas)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 5
    weapon.name         = "Disperas"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_spears)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:dual, :spears]
  end
  #==============================================================================#
  # ◙ REMAP
  #==============================================================================#
  ct.helper.adjust_weapons(ct.result, wep_sym)
  ct.result.compact.each do |weapon|
    weapon.atk_range.code     = MkRange::RANGE_LINE
    weapon.atk_range.range    = 2#2
    weapon.atk_range.minrange = 1
    weapon.effect_range.code  = MkRange::RANGE_LINE_TT
  end
end
