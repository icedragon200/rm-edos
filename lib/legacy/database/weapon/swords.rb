# // 02/29/2012
# // 02/29/2012
Database.construct(:weapons) do |ct|
  ct.stack!
  wep_sym = :swords
  #==============================================================================#
  # ◙ Weapon (Sword)(Lineage)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 1
    weapon.name         = "Lineage"
    weapon.icon.index   = 0
    weapon.description  = 'A mass produced sword, originally used by Emperor Linean'
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_swords)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:normal, :swords]
  end
  #==============================================================================#
  # ◙ Weapon (Sword)(Sulrive)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 2
    weapon.name         = "Sulrive"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_swords)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:magic, :swords]
  end
  #==============================================================================#
  # ◙ Weapon (Sword)(Fenseer)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 3
    weapon.name         = "Fenseer"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_swords)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:swift, :swords]
  end
  #==============================================================================#
  # ◙ Weapon (Sword)(Emperor)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 4
    weapon.name         = "Emperor"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_swords)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:heay, :swords]
  end
  #==============================================================================#
  # ◙ Weapon (Sword)(Shorlas)
  #==============================================================================#
  ct.new_weapon do |weapon|
    weapon.id           = 5
    weapon.name         = "Shorlas"
    weapon.icon.index   = 0
    weapon.description  = ''
    weapon.features     = []
    weapon.note         = ""
    weapon.price        = 0
    weapon.etype_id     = 0
    weapon.params       = ct.helper.params_from_growth(:base_swords)
    weapon.wtype_id     = 0
    weapon.animation_id = 0
    weapon.growths      = [:dual, :swords]
  end
#==============================================================================#
# ◙ REMAP
#==============================================================================#
  ct.helper.adjust_weapons(ct.result, wep_sym)
end
