#==============================================================================#
# ■ Database.weapons
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/07/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/07/2011 V1.0
#         Added:
#           Weapon 0 (Dummy)
#           Weapon 1 (<unnamed>)
#
#==============================================================================#
class Database

  def self.add_weapons(items)
    items.compact.each { |i| @weapons[i.id] = i }
  end

  def self.build_weapons
    @_weapon_param_blocks = {}
    @weapons = []
    #==========================================================================#
    # ◙ Weapon 0 (Dummy)
    #==========================================================================#
    weapon              = Weapon.new
    weapon.id           = 0
    weapon.name         = "Dummy Weapon"
    weapon.icon.index   = 0
    @weapons[weapon.id] = weapon

    weapons = Database.run_with_constructor(:weapons).collapse
    @weapons.concat(weapons)

    ### REMAP
    tmp_weapons = @weapons
    @weapons = []
    tmp_weapons.each do |weapon|
      next unless weapon
      weapon.rebuild_parameter_table( WEAPON_LEVELCAP ) do |p,l,o|
        Database::Helper.weapon_parameter(o, p, l)
      end
      8.times{|i|weapon.params[i] = weapon.level_params[i,1]}
      weapon.icon.iconset_name = "iconset_weapon"
      weapon.id_offset = 1000
      @weapons[weapon.id] = weapon
    end
    #File.open("WeaponNames.txt", "w+"){|f|
    #  @weapons.compact.each{|s|f.puts(format("%03d-%s", s.id, s.name))}
    #}
  end

end

require_relative 'weapon/spears'
require_relative 'weapon/brushes'
require_relative 'weapon/swords'
require_relative 'weapon/bows'
require_relative 'weapon/staves'
#require_relative 'weapons_chains'
#require_relative 'weapons_fans'

# Spear
# Sword
# Scythe
# Bow
# Brush
# Staff
# Bomb
# Hammer
# Knuckle
# Knife
# Fan
# Gun
# Chain
