[Window, Sprite, Bitmap, Viewport, Plane].each do |klass|
  eval(%Q(
    class #{klass}
      alias org_initialize initialize
      def initialize(*args)
        org_initialize(*args)
        self.made_by = caller.dup
      end

      attr_accessor :made_by
    end))
end

module EDOS
  module Debug

    def self.prep_logs
      Dir.mkdir('logs') unless Dir.exist?('logs')
    end

    def self.write_loaded_features_log
      prep_logs
      File.write('logs/loaded_features.log', $LOADED_FEATURES.join("\n"))
    end

    def self.write_bitmap_debug_log
      prep_logs
      bitmaps = {} # Hash<Class, Array<Bitmap>>
      Bitmap.bitmaps.each do |bmp|
        (bitmaps[bmp.class] ||= []).push(bmp)
      end
      bitmaps.values.each { |a| a.sort_by!(&:object_id) }
      File.write("logs/debug_bitmap.log", bitmaps.map do |(klass, bitmaps)|
        result = "CLASS: #{klass}\n" +
          bitmaps.map do |bitmap|
            #next if bitmap.is_a?(Cache::CacheBitmap)
            bitsize, channel_count = 8, 4
            b = bitmap.width * bitmap.height * bitsize * channel_count
            kb = b / 1024.0
            mb = kb / 1024.0
            str = "#{bitmap}\n" +
            "[DIMS: #{bitmap.width} x #{bitmap.height}]\n" +
            "[MEMSIZE: #{b} b || #{kb} kb || #{mb} mb]\n"
            str += "[CACHEKEY: #{bitmap.cache_key}]\n" if bitmap.is_a?(Cache::CacheBitmap)
            str += "[MADE BY: \n#{bitmap.made_by.join("\n").indent(2)}\n]\n"
          end.join("\n").indent(2)
        result
      end.compact.join("\n").indent(2))
    end

  end
end