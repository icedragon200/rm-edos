#
# EDOS/core/debug_server.rb
#   by IceDragon
#   dc 21/06/2013
#   dm 21/06/2013
# vr 0.0.1
Thread.abort_on_exception = true
$sinatra_thread = Thread.new do
  require 'sinatra'


## dc 22/06/2013
class Xeml

  def initialize
    @content  = []
  end

  def write(data)
    @content.push(data)
  end

  def embed(tag, params={})
    param_str = params.map { |(k, v)| "#{k}=#{v}" }.join(" ")
    tag_str = "<"
    tag_str.concat(tag)
    tag_str.concat(" " + param_str) unless param_str.empty?
    tag_str.concat(">")
    tag_close_str = "</#{tag}>"
    @content.push(tag_str)
    yield self
    @content.push(tag_close_str)
  end

  def render
    @content.join("\n")
  end

end

quick_xeml = -> do
  xeml = Xeml.new
  xeml.write("<!DOCTYPE html>")
  xeml
end

get '/ruby/symbols' do
  symbols = Symbol.all_symbols
  xeml = quick_xeml.()
  xeml.embed('html') do
    xeml.embed('body') do
      xeml.embed('h1') { xeml.write("Symbols") }
      xeml.embed('h2') { xeml.write("Count: %d" % symbols.size) }
      xeml.embed('ul') do
        symbols.each do |symbol|
          xeml.embed('li') { xeml.write(":" + symbol.to_s) }
        end
      end
    end
  end
  xeml.render
end

get '/graphics/canvas' do
  xeml = quick_xeml.()
  str.concat(Graphics.canvas.drawable.map do |obj|
    info = { id: obj.id,
             class_name: obj.class.name,
             pos: { x: obj.x, y: obj.y, z: obj.z },
             viewport: { viewport_id: obj.viewport ? obj.viewport.id : nil }
           }
    "<p>#{info}</p>"
  end.join("\n"))
  str.concat("</html>")
  str
end

get "/server/log" do
  xeml = quick_xeml.()
  str.concat("SERVER: #{EDOS.server.name}\n")
  EDOS.server.channels.each do |id, channel|
    str.concat("<div>\n")
    str.concat("<p>CHANNEL #{channel.id}: #{channel.name}</p>\n")
    str.concat(channel.log.map { |d| "<p>#{d}</p>" }.join("\n"))
    str.concat("</div>\n")
  end
  str.concat("</html>")
  str
end

#Sinatra::Application.run!
end