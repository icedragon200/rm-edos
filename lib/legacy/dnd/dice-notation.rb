#
# EDOS/src/dnd/dice-notation.rb
#   by IceDragon
#   dc 11/04/2013
#   dm 11/04/2013
# vr 1.0.0
module DnD

  class DiceError < TypeError
  end

  class Dice

    attr_accessor :seed
    attr_reader :sides

    def initialize(sides)
      @sides = sides.to_i
      @seed = Dice.seed
      @random = nil
    end

    def roll_abs
      @random ||= Random.new(@seed)
      @random.rand(@sides)
    end

    def roll
      roll_abs + 1
    end

    def self.seed
      @seed ||= rand(0xFFFF_FFFF)
    end

    def self.seed=(seed)
      @seed = seed
    end

  end

  def self.die_str_to_die(str)
    if str =~ /d(\d+)/i then Dice.new($1.to_i)
    else                     raise(DiceError, "Invalid die_str given")
    end
  end

  def self.die_str_to_roll(str)
  end

end

#dice = DnD.die_str_to_die('d20')
#p dice
#p dice.roll
