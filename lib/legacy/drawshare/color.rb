module DrawShare

  def change_color_ex(color, enabled=true)
    contents.font.color = color.dup
    contents.font.color.alpha = translucent_alpha unless enabled
  end

  def line_color
    col = Palette['droid_dark_ui_enb']
    col.alpha = 172#64
    #col.alpha = 48
    return col;
  end

  # // 03/05/2012
  def element_color(element_id)
    Palette["element#{element_id}"]
  end

end
