#
# EDOS/src/artist/drawshare/draw_icon.rb
#   by IceDragon
#   dc 30/03/2013
#   dm 30/03/2013
# vr 1.1.0
module DrawShare

  def draw_icon(icon, x, y, enabled=true)
    DrawExt.draw_icon(contents, icon, x, y, enabled)
  end

  def draw_icon_stretch(icon, trect, enabled=true)
    DrawExt.draw_icon_stretch(contents, icon, trect, enabled)
  end

  def draw_item_icon(item, x, y, enabled=true)
    icon = item ? item.icon : 0
    draw_icon(icon, x, y, enabled)
  end

  def draw_element_icon(element_id, x, y, enabled=true)
    DrawExt.draw_element_icon(contents, element_id, x, y, enabled)
  end

  def draw_entity_icons(entity, x, y, width = 96)
    windex = width / 24
    icons = (entity.state_icons + entity.buff_icons)[0, windex]
    bmp = Cache.system("status_stateborder(window)")
    for i in 0...windex
      contents.blt( x + 24 * i, y, bmp, bmp.rect )
    end
    icons.each_with_index do |n, i|
      draw_icon(n, x + 24 * i, y)
    end
  end

end