#
# EDOS/src/drawshare/entity_attr.rb
#   dc 06/05/2013
#   dm 06/05/2013
# vr 1.0.0
module DrawShare

  ## IEI::ArtsSystem
  # draw_entity_arts(REI::Entity entity, Rect rect)
  def draw_entity_arts(entity, rect)
    return unless entity.respond_to?(:arts)
    arts = entity.arts
    drect = rect.dup
    for i in 0...arts.size
      draw_art(entity, arts[i], drect)
      drect.y += drect.height
    end
    drect
  end

  def draw_entity_element(entity, x, y, enabled=true)
    draw_element_icon(entity.element_id, x, y, enabled)
  end

  def draw_entity_exp(entity, rect, use_text=true)
    exp  = entity.exp - entity.current_level_exp
    mexp = entity.next_level_exp - entity.current_level_exp

    rate = [exp, mexp]
    draw_gauge_ext_wtxt(rect, rate, 'exp', use_text)
  end

  def draw_entity_hp(entity, rect, use_text=true)
    rate = [entity.hp, entity.mhp]
    draw_gauge_ext_wtxt(rect, rate, 'hp', use_text)
  end

  def draw_entity_mp(entity, rect, use_text=true)
    rate = [entity.mp, entity.mmp]
    draw_gauge_ext_wtxt(rect, rate, 'mp', use_text)
  end

  def draw_entity_ap(entity, rect, use_text=true)
    rate = [entity.ap, entity.map]
    draw_gauge_ext_wtxt(rect, rate, 'ap', use_text)
  end

  def draw_entity_wt(entity, rect, use_text=true)
    rate = [entity.wt, entity.mwt]
    draw_gauge_ext_wtxt(rect, rate, 'wt', use_text)
  end

  def draw_entity_level(entity, rect)
    change_color(system_color)
    draw_text(rect, Vocab::level_a, 0)
    change_color(normal_color)
    draw_text(rect, entity.level, 2)
  end

  def draw_character_face(character, x, y, enabled = true)
    draw_face(character.face_name, character.face_index, x, y, enabled)
  end

  ##
  # draw_entity_element_rates(Entity entity, Rect rect, bool vertical, ALIGN align)
  def draw_entity_element_rates(entity, rect, vertical=false, align=0)
    div_color = Palette['droid_dark_ui_enb'].hset(alpha: 172)

    divs = {
      2 => {a: 1, r: 0.4, c: div_color},
      4 => {a: 1, r: 0.6, c: div_color},
      8 => {a: 1, r: 0.8, c: div_color}
    }

    for i in 0...6
      ei = i + 1
      rate  = entity.element_rate(ei)
      mrate = (rate / 2.0)
      c     = element_color(ei)
      c     = c.add((mrate - 1.0).clamp(0.0, 1.0))
      colors = DrawExt.quick_bar_colors(c, 1)

      xioff = 0
      yioff = 0
      drw_rect = rect.dup

      if vertical
        drw_rect.x += (drw_rect.width + 4) * i
        i32 = drw_rect.width >= 32
        i24 = drw_rect.width >= 24
      else
        drw_rect.y += (drw_rect.height + 4) * i
        i32 = drw_rect.height >= 32
        i24 = drw_rect.height >= 24
      end

      if i24
        DrawExt.enable_border if i32
        orct = draw_element_icon(ei, drw_rect.x, drw_rect.y, true)
        DrawExt.restore_border if i32
      else
        orct = drw_rect.dup
      end

      if vertical
        yioff = orct.height
      else
        xioff = orct.width
      end

      drw_rect.x += xioff
      drw_rect.y += yioff
      drw_rect.width -= xioff
      drw_rect.height -= yioff

      draw_gauge_ext(drw_rect, mrate, colors, vertical, align)
      DrawExt.draw_ruler(contents, drw_rect, divs, vertical, 0)
    end
  end

end
