module DrawShare

  def draw_entity_equip_at_ex(entity, index, rect, enabled=true)
    raise("nil entity") unless entity

    item = entity.equips[index]

    bmp_eqtab = Cache.system("equipment_tab3")

    bitmap = self.contents

    bitmap.blt(
      rect.x, rect.y, bmp_eqtab, bmp_eqtab.rect,
      enabled ? 255 : translucent_alpha)

    contents.font.snapshot do
      hrect = Rect.new(rect.x, rect.y + 2, bmp_eqtab.width, 14)
      bitmap.font.set_style('window_header')
      draw_text(hrect, Vocab.slot_name(entity, index), 1)

      if item
        exp_rect = Rect.new(rect.x + 32, rect.y + 16, 30, 6)
        itemname_rect = Rect.new(
          rect.x + 4, rect.y2 - 14, bmp_eqtab.width - 8, 14)

        bitmap.font.set_style('simple_black')

        draw_item_icon(item, rect.x + 4, rect.y + 19)
        draw_text(itemname_rect, item.name, 1)
        draw_item_small_exp(item, exp_rect)

        if ExDatabase.ex_weapon?(item)
          level_rect = exp_rect.dup
          level_rect.x += 2
          level_rect.height = 14
          level_rect.y += 18

          #bitmap.font.size = 13
          draw_text(level_rect, item.level_s)
        end
      end
    end

    return
  end

end
