module DrawShare

  def line_rect(x=0, y=0, w=0)
    return Rect.new(x, y, w, line_height)
  end

  def draw_unit_name(unit, rect)
    draw_text(Rect(rect), unit.name)
  end

end
