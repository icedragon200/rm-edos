__END__
module DrawShare

  SIMPLE_EXP = true

  def draw_battler_exp(battler, rect, use_text=true)
    x, y, width, height = rect.to_a

    exp  = battler.exp - battler.current_level_exp
    mexp = battler.next_level_exp - battler.current_level_exp
    exprate = exp / mexp.to_f

    if battler.level == battler.max_level
      exp = mexp ; use_text = false
    end

    colors = DrawExt::RUBY_BAR_COLORS

    rect = Rect.new(x, y, width, height - 6)
    DrawExt.draw_padded_rect_flat(
      self.contents, rect,
      colors[:base_outline1], colors[:base_inline1]
    )
    rect.contract!(anchor: 5, amount: 1)

    DrawExt.draw_gauge_specia1(
      self.contents, rect,
      colors[:bar_outline1], colors[:bar_outline2],
      colors[:bar_inline1], colors[:bar_inline2],
      colors[:bar_highlight], exprate
    )

    crexp, mlvexp = battler.exp, battler.max_level_exp
    crexp = crexp.min(mlvexp)

    rect = Rect.new(x, y + (height - 6), width, 6)

    DrawExt.draw_padded_rect_flat(
      self.contents, rect,
      colors[:base_outline1], colors[:base_inline1]
    )
    rect.contract!(anchor: 5, amount: 1)

    DrawExt.draw_gauge_specia3(
      self.contents, rect,
      [colors[:bar_outline1],
       colors[:bar_inline1],
       colors[:bar_outline2],
       colors[:bar_inline2]],
      crexp / mlvexp.to_f
    )

    if use_text
      drawing_sandbox do
        x += 4
        contents.font.size = Font.default_size - 4
        change_color( normal_color )
        text = SIMPLE_EXP ? format("%s/100", (exprate*100).to_i) : format("%s/%s", exp, mexp)
        draw_text( x, y, width, height, text, 1 )
      end
    end

  end

  # // 02/13/2012
  def draw_actor_equip_at_ex(actor, index, rect, enabled=true)
    return unless actor
    item = actor ? actor.equips[index] : nil
    w, h = rect.width, rect.height
    bmp = Bitmap.new(w, h)
    bmp.gui_draw_equip_header_base(
      x:      0,
      y:      0,
      width:  rect.width
      #//height: rect.height
    )
    contents.blt(rect.x,rect.y,bmp,bmp.rect,enabled ? 255 : translucent_alpha)
    bmp.dispose()
    hrect = rect.dup
    hrect.contract!(anchor: 46, amount: 12)
    contents.font.set_style( :window_header )
    draw_text( hrect.x, hrect.y, hrect.width, 14, Vocab.slot_name(index), 1 )
    return unless item
    drawing_sandbox() do
      rect2 = rect.dup
      contents.font.set_style( :simple_black )
      rect2.x += 6
      rect2.y += 14
      draw_item_icon( item, rect2.x, rect2.y )
      rect2.x += 26 + 4
      rect2.y += 3
      contents.font.size = Font.default_size - 8
      draw_text( rect2.x, rect2.y, rect2.width, 14, item.name )
      draw_item_small_exp(item,rect2.x-4,rect.y+38-6,rect.width-64)
    end
  end

end
