module DrawShare

  def draw_character_graphic(char, x, y)
    draw_character(char.character_name, char.character_index, x, y)
  end

  ## convinience
  # draw_unit_graphic
  def draw_unit_graphic(unit, x, y)
    draw_character_graphic(unit.character, x, y)
  end

end
