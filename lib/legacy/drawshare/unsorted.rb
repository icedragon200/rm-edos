# // 02/06/2012
# // 02/06/2012
module DrawShare

  def draw_menuicon(hsh)
    name    = hsh[:name]
    rect    = hsh[:rect]
    anchor  = hsh[:anchor] || 5
    opacity = hsh[:opacity] || 255

    bmp = Cache.system("menu-icons/#{name}")
    rect = bmp.rect.align_to(anchor: anchor, surface: rect)
    contents.blt(rect.x, rect.y, bmp, bmp.rect, opacity)

    rect = bmp.rect
    rect.x += x
    rect.y += y
    return rect
  end

  def draw_parameter_text(x,y,param_id,enabled=true)
    drawing_sandbox() do
      voc   = Vocab.param_a( param_id )
      contents.font.set_style('simple_brown1')
      #change_color_ex(contents.font.color,enabled)
      draw_text(x, y, 32, 12, voc, 0)
    end
    Rect.new(x, y, 32, 12)
  end

  def draw_parameter_base( x, y, param_id, enabled=true )
    bmp = Cache.system("status_statborder(window)")
    drawing_sandbox() do
      contents.blt(x + 32, y, bmp, bmp.rect, enabled ? 255 : translucent_alpha)
      draw_parameter_text(x, y, param_id, enabled)
    end
    Rect.new(x + 32, y, bmp.width, bmp.height)
  end

  def draw_parameter( obj, x, y, param_id, enabled=true )
    draw_parameter_i(obj ? obj.param( param_id ) : 0, x, y, param_id, enabled )
  end

  def draw_parameter_i( value, x, y, param_id, enabled=true )
    drawing_sandbox() do
      r = draw_parameter_base( x, y, param_id, enabled ).contract(anchor: 46, amount: 1)
      contents.font.set_style('simple_brown1')
      #change_color_ex(Palette['outline'], enabled)
      block_given? ? yield(r, value) : draw_text( r.x, r.y, r.width, r.height, value, 2 )
    end # // drawing_sandbox()
  end

  def draw_item_name(item, x, y, enabled = true, width = 172)
    return unless item
    drawing_sandbox() do
      draw_item_icon( item, x, y, enabled )
      change_color(normal_color, enabled)
      #contents.font.name = ["Microsoft YaHei"]
      draw_text(x + 24, y, width, line_height, item.name)
    end
  end

  def draw_tiny_character( info )
    x, y = info[:x] || 0, info[:y] || 0
    character_name  = info[:character_name]
    character_index = info[:character_index]
    character_hue   = info[:character_hue]
    if char = info[:character]
      character_name  ||= char.character_name
      character_index ||= char.character_index
      character_hue   ||= char.character_hue
    end

    return if !character_name || character_name.empty?
    bitmap = Cache.character(character_name, character_hue).dup
    sign = character_name[/^[\!\$]./]
    if sign && sign.include?('$')
      cw = bitmap.width / 3
      ch = bitmap.height / 4
    else
      cw = bitmap.width / 12
      ch = bitmap.height / 8
    end
    h = info[:height] || (ch * 0.8)
    n = character_index
    src_rect = Rect.new((n%4*3+1)*cw, (n/4*4)*ch, cw, h)
    contents.blt(x - cw / 2, y - ch, bitmap, src_rect)
    bitmap.dispose
    return src_rect
  end

  def draw_box(rect, out_color=Palette['brown1'], cont_color=line_color, pad=1)
    Rect.assert_type(rect)
    DrawExt.draw_box1(self.contents, rect, [out_color, cont_color], pad)
  end

  def draw_item_small_exp(item, rect)
    return unless ExDatabase.ex_equip_item?(item)

    colors = DrawExt::EXP_BAR_COLORS
    draw_gauge_ext(rect, item.exp_rate, colors)

    return rect;
  end

  # // 02/18/2012
  def draw_skill_ex(skill, rect, skill_border_id=0, entity=nil, enabled=true)
    w, h = rect.width, rect.height
    bmp = Bitmap.new(w, h)

    rects = DrawExt::Helper.default_seg_rects('skill_border', skill_border_id)
    DrawExt.repeat_bitmap_multi_seg(
      bmp, bmp.rect, Cache.system("skill_borders(window)"), rects, 1)

    contents.blt(rect.x,rect.y,bmp,bmp.rect,enabled ? 255 : translucent_alpha)
    bmp.dispose()
    contents.font.snapshot do
      return unless(skill && skill.id > 0)
      bat = entity
      rect2 = rect.dup #rect.squeeze_horz(24+16)
      rect2.width -= 72 #+ 40
      rect2.x += 40
      if(enabled) # // Draw the skill's icon
        draw_item_icon(skill, rect.x, rect.y, true)
      else # // Draw the disabled skill icon, the brown one . x .
        icon = RPG::BaseItem::Icon.new(16, Database.iconset_name(:elements))
        draw_icon(icon, rect.x, rect.y, true)
      end
      contents.font.set_style('window_header')
      rect3 = rect2.dup ; rect3.width -= 24
      change_color(contents.font.color, enabled)
      draw_text(rect3, skill.name, 1)
      #contents.font.outline = true
      #contents.font.out_color = contents.font.color
      contents.font.size += 2
      draw_skill_cost(skill, rect2, bat, enabled)
      icon = RPG::BaseItem::Icon.new(Game::Icon.cost(:mp), Database.iconset_name(:costs))
      draw_icon(icon, rect.x + rect.width - 24 - 6, rect.y, enabled)
      dy = 20 - h = 20 * bat.get_magiclearn_r(skill.id)
      contents.fill_rect(rect.x+25,rect.y+2+dy,3,h,Palette['sys1_orange'].hset(alpha:enabled ? 255 : translucent_alpha))
    end
  end

  def draw_skill_cost(skill, rect, entity=nil, enabled=enable?(skill))
    tp_cost = entity ? entity.skill_tp_cost(skill) : skill.tp_cost
    mp_cost = entity ? entity.skill_mp_cost(skill) : skill.mp_cost
    hp_cost = entity ? entity.skill_hp_cost(skill) : skill.hp_cost
    cost = [tp_cost, mp_cost, hp_cost].max
    if tp_cost > 0
      change_color(tp_cost_color, enabled)
    elsif mp_cost > 0
      change_color(mp_cost_color, enabled)
    elsif hp_cost > 0
      change_color(hp_cost_color, enabled)
    else
      change_color(normal_color, enabled)
    end
    draw_text(rect, cost, 2) if cost > 0
  end

  def draw_fract(x, y, t1, t2, w=56,h=line_height)
    draw_text(x, y, w, h, t1, 2)
    contents.fill_rect( x, y+h, w, 2, line_color )
    draw_text(x, y+h, w, h, t2, 2)
    Rect.new(x,y,w,h*2)
  end

  # //
  def draw_art(entity, item, rect, enabled=true)
    w, h = rect.width, rect.height

    bmp = Bitmap.new(w, h)
    rects = DrawExt::Helper.default_seg_rects('art')
    DrawExt.repeat_bitmap_multi_seg(
      bmp, bmp.rect, Cache.system("header_help(sprite)"), rects, 1)

    contents.blt(rect.x, rect.y, bmp, bmp.rect,
                 enabled ? 255 : translucent_alpha)
    bmp.dispose

    if item
      rect2 = rect.contract(anchor: MACL::Surface::ANCHOR_RIGHT, amount: 24)
      rect3 = rect2.dup

      draw_item_icon(item, rect.x, rect.y, enabled)
      contents.font.set_style('window_header')
      change_color(contents.font.color, enabled)
      draw_text(rect3, item.name, 1)
    end
  end

end
