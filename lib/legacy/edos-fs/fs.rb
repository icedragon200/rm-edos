#
# EDOS/src/edos-fs/fs.rb
#   by IceDragon
#   dc 13/04/2013
#   dm 13/04/2013
# vr 0.1.0
module EDOS
  #
end

class EDOS::FileSystem

  class Obj

    attr_accessor :id, :name, :path, :real_path

  end

  class Directory < Obj

    attr_accessor :contents

    def initialize
      super
      @contents = {}
    end

  end

  class File < Obj

    attr_accessor :data

  end

  class SymLink < Obj

    attr_accessor :sym_link

  end

  def initialize
    @dirs  = [] # Array<EDOS::FileSystem::Directory>
    @files = [] # Array<EDOS::FileSystem::File>
  end

  def mkdir(dirname)
    @dirs
  end

end

p fs = EDOS::FileSystem.new
