module ExDatabase
# // 03/04/2012 - Level System
  class Handler::Level

    WRAPS = [
      :level,:max_level,:max_level?,:change_level,:level_s,
      :current_level_exp,:next_level_exp,:max_level_exp,
      :exp,:change_exp,:inc_exp,:dec_exp,:exp_rate
    ]

    attr_reader :parent
    attr_reader :level
    attr_reader :max_level

    def initialize(parent, lvl, mxl)
      @parent = parent
      @level = lvl
      @max_level = mxl
    end

    def level_s
      sprintf( "-%s%d", Vocab.level_a, level )
    end

    def exp_for_level(level)
      parent.exp_for_level(level)
    end

    def init_exp
      @exp = current_level_exp
    end

    def exp
      @exp
    end

    def current_level_exp
      exp_for_level(@level)
    end

    def next_level_exp
      max_level? ? nil : exp_for_level( @level + 1 )
    end

    def max_level_exp
      exp_for_level( max_level-1 )
    end

    def exp_rate
      (exp - current_level_exp) / (next_level_exp - current_level_exp).to_f
    end

    def max_level
      @max_level
    end

    def max_level?
      @level >= max_level
    end

    def change_exp(exp, show)
      @exp = [exp, 0].max
      last_level = @level
      level_up while !max_level? && self.exp >= next_level_exp
      level_down while self.exp < current_level_exp
      #display_level_up(skills - last_skills) if show && @level > last_level
      parent.on_exp_change
    end

    def inc_exp(n)
      change_exp(exp+n,false)
    end

    def dec_exp(n)
      inc_exp(-n)
    end

    def change_level(level, show)
      level = [[level, max_level].min, 1].max
      change_exp(exp_for_level(level), show)
    end

    def level_up()
      @level += 1
      parent.on_level_change
    end

    def level_down()
      @level -= 1
      parent.on_level_change
    end

  end

end
