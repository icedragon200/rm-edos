# ExDatabase::Weapon
#==============================================================================#
# ♥ ExDatabase::Weapon
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
class ExDatabase::Weapon < ExDatabase::EquipItem

  def base_object
    $data_weapons[@item_id]
  end

  def is_weapon?
    true
  end

  def init_level
    super
    @max_level = Database::WEAPON_LEVELCAP
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
