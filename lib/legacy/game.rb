# // 02/19/2012
# // 02/19/2012
class Game
end

require_relative 'game/action.rb'

require_relative 'game/base_item.rb'
require_relative 'game/equip_item.rb'

require_relative 'game/battler.rb'
require_relative 'game/actors.rb'
require_relative 'game/guardian_spirit.rb'

require_relative 'game/character.rb'
require_relative 'game/vehicle.rb'
require_relative 'game/event.rb'
require_relative 'game/event_ex.rb'
require_relative 'game/player.rb'
require_relative 'game/player-fix.rb'
require_relative 'game/player_ex.rb'
require_relative 'game/follower.rb'
require_relative 'game/followers.rb'

require_relative 'game/interpreter.rb'
require_relative 'game/interpreter_ex.rb'

require_relative 'game/map.rb'
require_relative 'game/map_ex.rb'

require_relative 'game/hotkey.rb'
require_relative 'game/hotkeys.rb'

require_relative 'game/ai.rb'

require_relative 'game/inventory_base.rb'
require_relative 'game/inventory.rb'

require_relative 'game/message.rb'

require_relative 'game/party_base.rb'
require_relative 'game/party_base_ex.rb'
require_relative 'game/troop.rb'
require_relative 'game/party.rb'
require_relative 'game/party_ex.rb'

require_relative 'game/picture.rb'
require_relative 'game/pictures.rb'

require_relative 'game/rogue.rb'
require_relative 'game/rogue_ex.rb'
require_relative 'game/screen.rb'

require_relative 'game/self_switches.rb'
require_relative 'game/switches.rb'
require_relative 'game/variables.rb'

require_relative 'game/system.rb'
require_relative 'game/system_ex.rb'

require_relative 'game/settings.rb'

require_relative 'game/temp.rb'
require_relative 'game/timer.rb'

require_relative 'game/book.rb'
