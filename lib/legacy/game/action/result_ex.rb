# Game::ActionResult (Ex)
class Game::ActionResult
  
  attr_reader :xy_trigger
  attr_reader :xy_trigger_pos

  def clear
    clear_hit_flags
    clear_damage_values
    clear_status_effects
    clear_triggers
  end

  def clear_triggers()
    @xy_trigger = false
    @xy_trigger_pos = [0,0]
  end  

  def set_xy_trig( bool, x=0, y=0 )
    @xy_trigger = bool
    @xy_trigger_pos = [x, y]
  end  

end  
