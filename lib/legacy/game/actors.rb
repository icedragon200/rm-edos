#
# EDOS/src/game/actors.rb
#   by IceDragon
#   dc 11/05/2013
#   dm 11/05/2013
# vr 0.0.1
class Game::Actors

  include Enumerable

  def initialize
    @data = Array.new(13, nil)
    12.times { |i| init_actor(i) }
  end

  def each(&block)
    @data.each(&block)
  end

  def init_actor(unit_id)
    act = $data_actors[unit_id]
    return unless act
    @data[unit_id] = create_unit(act)
  end

  def [](unit_id)
    return nil unless unit_id
    init_actor(unit_id) unless @data[unit_id]
    return @data[unit_id]
  end

  def create_unit(actor)
    if REI::VERSION >= "2.0.0"
      unit = REI.create_unit_by_type(:actor) # REI2
    else
      unit = REI.create_unit(REI::EntityActor.new)
    end
    unit.setup(actor)
    unit
  end

  def size
    $data_actors.size
  end

end
