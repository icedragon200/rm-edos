# Game::AI
#==============================================================================#
# ♥ Game::AI
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/21/2011
# // • Data Modified : 12/21/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/21/2011 V1.0
#
#==============================================================================#
require_relative 'ai/constants.rb'
require_relative 'ai/class.rb'
require_relative 'ai/iq.rb'

class Game::AI

  attr_accessor :priorities
  attr_accessor :search_range
  attr_reader :iqs
  attr_reader :subject

  PRIORITIES = [[0, :offense], [1, :defense], [2, :support]]

  def initialize(subject)
    @subject      = subject
    @priorities   = Table.new(PRIORITIES.size) # Offensive, Defensive, Supportive
    @search_range = 15
    @iqs          = {}
  end

  def set_subject(subject)
    @subject = subject
    return self
  end

  def priotity_scores
    PRIORITIES.map { |pra| [@priorities[pra[0]], pra[1]] }
  end

  def lowest_priority
    (priotity_scores.min { |a, b| a[0] <=> b[0] })[1]
  end

  def highest_priority
    (priotity_scores.max { |a, b| a[0] <=> b[0] })[1]
  end

  def get_iq(code)
    return @iqs[code]
  end

  def set_iq(code, value)
    @iqs[code] = (get_iq(code) || IQ.new(0, 0)).set(code, value)
    return self
  end

  def has_iq?(code)
    return !@iqs[code].nil?()
  end

  def iq(code)
    i = get_iq(code)
    return i.nil? ? -1 : i.value
  end

  def clear_iqs
    @iqs.clear
    return self
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
