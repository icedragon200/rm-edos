# Game::AI (Class Methods)
#==============================================================================#
# ♥ Game::AI
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/21/2011
# // • Data Modified : 12/21/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/21/2011 V1.0
#
#==============================================================================#
class Game::AI

  def self.create_ai( type, perks=[] )

    ai = Game::AI.new( nil )
    case type
    when 0 # // Average
      ai.priorities[0] = 7
      ai.priorities[1] = 7
      ai.priorities[2] = 7
    when 1 # // High Offensive
      ai.priorities[0] = 9
      ai.priorities[1] = 7
      ai.priorities[2] = 7
    when 2 # // High Defensive
      ai.priorities[0] = 7
      ai.priorities[1] = 9
      ai.priorities[2] = 7
    when 3 # // High Supportive
      ai.priorities[0] = 7
      ai.priorities[1] = 7
      ai.priorities[2] = 9
    when 4 # // Guard Like
      ai.priorities[0] = 9
      ai.priorities[1] = 7
      ai.priorities[2] = 5
    when 5 # // Medic Like
      ai.priorities[0] = 9
      ai.priorities[1] = 5
      ai.priorities[2] = 7
    when 6 # // Fortress Like
      ai.priorities[0] = 7
      ai.priorities[1] = 9
      ai.priorities[2] = 5
    when 7 # // Doctor Like
      ai.priorities[0] = 5
      ai.priorities[1] = 9
      ai.priorities[2] = 7
    when 8 # // Wizard Like
      ai.priorities[0] = 7
      ai.priorities[1] = 5
      ai.priorities[2] = 9
    when 9 # // Healer Like
      ai.priorities[0] = 5
      ai.priorities[1] = 7
      ai.priorities[2] = 9
    else
      ai.priorities[0] = 10
      ai.priorities[1] = 10
      ai.priorities[2] = 10
    end

    if perks.include?( :smart )
      ai.set_iq( TRAP_IQ, 1 )      # // Avoid visible traps, that can retrigger
      ai.set_iq( SELF_PREV_IQ, 2 ) # // Keep distance from major threat
    elsif perks.include?( :mid_iq )
      ai.set_iq( TRAP_IQ, 0 )      # // Avoid all visible traps
      ai.set_iq( SELF_PREV_IQ, 1 ) # // Keep distance from mid threats
    elsif perks.include?( :fearful )
      ai.set_iq( TRAP_IQ, 0 )      # // Avoid all visible traps
      ai.set_iq( SELF_PREV_IQ, 0 ) # // Keep distance from any threat
    elsif perks.include?( :brainless )
    end

    if perks.include?( :trap_master )
      ai.set_iq( TRAP_IQ, 2 )      # // Avoid all traps (Inclusive of untriggered)
    end

    if perks.include?( :territory_guard )
      ai.set_iq( PATROL_IQ, 0 )    # // Only deal with room
    elsif perks.include?( :territory_watcher )
      ai.set_iq( PATROL_IQ, 1 )    # // Attack foes who are close to room
    elsif perks.include?( :territory_monster )
      ai.set_iq( PATROL_IQ, 2 )    # // Attack everyone, even other friends
    end

    ai.set_iq( AGGRO_IQ, 0 )       # // Normal
    if perks.include?( :hunter )
      ai.set_iq( AGGRO_IQ, 1 )     # // Head for strongest
    elsif perks.include?( :scavenger )
      ai.set_iq( AGGRO_IQ, 2 )     # // Head for weakest
    end

    if perks.include?( :party )
      ai.set_iq( PARTY_MOVE_IQ, 0 )   # // Stick close to the leader
      if perks.include?( :go_short )
        ai.set_iq( PARTY_MOVE_IQ, 1 ) # // Keep short distance between leader
      elsif perks.include?( :go_far )
        ai.set_iq( PARTY_MOVE_IQ, 2 ) # // Keep long distance between leader
      end
      ai.set_iq( PARTY_SUPPORT_IQ, 0 )# // Low Priority for party condition
      if perks.include?( :party_protect )
        ai.set_iq( PARTY_SUPPORT_IQ, 1 )# // High Priority for party condition
      end
    end

    if perks.include?( :heal )
      ai.set_iq( HEALING_IQ, 0 ) # // Heal self if you can
    elsif perks.include?( :heal_others )
      ai.set_iq( HEALING_IQ, 1 ) # // Heal friends if you can
    elsif perks.include?( :heal_all )
      ai.set_iq( HEALING_IQ, 2 ) # // Heal self and friends if you can
    end

    ai.set_iq( MOVE_IQ, 0 )      # // Find path, even if friend is blocking
    if perks.include?( :move_in_line )
      ai.set_iq( MOVE_IQ, 1 )    # // Choose shortest path even if blocked by friend
    end

    if perks.include?( :item_collect )
      ai.set_iq( PICKUP_IQ, 0 )  # // Will go ahead and pick up nearby items
    elsif perks.include?( :treasure_hunter )
      ai.set_iq( PICKUP_IQ, 1 )  # // Pickup items, even if its dangerous to do it
    end

    return ai
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
