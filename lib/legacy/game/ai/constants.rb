class Game::AI

  TRAP_IQ          = 1
  SELF_PREV_IQ     = 2
  PATROL_IQ        = 3
  AGGRO_IQ         = 4
  PARTY_MOVE_IQ    = 5
  PARTY_SUPPORT_IQ = 6
  HEALING_IQ       = 7
  MOVE_IQ          = 8
  PICKUP_IQ        = 9

end
