class Game::AI

  class IQ

    attr_accessor :code, :value

    def initialize(code, value)
      set( code, value )
    end

    def set(code, value)
      @code, @value = code, value
      return self
    end

    def ==(obj)
      return obj.is_a?(IQ) ? self._hashcode == obj._hashcode : false
    end

    def _hashcode
      return [@code, @value]
    end

  end

end
