# Game::BattlerBase (Ex)
#-// 19/06/2012
class Game::BattlerBase

  def initialize
    super
    pre_init_members
    init_members
    post_init_members
  end

  def pre_init_members
    @battler_id = 0
    @hp = @mp = @tp = 0
    @hidden = false
    @priority_type = 1
  end

  def init_members
    clear_param_plus
    clear_states
    clear_buffs
  end

  def post_init_members

  end

  def wat # // 02/20/2012 - Weapon Attack >_> I'll get around to this

  end

  def collide_with_characters?( x, y )
    super( x, y ) ||
      _map.characters_xy_nt( x, y ).any? { |c| c.normal_priority?() }
  end

  def equippable?(item)
    return false unless ExDatabase.equip_item?( item )
    return false if equip_type_sealed?(item.etype_id)
    return equip_wtype_ok?(item.wtype_id) if ExDatabase.weapon?(item)
    return equip_atype_ok?(item.atype_id) if ExDatabase.armor?(item)
    return false
  end

  def item_can?( item, op )
    case op
    when :use
      return usable?( item )
    when :equip
      return equippable?( item )
    when :set
      return !ExDatabase.equip_item?( item )
    when :drop
      return !item.nil? # //
    when :throw
      return !item.nil?
    end
    return true
  end

  # // 02/13/2012
  # // . x . Something Tactics Ogre Inspired,
  # // o3o It functions like an ATB but for a TBS O3O, XD, anyway its a countdown
  # // Thing that kills the need for a static turn list XD
  alias :wt_initialize :initialize

  def initialize(*args,&block)
    @wt = 0
    wt_initialize(*args,&block)
  end

  attr_accessor :wt

  def inc_wt(n=1)
    self.wt = (self.wt + n).clamp(0,mwt)
  end

  def dec_wt(n=1)
    inc_wt(-n)
  end

  def mwt
    # // Base WT - (Max WT Mod * agi / agi_cap)
    (500 - (200 * agi / 256.0)).to_i
  end

  def wt_rate
    self.wt.to_f / self.mwt.max(1)
  end

  def reset_wt
    self.wt = self.mwt
  end

  def wt_done?
    self.wt == 0
  end

  NULL_SKILL_ID = 100

  def base_skill_id sym
    @base_skill_ids[sym] || NULL_SKILL_ID
  end

  [:attack, :guard, :skip, :nudge, :tame, :move].each do |s|
    module_eval(%Q(
    def #{s}_skill_id
      base_skill_id(:#{s})
    end

    def #{s}_skill
      $data_skills[#{s}_skill_id]
    end))
  end

end
