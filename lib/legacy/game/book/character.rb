class Bestiary_Character < REI::BattlerUnit

  attr_accessor :sx, :sy

  def initialize
    @sx, @sy = 0, 0
    super
  end

  def screen_x
    return @sx + 16
  end

  def screen_y
    return @sy + 32
  end

  def screen_z
    return 100
  end

end

class Traptionary_Character < Game::Trap

  attr_accessor :sx, :sy

  def initialize
    @sx, @sy = 0, 0
    super(1)
  end

  def set_trap( trap_id )
    @battler_id  = trap_id
    @original_id = trap_id
    @through = true
    setup( @battler_id )
    trigger_trap([])
  end

  def screen_x
    return @sx + 16
  end

  def screen_y
    return @sy + 32
  end

  def screen_z
    return 100
  end

  def set_graphic(ch, chi)
    super(ch, chi, "", 0)
  end

end
