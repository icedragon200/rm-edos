# Game::CharacterBase (Ex)
class Game::CharacterBase
  # // 03/03/2012
  # // 03/03/2012
  def set_sprite_size(width,height,ox,oy)
    @spwidth, @spheight = width, height
    @spox, @spoy = ox, oy
  end  
  def spwidth
    @spwidth ||= 32
    @spwidth
  end  
  def spheight
    @spheight ||= 32
    @spheight
  end  
  def spox
    @spox ||= spwidth / 2
    @spox
  end  
  def spoy
    @spoy ||= spheight
    @spoy
  end  
  def sprect()
    @sprect ||= Rect.new(0,0,0,0)
    @sprect.set(screen_x-spox,screen_y-spoy,spwidth,spheight)
  end  
end  
