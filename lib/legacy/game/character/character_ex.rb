# Game::Character (Extend)
#==============================================================================#
# ♥ Game::Character
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/08/2011
# // • Data Modified : 12/08/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/08/2011 V1.0
#
#==============================================================================#
class Game::Character < Game::CharacterBase

  attr_accessor :camera_focused
  attr_writer :remove_this

  def character_hue()
    0
  end

  def character_id
    @id
  end

  def set_through( new_through )
    @through = new_through
  end

  def remove_this
    @remove_this = false if @remove_this.nil?()
    return @remove_this
  end

  def remove_sprite?
    return @remove_this == true
  end

  def pos_to_a()
    return x, y
  end

  def center_on!()
    center( x, y )
  end

  def focus_on()
    center_on!()
    @camera_focused = true
  end

  def focus_off()
    @camera_focused = false
  end

  #--------------------------------------------------------------------------
  # ● 画面中央の X 座標
  #--------------------------------------------------------------------------
  def center_x
    (Graphics.playwidth / 32 - 1) / 2.0
  end

  #--------------------------------------------------------------------------
  # ● 画面中央の Y 座標
  #--------------------------------------------------------------------------
  def center_y
    (Graphics.playheight / 32 - 1) / 2.0
  end

  #--------------------------------------------------------------------------
  # ● 画面中央に来るようにマップの表示位置を設定
  #--------------------------------------------------------------------------
  def center(x, y)
    _map.set_display_pos(x - center_x, y - center_y)
  end

  def update_scroll( last_real_x, last_real_y )
    ax1 = _map.adjust_x(last_real_x)
    ay1 = _map.adjust_y(last_real_y)
    ax2 = _map.adjust_x(@real_x)
    ay2 = _map.adjust_y(@real_y)
    _map.scroll_down (ay2 - ay1) if ay2 > ay1 && ay2 > center_y
    _map.scroll_left (ax1 - ax2) if ax2 < ax1 && ax2 < center_x
    _map.scroll_right(ax2 - ax1) if ax2 > ax1 && ax2 > center_x
    _map.scroll_up   (ay1 - ay2) if ay2 < ay1 && ay2 < center_y
  end

  def camera_focus?()
    return @camera_focused == true
  end

  def update()
    #@move_tween.update() if @move_tween
    focused = camera_focus?()
    if focused
      last_real_x = @real_x
      last_real_y = @real_y
      last_moving = moving?
    end
    super
    if focused
      update_scroll(last_real_x, last_real_y)
    end
  end

  def solid?
    return !@through && @priority_type == 1
  end

  def find_path(info)
    info[:sx] ||= self.x
    info[:sy] ||= self.y
    return _map.find_path( info )
  end

  def path_size(tx, ty)
    find_path( { :tx => tx, :ty => ty } ).size
  end

  #def update_move
  #  @move_tween ||= MACL::Tween.new( [0,0], [0,0], :sine_out, 1.0 )
  #  if @move_tween.end_values[0] != @x || @move_tween.end_values[1] != @y
  #    @move_tween.set_and_reset( [@real_x, @real_y], [@x, @y], :sine_inout,
  #      MACL::Tween.frame_to_sec((256*distance_per_frame*1.0)) )
  #  end
  #  @real_x, @real_y = *@move_tween.values
  #  @real_x, @real_y = @x, @y if @move_tween.done?
    #@real_x = [@real_x - distance_per_frame, @x].max if @x < @real_x
    #@real_x = [@real_x + distance_per_frame, @x].min if @x > @real_x
    #@real_y = [@real_y - distance_per_frame, @y].max if @y < @real_y
    #@real_y = [@real_y + distance_per_frame, @y].min if @y > @real_y
  #  update_bush_depth unless moving?
  #end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
