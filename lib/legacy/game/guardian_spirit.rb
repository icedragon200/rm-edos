# Game::GuardianSpirit
# // 03/06/2012 [
class Game::GuardianSpirit

  @@registered_ids = []

  class << self;alias :org_new :new;end

  def self.new(*args,&block)
    o = org_new(*args,&block)
    @@registered_ids << o.id
    iod = o.id
    # // Dipose O_O - Just in case you forgot to dispose it XD
    ObjectSpace.define_finalizer(o,proc{|id|free_id(iod)})
  end

  def self.mk_id(id=1)
    id += 1 while(@@registered_ids.include?(id))
    id
  end

  def self.free_id(id)
    @@registered_ids.delete(id)
  end

  # // >_> Yup yup -
  def dispose()
    Game::GuardianSpirit.free_id(@id)
    self
  end

  attr_accessor :name

  def initialize()
    @name      = ""
    @class_id  = 0
    @alignment = 1 # // Neutral
    init()
  end

  alias :real_class :class # // O_O Yeah...

  def init()
    init_id()
    init_class()
    init_level()
    init_exp()
  end

  def init_id()
    @id = self.real_class.mk_id(1)
  end

  # // Class
  attr_reader :class_id

  def class_id=(n)
    @class_id = n
  end

  def class
    $data_spirits[@class_id]
  end

  def init_class()
    @class_id = 1
  end

  # // Level System + Exp System . x .
  ExDatabase::Handler::Level::WRAPS.each do |sym|
    module_eval("def #{sym}(*a,&b);@level_handler.#{sym}(*a,&b);end")
  end

  def exp_for_level(level)
    self.class.exp_for_level(level)
  end

  def on_exp_change()
  end

  def on_level_change()
  end

  def init_level()
    @level_handler = ExDatabase::Handler::Level.new(self,1,20)
  end

  def init_exp()
    @level_handler.init_exp()
  end

  private :init
  private :init_id
  private :init_class
  private :init_level
  private :init_exp

end
# // ]
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
