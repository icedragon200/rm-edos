# Game::HotKeys
#==============================================================================#
# ♥ Game::HotKeys
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/24/2011
# // • Data Modified : 12/24/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/24/2011 V1.0
#
#==============================================================================#
class Game::HotKeys
  attr_reader :keys
  attr_reader :subject
  def initialize( subject )
    @subject = subject
    @keys = RogueManager::HOT_KEYS.map { |k| Game::HotKey.new( k ) }
  end
  def set_subject( subject )
    @subject = subject
    return self
  end
  def key_hash
    Hash[@keys.map { |k| [k.key, k] }]
  end
  def size
    return @keys.size
  end
  def []( index )
    return @keys[index]
  end
  def to_a
    @keys.dup
  end
  def get_hotkey( key )
    @keys.find { |k| k.key == key }
  end
  def set_hotkey( key, obj )
    get_hotkey( key ).set_obj( obj )
  end
  def obj_set?( obj )
    return nil if obj.nil?() || obj.id == 0
    return @keys.find { |k| k.object == obj }
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
