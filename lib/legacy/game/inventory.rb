# Game::Inventory
#==============================================================================#
# ♥ Game::Inventory
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/09/2012
# // • Data Modified : 01/09/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/09/2012 V1.0
#==============================================================================#
class Game::Inventory < Game::InventoryBase

  attr_reader :subject

  def initialize( subject )
    set_subject(subject)
    @ex_weapons = {}
    @ex_armors = {}
    super()
  end

  def set_subject( subject )
    @subject = subject
    self
  end

  def ex_symbols
    [:ex_weapon, :ex_armor]
  end

  def ex_item?( item )
    ExDatabase.ex?(item)
  end

  def ex_symbol?(symbol)
    ex_symbols.include?(symbol)
  end

  #def item_symbol( item )
  #  return :ex_weapon if item.class == ExDatabase::Weapon
  #  return :ex_armor if item.class == ExDatabase::Armor
  #  super( item )
  #end

  def item_container(item)
    return @ex_weapons if item.is_a?(ExDatabase::Weapon)
    return @ex_armors if item.is_a?(ExDatabase::Armor)
    super(item)
  end

  def containers()
    super + [@ex_weapons, @ex_armors]
  end

  def make_item( info )
    ex_symbol?( info[0] ) ? info[1] : super( info )
  end

  def item_ref_id( item )
    ex_item?( item ) ? item : super( item )
  end

  def items()
    super
  end

  def consumable_items()
    super
  end

  def weapons()
    super + @ex_weapons.keys.select{|a|has_item?(a)}.sort_by{|a|a.id}
  end

  def armors()
    super + @ex_armors.keys.select{|a|has_item?(a)}.sort_by{|a|a.id}
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
