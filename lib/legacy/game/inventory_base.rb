# Game::InventoryBase
#==============================================================================#
# ♥ Game::InventoryBase
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 12/11/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/11/2011 V1.0
#
#==============================================================================#
class Game::InventoryBase

  attr_reader :active_objs

  def initialize()
    @items   = {}
    @weapons = {}
    @armors  = {}
    #@removed = []
    limit!()
    resize(20)
  end

  def resize( new_size )
    @size = new_size
    @active_objs = Array.new( @size ) { [:nil, 0] }
  end

  def size()
    return @no_limit ? all_items.size : @size
  end

  def no_limit!()
    @no_limit = true
  end

  def limit!()
    @no_limit = false
  end

  def clear()
    @items.clear()
    @weapons.clear()
    @armors.clear()
    refresh()
    #@removed.clear()
  end

  def items()
    return @items.keys.sort.map { |i| $data_items[i] }
  end

  def consumable_items()
    return items.select { |i| i.consumable }
  end

  def weapons()
    return @weapons.keys.sort.map { |i| $data_weapons[i] }
  end

  def armors()
    return @armors.keys.sort.map { |i| $data_armors[i] }
  end

  def equip_items()
    return weapons + armors
  end

  def all_items_iv()
    return items + equip_items()
  end

  def all_items()
    return @no_limit ? all_items_iv : @active_objs.map { |a| make_item( a ) }
  end

  def item_symbol( item )
    Game::CraftSystem.item_symbol(item)
  end

  def make_item( info )
    return $data_items[info[1]]   if info[0] == :item
    return $data_weapons[info[1]] if info[0] == :weapon
    return $data_armors[info[1]]  if info[0] == :armor
    return nil
  end

  def item_container( item )
    return @items   if item.class == RPG::Item
    return @weapons if item.class == RPG::Weapon
    return @armors  if item.class == RPG::Armor
    return nil
  end

  def containers()
    return @items, @weapons, @armors
  end

  def refresh()
    containers.each{|c|keys=c.keys;keys.each{|k|c.delete(k) unless c[k] > 0}}
    active_items = all_items
    (all_items_iv-all_items).compact.each { |i|
      slot = empty_slots.shift
      if slot
        set_item( slot, i )
      else
        #@removed << [i, item_number(i)]
        lose_item( i, item_number(i) )
      end
    }
  end

  def item_ref_id( item )
    item ? item.id : 0
  end

  def get_item( index )
    item = make_item( @active_objs[index] )
    return item_number(item) > 0 ? item : nil
  end

  def set_item( index, item=nil )
    @active_objs[index] = [item_symbol( item ), item_ref_id( item )]
  end

  def unset_item( item )
    for i in 0...@active_objs.size
      a = @active_objs[i]
      set_item(i, nil) if (a[0] == item_symbol( item ) && a[1] == item_ref_id(item))
    end
  end

  def empty_slots()
    return (0...size).to_a.inject([]) { |result, i|
      result << i unless get_item( i ) ; result
    }
  end

  def item_number(item)
    container = item_container(item)
    container ? get_container_number(container, item) : 0
  end

  def max_item_number(item)
    return Database::MAX_ITEM_NUMBER
  end

  def max_item_number?( item )
    return true if item_number( item ) == max_item_number( item )
  end

  def get_container_number( container, item )
    container[item_ref_id(item)] || 0
  end

  def set_container_number( container, item, n )
    container[item_ref_id(item)] = n
  end

  def remove_item_from_container( container, item )
    return unless item
    container.delete(item_ref_id(item))
  end

  def gain_item( item, amount )
    container = item_container( item )
    return unless container
    last_number = item_number(item)
    new_number = last_number + amount
    set_container_number(container,item,[[new_number, 0].max, max_item_number(item)].min)
    if container[item.id] == 0
      remove_item_from_container( container, item )
      unset_item( item )
    end
    yield(new_number) if(block_given?())
    refresh()
  end

  def lose_item( item, n )
    gain_item( item, -n )
  end

  def consume_item( item )
    lose_item( item, 1 ) if item.is_a?(RPG::Item) && item.consumable
  end

  def has_item?(item)
    return true if item_number(item) > 0
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
