# Game::Map (Ex)
# // 02/14/2012
# // 02/14/2012
class Game::Map
  attr_reader :passage_table

  def setup_passage_table
    @passage_table = Table.new( width, height )
    for x in 0...@passage_table.xsize
      for y in 0...@passage_table.ysize
        @passage_table[x, y] = check_passage(x, y, 0x01) ? 1 : 0
      end
    end
  end

  alias :ps_tb_setup :setup

  def setup(*args,&block)
    ps_tb_setup(*args,&block)
    setup_passage_table()
  end

  def map_pass?(x, y)
    return @passage_table[x, y] == 1
  end

  def pass?(*args,&block)
    map_pass?(*args,&block)
  end

  def impassable?(x,y,d=5) # // 02/19/2012
    !passable?(x,y,d)
  end

  def characters
    []#events.values
  end

  def make_passage_table_ex( info={} )
    charas    = info[:characters].nil?() ? false : info[:characters]
    ignore_xy = info[:ignore_xy] || []
    forced_xy = info[:forced_xy] || []
    tab       = @passage_table.dup
    igtab     = Table.new( width, height )
    ignore_xy.each { |a| igtab[a[0], a[1]] = 1 }
    (events.values+[$game.player]).each { |ch|
      tab[ch.x, ch.y] = ch.solid? ? 0 : 1 unless igtab[ch.x, ch.y] == 1
    } if charas
    forced_xy.each { |a| tab[a[0], a[1]] = a[2] }
    return tab
  end

  def find_path( info )
    tx = info[:tx]
    ty = info[:ty]
    pstb = info[:passage_table] || make_passage_table_ex(
      characters: true,
      ignore_xy: (info[:ignore_xy] || []) + [[tx, ty]],
      forced_xy: info[:forced_xy] || []
    )
    return PathFinding.find_path(info)
  end

end
