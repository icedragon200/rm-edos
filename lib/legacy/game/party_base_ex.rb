#
# EDOS/src/game/PartyBase_Ex.rb
#   dm 19/05/2013
# vr 1.0.0
class Game::PartyBase

  def member_levels
    members.map { |m| m.entity.level }
  end

  def lowest_level
    lv = member_levels.min
  end

  def highest_level
    lv = member_levels.max
  end

  def average_level
    member_levels.inject(:+).div(members.size)
  end

  def random_level
    member_levels.sample
  end

  def index(unit)
    members.index(unit)
  end

end
