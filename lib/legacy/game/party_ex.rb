# Game::Party (Ex)
# // 02/29/2012
# // 02/29/2012
class Game::Party

  # // Inventory Wrapper . x .
  def inventory
    if @inventory.nil?
      @inventory = Game::Inventory.new(self)
      @inventory.no_limit!
    end
    @inventory
  end

  def items
    inventory.items
  end

  def weapons
    inventory.weapons
  end

  def armors
    inventory.armors
  end

  def item_container(*args,&block)
    inventory.item_container(*args,&block)
  end

  def item_number(*args,&block)
    inventory.item_number(*args,&block)
  end

  def max_item_number(*args,&block)
    inventory.max_item_number(*args,&block)
  end

  def max_item_number?(*args,&block)
    inventory.max_item_number?(*args,&block)
  end

  def gain_item(item, amount, include_equip = false)
    inventory.gain_item(item, amount) do |new_number|
      if include_equip && new_number < 0
        discard_members_equip(item, -new_number)
      end
      $game.map.need_refresh = true
    end
  end

  # // Debug!
  def gain_all_this(items)
    items.compact.each{|i|gain_item(i,max_item_number(i)) if i.id > 0}
  end

  def gain_all_items
    gain_all_this($data_items)
  end

  def gain_all_weapons
    gain_all_this($data_weapons)
  end

  def gain_all_armors
    gain_all_this($data_armors)
  end

  def gain_all_equips
    gain_all_weapons
    gain_all_armors
  end

  def gain_all_arts
    (1...40).each do |art_id|
      gain_art(art_id, 1)
    end
  end

  def setup_debug_party
    # Gain all items/equipment/arts
    gain_all_items
    gain_all_equips
    gain_all_arts

    # Equipment Optimization
    members.each { |m| m.entity.optimize_equipments }

    # Arts Equip
    artsys = IEI::ArtsSystem
    dark   = Database::Lookup.by_tags(artsys.arts, "element", "dark",  "lv1")
    earth  = Database::Lookup.by_tags(artsys.arts, "element", "earth", "lv1")
    fire   = Database::Lookup.by_tags(artsys.arts, "element", "fire",  "lv1")
    light  = Database::Lookup.by_tags(artsys.arts, "element", "light", "lv1")
    water  = Database::Lookup.by_tags(artsys.arts, "element", "water", "lv1")
    wind   = Database::Lookup.by_tags(artsys.arts, "element", "wind",  "lv1")

    members[0].entity.equip_arts(*(fire + light))
    members[1].entity.equip_arts(*(wind + dark))
    members[2].entity.equip_arts(*(water + earth))

    members.each{ |m| m.entity.equip_available_skills }
  end

end
