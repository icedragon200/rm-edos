# Game::Player (Ex)
# // 02/14/2012
# // 02/18/2012
class Game::Character
  def force_path(tx,ty)
    path = find_path(
      :tx => tx,
      :ty => ty,
      :as_route => true,
      :diagnol => false
    )
    # The path retrieved is actually backwards, so it must be reversed
    path.reverse!
    # Add an end ccommand
    path.push(RPG::MoveCommand.new(0))
    move_route = RPG::MoveRoute.new
    move_route.list = path
    move_route.repeat = false
    force_move_route(move_route)
    return !path.empty?
  end
end
class Game::Player
  def encounter
    return
  end
  def move_by_input
    return if _map.interpreter.running?

    return if !movable?
    if Input.dir4 > 0
      move_straight(Input.dir4)
    end
  end

end

# // 02/18/2012
class Game::System

  def move_target
    @move_target ||= MACL::Point.new(nil,nil)
    @move_target
  end

end
