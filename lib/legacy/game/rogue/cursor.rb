# Game::RogueCursor
#==============================================================================#
# ♥ Game::RogueCursor
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/18/2011
# // • Data Modified : 12/18/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/18/2011 V1.0
#
#==============================================================================#
class Game::RogueCursor
  attr_accessor :x, :y
  attr_accessor :opacity
  attr_accessor :active
  attr_accessor :bitmap_name
  attr_accessor :cursor_index
  attr_accessor :sprite_effect_type
  attr_reader :real_x, :real_y
  def initialize()
    @x, @y = 0, 0
    @real_x, @real_y = 0, 0
    @move_speed  = 5
    @visible     = true
    @opacity     = 255
    @active      = false
    @bitmap_name = "Cursors"
    @cursor_index= 0
    set_sprite_effect_type( nil )
  end
  def set_sprite_effect_type( type )
    @sprite_effect_type = type
    return self
  end
  attr_reader :visible
  def visible=( visible )
    @visible = visible
    set_sprite_effect_type( @visible ? :appear : :dissapear )
  end
  def moveto( x, y )
    @x = x % _map.width
    @y = y % _map.height
    @real_x = @x
    @real_y = @y
    RogueManager.call_handler( :on_cursor_move )
    set_sprite_effect_type( :appear ) if @visible
  end
  def moving?
    @real_x != @x || @real_y != @y
  end
  def real_move_speed
    @move_speed
  end
  def distance_per_frame
    2 ** real_move_speed / 256.0
  end
  def reverse_dir(d)
    return 10 - d
  end
  def update()
    focused = camera_focus?()
    if focused
      last_real_x = @real_x
      last_real_y = @real_y
      last_moving = moving?
    end
    @cursor_index = _map.passable?(@x, @y, 5) ? 0 : 1
    update_input() if active
    update_move() if moving?()
    if focused
      update_scroll(last_real_x, last_real_y)
    end
  end
  def update_input()
    return if moving?
    d = Input.dir4
    return unless d > 0
    mx = _map.round_x_with_direction(@x, d)
    my = _map.round_y_with_direction(@y, d)
    if _map.valid?( mx, my )
      @x, @y = mx, my#[[mx, _map.width-1].min, 0].max, [[my, _map.height-1].min, 0].max
      @real_x = _map.x_with_direction(@x, reverse_dir(d))
      @real_y = _map.y_with_direction(@y, reverse_dir(d))
      RogueManager.call_handler( :on_cursor_move )
    end
  end
  def update_move
    @real_x = (@real_x - distance_per_frame).max(@x) if @x < @real_x
    @real_x = (@real_x + distance_per_frame).min(@x) if @x > @real_x
    @real_y = (@real_y - distance_per_frame).max(@y) if @y < @real_y
    @real_y = (@real_y + distance_per_frame).min(@y) if @y > @real_y
  end
  def _map
    return $game.rogue
  end
  def screen_x
    _map.adjust_x(@real_x) * 32 + 16
  end
  def screen_y
    _map.adjust_y(@real_y) * 32 + 32
  end
  def screen_z
    1
  end
  def mm_x
    self.real_x #+ (@add_x / 32.0)
  end
  def mm_y
    self.real_y #+ (@add_y / 32.0)
  end
  def center_on!()
    center( x, y )
  end
  def focus_on()
    center_on!()
    @camera_focused = true
  end
  def focus_off()
    @camera_focused = false
  end
  def center_x
    (Graphics.playwidth / 32 - 1) / 2.0
  end
  def center_y
    (Graphics.playheight / 32 - 1) / 2.0
  end
  def center(x, y)
    _map.set_display_pos(x - center_x, y - center_y)
  end
  def update_scroll( last_real_x, last_real_y )
    ax1 = _map.adjust_x(last_real_x)
    ay1 = _map.adjust_y(last_real_y)
    ax2 = _map.adjust_x(@real_x)
    ay2 = _map.adjust_y(@real_y)
    _map.scroll_down (ay2 - ay1) if ay2 > ay1 && ay2 > center_y
    _map.scroll_left (ax1 - ax2) if ax2 < ax1 && ax2 < center_x
    _map.scroll_right(ax2 - ax1) if ax2 > ax1 && ax2 > center_x
    _map.scroll_up   (ay1 - ay2) if ay2 < ay1 && ay2 < center_y
  end
  def camera_focus?()
    return @camera_focused == true
  end
  def subject?()
    return false
  end
  def mm_bobbing?()
    return true
  end
  def mm_bob_amount()
    return 6
  end
  def remove_sprite?()
    return false
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
