# Game::RogueEvent
# // 01/20/2012
# // 01/20/2012
class Game::RogueEvent < Game::Event
  include Mixin::RogueCharacter
  def _map
    return $game.rogue
  end
end
