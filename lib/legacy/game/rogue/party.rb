# Game::RogueParty
# // 02/28/2012
# // 02/28/2012
class Game::RogueParty < Game::PartyBase

  def initialize()
    super()
    @members = []
  end

  attr_accessor :members

  def clear_members()
    @members.clear()
  end

  def add_member(battler)
    @members << battler unless @members.include?(battler)
  end

  def remove_member(battler)
    @members.delete(battler)
  end

  # // 02/29/2012
  def add_members(*mems)
    mems.each{|m|add_member(m)}
  end

  def remove_members(*mems)
    mems.each{|m|remove_member(m)}
  end

end
