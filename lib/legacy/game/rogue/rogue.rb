# Game::Rogue
#==============================================================================#
# ■ Game::Rogue (REI-Rogue Engine Icy)
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/11/2011
# // • Data Modified : 01/20/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/11/2011 V1.0
#     ♣ 12/17/2011 V1.0
#         Added
#           Traps
#     ♣ 01/20/2012 V1.0
#         Added
#           Few changes to fix problems with the Interpreter
#
#==============================================================================#
class Game::Rogue < Game::Map

  attr_accessor :items_need_refresh
  attr_accessor :trap_ids
  attr_reader :traps
  attr_reader :items
  attr_reader :turn_order
  attr_reader :phase
  attr_reader :cursor
  attr_reader :flash_data

  def initialize
    super
    @cursor = Game::RogueCursor.new
  end

  def interpreter_class
    Game::RogueInterpreter
  end

  def activate_cursor
    @cursor.active  = true
  end

  def deactivate_cursor
    @cursor.active  = false
  end

  def show_cursor
    @cursor.visible = true
  end

  def hide_cursor
    @cursor.visible = false
  end

  def events
    @events.values
  end

  def characters
    @characters
  end

  def objects
    events + characters + traps
  end

  def characters_at_cursor()
    characters_xy( @cursor.x, @cursor.y )
  end

  def characters_at_cursor_nt()
    characters_xy_nt( @cursor.x, @cursor.y )
  end

  def static_character_table()
    @character_table = IEI::ArrayTable.new( width, height ) { Array.new }
    @characters.each { |c| (@character_table[c.x, c.y] ||= []).push( c ) }
  end

  def release_character_table()
    @character_table.reset!() if @character_table
    @character_table = nil
  end

  def characters_xy( x, y )
    @character_table ? @character_table[x, y] : @characters.select {|event| event.pos?(x, y) }
  end

  def characters_xy?( x, y )
    characters_xy( x, y ).empty?
  end

  def characters_xy_nt(x, y)
    chars = @character_table ? @character_table[x, y] : @characters
    return(chars.select { |event| event.pos_nt?(x, y) })
  end

  def characters_xy_nt?( x, y )
    characters_xy_nt( x, y ).empty?
  end

  def characters_xyl(list)
    c = !(@character_table == nil)
    static_character_table() unless c
    chs = list.map{|a|characters_xy(*a)}
    release_character_table() unless c
    chs.flatten
  end

  def characters_alive()
    @characters.select{|c|c.alive?}
  end

  def characters_dead()
    @characters.select{|c|c.dead?}
  end

  def traps_xy( x, y )
    @traps.select { |t| t.pos?( x, y ) }
  end

  def objects_xy( x, y )
    return traps_xy( x, y ) + characters_xy( x, y ) + events_xy( x, y )
  end

  def objects_xy_nt( x, y )
    return traps_xy( x, y ) + characters_xy_nt( x, y ) + events_xy_nt( x, y )
  end

  def items_xy( x, y )
    return @items.select { |i| i.pos?( x, y ) }
  end

  def all_xy( x, y )
    return objects_xy( x, y ) + items_xy( x, y )
  end

  def remove_items( items )
    items.each { |i|
      i.remove_this = true
      @items.delete(i)
    }
  end

  def visible_traps
    @traps.select { |t| t.triggered? }
  end

  def make_passage_table_ex( info={} )
    charas    = info[:characters].nil?() ? false : info[:characters]
    ignore_xy = info[:ignore_xy] || []
    forced_xy = info[:forced_xy] || []
    tab       = @passage_table.dup
    igtab     = Table.new( width, height )
    ignore_xy.each { |a| igtab[a[0], a[1]] = 1 }
    characters.each { |ch|
      tab[ch.x, ch.y] = ch.solid? ? 0 : 1 unless igtab[ch.x, ch.y] == 1
    } if charas
    forced_xy.each { |a| tab[a[0], a[1]] = a[2] }
    return tab
  end

  def setup( map_id )
    @flash_data = Table.new( 1, 1 )
    super( map_id )
    @trap_ids = [1, 2, 3, 4, 5]#, 30]
    @flash_data.resize(width,height)
    @active_ranges = []
    reset_turn_count()
    create_parties()
    create_characters()
    create_traps()
    create_items()
    @turn_index = 0 ; @turn_order = []
    @phase = :init
    #@tileset_id = @map.tileset_id.to_i
    events.each { |ev| ev.moveto(*rand_room_floor_pos(1)) }
    mk_fake_party()
    characters.each do |c|
      c.clear_actions
      c.reset_wt
    end
  end

  def event_class
    Game::RogueEvent
  end

  def map
    return @map
  end

  def rooms
    @map.rooms
  end

  def random_room
    @map.random_room()
  end

  def room_table
    @map.room_table
  end

  def seed
    @map.seed
  end

  def room_at( x, y )
    rid = room_table[x, y]
    return rid == 0 ? nil : rooms[rid-1]
  end

  def dummy_pass_check( x, y )
    true
  end

  def rand_scatter_pos( x, y, scatter=0, pass_check=nil )
    pass_check = method(:dummy_pass_check) unless pass_check
    a = adjust_range(MkRange.diamond(scatter,0), x, y).select{|a|
      passable?(a[0],a[1],5)&&pass_check.call(a[0],a[1])
    }
    return x, y if a.empty?
    return *a.sample
  end

  def rand_room_floor_pos( contract=0, pass_check=nil )
    pass_check = method(:characters_xy_nt) unless pass_check
    retry_count = 0
    pos = random_room.random_floor_pos( contract )
    simp = proc { |x, y|
      !(passable?( round_x(x+1), round_x(y), 5 ) ||
      passable?( round_x(x-1), round_x(y), 5 ) ||
      passable?( round_x(x), round_x(y+1), 5 ) ||
      passable?( round_x(x), round_x(y-1), 5 ))
    }
    while (!passable?( pos[0], pos[1], 5 ) || !pass_check.call( pos[0], pos[1] ).empty?()) || simp.call( pos[0], pos[1] )
      pos = random_room.random_floor_pos( contract )
      retry_count += 1
      raise "Couldn't get an open random position after #{retry_count} tries" if retry_count > 999
    end
    return pos
  end

  def create_parties()
    @party = [$game.party,Game::RogueParty.new,
              Game::RogueParty.new, Game::RogueParty.new,
              Game::RogueParty.new
             ] # // Player, Enemies, Guests, Fake(All4Traps), Traps
    self
  end

  def mk_fake_party()
    party(3).clear_members()
    party(3).add_members(*(party(0).members+party(1).members))
  end

  def party(id)
    @party[id]
  end

  def enemy_party(id)
    id == 0 ? @party[1] : @party[0]
  end

  def create_characters()
    @characters = []
    @avail = (1..8).to_a#.shuffle
    chs = []
    for i in 1..3
      c = $game.party.battle_members[i-1]
      @characters << c
      chs << c
      pos = rand_room_floor_pos()
      c.moveto( *pos )
      c.recover_all()
      c.set_id( i )
    end
    chs[0].set_control( :player )
    for i in 1...chs.size
      chs[i].set_control( :ai )
      a = chs[0].pos_a
      chs[i].moveto( *rand_scatter_pos( a[0], a[1], 3, method(:characters_xy_nt?) ) )
    end
    chs.each { |c| c.party_id = 0 }
    chs[0].center_on!
    create_enemies()
    @characters.each { |c| c.reset_wt() }
  end

  def create_enemies()
    @wave ||= 0
    party(1).clear_members()
    for i2 in 0..8
      c = Game::RogueEnemy.new( 1 )
      c.party_id = 1
      party(c.party_id).add_member(c)
      @characters << c
      pos = rand_room_floor_pos()
      c.moveto( *pos )
      c.change_level( $game.party.average_level + c.level + @wave, false )
      c.recover_all()
      c.set_id( i2 )
    end
    @wave += 1
  end

  def create_traps
    @traps = []
    return if @trap_ids.empty?
    avail = @trap_ids
    for i in 0...(5)#(15)
      c = Game::Trap.new( avail[rand(avail.size)] )
      @traps << c
      pos = rand_room_floor_pos( 1, method(:objects_xy_nt) )
      c.moveto( *pos )
      c.change_level( $game.party.average_level + c.level + @wave, false )
      c.recover_all()
      c.set_id( i )
    end
  end

  def create_items()
    @items = []
    for i in 0...(10)
      pos = rand_room_floor_pos( 1, method(:characters_xy_nt) )
      c = Game::RogueItem.new( pos[0], pos[1], $data_items[[1,2,6,7].sample] )
      @items << c
    end
    #puts "Creating Items"
    refresh_items()
  end

  def place_item( x, y, item, n=1 )
    ite = items_xy( x, y ).find { |i| i.item == item }
    return if ite == item
    unless ite.nil?()
      ite.item_number += n
    else
      @items << Game::RogueItem.new( x, y, item )
      @items_need_refresh = true
    end
    #puts "Placing Items"
  end

  def scatter_items( x, y, items, igc, scatter=0 )
    return if items.empty?()
    a = items.inject([]) { |r, item|
      dx, dy = *rand_scatter_pos( x, y, scatter )
      c = Game::RogueItem.new( dx, dy, item )
      chs = characters_xy( c.x, c.y ) - igc
      chs.empty? ? r << c : do_item_pickup( chs, [c] )
      r
    }
    a.each{|i|place_item(i.x,i.y,i.item,i.item_number)}
    #puts "Scattering Items"
    refresh_items()
  end

  def do_item_pickup( characters, items=[] )
    return if items.empty?
    t = characters.max_by { |a| a.luk }
    return unless t
    Sound.play_equip
    gi = []
    items.each { |ite|
      i = ite.item_number
      item = ite.item
      n = 0
      i.times {
        break if t.inventory.max_item_number?( item )
        t.inventory.gain_item( item, 1 )
        ite.item_number -= 1
        n += 1
      }
      gi << [item, n]
      #if n > 0
      #  RogueManager.call_handler(
      #    :pop_quick_text,
      #    :header_text => "#{t.name}, Picked Up",
      #    :x => 0, :y => 0,
      #    :text => item.count_name(n),
      #    :wait => 60 ) { |win|
      #      win.center_xy().windowskin = Window::SkinCache.bitmap("window_console") }
      #end
    }
    RogueManager.call_handler(
      :pop_chest_items,
      :text => "#{t.name}, Got Item(s)",
      :items => gi,
      :x => 0, :y => 0,
      :wait => 90,
    ) {|win| win.salign!(5).windowskin = Window::SkinCache.bitmap("window_console") }
    remove_items( items.select { |i| i.item_number <= 0 } )
    #puts "Picking Up Items"
    refresh_items()
  end

  def refresh_items()
    hsh = {}
    @items.each { |itm| (hsh[[itm.x, itm.y]] ||= []) << itm }
    hsh.each_pair do |key, val|
      ans = {}
      val.each { |it|
        it.item.nil?() ? it.remove_this = true : (ans[it.item] ||= []) << it
      }
      ans.each_pair { |ite, arra|
        for i in 1...arra.size
          next if arra[0] == arra[i]
          arra[0].item_number += arra[i].item_number
          arra[i].remove_this = true
        end if arra.size > 1
      }
    end
    @items.select! { |i| !i.remove_this }
    @items_need_refresh = true
  end

  def get_char_by_hashcode( code )
    characters.find { |ch| ch.get_hash_code == code }
  end

  def get_obj_by_hashcode( code )
    objects.find { |ch| ch.get_hash_code == code }
  end

  def reset_traps()
    @traps.each { |t| t.remove_this = true }
    create_traps()
  end

  def reset_turn_count
    @turn_count = 0
  end

  def create_turn_order
    # // Legacy Mode >.>
    #@turn_order = Array.new( @characters )
    #@turn_order.sort! { |a, b| a.agi <=> b.agi }
    #@turn_order.reverse!
    #@turn_index = 0
    # // TO Style
    @turn_order = Array.new( @characters )
    unless @turn_order.any? { |c| c.wt_done? }
      @turn_order.each{|c|c.dec_wt} until @turn_order.any? { |c| c.wt_done? }
    end
    @turn_order.sort! { |a, b| a.wt <=> b.wt }
    @act_counter_max = (@turn_order.size-1).max(0)
    @turn_index = 0
  end

  def turn_character
    return @turn_order[@turn_index]
  end

  def subject
    return turn_character
  end

  def before_ntc
    if turn_character
      turn_character.focus_off
      turn_character.on_turn_end()
    end
  end

  def after_ntc
    turn_character_start()
  end

  def next_turn_character
    before_ntc()
    create_turn_order()
    after_ntc()
    while turn_character.nil?() || turn_character.dead?()
      before_ntc()
      create_turn_order()
      after_ntc()
      next_turn if turn_end?() || @turn_order.all? { |c| c.dead? }
    end
  end

  def turn_character_start()
    if turn_character
      turn_character.on_turn_start()
      turn_character.focus_on() if turn_character.actor? && !turn_character.dead?
      @cursor.moveto( turn_character.x, turn_character.y )
    end
  end

  def on_phase_init()
    next_turn
  end

  def all_foes_dead_or_none?
    return false if characters.any? { |c| c.foe?(0) && !c.dead? }
    return true
  end

  def all_friends_dead_or_none?()
    return characters.select { |c| c.friend?(0) }.all? { |c| c.dead? }
  end

  def turn_process
    turn_character.on_turn_end() if turn_character
    if all_foes_dead_or_none?()
      #Graphics.freeze
      reset_turn_count
      create_enemies
      reset_traps()
      RogueManager.call_handler( :refresh_spriteset )
      #Graphics.transition( 60 )
      #puts "Starting new wave: #{@wave}"
      RogueManager.call_handler(
        :pop_quick_text,
        :x => 0, :y => 0,
        :text => "Starting new wave: #@wave" ) { |win|
          win.center_xy().windowskin = Window::SkinCache.bitmap("window_console") }
    elsif all_friends_dead_or_none?()
      characters.each do |c| c.recover_all() if c.friend?() ; end
      puts "Refreshing due to dead party"
      RogueManager.call_handler( :refresh_spriteset )
    end
    @act_counter += 1
    puts "Act Count: #{@act_counter}"
    turn_end? ? next_turn() : next_turn_character()
  end

  def current_turn()
    @turn_count
  end

  def act_counter_max()
    @act_counter_max
  end

  def turn_end?()
    @act_counter >= act_counter_max
    #@turn_index >= @turn_order.size
  end

  def next_turn()
    @act_counter = 0
    turn_end() if @turn_count > 0
    @turn_count += 1
    puts "Turn: #{@turn_count}"
    #RogueManager.call_handler(
    #  :pop_quick_text,
    #  :x => 0, :y => 0,
    #  :text => "Advancing to turn: #{current_turn}",
    #  :wait => 30 ) { |win| win.center_xy() }
    turn_start()
  end

  def turn_end()
    # // Call a common event later
    @phase = :turn_end
    @characters.each { |c| c.on_global_turn_end }
    @traps.each { |c| c.on_global_turn_end }
  end

  def turn_start()
    # // Call a common event later
    @phase = :turn_start
    @characters.each { |c| c.on_global_turn_start; c.focus_off }
    @traps.each { |c| c.on_global_turn_start }
    create_turn_order()
    turn_character_start()
  end

  def start_turn()
    @phase = :turn
  end

  def refresh()
    super
    @characters = @characters.select { |c| c.remove_this == false }
    @traps = @traps.select { |c| c.remove_this == false }
    @items = @items.select { |c| c.remove_this == false } #refresh_items()
  end

  def update(main=false)
    super(main)
    (@characters+@traps).each { |c| c.update }
    update_cursor if @cursor.active
  end

  def update_cursor
    @cursor.update
  end

  def triggered_here( x, y )
    hsh = {}
    hsh[:traps]      = traps_xy( x, y )
    hsh[:characters] = characters_xy( x, y )
    hsh[:items]      = items_xy( x, y )
    return hsh
  end

  def start_map_event(x, y, triggers, normal)
    events_xy(x, y).each do |event|
      if event.trigger_in?(triggers) && event.normal_priority? == normal
        event.start
      end
    end
  end

  def check_event_trigger_here(triggers,x,y)
    start_map_event(x, y, triggers, false)
  end

  def check_event_trigger_there(triggers,x,y,direction)
    x2 = round_x_with_direction(x, direction)
    y2 = round_y_with_direction(y, direction)
    start_map_event(x2, y2, triggers, true)
    return if any_event_starting?
    return unless counter?(x2, y2)
    x3 = round_x_with_direction(x2, direction)
    y3 = round_y_with_direction(y2, direction)
    start_map_event(x3, y3, triggers, true)
  end

  def check_event_trigger_touch(x, y)
    start_map_event(x, y, [1,2], true)
  end

  def add_ranges( ranges, color=0xf84 )
    ranges.each { |r| @flash_data[r[0], r[1]] = color }
    @active_ranges += ranges
  end

  def remove_ranges( ranges )
    ranges.each { |r| @flash_data[r[0], r[1]] = 0x000 }
    @active_ranges -= ranges
  end

  def replace_ranges(ranges, color=0xf84 )
    clear_active_ranges()
    add_ranges(ranges,color)
  end

  def clear_active_ranges()
    @active_ranges.each { |r| @flash_data[r[0], r[1]] = 0x000 }
    @active_ranges.clear()
  end

  attr_reader :active_ranges

  def range_xy_valid?(x,y)
    @flash_data[x,y] > 0
  end

  def reset_flash_data()
    for x in 0...@flash_data.xsize
      for y in 0...@flash_data.ysize
        @flash_data[x, y] = 0xf84
      end
    end
  end

  def adjust_range(list,x,y)
    list.map{|a|[round_x(x+a[0]),round_y(y+a[1])]}
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
