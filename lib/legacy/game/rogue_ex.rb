# Game::Rogue (Ex)
# // 01/20/2012
# // 01/20/2012
class Game::Rogue

  include RPG::Rogue::RogueConstants

  def advance_floor
    self.floor += 1
    refresh_floor
  end

  def basement_floors?
    false
  end

  def floor
    $game.variables[FLOOR_VAR]
  end

  def floor=(n)
    $game.variables[FLOOR_VAR] = n
  end

  def floor_s()
    (basement_floors? ? '\c[2]B\c[0]' : '\c[1]F\c[0]') + floor.to_s
  end

  def refresh_floor()
    setup(-1)
    RogueManager.call_handler( :refresh_spriteset )
  end

end
