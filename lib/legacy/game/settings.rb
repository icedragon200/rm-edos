# Game::Settings 
#==============================================================================#
# ■ Game::Settings
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/29/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/29/2011 V1.0
#
#==============================================================================#
class Game::Settings
  def initialize()
    init_settings()
  end  
  def init_settings()  
    self.bgm_volume = 100
    self.bgs_volume = 60
    self.se_volume  = 80
    self.message_speed = 100
  end  
  def _clamp_percent( volume )
    volume.clamp(0, 100).to_i
  end  
  def adjust_wait( n )
    (n * (1.0 - message_speed_rate)).to_i
  end
  attr_reader :bgm_volume
  attr_reader :bgs_volume
  attr_reader :se_volume
  attr_reader :me_volume
  def bgm_volume=( volume )
    @bgm_volume = _clamp_percent( volume )
  end  
  def bgs_volume=( volume )
    @bgs_volume = _clamp_percent( volume )
  end
  def se_volume=( volume )
    @se_volume = _clamp_percent( volume )
  end
  def me_volume=( volume )
    @me_volume = _clamp_percent( volume )
  end
  attr_reader :message_speed
  def message_speed=( speed )
    @message_speed = speed.to_i
  end  
  def bgm_volume_rate
    bgm_volume / 100.0
  end  
  def bgs_volume_rate
    bgs_volume / 100.0
  end  
  def se_volume_rate
    se_volume / 100.0
  end  
  def me_volume_rate
    me_volume / 100.0
  end  
  def message_speed_rate
    message_speed / 200.0
  end  
  def set_bgm_volume_rate( rate )
    self.bgm_volume = 100 * rate
    return self
  end
  def set_bgs_volume_rate( rate )
    self.bgs_volume = 100 * rate
    return self
  end
  def set_se_volume_rate( rate )
    self.se_volume = 100 * rate
    return self
  end
  def set_me_volume_rate( rate )
    self.me_volume = 100 * rate
    return self
  end
  def set_message_speed_rate( rate )
    self.message_speed = 200 * rate
    return self
  end  
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
