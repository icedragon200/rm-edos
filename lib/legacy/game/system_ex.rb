# Game::System (Ex)
# // 12/05/2011
# // 02/02/2012
class Game::System
  attr_accessor :in_rogue
  def in_rogue?
    @in_rogue == true
  end
  def rogue?
    in_rogue? # // && whatever_else_i_wanna_add_later
  end
  # // 02/19/2012
  def _map() # // . x . Points to whichever Game::Map is currently being used
    return in_rogue? ? $game.rogue : $game.map
  end
  def window_positions
    (@window_positions ||= {})
  end
end
