#
# EDOS/core/goranka.rb
#   dc ??/03/2013
#   dm 30/03/2013
# vr 0.0.1
require 'opengl'

require_relative 'goranka/helper'

module Goranka

  NULL = 0 # convinience
  include Gl, Glu, Glut

  def self.init_opengl
    # Initialize the GLUT core
    glutInit
    # Initialize the display with:
    #   double_buffer
    #   rgba color support
    #   alpha channels
    #   depth testing
    glutInitDisplayMode GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA #| GLUT_DEPTH
    # Initialize the window size and position
    glutInitWindowSize 640, 480 # width, height
    glutInitWindowPosition 0, 0 # x, y
    # create the window from the aforementioned settings
    @window = glutCreateWindow "Goranka" # title
    @ticks = 0
    @pos = StarRuby::Vector3F.new(0.0, 0.0,-1.0)
    @rot = StarRuby::Vector3F.new(0.0, 0.0, 0.0)

    init_textures

    init_gl(640, 480)
    init_glut_functions

    ## enter main loop
    glutMainLoop
  end

  def self.init_textures
    @texture = glGenTextures(1)
    tt = StarRuby::Texture.load("screenshots/shot0001.png")
    data = tt.dump('rgba')
    glBindTexture(GL_TEXTURE_2D, @texture[0])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tt.width, tt.height, NULL, GL_RGBA, GL_UNSIGNED_BYTE, data.dup)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    tt.dispose
  end

  def self.init_gl(width=640, height=480)
    glClearColor 0.0, 0.0, 0.0, 0

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity
    glOrtho(0, width, height, 0, 0, 1000)
    glDisable(GL_DEPTH_TEST)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity

    glEnable GL_TEXTURE_2D

    render_gl_scene
  end

  def self.init_glut_functions
    glutIdleFunc(:idle)
    glutDisplayFunc(:render_gl_scene)
    glutReshapeFunc(:reshape)
    glutKeyboardFunc(:keyboard)
  end

  def self.idle
    glutPostRedisplay
  end

  def self.render_gl_scene
    glClear GL_COLOR_BUFFER_BIT# | GL_DEPTH_BUFFER_BIT

    # reset_view
    glMatrixMode GL_MODELVIEW
    glLoadIdentity

    glRotatef 90.0 , *@rot.to_a
    glTranslatef *@pos.to_a #-320.0, -240.0, -64.0

    glBindTexture(GL_TEXTURE_2D, @texture[0])

    #draw_cube()

    glBegin(GL_QUADS) do
      glTexCoord2f(-0.0,-1.0); glVertex3i( -1, +1, 0);
      glTexCoord2f(-0.0, 0.0); glVertex3i( -1, -1, 0);
      glTexCoord2f(+1.0, 0.0); glVertex3i( +1, -1, 0);
      glTexCoord2f(+1.0,-1.0); glVertex3i( +1, +1, 0);
    end

    @ticks += 1
    ## swap_buffer
    glutSwapBuffers
  end

  def self.keyboard(key, x, y)
    case key
    when ?\e
      glutDestroyWindow @window
      exit(0)
    when "w"
      @pos.y -= 1
    when "a"
      @pos.x -= 1
    when "s"
      @pos.y += 1
    when "d"
      @pos.x += 1
    when "q"
      @pos.z -= 1
    when "e"
      @pos.z += 1

    when "u" then @rot.y -= 0.01
    when "h" then @rot.x += 0.01
    when "j" then @rot.y += 0.01
    when "k" then @rot.x -= 0.01
    when "y" then @rot.z -= 0.01
    when "i" then @rot.z += 0.01

    when "x"
      @pos.set(0, 0, 1.0)
    when "m"
      @rot.set(0.0, 0.0, 0.0)
    end
    p "POS: %s | ROT: %s" % [@pos.to_a.inspect, @rot.to_a.inspect]
    glutPostRedisplay
  end

  def self.reshape(width, height)
    height = 1 if height == 0

    # Reset current viewpoint and perspective transformation
    glViewport 0, 0, width, height

    glMatrixMode GL_PROJECTION
    glLoadIdentity

    #gluPerspective 45.0, aspect_ratio(width, height), 0.1, 100.0
  end

  def self.main
    require_relative('goranka/main_mc_cube.rb') && main_mc_cube
  end

end
