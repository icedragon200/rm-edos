#
# EDOS/core/goranka/helper.rb
#   dc 09/06/2013
#   dm 09/06/2013
# vr 0.0.1
module Goranka

  # calculate the aspect ratio for given values
  def self.aspect_ratio(width, height)
    width.to_f / height.to_f
  end

  def self.draw_cube
    # Draw Cube
    col_flt_a = [1.0, 1.0, 1.0]
    glBegin GL_QUADS do
      # front face
      glNormal3f( 0.0,  0.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)

      # back face
      glNormal3f( 0.0,  0.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)

      # top face
      glNormal3f( 0.0,  1.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)

      # bottom face
      glNormal3f( 0.0, -1.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)

      # right face
      glNormal3f( 1.0,  0.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)

      # left face
      glNormal3f(-1.0,  0.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
    end
  end

end
