#
# EDOS/core/goranka.rb
#   dc ??/03/2013
#   dm 30/03/2013
# vr 0.0.1
module Goranka

  def self.main_mc_cube
    w, h = Graphics.width, Graphics.height
    # Initliaze our GLUT code
    glutInit
    # Setup a double buffer, RGBA color, alpha components and depth buffer
    glutInitDisplayMode GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH
    glutInitWindowSize w, h
    glutInitWindowPosition 0, 0

    window = glutCreateWindow "CUBE"

    glutIdleFunc(:idle)
    glutDisplayFunc(:draw_gl_scene)
    glutReshapeFunc(:reshape)
    glutKeyboardFunc(:keyboard)
    glutMouseFunc(:mouse)

    index_to_xy = ->(index) do
      [(index % 16) * 16, (index / 16) * 16]
    end
    calc_crop = ->(index) do
      index_to_xy.(index) + [16, 16]
    end

    bitmap   = Bitmap.new("blocks.png")
    #bitmap.draw_gauge_ext_sp5(bitmap.rect, 1.0, DrawExt::RUBY_BAR_COLORS)
    src_texture  = bitmap.texture

    @textures = []
    i = 0
    for i in 0...(16 * 9)
      @textures[i] = glGenTextures(6)
    end
    @texture_index = 0

    setup_tile = ->(i, tile_index) do
      texture = src_texture.crop(*calc_crop.(tile_index))
      dumpdata = texture.dump('rgba')
      setup_texture(@textures[@texture_index], i, texture.width, texture.height, dumpdata.dup)
      texture.dispose
    end

    for x in 0...@textures.size
      @texture_index = x
      for i in 0...6
        setup_tile.(i, x)
      end
    end

    #for i in 0...6
    #  setup_tile.(i, 48 + 4)
    #end

    # Reactor
    #   states
    #for x in 0...5
    #  @texture_index = x
    #  # cube faces
    #  for i in 0...6
    #    setup_tile.(i, 35 + x)
    #  end
    #end

    # Scaffold
    #setup_tile.(0, 28)
    #setup_tile.(1, 0)
    #for i in 2...6
    #  setup_tile.(i, 27)
    #end

    bitmap.dispose

    @texture_index = 0
    init_gl_window(w, h)

    glutMainLoop
  end

  def self.setup_texture(textures, id, w, h, data)
    glBindTexture(GL_TEXTURE_2D, textures[id])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, NULL, GL_RGBA, GL_UNSIGNED_BYTE, data)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
  end

  def self.init_gl_window width = 640, height = 480
    # Background color to black
    glClearColor 0.0, 0.0, 0.0, 0
    # Enables clearing of depth buffer
    glClearDepth 1.0
    # Set type of depth test
    glDepthFunc GL_LEQUAL
    # Enable depth testing
    glEnable GL_DEPTH_TEST
    # Enable 2D Texture
    glEnable GL_TEXTURE_2D
    # Enable Color Material
    glEnable GL_COLOR_MATERIAL
    # Enabled BackFacing
    glEnable(GL_CULL_FACE);

    #glEnable GL_LIGHT0
    #glEnable GL_LIGHTING
    # Enable smooth color shading
    glShadeModel GL_SMOOTH
    #glHint GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST

    glMatrixMode GL_PROJECTION
    glLoadIdentity
    # Calculate aspect ratio of the window
    gluPerspective 45.0, aspect_ratio(width, height), 0.1, 100.0

    glMatrixMode GL_MODELVIEW

    draw_gl_scene
  end

  def self.reshape width, height
    height = 1 if height == 0

    # Reset current viewpoint and perspective transformation
    glViewport 0, 0, width, height

    glMatrixMode GL_PROJECTION
    glLoadIdentity

    gluPerspective 45.0, aspect_ratio(width, height), 0.1, 100.0
  end

  $quad_angle = 0
  $counter = 0
  def self.draw_gl_scene
    $counter += 1
    $quad_angle += 0.5
    glClear GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT

    # Reset the view
    glMatrixMode GL_MODELVIEW
    glLoadIdentity
    glFrontFace(GL_CW)
    #glCullFace(GL_FRONT /* or GL_BACK or even GL_FRONT_AND_BACK */);
    #glFrontFace(GL_CW /* or GL_CCW */);

    #glBindTexture(GL_TEXTURE_2D, @textures[0])

    # Move left 1.5 units and into the screen 6.0 units
    glTranslatef 0.0, 0.0, -3.0

    #glRotatef $quad_angle, 0.0, 1.0, 0.0
    #(1 / Math.sqrt(2))

    #glRotatef 90, 1.0, 0.0, 0.0
    glRotatef 180.0 , 1.0, 0.0, 0.0
    glRotatef 35.264, 1.0, 0.0, 0.0
    glRotatef 45.0  , 0.0,-1.0, 0.0
    glRotatef $quad_angle, 0.0, 1.0, 0.0

    @texture_index = (@texture_index + 1) % @textures.size if $counter % 60 == 0
    glCullFace(GL_BACK)
    draw_texture_cube(GL_TEXTURE_2D, @textures[@texture_index])
    glCullFace(GL_FRONT)
    draw_texture_cube(GL_TEXTURE_2D, @textures[@texture_index])

    #glTranslatef 1.0, 0.0, 0.0
    #draw_texture_cube

    glutSwapBuffers
  end

  # The idle function to handle
  def self.idle
    glutPostRedisplay
  end

  def self.mouse(*args)
    puts args
  end

  # Keyboard handler to exit when ESC is typed
  def self.keyboard(key, x, y)
    case key
    when ?\e
      glutDestroyWindow window
      exit(0)
    end
    glutPostRedisplay
  end

  def self.draw_texture_cube(texture_type, textures)
    glBindTexture(texture_type, textures[0])
    base, top = -0.5, 0.5
    glBegin(GL_QUADS) do
      # // Top Face
      glTexCoord2f(0.0, 1.0); glVertex3f(base,  top,  base);  #// Top Left Of The Texture and Quad
      glTexCoord2f(0.0, 0.0); glVertex3f(base,  top,   top);  #// Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0, 0.0); glVertex3f( top,  top,   top);  #// Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0, 1.0); glVertex3f( top,  top,  base);  #// Top Right Of The Texture and Quad
    end

    glBindTexture(texture_type, textures[1])
    glBegin(GL_QUADS) do
      # // Bottom Face

      glTexCoord2f(1.0, 1.0); glVertex3f(base, base, base);  #// Top Right Of The Texture and Quad
      glTexCoord2f(0.0, 1.0); glVertex3f( top, base, base);  #// Top Left Of The Texture and Quad
      glTexCoord2f(0.0, 0.0); glVertex3f( top, base,  top);  #// Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0, 0.0); glVertex3f(base, base,  top);  #// Bottom Right Of The Texture and Quad
    end

    glBindTexture(texture_type, textures[2])
    glBegin(GL_QUADS) do
      # // Front Face
      glTexCoord2f(0.0, 0.0); glVertex3f(base, base,  top);  #// Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0, 0.0); glVertex3f( top, base,  top);  #// Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0, 1.0); glVertex3f( top,  top,  top);  #// Top Right Of The Texture and Quad
      glTexCoord2f(0.0, 1.0); glVertex3f(base,  top,  top);  #// Top Left Of The Texture and Quad
    end

    glBindTexture(texture_type, textures[3])
    glBegin(GL_QUADS) do
      # // Back Face
      glTexCoord2f(1.0, 0.0); glVertex3f(base, base, base);  #// Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0, 1.0); glVertex3f(base,  top, base);  #// Top Right Of The Texture and Quad
      glTexCoord2f(0.0, 1.0); glVertex3f( top,  top, base);  #// Top Left Of The Texture and Quad
      glTexCoord2f(0.0, 0.0); glVertex3f( top, base, base);  #// Bottom Left Of The Texture and Quad
    end

    glBindTexture(texture_type, textures[4])
    glBegin(GL_QUADS) do
      # // Left Face
      glTexCoord2f(0.0, 0.0); glVertex3f(base, base, base);  #// Bottom Left Of The Texture and Quad
      glTexCoord2f(1.0, 0.0); glVertex3f(base, base,  top);  #// Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0, 1.0); glVertex3f(base,  top,  top);  #// Top Right Of The Texture and Quad
      glTexCoord2f(0.0, 1.0); glVertex3f(base,  top, base);  #// Top Left Of The Texture and Quad
    end

    glBindTexture(texture_type, textures[5])
    glBegin(GL_QUADS) do
      # // Right face
      glTexCoord2f(1.0, 0.0); glVertex3f( top, base, base);  #// Bottom Right Of The Texture and Quad
      glTexCoord2f(1.0, 1.0); glVertex3f( top,  top, base);  #// Top Right Of The Texture and Quad
      glTexCoord2f(0.0, 1.0); glVertex3f( top,  top,  top);  #// Top Left Of The Texture and Quad
      glTexCoord2f(0.0, 0.0); glVertex3f( top, base,  top);  #// Bottom Left Of The Texture and Quad
    end
  end

  def self.draw_cube
    # Draw Cube
    col_flt_a = [1.0, 1.0, 1.0]
    glBegin GL_QUADS do
      # front face
      glNormal3f( 0.0,  0.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)

      # back face
      glNormal3f( 0.0,  0.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)

      # top face
      glNormal3f( 0.0,  1.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)

      # bottom face
      glNormal3f( 0.0, -1.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)

      # right face
      glNormal3f( 1.0,  0.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f( 1.0, -1.0,  1.0)

      # left face
      glNormal3f(-1.0,  0.0,  0.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0, -1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0, -1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0,  1.0)
      glColor3f(*col_flt_a)
      glVertex3f(-1.0,  1.0, -1.0)
    end
  end

end
