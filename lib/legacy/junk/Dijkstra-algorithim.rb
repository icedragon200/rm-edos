#
# Dijkstra-algorithim.rb
# vr 1.00
module Dijkstra

  Node = Struct.new(:x, :y, :paths)
  Path = Struct.new(:node1, :node2, :length)

  def node(*args)
    return Node.new(*args)
  end

  def path(*args)
    return Path.new(*args)
  end

  def link_nodes(parent_node, *nodes)
    nodes.each do |node|
      parent_node.paths.push(path(parent_node, node, Float::INFINITY))
    end
  end

  def calc_length(node1, node2)
    return (node2.x - node1.x).abs + (node2.y - node1.y).abs
  end

  def calc_node_paths_p(node)
    node.paths.each { |pth| pth.length = calc_length(pth.node1, pth.node2) }
  end

  def rescale_node_p(node)
    node.x *= 24
    node.y *= 24
  end

  def test
    grid_size = 24

    _nodes = []

    n0  = node( 4,  4, [])
    n1  = node( n0.x - 2,  n0.y - 3, [])
    n2  = node( n0.x + 4,  n0.y - 2, [])
    n3  = node( n2.x + 3,  n2.y + 3, [])
    n4  = node( n2.x + 1,  n2.y + 7, [])
    n5  = node( n3.x + 1,  n3.y + 3, [])
    n6  = node( n4.x - 1,  n4.y + 3, [])
    n7  = node( n5.x + 3,  n5.y + 1, [])
    n8  = node( n6.x + 1,  n6.y + 3, [])
    n9  = node( n7.x - 4,  n7.y - 1, [])
    n10 = node( n8.x - 4,  n8.y - 1, [])
    n11 = node( n10.x - 3,  n10.y - 3, [])
    n12 = node( n0.x - 3,  n0.y, [])

    link_nodes(n0, n1, n2)
    link_nodes(n1, n2)
    link_nodes(n2, n3, n4)
    link_nodes(n3, n5)
    link_nodes(n4, n5, n6)
    link_nodes(n5, n7)
    link_nodes(n6, n8)
    link_nodes(n7, n6)

    _nodes += [n0, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12]
    _nodes.each(&method(:rescale_node_p))
    _nodes.each(&method(:calc_node_paths_p))

    _sps = _nodes.map do |node|
      sp = Sprite.new(nil)
      sp.bitmap = Bitmap.new(grid_size, grid_size)
      sp.bitmap.draw_gauge_ext_sp5(sp.bitmap.rect, 1.0, DrawExt::GREEN_BAR_COLORS)
      sp.x = node.x
      sp.y = node.y
      sp
    end

    loop do
      Main.update
    end
  end

  extend self

end

__END__
Dijkstra's algorithm

Let the node at which we are starting be called the initial node. Let the
distance of node Y be the distance from the initial node to Y. Dijkstra's
algorithm will assign some initial distance values and will try to improve them
step by step.

1. Assign to every node a tentative distance value: set it to zero for our initial
node and to infinity for all other nodes.

2. Mark all nodes unvisited. Set the initial node as current. Create a set of the
unvisited nodes called the unvisited set consisting of all the nodes except the
initial node.

3. For the current node, consider all of its unvisited neighbors and calculate
their tentative distances. For example, if the current node A is marked with a
distance of 6, and the edge connecting it with a neighbor B has length 2, then
the distance to B (through A) will be 6+2=8. If this distance is less than the
previously recorded tentative distance of B, then overwrite that distance. Even
though a neighbor has been examined, it is not marked as "visited" at this time,
and it remains in the unvisited set.

4. When we are done considering all of the neighbors of the current node, mark the
current node as visited and remove it from the unvisited set. A visited node
will never be checked again.

5. If the destination node has been marked visited (when planning a route between
two specific nodes) or if the smallest tentative distance among the nodes in the
unvisited set is infinity (when planning a complete traversal), then stop. The
algorithm has finished.

6. Select the unvisited node that is marked with the smallest tentative distance,
and set it as the new "current node" then go back to step 3.
