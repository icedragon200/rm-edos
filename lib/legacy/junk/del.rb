@count = 2

64.times do |i|
  delta = i / 64.0
  m = 1.0 / @count
  edelta = (delta % m) / m

  p [delta, edelta]
end