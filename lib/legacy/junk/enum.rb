def enum(*args)
  Hash[args.each_with_index.to_a]
end

p enum :ZOMG,
       :COOKIES,
       :AND_SHEET
