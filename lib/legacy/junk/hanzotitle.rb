
module HanzoTitle

  # In Graphics/Pictures
  CommandImgs  = ["new_game.png","continue.png","options.png","exit.png"]
  ArrowImg     = "TitleArrow.png"  # In Graphics/Pictures/
  TitleImg     = "title.png"       # In Graphics/Pictures/
  TitleBackImg = "titleback.png"   # In Graphics.Title1/

end

class Scene_Title < Scene_Base

  def initialize(*args)
    @index = 0
  end

  def create_command_window
  end

  def close_command_window
  end

  def create_background
    @background = Sprite_Base.new
    @background.bitmap = Cache.title1(HanzoTitle::TitleBackImg)
    create_middleground
  end

  def create_middleground
    @glows = []
    @cmd_glows = []
  end

  def create_foreground
    @title = Sprite_Base.new
    @title.bitmap = Cache.picture(HanzoTitle::TitleImg)
    @command = Sprite_TitleCommand.new(method(:current_cmd))
    @arrowL = Sprite_TitleArrow.new(true,method(:cmd_rect))
    @arrowR = Sprite_TitleArrow.new(false,method(:cmd_rect))
  end

  def current_cmd
    HanzoTitle::CommandImgs[@index]
  end

  def cmd_rect
    Rect.new(@command.x,@command.y,@command.width,@command.height)
  end

  def cmd_max
    HanzoTitle::CommandImgs.size - 1
  end

  def update
    super
    update_input
    update_graphics
  end

  def update_input
    if Input.repeat?(:RIGHT)
      @index = (@index + 1) % cmd_max
    elsif Input.repeat?(:LEFT)
      @index = (@index - 1) < 0 ? cmd_max : @index - 1
    elsif Input.trigger?(:C)
      select_cmd
    end
  end

  def update_graphics
    @background.update
    @glows.each {|g| g.update }
    @cmd_glows.each {|g| g.update }
    [@title,@arrowL,@arrowR,@command].each {|i| i.update }
  end

  def select_cmd
    case @index
    when 0
      command_new_game
    when 1
      command_continue
    when 2
      msgbox("Options not implemented!")
    when 3
      command_shutdown
    end
  end
end

class Sprite_TitleCommand < Sprite_Base
  def initialize(command_method)
    super(nil)
    @cmd_check = command_method
    self.bitmap = Cache.picture(current_command)
  end

  def update
    super
    self.x = Graphics.width/2 - width/2
    self.y = (Graphics.height/4) * 3
    self.bitmap = Cache.picture(current_command)
  end

  def current_command
    @cmd_check.call
  end
end

class Sprite_TitleArrow < Sprite_Base

  def initialize(left=false, cmd_rect_method)
    super(nil)
    self.bitmap = Cache.picture(HanzoTitle::ArrowImg)
    self.mirror = @left = left
    @cmd_rect = cmd_rect_method
  end

  def update
    update_xy
    super
  end

  def update_xy
    r = @cmd_rect.call
    self.y = r.y + (r.height / 2 - self.height / 2)
    if @left
      self.x = (r.x - width) - 20
    else
      self.x = (r.x + r.width) + 20
    end
  end
end

#~ class Tween
#~
#~   attr_reader :x,:y
#~   def initialize(x,y,opts={})
#~     @ox,@oy = x,y
#~     @x,@y = @ox,@oy
#~     @type = opts[:type] || :nil
#~     @opts = { line: [0,0], pattern: [[0,0]], reverse: false, distance: 10 }
#~     @opts.merge!(opts) if opts.is_a?(Hash)
#~     @reversed = false
#~   end
#~
#~   def distance
#~     @opts[:distance]
#~   end
#~
#~   def reverse?
#~     @opts[:reverse]
#~   end
#~
#~   def pattern
#~     @opts[:pattern]
#~   end
#~
#~   def line
#~     @opts[:line]
#~   end
#~
#~   def travel_x
#~     @ox + @x
#~   end
#~
#~   def travel_y
#~     @oy + @y
#~   end
#~
#~   def update
#~     case @type
#~     when :line
#~       @x += line[0]
#~       @y += line[1]
#~       need_reverse &&
#~
