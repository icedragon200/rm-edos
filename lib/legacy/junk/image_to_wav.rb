
    #require '/home/icy/docs/codes/Git/wavey/wavey'
    #filename = 'nscreenshots/new-status'
    #bmp = Bitmap.new(filename)
    #bmp_to_wave(bmp, "#{File.basename(filename, File.extname(filename))}3.wav")
    #bmp.dispose
def bmp_to_wave(bmp, filename)
    colors = []
    texture = bmp.texture
    for y in 0...bmp.height
      for x in 0...bmp.width
        colors.push(texture[x, y])
      end
    end

    color_ratings = colors.map(&ColorTool.method(:color_rating))

    raw_wavedata = color_ratings.map do |r|
      (r * 0xFFFF).to_i
    end

    wavedata_rows = []
    for y in 0...bmp.height
      wavedata_rows.push(raw_wavedata[(y * bmp.width), bmp.width])
    end

    wavedata = Array.new(bmp.width, 0)

    freqs = wavedata_rows.map do |row|
      row.inject(:+) / row.size
    end

    sines = freqs.map do |freq|
      (0...wavedata.size).map do |i|
        Math.sin((Math::PI * i * freq / 440) / wavedata.size) * freq
      end
    end

    sines.each do |sine|
      for x in 0...wavedata.size
        wavedata[x] += sine[x]
      end
    end

    #for x in 0...bmp.width
      #for y in 0...bmp.height
        #wavedata[x] = (wavedata[x] * wavedata_rows[y][x]) / 0xFFFF
      #end
    #end

    for x in 0...wavedata.size
      wavedata[x] = (wavedata[x] / bmp.height).to_i
    end

    sample_rate = 44100 #44100
    channel_count = 1
    sample_count = wavedata.size # 2 seconds
    bytes_per_sample = 2

    bit_rate = sample_rate * channel_count * bytes_per_sample
    datasize = bytes_per_sample * sample_count * channel_count

    riff = Wavey::RIFF.new
    riff.chunk_id = "RIFF"
    riff.chunk_size = 36 + datasize
    riff.riff_type = "WAVE"

    format = Wavey::Format.new
    format.chunk_id = "fmt "
    format.chunk_size = 16
    format.format_tag = 1 # PCM
    format.channel_count = channel_count
    format.sample_rate = sample_rate
    format.bit_rate = bit_rate
    format.block_align = channel_count * bytes_per_sample
    format.bytes_per_sample = 8 * bytes_per_sample

    data = Wavey::WaveData.new
    data.chunk_id = "data"
    data.chunk_size = datasize
    data.data = wavedata

    riff_s = riff.to_binary_s
    fmt_s = format.to_binary_s
    data_s = data.to_binary_s

    File.open(filename, 'wb') do |f|
      f.write(riff_s)
      f.write(fmt_s)
      f.write(data_s)
    end
end
