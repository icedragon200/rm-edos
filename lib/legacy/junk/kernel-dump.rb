module EDoS::Window_Manager

  def active_pop?
    @windows.any? do |w| w.is_a? Window::PopText end
  end

  def dropdown_windows *windows
    or_window_pos = windows.map(&:to_rect)
    windows.each do |w|
      #w.x, w.y = w.x, -w.height
      #w.width = w.height = w.standard_padding*2
      rect = MACL::Surface::Tool.center(
        Graphics.rect, MACL::Surface::Tool.area_rect(w))
      rect.width = rect.height = w.standard_padding*2
      rect.y = -w.height
      w.move *rect.to_a
      w.x = w.x
      w.y = w.y
      #w.opacity = 0
    end
    for i in 0...windows.size
      win   = windows[i]
      orpos = or_window_pos[i]
      tween = MACL::Tween::Multi.new
      tween.clear
      easer = :back_out
      frm = 10
      tween.add_tween win.y, orpos.y, easer, MACL::Tween.frames_to_tt(frm)
      until tween.done?
        tween.update
        win.y = tween.values[0]
        update_for_wait
      end
      tween.clear
      easer = :elastic_out
      frm = 10
      tween.add_tween win.height, orpos.height, easer, MACL::Tween.frames_to_tt(frm)
      until tween.done?
        tween.update
        win.height = tween.values[0]
        update_for_wait
      end
      tween.clear
      frm = 30
      easer = :sine_out
      tween.add_tween win.x, orpos.x, easer, MACL::Tween.frames_to_tt(frm)
      easer = :sine_in
      tween.add_tween win.width, orpos.width, easer, MACL::Tween.frames_to_tt(frm)
      until tween.done?
        tween.update
        win.x     = tween.values[0]
        win.width = tween.values[1]
        update_for_wait
      end
      #tween.clear
      #easer = :elastic_out
      #frm = 10
      #tween.add_tween win.x, orpos.x, easer, MACL::Tween.frames_to_tt(frm)
      #until tween.done?
      #  tween.update
      #  win.x = tween.values[0]
      #  #win.x, win.y, win.width, win.height = *tween.values
      #  update_for_wait
      #end
      #tween.clear
      #easer = :sine_in
      #frm = 20
      #tween.add_tween win.opacity, 255, easer, MACL::Tween.frames_to_tt(frm)
      #until tween.done?
      #  tween.update
      #  win.opacity = tween.value(0)
      #  update_for_wait
      #end
      win.x, win.y, win.width, win.height = *orpos.to_a
    end
  end

end
  def show_wait wait_time, x=0, y=0, z=0, width=128, height=8
    sp = Sprite::Progress.new @viewport, :horz, width, height
    sp.x,sp.y,sp.z=x,y,z;wtf=wait_time.to_f # // xD LOL
    wait_ex(wait_time){|i|sp.rate=i/wtf ; break if yield i if block_given?}
    sp.dispose
  end

  RESULT_TRUE  = 0
  RESULT_FALSE = 1

  def confirm_window_ok
    @confirm_result = RESULT_TRUE
  end

  def confirm_window_cancel
    @confirm_result = RESULT_FALSE
  end

  def pop_confirm_window *rargs
    if rargs[0].is_a? Hash
      args = rargs[0]
    else
      args = {text: rargs[0], x: rargs[1], y: rargs[2]}
    end
    @confirm_result = nil
    x         = args[:x] || 0
    y         = args[:y] || 0
    window = Window::Confirm.new x, y, args[:text]
    window.set_handler :ok, args[:yes_handler] || method(:confirm_window_ok)
    window.set_handler :cancel, args[:no_handler] || method(:confirm_window_cancel)
    window.activate
    yield window if block_given?
    window_manager.add window
    window.start_open
    wait_for_windows window
    update_for_wait while window.active
    window.start_close
    wait_for_windows window
    remove_window window
    @confirm_result
  end

  def pop_chest_items *rargs
    if rargs[0].is_a?(Hash)
      args = rargs[0]
    else
      args = {items: rargs[0], text: rargs[1], x: rargs[2], y: rargs[3], wait: rargs[4]}
    end
    x         = args[:x] || 0
    y         = args[:y] || 0
    wait_time = args[:wait] || 90 #   // [item, number]
    window = Window::ChestItems.new x, y, args[:items]||[], args[:text]
    #window.activate
    yield window if block_given?
    window_manager.add window
    window.start_open
    wait_for_windows window
    block = args[:break] ? proc { |i| Input.mtrigger_any?(:A, :B, :C) } : nil
    show_wait wait_time, window.x, window.y2, window.z, window.width, &block
    window.start_close
    wait_for_windows window
    remove_window window
  end

  def pop_quick_text(*rargs)
    if rargs[0].is_a?(Hash)
      args = rargs[0]
    else
      args = {
        text:        rargs[0],
        header_text: rargs[6],
        x: rargs[1], y: rargs[2],
        width: rargs[3], height: rargs[4],
        wait:        rargs[5],
        break:       rargs[7],
      }
    end
    x         = args[:x] || 0
    y         = args[:y] || 0
    width     = args[:width] || 172
    height    = args[:height] || 40
    texts     = args[:text] || [""]
    texts     = [texts] unless texts.is_a?(Enumerable)
    wait_time = args[:wait] || 90
    window    = Window::PopText.new x, y, width, height, nil, args[:header_text]
    yield window if block_given?
    window_manager.add window
    block = args[:break] ? proc { |i| Input.mtrigger_any?(:A, :B, :C) } : nil
    texts.each do |t|
      window.set_text t
      window.start_open
      wait_for_windows window
      show_wait wait_time, window.x, window.y2, window.z, window.width, &block
      window.start_close
      wait_for_windows window
    end
    remove_window window
  end

  def pop_quick_text_c(*args)
    pop_quick_text(*args) do |win| win.align_to!(anchor: 5) end
  end

  def self.center_windows(xc=true, yc=true, windows=@windows)
    winrect = MACL::Surface::Tool.area_rect(*windows)
    rect = MACL::Surface::Tool.center(Graphics.rect, winrect)
    dif_x = xc ? winrect.x - rect.x : 0
    dif_y = yc ? winrect.y - rect.y : 0
    windows.each { |win| win.x -= dif_x; win.y -= dif_y }
  end

end
