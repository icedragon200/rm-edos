
NULL = 0

class String

  ##
  # "F#3", "C2", "Cb2"
  def to_note
    base, sub, octave = self.split("")
    {
      "A" => 9,
      "B" => 11,
      "C" => 0,
      "D" => 2,
      "E" => 4,
      "F" => 5,
      "G" => 7
    }[base.upcase] +
    (octave || sub).to_i * 12 +
    (sub == "#" ? 1 : (sub == "b" ? -1 : 0))
  end

  ##
  # "1/1", "1/2"
  def to_note_length
    num, dem = self.split("/")
    return (num.to_f / dem.to_f) * 4.0
  end

end

class PatternMatrix

  attr_reader :notes, :volume

  def initialize(size, note_length)
    @note_length = note_length
    @size = size
    @notes  = Array.new(size, -1)
    @volume = Array.new(size, 0x7F)
  end

  def set_note(id, note, vel = @volume[id])
    @notes[id] = note
    @volume[id] = vel
  end

  def set_volume(id, vel)
    @volume[id] = vel
  end

  def render_events(channel_id)
    delta = @note_length
    last_delta = 0
    @size.times.map do |i|
      note = @notes[i]
      vol  = @volume[i]

      if note > -1
        result = [MIDI::NoteOn.new( channel_id, note, vol, last_delta),
                  MIDI::NoteOff.new(channel_id, note, vol,      delta)]
        last_delta = 0
      else
        last_delta += delta
        result = []
      end
      result
    end.flatten + [MIDI::NoteOn.new(channel_id, NULL, 0, last_delta),
                   MIDI::NoteOff.new(channel_id, NULL, 0, 0)]
  end

end

require 'midilib'

module MIDI

  (1..16).each { |i| const_set("CHANNEL#{i}", i-1) }
  DRUM_CHANNEL = 9

  class Sequence
    hsh = {}
    (1..8).each do |y|
      (1..6).each do |x|
        x = 2 ** x
        hsh["#{y}/#{x}"] = (y.to_f / x.to_f) * 4.0
      end
    end
    NOTE_TO_LENGTH.merge!(hsh)
    puts NOTE_TO_LENGTH
  end

end

def midi_make
  #require 'midilib/sequence'
  #require 'midilib/consts'
  include MIDI

  seq = Sequence.new()

  # Create a first track for the sequence. This holds tempo events and stuff
  # like that.
  track = Track.new(seq)
  seq.tracks << track
  track.events << Tempo.new(Tempo.bpm_to_mpq(120))
  track.events << MetaEvent.new(META_SEQ_NAME, 'Sequence Name')

  d16th = seq.note_to_delta('1/16')
  kick_pattern = PatternMatrix.new(16, d16th)
  snare_pattern = PatternMatrix.new(16, d16th)
  hihat_pattern = PatternMatrix.new(16, d16th)

  4.times { |i| kick_pattern.set_note(i * 4, "C3".to_note) }
  2.times { |i| snare_pattern.set_note(i * 8, "D3".to_note) }; snare_pattern.notes.rotate!(4)
  8.times { |i| hihat_pattern.set_note(i * 2, "F#3".to_note) }

  # Create a track to hold the notes. Add it to the sequence.
  trck_kick = Track.new(seq)
  trck_snare = Track.new(seq)
  trck_hihat = Track.new(seq)
  trck_melody = Track.new(seq)
  seq.tracks << trck_kick
  seq.tracks << trck_snare
  seq.tracks << trck_hihat
  seq.tracks << trck_melody

  trck_kick.name = 'KK'
  trck_snare.name = 'SN'
  trck_hihat.name = 'HH'
  trck_melody.name = 'Melody'
  trck_kick.instrument = GM_PATCH_NAMES[0]

  trck_kick.events << Controller.new(DRUM_CHANNEL, CC_VOLUME, 101)
  trck_kick.events << ProgramChange.new(DRUM_CHANNEL, 0, 0)
  trck_snare.events << Controller.new(DRUM_CHANNEL, CC_VOLUME, 101)
  trck_snare.events << ProgramChange.new(DRUM_CHANNEL, 0, 0)
  trck_hihat.events << Controller.new(DRUM_CHANNEL, CC_VOLUME, 101)
  trck_hihat.events << ProgramChange.new(DRUM_CHANNEL, 0, 0)

  4.times do |i|
    trck_kick.events.concat(kick_pattern.render_events(DRUM_CHANNEL))
    trck_snare.events.concat(snare_pattern.render_events(DRUM_CHANNEL))
    trck_hihat.events.concat(hihat_pattern.render_events(DRUM_CHANNEL))
  end

  trck_melody
  #quarter_note_length = seq.note_to_delta('quarter')
  #["C5", "D5", "E5", "F5", "G5", "A5", "B5", "C6"].each do |str|
  #  note = str.note
  #  track.events << NoteOn.new(0, note, 127, 0)
  #  track.events << NoteOff.new(0, note, 127, quarter_note_length)
  #end

  # Calling recalc_times is not necessary, because that only sets the events'
  # start times, which are not written out to the MIDI file. The delta times are
  # what get written out.

  # track.recalc_times

  File.open('from_scratch.mid', 'wb') { | file |
    seq.write(file)
  }

  `timidity from_scratch.mid` #-c tim.cfg`
  exit
end
