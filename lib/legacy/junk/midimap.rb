#
# wavey/lib/piano.rb
#
module Wavey

  module Piano
    PIANO_KEY_FREQ = Array.new(12 * 12) do |i|
      2 ** ((i - 49 - 8) / 12.0) * 440
    end

    TONES = {
      "C" => 0,
      "D" => 2,
      "E" => 4,
      "F" => 5,
      "G" => 7,
      "A" => 9,
      "B" => 11
    }

    def self.str_to_pkey(str)
      data = str.upcase.split('')
      key = 0
      data.each do |ch|
        case ch
        when "A"..."G"
          key += TONES[ch]
        when "#"
          key += 1
        when /(\d+)/
          key += 12 * $1.to_i
        end
      end

      return key
    end

    def self.piano_key(str)
      return PIANO_KEY_FREQ[str_to_pkey(str)]
    end

    def self.test
      puts "C5 freq is #{piano_key("C5")}"
    end

  end

end

def make_midimap(filename)
  color_pairs = [
    [Palette['droid_dark'], Palette['droid_light_ui_enb']],
    [Palette['droid_light'], Palette['droid_dark_ui_enb']]
  ]

  octaves = 10
  midikeys = 0...(12 * octaves)

  rows = 12
  cols = midikeys.last / rows
  cols += 1 if midikeys.last % rows > 0

  w, h = 128, Metric.ui_element_sml
  bmp = Bitmap.new(w * cols, h * rows)

  keys = %w(C C# D D# E F F# G G# A A# B)
  midikeys.each do |key|
    str = yield key, keys

    r = Rect.new(key / rows * w, key % rows * h, w, h)
    bak_color, txt_color = color_pairs[(key / rows) % color_pairs.size]

    bmp.font.color = txt_color
    bmp.font.size = Metric.ui_font_size(:small)
    bmp.font.outline = false

    bmp.fill_rect(r, bak_color)
    bmp.draw_text(r.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                             amount: Metric.contract), str)
  end

  bmp.texture.save(filename)
end

    #make_midimap('midikey_map.png') do |key, keys|
    #  key_str = keys[key % 12] + (key / 12).to_s
    #  freq_str = Wavey::Piano::PIANO_KEY_FREQ[key].round(2).to_s
    #  "%03s - %04s : %s" % [key, key_str, freq_str]
    #end

    #make_midimap('midikey_nummap.png') do |key, keys|
    #  "#{"%03d" % key} - " + keys[key % 12] + (key / 12).to_s
    #end

    #make_midimap('midikey_freqmap.png') do |key, keys|
    #  (keys[key % 12] + (key / 12).to_s) + " " + Wavey::Piano::PIANO_KEY_FREQ[key].round(2).to_s
    #end
