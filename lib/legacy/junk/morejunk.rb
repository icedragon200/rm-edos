module TrackCalls

  def method_added()
  end

  def init_tracker
  end

end

class TestObject

  def ~@
  end

  def !@
  end

  def -@
  end

  def +@
  end

  def []
  end

  def call
    puts "Test"
  end

end

TestObject.new.()
