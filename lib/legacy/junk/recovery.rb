def recovery_console
  loop do
    begin
      print '?> '
      p eval(gets)
    rescue Interrupt 
      break  
    rescue Exception => ex
      p ex
      p ex.backtrace[0]
    end
  end
  puts 
  exit
end

def recovery
  yield
rescue Exception => ex
  p ex
  puts ex.backtrace[0...4]
  puts ">!! Entering Recovery Console"
  recovery_console
end
