class Sprite::Title < Sprite

  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system("title")
    self.anchor_oxy!(anchor: 5)
    self.align_to!(anchor: 8)
    self.x += self.ox
    self.y += self.oy
  end

  def dispose
    super
  end

  def update
    super
  end

end
