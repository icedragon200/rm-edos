class Sprite::TitleText < Sprite

  def initialize(viewport, text)
    super(viewport)
    @text = text
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def refresh
    bmp = self.bitmap = Bitmap.new(360, 96)

    font = bmp.font
    font.name = ["Ubuntu-R"]
    font.size = 72
    font.outline = false
    font.shadow  = true
    font.shadow_color = Palette['black']
    font.shadow_conf = [3, 3]

    bmp.draw_text(bmp.rect, @text, 1)
  end

end
