# Window::Files
# // 12/??/2011
class Window::Files < Window::Selectable

  def initialize(x, y)
    super(x, y, window_width, window_height)
    refresh
    activate.select(0)
  end

  def window_width
    (item_width * col_max) + standard_padding * 2
  end

  def window_height
    ((Graphics.height - 64) / item_height) * item_height + standard_padding * 2
  end

  def item_max
    DataManager.savefile_max
  end

  def col_max
    Graphics.width / item_width
  end

  def item_width
    256 + 4
  end

  def item_height
    96 + 4
  end

  def spacing
    0
  end

  def draw_item( index )
    rect    = item_rect( index ).contract(anchor: 5, amount: 2)
    brect   = Rect.new( 0, 0, 256, 96 )
    header  = DataManager.load_header(index)
    bi      = (header ? header[:save_background_index] : 0) || 0
    brect.y = bi * brect.height
    trect   = rect.dup
    trect.x+= 30
    artist do |art|
      bmp = Cache.system("save_borders(window)")
      contents.blt(rect.x, rect.y, bmp, brect,
        header ? 255 : art.translucent_alpha)
      contents.font.set_style('window_header')

      art.draw_text(trect.x, trect.y, trect.width, 14, "File: #{index+1}")
      art.draw_party_characters(rect.x+42+16, rect.y+48+32, index)
      art.draw_playtime(rect.x+28, rect.y+22, 86, 2, index)
    end
  end

  def standard_artist
    Artist::Files
  end

end
