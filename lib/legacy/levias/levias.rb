#
# EDOS/src/levias/levias.rb
#   by IceDragon
#   dc 30/03/2013
#   dm 30/03/2013
module Levias

  VERSION = "0.0.1".freeze

end

module Levias
  class Scene < ::Scene::Base

  end
end

dir = File.dirname(__FILE__)

%w(map area stage).each do |fn|
  require File.join(dir, 'struct', fn)
end

%w(vocab mapmanager).each do |fn|
  require File.join(dir, 'module', fn)
end

%w(panel lp minimap).each do |fn|
  require File.join(dir, 'panel', fn)
end

%w(map ui).each do |fn|
  require File.join(dir, 'spriteset', fn)
end

%w(map).each do |fn|
  require File.join(dir, 'scene', fn)
end