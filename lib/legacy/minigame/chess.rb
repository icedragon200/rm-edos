#
# EDOS/src/minigame/chess.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess

    VERSION = "0.0.1"

  end
end

dir = File.dirname(__FILE__)
%w(role piece team field_piece move_rule sprite funcs).each do |fn|
  require File.join(dir, 'chess', fn)
end
