#
# EDOS/src/minigame/chess/field_piece.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess
    class FieldPiece

      attr_accessor :name, :piece
      attr_accessor :pos, :team

      def initialize(piece, name=piece.name)
        @piece = piece
        @name  = name
        @pos   = MACL::Pos3.new

        @move_count = 0 # how many moves has this piece made?
        @travelled  = 0 # how far has this piece travelled
        @prev_poss  = []
      end

      def team_sym
        @team == Chess::TEAM_WHITE ? "W" : "B"
      end

      def role
        @piece.role
      end

      def x
        @pos.x
      end

      def x=(new_x)
        @pos.x = new_x
      end

      def y
        @pos.z
      end

      def y=(new_y)
        @pos.z = new_y
      end

      def set_pos(x, y)
        self.x, self.y = x, y
        self
      end

      def move(new_x, new_y)
        diff_x, diff_y = x - new_x, y - new_y
        @prev_poss.push(@pos.dup)

        set_pos(new_x, new_y)

        @move_count += 1
        @travelled += diff_y.abs + diff_x.abs
        self
      end

      def calc_move_a
        axis = @pos.axis
        raise(RuntimeError, "Could not get a valid axis") unless axis
        a = Chess.calc_move_a_by_role(axis, @piece.role, @move_count)
        a.reject(&:zero?).map { |pos| pos + @pos }
      end

    end
  end
end