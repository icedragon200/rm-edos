#
# EDOS/src/minigame/chess/funcs.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess

    def self.move_rule_str_by_role(role, move_count=0)
      ##
      # f     - Forward
      # b     - Backward
      # n     - North
      # s     - South
      # e     - East
      # w     - West
      # &     - Joint rule
      # :     - seperate rule
      # <int> - distance to move
      # =     - maximum available distance
      case role
      when RolePawn   then move_count == 0 ? "f2" : "f1"
      when RoleRook   then "n=:s=:e=:w="
      when RoleKnight then "n2&(e1):n2&(w1):s2&(e1):s2&(w1):e2&(n1):e2&(s1):w2&(n1):w2&(s1)"
      when RoleBishop then "nw=:ne=:sw=:se="
      when RoleQueen  then "n=:s=:e=:w=:nw=:ne=:sw=:se="
      when RoleKing   then "n1:s1:e1:w1:nw1:ne1:sw1:se1"
      end
    end

    def self.mk_move_rule
      MoveRule.new
    end

    def self.parse_rule_str(rule_str)
      move_rule = mk_move_rule
      while c = rule_str.shift
        case c
        when "f" then move_rule.axes.push(:forward)
        when "b" then move_rule.axes.push(:backward)
        when "n" then move_rule.axes.push(:north)
        when "s" then move_rule.axes.push(:south)
        when "e" then move_rule.axes.push(:east)
        when "w" then move_rule.axes.push(:west)
        when "=" then move_rule.dist = -1
        when "&"
          new_rule_str = []
          n = rule_str.shift
          raise("Invalid Joint") if n != "("
          indent = 0
          while n
            case n
            when "("
              new_rule_str.push(n) if indent > 0
              indent += 1
            when ")"
              indent -= 1
              new_rule_str.push(n) if indent > 0
            else
              new_rule_str.push(n)
            end
            n = rule_str.shift
            break if indent == 0
          end
          move_rule.next_rule = parse_rule_str(new_rule_str)
        when /(\d+)/
          num_str = c.dup
          num_str.concat(rule_str.shift) while rule_str[0] =~ /\d+/
          move_rule.dist = num_str.to_i
        end
      end
      return move_rule
    end

    def self.move_rule_str_to_move_rules(move_rule_str)
      move_rule_str.split(':').map { |rule| parse_rule_str(rule.split("")) }
    end

    def self.calc_move_a_by_role(axis, role, move_count=0)
      str = move_rule_str_by_role(role, move_count)
      move_rules = move_rule_str_to_move_rules(str)
      move_a = move_rules.map { |move_rule| move_rule.calc_pos_a(axis) }
      return move_a.flatten.uniq
    end

  end
end