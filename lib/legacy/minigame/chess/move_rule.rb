#
# EDOS/src/minigame/chess/move_rule.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess
    class MoveRule

      attr_accessor :axes, :dist, :next_rule

      def initialize
        @axes = []
        @dist = 0
        @next_rule = nil
      end

      def calc_axis_vec(forward_axis)
        return @axes.inject(MACL::Pos3.new(0, 0, 0, MACL::Pos3::UNKNOWN)) do |r, axis|
          if axis.is_a?(Symbol)
            axis =case axis
                  when :forward  then forward_axis
                  when :backward then -forward_axis
                  when :north    then MACL::Pos3::NORTH
                  when :south    then MACL::Pos3::SOUTH
                  when :west     then MACL::Pos3::WEST
                  when :east     then MACL::Pos3::EAST
                  end
          end
          if !axis.kind_of?(MACL::Pos3)
            raise(RuntimeError, "Invalid Axis #{axis.inspect}")
          end
          r.add!(axis)
          r
        end
      end

      def calc_pos_a(axis)
        dist = @dist == -1 ? 8 : @dist
        pos = MACL::Pos3.new(0, 0, 0, MACL::Pos3::UNKNOWN)
        vec = calc_axis_vec(axis)
        pos_a = (0..dist).map { |i| pos.dup.move_straight(i, vec) }
        ext_pos = pos.dup.move_straight(dist, vec).freeze
        pos_a.concat(@next_rule.calc_pos_a(axis).map { |apos| apos + ext_pos }) if @next_rule
        return pos_a
      end

      # final target
      def calc_pos(axis)
        dist = @dist == -1 ? 8 : @dist
        pos = MACL::Pos3.new(0, 0, 0, MACL::Pos3::UNKNOWN)
        vec = calc_axis_vec(axis)
        pos.move_straight(dist, vec)
        pos.add!(@next_rule.calc_pos) if @next_rule
        return pos
      end

      def to_s
        dist_s = @dist == -1 ? "=" : @dist.to_s
        str = @axes.map { |axis| self.class.axis_to_str(axis) }.join('') + dist_s
        str.concat("&(#{@next_rule.to_s})") if @next_rule
        return str
      end

      def self.axis_to_str(axis)
        case axis
        when :forward  then "f"
        when :backward then "b"
        when :north    then "n"
        when :south    then "s"
        when :east     then "e"
        when :west     then "w"
        end
      end

    end
  end
end