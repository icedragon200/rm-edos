#
# EDOS/src/minigame/chess/piece.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess

    Piece = Struct.new(:name, :role)

    Pawn   = Piece.new('Pawn'  , RolePawn)
    Rook   = Piece.new('Rook'  , RoleRook)
    Bishop = Piece.new('Bishop', RoleBishop)
    Knight = Piece.new('Knight', RoleKnight)
    Queen  = Piece.new('Queen' , RoleQueen)
    King   = Piece.new('King'  , RoleKing)

  end
end