#
# EDOS/src/minigame/chess/role.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess

    Role  = Struct.new(:name)

    RolePawn   = Role.new('Pawn')
    RoleRook   = Role.new('Rook')
    RoleBishop = Role.new('Bishop')
    RoleKnight = Role.new('Knight')
    RoleQueen  = Role.new('Queen')
    RoleKing   = Role.new('King')

  end
end