#
# EDOS/src/minigame/chess/sprite.rb
#   dc 02/04/2013
#   dm 02/04/2013
# vr 0.0.1
module Minigame
  module Chess
    module Sprite
      class Piece < ::Sprite

        attr_reader :fpiece

        def initialize(viewport, fpiece)
          super(viewport)
          @fpiece = fpiece
          @pos_tween = MACL::Tween2.new(15, :sine_in, [0, 0], [0, 0])
          @last_pos_a = []
          create_bitmap
          refresh
        end

        def dispose
          dispose_bitmap_safe
          super
        end

        def create_bitmap
          self.bitmap = Bitmap.new(32, 32)
        end

        def refresh
          bitmap = self.bitmap
          rect = bitmap.rect.dup
          text_rect = rect.dup
          text_rect.height = 8
          text_rect.y = bitmap.width - text_rect.height
          colors = @fpiece.team == Chess::TEAM_WHITE ? DrawExt::BLUE_BAR_COLORS : DrawExt::RED_BAR_COLORS

          bitmap.clear
          DrawExt.draw_gauge_ext_sp4(bitmap,
                                     rect.contract(anchor: 5, amount: 4),
                                     1.0, colors)
          #if @fpiece.team == Chess::TEAM_WHITE
          bitmap.font.set_style('simple_white')
          bitmap.draw_text(text_rect, @fpiece.name, 1)
        end

        def update
          super
          if @last_pos_a != n = [@fpiece.x, @fpiece.y]
            @last_pos_a = n
            x, y = @pos_tween.result
            @pos_tween.setup_pairs([x, @last_pos_a[0]], [y, @last_pos_a[1]])
            @pos_tween.reset_tick
          end
          @pos_tween.update if @pos_tween.active?
          x, y = @pos_tween.result
          self.x = x * bitmap.width
          self.y = y * bitmap.height
        end

      end
    end
  end
end
