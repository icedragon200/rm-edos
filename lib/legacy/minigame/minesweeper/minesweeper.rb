class Minigame::Minesweeper

  attr_reader :result
  attr_reader :mine_table
  attr_reader :reveal_table
  attr_accessor :redraw
  attr_accessor :need_refresh

  ID_MINE = 9
  RESULT_WIN = 0
  RESULT_LOSE = 1

  def initialize
    @width = 13 # // 9
    @height = 9
    @mine_count = 10
  end

  def new_game(width=13, height=9, mine_count=20)
    @width = width
    @height = height
    @mine_count = mine_count
    xys = []
    xy = nil
    @mine_table = Table.new(@width, @height)
    size = (@mine_table.xsize * @mine_table.ysize)
    @reveal_table = Table.new(@mine_table.xsize, @mine_table.ysize, 2)

    for x in 0...@mine_table.xsize
      for y in 0...@mine_table.ysize
        xys.push([x, y])
      end
    end

    @mine_count = @mine_count.clamp(1,size-[@mine_table.xsize,@mine_table.ysize].max)
    @mines = Array.new(mine_count){xy=xys.pick!();@mine_table[xy[0],xy[1]]=ID_MINE;xy}

    for x in 0...@mine_table.xsize
      for y in 0...@mine_table.ysize
        i = @mine_table[x, y]
        @mine_table[x, y] = i == ID_MINE ? i : mines_sorrounding_count(x, y)
      end
    end

    @revealed = []
    @redraw = []
    @need_refresh = true
    @mine_uncovered = nil
    @result = nil
    @total_reveals = size - @mines.size
    #puts
    #for y in 0...@mine_table.ysize
    #  puts ""
    #  for x in 0...@mine_table.xsize
    #    print @mine_table[x,y] == 9 ? "#" : @mine_table[x,y]
    #  end
    #end
  end

  def check_conditions()
    if @mine_uncovered
      end_game(RESULT_LOSE)
      return true
    elsif @total_reveals == @revealed.size
      end_game(RESULT_WIN)
      return true
    end
    return false
  end

  def end_game(result)
    @result=result
    case @result
    when RESULT_WIN
      reveal_all()
    when RESULT_LOSE
      reveal_mines()
    end
  end

  def mine_here?(x,y)
    @mine_table[x,y] == ID_MINE
  end

  def bad_flag?(x,y)
    @mine_table[x,y] != ID_MINE && @reveal_table[x,y,1] > 0
  end

  def reveal_spaces(x,y)
    return if @mine_table.oor?(x,y)
    return @mine_uncovered=[x,y] if mine_here?(x,y)
    return reveal_space(x,y) if @mine_table[x,y] > 0
    nodes = []
    closed_nodes = Table.new(@mine_table.xsize, @mine_table.ysize)
    open_nodes = [[x,y]]
    node = nil
    maxit = closed_nodes.xsize * closed_nodes.ysize
    n = nil
    maxit.times() do
      node = open_nodes.pop
      break if node.nil?()
      unless closed_nodes[node[0],node[1]] == 1
        closed_nodes[node[0],node[1]] = 1
        nodes << [node[0],node[1]]
        for oy in -1..1
          for ox in -1..1
            n = [node[0] + ox, node[1] + oy]
            next if n[0] < 0 || n[1] < 0
            if closed_nodes[*n] != 1 && !closed_nodes.oor?(*n)
              if !(@mine_table[*n] > 0)
                open_nodes << n
              else
                reveal_space(*n)
              end
            end
          end
        end
      end
    end
    nodes.each { |n| reveal_space(*n) }
  end

  def reveal_all()
    for y in 0...@mine_table.ysize
      for x in 0...@mine_table.xsize
        reveal_space(x, y)
      end
    end
    @need_refresh = true
  end

  def reveal_mines()
    @mines.each {|n|reveal_space(*n)}
    @need_refresh = true
  end

  def reveal_space(x,y)
    return if @mine_table.oor?(x,y)
    return if @reveal_table[x,y,0] == 1 || @reveal_table[x,y,1] > 0
    @reveal_table[x,y,0] = 1
    @revealed << [x, y]
    @redraw << [x,y]
  end

  def flag_space(x,y)
    return if @mine_table.oor?(x,y)
    @reveal_table[x,y,1] = @reveal_table[x,y,1].succ.modulo(3)
    @redraw << [x,y]
  end

  def mines_sorrounding_count(x,y)
    result = 0
    for i in (-1..1)
      for i2 in (-1..1)
        result += 1 if(@mine_table[x+i,y+i2] == 9) unless @mine_table.oor?(x + i, y + i2)
      end
    end
    result
  end

end