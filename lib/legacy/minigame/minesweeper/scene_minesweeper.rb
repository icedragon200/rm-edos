class Scene::Minesweeper < Scene::Base

  def start
    super
    create_background
    @minesweeper = Minigame::Minesweeper.new
    @mspriteset = Minigame::Minesweeper::Spriteset.new(@viewport)
    create_all_windows
    new_game
    @mspriteset.minesweeper = @minesweeper
  end

  def terminate
    @mspriteset.dispose
    super
  end

  def create_background
    super
    @background.bitmap = scene_manager.background_bitmap
  end

  def check_conditions
    at_end_game if @minesweeper.check_conditions
  end

  def new_game
    @minesweeper.new_game(Graphics.width/32,(Graphics.height-32)/32)
    @timer_window.reset
    @timer_window.deactivate
  end

  def at_end_game
    @timer_window.deactivate
    case @minesweeper.result
    when Minigame::Minesweeper::RESULT_WIN
      pop_quick_text_c("=w=d WIN")
      pop_quick_text_c("Time Taken: #{@timer_window.time} sec(s)")
    when Minigame::Minesweeper::RESULT_LOSE
      pop_quick_text_c(".x.; LOSE")
    end
    create_dim_background
    pop_confirm_window( :text => "New Game?" ) { |win| win.salign!(5) }
    dispose_dim_background
    if(@confirm_result == 0)
      new_game
    elsif(@confirm_result == 1)
      return_scene
    end
  end

  def create_all_windows
    create_timer_window
  end

  def create_timer_window
    @timer_window = Minigame::Minesweeper::Window_Timer.new(0,0)
    window_manager.add(@timer_window)
  end

  def update_basic
    super
    @mspriteset.update
  end

  def update
    super

    @mspriteset.x = (Graphics.width - @mspriteset.width) / 2
    @mspriteset.y = 32
    @mspriteset.z = 1
    @mspriteset.update

    x32, y32 = (Mouse.x / 32).to_i, (Mouse.y / 32).to_i
    ox32, oy32 = (@mspriteset.x / 32).to_i, (@mspriteset.y / 32).to_i
    x32 -= ox32
    y32 -= oy32

    if Mouse.left_click?
      @timer_window.activate
      @minesweeper.reveal_spaces(x32,y32)
      check_conditions
    elsif Mouse.right_click?
      @timer_window.activate
      @minesweeper.flag_space(x32,y32)
    end

  end

end