class Minigame::Minesweeper::Spriteset

  @@tileset = nil
  @@tilesize = 32

  def self.tileset
    if @@tileset.nil? || @@tileset.disposed?
      @@tileset = IEI::Tileset.new(7, 1, @@tilesize, @@tilesize)
      [DrawExt::TRANS_BAR_COLORS, # // Tile
       DrawExt::RED_BAR_COLORS,   # // Mine
       DrawExt::GREEN_BAR_COLORS, # // Flag
       DrawExt::BLUE_BAR_COLORS,  # // Hidden
       DrawExt::YELLOW_BAR_COLORS,# // ?
       DrawExt::WHITE_BAR_COLORS, # // (Unused)
       DrawExt::BLACK_BAR_COLORS  # // Bad Flag
      ].each_with_index do |c, i|
        @@tileset.bitmap.draw_gauge_ext(@@tileset.cell_r(i), 1.0, c)
      end
    end
    @@tileset
  end

  def initialize(viewport=nil)
    @base_layer = Sprite.new(viewport)
    @flag_layer = Sprite.new(viewport)
    @grid = MACL::Grid.new(8, 1, tilesize, tilesize)
    @minesweeper = nil
    self.x, self.y, self.z = 0,0,0
  end

  def tilesize
    @@tilesize
  end

  attr_reader :minesweeper
  def minesweeper=(n)
    @minesweeper = n
    refresh
  end

  def data
    @minesweeper.mine_table
  end

  def rdata
    @minesweeper.reveal_table
  end

  attr_reader :x
  def x=(n)
    @flag_layer.x = @base_layer.x = @x = n
  end

  attr_reader :y
  def y=(n)
    @flag_layer.y = @base_layer.y = @y = n
  end

  attr_reader :z
  def z=(n)
    @flag_layer.z = 1 + @base_layer.z = @z = n
  end

  def width
    @base_layer.width
  end

  def height
    @base_layer.height
  end

  def viewport=(n)
    @flag_layer.viewport = @base_layer.viewport = n
  end

  def refresh
    @base_layer.dispose_bitmap_safe
    @flag_layer.dispose_bitmap_safe
    @grid.columns = data.xsize
    @grid.rows    = data.ysize
    @base_layer.bitmap = Bitmap.new(data.xsize*tilesize,data.ysize*tilesize)
      rect = @base_layer.bitmap.rect
    @flag_layer.bitmap = Bitmap.new(rect.width, rect.height)
    redraw
  end

  def redraw
    for y in 0...data.ysize
      for x in 0...data.xsize
        redraw_tile(x, y)
      end
    end
  end

  def redraw_tile(x,y)
    id   =  data[x,y]
    rid  = rdata[x,y,0]
    rid2 = rdata[x,y,1]
    ts = self.class.tileset
    b  = ts.bitmap
    gr = @grid.cell_r(x,y)
    @base_layer.bitmap.clear_rect(gr)
    if(rid == 1)
      if(id != 9)
        text = id == 0 ? "" : id
        @base_layer.bitmap.blt(gr.x,gr.y,b,ts.cell_r(0))
        @base_layer.bitmap.font.size = tilesize
        @base_layer.bitmap.font.color = Palette['sys_orange1']
        @base_layer.bitmap.draw_text(gr,text,1)
      else
        if(@minesweeper.bad_flag?(x,y) && @minesweeper.result == 1)
          r = ts.cell_r(6)
        else
          r = ts.cell_r(rid2 > 0 ? 2 : 1)
        end
        @base_layer.bitmap.blt(gr.x,gr.y,b,r)
      end
    #elsif rid == 2
    else
      case(rid2)
      when 1 # // . x . Flag
        r = ts.cell_r(2)
      when 2 # // . x . ?
        r = ts.cell_r(4)
      else # // Normal Space
        r = ts.cell_r(3)
      end
      r = ts.cell_r(6) if @minesweeper.bad_flag?(x,y) && @minesweeper.result == 1
      @base_layer.bitmap.blt(gr.x,gr.y,b,r)
    end
  end

  def dispose
    @base_layer.dispose_all
    @flag_layer.dispose_all
    @disposed = true
  end

  def update
    if @minesweeper.need_refresh
      @minesweeper.need_refresh = false
      @minesweeper.redraw.clear
      refresh
    end
    if @minesweeper.redraw.size > 0
      @minesweeper.redraw.each { |n| redraw_tile(*n) }
      @minesweeper.redraw.clear
    end
  end

end