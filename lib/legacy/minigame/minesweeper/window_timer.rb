class Minigame::Minesweeper::Window_Timer < Shell::Window

  attr_reader :time

  def initialize(x, y)
    super(x, y, window_width, window_height)
    contents.font.set_style('window_header2')
    reset
  end

  def line_height
    24
  end

  def standard_padding
    Metric.padding
  end

  def window_width
    96
  end

  def window_height
    32
  end

  def reset
    @counter = 0
    @time = 0
    refresh
  end

  def refresh
    contents.clear
    @artist.draw_text(0,0,self.contents.width,line_height,"%03d" % @time, 1)
  end

  def update
    super
    update_time if self.active
  end

  def update_time
    @counter = @counter.pred.max(0)
    if @counter.eql?(0)
      @counter = Graphics.frame_rate
      @time += 1
      refresh
    end
  end

end