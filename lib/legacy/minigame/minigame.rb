module Minigame
end

dir = File.dirname(__FILE__)
%w(chess minesweeper paddle pegs).each do |fn|
  require File.join(dir, fn)
end
