module Minigame
  module Paddle
  end
end

require_relative 'paddle/block.rb'
require_relative 'paddle/paddle.rb'
require_relative 'paddle/window_score.rb'

class Scene::Paddle < Scene::Base

  def relative_path
    File.dirname(__FILE__)
  end

  def local_graphic(filename)
    return File.join(relative_path, "graphics/#{filename}")
  end

  def local_audio(filename)
    return File.join(relative_path, "audio/#{filename}")
  end

  def local_se(filename)
    return local_audio("se/#{filename}")
  end

  def start
    super
    create_background

    @stage = Rect.new(0, 64, Graphics.width, Graphics.height - 64)
    @sprites = []

    @se_wall_hit   = SRRI::Audio::SE.new(local_se("wall_hit"))
    @se_paddle_hit = SRRI::Audio::SE.new(local_se("paddle_hit"))
    @se_block_hit  = SRRI::Audio::SE.new(local_se("block_hit"))
    @se_score_up   = SRRI::Audio::SE.new(local_se("score_up"))

    @ball_radian = 0.0

    @paddle = Minigame::Paddle::Paddle.new(0, 0, 104, 24)
    @rect_ball   = MACL::Surface.new(0, 0, 22, 22)

    @paddle.stage = @stage
    @rect_ball.freeform   = false

    @paddle.align_to!(anchor: 2)
    @paddle.y -= @paddle.height * 2

    @rect_ball.cx = @stage.cx
    @rect_ball.cy = @stage.cy

    @sprite_ball = Sprite.new(@viewport)
    @sprite_ball.bitmap = Bitmap.new(local_graphic("ball_gray"))
    @sprite_ball.ox = @sprite_ball.width / 2
    @sprite_ball.oy = @sprite_ball.height / 2

    @sprite_paddle = Sprite.new(@viewport)
    @sprite_paddle.bitmap = Bitmap.new(local_graphic("paddle_red"))

    #create_angle_line() # debug

    @sprites.push(@sprite_ball)
    @sprites.push(@sprite_paddle)

    @sprites_block = []
    @blocks = []

    bmp = Cache.normal_bitmap(local_graphic("element_blue_square"))
    cols = @stage.width / bmp.width
    raise("Not enough space for blocks") if cols <= 0

    n = (cols * 3)

    n.times do |i|
      x = @stage.x + (i % cols) * bmp.width
      y = @stage.y + (i / cols) * bmp.height
      block = Minigame::Paddle::Block.new(x, y, bmp.width, bmp.height)

      sp = Minigame::Paddle::Sprite_Block.new(block, @viewport)
      sp.bitmap = bmp
      sp.ox = sp.bitmap.width / 2
      sp.oy = sp.bitmap.height / 2

      @blocks[i] = block
      @sprites_block.push(sp)
    end

    @use_paddle = true

    unless @use_paddle
      @paddle.y = @stage.y2 + 16
    end

    create_windows()
  end

  def terminate
    dispose_background
    @sprites.each(&:dispose)
    @sprites.clear
    super
  end

  def create_background
    super()

    bmp = @background.bitmap = Bitmap.new(Graphics.width, Graphics.height)
    bmp.gradient_fill_rect(
      bmp.rect, Palette['gray3'], Palette['gray5'], true
    )
    y = bmp.rect.y2 - 48
    color = bmp.get_pixel(0, y)
    color2 = color.dup.hset(alpha: 0)
    bmp.gradient_fill_rect(
      Rect.new(0, y, bmp.width, 48), color, color2, true
    )

    return self
  end

  def dispose_background
    @background.bitmap.dispose if @background.bitmap
    super
  end

  def create_windows
    super
    @score_window = Minigame::Paddle::WindowScore.new(
      @paddle, 0, 0, Graphics.width, 64)

    window_manager.add(@score_window)
  end

  def create_angle_line
    @sprite_line = Sprite.new
    @sprite_line.bitmap = Bitmap.new(72, 12)
    @sprite_line.bitmap.fill_rect(@sprite_line.bitmap.rect, Palette['white'])
    @sprite_line.bitmap.fill_rect(Rect.new(60, 0, 12, 12), Palette['brown3'])
    @sprite_line.ox = @sprite_line.width / 2
    @sprite_line.oy = @sprite_line.height / 2
    @sprite_line.x = @stage.cx
    @sprite_line.y = @stage.cy

    @sprite_angle_text = Sprite.new
    @sprite_angle_text.bitmap = Bitmap.new(192, 24)

    @sprites.push(@sprite_line)
    @sprites.push(@sprite_angle_text)
  end

  def hit_block(block, diff)
    @se_block_hit.play

    block.hit(@paddle, diff)
  end

  def update
    super
    if Input.sr_trigger?(:keyboard, :esc)
      Main.scene_manager.return
      return;
    end

    @blocks.select!(&:update)

    update_paddle() if @use_paddle
    #update_ball_debug()

    if @paddle.ball_docked and @use_paddle
      @rect_ball.cx = @paddle.cx
      @rect_ball.y2 = @paddle.y
    else
      update_collision_with_stage()
      update_collision_with_paddle() if @use_paddle
      update_collision_with_blocks()
    end

    update_positions

    @sprites_block.reject! do |s|
      s.block.remove_sprite? ? (s.dispose; true) : (s.update; false)
    end
    #update_angle_line()
  end

  def update_collision_with_stage
    @ball_radian %= Math::PI * 2

    speed = 6.0
    @ball_x_vel = (speed * Math.cos(@ball_radian))#.to_i
    @ball_y_vel = (speed * Math.sin(@ball_radian))#.to_i

    @rect_ball.x += @ball_x_vel
    @rect_ball.y += @ball_y_vel

    # left
    if @rect_ball.x < @stage.x
      @rect_ball.x = @stage.x
      xa = Math.atan2(@ball_y_vel, -@ball_x_vel)
      @ball_radian = xa

      @se_wall_hit.play
    # right
    elsif @rect_ball.x2 > @stage.x2
      @rect_ball.x2 = @stage.x2
      xa = Math.atan2(@ball_y_vel, -@ball_x_vel)
      @ball_radian = xa

      @se_wall_hit.play
    end

    # collide with top
    if @rect_ball.y < @stage.y
      @rect_ball.y = @stage.y
      ya = Math.atan2(1, @ball_x_vel)
      @ball_radian = ya

      @se_wall_hit.play
    # Collide with bottom : Gameover if @use_paddle
    elsif @rect_ball.y2 > @stage.y2
      if @use_paddle
        Sound.play_buzzer
        @paddle.ball_docked = true
        @ball_x_vel = 0.0
        @ball_y_vel = 0.0
      else
        @rect_ball.y2 = @stage.y2
        ya = Math.atan2(-1, @ball_x_vel)
        @ball_radian = ya
        #@ball_radian -= ya

        @se_wall_hit.play
      end
    end
  end

  def update_collision_with_paddle
    if (@rect_ball.y2.between?(@paddle.y, @paddle.cy) and
     @rect_ball.cx.between?(@paddle.x, @paddle.x2))
      x = @rect_ball.cx - @paddle.cx
      y = @rect_ball.cy - @paddle.cy
      @ball_radian = Math.atan2(y, x) #-@ball_radian
      @rect_ball.y2 = @paddle.y
      @se_paddle_hit.play
    end
  end

  def update_collision_with_blocks
    drop_b = []
    x, y, x2, y2 = @rect_ball.x, @rect_ball.y, @rect_ball.x2, @rect_ball.y2
    @blocks.each do |b|
      next if b.broken?
      if (x.between?(b.x, b.x2) && y.between?(b.y, b.y2)) ||
        (x2.between?(b.x, b.x2) && y2.between?(b.y, b.y2))

        dx, dy = b.cx - @rect_ball.cx, b.cy - @rect_ball.cy
        hit_block(b, [dx, dy])
        @ball_radian = Math.atan2(-dy, -dx)
      end
    end
  end

  def update_paddle()
    @paddle.update
  end

  def update_ball_debug()
    if Input.press?(:LEFT)
      @ball_radian -= 0.1
    elsif Input.press?(:RIGHT)
      @ball_radian += 0.1
    # 90*
    elsif Input.trigger?(:UP)
      @ball_radian -= Math::PI / 2
    elsif Input.trigger?(:DOWN)
      @ball_radian += Math::PI / 2
    # Flip
    elsif Input.trigger?(:C)
      @ball_radian = -@ball_radian
    # 0
    elsif Input.trigger?(:B)
      @ball_radian = 0
    # Reset!
    elsif Input.trigger?(:A)
      @ball_radian = 0
      @rect_ball.cx = @stage.cx
      @rect_ball.cy = @stage.cy
    end
  end

  def update_positions
    @sprite_ball.x, @sprite_ball.y = @rect_ball.cx, @rect_ball.cy
    @sprite_paddle.x, @sprite_paddle.y = @paddle.x, @paddle.y
  end

  def update_angle_line
    @sprite_line.angle = (
      Math::atan2(
        @rect_ball.cy - @sprite_line.y,
        @rect_ball.cx - @sprite_line.x) * 180 / Math::PI)

    if @last_radian != @ball_radian
      @last_radian = @ball_radian
      @sprite_angle_text.bitmap.clear
      @sprite_angle_text.bitmap.draw_text(
        @sprite_angle_text.bitmap.rect,
        "Angle: #{(@last_radian * 180 / Math::PI).to_i}")
    end
  end

end
