class Minigame::Paddle::Sprite_Block < Sprite

  attr_accessor :block

  def initialize(block, viewport=nil)
    @block = block
    super(viewport)
  end

  def update
    super
    self.x = @block.cx
    self.y = @block.cy
  end

end

class Minigame::Paddle::Block < MACL::Surface::Surface2

  def initialize(x, y, w=24, h=24)
    super(x, y, x + w, y + h)
    self.freeform = false

    @broken = false
    @tween = nil

    @remove_sprite = false
  end

  def remove_sprite?
    return @remove_sprite
  end

  def broken?
    return !!@broken
  end

  def hit(paddle, (diff_x, diff_y))
    @broken = true
    paddle.score += 200

    x, y = self.x, self.y
    tx, ty = x + diff_x, y + diff_y

    @tween = MACL::Tween::Sequencer.new()

    @tween.add_tween([x, y], [tx, ty], :back_out, MACL::Tween.frame_to_sec(15))
    @tween.add_tween([tx, ty], [x, y], :back_out, MACL::Tween.frame_to_sec(15))
    @tween.cycles = 1
  end

  def update
    return false if @remove_sprite

    if @tween
      @tween.update
      @remove_sprite = @tween.done?
      self.x, self.y = *@tween.values unless @remove_sprite
    end

    return true
  end

end
