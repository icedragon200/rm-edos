#
# EDOS/src/minigame/paddle/paddle.rb
#
class Minigame::Paddle::Paddle < MACL::Surface::Surface2

  attr_accessor :ball_docked, :stage, :score

  def initialize(x, y, w=104, h=24)
    super(x, y, x + w, y + h)
    self.freeform = false
    @stage = nil
    @score = 0
    @accel_x = 0.98
    @decel_x = 0.58
    @vel_x = 0

    @ball_docked = true
  end

  def update
    if @vel_x > 0
      @vel_x = [@vel_x - @decel_x, 0].max
    elsif @vel_x < 0
      @vel_x = [@vel_x + @decel_x, 0].min
    end

    if Input.trigger?(:C) and @ball_docked
      r = @stage
      x = self.cx - r.cx
      y = self.cy - r.cy
      @ball_docked = false
    end

    if Input.press?(:LEFT)
      @vel_x -= @accel_x
    elsif Input.press?(:RIGHT)
      @vel_x += @accel_x
    end

    self.x += @vel_x
    self.x = [self.x, @stage.x].max
    self.x2 = [self.x2, @stage.x2].min
  end

end
