class Minigame::Paddle::WindowScore < Shell::Window

  def initialize(paddle, x, y, w, h)
    @paddle = paddle
    super(x, y, w, h)
    self.windowskin = Window::SkinCache.bitmap("window_glass")
  end

  def update
    super

    if @last_score != @paddle.score
      @last_score = @paddle.score

      refresh()
    end
  end

  def refresh
    bmp = contents
    bmp.clear

    bmp.draw_text(Rect.new(0, 0, 192, 24), "Score #@last_score")
  end

end
