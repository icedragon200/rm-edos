#
# src/minigame/pegs.rb
# vr 0.5
module Minigame
  class Pegs

    def initialize
      @target_pegs = []
    end

    def setup(size=4, retries=8, seed=rand(0xFFFFFF))
      @size, @retries = size, retries
      rnd = Random.new(seed)

      @target_pegs = Array.new(size){1+rnd.rand(6)}
      @peg_list    = Array.new(retries) { Array.new(size,0) }
      @line, @column = 0, 0
    end

    def match?(n)
      @target_pegs == n
    end

    def mark_match(n)
      result = Array.new(@pegs.size){false}
      @target_pegs.each_with_index do |id,i|
        result[i] = id == n[i]
      end
      result
    end

    attr_accessor :line
    attr_accessor :column

    def xy ; return @line, @column ; end

    def current_line
      @peg_list[@line]
    end

    def goto(sym)
      case(sym)
      when :next_line
        @line = (@line+1).minmax(0,@retries)
      when :prev_line
        @line = (@line-1).minmax(0,@retries)
      when :start
        @line = 0
      when :end
        @line = @retries
      end
    end

    def check_condition
      match?(current_line)
    end

  end

  module Spriteset
    class Pegs

      @@tileset = nil
      @@tilesize = 32

      def self.tileset
        if(@@tileset.nil?() || @@tileset.disposed?())
          @@tileset = IEI::Tileset.new(7,1,@@tilesize,@@tilesize)
          [DrawExt::TRANS_BAR_COLORS, # // Empty
           DrawExt::RED_BAR_COLORS,   # //
           DrawExt::GREEN_BAR_COLORS, # //
           DrawExt::BLUE_BAR_COLORS,  # //
           DrawExt::YELLOW_BAR_COLORS,# //
           DrawExt::WHITE_BAR_COLORS, # //
           DrawExt::BLACK_BAR_COLORS  # //
          ].each_with_index { |c, i|
            b = CacheExt.make_button("",c,@@tileset.cell_width,@@tileset.cell_height)
            @@tileset.bitmap.blt(@@tileset.cell_r(i).x,0,b,b.rect) ; b.dispose
          }
        end
        @@tileset
      end

      def initialize(viewport)
        @viewport = viewport
        @layer = Sprite.new(viewport)
        self.x, self.y, self.z = 0, 0, 0
      end

      def tilesize
        @@tilesize
      end

      attr_reader :x, :y, :z

      def x=(n)
        @layer.x = @x = n
      end

      def y=(n)
        @layer.y = @y = n
      end

      def z=(n)
        @layer.z = @z = n
      end

      def draw_peg(x,y)
      end

    end
  end

  module Scene
    class Pegs < ::Scene::Base

      def start
        super
        @spriteset = Spriteset::Pegs
        @pegs = Game::Pegs.new
      end

      def update
        super
        if Input.repeat?(:LEFT)
        elsif Input.repeat?(:RIGHT)
        end
      end

    end
  end
end

