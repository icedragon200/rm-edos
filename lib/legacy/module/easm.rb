#
# EDOS/lib/module/easm.rb
#   EASM is a module for writing events using either the EASM language
#   or the ruby DSL, EASM is converted to the proper RMVX/RMVXA event commands
#   as needed.
#   EASM seeks to ease the creation of event lists through its functions.
module EASM

  extend self

  def tokenize(ary)
    result = []
    ary.each_with_index do |line, i|
      (warn "nulls found in ary" && next) unless line
      str = line.delete("\n")
      # is a label line
      tokendata = if data = str.match(/\A\s*(?<name>\S+):/)
        [:label, data[:name]]
      # is a instruction line
      elsif data = str.match(/\A\s*(?<name>\S+)(?:\s+(?<params>.+))?/)
        [:inst, data[:name], data[:params]]
      else
        [:null]
      end
      result.push({ line: i, raw: line, token: tokendata})
    end
    result
  end

  def read_easm_stream(stream)
    @variable_table = {}
    stream_nc_ary = stream.each_line.map do |l|
      l.gsub!(/;(.*)/, '');
      l.delete!("\n")
      l.delete!("\r")
      l
    end
    # preprocess
    stream_pp_ary = stream_nc_ary.map do |line|
      if data = line.match(/\#undef\s+(?<name>\S+)/)
        @variable_table.delete(data[:name])
        nil
      elsif data = line.match(/\#define\s+(?<name>\S+)(?:\s+(?<value>.+))?/)
        v = data[:value].to_s
        @variable_table.each_pair do |kon, vl|
          v.gsub!(kon, vl)
        end
        @variable_table[data[:name]] = v
        nil
      else
        v = line.to_s
        @variable_table.each_pair do |kon, vl|
          v.gsub!(kon, vl)
        end
        v.gsub!(/\A\s+/, '') # remove preceding whitespace
        v.gsub!(/\s+\z/, '') # remove trailing whitespace
        v
      end
    end
    # convert to Token hashes
    return tokenize(stream_pp_ary)
  end

  def load_easm_file(filename)
    tokenlist = read_easm_stream(File.open(filename, "r"))
    tokenlist
  end

  module TokenToEventCommands

    extend self

    @evcmds = {}

    ## TODO
    #
    def tokenlist_to_eventcommands!(tokenlist)

    end

    def evcmd(token_name, evcmd_id, *param_types, &block)
      @evcmds[token_name] = [evcmd_id, param_types, block]
    end

    def evcmd_alias(newname, oldname)
      @evcmds[newname] = oldname
    end

    ### RMVXA Event Commands
    ##    name                     code  Types
    # show_text face_name, face_index, background, position
    evcmd :show_text,               101, :String, :Integer, :String, :Integer
    evcmd :text,                    401, :String
    ##
    # show_choice ["String", "String"], choice_id
    evcmd :show_choice,             102, [String], :Integer
    evcmd :skip_if_choice,          402 # When [**]
    evcmd :skip_if_cancel,          403 # When Cancel
    ##
    # show_num_input var_id, digits_max
    evcmd :show_num_input,          103, :Integer, :Integer
    ##
    # show_item_choice trg_var_id
    evcmd :show_item_choice,        104, :Integer # ???
    ##
    # show_text_scrolling scroll_speed, scroll_no_fast
    evcmd :show_text_scrolling,     105, :Integer, :Boolean
    evcmd :text_scrolling,          405, :String
    ##
    # comment Comment String
    evcmd :comment,                 108, :String
    evcmd :text_comment,            408, :String
    ## TODO
    # conditional_branch
    evcmd :conditional_branch,      111 do |str|
      # TODO convert str to a param list (and compensate for String parameters)
      case param[0] # opcode
      when 0  # Switch
      when 1  # Variable
      when 2  # Self Switch
      when 3  # Timer
      when 4  # Actor
      when 5  # Enemy
      when 6  # Character
      when 7  # Gold
      when 8  # Item
      when 9  # Weapon
      when 10 # Armor
      when 11 # Button
      when 12 # Script
      when 13 # Vehicle
      end
    end

    evcmd :else,                    411
    evcmd :loop,                    112
    evcmd :repeat,                  413
    evcmd :break,                   113
    evcmd :terminate,               115
    ##
    # call_common_event common_event_id
    evcmd :call_common_event,       117, :Integer
    ##
    # label_name:
    # label label_name
    evcmd :label,                   118, :String
    ##
    # jump_to_label label_name
    evcmd :jump_to_label,           119, :String
    ##
    # control_switch range_start, range_end, state
    evcmd :control_switch,          121, :Integer, :Integer, :Integer
    ## TODO
    # control_variable range_start, range_end, opcode, operation, *
    evcmd :control_variable,        122 do |str|
      # TODO convert str to a param list (and compensate for String parameters)
      case param[3]
      when 0 # Constant Integer
      when 1 # Variable Integer
      when 2 # Random Integer, Integer
      when 3 # GameData Integer, Integer, Integer
      when 4 # Script String
      end
    end
    ##
    # control_self_switch self_switch_id, state
    evcmd :control_self_switch,     123, :String, :Integer
    ##
    # control_timer 0, time_in_secs
    # control_timer 1 # turn off
    evcmd :control_timer,           124, :Integer, :Integer
    ##
    # change_gold operation, operand_type, operand
    # change_gold 0, operand_type, operand # increase
    # change_gold 0, 0, integer            #   constant
    # change_gold 0, 1, var_id             #   variable
    # change_gold 1, operand_type, operand # decrease
    # change_gold 1, 0, integer            #   constant
    # change_gold 1, 1, var_id             #   variable
    evcmd :change_gold,             125, :Integer, :Integer, :Integer
    ##
    # change_item item_id, operation, operand_type, operand
    # change_item item_id, 0, operand_type, operand # add
    # change_item item_id, 0, 0, integer            #   constant
    # change_item item_id, 0, 1, var_id             #   variable
    # change_item item_id, 1, operand_type, operand # remove
    # change_item item_id, 1, 0, integer            #   constant
    # change_item item_id, 1, 1, var_id             #   variable
    evcmd :change_item,             126, :Integer, :Integer, :Integer, :Integer
    ##
    # change_weapon weapon_id, operation, operand_type, operand, check_equipped
    # change_weapon weapon_id, 0, operand_type, operand, check_equipped # increase
    # change_weapon weapon_id, 0, 0, integer, check_equipped            #   constant
    # change_weapon weapon_id, 0, 1, var_id, check_equipped             #   variable
    # change_weapon weapon_id, 1, operand_type, operand, check_equipped # decrease
    # change_weapon weapon_id, 1, 0, integer, check_equipped            #   constant
    # change_weapon weapon_id, 1, 1, var_id, check_equipped             #   variable
    evcmd :change_weapon,           127, :Integer, :Integer, :Integer, :Integer, :Integer
    ##
    # change_armor armor_id, operation, operand_type, operand, check_equipped
    # change_armor armor_id, 0, operand_type, operand, check_equipped # increase
    # change_armor armor_id, 0, 0, integer, check_equipped            #   constant
    # change_armor armor_id, 0, 1, var_id, check_equipped             #   variable
    # change_armor armor_id, 1, operand_type, operand, check_equipped # decrease
    # change_armor armor_id, 1, 0, integer, check_equipped            #   constant
    # change_armor armor_id, 1, 1, var_id, check_equipped             #   variable
    evcmd :change_armor,            128, :Integer, :Integer, :Integer, :Integer, :Integer
    ##
    # change_party_member 0, actor_id # add actor
    # change_party_member 1, actor_id # remove actor
    evcmd :change_party_member,     129, :Integer, :Integer
    ## TODO
    # change_battle_bgm bgm
    evcmd :change_battle_bgm,       132, "RPG::BGM"
    ## TODO
    # change_battle_end_me me
    evcmd :change_battle_end_me,    133, "RPG::ME"
    ##
    # change_save_access state
    evcmd :change_save_access,      134, :Integer
    ##
    # change_menu_access state
    evcmd :change_menu_access,      135, :Integer
    ##
    # change_encounter state
    evcmd :change_encounter,        136, :Integer
    ##
    # change_formation_access state
    evcmd :change_formation_access, 137, :Integer
    ##
    # change_window_tone state
    evcmd :change_window_tone,      138, :Tone
    ##
    # transfer_player opcode, map_id, p1, p2, keep_dir, fade_type
    # transfer_player 0, map_id, x, y, keep_dir, fade_type
    # transfer_player 1, map_id, var1, var2, keep_dir, fade_type
    evcmd :set_player_position,     201, :Integer, :Integer, :Integer, :Integer, :Integer
    ##
    # set_vehicle_position vehicle_id, opcode, map_id, p1, p2, keep_dir, fade_type
    # set_vehicle_position vehicle_id, 0, map_id, x, y, keep_dir, fade_type
    # set_vehicle_position vehicle_id, 1, map_id, var1, var2, keep_dir, fade_type
    evcmd :set_vehicle_position,    202, :Integer, :Integer, :Integer, :Integer, :Integer
    ##
    # set_event_position event_id, opcode, map_id, p1, p2, keep_dir, fade_type
    # set_event_position event_id, 0, map_id, x, y, keep_dir, fade_type
    # set_event_position event_id, 1, map_id, var1, var2, keep_dir, fade_type
    # set_event_position event_id, 2, map_id, char_id, _, keep_dir, fade_type
    evcmd :set_event_position,      203, :Integer, :Integer, :Integer, :Integer, :Integer
    ## TODO
    # scroll_map ***
    evcmd :scroll_map,              204, :Integer, :Integer, :Integer
    ## TODO
    # set_move_route char_id, move_route
    evcmd :set_move_route,          205, :Integer, "RPG::Event::MoveRoute"
    ##
    # get_on_off_vehicle
    evcmd :get_on_off_vehicle,      206
    ##
    # change_transperency state
    evcmd :change_transperency,     211, :Integer
    ##
    # show_animation char_id, animation_id, wait_for_animation
    evcmd :show_animation,          212, :Integer, :Integer, :Integer
    ##
    # show_balloon char_id, balloon_id, wait_for_balloon
    evcmd :show_balloon,            213, :Integer, :Integer, :Integer
    ##
    # erase_event
    evcmd :erase_event,             214
    ##
    # set_followers_visiblity state
    evcmd :set_followers_visiblity, 216, :Integer
    ##
    # gather_followers
    evcmd :gather_followers,        217
    ##
    # screen_fadeout
    evcmd :screen_fadeout,          221
    ##
    # screen_fadein
    evcmd :screen_fadein,           222
    ##
    # screen_tint tone, time, wait
    evcmd :screen_tint,             223, :Tone, :Integer, :Integer
    ##
    # screen_flash color, time, wait
    evcmd :screen_flash,            224, :Color, :Integer, :Integer
    ##
    # screen_shake force, time, wait
    evcmd :screen_shake,            225, :Integer, :Integer, :Integer
    ##
    # wait time
    evcmd :wait,                    230, :Integer
    ## TODO
    # show_picture
    evcmd :show_picture,            231, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer
    ## TODO
    # move_picture
    evcmd :move_picture,            232, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer, :Integer
    ##
    # rotate_picture picture_id, angle
    evcmd :rotate_picture,          233, :Integer, :Integer
    ##
    # tint_picture picture_id, tone, time, wait
    evcmd :tint_picture,            234, :Integer, :Integer, :Integer, :Integer
    ##
    # erase_picture picture_id
    evcmd :erase_picture,           235, :Integer
    ## TODO
    # set_weather **
    evcmd :set_weather,             236, :Integer, :Integer, :Integer, :Integer, :Integer
    ## TODO
    # set_weather **
    evcmd :set_weather,             236, :Integer, :Integer, :Integer, :Integer, :Integer

    ## TODO
    # bgm_play bgm
    evcmd :bgm_play,                241, "RPG::BGM"
    ##
    # bgm_fadeout time_in_seconds
    evcmd :bgm_fadeout,             242, :Integer
    ##
    # bgm_save
    evcmd :bgm_save,                243
    ##
    # bgm_resume
    evcmd :bgm_resume,              244
    ## TODO
    # bgs_play bgs
    evcmd :bgs_play,                245, "RPG::BGS"
    ##
    # bgs_fadeout time_in_seconds
    evcmd :bgs_fadeout,             246, :Integer
    ## TODO
    # me_play me
    evcmd :me_play,                 249, "RPG::ME"
    ## TODO
    # se_play se
    evcmd :se_play,                 250, "RPG::SE"
    ##
    # se_stop
    evcmd :se_stop,                 251

    ##
    # movie_play filename
    evcmd :movie_play,              261, :String

    ##
    # set_map_name_display state
    evcmd :set_map_name_display,    281, :Integer

    ##
    # change_tileset tileset
    evcmd :change_tileset,          282, "RPG::Tileset"

    ## TODO
    # change_battle_background
    evcmd :change_battle_background,283, :String, :String

    ## TODO
    # change_parallax_background
    evcmd :change_parallax_background,284, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown

    ## TODO
    # get_location_info variable_id, info_id, is_direct, x, y
    evcmd :get_location_info,       285, :Integer, :Integer, :Integer, :Integer, :Integer

    ## TODO
    # process_battle
    evcmd :process_battle,          301
    evcmd :battle_if_win,           601
    evcmd :battle_if_escape,        602
    evcmd :battle_if_lose,          603
    ## TODO
    # process_shop
    evcmd :process_shop,            302
    ## TODO
    # process_name_input
    evcmd :process_name_input,      303
    ##
    # process_menu
    evcmd :process_menu,            351
    ##
    # process_save
    evcmd :process_save,            352
    ##
    # process_gameover
    evcmd :process_gameover,        353
    ##
    # process_title
    evcmd :process_title,           354

    ## TODO
    # change_actor_hp
    evcmd :change_actor_hp,         311, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_mp
    evcmd :change_actor_mp,         312, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_state
    evcmd :change_actor_state,      313, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # actor_recover_all
    evcmd :actor_recover_all,       314, :Unknown, :Unknown
    ## TODO
    # change_actor_exp
    evcmd :change_actor_exp,        315, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_level
    evcmd :change_actor_level,      316, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_parameters
    evcmd :change_actor_parameters, 317, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_skill
    evcmd :change_actor_skill,      318, :Unknown, :Unknown, :Unknown, :Unknown
    ## TODO
    # change_actor_equip
    evcmd :change_actor_equip,      319, :Unknown, :Unknown, :Unknown
    ##
    # change_actor_name actor_id, new_name
    evcmd :change_actor_name,       320, :Integer, :String
    ##
    # change_actor_class actor_id, class_id
    evcmd :change_actor_class,      321, :Integer, :Integer
    ##
    # change_actor_graphic actor_id, character_name, character_index, face_name, face_index
    evcmd :change_actor_graphic,    322, :Integer, :String, :Integer, :String, :Integer
    ##
    # change_graphic actor_id, character_name, character_index, face_name, face_index
    evcmd :change_vehicle_graphic,  323, :Integer, :String, :Integer
    ##
    # change_actor_nickname actor_id, new_name
    evcmd :change_actor_nickname,   324, :Integer, :String

    ## TODO
    # change_enemy_hp
    evcmd :change_enemy_hp,         331
    ## TODO
    # change_enemy_mp
    evcmd :change_enemy_mp,         332
    ## TODO
    # change_enemy_state
    evcmd :change_enemy_state,      333
    ##
    # enemy_recover_all
    evcmd :enemy_recover_all,       334
    ##
    # enemy_appear enemy_index
    evcmd :enemy_appear,            335, :Integer
    ##
    # enemy_transform enemy_index, new_enemy_id
    evcmd :enemy_transform,         336, :Integer, :Integer
    ##
    # show_battle_animation enemy_index, animation_id
    evcmd :show_battle_animation,   337, :Integer, :Integer
    ## TODO
    # battle_force_action
    evcmd :battle_force_action,     339, :Unknown, :Unknown, :Unknown, :Unknown
    ##
    # battle_abort
    evcmd :battle_abort,            340

    ##
    # script ruby_string
    evcmd :script,                  355, :String

    ### aliases
    evcmd_alias :if, :conditional_branch
    evcmd_alias :transfer_player, :set_player_position
    evcmd_alias :eval, :script
  end

end

__END__

test_easm = <<-__EOF__
#define CONSTANT "delta"
#define XINST inst2

main:
  inst1
  XINST CONSTANT ; something
  if (x == y)
    inst3
  endif
  ; comment
__EOF__

puts EASM.read_easm_stream(test_easm).map(&:inspect).join("\n")