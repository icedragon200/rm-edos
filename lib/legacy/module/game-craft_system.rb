# // 01/29/2012
# // 01/29/2012
class Game
  module CraftSystem

    @@clean_item_symbol = {
      ex_weapon: :weapon,
      ex_armor: :armor
    }

    def self.craft
    end

    def self.item_symbol(item)
      case item
      when ExDatabase::Weapon then :ex_weapon
      when ExDatabase::Armor  then :ex_armor
      when RPG::Item          then :item
      when RPG::Weapon        then :weapon
      when RPG::Armor         then :armor
      when RPG::Skill         then :skill
      else                         :nil
      end
    end

    def self.clean_item_symbol(sym)
      return @@clean_item_symbol[sym] || sym
    end

    def self.to_material(i)
      Database.item_to_material(i)
    end

    def self.items_to_recipe(a)
      Database.mk_recipe(*(a.map { |i| to_material(i) }))
    end

    def self.recipe_match?(a)
      Database.craft_recipes.has_key?(a)
    end

    def self.get_craft_from(a)
      recipe_match?(a) ? Database.craft_recipes[a] : nil
    end

    # // Scene Craft
    def self.sc_get_craft_from(items)
      get_craft_from(items_to_recipe(items))
    end

  end
end
