# Isometric
#==============================================================================#
# ■ Isometric
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/23/2011
# // • Data Modified : 12/23/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/23/2011 V1.0
#
#==============================================================================#
module Isometric

  #def self.calc_screen_xy(x, y, width, height, mwidth, mheight)
  #  hw, hh = width / 2, height / 2
  #  # // 0, 0 == Top, Right
  #  #xo = mwidth - x * width + y * height
  #  #yo = x * hw + y * hh
  #  # // 0, 0 == Bottom, Left
  #  xo = x * width + y * height
  #  #yo = mheight - x * hw + y * hh
  #  yo = mheight - x * hw + y * hh
  #  xo /= 2
  #  yo /= 2
  #  yo -= height
  #  return xo, yo
  #end

  def self.calc_iso_width(x, dh, width)
    return ((x * (width)) + (dh * (width))) / 2
  end

  def self.calc_iso_height(dw, y, height)
    return ((y * (height / 2)) + (dw * (height / 2))) / 2
  end

  def self.calc_screen_xy(x, y, width, height)
    iso_x = (x * (width / 2)) + (y * (width / 2))
    iso_y = (y * (height / 2)) - (x * (height / 2))# (x * width) + y * height / 2
    return iso_x, iso_y / 2
  end

  def self.run_iso_test()
    tab = Table.new(8, 8, 3)#Table.new(18, 18)

    for x in 0...(tab.xsize)
      for y in 0...(tab.ysize)
        tab[x, y, 0] = 1
      end
    end

    for x in 1...(tab.xsize - 1)
      for y in 1...(tab.ysize - 1)
        tab[x, y, 0] = 2 + rand(2)
      end
    end

    tab[2, 2, 0] = 0

    tab[0, 0, 1] = 4
    tab[1, 0, 2] = 4

    sprite = Sprite::Minimap.new(nil)
    sprite.data = tab

    loop do
      Graphics.update
      Input.update
      sprite.update
    end
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
