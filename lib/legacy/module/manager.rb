#
# src/modules/manager.rb
#
require_relative 'manager/data'
require_relative 'manager/event'
require_relative 'manager/game'
require_relative 'manager/map'
require_relative 'manager/map-map_gen'
require_relative 'manager/rogue'
require_relative 'manager/tile'