#
# src/module/manager/data.rb
# vr 1.00
module DataManager

  extend MACL::Mixin::Log

  BASE_ROOT = File.join(ENV['HOME'], "Turtle Factory")
  SAVE_ROOT = File.join(BASE_ROOT, "EarthenI")
  SAVEFILE_MAX = 8
  EXT_BIN = ".rbd"

  @last_savefile_index = 0

  def self.last_exception
    @last_exception
  end

  def self.init
    @last_savefile_index = 0
    ensure_savepath
    load_database
  end

  def self.ensure_savepath
    unless Dir.exist?(BASE_ROOT)
      Dir.mkdir(BASE_ROOT)
      try_log do |l|
        l.puts("DataManager| Created #{BASE_ROOT} directory")
      end
    end
    unless Dir.exist?(SAVE_ROOT)
      Dir.mkdir(SAVE_ROOT)
      try_log do |l|
        l.puts("DataManager| Created #{SAVE_ROOT} directory")
      end
    end
    return true
  end

  def self.load_database
    load_normal_database
  end

  def self.load_normal_database
    Database.load_and_save
    pre_db_load
    $data_actors        = Game::Data.get_actors
    $data_classes       = Game::Data.get_classes
    $data_skills        = Game::Data.get_skills
    $data_items         = Game::Data.get_items
    $data_weapons       = Game::Data.get_weapons
    $data_armors        = Game::Data.get_armors
    $data_enemies       = Game::Data.get_enemies
    $data_troops        = Game::Data.get_troops
    $data_states        = Game::Data.get_states
    $data_animations    = Game::Data.get_animations
    $data_tilesets      = Game::Data.get_tilesets
    $data_common_events = Game::Data.get_common_events
    $data_system        = Game::Data.get_system
    $data_mapinfos      = Game::Data.get_mapinfos
    # //
    $data_startup       = Game::Data.get_startup
    $data_temp          = Game::Data.get_temp
    $data_traps         = Game::Data.get_traps
    $data_crafts        = Game::Data.get_crafts
    # //
    $data_spirit_classes= Game::Data.get_spirit_classes
    post_db_load
  end

  def self.pre_db_load
  end

  def self.post_db_load
  end

  def self.create_game_objects
    $game = Game.new
    $game.create_objects
  end

  def self.setup_new_game
    create_game_objects
    $game.party.setup_starting_members
    $game.map.setup($data_system.start_map_id)
    #$game.player.moveto($data_system.start_x, $data_system.start_y)
    #$game.player.refresh
    Graphics.frame_count = 0
  end

  def self.setup_battle_test
    $game.party.setup_battle_test
    BattleManager.setup($data_system.test_troop_id)
    BattleManager.play_battle_bgm
  end

  def self.make_path_with_filename(filename)
    File.join(SAVE_ROOT, filename)
  end

  def self.make_filename_abs(sub)
    return "EDOS#{sub}#{EXT_BIN}"
  end

  def self.make_save_filename(index)
    return make_path_with_filename(sprintf(make_filename_abs("%02d"), index + 1))
  end

  def self.save_file_exists?(index)
    return File.exists?(make_save_filename(index))
  end

  def self.any_save_file_exists?
    return !Dir.glob(make_path_with_filename(make_filename_abs("*"))).empty?
  end

  def self.savefile_max
    return SAVEFILE_MAX
  end

  def self.save_game(index)
    begin
      ensure_savepath
      save_game_without_rescue(index)
    rescue => ex
      try_log do |l|
        l.puts("Save Game: FAILED")
        l.puts(ex.inspect)
        l.puts(ex.backtrace.join("\n"))
      end
      @last_exception = ex
      delete_save_file(index) # may be corrupt
      false
    end
  end

  def self.load_game(index)
    begin
      load_game_without_rescue(index)
    rescue Errno::ENOENT
      return nil
    rescue => ex
      try_log do |l|
        l.puts("Load Game: FAILED")
        l.puts(ex.inspect)
        l.puts(ex.backtrace.join("\n"))
      end
      @last_exception = ex
      nil
    end
  end

  def self.load_header(index)
    begin
      load_header_without_rescue(index)
    rescue Errno::ENOENT
      return nil
    rescue => ex
      try_log do |l|
        l.puts("Load Header: FAILED")
        l.puts(ex.inspect)
        l.puts(ex.backtrace.join("\n"))
      end
      @last_exception = ex
      nil
    end
  end

  def self.save_game_without_rescue(index)
    # File.open(filename, "wb") # open file, write in binary
    File.open(make_save_filename(index), "wb") do |file|
      $game.pre_save
      Marshal.dump(make_save_header, file)
      Marshal.dump(make_save_contents, file)
      $game.post_save
      @last_savefile_index = index
    end
    return true
  end

  def self.load_game_without_rescue(index)
    File.open(make_save_filename(index), "rb") do |file|
      Marshal.load(file)
      $game.pre_load
      extract_save_contents(Marshal.load(file))
      $game.post_load
      @last_savefile_index = index
    end
    return true
  end

  def self.load_header_without_rescue(index)
    # File.open(filename, "rb") # open file, read in binary
    File.open(make_save_filename(index), "rb") do |file|
      return Marshal.load(file)
    end
    return nil
  end

  def self.delete_save_file(index)
    filename = make_save_filename(index)
    if File.exists?(filename)
      File.delete(filename)
    end
  end

  def self.make_save_header
    return $game.make_save_header
  end

  def self.make_save_contents
    return $game.make_save_contents
  end

  def self.extract_save_contents(contents)
    $game.extract_save_contents(contents)
  end

  def self.savefile_time_stamp(index)
    File.mtime(make_save_filename(index)) rescue Time.at(0)
  end

  def self.latest_savefile_index
    savefile_max.times.max_by { |i| savefile_time_stamp(i) }
  end

  def self.first_empty_savefile_index
    savefile_max.times.find { |i| !save_file_exists?(i) }
  end

  class << self
    attr_accessor :last_savefile_index
  end

end