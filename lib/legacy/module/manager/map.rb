#
# EDOS/src/module/manager/map.rb
#
module MapManager

  @maps = {}

  def self.load_map(map_id)
    Tmx.load(format("data/map/map%03d.tmx", map_id))
  end

  def self.get_map(map_id)
    return FLAT_MAP() if map_id == -1
    unless @maps[map_id]
      @maps[map_id] = load_map(map_id)
    end
    return @maps[map_id]
  end

  def self.init
    MapGen.init
  end

end