# // 12/25/2011
# // 01/31/2012
module RogueManager

  def self.init
    @handler = {}

    @tips = []
    @tips << "No tip for you >_>"
    @tips << "You can use the mouse to play"
    @tips << "Move the mouse near the edge to scroll"
    @tips << "It takes around 90 turns to complete a dungeon"
    @tips << "Spears are great weapons for distance fights"
    @tips << "OwO"
    @tips << "=w=d"
  end

  def self.random_tip
    @tips.sample
  end

  def self.clear_handler()
    @handler.clear()
  end

  def self.set_handler( symbol, method )
    @handler[symbol] = method
  end

  def self.call_handler( symbol, *args, &block )
    @handler[symbol].call( *args, &block ) if @handler[symbol]
  end

  HOT_KEYS = [:A, :B, :C, :X, :Y, :Z, :LR, :LX, :LY, :LZ, :RX, :RY, :RZ]
  LOCKED_KEYS = [:A, :B, :C, :X, :Y, :Z]

  def self.locked_key?( key )
    return LOCKED_KEYS.include?( key )
  end

  def self._map
    return $game.rogue
  end

  def self.setup_map( map_id )
    _map.setup( map_id )
    return self
  end

  def self.start_dungeon()
    start_rogue()
  end

  def self.start_rogue()
    $game.system.in_rogue = true
    SceneManager.goto( Scene::Rogue )
    return self
  end

  def self.end_rogue(n=Scene::Map) # // . x . Return Scene
    $game.system.in_rogue = false
    SceneManager.goto( n )
  end

end
