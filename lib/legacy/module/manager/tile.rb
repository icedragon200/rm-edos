# TileManager
#==============================================================================#
# ■ TileManager
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/30/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
module TileManager

  @@cache_location = nil

  def self.load_tile_cache()

    unless FileTest.exist?(@@cache_location)
      tile_ids = {}
      # // Tile A1
      0.upto(15) { |i| tile_ids["tilea1_#{i+1}"] = 2048 + (48 * i) }
      # // Tile A2
      offset = (48 * 16)
      0.upto(31) { |i| tile_ids["tilea2_#{i+1}"] = 2048 + offset + (48 * i) }
      # // Tile A3
      offset = (48 * 16) + (48 * 32)
      0.upto(31) { |i| tile_ids["tilea3_#{i+1}"] = 2048 + offset + (48 * i) }
      # // Tile A4
      offset = (48 * 16) + (48 * 32) + (48 * 32)
      0.upto(47) { |i| tile_ids["tilea4_#{i+1}"] = 2048 + offset + (48 * i) }
      # // Tile A5
      0.upto(127){ |i| tile_ids["tilea5_#{i+1}"] = 1536 + i }
      # // Tile A1 (ExMapping)
      0.upto(7) { |i|
        tile_ids["water#{i+1}"] = 2048 + (48 * i*2)
        tile_ids["waterfall#{i+1}"] = 2048 + (48 * (1+(i*2)))
      }
      # // Tile A2 (ExMapping)
      offset = (48 * 16)
      0.upto(31) { |i| tile_ids["floor#{i+1}"] = 2048 + offset + (48 * i) }
      # // Tile A3 (ExMapping)
      offset = (48 * 16) + (48 * 32)
      0.upto(7) { |i|
        tile_ids["roof#{i+1}"] = 2048 + offset + (48 * i)
        tile_ids["roof#{i+9}"] = 2048 + offset + (48 * (16+i))
        tile_ids["outerwall#{i+1}"] = 2048 + offset + (48 * (8+i))
        tile_ids["outerwall#{i+9}"] = 2048 + offset + (48 * (24+i))
      }
      # // Tile A4 (ExMapping)
      offset = (48 * 16) + (48 * 32) + (48 * 32)
      0.upto(23) { |i|
        tile_ids["ceil#{i+1}"]  = 2048 + offset + (48 * i)
        tile_ids["ceil#{i+9}"]  = 2048 + offset + (48 * (16+i))
        tile_ids["ceil#{i+17}"] = 2048 + offset + (48 * (32+i))
        tile_ids["wall#{i+1}"]  = 2048 + offset + (48 * (8+i))
        tile_ids["wall#{i+9}"]  = 2048 + offset + (48 * (24+i))
        tile_ids["wall#{i+17}"] = 2048 + offset + (48 * (40+i))
      }
      # // Tile A5 (ExMapping)
      tile_ids["black"]       = 1536
      tile_ids["transparent"] = 1536 + 8
      save_data(tile_ids, @@cache_location)
    end

    @tile_ids = load_data(@@cache_location)

  end

  def self.sym_tile( sym )
    return @tile_ids[sym]
  end

  def self.sym_autotile( sym )
    tid = @tile_ids[sym]
    return tid...(tid+48)
  end

  def self.init
    @@cache_location = "data/tile_cache.rvdata2"
    load_tile_cache
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
