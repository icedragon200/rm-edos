module Markie

  TextStruct = Struct.new(:rect, :string, :align)

module_function

private

  def sandbox!(bitmap)
    old_font = Mixin::FontStyle.copy_font_to(bitmap.font, Font.new)
    yield bitmap
    Mixin::FontStyle.copy_font_to(old_font, bitmap.font)
    return self
  end

  #\\       Escape \
  #\n       New Line
  #\[       Increase Font Size by 10%
  #\]       Decrease Font Size by 10%
  #\!       Sandbox
  #\.
  #\c[n]    Color n
  #\E[eval] Evaluate contents as ruby code, replacing with the result
  #\i       Italic
  #\B       Bold

  def process_characters(bitmap, chars)
    cstr = str.dup

    while(c = chars.shift)
      esc = false
      if c == "/"
        c = chars.shift
        esc = true
      end

      if escape
        case c
        when "["
          bitmap.font.size *= 1.1
        when "]"
          bitmap.font.size *= 0.9
        when "c" # Color :O
          res = ""
          until(c == "]" or c == nil)
            res << (c = chars.shift)
          end

        #when
        #when
        end
      else
        yield c
      end
    end
  end

public

  def prep_text(rect, string, align)
    return TextStruct.new(rect, string, align)
  end

  def markie!(bitmap, txstruct)
    str = txstruct.string.dup

    esced_str = process_characters(bitmap, str, &method())


    #sandbox!(bitmap) do |bmp|
      ntxstruct = txstruct.dup
      process_characters(bitmap, str.split("")) do |c|
        draw_text(ntxstruct.rect, ntxstruct.string, ntxstruct.align)
      end

    #end

    return self
  end

end
