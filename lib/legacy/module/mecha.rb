#
# EDOS/src/module/Mecha.rb
#   by IceDragon
#   dc 06/04/2013
#   dm 06/04/2013
module Mecha

  VERSION = "0.0.1".freeze

end

dir = File.dirname(__FILE__)
%w(
  state_control
  turn_control
  field
  element
  weapon
  entity
  character
  unit).each do |fn|
  require File.join(dir, 'mecha', fn)
end
