#
# EDOS/src/module/mecha/character.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha
  class Character

    attr_accessor :pos
    attr_accessor :unit

    def initialize
      @pos = StarRuby::Vector2I.new(0, 0)
    end

    def entity
      @unit.entity
    end

    def x
      @pos.x
    end

    def y
      @pos.y
    end

    def pos?(vec)
      @pos == vec
    end

    def screen_x(tilesize)
      @pos.x * tilesize
    end

    def screen_y(tilesize)
      @pos.y * tilesize
    end

    def screen_z(tilesize)
      1
    end

    def move_straight(numpad_dir)
      case numpad_dir
      when 2
        @pos.y += 1
      when 4
        @pos.x -= 1
      when 6
        @pos.x += 1
      when 8
        @pos.y -= 1
      end
    end

    def moveto(x, y)
      @pos.x = x
      @pos.y = y
    end

  end
end