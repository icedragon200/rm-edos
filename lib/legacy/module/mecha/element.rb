#
# EDOS/src/module/Mecha/Element.rb
#   by IceDragon
#   dc 07/04/2013
#   dm 07/04/2013
# vr 1.0.0
module Mecha
  class FloatTable

    attr_reader :xsize, :ysize

    def initialize(xsize, ysize)
      @xsize, @ysize = xsize, ysize
      @data = Array.new(@ysize) { Array.new(@xsize, 0.0) }
    end

    def [](x, y)
      @data[y][x]
    end

    def []=(x, y, n)
      @data[y][x] = n.to_f
    end

    alias :get :[]
    alias :set :[]=

  end

  class ElementTable < FloatTable

    def initialize(element_count)
      super(element_count, element_count)
    end

    def get_rate(e1, e2)
      get(e1.id, e2.id)
    end

    def set_rate(e1, e2, r)
      set(e1.id, e2.id, r)
    end

    def calc_rate(src_element, *molecules)
      molecules.inject(1.0) do |r, mol|
        r * (get_rate(src_element, mol.element) ** mol.count)
      end
    end

  end

  class Element

    attr_accessor :id   # Integer
    attr_accessor :name # String

    def initialize(id, name)
      @id   = id
      @name = name
    end

  end

  class Molecule

    attr_accessor :element # Element
    attr_accessor :count   # Integer

  end

  class Compound

    attr_accessor :molecules

  end
end

#Element.new(0, "Carbon") # (Earth) Element of Structure
#Element.new(1, "") # (Fire)  Element of Force
#Element.new(2, "Carbohydrate") # (Water) Element of Energy
#Element.new(3, "Oxygen") # (Air)   Element of Flow/Life