#
# EDOS/src/module/Mecha/Entity.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha

class Entity

  attr_reader :hp,
              :ap,
              :wt,
              :name

  attr_accessor :unit

  def initialize
    init
    post_init
  end

  def init
    init_members
  end

  def init_members
    # initialize instance variables here
    @hp = 0
    @ap = 0
  end

  def post_init
    # do post actions such as recovery or stuff like that
    recover_all
    reset_wt
  end

  def character
    @unit.character
  end

  # MaxHP
  def mhp
    100 # TEMP
  end

  # Not literal Map, MaxAP
  def map
    1
  end

  def mwt
    500
  end

  def hp=(new_hp)
    @hp = [[new_hp, 0].max, mhp].min
  end

  def ap=(new_ap)
    @ap = [[new_ap, 0].max, map].min
  end

  def wt=(new_wt)
    @wt = [new_wt, 0].max
  end

  def change_name(new_name)
    @name = new_name.to_s
  end

  def recover_all_hp
    @hp = mhp
  end

  def recover_all_ap
    @ap = map
  end

  def recover_all
    recover_all_hp
    recover_all_ap
  end

  def reset_wt
    @wt = mwt
  end

  def dec_wt(wt)
    self.wt -= wt
  end

  def hp_rate
    hp.to_f / mhp
  end

  def ap_rate
    ap.to_f / map
  end

  def wt_rate
    wt.to_f / mwt
  end

end

end
