#
# EDOS/src/module/Mecha/Field.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha

class Field

  attr_reader :data
  attr_reader :shadow_data
  attr_reader :flash_data

  attr_accessor :data_changed

  def initialize(w, h)
    create_data(w, h)
    @data_changed = false
  end

  def create_data(w, h)
    @data        = Table.new(w, h)
    @shadow_data = Table.new(w, h)
    @flash_data  = Table.new(w, h)
  end

  def width
    @data.xsize
  end

  def height
    @data.ysize
  end

  def valid_pos?(x, y)
    return false if x < 0 || x >= width
    return false if y < 0 || y >= height
    return true
  end

  def modify_flash_data(x=nil, y=nil)
    result = yield @flash_data, x, y
    @flash_data[x, y] = result if x || y
    @data_changed = true
    return nil
  end

  def add_flash(x, y, color)
    modify_flash_data(x, y) do |_, _, _|
      color.is_a?(Fixnum) ? color : color.to_rgb12
    end
  end

end

end
