#
# EDOS/src/module/Mecha/StateControl.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha

module StateControl

  attr_reader :state_order

  def before_state_set(&func)
    @before_state = func
    if @before_state
      define_method(:before_state, &@before_state)
    else
      undef_method(:before_state) if method_defined?(:before_state)
    end
  end

  def after_state_set(&func)
    @after_state = func
    if @after_state
      define_method(:after_state, &@after_state)
    else
      undef_method(:after_state) if method_defined?(:after_state)
    end
  end

  def state(sym, &func)
    before_state = @before_state
    after_state = @after_state
    define_method(sym, &func)
    old_meth = instance_method(sym)
    alias_method("org_#{sym}", sym)
    define_method(sym) do |*args, &block|
      old_state = @state
      change_state(sym)
      before_state(old_state) if before_state
      old_meth.bind(self).call(*args, &block)
      after_state(sym) if after_state
    end
  end

  def state_flow_to(from, to)
    (@state_order ||= {})[from] = to
  end

end

end
