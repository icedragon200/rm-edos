#
# EDOS/src/module/Mecha/TurnControl.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha

  class TurnControl

    extend StateControl
    include MACL::Mixin::Callback

    attr_reader :state, :server, :result

    def initialize(server)
      @server = server
      # internal
      @state  = nil # state variable
      @result = nil # state result after yielding control back to the caller

      # external
      @turn = 0

      init_callbacks
    end

    def change_state(new_state)
      old_state = @state
      @state = new_state
      try_callback(:on_change_state, @state, old_state)
    end

    def change_turn(new_turn)
      old_turn = @turn
      @turn = new_turn
      try_callback(:on_change_turn, @turn, old_turn)
    end

    # only call this function within a Fiber-ized block
    def main
      battle_start
      loop do
        turn_start
        turn_loop
        turn_end
        next_turn
        break unless @result
      end
      battle_end
      @state = :dead
    end

    def turn_loop
      loop do
        unit_start
        unit_action_loop
        unit_end
        next_unit
        break unless @result
      end
    end

    def unit_action_loop
      loop do
        unit_make_action
        unit_action_start
        unit_action
        unit_action_end
        unit_next_action
        break unless @result
      end
    end

    before_state_set do |old_state|
      @result = Fiber.yield(@state)
    end

    after_state_set do |state|
      try_callback(state)
    end

    state :battle_start do
      if agent = @server.request('turn_control')
        agent.set_data('Battle Started').send_to('log').dispatch
      end
    end

    state :turn_start do
      change_turn(@turn + 1)
    end

    state :unit_start do

    end

    state :unit_make_action do
    end

    state :unit_action_start do

    end

    state :unit_action do
    end

    state :unit_action_end do
    end

    state :unit_next_action do
    end

    state :unit_end do
    end

    state :next_unit do
    end

    state :turn_end do
    end

    state :next_turn do
    end

    state :battle_end do
    end

    #state_flow_to(:battle_start, :turn_start)
    #state_flow_to(:turn_start, :unit_start)
    #state_flow_to(:unit_start, :unit_action_start)
    #state_flow_to(:unit_action_start, :unit_action)
    #state_flow_to(:unit_action, :unit_action_end)
    #state_flow_to(:unit_action_end, :unit_end)
    #state_flow_to(:unit_end, :next_unit)
    #state_flow_to(:next_unit, :turn_end)
    #state_flow_to(:turn_end, :next_turn)
    #state_flow_to(:next_turn, :battle_end)

  end

end
