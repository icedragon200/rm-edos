#
# EDOS/src/module/Mecha/Unit.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 06/04/2013
# vr 1.0.0
module Mecha

class Unit

  attr_accessor :entity
  attr_accessor :character

  def initialize(entity, character)
    # circular references
    @entity    = entity
    @character = character
    @entity.unit    = self
    @character.unit = self
  end

  ## wrapper
  # name
  def name
    @entity.name
  end

  ## callback
  def on_battle_start

  end

  def on_turn_start

  end

  def on_unit_start

  end

  def on_unit_make_action

  end

  def on_unit_action_start

  end

  def on_unit_action

  end

  def on_unit_action_end

  end

  def on_unit_next_action

  end

  def on_unit_end

  end

  def on_next_unit

  end

  def on_turn_end

  end

  def on_next_turn

  end

  def on_battle_end

  end

end

end
