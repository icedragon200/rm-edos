#
# EDOS/src/module/Mecha/Weapon.rb
#   by IceDragon
#   dc 06/04/2013
#   dm 07/04/2013
# vr 1.0.0
module Mecha

  class Composite

    attr_accessor :compound

  end

  class ArmorComposite < Composite

    attr_accessor :defense_rate

  end

  class Warhead < Composite

    attr_accessor :damage_rate

  end

  class Armor

    attr_accessor :composite
    attr_accessor :defense_rate
    attr_accessor :element_table

  end

  class Weapon

    attr_accessor :composite
    attr_accessor :damage_rate
    attr_accessor :element_table

    attr_accessor :range

  end

end
