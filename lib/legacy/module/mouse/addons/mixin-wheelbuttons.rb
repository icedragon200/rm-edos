module Mixin::WheelButtons

  def ex_init
    super()
    @buttons = []
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 13 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 14 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 15 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 12 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 4 )
    @buttons << GUIExt::Button_Base.new( nil, 0, 0, 5 )
    @buttons[0].set_handler(:click, method(:wheel_left))
    @buttons[1].set_handler(:click, method(:wheel_right))
    @buttons[2].set_handler(:click, method(:wheel_up))
    @buttons[3].set_handler(:click, method(:wheel_down))
    @buttons[4].set_handler(:click, method(:wheel_cancel))
    @buttons[5].set_handler(:click, method(:wheel_ok))
    @buttons[0].help_text = "Previous"
    @buttons[1].help_text = "Next"
    @buttons[2].help_text = "Previous Page"
    @buttons[3].help_text = "Next Page"
    @buttons[4].help_text = "Cancel"
    @buttons[5].help_text = "Ok"
    @buttons.each{|b|b.help_window = MouseCore.tooltip_sprite}
  end

  def update()
    super()
  end

  def refresh_x()
    super()
    @buttons[0].x = self.x - 32
    @buttons[1].x = self.x2 + 32
    @buttons[2].x = self.x #- 12
    @buttons[3].x = self.x #- 12
    @buttons[4].x = self.x - 24
    @buttons[5].x = self.x2 + 24
  end

  def refresh_y()
    super()
    @buttons[0].y = self.y #- 12
    @buttons[1].y = self.y #- 12
    @buttons[2].y = self.y - 52
    @buttons[3].y = self.y2 + 32
    @buttons[4].y = self.y2 + 40
    @buttons[5].y = self.y2 + 40
  end

  def refresh_z()
    super()
    @buttons.each { |but| but.z = self.z + 4 }
  end

  def refresh_visible()
    super()
    @buttons.each { |but| but.visible = self.visible }
  end

  def refresh_active()
    super()
    @buttons.each { |but| but.active = self.active }
  end

  def refresh_viewport()
    super()
    @buttons.each { |but| but.viewport = self.viewport }
  end

end
