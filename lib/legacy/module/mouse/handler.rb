#
# EDOS/src/module/mouse/handler.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 19/06/2013
#   dm 19/06/2013
# vr 2.0.0
#   MouseCore Handle V2
#     Optimized and compressed, removed old bindings and functions that
#     where used, implementing a new model
class MouseCore::Handler

  ### mixins
  include MouseCore::Constants

  ### attributes
  attr_accessor :ticks
  attr_accessor :pos

  ##
  # initialize
  def initialize
    MouseCore.init
    @pos   = [0, 0]
    @last_pos = [-1, -1]
    @z = 0xFFFF
    @click  = Hash[BUTTONS.zip([false] * BUTTONS.size)]
    @press  = Hash[BUTTONS.zip([false] * BUTTONS.size)]
    @repeat = Hash[BUTTONS.zip([false] * BUTTONS.size)]
    # keeps track of how long the button has been pressed
    @counter = Hash[BUTTONS.zip([0] * BUTTONS.size)]
    @ticks = 0 # keeps track of how many times the module has been updated
  end

  ##
  # update
  #   this function is called by Main::update
  def update
    @pos = MouseCore.pos
    @moved = @pos != @last_pos
    @last_pos = @pos
    BUTTONS.each do |button|
      @click[button] = MouseCore.click?(button)
      @repeat[button] = MouseCore.repeat?(button)
      # kill 2 birds with 1 stone :)
      if @press[button] = MouseCore.press?(button)
        @counter[button] += 1
      else
        @counter[button] = 0 # button should be reset since it wasn't pressed
      end
    end
    @ticks += 1
  end

  ##
  # ::post_update
  #   this function is called by Main::post_update
  def post_update
    MouseCore.update_cursor_sprite
    MouseCore.update_tooltip_sprite
  end

  ##
  # count(BUTTON button)
  def count(button)
    @counter[button]
  end

  ##
  # x -> int
  def x
    @pos[0]
  end

  ##
  # y -> int
  def y
    @pos[1]
  end

  ##
  # z -> int
  def z
    @z
  end

  ##
  # calc_finetune_pos(int fine) -> Array<int>[x, y]
  def calc_finetune_pos(fine)
    if fine.is_a?(Numeric)
      [(x / fine.to_i) * fine, (y / fine.to_i) * fine]
    else
      [(x / fine.width.to_i) * fine.width, (y / fine.height.to_i) * fine.height]
    end
  end

  ##
  # click?(BUTTON button) -> Boolean
  def click?(button)
    @click[button]
  end

  ##
  # repeat?(BUTTON button) -> Boolean
  def repeat?(button)
    @repeat[button]
  end

  ##
  # press?(BUTTON button) -> Boolean
  def press?(button)
    @press[button]
  end

  ### fine_clicks
  ##
  # left_click? -> Boolean
  def left_click?
    click?(BUTTON_LEFT)
  end

  ##
  # right_click? -> Boolean
  def right_click?
    click?(BUTTON_RIGHT)
  end

  ##
  # middle_click? -> Boolean
  def middle_click?
    click?(BUTTON_MIDDLE)
  end

  ##
  # left_repeat? -> Boolean
  def left_repeat?
    repeat?(BUTTON_LEFT)
  end

  ##
  # right_repeat? -> Boolean
  def right_repeat?
    repeat?(BUTTON_RIGHT)
  end

  ##
  # middle_repeat? -> Boolean
  def middle_repeat?
    repeat?(BUTTON_MIDDLE)
  end

  ##
  # left_press? -> Boolean
  def left_press?
    press?(BUTTON_LEFT)
  end

  ##
  # right_press? -> Boolean
  def right_press?
    press?(BUTTON_RIGHT)
  end

  ##
  # middle_press? -> Boolean
  def middle_press?
    press?(BUTTON_MIDDLE)
  end

  ##
  # moved?
  def moved?
    @moved
  end

  ##
  # at_pos?(int x, int y)
  def at_pos?(x, y)
    (@pos[0] == x && @pos[1] == y)
  end

  ##
  # in_area?(Object* obj)
  def in_area?(obj)
    return x.between?(obj.x, obj.x2) && y.between?(obj.y, obj.y2)
  end

end
