# Input (MouseCore Addon)
# // 01/24/2012
# // 01/25/2012
module Input
class << self

  #alias :pre_mouse_update :update
  #alias :pre_mouse_trigger? :trigger?
  #alias :pre_mouse_press? :press?
  #alias :pre_mouse_repeat? :repeat?
  #def update(*args,&block)
  #  pre_mouse_update(*args,&block)
  #  Mouse.update()
  #end

  def mtrigger?(*n,&b)
    return true if n[0] == :C && Mouse.left_click?
    return true if n[0] == :B && Mouse.right_click?
    #pre_mouse_trigger?(*n)
    trigger?(*n, &b)
  end

  def mpress?(*n, &b)
    return true if n[0] == :C && Mouse.left_press?
    return true if n[0] == :B && Mouse.right_press?
    #pre_mouse_press?(*n)
    press?(*n, &b)
  end

  def mrepeat?(*n,&b)
    return true if n[0] == :C && Mouse.left_press?
    return true if n[0] == :B && Mouse.right_press?
    #pre_mouse_repeat?(*n)
    repeat?(*n, &b)
  end

end
end # // Input
