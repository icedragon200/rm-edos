module MouseCore

  class MouseError < Exception
  end

  module Constants
    BUTTON_LEFT   = :left
    BUTTON_RIGHT  = :right
    BUTTON_MIDDLE = :middle
    #WHEEL_UP      = nil # // Unused
    #WHEEL_DOWN    = nil # // Unused
    BUTTONS = [BUTTON_LEFT, BUTTON_RIGHT, BUTTON_MIDDLE]
  end

  include Constants

  def self.init
    #raise(MouseError, "#{self} was already initialized") if @initialized
    @initialized = true
  end

  def self.pos
    return StarRuby::Input.mouse_location
  end

  def self.rpos # // Real Client Position
    return self.pos
  end

  def self.in_client?
    return true
  end

  def self.x_in_client?
    return true
  end

  def self.y_in_client?
    return true
  end

  def self.click?(n)
    Input.sr_trigger?(:mouse, n)
  end

  def self.repeat?(n)
    Input.sr_repeat?(:mouse, n)
  end

  def self.press?(n)
    Input.sr_press?(:mouse, n)
  end

end