# // 02/09/2012
# // 02/09/2012
module Music

  @@bgms = {}

  def self.init
    @@bgms['tutorial'] = RPG::BGM.new("Kii(TutorialVer)")
  end
  
  def self.play_tutorial()
    @@bgms['tutorial'].play()
  end  

end  
