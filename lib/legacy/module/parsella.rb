#
# EDOS/src/module/Parsella.rb
#
# vr 1.0.0
module Parsella

  def self.gpl_to_palette_s(str, need_name=true)
    result = []
    str.scan(/\s*(\d+)\s*(\d+)\s*(\d+)\s*(.*)/) do |(r, g, b, nm)|
      if need_name && (!nm || nm.empty?)
        raise(Exception, "Color needs a name #{[r, g, b]}")
      end
      new_name = block_given? ? yield(:name, nm) : nm
      h = { name: new_name, red: r, green: g, blue: b }
      result.push('set_color("%<name>s", %<red>d, %<green>d, %<blue>d)' % h)
    end
    result
  end

  def self.bitmap_to_gpl(bmp)
    cols = bmp.width / 12
    rows = bmp.height / 12
    result = []
    for y in 0...rows
      for x in 0...cols
        result << bmp.get_pixel(x * 12, y * 12)
      end
    end

    File.open("ps.gpl", "w+") do |f|
      f.puts("GIMP Palette")
      f.puts("Name: PS")
      f.puts("Columns: 16")
      result.each do |c|
        f.puts("%-4s %-4s %-4s" % c.to_a_na.map(&:to_i))
      end
    end
  end

end
