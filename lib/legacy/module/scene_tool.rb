module SceneTool

  def self.fadeout_all(time = 1000)
    RPG::BGM.fade(time)
    RPG::BGS.fade(time)
    RPG::ME.fade(time)
    Graphics.fadeout(time * Graphics.frame_rate / 1000)
    RPG::BGM.stop
    RPG::BGS.stop
    RPG::ME.stop
  end

  def self.check_gameover
    SceneManager.goto(Scene::Gameover) if $game.party.all_dead?
  end

end