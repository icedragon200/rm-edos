#==============================================================================#
# ■ Sound
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/07/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0
#==============================================================================#
module Sound

  @@sounds = {}
  @@ex_sounds = {}

  def self.init
    @@sounds[:cursor]    = RPG::SE.new("melodic2_click", 100, 100 )
    @@sounds[:ok]        = RPG::SE.new("melodic3_affirm", 100, 100 )
    @@sounds[:cancel]    = RPG::SE.new("melodic4_affirm", 100, 100 )
    @@sounds[:buzzer]    = RPG::SE.new("echo_deny", 100, 100 )
    @@sounds[:bdmg]      = RPG::SE.new("Hit_Hurt9", 80, 100 )
    @@sounds[:bcol]      = RPG::SE.new("Explosion9", 70, 100 )
    @@sounds[:evasion]   = RPG::SE.new("Jump11", 80, 100 )
    @@sounds[:mgevasion] = RPG::SE.new("Jump17", 80, 100 )
    @@sounds[:miss]      = RPG::SE.new("Jump18", 80, 100 )
    @@sounds[:recovery]  = RPG::SE.new("Recovery", 80, 100 )
    @@sounds[:equip]     = RPG::SE.new("ISTS-Equip", 100, 100 )
    @@sounds[:win_open]  = RPG::SE.new("ISTS-OpenWindow", 100, 100 )
    @@sounds[:win_close] = RPG::SE.new("ISTS-CloseWindow", 100, 100 )
    @@sounds[:op_change] = RPG::SE.new("ISTS-OptionChange", 100, 100 )
    # // 02/29/2012
    @@sounds[:use_skill] = RPG::SE.new("", 100, 100 )

    @@sounds[:save] = RPG::SE.new("sfx/save", 100, 100)

    @@ex_sounds['typing'] = RPG::SE.new("Typing-Key1", 80, 100)
    @@ex_sounds['button'] = RPG::SE.new('button', 80, 100)

    @@ex_sounds['cursor2'] = RPG::SE.new("mouse_over2", 100, 100)
  end

  def self.play(name)
    @@sounds[name].play
  end

  def self.play_ex(name)
    @@ex_sounds[name].play
  end

  def self.play_cursor()
    @@sounds[:cursor].play()
  end

  def self.play_ok()
    @@sounds[:ok].play()
  end

  def self.play_cancel()
    @@sounds[:cancel].play()
  end

  def self.play_buzzer()
    @@sounds[:buzzer].play()
  end

  def self.play_battler_damage()
    @@sounds[:bdmg].play()
  end

  def self.play_battler_collapse()
    @@sounds[:bcol].play()
  end

  def self.play_evasion()
    @@sounds[:evasion].play()
  end

  def self.play_magic_evasion()
    @@sounds[:mgevasion].play()
  end

  def self.play_miss()
    @@sounds[:miss].play()
  end

  def self.play_recovery()
    @@sounds[:recovery].play()
  end

  def self.play_equip()
    @@sounds[:equip].play()
  end

  def self.play_window_open()
    @@sounds[:win_open].play()
  end

  def self.play_window_close()
    @@sounds[:win_close].play()
  end

  def self.play_option_change()
    @@sounds[:op_change].play()
  end

  # // 02/29/2012
  def self.play_use_skill()
    @@sounds[:use_skill].play()
  end

  def self.play_save
    play(:save)
  end

end