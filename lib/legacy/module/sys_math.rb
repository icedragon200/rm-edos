module SysMath

  CAP = 0x200
  CAP_F = CAP.to_f
  SINE_TABLE = Table.new(CAP)
  COSINE_TABLE = Table.new(CAP)

  for x in 0...CAP
    SINE_TABLE[x] = (Math.sin(Math::PI * (x / CAP_F)) * 0xFFFF).to_i
    COSINE_TABLE[x] = (Math.cos(Math::PI * (x / CAP_F)) * 0xFFFF).to_i
  end

  def self.sin(index)
    return SINE_TABLE[index]
  end

  def self.cos(index)
    return COSINE_TABLE[index]
  end

  def self.sin_f(index)
    return SINE_TABLE[index] / 0xFFFF.to_f
  end

  def self.cos_f(index)
    return COSINE_TABLE[index] / 0xFFFF.to_f
  end

end
