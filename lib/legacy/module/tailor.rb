#
# EDOS/src/module/tailor.rb
# vr 1.0.0
#   Bitmap stitching module, used for creating spritesheets from seperate
#   bitmaps, or for unstitching a spritesheet into seperate bitmaps
module Tailor

  ##
  # ::stitch(Array<Bitmap> bitmaps, int cell_w, int cell_h,
  #          int cols, int rows, bool stretch, ANCHOR anchor)
  def self.stitch(bitmaps, cell_w, cell_h, cols=nil, rows=nil,
                  stretch=false, anchor=0)
    raise(ArgumentError, "No bitmaps where given!") if bitmaps.empty?
    if !anchor.is_a?(Integer)
      puts "Use of Boolean for anchor is depreceated"
      anchor = anchor ? 5 : 0 # anchor was originally center
    end
    cell_rect = Rect.new(0, 0, cell_w, cell_h)
    stitch_by_col = !!cols

    if stitch_by_col
      w = cell_w * cols
      h = cell_h * (bitmaps.size / cols + (bitmaps.size % cols > 0 ? 1 : 0))
      rows = h / cell_h
    else
      w = cell_w * (bitmaps.size / rows + (bitmaps.size % rows > 0 ? 1 : 0))
      h = cell_h * rows
      cols = w / cell_w
    end

    result_bmp = Bitmap.new(w, h)

    calc_xy = ->(index) do
      stitch_by_col ? [(index % cols), (index / cols)] :
                      [(index / rows), (index % rows)]
    end

    if stretch
      bitmaps.each_with_index do |bmp, i|
        x, y = calc_xy.(i)
        r = Rect.new(x * cell_w, y * cell_h, cell_w, cell_h)
        result_bmp.stretch_blt(r, bmp, bmp.rect)
      end
    else
      r = Rect.new(0, 0, cell_w, cell_h)

      bitmaps.each_with_index do |bmp, i|
        x, y = calc_xy.(i)
        cr = bmp.rect.align_to(anchor: anchor, surface: cell_rect)
        cx, cy = cr.x, cr.y
        #cell_rect!center ? [0, 0] : [(cell_w - bmp.rect.width) / 2,
        #                   (cell_h - bmp.rect.height) / 2]
        result_bmp.blt(x * cell_w + cx, y * cell_h + cy, bmp, bmp.rect)
      end
    end

    return result_bmp
  end

  def self.stitch_folder(source, *args)
    files = Dir.glob(source).sort
    raise(ArgumentError, "No source files where found") if files.empty?
    bitmaps = files.map { |fn| Bitmap.new(fn) }
    result = stitch(bitmaps, *args)
    bitmaps.each(&:dispose)
    return result
  end

end
