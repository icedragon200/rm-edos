#==============================================================================#
# ■ WindowAddons
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/15/2011
# // • Data Modified : 02/19/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/15/2011 V1.0
#         Added
#           Header
#           ScrollBar
#
#     ♣ 12/15/2011 V1.0
#         Updated
#           Header
#
#     ♣ 12/15/2011 V1.0
#         Added
#           Devi::Header
#==============================================================================#
module WindowAddons ; end
require_relative 'window_addons/addonbin.rb'
require_relative 'window_addons/base.rb'
require_relative 'window_addons/header.rb'
require_relative 'window_addons/scrollbar.rb'
require_relative 'window_addons/winbuttons.rb'
require_relative 'window_addons/mouse-movewindow.rb'
require_relative 'window_addons/tabs.rb'
require_relative 'window_addons/option_cursor.rb'
require_relative 'window_addons/footer.rb'
require_relative 'window_addons/collections'

# // Small Antilag :D
module WindowAddons::Base_Tail

  def active_addons
    super + [:base_tail]
  end

  alias :all_active_addons :active_addons
  def active_addons
    @active_addons ||= all_active_addons
    @active_addons
  end

end

# // << >>
class Window::Base < Window

  include WindowAddons::Base

end
