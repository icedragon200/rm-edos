class WindowAddons::AddonBin

  def self.[](*addons)
    ::Class.new(self) { addons.each { |a| include a } }
  end

  attr_reader :parent

  def initialize(parent)
    @parent = parent
    init_addons
  end

  def update
    update_addons
  end

  def dispose
    dispose_addons
  end

  def method_missing(sym,*args)
    @parent.send(sym, *args)
  end

  def active_addons
    []
  end

  def init_addons
  end

  def update_addons
  end

  def dispose_addons
  end

  def addons_refresh_x
  end

  def addons_refresh_y
  end

  def addons_refresh_z
  end

  def addons_refresh_opacity
  end

  def addons_refresh_visible
  end

  def addons_refresh_viewport
  end

  def addons_refresh_openness
  end

  def addons_refresh_active
  end

  def header_space
    14
  end

  def tail_space
    14
  end

  HEADER_TAGS = [:header,:winbuttons,:tabs]
  TAIL_TAGS   = [:tail]

  # // Is there any form of header type addon installed? O_O;
  def header_addon?
    (active_addons&HEADER_TAGS).empty?
  end

  # // Is there any form of tailing type addon installed? @_@;
  def tail_addon?
    (active_addons&TAIL_TAGS).empty?
  end

  attr_writer :can_move_window

  def can_move_window
    @can_move_window = false if @can_move_window.nil?
    @can_move_window
  end

  def mouse_can_move_window?
    @can_move_window
  end

  def win_busy?
    false
  end

end
