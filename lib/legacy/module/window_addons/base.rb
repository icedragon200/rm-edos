module WindowAddons::Base 

  def addon_bin
    WindowAddons::AddonBin
  end  

  def init_addons
    @addon_bin = addon_bin.new(self)
  end  

  def update_addons
    @addon_bin.update
  end  

  def dispose_addons
    @addon_bin.dispose
  end

  def x=(x)
    super(x)
    @addon_bin.addons_refresh_x
  end  

  def y=(y)
    super(y)
    @addon_bin.addons_refresh_y
  end  

  def z=(z)
    super(z)
    @addon_bin.addons_refresh_z
  end

  def opacity=(opacity)
    super(opacity)
    @addon_bin.addons_refresh_opacity
  end  

  def visible=(visible)
    super(visible)
    @addon_bin.addons_refresh_visible
  end  

  def openness=(openness)
    super(openness)
    @addon_bin.addons_refresh_openness
  end  

  def viewport=(viewport)
    super(viewport)
    @addon_bin.addons_refresh_viewport
  end

  def win_busy?
    @addon_bin.win_busy?
  end  

  def can_move_window=(n)
    @addon_bin.can_move_window = n 
  end  

  def can_move_window
    @addon_bin.can_move_window
  end  

  def mouse_can_move_window?
    @addon_bin.mouse_can_move_window? 
  end  
  
  def space_rect
    r = super.dup 
    r.y += (@addon_bin.header_addon? ? 0 : @addon_bin.header_space)
    r.height += (@addon_bin.tail_addon? ? 0 : @addon_bin.tail_space)
    r
  end  

end
