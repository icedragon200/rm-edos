class Collection_Base

  attr_reader :parent, :objects, :index

  def initialize(parent)
    @parent  = parent
    @objects = []
    @index   = 0
  end

  def index=(n)
    if @index != n
      @index = n
      update_index
    end
  end

  def update_index()
  end

  def each_object()
    @objects.each { |o| yield o }
  end

  def each_object_with_index()
    @objects.each_with_index { |o,i| yield o,i }
  end

  [:x,:y,:z,:visible,:opacity,:openness,:viewport,:active].each do |sym|
    module_eval("def #{sym};@parent.#{sym};end") # // Wrap these
  end

  def refresh()
    update_openness()
    update_visible()
    update_opacity()
    update_active()
    update_viewport()
    update_position()
    update_index()
  end

  def update_visible()
    each_object { |o| o.visible = self.visible }
  end

  def update_opacity()
    each_object { |o| o.opacity = self.opacity }
  end

  def update_active()
    each_object { |o| o.active = self.active }
  end

  def update_viewport()
    each_object { |o| o.viewport = self.viewport }
  end

  def update_position()
    each_object { |o|
      o.x = self.x + o.ax
      o.y = self.y + o.ay
      o.z = self.z + o.az
    }
  end

  def update_openness()
  end

  def disposed?()
    @disposed == true
  end

  def dispose()
    each_object { |o| o.dispose }
    @disposed = true
  end

  def update()
    each_object { |o| o.update }
  end

end

class Collection_WinButtons < Collection_Base

  def initialize(parent)
    super( parent )
    @objects = Array.new(3) { WinButton.new(self.viewport) }
    @objects.each_with_index { |b,i|
      b.aset((CacheExt.win_button_rect.width+1)*i, 0, 0).org_button_index = 6 + i * 4
    }
    @objects[0].help_text = "Shrink"
    @objects[1].help_text = "Restore"
    @objects[2].help_text = "Close"
    @objects.each{|b|b.help_window = MouseCore.tooltip_sprite}
    refresh()
  end

  def set_handler(button_sym, method)
    case button_sym
    when :minimize
      @objects[0].set_handler(:click, method)
    when :maximize
      @objects[1].set_handler(:click, method)
    when :close
      @objects[2].set_handler(:click, method)
    end
  end

  def x
    @parent.winbuttons_x
  end

  def y
    @parent.winbuttons_y
  end

  def z
    @parent.winbuttons_z
  end

  def visible
    super && !@parent.close?
  end

  def update_active()
    self.parent.winbuttons_actives.each_with_index { |b,i|@objects[i].active=b }
  end

  def update_openness()
    super()
    update_position()
    update_visible()
  end

end

class Collection_Tabs < Collection_Base

  def initialize(parent)
    super(parent)
    refresh()
  end

  def on_mouse_over_tab(tid, tab)
    return if tid.nil?
  end

  def on_mouse_not_over_tab(tid, tab)
    return if tid.nil?
  end

  def on_mouse_left_click_tab(tid, tab)
    return if tid.nil?
    self.index = tid
  end

  def refresh
    @objects.each { |t| t.dispose } if @objects
    @objects = @parent.tabs.map { |t|
      GUIExt::Tab.new(self,viewport,t,@parent.tab_width,@parent.tab_height)
    }
    each_object_with_index { |o,i| o.tid = i }
    refresh_axyz()
    super()
  end

  def refresh_axyz
    each_object_with_index { |t, i|
      t.ax = i * tab_spacing
    }
  end

  def tab_spacing
    @parent.tab_spacing
  end

  def x
    @parent.tabs_x
  end

  def y
    @parent.tabs_y
  end

  def z
    @parent.tabs_z
  end

  def update_opacity
    self
  end

  def update_index
    each_object { |o| i = o.tid
      act = i == @index
      o.opacity = act && @parent.active ? 255 : 198
      o.ay = (act ? 0 : @parent.tab_active_height - @parent.tab_inactive_height)
      o.az = i + (act ? @objects.size + 1 : 0)
      o.height = (act ? @parent.tab_active_height : @parent.tab_inactive_height)
      o.update_sprite()
    }
    @parent.on_tab_change()
    update_position()
  end

end
