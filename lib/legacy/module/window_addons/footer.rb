# // Opposite of header . x . Just a little sprite at the bottom of a window
module WindowAddons::Footer

  def active_addons
    super + [:tail]
  end

  def init_tail
    @tail_sprite = Sprite::WindowTail.new(self.viewport, self)
    @tail_sprite.refresh
  end

  def dispose_tail
    @tail_sprite.dispose
  end

  def update_tail
    @tail_sprite.update
  end

  def footer_bitmap
    Cache.system("tail_base(window)")
  end

  def _redraw
    super
    @tail_sprite.refresh
  end

  def tail_x
    self.x
  end

  def tail_y
    self.open_y2
    #self.y2
  end

  def tail_z
    self.z
  end

  def tail_width
    self.width
  end

  def tail_height
    14
  end

  def tail_visible?
    self.visible && !self.close?
  end

  def tail_opacity
    255
  end

  def addons_refresh_x
    super
    @tail_sprite.update_position
  end

  def addons_refresh_y
    super
    @tail_sprite.update_position
  end

  def addons_refresh_z
    super
    @tail_sprite.update_position
  end

  def addons_refresh_openness
    super
    @tail_sprite.update_position
    @tail_sprite.update_visible
  end

  def addons_refresh_opacity
    super
    @tail_sprite.update_opacity
  end

  def addons_refresh_visible
    super
    @tail_sprite.update_visible
  end

  def addons_refresh_viewport
    super
    @tail_sprite.update_viewport
  end

  def init_addons
    super
    init_tail
  end

  def dispose_addons
    super
    dispose_tail
  end

  def update_addons
    super
    update_tail
  end

end
