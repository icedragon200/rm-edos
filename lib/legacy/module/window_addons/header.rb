module WindowAddons::Header

  def active_addons
    super + [:header]
  end

  def init_header
    @header_sprite = Sprite::WindowHeader.new(viewport, self)
    @header_sprite.refresh
  end

  def dispose_header
    @header_sprite.dispose
  end

  def update_header
    @header_sprite.update
  end

  def _redraw
    super
    @header_sprite.refresh
  end

  def header_cube
    MACL::Cube.new(
      self.x + 2, self.open_y - 14, self.z,
      96 + (self.width * 0.3).to_i, 14, 0
    )
  end

  def header_bitmap
    Cache.system("header_base(window)")
  end

  def header_text
    ""
  end

  def header_text_settings
    fnt = Font.new.set_style('window_header')
    return {"align" => 0, "font" => fnt}
  end

  def header_visible?
    return self.visible && !self.close?
  end

  def addons_refresh_x
    super
    @header_sprite.update_position
  end

  def addons_refresh_y
    super
    @header_sprite.update_position
  end

  def addons_refresh_z
    super
    @header_sprite.update_position
  end

  def addons_refresh_visible
    super
    @header_sprite.update_visible
  end

  def addons_refresh_openness
    super
    @header_sprite.update_visible
    @header_sprite.update_position
  end

  def addons_refresh_viewport
    super
    @header_sprite.viewport = viewport
  end

  def init_addons
    super
    init_header
  end

  def update_addons
    super
    update_header
  end

  def dispose_addons
    super
    dispose_header
  end

end
