module WindowAddons::Mouse_MoveWindow

  @@mouse_moving = nil

  def active_addons
    super + [:mouse_movewindow]
  end

  def init_mmw
    @@mouse_moving = nil if @@mouse_moving.disposed? if @@mouse_moving
    @can_move_window = true
  end

  def update_mmw
    if @@mouse_moving == self
      if Mouse.rmoved?
        self.x = (Mouse.rx - (self.width / 2)) #if Mouse.x_in_client?
        self.y = (Mouse.ry - (self.height / 2)) #if Mouse.y_in_client?
        clamp_to_space
      end
      MouseCore.tooltip_sprite.set_text("Moving...")
    end
    if mouse_can_move_window?
      if Mouse.middle_click? #|| (@@mouse_moving ? Mouse.left_click? : Mouse.dleft_click?)
        if @@mouse_moving
          if @@mouse_moving == self || @@mouse_moving.disposed?
            @@mouse_moving = nil
          end
        else
          @@mouse_moving = self
          MouseCore.set_pos(self.cx, self.cy)
        end
      end if mouse_in_window?
    else
      @@mouse_moving = nil if @@mouse_moving == self
    end
  end

  def win_busy?
    super || !@@mouse_moving.nil?
  end

  def init_addons
    super
    init_mmw
  end

  def update_addons
    super
    update_mmw
  end

end
