#
# EDOS/src/module/window_addons/option_cursor.rb
#   by IceDragon
#   dc ??/??/2011
#   dm 16/06/2013
# vr 1.0.0
# // 02/18/2012
module WindowAddons
  module OptionCursor

    def init_opt_cursor
      @opt_cursor = Sprite::OptionCursor.new(self.viewport)
      update_opt_cursor_pos
      update_opt_cursor_visible
    end

    def update_opt_cursor
      @opt_cursor.update
      update_opt_cursor_pos
    end

    def dispose_opt_cursor
      @opt_cursor.dispose
    end

    def update_opt_cursor_pos
      r = item_rect_to_screen(cursor_rect)
      @opt_cursor.x = r.x
      @opt_cursor.y = r.y
      @opt_cursor.z = self.z + 1
      @opt_cursor.oy = -(r.height-@opt_cursor.height)/2
      @opt_cursor.ox = @opt_cursor.width
    end

    def update_opt_cursor_visible
      @opt_cursor.visible = self.visible && self.openness > 0
    end

    def update_opt_cursor_active
      @opt_cursor.active = self.active
    end

    def update_opt_cursor_opacity
      @opt_cursor.opacity = 255
    end

    def update_opt_cursor_openness
      update_opt_cursor_visible
    end

    def x=(n)
      super(n)
      update_opt_cursor_pos
    end

    def y=(n)
      super(n)
      update_opt_cursor_pos
    end

    def z=(n)
      super(n)
      update_opt_cursor_pos
    end

    def active=(n)
      super(n)
      update_opt_cursor_active
    end

    def opacity=(n)
      super(n)
      update_opt_cursor_opacity
    end

    def openness=(n)
      super(n)
      update_opt_cursor_openness
    end

    def visible=(n)
      super(n)
      update_opt_cursor_visible
    end

    def viewport=(n)
      super(n)
      @opt_cursor.viewport = self.viewport
    end

    def init_addons
      super
      init_opt_cursor
    end

    def update_addons
      super
      update_opt_cursor
    end

    def dispose_addons
      super
      dispose_opt_cursor
    end

  end
end
