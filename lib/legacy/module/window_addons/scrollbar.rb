module WindowAddons::ScrollBar

  def active_addons
    super + [:scrollbar]
  end

  def init_scroll_bar
    @scroll_bar = Sprite::Scroller.new(self.viewport, self)
  end

  def dispose_scroll_bar
    @scroll_bar.dispose
  end

  def update_scroll_bar
    @scroll_bar.update
  end

  def _redraw
    super
    @scroll_bar.refresh
  end

  def scroll_bar_value_method
    @parent.method(:row_index)
  end

  def scroll_bar_max_method
    @parent.method(:row_max)
  end

  def scroll_bar_type
    :vert
  end

  def scroll_bar_cube
    length = self.height-(standard_padding*2)
    x,y = self.x2-standard_padding,self.y+(self.height-length)/2
    MACL::Cube.new(x,y,self.z+1,8,0,length)
  end

  def scroll_base_draw(bmp)
    bmp.lock do |b|
      rect = b.rect
      Artist.new(b).merio.snapshot do |mer|
        mer.draw_dark_rect(rect)
      end
    end
  end

  def scroll_bar_draw(bmp, type, (xpad, ypad))
    bmp.lock do |b|
      rect = b.rect
      Artist.new(b).merio.snapshot do |mer|
        mer.draw_light_rect(rect)
      end
    end
  end

  def scroll_bar_visible?
    return self.visible && self.open?
  end

  def addons_refresh_x
    super
    @scroll_bar.update_position
  end

  def addons_refresh_y
    super
    @scroll_bar.update_position
  end

  def addons_refresh_z
    super
    @scroll_bar.update_position
  end

  def addons_refresh_visible
    super
    @scroll_bar.update_visible
  end

  def addons_refresh_openness
    super
    @scroll_bar.update_visible
  end

  def addons_refresh_viewport
    super
    @scroll_bar.viewport = viewport
  end

  def init_addons
    super
    init_scroll_bar
  end

  def update_addons
    super
    update_scroll_bar
  end

  def dispose_addons
    super
    dispose_scroll_bar
  end

end
