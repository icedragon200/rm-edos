module WindowAddons::Tabs

  def active_addons
    super + [:tabs] 
  end

  def init_tabs
    @tab_collection = Collection_Tabs.new(self)
  end

  def dispose_tabs
    @tab_collection.dispose
  end  

  def update_tabs
    @tab_collection.update
  end 

  def _redraw
    super
    @tab_collection.refresh
  end 

  def on_tab_change
  end 

  def header_space
    tabs_y < self.y ? self.y - tabs_y : super
  end  

  def tabs
    []
  end  

  def tabs_width
    tab_spacing * tabs.size
  end  

  def tabs_height
    tab_height
  end  

  def tab_width
    96
  end  

  def tab_height
    24
  end

  def tab_active_height
    tab_height
  end  

  def tab_inactive_height
    16
  end 

  def tabs_x
    self.x
  end

  def tabs_y
    self.open_y1 - tabs_height
  end

  def tabs_z
    self.z
  end  

  def tab_spacing
    tab_width - 16
  end  

  def addons_refresh_x
    super
    @tab_collection.update_position
  end  

  def addons_refresh_y
    super
    @tab_collection.update_position
  end  

  def addons_refresh_z
    super
    @tab_collection.update_position
  end  

  def addons_refresh_openness
    super
    @tab_collection.update_position
  end 

  def addons_refresh_opacity
    super
    @tab_collection.update_opacity
  end  

  def addons_refresh_visible
    super
    @tab_collection.update_visible
  end  

  def addons_refresh_viewport
    super
    @tab_collection.update_viewport
  end  

  def init_addons
    super
    init_tabs
  end

  def dispose_addons
    super
    dispose_tabs
  end

  def update_addons
    super
    update_tabs
  end  
  
end
