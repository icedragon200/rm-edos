module WindowAddons::WinButtons

  def active_addons
    super + [:winbuttons]
  end

  def init_win_buttons
    @win_buttons = Collection_WinButtons.new(self)
    @win_buttons.set_handler(:minimize, method(:winbuttons_minimize))
    @win_buttons.set_handler(:maximize, method(:winbuttons_maximize))
    @win_buttons.set_handler(:close, method(:winbuttons_close))
  end

  def wb_openness
    self.height
  end

  def wb_openness=(n)
    self.height = n
  end

  def wb_tminimize # // Target Minimize
    standard_padding * 2
  end

  def wb_tmaximize # // Target Maximize
    winbuttons_orgsize.height
  end

  def wb_do_minimize
    self.wb_openness = (self.wb_openness-(Graphics.height/10.0)).max(wb_tminimize)
    clamp_to_space
    if self.wb_openness <= wb_tminimize
      @minimizing = false
      self.wb_openness = wb_tminimize
    end
  end

  def wb_do_maximize
    self.wb_openness = (self.wb_openness+(Graphics.height/10.0)).min(wb_tmaximize)
    clamp_to_space
    if self.wb_openness >= wb_tmaximize
      @maximizing = false
      self.wb_openness = wb_tmaximize
    end
  end

  def wb_minimizing?
    self.wb_openness > wb_tminimize && @minimizing
  end

  def wb_maximizing?
    self.wb_openness < wb_tmaximize && @maximizing
  end

  def wb_mizing?
    wb_minimizing? || wb_maximizing?
  end

  def win_busy?
    super || wb_mizing?
  end

  def update_win_buttons
    wb_do_minimize if wb_minimizing?
    wb_do_maximize if wb_maximizing?
    @win_buttons.update
  end

  def dispose_win_buttons
    @win_buttons.dispose
  end

  def winbuttons_orgsize
    if @orgsize.nil?
      @orgsize = Rect.new.set(self.to_rect)
      @orgsize.height = wb_openness
    end
    @orgsize
  end

  def winbuttons_minimize
    winbuttons_orgsize
    @minimizing = true
    @maximizing = false
  end

  def winbuttons_maximize
    winbuttons_orgsize
    @minimizing = false
    @maximizing = true
  end

  def winbuttons_close
  end

  def winbuttons_x
    self.x2 - (CacheExt.win_button_rect.width*3 + 4)#+ standard_padding)
  end

  def winbuttons_y
    self.open_y1 - CacheExt.win_button_rect.height  #+ standard_padding
  end

  def winbuttons_z
    self.z #+ 1
  end

  def winbuttons_width
    (CacheExt.win_button_rect.width+1) * 3
  end

  def winbuttons_actives
    [true, true, false]
  end

  def addons_refresh_x
    super
    @win_buttons.update_position
  end

  def addons_refresh_y
    super
    @win_buttons.update_position
  end

  def addons_refresh_z
    super
    @win_buttons.update_position
  end

  def addons_refresh_visible
    super
    @win_buttons.update_visible
  end

  def addons_refresh_openness
    super
    @win_buttons.update_openness
  end

  def addons_refresh_viewport
    super
    @win_buttons.update_viewport
  end

  def init_addons
    super
    init_win_buttons
  end

  def update_addons
    super
    update_win_buttons
  end

  def dispose_addons
    super
    dispose_win_buttons
  end

end
