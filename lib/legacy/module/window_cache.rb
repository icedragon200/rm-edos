#
# module/window_cache.rb
#
Window::TextPalette = MACL::PixelPalette.new("Graphics/Windows/parts/palette_basic.png")
Window::TextPalette.columns = 8
Window::TextPalette.cell_size = 8

Window::SkinCache = MACL::Cacher.new
Window::SkinCache.instance_exec do

  get_windowskin_part_path = ->(*fn) do
    windowskin_parts_path = "Graphics/Windows/parts"
    (fn.map { |f| f ? File.join(windowskin_parts_path, f) : "" })
  end
  #
  # Make a windowkin based on given filenames
  #
  mk_windowskin = ->(background, border, arrows,
                     pattern, cursor, msg_arws,
                     palette) do

    cache = {}
    [background, border, arrows, pattern, cursor, msg_arws, palette].each do |f|
      cache[f] = Bitmap.new(f) if(f && !f.empty?)
    end

    stuff = [
      [0 ,  0, cache[background]],
      [64,  0, cache[border]],
      [80, 16, cache[arrows]],
      [0 , 64, cache[pattern]],
      [64, 64, cache[cursor]],
      [96, 64, cache[msg_arws]],
      [64, 96, cache[palette]]
    ]

    result_bmp = Cache::CacheBitmap.new(128, 128)

    stuff.each do |(x, y, bmp)|
      result_bmp.blt(x, y, bmp, bmp.rect) if bmp
    end

    # clean up
    cache.each_value(&:dispose)
    cache.clear
    GC.start

    # Return the created windowskin
    result_bmp
  end

  # |background, border, arrows, pattern, cursor, msg_arws, palette|

  construct("window") do
    paths = get_windowskin_part_path.(
      "background_basic", "border_normal", "arrows_basic", "pattern_edos",
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

  construct("window_basic") do
    paths = get_windowskin_part_path.(
      "background_basic", "border_basic", "arrows_basic", nil,
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

  construct("window_console") do
    paths = get_windowskin_part_path.(
      "background_basic", "border_console", "arrows_basic", "pattern_code",
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

  construct("window_craft") do
    paths = get_windowskin_part_path.(
      "background_basic", "border_craft", "arrows_basic", "pattern_edos",
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

  construct("window_glass") do
    paths = get_windowskin_part_path.(
      "background_glass", nil, "arrows_basic", nil,
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

  construct("window_smallborder") do
    paths = get_windowskin_part_path.(
      "background_basic", "border_small", "arrows_basic", nil,
      "cursor_basic", "msg_arrows_basic", nil)

    mk_windowskin.(*paths)
  end

end
