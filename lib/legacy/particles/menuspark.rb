#
# EDOS/src/particles/menuspark.rb
# vr 1.0.0
module Particle
  class MenuSpark < Palila::Particle

    def init
      super
      @acceleration.set(
        Palila::Util.range_randomf(-0.05, 0.05), Palila::Util.range_randomf(-0.05, 0.05))
      @velocity.set(
        Palila::Util.range_randomf(-0.5, 0.5), Palila::Util.range_randomf(-0.5, 0.5))
    end

    def init_lifespan
      @life = @lifespan = 60
    end

  end
end
