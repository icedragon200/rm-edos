#
# src/particles/mousespark.rb
# vr 1.0.0
module Particle
  class MouseSpark < Palila::Particle

    def init
      super
      accel = 0.08
      vel = 0.34
      @acceleration.set(
        Palila::Util.range_randomf(-accel, accel), Palila::Util.range_randomf(-accel, accel))
      @velocity.set(
        Palila::Util.range_randomf(-vel, vel), Palila::Util.range_randomf(-vel, vel))
    end

    def init_lifespan
      @life = @lifespan = 60
    end

    attr_accessor :color

  end
end
