#
# EDOS/core/prototypes.rb
#   by IceDragon
#   dc 08/04/2013
#   dm 08/04/2013
# vr 1.0.0
# ■ Prototypes
# // 02/09/2012

# // 02/09/2012
module RPG
  module Rogue
    module RogueConstants
    end
  end
  class BaseItem
    class Range
      module Constants
      end
    end
  end
end

module Mixin
  module SpaceBoxBody
  end
  module Automatable
  end
  module IDisposable
  end
end

module Automation
end

module Lacio
  class SpaceBox
  end
end

module Hazel
  class Shell
    class Base < Shell
    end
    class Window < Base
    end
    class WindowSelectable < Window
    end
    class Command < WindowSelectable
    end
    module Addons
      module Base
      end
      module OwnViewport
      end
      module Background
      end
    end
    module Decoration
    end
    class MerioItemList < WindowSelectable
    end
  end
end

Shell = Hazel::Shell

module WindowAddons
  module Base
  end
end

# // 04/19/2012
module IEI
end

class Sprite
  class Base < Sprite
  end
  class Icon < Base
  end
  class MerioTextBox < Sprite
  end
end

##
# class Game
#
class Game
  class Trap
  end
  class RogueActor
  end
  class RogueEnemy
  end
  class BattlerBase
  end
  class Battler < BattlerBase
  end
end

# REI
module REI
end

class REI::EntityBase
end

class REI::BattlerUnit < Game::Battler
end

class REI::BattlerUnit::Actor < REI::BattlerUnit
end

##
# module Scene
#
module Scene
  class PreBase
  end
  class Base < PreBase
  end
  class MenuBase < Base
  end
  class MenuUnitBase < MenuBase
  end
  class ItemBase < MenuUnitBase
  end
  class Loading < Base
  end
end

class Window
  class Base < Window
  end
  class Selectable < Base
  end
  class Command < Selectable
  end
  class SmallText < Base
  end
  class QuickText < SmallText
  end
  class Equip < Selectable
  end
end