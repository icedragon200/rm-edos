module Kernel

  def require_relative_dir(dirname, &block)
    calldir  = File.expand_path(File.dirname(caller[0]))
    fdirname = File.expand_path(dirname)
    require_dir(File.join(calldir, fdirname.gsub(calldir, '')), &block)
  end

  # Without color and logging
  def require_dir(dirname)
    files = Dir.glob(File.join(dirname, "**/*.rb"))

    yield files if block_given?

    files.sort.each do |f| require f ; end
  end

  # Logging with color
  def require_relative(filename)
    dirname = File.dirname(File.expand_path(caller[0]))
    filename = File.join(dirname, filename.gsub(dirname, '').to_str)
    require filename
    puts ">>$ ".colorize(:light_green) + "Loaded: " + filename.colorize(:light_blue)
  rescue(Exception) => ex
    pstr = ">!! ".colorize(:light_red) + "Failed: " + filename.colorize(:light_blue)
    pstr += "\n#{ex.inspect}\n#{ex.backtrace[0]}"
    puts pstr
    raise ex
  end

  def require_dir(dirname)
    files = Dir.glob(File.join(dirname, "**/*.rb"))

    yield files if block_given?

    files.each do |f|
      begin
        filename = f #File.join(calldir, f.gsub(calldir, ''))
        require filename
        puts ">>$ ".colorize(:light_green) + "Loaded: " + filename.colorize(:light_blue)
      rescue(Exception) => ex
        pstr = ">!! ".colorize(:light_red) + "Failed: " + filename.colorize(:light_blue)
        pstr += "\n#{ex.inspect}\n#{ex.backtrace[0]}"
        puts pstr
        raise ex
      end
    end
  end

  remove_method :require_dir
  remove_method :require_relative_dir

end
