#
# EDOS/src/rpg.rb
#   dm 12/05/2013
#
module RPG
## Prototype
  class BaseItem
  end

  class EquipItem < BaseItem
  end

  class UsableItem < BaseItem
  end

  class Map
  end

end

dir = File.dirname(__FILE__)
Dir.glob(File.join(dir, "rpg", "*.rb")).sort.each do |fn|
  require fn
end
