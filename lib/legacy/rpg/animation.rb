#
# EDOS/src/rpg/Animation.rb
#
class RPG::Animation

  attr_accessor :id
  attr_accessor :name
  attr_accessor :animation1_name
  attr_accessor :animation1_hue
  attr_accessor :animation2_name
  attr_accessor :animation2_hue
  attr_accessor :position
  attr_accessor :frame_max
  attr_accessor :frames
  attr_accessor :timings
  attr_accessor :cell_max

  def initialize
    @id              = 0
    @name            = ''
    @animation1_name = ''
    @animation1_hue  = 0
    @animation2_name = ''
    @animation2_hue  = 0
    @position        = 1
    @frame_max       = 1
    @frames          = [RPG::Animation::Frame.new]
    @timings         = []
  end

  def to_screen?
    @position == 3
  end

  def initialize_add
    @cell_max = @frames.map{ |a| a.cell_max }.max
  end

end

require_relative 'animation/Frame.rb'
require_relative 'animation/Timing.rb'
