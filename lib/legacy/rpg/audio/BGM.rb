 # RPG::BGM (Addon)
class RPG::BGM < RPG::AudioFile

  @@last = RPG::BGM.new

  attr_accessor :pos

  def play(pos = 0)
    if name.empty?
      Audio.bgm_stop
      @@last = RPG::BGM.new
    else
      Audio.bgm_play('Audio/BGM/' + name, volume, pitch, pos)
      @@last = self.clone
    end
  end

  def replay
    play(@pos)
  end

  def vol_rate
    # replace
    #$game.settings ? $game.settings.bgm_volume_rate : 1.0
    1.0
  end

  def volume
    return super * vol_rate #* @@volrate
  end

  def self.stop
    Audio.bgm_stop
    @@last = RPG::BGM.new
  end

  def self.fade(time)
    Audio.bgm_fade(time)
    @@last = RPG::BGM.new
  end

  def self.last
    @@last.pos = Audio.bgm_pos
    @@last
  end

end
