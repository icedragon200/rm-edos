# RPG::BGS (Addon)
class RPG::BGS < RPG::AudioFile

  @@last = RPG::BGS.new

  attr_accessor :pos

  def play(pos = 0)
    if name.empty?
      Audio.bgs_stop
      @@last = RPG::BGS.new
    else
      Audio.bgs_play('Audio/BGS/' + name, volume, pitch, pos)
      @@last = self.dup
    end
  end

  def replay
    play(pos)
  end

  def vol_rate
    #$game.settings ? $game.settings.bgs_volume_rate : 1.0
    1.0
  end

  def volume
    return super * vol_rate
  end

  def self.stop
    Audio.bgs_stop
    @@last = RPG::BGS.new
  end

  def self.fade(time)
    Audio.bgs_fade(time)
    @@last = RPG::BGS.new
  end

  def self.last
    @@last.pos = Audio.bgs_pos
    @@last
  end

end

