# RPG::ME (Addon)
class RPG::ME < RPG::AudioFile

  def play
    if name.empty?
      Audio.me_stop
    else
      Audio.me_play('Audio/ME/' + name, volume, pitch)
    end
  end

  def vol_rate
    #$game.settings ? $game.settings.me_volume_rate : 1.0
    1.0
  end

  def volume
    return super * vol_rate
  end

  def self.stop
    Audio.me_stop
  end

  def self.fade(time)
    Audio.me_fade(time)
  end

end

