# RPG::SE (Addon)
class RPG::SE < RPG::AudioFile

  def play
    unless name.empty?
      Audio.se_play('Audio/SE/' + name, volume, pitch)
    end
  end

  def vol_rate
    ($game && $game.settings) ? $game.settings.se_volume_rate : 1.0
  end

  def volume
    return super * vol_rate
  end

  def self.stop
    Audio.se_stop
  end

end
