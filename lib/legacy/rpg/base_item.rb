#
# EDOS/src/rpg/BaseItem.rb
#   by IceDragon
#   dc 12/05/2013
#   dm 12/05/2013
require_relative 'base_item/feature'
require_relative 'base_item/icon'
require_relative 'base_item/range'

module RPG
  class BaseItem

    VERSION = "3.0.2".freeze

    attr_accessor :id
    attr_accessor :id_offset
    attr_accessor :name
    attr_accessor :title
    attr_accessor :icon
    attr_accessor :description
    attr_accessor :features
    attr_accessor :note
    attr_accessor :tags
    attr_accessor :meta

    attr_accessor :element_id
    attr_accessor :effect_range
    attr_accessor :atk_range

    def initialize
      @id           = 0
      @id_offset    = 0
      @name         = ''
      @title        = ''
      @icon         = RPG::BaseItem::Icon.new(0, 'iconset')
      @description  = ''
      @features     = [] # Array<RPG::BaseItem::Feature>
      @note         = ''
      @tags         = [] # Array<String>
      @meta         = {} # Hash<String, Object>
      @element_id   = 0
      @effect_range = RPG::BaseItem::Range.new(RPG::BaseItem::Range::RANGE_DIAMOND, 0, 0)
      @atk_range    = RPG::BaseItem::Range.new(RPG::BaseItem::Range::RANGE_DIAMOND, 1, 1)
    end

    def initialize_add

    end


    def all_tags
      # super-fy with subclasses to add Class related tags
      # (something which is common to a group of objects)
      @tags
    end

    ##
    # id_abs
    #   Absolute ID or Database ID, used for Item sorting
    def id_abs
      @id_offset + @id
    end

    ## alias
    # icon_index
    def icon_index
      warn "Use of #icon_index is depreceated: \n#{caller[0, 4].join("\n")}"
      @icon.index
    end

    def icon_index=(new_index)
      warn "Use of #icon_index= is depreceated: \n#{caller[0, 4].join("\n")}"
      @icon.index = new_index
    end

    def iconset_name
      warn "Use of #iconset_name is depreceated: \n#{caller[0, 4].join("\n")}"
      @icon.iconset_name
    end

    def iconset_name=(new_iconset_name)
      warn "Use of #iconset_name= is depreceated: \n#{caller[0, 4].join("\n")}"
      @icon.iconset_name = new_iconset_name
    end

  end
end