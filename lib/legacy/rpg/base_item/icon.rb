#
# EDOS/src/rpg/baseitem/Icon.rb
#   by IceDragon
#   dc 12/05/2013
#   dm 12/05/2013
# vr 1.0.0
module RPG
  class BaseItem
    class Icon

      VERSION = "1.0.0".freeze

      attr_accessor :iconset_name
      attr_accessor :index

      def initialize(index, iconset_name="iconset")
        @iconset_name, @index = iconset_name, index
      end

    end
  end
end
