# RPG::Class::Learning (Addon)
module RPG
  class Class < BaseItem
    class Learning

      attr_accessor :level
      attr_accessor :skill_id
      attr_accessor :note

      def initialize
        @level = 1
        @skill_id = 1
        @note = ''
      end

      def set(level=@level, skill_id=@skill_id, note=@note)
        @level    = level
        @skill_id = skill_id
        @note     = note
        self
      end

    end
  end
end
