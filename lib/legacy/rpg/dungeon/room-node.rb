# RPG::Dungeon::Room::Node
# // 12/19/2011
# // 12/21/2011
class RPG::Dungeon::Room::Node

  attr_accessor :x, :y
  attr_accessor :map_x, :map_y

  def initialize( x, y )
    set( x, y )
    set_map( x, y )
  end  

  def set( x, y )
    @x, @y = x, y
    return self
  end 

  def set_map( x, y )
    @map_x, @map_y = x, y
    return self
  end  

  def real_x
    return @map_x + @x
  end

  def real_y
    return @map_y + @y
  end

  def to_pos_r()
    return IEI::Pos.new( real_x, real_y )
  end  
  
end  
