# RPG::Dungeon::Room
# // 12/19/2011
# // 12/21/2011
class RPG::Dungeon::Room
  def initialize( x1, y1, x2, y2, type=0 )
    @id = 0
    @x1, @y1 = x1, y1
    @x2, @y2 = x2, y2
    @type = type
    @features = []
    @nodes = [] # Node[]
    @data = Table.new( 1, 1, 4 )
    @floor_rect = Rect.new( 0, 0, 1, 1 )
    @seed = srand
    resize_data()
  end
  def seed=( nseed )
    @seed = nseed
    set_seed_rand()
  end
  def set_seed_rand()
    @seed_rand = Random.new( @seed )
  end
  def floor_pos_x( fx )
    return self.x + fx
  end
  def floor_pos_y( fy )
    return self.y + fy
  end
  def floor_rect_map()
    return Rect.new( floor_pos_x( @floor_rect.x ), floor_pos_y( @floor_rect.y ),
      floor_pos_x( @floor_rect.width+1 ), floor_pos_y( @floor_rect.height+1 ) )
  end
  def random_floor_pos( cont=0 )
    flr = floor_rect_map.contract(anchor: 5, amount: cont)
    rx = flr.x + @seed_rand.rand(flr.width-flr.x+1)
    ry = flr.y + @seed_rand.rand(flr.height-flr.y+1)
    return rx, ry
  end
  def add_node( x, y )
    n = Node.new( x, y )
    @nodes << n
    return n
  end
  def remap_nodes
    @nodes.each { |n| n.set_map( self.x, self.y ) }
  end
  def resize_data
    @data.resize( width, height, 4 )
  end
  def x
    @x1
  end
  def y
    @y1
  end
  def x=( new_x )
    @x2 = new_x + width
    @x1 = new_x
  end
  def y=( new_y )
    @y2 = new_y + height
    @y1 = new_y
  end
  def set_xy( x, y )
    self.x = x
    self.y = y
    return self
  end
  def width
    return @x2 - @x1
  end
  def height
    return @y2 - @y1
  end
  def width=( new_width )
    @x2 = @x1 + new_width
  end
  def height=( new_height )
    @y2 = @y1 + new_height
  end
  def to_va()
    return [@x1, @y1, @x2, @y2]
  end
  def to_vrect
    return Rect.new( @x1, @y1, @x2, @y2 )
  end
  def to_a
    return [@x1, @y1, width, height]
  end
  def to_rect
    return Rect.new( @x1, @y1, width, height )
  end
  def to_v
    return Rect.new( @x1, @y1, width, height )
  end
  attr_accessor :id
  attr_accessor :x1, :x2, :y1, :y2
  attr_accessor :type
  attr_accessor :features
  attr_accessor :nodes
  attr_accessor :data
  attr_accessor :floor_rect
  attr_reader :seed
  attr_reader :seed_rand
end
