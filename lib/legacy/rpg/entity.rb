#
# EDOS/src/rpg/Entity.rb
#   by IceDragon
#   dc 11/05/2013
#   dm 12/05/2013
# vr 1.0.0
class RPG::Entity < RPG::BaseItem

  def initialize
    super
    # Data
    @nickname        = '' # String
    @class_id        = 1  # UInteger
    @initial_level   = 1  # UInteger
    @max_level       = 99 # UInteger
    @equips          = Array.new(8, 0) # Array<Integer>

    # Looks
    @character_name  = '' # String
    @character_index = 0  # UInteger
    @character_hue   = 0  # UInteger(0...360)

    @face_name       = '' # String
    @face_index      = 0  # UInteger
    @face_hue        = 0  # UInteger(0...360)

    @portrait_name   = ''
    @portrait_hue    = 0
  end

  attr_accessor :nickname
  attr_accessor :class_id
  attr_accessor :initial_level
  attr_accessor :max_level
  attr_accessor :character_name
  attr_accessor :character_index
  attr_accessor :character_hue
  attr_accessor :face_name
  attr_accessor :face_index
  attr_accessor :face_hue
  attr_accessor :portrait_name
  attr_accessor :portrait_hue
  attr_accessor :equips

end
