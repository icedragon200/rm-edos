# RPG::EquipItem (Addon)
class RPG::EquipItem < RPG::BaseItem

  attr_accessor :price
  attr_accessor :etype_id
  attr_accessor :params
  attr_accessor :exp_params
  attr_accessor :level_params
  attr_accessor :growths

  def initialize
    super
    @price = 0
    @etype_id = 0
    @params = [0] * 8
    @growths = []
    @exp_params = [10,10,10,10]
    rebuild_parameter_table( 100 )
  end

  def rebuild_parameter_table( maxlevel=100 )
    @level_params = Table.new( 8, maxlevel )
    if block_given?()
      (1..maxlevel).each do |i|
        for j in 0...8
          # Parameter, Level = block { |parameter, level| integer }
          @level_params[j,i] = yield j, i, self
        end
      end
    else
      (1..maxlevel).each do |i|
        @level_params[0,i] = 0#400+i*50
        @level_params[1,i] = 0#80+i*10
        (2..5).each {|j| @level_params[j,i] = 0 }#15+i*5/4 }
        (6..7).each {|j| @level_params[j,i] = 0 }#30+i*5/2 }
      end
    end
  end

  def exp_for_level(level)
    lv = level.to_f
    basis = @exp_params[0].to_f
    extra = @exp_params[1].to_f
    acc_a = @exp_params[2].to_f
    acc_b = @exp_params[3].to_f
    return (basis*((lv-1)**(0.9+acc_a/250))*lv*(lv+1)/
      (6+lv**2/50/acc_b)+(lv-1)*extra).round.to_i
  end

  def param(param_id)
    params[param_id]
  end

end
