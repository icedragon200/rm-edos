# RPG::EventCommand (Addon)
class RPG::EventCommand

  def initialize(code = 0, indent = 0, parameters = [])
    @code = code
    @indent = indent
    @parameters = parameters
  end

  def initialize_add
    #
  end

  def self.each_comment(ev_list)
    ev_list.each { |c| yield c.parameters.to_s if COMMENT_CODES.include?(c.code) }
  end

  def to_s_exp
    ("  " * @indent) + "CODE: #@code - #{@parameters.join(", ")}"
  end

  attr_accessor :code
  attr_accessor :indent
  attr_accessor :parameters

end