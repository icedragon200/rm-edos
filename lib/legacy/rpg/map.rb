class RPG::Map

  VERSION = "2.0.0"

  attr_accessor :display_name
  attr_accessor :tileset_id
  attr_accessor :width
  attr_accessor :height
  attr_accessor :scroll_type
  attr_accessor :specify_battleback
  attr_accessor :battleback1_name
  attr_accessor :battleback2_name
  attr_accessor :autoplay_bgm
  attr_accessor :bgm
  attr_accessor :autoplay_bgs
  attr_accessor :bgs
  attr_accessor :disable_dashing
  attr_accessor :encounter_list
  attr_accessor :encounter_step
  attr_accessor :parallax_name
  attr_accessor :parallax_loop_x
  attr_accessor :parallax_loop_y
  attr_accessor :parallax_sx
  attr_accessor :parallax_sy
  attr_accessor :parallax_show
  attr_accessor :note
  attr_accessor :data
  attr_accessor :events

  def initialize(width, height)
    @display_name = ''
    @tileset_id = 1
    @width = width
    @height = height
    @scroll_type = 0
    @specify_battleback = false
    @battleback_floor_name = ''
    @battleback_wall_name = ''
    @autoplay_bgm = false
    @bgm = RPG::BGM.new
    @autoplay_bgs = false
    @bgs = RPG::BGS.new('', 80)
    @disable_dashing = false
    @encounter_list = []
    @encounter_step = 30
    @parallax_name = ''
    @parallax_loop_x = false
    @parallax_loop_y = false
    @parallax_sx = 0
    @parallax_sy = 0
    @parallax_show = false
    @note = ''
    @data = Table.new(width, height, 4)
    @events = {}
  end

  def is_rgss3_map?
    @display_name != nil
  end

  def convert_to_rgss3_map
    clone.convert_to_rgss3_map!
  end

  def convert_to_rgss3_map!
    @display_name ||= ''
    @tileset_id   ||= 0 # // 1
    @specify_battleback = false if @specify_battleback.nil?()
    @battleback_floor_name ||= ''
    @battleback_wall_name  ||= ''
    @autoplay_bgm = false if @autoplay_bgm.nil?()
    @bgm ||= RPG::BGM.new
    @autoplay_bgs = false if @autoplay_bgs.nil?()
    @bgs ||= RPG::BGS.new('', 80)
    @disable_dashing = false if @disable_dashing.nil?()
    @encounter_list ||= []
    @encounter_step ||= 30
    @parallax_name  ||= ''
    @parallax_loop_x = false if @parallax_loop_x.nil?()
    @parallax_loop_y = false if @parallax_loop_y.nil?()
    @parallax_sx ||= 0
    @parallax_sy ||= 0
    @parallax_show = false if @parallax_show.nil?()
    @note   ||= ''
    @data.resize(@data.xsize, @data.ysize, 4)
    @events ||= {}
  end

end

class RPG::Map::Encounter
  def initialize
    @troop_id = 1
    @weight = 10
    @region_set = []
  end
  attr_accessor :troop_id
  attr_accessor :weight
  attr_accessor :region_set
end

class RPG::MapInfo
  def initialize
    @name = ''
    @parent_id = 0
    @order = 0
    @expanded = false
    @scroll_x = 0
    @scroll_y = 0
  end
  attr_accessor :name
  attr_accessor :parent_id
  attr_accessor :order
  attr_accessor :expanded
  attr_accessor :scroll_x
  attr_accessor :scroll_y
end
