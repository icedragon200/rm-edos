# RPG::Splash
class RPG::Splash

  module Command_Codes
    NIL_CODE           = 0
    WAIT_CODE          = 1
    SHOW_PICTURE_CODE  = 11
    ALIGN_PICTURE_CODE = 12
    ERASE_PICTURE_CODE = 13
    FLASH_PICTURE_CODE = 14
    FREEZE_CODE        = 51
    TRANSITION_CODE    = 52
    SPLASH_END_CODE    = 101
  end  

  include Command_Codes

  def initialize( filename="", wait_count=180 )
    set_default_sequence( filename, wait_count )
  end  

  def set_default_sequence( filename="", wait_count=180 ) 
    @sequence = [
      SequenceCommand.new(FREEZE_CODE       , []),
      SequenceCommand.new(SHOW_PICTURE_CODE , [0, filename]),
      SequenceCommand.new(ALIGN_PICTURE_CODE, [0, 0, 1]),
      SequenceCommand.new(ALIGN_PICTURE_CODE, [0, 1, 1]),
      SequenceCommand.new(TRANSITION_CODE   , [60]),
      SequenceCommand.new(WAIT_CODE         , [wait_count]),
      SequenceCommand.new(FREEZE_CODE       , []),
      SequenceCommand.new(ERASE_PICTURE_CODE, [0]),
      SequenceCommand.new(SPLASH_END_CODE   , [])
    ]
  end  

  attr_accessor :sequence
  
end  

require_relative 'splash/seqcommand.rb'
