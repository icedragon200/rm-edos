# RPG::Splash::Sequence
class RPG::Splash::SequenceCommand

  def initialize(code, params)
    set( code, params )
  end  

  def set(code, params)
    @code = code
    @params = params
    return self
  end  

  attr_accessor :code
  attr_accessor :params

end  
