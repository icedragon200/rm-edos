# RPG::System::Terms (Addon)
class RPG::System::Terms

  attr_accessor :basic
  attr_accessor :params
  attr_accessor :etypes
  attr_accessor :commands
  attr_accessor :params_a
  attr_accessor :rogue

  def initialize
    @basic = Array.new(8) {''}
    @params = Array.new(8) {''}
    @etypes = Array.new(5) {''}
    @commands = Array.new(23) {''}
  end

  def initialize_add
    @params_a = Array.new(@params.size) { "" }
    @rogue    = Array.new(30) { "" }
  end

end
