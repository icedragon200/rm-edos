class RPG::Troop

  attr_accessor :id
  attr_accessor :name
  attr_accessor :members
  attr_accessor :pages

  def initialize
    @id = 0
    @name = ''
    @members = []
    @pages = [RPG::Troop::Page.new]
  end

end

require_relative 'troop/Member.rb'
require_relative 'troop/Page.rb'
