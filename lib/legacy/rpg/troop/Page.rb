class RPG::Troop::Page

  attr_accessor :condition
  attr_accessor :span
  attr_accessor :list

  def initialize
    @condition = RPG::Troop::Page::Condition.new
    @span = 0
    @list = [RPG::EventCommand.new]
  end

end

require_relative 'page/Condition.rb'
