#
# EDOS/src/sadie/sprites.rb
# vr 0.1.0
module Sadie
  module Sprite

    class ReaktorBase < ::Sprite

      def initialize(viewport, reaktor)
        super(viewport)
        @reaktor = reaktor
        self.bitmap = Bitmap.new(32, 32)
        init
      end

      def dispose
        dispose_bitmap_safe
        super
      end

      def init
        # overload
      end

      def refresh
        # do something
      end

    end

    class Counter < ReaktorBase

      def init
        super
        @reaktor.add_callback(:on_react_abs, ->(_,_,_) { refresh })
      end

      def refresh
        bitmap = self.bitmap
        bitmap.clear
        bitmap.draw_gauge_ext(bitmap.rect, 1.0, DrawExt::BLUE_BAR_COLORS)
        bitmap.draw_text(bitmap.rect, @reaktor.counter, 1)
      end

    end

    class Emitter < ReaktorBase

      def init
        super
        @counter = 0
        @reaktor.add_callback(:on_emit, ->(_,_,_) { trigger })
        refresh
      end

      def refresh
        self.bitmap.clear
        self.bitmap.draw_gauge_ext(self.bitmap.rect, 1.0, DrawExt::RED_BAR_COLORS)
      end

      def trigger
        @counter = 15
      end

      def update
        super
        self.opacity = (@counter / 15.0) * 255
        @counter -= 1 if @counter > 1
        #p self.opacity
      end

    end

    class Relay < ReaktorBase

      def init
        super
        @reaktor.add_callback(:on_react_coil, ->(_,_,_) { refresh })
        refresh
      end

      def refresh
        self.bitmap.clear
        rect = self.bitmap.rect
        rect.height /= 2
        rect.y += rect.height if @reaktor.coil_state
        self.bitmap.draw_gauge_ext(rect, 1.0, DrawExt::GREEN_BAR_COLORS)
      end

      def update
        super
      end

    end

    class Floodgate < ReaktorBase

      def init
        super
        @reaktor.add_callback(:on_react_flood, ->(_,_,_) { self.opacity = 255 })
        refresh
      end

      def refresh
        self.bitmap.clear
        rect = self.bitmap.rect
        colors = DrawExt::YELLOW_BAR_COLORS
        self.bitmap.draw_gauge_ext(rect, 1.0, colors)
      end

      def update
        super
        self.opacity -= 255 / 20.0 if self.opacity > 0
      end

    end

    class Drain < ReaktorBase

      def init
        super
        @max = 0
        @value = 0
        @reaktor.add_callback(:on_react_drain_pre, ->(_,_,energy){ @max = @value = energy.value })
        refresh
      end

      def refresh
        mm = @max.max(1).to_f
        self.bitmap.clear
        rect = self.bitmap.rect
        colors = DrawExt::RUBY_BAR_COLORS
        r = (@value.max(0) / mm).clamp(0, 1.0)
        self.bitmap.draw_gauge_ext(rect, r, colors)
        self.bitmap.draw_text(rect, @value.to_i, 1) if @value > 0
        @value -= mm / 40.0
      end

      def update
        super
        refresh unless @value < 0
      end

    end

  end
end
