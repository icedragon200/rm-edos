#
# Earthen/src/scene/animation_player.rb
#   by IceDragon
#   dc 24/02/2013
#   dm 24/02/2013
# vr 0.10
class Scene::AnimationPlayer < Scene::Base

  def start
    super
    @animation = RemoteAnimation.new
    #@animation.anchor_oxy(anchor: 5)
    @animation.align_to!(anchor: 5)
    @help_window = Window::Help.new(0, 0)
    @help_text_fmt = "[>] Next || [<] Prev || [C] Play || Animation: %d %s"
    @help_window.set_text(@help_text_fmt % [0, ""])

    @data_animations = load_data("data-db/animations.rvdata2")

    puts "Loaded Animations.rvdata2"
    puts "There are #{@data_animations.size} animations in total"

    names = @data_animations.map do |a| a ? [a.id, a.name] : [0, ""] end
    names.each_with_index do |a, i|
      a[0] = i
    end
    longest = names.map(&:last).max_by(&:length).length
    cols = 5
    fmt = "|%03d|%-0#{longest}s"
    slices = names.each_slice(cols).to_a
    slices.each_with_index do |nms, i|
      nms.push([0, ""]) while nms.size < cols
      nms.each { |pr| print fmt % pr }
      puts ""
    end
    @animation_id = 0
  end

  def update
    super
    if Input.repeat?(:LEFT)
      if @animation_id > 0
        @animation_id -= 1
        Sound.play_cursor
      else
        Sound.play_buzzer
      end
    elsif Input.repeat?(:RIGHT)
      if @animation_id < (@data_animations.size - 1)
        @animation_id += 1
        Sound.play_cursor
      else
        Sound.play_buzzer
      end
    end

    if @last_animation_id != @animation_id
      @last_animation_id = @animation_id
      anim = @data_animations[@last_animation_id]
      nm = anim ? anim.name : "[NULL]"
      @help_window.set_text(@help_text_fmt % [@last_animation_id, nm])
    end

    if Input.trigger?(:C)
      animation = @data_animations[@last_animation_id]
      begin
        @animation.start_animation(animation)
      rescue => ex
        puts "Animation #@animation_id failed to play #{ex.inspect}"
        @animation.start_animation(nil)
      end
    end
    @animation.update
  end

end
