# Scene::Book
#==============================================================================#
# ♥ Scene::Book
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0
#==============================================================================#
class Scene::Book < Scene::Base

  def start
    super
    @status_window = window_status.new
    @list_window   = window_list.new( 0, 0 )
    @list_window.x = Graphics.width - @list_window.width
    @list_window.help_window = @status_window
    @list_window.set_handler( :ok, method( :on_ok ) )
    @list_window.set_handler( :cancel, method( :on_cancel ) )
    window_manager.adds( @status_window, @list_window )
  end

  def on_ok
  end

  def on_cancel
    return_scene
  end

  def terminate
    super
  end

  def update
    super
  end

  def window_list
  end

  def window_status
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
