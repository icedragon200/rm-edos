# Scene::ButtonTest
# // 02/24/2012
# // 02/24/2012
class Scene::ButtonTest < Scene::Base

  def start
    super
    create_background
    @button_window = Shell::ButtonTest.new(0,0)
    @button_window.align_to!(anchor: 5)
    @button_window.set_handler(
      :button_size_change, @button_window.method(:center_xy))
    @button_window.set_handler(
      :wb_close, method(:on_button_close))
    window_manager.add(@button_window)
    @button_window.start_open
  end

  def post_start
    super
    @button_window.activate
  end

  def terminate
    dispose_background
    super
  end

  def on_button_close
    @button_window.start_close
    wait_for_windows(@button_window)
    return_scene
  end

  def update
    update_background
    super
    on_button_close if Input.trigger?(:escape)
  end

end
