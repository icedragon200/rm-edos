# Scene::Craft
#==============================================================================#
# ♥ Scene::Craft
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/02/2012
# // • Data Modified : 01/03/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/02/2011 V1.0
#
#==============================================================================#
class Scene::Craft < Scene::Base

  def start
    super
    create_all_windows
    RPG::BGM.new("Bribs",100,100).play
    #dropdown_windows( *@windows )
    pop_chest_items(
      :items => [
        [$data_items[1], 1],
        [$data_items[2], 3],
        [$data_weapons[1], 1],
        [$data_weapons[2], 1],
        [$data_weapons[11], 1],
        [$data_weapons[21], 1]
      ],
      :text => "Got Items",
      :wait => 180
    ) { |win| win.salign!(5) }
  end

  def create_all_windows()
    create_craft_input_window()
    create_craft_output_window()
    create_help_window()
    create_item_window()
    create_help_window2()
    position_windows()
    @craft_input_window.help_window = @help_window
    @item_window.help_window = @help_window
    @item_window.refresh()
    @help_window.refresh()
    refresh_craft()
  end

  def position_windows()
    center_windows(true, true) # // Center x && y
  end

  def craft_input_ok()
    @item_window.activate()
  end

  def create_craft_input_window()
    @craft_input_window = Window::CraftInput.new( 0, 0 )
    @craft_input_window.set_handler(:ok, method(:craft_input_ok))
    window_manager.add( @craft_input_window )
  end

  def create_craft_output_window()
    @craft_output_window = Window::CraftOutput.new( @craft_input_window.x2, @craft_input_window.y )
    window_manager.add( @craft_output_window )
  end

  def refresh_craft
    @craft_output_window.item = CraftSystem.sc_get_craft_from(@craft_input_window.items)
  end

  def item_ok()
    @craft_input_window.set_current_item(@item_window.item)
    @craft_input_window.activate
    refresh_craft()
  end

  def create_item_window()
    @item_window = Window::FullInventory.new( @help_window.x, @help_window.y2 )
    @item_window.set_handler(:ok, method(:item_ok))
    window_manager.add( @item_window )
  end

  def create_help_window()
    arra = @craft_input_window.to_a
    arra[1] = @craft_input_window.y2
    arra[3] = 40
    @help_window = Window::Help2.new( *arra )
    window_manager.add( @help_window )
  end

  def create_help_window2()
    @help_window2 = Window::Help.new(
     @item_window.x, @item_window.y2, @item_window.width, 40 )
    window_manager.add( @help_window2 )
  end

  def update()
    super()
    dropdown_windows( *@windows ) if Input.trigger?( :A )
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
