
class Scene::Equip < Scene::MenuUnitBase
  #--------------------------------------------------------------------------
  # ● 開始処理
  #--------------------------------------------------------------------------
  def start
    super
    create_help_window
    create_status_window
    create_command_window
    create_slot_window
    create_item_window
  end

  def create_help_window
    rect = Rect.new(0, 0, 0, 96) # // . x . To gain access to surface stuff
    rect = rect.expand(anchor: 5, amount: 8)
    @help_window = Window::EquipHelp.new(
      @canvas.x, @canvas.y, @canvas.width, rect.height)
    @help_window.viewport = @viewport
    @help_window.set_unit(@unit)

    window_manager.add(@help_window)
  end

  def create_status_window
    @status_window = Window::EquipStatus.new(@help_window.x, @help_window.y2, @help_window.width)
    @status_window.viewport = @viewport
    @status_window.set_unit(@unit)

    window_manager.add(@status_window)
  end

  def create_command_window
    wx = @status_window.x
    wy = @status_window.y2
    ww = @status_window.width
    @command_window = Window::EquipCommand.new(wx, wy, ww)
    @command_window.viewport = @viewport
    @command_window.help_window = @help_window
    @command_window.set_handler(:equip,    method(:command_equip))
    @command_window.set_handler(:optimize, method(:command_optimize))
    @command_window.set_handler(:clear,    method(:command_clear))
    @command_window.set_handler(:cancel,   method(:return_scene))
    @command_window.set_handler(:pagedown, method(:next_unit))
    @command_window.set_handler(:pageup,   method(:prev_unit))

    window_manager.add(@command_window)
  end

  def create_slot_window
    wx = @help_window.x
    wy = @command_window.y2
    ww = nil #@help_window.width#Graphics.width - @status_window.width
    @slot_window = Window::EquipSlot.new(wx, wy, ww)
    @slot_window.viewport = @viewport
    @slot_window.help_window = @help_window
    @slot_window.status_window = @status_window
    @slot_window.set_unit(@unit)
    @slot_window.set_handler(:ok,       method(:on_slot_ok))
    @slot_window.set_handler(:cancel,   method(:on_slot_cancel))

    window_manager.add(@slot_window)
  end

  def create_item_window
    wx = @slot_window.x2
    wy = @slot_window.y
    ww = @canvas.width - @slot_window.width
    wh = @slot_window.height
    @item_window = Window::EquipItem.new(wx,wy,ww,wh)#Window::EquipItem.new(wx, wy, ww, wh)
    @item_window.viewport = @viewport
    @item_window.help_window = @help_window
    @item_window.status_window = @status_window
    @item_window.set_unit(@unit)
    @item_window.set_handler(:ok,     method(:on_item_ok))
    @item_window.set_handler(:cancel, method(:on_item_cancel))
    @slot_window.item_window = @item_window

    window_manager.add(@item_window)
  end

  def command_equip
    @slot_window.activate
    @slot_window.select(0)
  end

  def command_optimize
    Sound.play_equip
    @unit.entity.optimize_equipments
    @status_window.refresh
    @slot_window.refresh
    @command_window.activate
  end

  def command_clear
    Sound.play_equip
    @unit.entity.clear_equipments
    @status_window.refresh
    @slot_window.refresh
    @command_window.activate
  end

  def on_slot_ok
    @item_window.activate
    @item_window.select(0)
  end

  def on_slot_cancel
    @slot_window.unselect
    @command_window.activate
  end

  def on_item_ok
    Sound.play_equip
    @unit.entity.change_equip(@slot_window.index, @item_window.item)
    @slot_window.activate
    @slot_window.refresh
    @item_window.unselect
    @item_window.refresh
    @status_window.refresh()
  end

  def on_item_cancel
    @slot_window.activate
    @item_window.unselect
  end

  def on_unit_change
    window_cycle_ajar do
      @help_window.set_unit(@unit)
      @status_window.set_unit(@unit)
      @slot_window.set_unit(@unit)
      @item_window.set_unit(@unit)
      @command_window.activate
    end
  end

end
