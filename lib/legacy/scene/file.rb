#
# EDOS/src/scene/file.rb
#   by IceDragon
#   dc ??/??/2011
#   dm 22/06/2013
# vr 2.0.0
class Scene::File < Scene::MenuBase

  ##
  # start
  def start
    super
  end

  def create_all_windows
    super
    create_savefiles_window
    create_scrollbar
    @help_window.set_text(help_window_text)
    init_selection
  end

  ##
  # create_savefiles_window
  def create_savefiles_window
    x = @canvas.x
    y = @canvas.y
    w = @canvas.width - Metric.ui_element
    h = @canvas.height - @help_window.height - @title_window.height
    @savefiles_window = Shell::MerioFiles.new(x, y, w, h)
    @savefiles_window.set_handler(:ok, method(:on_savefile_ok))
    @savefiles_window.set_handler(:cancel, method(:on_savefile_cancel))
    window_manager.add(@savefiles_window)
  end

  ##
  # create_scrollbar
  def create_scrollbar
    w = Metric.ui_element
    h = @savefiles_window.height
    @file_scrollbar = MerioScrollBar.new(@viewport, w, h)
    @file_scrollbar.align_to!(anchor: 9, surface: @canvas)
    @file_scrollbar.padding = @savefiles_window.padding
    @savefiles_window.add_callback(:index=) do |win, *_, &_|
      @file_scrollbar.set_index_max(win.row_max)
      @file_scrollbar.set_index(win.row_index)
    end
  end

  ##
  # create_confirm_window
  def create_confirm_window
    @confirm_window = Window::Confirm.new(0, 0)
    @confirm_window.help_window = @help_window
    window_manager.add(@confirm_window)
  end

  ##
  # title_text -> String
  def title_text
    "File Selection"
  end

  ##
  # terminate
  def terminate
    super
    @file_scrollbar.dispose
  end

  ##
  # update
  def update
    super
    @file_scrollbar.update
  end

  ##
  # init_selection
  def init_selection
    @savefiles_window.select(first_savefile_index)
  end

  ##
  # first_savefile_index
  def first_savefile_index
    return 0
  end

  ##
  # help_window_text
  def help_window_text
    return ""
  end

  ##
  # on_savefile_ok
  def on_savefile_ok
    ##
  end

  ##
  # on_savefile_cancel
  def on_savefile_cancel
    Sound.play_cancel
    return_scene
  end

end

require_relative 'file/save.rb'
require_relative 'file/load.rb'
require_relative 'file/save-force.rb'
require_relative 'file/load-force.rb'
require_relative 'file/file_select.rb'