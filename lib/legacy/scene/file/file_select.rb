class Scene::FileSelect < Scene::File

  def help_window_text
    "Choose a Slot"
  end

  def first_savefile_index
    DataManager.first_empty_savefile_index || 0
  end

  def on_savefile_ok
    confirm_selection
  end

  ##
  # on_savefile_cancel
  def on_savefile_cancel
    scene_manager.clear
    scene_manager.goto(Scene::Title)
  end

  def confirm_selection
    create_confirm_window
    @confirm_window.set_title("Use Slot #{@savefiles_window.index + 1}")
    @confirm_window.set_handler(:ok,     method(:on_confirm_ok))
    @confirm_window.set_handler(:cancel, method(:on_confirm_cancel))
    @confirm_window.align_to!(anchor: 5, surface: menu_canvas)
    @confirm_window.z = 9999
  end

  def on_confirm_ok
    DataManager.last_savefile_index = @savefiles_window.index
    Sound.play_save
    return_scene
  end

  def on_confirm_cancel
    @savefiles_window.activate
    window_manager.delay_dispose(@confirm_window)
    @confirm_window = nil
  end

end