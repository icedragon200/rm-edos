# Scene::ForceSave
#==============================================================================#
# ♥ Scene::ForceSave
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/31/2011
# // • Data Modified : 12/31/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/31/2011 V1.0
#
#==============================================================================#
class Scene::ForceSave < Scene::Save

  def start
    super
    @savefiles_window.remove_handler(:cancel)
  end

  def help_window_text
    "Select file for New Game"
    #Vocab::SaveMessage
  end

  def on_save_success()
    super()
    $game.temp.other[0] = @savefiles_window.index
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
