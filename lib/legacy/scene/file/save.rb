class Scene::Save < Scene::File

  def help_window_text
    "Choose Save"
    #Vocab::SaveMessage
  end

  def first_savefile_index
    DataManager.last_savefile_index
  end

  def on_savefile_ok
    super
    if DataManager.save_game(@savefiles_window.index)
      on_save_success
    else
      raise SaveFailed
    end
  end

  def on_save_success
    Sound.play_save
    #$game.temp.other[0] = @savefiles_window.index
    return_scene
  end

end
