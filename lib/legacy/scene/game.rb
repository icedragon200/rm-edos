##
# class Window::DebugEditor
#
class Window::DebugEditor < Shell::Command

  include Hazel::Shell::Addons::Background

  def initialize(x, y)
    super(x, y)
  end

  def make_decorations
    deco = add_decoration(:title)
    deco.set_text("Debug Editor", 0)
  end

  def spacing
    0
  end

  def standard_padding
    0
  end

  def make_command_list
    add_command("Character", :character)
    add_command("Party",     :party)
    add_command("Inventory", :inventory)
  end

  def update
    super
  end

  def update_help
    text =  case current_symbol
            when :character then "Goto Character Editor"
            when :party     then "Goto Party Editor"
            when :inventory then "Goto Inventory Editor"
            else             ""
            end
    @help_window.set_text(text)
  end

end

##
# class Window::GameCommand
#
class Window::GameCommand < Shell::Command

  include Hazel::Shell::Addons::Background

  def initialize(x, y)
    super(x, y)
  end

  def make_decorations
    deco = add_decoration(:title)
    deco.set_text("Game", 0)
  end

  def spacing
    0
  end

  def standard_padding
    0
  end

  def make_command_list
    #add_command()
    add_command('Menu',   :menu)
    add_command('Editor', :editor)
    add_command('Map',    :map)
    add_command('REI',    :rei, false)
    add_command('Title',  :title)
  end

  def update
    super
  end

  def update_help
    text =  case current_symbol
            when :menu   then "Goto Main Menu"
            when :editor then "Goto Ingame Editors"
            when :map    then "Goto Map"
            when :rei    then "Enter REI mode"
            when :title  then "Return to title"
            else              ""
            end
    @help_window.set_text(text)
  end

end

class Shell::EditorWindow < Shell::WindowSelectable

  include Hazel::Shell::Addons::Background

  def initialize(x, y)
    super(x, y, window_width, window_height)
    refresh
    init_active
  end

  def make_decorations
    deco = add_decoration(:title)
    deco.set_text(title_text, 0)
  end

  def init_active
    select(0)
    activate
  end

  def title_text
    ""
  end

  def window_width
    Screen.menu_content.width / 2
  end

  def window_height
    Screen.menu_content.height - Metric.ui_element_sml
  end

  def standard_padding
    0
  end

end

class Window::CharacterList < Shell::EditorWindow

  def title_text
    "Character List"
  end

  def item_max
    $game.actors.size - 1
  end

  def draw_item(index)
    rect = item_rect(index)
    unit = $game.actors[index + 1]
    artist do |art|
      rect.contract!(anchor: 46, amount: Metric.spacing)
      art.draw_text(rect, unit.id, 0)
      rect.contract!(anchor: 6, amount: 24)
      art.draw_unit_name(unit, rect)
    end
  end

  def current_unit
    index >= 0 ? $game.actors[index + 1] : nil
  end

end

class Window::CharacterEditor < Shell::EditorWindow

  include REI::Mixin::UnitHost

  def init_active
    activate
  end

  def title_text
    "Character Editor"
  end

  def on_unit_change
    refresh
  end

  def refresh
    art     = artist
    bmp     = art.bitmap
    drawext = art.drawext
    merio   = art.merio
    bmp.clear
    ###
    return unless unit
    merio.font_config(:light, :h1, :enb)
    merio.draw_light_rect([0, 0, 96, 96])
    art.draw_character_face(character, 0, 0, true)
    art.draw_unit_name(unit, [96, 0, 128, bmp.font.size + 4])
  end

end

class Window::PartyEditor < Shell::EditorWindow

  def title_text
    "Party Editor"
  end

end

class Window::InventoryEditor < Shell::EditorWindow

  def title_text
    "Inventory Editor"
  end

end

class Sprite::SpritesheetAnimation < Sprite

  def initialize(viewport)
    super(viewport)
    @tick = 0
    @index = 0
    update_bitmap
    update_src_rect
  end

  def update_index
  end

  def update_src_rect
  end

  def update
    update_index
    update_src_rect
    super
    @tick += 1
  end

end

class Sprite::AstroAnimation < Sprite::SpritesheetAnimation

  def update_bitmap
    self.bitmap = Cache.load_bitmap("spritesheet/", "astrosheet")
    self.ox = 12
  end

  def update_index
    @index = (@tick / 7) % 4
  end

  def update_src_rect
    self.src_rect.set(@index % 5 * 24, @index / 5 * 24, 24, 24)
  end

  def update
    #if self.oy == 0
    #  @y_eneergy = 8
    #end
    #self.oy = (self.oy + @y_eneergy).max(0)
    self.x += 1
    self.x %= Screen.content.width
    super
    #@y_eneergy -= 0.98
  end

end

class Sprite::CoinAnimation < Sprite::SpritesheetAnimation

  def update_bitmap
    self.bitmap = Cache.load_bitmap("spritesheet/", "coinspin2sheet")
  end

  def update_index
    @index = (@tick / 7) % 4
  end

  def update_src_rect
    self.src_rect.set(@index * 16, 0, 16, 16)
  end

end

##
# class Scene::Game
#
class Scene::Game < Scene::MenuBase

  def start
    super
    @command_window.autoajar(:force_open)
  end

  def terminate
    super
  end

  def create_all_windows
    super
    create_command_window
  end

  def create_command_window
    @command_window = Window::GameCommand.new(0, Metric.ui_element_sml)
    @command_window.set_handler(:map,    method(:command_map))
    @command_window.set_handler(:menu  , method(:command_menu))
    @command_window.set_handler(:editor, method(:command_editor))
    @command_window.set_handler(:rei   , method(:command_rei))
    @command_window.set_handler(:title , method(:command_title))
    @command_window.set_handler(:cancel, method(:command_title))
    @command_window.help_window = @help_window
    window_manager.add(@command_window)
  end

  def create_character_list_window
    @character_list_window = Window::CharacterList.new(0, Metric.ui_element_sml)
    @character_list_window.set_handler(:ok,     method(:on_character_list_ok))
    @character_list_window.set_handler(:cancel, method(:on_character_list_cancel))
    @character_list_window.help_window = @help_window
    #@character_list_window.set_handler
    window_manager.add(@character_list_window)
  end

  def create_character_editor_window
    @character_editor_window = Window::CharacterEditor.new(0, Metric.ui_element_sml)
    @character_editor_window.set_handler(:ok,     method(:on_character_editor_ok))
    @character_editor_window.set_handler(:cancel, method(:on_character_editor_cancel))
    @character_editor_window.help_window = @help_window
    #@character_editor_window.set_handler
    window_manager.add(@character_editor_window)
  end

  def create_party_editor_window
    @party_editor_window = Window::PartyEditor.new(0, Metric.ui_element_sml)
    @party_editor_window.set_handler(:cancel, method(:on_party_editor_cancel))
    @party_editor_window.help_window = @help_window
    #@party_editor_window.set_handler
    window_manager.add(@party_editor_window)
  end

  def create_inventory_editor_window
    @inventory_editor_window = Window::InventoryEditor.new(0, Metric.ui_element_sml)
    @inventory_editor_window.set_handler(:cancel, method(:on_inventory_editor_cancel))
    @inventory_editor_window.help_window = @help_window
    #@inventory_editor_window.set_handler
    window_manager.add(@inventory_editor_window)
  end

  def create_confirm_window
    @confirm_window = Window::Confirm.new(0, 0)
    @confirm_window.help_window = @help_window
    window_manager.add(@confirm_window)
  end

  def create_editor_window
    @editor_window = Window::DebugEditor.new(@command_window.x, @command_window.y)
    @editor_window.set_handler(:cancel,    method(:on_editor_cancel))
    @editor_window.set_handler(:character, method(:on_editor_character))
    @editor_window.set_handler(:party,     method(:on_editor_party))
    @editor_window.set_handler(:inventory, method(:on_editor_inventory))
    @editor_window.help_window = @help_window
    window_manager.add(@editor_window)
  end

  def command_map
    $game.map.setup(2)
    $game.player.make_encounter_count
    $game.player.moveto(6, 6)
    scene_manager.goto(Scene::Map)
  end

  def command_menu
    scene_manager.call(Scene::Menu)
  end

  def command_editor
    create_editor_window
    @editor_window.reanchor! 7 => 9
    x = @editor_window.width
    @command_window.automove(x, 0)
    @editor_window.automove(x, 0)
  end

  def command_title
    create_confirm_window
    @confirm_window.set_title("Return to Title?")
    @confirm_window.set_handler(:ok)     { scene_manager.goto(Scene::Title) }
    @confirm_window.set_handler(:cancel) do
      @command_window.activate
      window_manager.delay_dispose(@confirm_window)
      @confirm_window = nil
    end
    @confirm_window.move(@command_window.x, @command_window.y2)
    @confirm_window.move(0, Metric.ui_element_sml)
  end

  def command_rei
    RogueManager.setup_map(-1).start_rogue
    #scene_manager.goto(Scene::Rogue)
  end

  def on_editor_cancel
    @command_window.activate
    x = -@editor_window.width
    @command_window.automove(x, 0)
    @editor_window.automove(x, 0)
    w = @editor_window
    window_manager.delay_dispose(@editor_window)
    @editor_window = nil
  end

  def on_editor_sub(sub)
    sub.reanchor! 7 => 9
    w = sub.width
    sub.automove(w, 0)
    @command_window.automove(w, 0)
    @editor_window.automove(w, 0)
  end

  def on_editor_character
    create_character_list_window
    on_editor_sub(@character_list_window)
  end

  def on_editor_party
    create_party_editor_window
    on_editor_sub(@party_editor_window)
  end

  def on_editor_inventory
    create_inventory_editor_window
    on_editor_sub(@inventory_editor_window)
  end

  def on_sub_editor_cancel(sub)
    @editor_window.activate
    w = sub.width
    sub.automove(-w, 0)
    @command_window.automove(-w, 0)
    @editor_window.automove(-w, 0)
    window_manager.delay_dispose(sub)
  end

  def on_character_list_ok
    create_character_editor_window
    @character_editor_window.set_unit(@character_list_window.current_unit)
    @character_list_window.close
  end

  def on_character_list_cancel
    on_sub_editor_cancel(@character_list_window)
    @character_list_window = nil
  end

  def on_character_editor_ok
    @character_editor_window.activate
  end

  def on_character_editor_cancel
    @character_list_window.open.activate
    window_manager.delay_dispose(@character_editor_window)
    @character_editor_window = nil
  end

  def on_party_editor_cancel
    on_sub_editor_cancel(@party_editor_window)
    @party_editor_window = nil
  end

  def on_inventory_editor_cancel
    on_sub_editor_cancel(@inventory_editor_window)
    @inventory_editor_window = nil
  end

  def update
    #@coin_animation.update
    super
  end

  def title_text
    "Game"
  end

end