# Scene::Item
# // 03/08/2012
# // 03/08/2012
class Scene::Item < Scene::ItemBase

  def start
    super
    create_help_window
    create_category_window
    create_item_window
  end

  def create_category_window
    @category_window = Window::ItemCategory.new
    @category_window.viewport = @viewport
    @category_window.help_window = @help_window
    @category_window.y = @help_window.height
    @category_window.set_handler(:ok,     method(:on_category_ok))
    @category_window.set_handler(:cancel, method(:return_scene))

    window_manager.add(@category_window)
  end

  def create_item_window
    wy = @category_window.y + @category_window.height
    wh = Graphics.height - wy
    @item_window = Window::ItemList.new(0, wy, Graphics.width, wh)
    @item_window.viewport = @viewport
    @item_window.help_window = @help_window
    @item_window.set_handler(:ok,     method(:on_item_ok))
    @item_window.set_handler(:cancel, method(:on_item_cancel))
    @category_window.item_window = @item_window

    window_manager.add(@item_window)
  end

  def on_category_ok
    @item_window.activate
    @item_window.select_last
  end

  def on_item_ok
    $game.party.last_item.object = item
    determine_item
  end

  def on_item_cancel
    @item_window.unselect
    @category_window.activate
  end

  def play_se_for_item
    Sound.play_use_item
  end

  def use_item
    super
    @item_window.redraw_current_item
  end

end
