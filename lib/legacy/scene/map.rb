# Scene::Map
# // 02/14/2012
# // 02/14/2012
class Scene::Map < Scene::Base
=begin
  def initialize()
    $game.party.add_actor(1)
    $game.party.add_actor(2)
    $game.party.add_actor(3)
    $game.map.setup(2)
    $game.system.in_rogue = false
    $game.player.make_encounter_count
    $game.player.moveto(6,6)#11,11)
    super()
  end # // Remove this later
=end
  attr_reader :pop_spriteset

  def start()
    super()
    scene_manager.clear
    $game.player.straighten
    _map.refresh
    $game.message.visible = false
    create_spriteset()
    @pop_spriteset = Spriteset::Pop.new()
    create_all_windows()
    @menu_calling = false
    #@minimap_spriteset = Spriteset_Minimap.new()
    #@minimap_spriteset.visible = true
    #@minimap_spriteset.viewport1.z = 9999
    #@minimap_spriteset.set_rect(*Graphics.rect.to_a)
  end

  def _map
    $game.map
  end

  def create_spriteset
    @spriteset = Spriteset::Map.new()
  end

  def create_all_windows
    create_message_window()
  end

  def create_message_window
    @message_window = Window::Message.new
    window_manager.add( @message_window )
  end

  def perform_transition
    if Graphics.brightness == 0
      Graphics.transition(0)
      fadein(fadein_speed)
    else
      super
    end
  end

  #--------------------------------------------------------------------------#
  # ● super-method :terminate
  #--------------------------------------------------------------------------#
  def terminate()
    super()
    @pop_spriteset.dispose() if @pop_spriteset
    scene_manager.snapshot_for_background()
    dispose_spriteset()
  end
  def dispose_spriteset()
    @spriteset.dispose()
  end
  def update_basic()
    super()
    #@minimap_spriteset.update()
    @pop_spriteset.update() if @pop_spriteset
    @spriteset.update
  end
  def update_for_wait()
    super()
    _map.update(false)
  end
  def update()
    super()
    _map.update(true)
    $game.player.update
    $game.timer.update
    update_scene if scene_change_ok?
  end
  def scene_change_ok?
    !$game.message.busy? && !$game.message.visible
  end
  #--------------------------------------------------------------------------
  # ● シーン遷移に関連する更新
  #--------------------------------------------------------------------------
  def update_scene
    SceneTool.check_gameover
    update_transfer_player unless scene_changing?
    update_encounter unless scene_changing?
    update_call_menu unless scene_changing?
    update_call_debug unless scene_changing?
  end
  #--------------------------------------------------------------------------
  # ● フレーム更新（フェード用）
  #--------------------------------------------------------------------------
  def update_for_fade
    update_basic
    _map.update(false)
    @spriteset.update
  end
  #--------------------------------------------------------------------------
  # ● 汎用フェード処理
  #--------------------------------------------------------------------------
  def fade_loop(duration)
    duration.times do |i|
      yield 255 * (i + 1) / duration
      update_for_fade
    end
  end
  #--------------------------------------------------------------------------
  # ● 画面のフェードイン
  #--------------------------------------------------------------------------
  def fadein(duration)
    fade_loop(duration) {|v| Graphics.brightness = v }
  end
  #--------------------------------------------------------------------------
  # ● 画面のフェードアウト
  #--------------------------------------------------------------------------
  def fadeout(duration)
    fade_loop(duration) {|v| Graphics.brightness = 255 - v }
  end
  #--------------------------------------------------------------------------
  # ● 画面のフェードイン（白）
  #--------------------------------------------------------------------------
  def white_fadein(duration)
    fade_loop(duration) {|v| @viewport.color.set(255, 255, 255, 255 - v) }
  end
  #--------------------------------------------------------------------------
  # ● 画面のフェードアウト（白）
  #--------------------------------------------------------------------------
  def white_fadeout(duration)
    fade_loop(duration) {|v| @viewport.color.set(255, 255, 255, v) }
  end
   def update_transfer_player
    perform_transfer if $game.player.transfer?
  end
  #--------------------------------------------------------------------------
  # ● エンカウントの更新
  #--------------------------------------------------------------------------
  def update_encounter
    scene_manager.call(Scene::Battle) if $game.player.encounter
  end
  #--------------------------------------------------------------------------
  # ● キャンセルボタンによるメニュー呼び出し判定
  #--------------------------------------------------------------------------
  def update_call_menu
    if $game.system.menu_disabled || _map.interpreter.running?
      @menu_calling = false
    else
      @menu_calling ||= Input.trigger?(:B)
      call_menu if @menu_calling && !$game.player.moving?
    end
  end
  #--------------------------------------------------------------------------
  # ● メニュー画面の呼び出し
  #--------------------------------------------------------------------------
  def call_menu
    Sound.play_ok
    scene_manager.call(Scene::Menu)
    Window::MenuCommand::init_command_position
  end
  #--------------------------------------------------------------------------
  # ● F9 キーによるデバッグ呼び出し判定
  #--------------------------------------------------------------------------
  def update_call_debug
    scene_manager.call(Scene::Debug) if $TEST && Input.press?(:F9)
  end
  #--------------------------------------------------------------------------
  # ● 場所移動の処理
  #--------------------------------------------------------------------------
  def perform_transfer
    pre_transfer
    $game.player.perform_transfer
    post_transfer
  end
  #--------------------------------------------------------------------------
  # ● 場所移動前の処理
  #--------------------------------------------------------------------------
  def pre_transfer
    @map_name_window.close if @map_name_window
    case $game.temp.fade_type
    when 0
      fadeout(fadeout_speed)
    when 1
      white_fadeout(fadeout_speed)
    end
  end
  #--------------------------------------------------------------------------
  # ● 場所移動後の処理
  #--------------------------------------------------------------------------
  def post_transfer
    case $game.temp.fade_type
    when 0
      Graphics.wait(fadein_speed / 2)
      fadein(fadein_speed)
    when 1
      Graphics.wait(fadein_speed / 2)
      white_fadein(fadein_speed)
    end
    @map_name_window.open if @map_name_window
  end
  #--------------------------------------------------------------------------
  # ● バトル画面遷移の前処理
  #--------------------------------------------------------------------------
  def pre_battle_scene
    Graphics.update
    Graphics.freeze
    @spriteset.dispose_characters
    BattleManager.save_bgm_and_bgs
    BattleManager.play_battle_bgm
    Sound.play_battle_start
  end
  #--------------------------------------------------------------------------
  # ● タイトル画面遷移の前処理
  #--------------------------------------------------------------------------
  def pre_title_scene
    fadeout(fadeout_speed_to_title)
  end
  #--------------------------------------------------------------------------
  # ● 戦闘前トランジション実行
  #--------------------------------------------------------------------------
  def perform_battle_transition
    Graphics.transition(60, "Graphics/System/BattleStart", 100)
    Graphics.freeze
  end
  #--------------------------------------------------------------------------
  # ● フェードアウト速度の取得
  #--------------------------------------------------------------------------
  def fadeout_speed
    return 30
  end
  #--------------------------------------------------------------------------
  # ● フェードイン速度の取得
  #--------------------------------------------------------------------------
  def fadein_speed
    return 30
  end
  #--------------------------------------------------------------------------
  # ● タイトル画面遷移用フェードアウト速度の取得
  #--------------------------------------------------------------------------
  def fadeout_speed_to_title
    return 60
  end
end
