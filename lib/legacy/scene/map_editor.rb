# Scene::MapEditor
#==============================================================================#
# ♥ Scene::MapEditor
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/08/2011
# // • Data Modified : 12/08/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/08/2011 V1.0
#
#==============================================================================#
class Window::MapNumber < Window::Selectable

  @@tone = Tone.new( 0, 0, 0 )

  attr_accessor :numbers

  def initialize
    @numbers = [0, 2, 0, "x", 0, 2, 0]
    super(0, 0, window_width, window_height)
    update_placement
    refresh
    select(0)
    activate
  end

  def update_tone
    self.tone.set( @@tone )
  end

  #--------------------------------------------------------------------------
  # ● ウィンドウ幅の取得
  #--------------------------------------------------------------------------
  def window_width
    return 320
  end

  #--------------------------------------------------------------------------
  # ● ウィンドウ高さの取得
  #--------------------------------------------------------------------------
  def window_height
    return 56
  end

  #--------------------------------------------------------------------------
  # ● ウィンドウ位置の更新
  #--------------------------------------------------------------------------
  def update_placement
    self.x = (Graphics.width - width) / 2
    self.y = (Graphics.height - height) / 2
  end

  def col_max
    return 7
  end

  def item_max
    return 7
  end

  def spacing
    return 8
  end

  def draw_item( index )
    change_color( normal_color, true )
    draw_text( item_rect_for_text( index ), @numbers[index], 1 )
  end

  def update
    super
    if Input.trigger?( :UP )
      Sound.play_cursor
      @numbers[index] = (@numbers[index] + 1) % 10
      clear_item( index )
      draw_item( index )
    elsif Input.trigger?( :DOWN )
      Sound.play_cursor
      @numbers[index] = (@numbers[index] - 1) % 10
      clear_item( index )
      draw_item( index )
    end unless @numbers[index] == "x"
  end

end

class Window::MapCommand < Window::Command

  @@tone = Tone.new( 0, 0, 0 )

  def update_tone
    self.tone.set( @@tone )
  end

  def make_command_list
    add_command( "New Map",  :new_map )
    add_command( "Load Map", :load_map )
    add_command( "Exit Map", :exit_map )
  end

end

class Window::TileSelect < Window::Selectable
  @@tone = Tone.new( 0, 0, 0 )
  def initialize( x, y )
    @tile_max = 1
    super( x, y, window_width, window_height )
    @tile_viewport = Viewport.new( x + standard_padding, y + standard_padding,
      window_width-(standard_padding*2), 32 )
    @tile_viewport.z = 1200
    @tilemap = Tilemap.new( @tile_viewport )
    @tilemap.map_data = Table.new( 256, 1, 4 )
    @tile_index = 0
    @tile_group = [:a, :b, :c, :d, :e]
    @tile_branch = false
    @return_branch = []
    show_tiles( @tile_group[@tile_index] )
    refresh
    select(0)
    activate
  end
  def show_tiles( group, tile_id=0 )
    old_group = @group
    @group = group
    @tile_ids = Array.new( 256, 0 )
    case @group
    when :branch
      @tile_branch = !@tile_branch
      if @tile_branch
        @tile_ids = []
        for i in 0...48
          @tile_ids << (tile_id - 46) + i
        end
        @return_branch = [old_group, self.index]
        self.index = 0
      else
        show_tiles( @return_branch[0] )
        self.index = @return_branch[1]
        @return_branch.clear
      end
    when :a
      @tile_ids = []
      for i in 0...128
        @tile_ids << 2048 + ((i * 48) + 46)
      end
      @tile_ids += (1536...1664).to_a
    when :b
      @tile_ids = (0...(256+(256*0))).to_a
    when :c
      @tile_ids = ((256+(256*0))...(256+(256*1))).to_a
    when :d
      @tile_ids = ((256+(256*1))...(256+(256*2))).to_a
    when :e
      @tile_ids = ((256+(256*2))...(256+(256*3))).to_a
    end
    @tile_max = @tile_ids.size
    for x in 0...@tilemap.map_data.xsize
      @tilemap.map_data[x, 0, 0] = @tile_ids[x] || 0
    end
  end
  def tile_id
    return @tile_ids[index]
  end
  #--------------------------------------------------------------------------
  # ● ウィンドウ幅の取得
  #--------------------------------------------------------------------------
  def window_width
    return Graphics.width
  end
  #--------------------------------------------------------------------------
  # ● ウィンドウ高さの取得
  #--------------------------------------------------------------------------
  def window_height
    return 72
  end
  #--------------------------------------------------------------------------
  # ● 項目の幅を取得
  #--------------------------------------------------------------------------
  def item_width
    return 32
  end
  #--------------------------------------------------------------------------
  # ● 項目の高さを取得
  #--------------------------------------------------------------------------
  def item_height
    return 48
  end
  def col_max
    return 256
  end
  def item_max
    return @tile_max #256
  end
  def spacing
    return 0
  end
  def tileset=( new_tileset )
    @tileset = new_tileset
    load_tileset
  end
  def load_tileset
    @tileset.tileset_names.each_with_index do |name, i|
      @tilemap.bitmaps[i] = Cache.tileset(name)
    end
    @tilemap.flags = @tileset.flags
  end
  def update_tone
    self.tone.set( @@tone )
  end
  def update
    super
    @tilemap.update
    @tilemap.ox = (32 * (index-8))
  end
  def process_cursor_move
    return unless cursor_movable?
    last_index = @index
    last_tile_index = @tile_index
    if Input.repeat?( :Y )
      Sound.play_cursor
      @tile_index = (@tile_index + 1) % @tile_group.size
    elsif Input.repeat?( :X )
      Sound.play_cursor
      @tile_index = (@tile_index - 1) % @tile_group.size
    end unless @tile_branch
    if Input.trigger?( :Z )
      Sound.play_ok
      show_tiles( :branch, tile_id )
    end
    cursor_right(Input.trigger?(:R))     if Input.repeat?(:R)
    cursor_left (Input.trigger?(:L))     if Input.repeat?(:L)
    #cursor_pagedown   if !handle?(:pagedown) && Input.trigger?(:R)
    #cursor_pageup     if !handle?(:pageup)   && Input.trigger?(:L)
    Sound.play_cursor if @index != last_index
    show_tiles( @tile_group[@tile_index] ) if @tile_index != last_tile_index
    if @index != last_index || @tile_index != last_tile_index
      contents.font.size = 16
      clear_item(8); rect = item_rect_for_text(8) ; rect.y += 16 ; rect.width += 32 #item_rect(8).height-16
      draw_text( rect, tile_id )
    end
  end

  def update_cursor
    cursor_rect.set(item_rect(8))
  end

end

class Scene::MapEditor < Scene::Base
  #--------------------------------------------------------------------------#
  # ● super-method :start
  #--------------------------------------------------------------------------#
  def start
    super
    scene_manager.clear
    @viewport1 = Viewport.new( 0, 56, Graphics.width, Graphics.height-(112+16) )
    @viewport1.z = 201
    @background = Sprite.new( @viewport )
    @background.bitmap = Cache.picture( "temp" )
    @background.z = -20
    @background.opacity = 198
    @map_data = Table.new( 20, 20, 4 )
    @tilemap = Tilemap.new( @viewport1 )
    @tilemap.map_data = @map_data
    @tilemap.visible = true
    @tileset = $data_tilesets[0]
    @cursor = Sprite.new( @viewport1 )
    @cursor.blend_type = 1
    @cursor.z = 400
    @cursor.bitmap = Bitmap.new( 32, 32 )
    @cursor.bitmap.fill_rect( 0, 0, 32, 32, Color.new( 57, 100, 145 ) )
    @cursor.bitmap.clear_rect( 2, 2, 28, 28 ) #, Color.new( 145, 100, 57 ) )
    @cursor_pos = [0, 0, 0]
    @cursor_active = false
    load_tileset
    create_command_window
  end
  def load_tileset
    @tileset.tileset_names.each_with_index do |name, i|
      @tilemap.bitmaps[i] = Cache.tileset(name)
    end
    @tilemap.flags = @tileset.flags
  end
  #--------------------------------------------------------------------------#
  # ● new-method :create_command_window
  #--------------------------------------------------------------------------#
  def create_command_window
    @command_window = Window::MapCommand.new( 20, 20 )
    @command_window.viewport = @viewport
    @command_window.set_handler( :new_map , method(:command_new_map) )
    @command_window.set_handler( :load_map, method(:command_load_map) )
    @command_window.set_handler( :exit_map, method(:command_exit_map) )
    window_manager.add( @command_window )
  end
  def create_mapsize_input_window
    @mapsize_input_window = Window::MapNumber.new
    @mapsize_input_window.viewport = @viewport
    window_manager.add( @mapsize_input_window )
  end
  def create_tile_window
    @tile_window = Window::TileSelect.new( 0, Graphics.height-72 )
    @tile_window.viewport = @viewport
    @tile_window.tileset = @tileset
    window_manager.add( @tile_window )
  end
  #--------------------------------------------------------------------------#
  # ● super-method :terminate
  #--------------------------------------------------------------------------#
  def terminate
    super
  end
  #--------------------------------------------------------------------------#
  # ● super-method :update
  #--------------------------------------------------------------------------#
  def update
    super
    if @cursor_active
      if Input.repeat?( :RIGHT )
        @cursor_pos[0] = (@cursor_pos[0] + 1) % @map_data.xsize
      elsif Input.repeat?( :LEFT )
        @cursor_pos[0] = (@cursor_pos[0] - 1) % @map_data.xsize
      elsif Input.repeat?( :DOWN )
        @cursor_pos[1] = (@cursor_pos[1] + 1) % @map_data.ysize
      elsif Input.repeat?( :UP )
        @cursor_pos[1] = (@cursor_pos[1] - 1) % @map_data.ysize
      end
      if Input.repeat?( :C )
        tile_id = @tile_window.tile_id
        @map_data[@cursor_pos[0], @cursor_pos[1], @cursor_pos[2]] = tile_id
      end
    end
    if @mapsize_input_window
      if Input.trigger?( :C )
        val = @mapsize_input_window.numbers
        val = [(val[0].to_s + val[1].to_s + val[2].to_s).to_i,
              (val[4].to_s + val[5].to_s + val[6].to_s).to_i]
        resize_map( val[0], val[1] )
        remove_window( @mapsize_input_window )
        @mapsize_input_window = nil
        @cursor_active = true
        create_tile_window
      end
    end
    @tilemap.update
    scrnsqWidth  = (@tilemap.viewport.rect.width / 32)
    scrnsqHeight = (@tilemap.viewport.rect.height / 32)
    halfwidth  = scrnsqWidth / 2
    halfheight = scrnsqHeight / 2
    cx = [[(@cursor_pos[0] - halfwidth),0].max,
      ((@map_data.xsize-1) - [@map_data.xsize-1, scrnsqWidth-1].min)].min * 32
    cy = [[(@cursor_pos[1] - halfheight),0].max,
      ((@map_data.ysize-1) - [@map_data.ysize-1, scrnsqHeight-1].min)].min * 32
    @tilemap.ox = cx
    @tilemap.oy = cy
    @cursor.x = (@cursor_pos[0] * 32) - cx
    @cursor.y = (@cursor_pos[1] * 32) - cy
  end
  def resize_map( xsize, ysize )
    @map_data.resize( xsize, ysize, 4 )
    @tilemap.map_data = @map_data
    for x in 0...@map_data.xsize
      for y in 0...@map_data.ysize
        @map_data[x, y, 0] = 1536
        @map_data[x, y, 1] = 0
        @map_data[x, y, 2] = 0
      end
    end
  end
  #--------------------------------------------------------------------------#
  # ● new-method :command_new_map
  #--------------------------------------------------------------------------#
  def command_new_map
    remove_window( @command_window )
    @command_window = nil
    @map_data = Table.new( 1, 1, 4 )
    create_mapsize_input_window
    return true
  end
  #--------------------------------------------------------------------------#
  # ● new-method :command_load_map
  #--------------------------------------------------------------------------#
  def command_load_map
    return true
  end
  #--------------------------------------------------------------------------#
  # ● new-method :command_exit_map
  #--------------------------------------------------------------------------#
  def command_exit_map
    return true
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
