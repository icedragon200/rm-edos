#
# EDOS/src/scene/menu.rb
#   by IceDragon
#   dm 02/07/2013
#   dc 02/07/2013
# vr 2.0.0
class Scene::Menu < Scene::MenuUnitBase

  def start
    super
    #@windows.reverse!
  end

  def create_all_windows
    super
    create_time_window
    create_command_window
    create_status_window
    create_gold_window
  end

  def title_text
    "Menu"
  end

  def create_command_window
    @command_window = Window::MenuCommand.new(0, 0)
    #@command_window.
    @command_window.set_handler(:item,        method(:command_item))

    @command_window.set_handler(:skill,       method(:command_personal))
    @command_window.set_handler(:equip,       method(:command_personal))
    @command_window.set_handler(:status,      method(:command_personal))
    @command_window.set_handler(:arts,        method(:command_personal))
    @command_window.set_handler(:equip_skill, method(:command_personal))

    @command_window.set_handler(:formation,   method(:command_formation))
    @command_window.set_handler(:save,        method(:command_save))
    @command_window.set_handler(:game_end,    method(:command_game_end))

    @command_window.set_handler(:cancel,      method(:return_scene))

    @command_window.align_to!(anchor: MACL::Surface::ANCHOR_CENTER)
    window_manager.add(@command_window)
  end

  def create_time_window
    @time_window = Sprite::MerioPlaytime.new
    @time_window.align_to!(anchor: MACL::Surface::ANCHOR_TOP_RIGHT,
                           surface: @title_window)
    @time_window.z = 10
    window_manager.add(@time_window)
  end

  def create_gold_window
    @gold_window = Sprite::MerioGold.new
    @gold_window.align_to!(anchor: MACL::Surface::ANCHOR_BOTTOM_RIGHT,
                           surface: @title_window)
    @time_window.z = 10
    window_manager.add(@gold_window)
  end

  def create_status_window
    @status_window = Window::MenuParty.new(0, 0)
    @status_window.y2 = @help_window.y
    window_manager.add(@status_window)
  end

  def command_item
    scene_manager.call(Scene::Item)
  end

  def command_personal
    #@status_window.select_last
    @status_window.activate
    @status_window.set_handler(:ok,     method(:on_personal_ok))
    @status_window.set_handler(:cancel, method(:on_personal_cancel))
  end

  def command_formation
    #@status_window.select_last
    @status_window.activate
    @status_window.set_handler(:ok,     method(:on_formation_ok))
    @status_window.set_handler(:cancel, method(:on_formation_cancel))
  end

  def command_save
    scene_manager.call(Scene::Save)
  end

  def command_game_end
    scene_manager.call(Scene::End)
  end

  def on_personal_ok
    scene_manager.call  case @command_window.current_symbol
                        when :skill       then Scene::Skill
                        when :equip       then Scene::Equip
                        when :status      then Scene::Status
                        when :arts        then Scene::Arts
                        when :equip_skill then Scene::SkillEquip
                        end
  end

  def on_personal_cancel
    @status_window.unselect
    @command_window.activate
  end

  def on_formation_ok
    if @status_window.pending_index >= 0
      $game.party.swap_order(@status_window.index,
                             @status_window.pending_index)
      @status_window.pending_index = -1
      @status_window.redraw_item(@status_window.index)
    else
      @status_window.pending_index = @status_window.index
    end
    @status_window.activate
  end

  def on_formation_cancel
    if @status_window.pending_index >= 0
      @status_window.pending_index = -1
      @status_window.activate
    else
      @status_window.unselect
      @command_window.activate
    end
  end

end