#
# EDOS/src/scene/options.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 0.0.1
class Scene::Options < Scene::MenuBase

  ##
  # start
  def start
    super
  end

  ##
  # create_all_windows
  def create_all_windows
    super
    w = @canvas.width
    h = @canvas.height - @help_window.height - @title_window.height
    @option_state = {
      thing: false
    }
    @options_window = Shell::MerioPanel.new(0, 0, w, h)
    @options_window.add_widget(:thing, :switch, 'Button', method(:get_options_state), "OFF", "ON")
    @options_window.set_handler(:ok, method(:command_options_ok))
    @options_window.set_handler(:cancel, method(:return_scene))
    @options_window.refresh
    @options_window.activate
    @options_window.select(0)
    window_manager.add(@options_window)
  end

  def title_text
    "Options"
  end

  def get_options_state(symbol)
    p [symbol, @option_state[symbol]]
    @option_state[symbol]
  end

  def command_options_ok
    symbol = @options_window.command_symbol

    case symbol
    when :thing
      p symbol
      p @option_state[:thing] = !@option_state[:thing]
    end
    @options_window.activate
    @options_window.redraw_current_item
  end

end
