# Scene::Rogue (Commands)
# // 12/08/2011
# // 02/01/2012
class Scene::Rogue < Scene::Base
  def command_attack()
    hide_command_window()
    command_skill_use(@subject.attack_skill())
  end
  def command_nudge()
    hide_command_window()
    command_skill_use(@subject.nudge_skill())
  end
  def command_tame()
    hide_command_window()
    command_skill_use(@subject.tame_skill())
  end
  def command_skill_use(skill)
    show_command_window() if start_quick_sel_xy(skill)==RESULT_FALSE
  end
  def command_guard()
    hide_command_window()
    @subject.set_guard_action()
  end

  def command_skill()
    hide_command_window()
    start_skill_selection()
  end
  def command_item()
    hide_command_window()
    start_item_selection()
  end
  def command_status()
    hide_command_window()
    start_status_view()
  end
  def command_equip()
    hide_command_window()
    start_equip_setup()
  end
  def command_hotkeys()
    hide_command_window()
    start_hotkey_setup()
  end

  def command_list()
    hide_command_window()
    raise "Incomplete Section Called!"
    start_list_view()
  end

  def command_options()
    hide_command_window()
    start_options_view()
  end
  def command_save()
    hide_command_window()
    if DataManager.save_game( $game.system.savefile_index )
      pop_quick_text_c("Save Successful!")
    else
      pop_quick_text_c("Couldn't save the game!")
    end
    show_command_window()
  end
  def command_minimap
    hide_command_window()
    set_ss_and_uun(SubScene::RogueMinimap)
    show_command_window()
  end
  def command_skip
    hide_command_window()
    @subject.set_skip_action()
  end
  def command_to_ai()
    hide_command_window()
    @subject.set_control( :ai )
    @subject._ai.search_range = 0
    @subject.update_turn_phase
  end
  # //
  def rscommand_control()
    @rscommand_window.toggle_battler_control()
    @rscommand_window.activate()
  end
  def rscommand_status
    start_status_view()
    @status_window.set_handler(:cancel, method(:on_rs_status_cancel))
  end
end
