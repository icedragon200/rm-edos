# Scene::Rogue
#==============================================================================#
# ♥ Scene::Rogue
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/08/2011
# // • Data Modified : 01/01/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/08/2011 V1.0
#     ♣ 12/14/2011 V1.0
#         Changed Scene::Map to Scene::Rogue
#
#     ♣ 01/01/2012 V1.0
#         Made a few changes
#
#==============================================================================#
class Scene::Rogue < Scene::Base
  #--------------------------------------------------------------------------#
  # ● super-method :start
  #--------------------------------------------------------------------------#
  def start
    #Graphics.__reset__
    #Cache.clear
    super
    scene_manager.clear
    create_spriteset
    create_all_windows
    #@minimap_spriteset = Spriteset_Minimap.new
    @pop_spriteset = Spriteset_Pop.new
    @command_window.set_to_close
    @command_window.deactivate
    @command_window.hide
    PlayerInput.clear_handler
    PlayerInput.set_handler( :dir4, method( :move_character ) )
    RogueManager::HOT_KEYS.each { |k|
      PlayerInput.set_handler( k, proc { set_action_hotkey( k ) } )
    }
    RogueManager.clear_handler
    RogueManager.set_handler( :on_cursor_move   , method( :on_cursor_move ) )
    RogueManager.set_handler( :refresh_spriteset, method( :refresh_spriteset ) )
    RogueManager.set_handler( :pop_quick_text   , method( :pop_quick_text ) )
    RogueManager.set_handler( :pop_chest_items  , method( :pop_chest_items ) )
    @return_stack = []
    set_focus_character
    puts sprintf( "Current Dungeon Seed: %s", _map.seed )
    RPG::BGM.new("Underground",100,100).play
  end
  def post_start
    super
    show_map_name
    pop_quick_text_c(
      :header_text => "Today's Tip",
      :text  => RogueManager.random_tip,
      :width => Graphics.rect.contract(amount: 18, anchor: 46).width,
      :break => true,
      :wait  => 600
    )
    m = $game.party.members[0]
    @pop_spriteset.add_speechbub( m.screen_x, m.screen_y, "Alright" )
  end
  def show_map_name
    @ginfo_window.visible = false
    s = @spriteset
    s.tween_viewports(
      s.default_viewport_rect.squeeze(
        anchor: 28, amount: 64
      ).xto_h(*Rect::SYM_ARGS).merge( :time => 60, :easer => :sine_out )
    )
    update_for_wait while s.tweening?
    sprite   = Sprite.new(nil)
    sprite.z = @viewport.z + 1
    sprite.bitmap = Bitmap.new(Graphics.width, 32)
    sprite.bitmap.font.size = sprite.bitmap.height - 4
    sprite.bitmap.font.italic = true
    sprite.bitmap.draw_text(sprite.bitmap.rect, _map.display_name)
    sprite.opacity = 0
    tweener = MACL::Tween::Seqeuncer.new
    tweener.add_tween(  0, 198, :sine_in , MACL::Tween.frame_to_sec(60))
    tweener.add_tween(198, 255, :back_out, MACL::Tween.frame_to_sec(120))
    tweener.add_tween(255,   0, :sine_in , MACL::Tween.frame_to_sec(60))
    loop do
      update_for_wait
      tweener.update
      sprite.y2 = s.viewport_rect.y
      sprite.opacity = tweener.value
      break if tweener.done?
    end
    sprite.dispose_all
    sprite  = nil
    tweener = nil
    s.restore_viewport(60)
    update_for_wait while s.tweening?
    @ginfo_window.visible = true
  end
  def message?
    !$game.message.busy? && !$game.message.visible
  end
  #--------------------------------------------------------------------------#
  # ● new-method :_map
  #--------------------------------------------------------------------------#
  def _map
    return $game.rogue
  end
  #--------------------------------------------------------------------------#
  # ● new-method :set_action_hotkey
  #--------------------------------------------------------------------------#
  def set_action_hotkey( key )
    hotkey = @subject._hot_keys.get_hotkey( key )
    case hotkey.code
    when Game::HotKey::MENU_OPEN_CODE
      #Sound.play_option_change
      show_command_window
    when Game::HotKey::CURSOR_MODE_CODE
      Sound.play_option_change
      trigger_cursor_mode
    when Game::HotKey::MINIMAP_TOGGLE_CODE
      Sound.play_option_change
      @minimap_spriteset.visible = !@minimap_spriteset.visible if @minimap_spriteset
    when Game::HotKey::ATTACK_CODE
      if @subject.usable?(@subject.attack_skill)
        start_quick_sel_xy(@subject.attack_skill,:skill)
      else
        pop_quick_text_c("You cannot Attack")
      end
    when Game::HotKey::GUARD_CODE
      @subject.set_guard_action
    when Game::HotKey::SKIP_CODE
      @subject.set_skip_action
    when Game::HotKey::SKILL_CODE
      item = hotkey.object
      if @subject.usable?(item)
        start_quick_sel_xy( hotkey.object, :skill )
      else
        pop_quick_text_c("You cannot use #{item.name}")
      end
    when Game::HotKey::ITEM_CODE
      item = hotkey.object
      if @subject.usable?(item)
        start_quick_sel_xy( hotkey.object, :item )
      else
        pop_quick_text_c("You cannot use #{item.name}")
      end
    else
      pop_quick_text(
        :text => "No action set for #{hotkey.key.to_s.upcase}",
        :wait => 60
      ) { |win| win.salign!(5) }
    end
  end
  def start_quick_sel_xy( item, type=nil )
    type = ExDatabase.obj_symbol(item) unless type

    rect = Rect.new(0, 56,
      Graphics.rect.contract(amount: 32, anchor: 46).width, 40)
    create_help_window(rect)
    @help_window.salign!(MACL::Constants::ANCHOR[:horz1])
    @help_window.set_item(item)
    @help_window.clamp_to_space
    show_help_window
    r = set_subject_action_obj( item, type )
    hide_help_window
    dispose_help_window
    r
  end
  def set_subject_action_obj( item, type )
    case type
    when :skill
      @subject.set_skill_action( item )
    when :item
      @subject.set_item_action( item )
    end
    case start_xy_selection
    when RESULT_TRUE
      @subject.ready_action
      RESULT_TRUE
    when RESULT_FALSE
      @subject.suspend_action
      @subject.remove_current_action
      RESULT_FALSE
    end
  end
  def set_subject_skill( item )
    set_subject_action_obj(item,:skill)
  end
  def set_subject_item( item )
    set_subject_action_obj(item,:item)
  end
  def start_xy_selection
    action = @subject.current_action
    item   = action.item
    return RESULT_FALSE unless item
    r      = @subject.get_range(item).mk_range_a
    color  = @subject.range_color(item).to_flash
    rngs   = _map.adjust_range(r, @subject.x, @subject.y)
    _map.replace_ranges(rngs, color)
    _map.cursor.moveto(@subject.x, @subject.y)
    _map.activate_cursor
    _map.cursor.focus_on
    result = RESULT_FALSE # // 0-Yes 1-No
    Input.update
    loop do
      update_basic
      if Input.trigger?(:C)
        _map.deactivate_cursor
        if _map.range_xy_valid?(_map.cursor.x,_map.cursor.y)
          action.set_effect_pos(_map.cursor.x,_map.cursor.y)
          nranges = action.mk_effect_a
          _map.replace_ranges(nranges, color)
          show_wait(90,@status_window.x,@status_window.y-12,@status_window.width/2,12)
          pop_confirm_window(:text=>"Is this okay?") {|win|win.salign!(5)}
          if @confirm_result == RESULT_TRUE
            result = RESULT_TRUE
            break
          else
            action.set_effect_pos(nil,nil)
            _map.replace_ranges(rngs, color)
          end
        else
          pop_quick_text_c("Outside Allowed Range!")
        end
        _map.activate_cursor
      elsif Input.trigger?(:B)
        break
      end
    end
    Input.update
    _map.clear_active_ranges
    _map.deactivate_cursor
    result
  end
  #--------------------------------------------------------------------------#
  # ● new-method :refresh_spriteset
  #--------------------------------------------------------------------------#
  def trigger_cursor_mode( forced_off=false )
    if _map.cursor.active || forced_off
      _map.cursor.moveto( @subject.x, @subject.y )
      _map.release_character_table
      _map.deactivate_cursor
      _map.cursor.focus_off
      _map.clear_active_ranges
      @subject.focus_on
    else
      @subject.focus_off
      _map.cursor.moveto( @subject.x, @subject.y )
      _map.static_character_table
      _map.activate_cursor
      _map.cursor.focus_on
      on_cursor_move
      #rngs = [[0, 1], [0, -1],
      # [1, 0], [-1, 0]].map { |r| [r[0] + @subject.x, r[1] + @subject.y] }
      #_map.add_ranges( rngs, Color.new(0, 32, 168).to_flash )
    end
  end
  #--------------------------------------------------------------------------#
  # ● new-method :create_spriteset
  #--------------------------------------------------------------------------#
  def create_spriteset
    @spriteset = Spriteset_Rogue.new
  end
  #--------------------------------------------------------------------------#
  # ● new-method :refresh_spriteset
  #--------------------------------------------------------------------------#
  def refresh_spriteset
    @spriteset.refresh
    @minimap_spriteset.refresh if @minimap_spriteset
  end
  #--------------------------------------------------------------------------#
  # ● new-method :dispose_spriteset
  #--------------------------------------------------------------------------#
  def dispose_spriteset
    @spriteset.dispose
  end
  # << Put back window stuff here
  def on_cursor_move
  end
  def refresh_status
    @status_window.refresh
  end
  def characters
    $game.rogue.characters
  end
  def focus_character
    $game.rogue.turn_character
  end
  def set_focus_character
    @subject                  = focus_character
    @command_window.battler   = @subject
    #@item_window.battler      = @subject
    #@skill_window.battler     = @subject
    @status_window.battler    = @subject
  end
  def move_character( d )
    return unless @subject
    unless @subject.moving?
      @subject.set_direction( d )
      c = @subject.get_allies_infront.pop
      if c
        pop_confirm_window( :text => "Switch Places?" ) { |win| win.salign!(5) }
        return unless @confirm_result == 0
        c.turn_toward_character(@subject)
        wait(3)
        #x,y = @subject.screen_x, @subject.screen_y
        #@pop_spriteset.add_speechbub( x, y, "Hey lets switch places" )
        #wait_for_pop
        #x,y = c.screen_x, c.screen_y
        #@pop_spriteset.add_speechbub( x, y, "Sure" )
        #wait_for_pop
        @subject.set_through( true )
        @subject.move_forward
        c.set_through( true )
        c.move_forward
        wait_for_move( [c,@subject] )
        @subject.set_through( false )
        c.set_through( false )
        @subject.set_xy_trigger_action
      else
        @subject.set_direction( d )
        if @subject.passable_infront?( d )
          @subject.set_move_action
        end
      end
    end
  end
  #--------------------------------------------------------------------------#
  # ● super-method :terminate
  #--------------------------------------------------------------------------#
  def terminate
    super
    dispose_spriteset
    @minimap_spriteset.dispose if @minimap_spriteset
    @pop_spriteset.dispose if @pop_spriteset
  end
  #--------------------------------------------------------------------------#
  # ● super-method :update_basic
  #--------------------------------------------------------------------------#
  def update_basic
    $game.rogue.update(true)
    @spriteset.update
    @minimap_spriteset.update if @minimap_spriteset
    @pop_spriteset.update if @pop_spriteset
    r = @spriteset.viewport_rect
    @command_window.x, @command_window.y = r.x, r.y
    if(@subject)
      @command_window.x += @subject.screen_x - 16
      @command_window.y += @subject.screen_y - 32
    end
    super
  end
  #--------------------------------------------------------------------------#
  # ● super-method :update
  #--------------------------------------------------------------------------#
  def update
    super
    case $game.rogue.phase
    when :init
      $game.rogue.on_phase_init
    when :turn_start
      set_focus_character
      #puts MapManager.text_based_minimap(
      #  characters,
      #  $game.rogue.data,
      #  $game.rogue.tileset.flags
      #)
      $game.rogue.start_turn
    when :turn
    when :turn_end
    end
    case @subject.turn_phase
    when :start
      @subject.set_turn_phase( :make_action )
    when :make_action
      if @subject.made_action
        #wait( 20 )
        @subject.set_turn_phase( :action )
      end
    when :action
      #msgbox "Processing Action"
      process_action
      #wait( 20 )
      @subject.set_turn_phase( :finalize )
    when :finalize
      @subject.set_turn_phase( :end )
    when :end
      $game.rogue.turn_process ; set_focus_character
    end if @subject
    @status_window.battler, = _map.characters_xy(_map.cursor.x, _map.cursor.y)
    if @rscommand_window.active

    elsif $game.rogue.cursor.active
      @subject.turn_toward_character( $game.rogue.cursor )
      if Input.trigger?(:X)
        $game.rogue.clear_active_ranges
        $game.rogue.add_ranges(
         @subject.find_path( :tx => $game.rogue.cursor.x, :ty => $game.rogue.cursor.y ) )
      elsif Input.trigger?(:Y) || Mouse.middle_click?
        @subject.moveto($game.rogue.cursor.x, $game.rogue.cursor.y)
      elsif Input.trigger?(:C)
        @rscommand_window.activate
        $game.rogue.deactivate_cursor
      elsif Mouse.left_click?
        _map.cursor.moveto( *_map.mouse_x32_pos ) if defined? MouseCore
      elsif Input.mtrigger?(:B)
        trigger_cursor_mode
      end
    elsif @command_window.active
    elsif @skill_window
    elsif @item_window
    elsif @equip_window
    elsif @status_window.active
    elsif @options_window
    elsif @hotkey_window
    elsif @skill_command
    elsif @item_command
    else
      if @subject.waiting_for_input?
        PlayerInput.update
        if Mouse.left_click? && Mouse.in_area?(@subject.sprect)
          show_command_window
        elsif Mouse.right_click?
          Sound.play_option_change
          trigger_cursor_mode
        end if defined? MouseCore
      end if @subject
    end
  end
  def abs_wait(duration)
    duration.times {|i| update_for_wait }
  end
  def wait_for_pop
    update_for_wait while @pop_spriteset.busy?
  end
  def wait_for_move( movers=[@subject] )
    update_for_wait
    update_for_wait while movers.any? { |c| c.moving? || c.jumping? }
  end
  def wait_for_message
    @message_window.update
    update_for_wait while $game.message.visible
  end
  #--------------------------------------------------------------------------
  # ● アニメーション表示が終わるまでウェイト
  #--------------------------------------------------------------------------
  def wait_for_animation
    update_for_wait
    update_for_wait while @spriteset.animation?
  end
  def wait_for_sanimation
    update_for_wait
    update_for_wait while @spriteset.sanimation?
  end
  #--------------------------------------------------------------------------
  # ● エフェクト実行が終わるまでウェイト
  #--------------------------------------------------------------------------
  def wait_for_effect
    update_for_wait
    update_for_wait while @spriteset.effect?
  end
  #--------------------------------------------------------------------------
  # ● 短時間ウェイト（早送り無効）
  #--------------------------------------------------------------------------
  def abs_wait_short
    abs_wait(15)
  end
  def process_forced_action
    if BattleManager.action_forced?
      last_subject = @subject
      @subject = BattleManager.action_forced_battler
      BattleManager.clear_action_force
      process_action
      @subject = last_subject
    end
  end
  #--------------------------------------------------------------------------
  # ● 戦闘行動の処理
  #--------------------------------------------------------------------------
  def process_action
    return if scene_changing?
    #if !@subject || !@subject.current_action
    #  @subject = BattleManager.next_subject
    #end
    return turn_end unless @subject
    if @subject.current_action
      @subject.current_action.prepare
      if @subject.current_action.valid?
        #@status_window.open
        execute_action
      end
      @subject.remove_current_action
    end
    process_action_end #unless @subject.current_action
    wait_for_effect
    characters.each { |c| c.remove_this = true if c.dead? && c.enemy? }
    _map.need_refresh = true
  end
  #--------------------------------------------------------------------------
  # ● 戦闘行動終了時の処理
  #--------------------------------------------------------------------------
  def process_action_end
    @subject.on_action_end
    #refresh_status
    @log_window.display_auto_affected_status(@subject)
    @log_window.wait_and_clear
    @log_window.display_current_state(@subject)
    @log_window.wait_and_clear
    #BattleManager.judge_win_loss
  end
  #--------------------------------------------------------------------------
  # ● 戦闘行動の実行
  #--------------------------------------------------------------------------
  def execute_action
    @subject.set_sprite_effect_type( :whiten )
    use_item
    @log_window.wait_and_clear
  end
  #--------------------------------------------------------------------------
  # ● スキル／アイテムの使用
  #--------------------------------------------------------------------------
  def use_item
    action = @subject.current_action
    item = @subject.current_action.item
    return if item.nil?
    @log_window.display_use_item(@subject, item)
    @subject.use_item(item)
    #refresh_status
    targets = @subject.current_action.make_targets.compact
    if @subject.current_action.parameter_code.code == 11
      case @subject.current_action.parameter_code.value1
      when 1
        @subject.move_forward if @subject.passable?( @subject.x, @subject.y, @subject.direction )
      when 2
        @subject.move_backward if @subject.passable?( @subject.x, @subject.y, @subject.revrese_dir( @subject.direction ) )
      end
      wait_for_move
    end
    @subject.turn_toward_character(IEI::Pos.new(*action.effect_pos))
    item.repeats.times { |i|
      @subject.on_item_repeat( item, i )
      targets.each {|target|
        target.center_on! if target.actor?
        show_animation([target], item.animation_id) if item.animation_id != 0
        invoke_item(target, item)
        @subject.on_target_attack( target, item )
        target.on_attack( @subject, item )
        if target.result.xy_trigger
          x, y = *target.result.xy_trigger_pos
          hsh     = $game.rogue.triggered_here( x, y )
          items   = hsh[:items]
          targets = hsh[:characters]
          traps   = hsh[:traps]
          _map.do_item_pickup( targets, items )
          invoke_trap_effects( traps, targets ) unless traps.empty?
          _map.check_event_trigger_here([0,1,2], x, y)
          wait(6)
          wait_for_message #if message?
        end
        if target.request_warp
          show_normal_animation([target], 11)
          wait_for_sanimation # // . x . Wait for animation but break if only 1 frame is left
          target.moveto( *target.request_warp )
          target.request_warp = nil
          target.center_on!
          show_normal_animation([target], 12)
          wait_for_animation
        end
      }
    }
    #targets.each {|target| item.repeats.times { invoke_item(target, item) } }
  end
  #--------------------------------------------------------------------------
  # ● スキル／アイテムの発動
  #--------------------------------------------------------------------------
  def invoke_item(target, item)
    if rand < target.item_cnt(@subject, item)
      invoke_counter_attack(target, item)
    elsif rand < target.item_mrf(@subject, item)
      invoke_magic_reflection(target, item)
    else
      apply_item_effects(apply_substitute(target, item), item)
    end
    @subject.last_target_index = target.index
  end
  #--------------------------------------------------------------------------
  # ● スキル／アイテムの効果を適用
  #--------------------------------------------------------------------------
  def apply_item_effects(target, item)
    target.item_apply(@subject, item)
    #refresh_status
    if target.result.missed
      target.on_miss( @subject, item )
      @subject.on_target_miss( target, item )
    elsif target.result.evaded
      target.on_evade( @subject, item )
      @subject.on_target_evade( target, item )
    else
      target.on_hit( @subject, item )
      @subject.on_target_hit( target, item )
    end
    if target.dead?
      target.on_death( @subject, item )
      @subject.on_target_death( target, item )
    end
    hnd = Pop.mk_hnd
    if target.result.missed
      hnd.pop.text = "Miss"
    elsif target.result.evaded
      hnd.pop.text = "Evaded"
    else
      unless target.result.hp_damage == 0 && item && !item.damage.to_hp?
        hnd.pop.text = target.result.hp_damage.to_s#_text
      end
      unless target.dead? || target.result.mp_damage == 0
        hnd.pop.text = target.result.mp_damage.to_s#_text
      end
      unless target.dead? || target.result.tp_damage == 0
        hnd.pop.text = target.result.tp_damage.to_s#_text
      end
    end
    hnd.x = target.x
    hnd.y = target.y
    #hnd.font.color = Palette['sys_orange1']
    hnd.font.bold = true
    hnd.font.size = 24
    @pop_spriteset.add_pop(hnd)
    @log_window.display_action_results(target, item)
  end
  def invoke_trap_effects( traps, targets )
    @original_subject = @subject
    traps.each { |t|
      @subject = t
      #puts "Triggering Trap: #{t} at: #{t.x}, #{t.y}"
      targets.each { |tg|
        tg.on_trap_step( t )
        @log_window.add_text(sprintf(Vocab::TrapSteppedOn, tg.name, t.name))
        @log_window.wait_and_clear
      }
      wait( 10 )
      if t.trigger_trap( targets )
        targets.each { |tg| tg.on_trap_trigger( t ) }
        update_for_wait while t.animating?
        @log_window.add_text(sprintf(Vocab::TrapTriggered, t.name))
        @log_window.wait_and_clear
        execute_action
      else
        targets.each { |tg| tg.on_trap_fail( t ) }
        @log_window.add_text(sprintf(Vocab::TrapFailed))
        @log_window.wait_and_clear
      end
      t.clear_actions
      t.on_finish_trigger( targets )
    }
    @subject = @original_subject
  end
  #--------------------------------------------------------------------------
  # ● 反撃の発動
  #--------------------------------------------------------------------------
  def invoke_counter_attack(target, item)
    @log_window.display_counter(target, item)
    attack_skill = $data_skills[target.attack_skill_id]
    @subject.item_apply(target, attack_skill)
    refresh_status
    @log_window.display_action_results(@subject, attack_skill)
  end
  #--------------------------------------------------------------------------
  # ● 魔法反射の発動
  #--------------------------------------------------------------------------
  def invoke_magic_reflection(target, item)
    @log_window.display_reflection(target, item)
    apply_item_effects(@subject, item)
  end
  #--------------------------------------------------------------------------
  # ● 身代わりの適用
  #--------------------------------------------------------------------------
  def apply_substitute(target, item)
    if check_substitute(target, item)
      substitute = target.get_substitue_character #friends_unit.substitute_battler
      if substitute && target != substitute
        @log_window.display_substitute(substitute, target)
        return substitute
      end
    end
    target
  end
  #--------------------------------------------------------------------------
  # ● 身代わり条件チェック
  #--------------------------------------------------------------------------
  def check_substitute(target, item)
    target.hp < target.mhp / 4 && (!item || !item.certain?)
  end
  #--------------------------------------------------------------------------
  # ● アニメーションの表示
  #     targets      : 対象者の配列
  #     animation_id : アニメーション ID（-1: 通常攻撃と同じ）
  #--------------------------------------------------------------------------
  def show_animation(targets, animation_id)
    if animation_id < 0
      show_attack_animation(targets)
    else
      show_normal_animation(targets, animation_id)
    end
    @log_window.wait
    wait_for_animation
  end
  #--------------------------------------------------------------------------
  # ● 攻撃アニメーションの表示
  #     targets : 対象者の配列
  #    アクターの場合は二刀流を考慮（左手武器は反転して表示）。
  #    敵キャラの場合は [敵の通常攻撃] 効果音を演奏して一瞬待つ。
  #--------------------------------------------------------------------------
  def show_attack_animation(targets)
    show_normal_animation(targets, @subject.atk_animation_id1, false)
    show_normal_animation(targets, @subject.atk_animation_id2, true)
  end
  #--------------------------------------------------------------------------
  # ● 通常アニメーションの表示
  #     targets      : 対象者の配列
  #     animation_id : アニメーション ID
  #     mirror       : 左右反転
  #--------------------------------------------------------------------------
  def show_normal_animation(targets, animation_id, mirror = false)
    animation = $data_animations[animation_id]
    if animation
      targets.each do |target|
        target.animation_id = animation_id
        target.animation_mirror = mirror
        abs_wait_short unless animation.to_screen?
      end
      abs_wait_short if animation.to_screen?
    end
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
