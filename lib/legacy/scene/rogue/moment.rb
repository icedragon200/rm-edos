# Scene::Rogue (Start/End)
# // 03/05/2012
# // 03/05/2012
class Scene::Rogue < Scene::Base
  # // Skill Handle
  def start_skill_selection()
    create_skill_window()
    create_help_window(@skill_window)
    @skill_window.battler     = @subject
    @skill_window.help_window = @help_window
    @skill_window.set_to_close() # // ? . x .
    show_help_window()
    show_skill_window()
  end
  def end_skill_selection()
    hide_skill_window()
    hide_help_window()
    dispose_skill_window()
    dispose_help_window()
  end
  # // Skill Command
  def start_skill_command()
    create_skill_command()
    @skill_command.set_and_refresh( @subject, @skill_window.item )
    show_skill_command()
  end
  def end_skill_command()
    hide_skill_command()
    dispose_skill_command()
  end
  # // Item Handle
  def start_item_selection()
    create_item_window()
    create_help_window(@item_window)
    @item_window.battler     = @subject
    @item_window.help_window = @help_window
    @item_window.set_to_close()
    show_help_window()
    show_item_window()
  end
  def end_item_selection()
    hide_item_window()
    hide_help_window()
    dispose_item_window()
    dispose_help_window()
  end
  # // Item Operation
  def start_item_command()
    create_item_command()
    @item_command.set_and_refresh( @subject, @item_window.item )
    show_item_command()
  end
  def end_item_command()
    hide_item_command()
    dispose_item_command()
  end
  # // Equip
  def start_equip_window()
    create_equip_window()
    yield @equip_window if(block_given?)
    show_equip_window()
  end
  def end_equip_window()
    hide_equip_window()
    yield @equip_window if(block_given?)
    dispose_equip_window()
  end
  # // Equip Setup
  def start_equip_setup()
    create_help_window(Graphics.rect,true)
    @help_window.y = 0
    create_equip_command()
    @equip_command.y = @help_window.y2 + 16
    show_equip_command()
    start_equip_window() do |window|
      window.width = Graphics.width / 2
      window.battler = @subject
      window.x = @equip_command.x
      window.y = @equip_command.y2+16
    end
    create_eitem_window()
    @equip_window.set_handler(:ok, method(:equip=up_ok))
    @equip_window.set_handler(:cancel, method(:equip=up_cancel))
    @equip_window.item_window = @eitem_window
    @equip_window.deactivate()
    show_eitem_window()
    @eitem_window.deactivate()
    @help_window.clear
    show_help_window()
    @equip_command.call_update_help()
  end
  def end_equip_setup()
    hide_eitem_window()
    end_equip_window()
    hide_equip_command()
    hide_help_window()
    dispose_eitem_window()
    dispose_equip_command()
    dispose_help_window()
  end
  # // Equip Set
  def start_equip_set()
    start_equip_window() do |window|
      window.battler = @subject
      window.salign!(MACL::Constants::ANCHOR[:horz1])
      window.y = @item_window.y2 + 16
    end
  end
  def end_equip_set()
    end_equip_window()
  end
  def start_eitem_selection()
    create_eitem_window()
    show_eitem_window()
  end
  # // Status Handle
  def switch_status()
    hide_status_window()
    yield @status_window if(block_given?)
    show_status_window()
  end
  def switch_status_to_full()
    switch_status() do |window|
      window.draw_mode = 2
      window.set_position( 0 )
    end
  end
  def switch_status_to_normal()
    switch_status() do |window|
      window.draw_mode = 0
      window.set_position( 2 )
    end
    @status_window.deactivate()
  end
  def start_status_view()
    switch_status_to_full()
  end
  def end_status_view()
    switch_status_to_normal()
  end
  # // Options View
  def start_options_view()
    create_options_window()
    show_options_window()
  end
  def end_options_view()
    @options_window.on_close()
    hide_options_window()
    dispose_options_window()
  end
  # // Hotkey
  def start_hotkey_window()
    create_hotkey_window()
    yield @hotkey_window if(block_given?)
    show_hotkey_window()
  end
  def end_hotkey_window()
    hide_hotkey_window()
    yield @hotkey_window if(block_given?)
    dispose_hotkey_window()
  end
  # // Hotkey Setup
  def start_hotkey_setup()
    start_hotkey_window() do |window|
      window.set_handler( :cancel, method(:hotkey=up_cancel) )
      window.set_handler( :ok, method(:hotkey=up_ok) )
      window.battler = @subject
      window.y = 16
      window.salign!(MACL::Constants::ANCHOR[:horz1])
    end
  end
  def end_hotkey_setup()
    end_hotkey_window()
  end
  # // Hotkey Set
  def start_hotkey_set()
    start_hotkey_window() do |window|
      window.set_handler( :cancel, method(:hotkey=_cancel) )
      window.set_handler( :ok, method(:hotkey=_ok) )
      window.battler = @subject
      window.salign!(MACL::Constants::ANCHOR[:horz1])
      window.y = (@skill_command || @item_command).y2
    end
  end
  def end_hotkey_set()
    end_hotkey_window()
  end
  def start_skill_hotkey_set()
    start_hotkey_set()
    @hotkey_window.set_handler(:cancel   ,method(:hotkey_skill=_cancel))
    @hotkey_window.set_handler(:return_ok,method(:on_hotkey_skill_return_ok))
  end
  def start_item_hotkey_set()
    start_hotkey_set()
    @hotkey_window.set_handler(:cancel   ,method(:hotkey_item=_cancel))
    @hotkey_window.set_handler(:return_ok,method(:on_hotkey_item_return_ok))
  end
  def start_hotkey_command()
    create_hotkey_command()
    show_hotkey_command()
  end
  def end_hotkey_command()
    hide_hotkey_command()
    dispose_hotkey_command()
  end
  def start_hotkey_selection()
    create_help_window(Graphics.rect.squeeze(
      anchor: 28, amount: 40).contract(amount: 16, anchor: 46))
    create_item_window()
    create_skill_window()
    @item_window.salign!(MACL::Constants::ANCHOR[:horz1])
    @item_window.y  = @hotkey_command.y2 + 56
    @item_window.battler = @subject
    @item_window.set_handler(:cancel   , method(:on_hotkey_selection_cancel))
    @item_window.set_handler(:ok       , method(:on_hotkey_selection_ok_i))
    @item_window.set_handler(:pageup   , method(:on_hotkey_selection_prev_i))
    @item_window.set_handler(:pagedown , method(:on_hotkey_selection_next_i))
    @item_window.set_to_close()
    @skill_window.salign!(MACL::Constants::ANCHOR[:horz1])
    @skill_window.y = @item_window.y
    @skill_window.battler = @subject
    @skill_window.set_handler(:cancel  , method(:on_hotkey_selection_cancel))
    @skill_window.set_handler(:ok      , method(:on_hotkey_selection_ok_s))
    @skill_window.set_handler(:pageup  , method(:on_hotkey_selection_prev_s))
    @skill_window.set_handler(:pagedown, method(:on_hotkey_selection_next_s))
    @skill_window.set_to_close()
    @skill_window.deactivate()
    @help_window.salign!(MACL::Constants::ANCHOR[:horz1])
    @help_window.y2= @item_window.y
    @help_window.battler = @subject
    show_help_window()
    show_item_window()
  end
  def end_hotkey_selection()
    hide_item_window() if @item_window.open?
    hide_skill_window() if @skill_window.open?
    hide_help_window()
    dispose_help_window()
    dispose_skill_window()
    dispose_item_window()
  end
end
