# Scene::Rogue (on_*)
# // 03/05/2012
# // 03/05/2012
class Scene::Rogue < Scene::Base
  def on_skill_ok()
    start_skill_command()
  end
  def on_skill_cancel()
    end_skill_selection()
    show_command_window()
  end

  def on_skill_command_use()
    item = @skill_window.item
    hide_skill_command()
    hide_skill_window()
    case set_subject_skill( item )
    when RESULT_TRUE
      end_skill_command()
      end_skill_selection()
    when RESULT_FALSE
      show_skill_window()
      @skill_window.deactivate()
      show_skill_command()
      @skill_command.activate()
    end
  end
  def on_skill_command_set()
    start_skill_hotkey_set()
  end
  def on_skill_command_cancel()
    end_skill_command()
    @skill_window.activate()
  end

  def on_item_ok()
    start_item_command()
  end
  def on_item_cancel()
    end_item_selection()
    show_command_window()
  end

  def on_item_command_cancel()
    end_item_command()
    @item_window.activate()
  end
  def on_item_command_use()
    item = @item_window.item
    hide_item_command()
    hide_item_window()
    case set_subject_item( item )
    when RESULT_TRUE
      end_item_command()
      end_item_selection()
    when RESULT_FALSE
      show_item_window()
      @item_window.deactivate()
      show_item_command()
      @item_command.activate()
    end
  end
  def on_item_command_throw()
  end
  def on_item_command_drop()
    ite = @item_window.current_item
    n = @subject.inventory.item_number( ite )
    @subject.inventory.lose_item( ite, n )
    _map.place_item( @subject.x, @subject.y, ite, n )
    _map.refresh_items()
    @item_window.refresh()
    @item_window.activate()
    end_item_command()
  end
  def on_item_command_equip()
    hide_item_command()
    start_equip_set()
  end
  def on_item_command_set()
    start_item_hotkey_set()
  end

  def on_equip_ok()
    if @subject.valid_slot?(@equip_window.index, @item_window.item)
      Sound.play_equip()
      @subject.change_equip(@equip_window.index, @item_window.item)
      @item_window.refresh()
      @equip_window.refresh()
      text = "#{@item_window.item.name} Equipped!"
      pop_quick_text(
        :x     => 0,
        :width => 156,
        :wait  => 45,
        :text  => text
      ) { |win| win.y = @equip_window.y2; win.salign!(MACL::Constants::ANCHOR[:horz1]) }
      @status_window.refresh()
      end_equip_set()
      hide_item_command()
      @item_window.activate()
    else
      Sound.play_buzzer
      text = "Invalid Slot"
      pop_quick_text(
        :x     => 0,
        :width => 156,
        :wait  => 45,
        :text  => text
      ) { |win| win.y = @equip_window.y2; win.salign!(MACL::Constants::ANCHOR[:horz1]) }
      @equip_window.activate
    end
  end
  def on_equip_cancel()
    end_equip_set()
    show_command_window()
  end

  def on_equip_setup_ok()
    case @equip_command.current_symbol
    when :equip
      @eitem_window.activate
    when :unequip
      @equip_window.unequip_current()
      @equip_window.activate()
    end
  end
  def on_equip_setup_cancel()
    @equip_command.activate()
  end
  def on_equip_command_equip()
    @equip_window.activate()
  end
  def on_equip_command_unequip()
    @equip_window.activate()
  end
  def on_equip_command_optimize()
    @equip_window.optimize_equipments()
    @equip_command.activate()
  end
  def on_equip_command_clear()
    @equip_window.clear_equipments()
    @equip_command.activate()
  end
  def on_equip_command_cancel()
    end_equip_setup()
    show_command_window()
  end
  def on_eitem_ok()
    @equip_window.equip_current(@eitem_window.item)
    @equip_window.activate()
  end
  def on_eitem_cancel()
    @equip_window.activate
  end

  def on_hotkey_setup_cancel()
    end_hotkey_setup()
    show_command_window()
  end
  def on_hotkey_setup_ok()
    start_hotkey_command()
  end
  def on_hotkey_set()
    locked = @hotkey_window.current_key_locked?
    win = @skill_window || @item_window
    wincom = @skill_command || @item_command
    unless(locked)
      item = win.current_item
      key = @subject._hot_keys.obj_set?( item )
      if key
        text = "Item was already set!"
        r = win.to_v4a()
        pop_quick_text(
          :x     => 0,
          :width => 156,
          :wait  => 45,
          :text  => text
        ) { |win| win.y = @hotkey_window.y2; win.salign!(MACL::Constants::ANCHOR[:horz1]) }
        key.set_obj( nil )
        @subject._hot_keys.set_hotkey(
          @hotkey_window.current_item.key,
          item
        )
        @hotkey_window.redraw_key( key )
        @hotkey_window.redraw_current_item()
        win.refresh()
      else
        @subject._hot_keys.set_hotkey(
          @hotkey_window.current_item.key,
          item
        )
        @hotkey_window.redraw_current_item()
        win.refresh()
        wait( 3 )
        Sound.play_equip
        text = "Item Set!"
        r = win.to_v4a()
        pop_quick_text(
          :x     => r[2],
          :width => 156,
          :wait  => 45,
          :text  => text
        ) { |win| win.y = @hotkey_window.y2; win.salign!(MACL::Constants::ANCHOR[:horz1]) }
      end
      #wait( 30 )
      #end_hotkey_set()
      @hotkey_window.call_handler(:return_ok)
    else
      text = "This key is locked!"
      r = win.to_v4a()
      pop_quick_text(
        :x     => r[2],
        :width => 156,
        :wait  => 45,
        :text  => text
      ) { |win| win.y = @hotkey_window.y2; win.salign!(MACL::Constants::ANCHOR[:horz1]) }
      @hotkey_window.activate
    end
  end
  def on_hotkey_set_ok()
    on_hotkey_set()
  end
  def on_hotkey_cancel()
    end_hotkey_set()
    show_command_window()
  end
  def on_hotkey_set_cancel()
    end_hotkey_set()
  end
  def on_hotkey_return_ok()
    end_hotkey_set()
  end
  def on_hotkey_skill_set_cancel()
    on_hotkey_set_cancel()
    @skill_command.activate()
  end
  def on_hotkey_skill_return_ok()
    on_hotkey_return_ok()
    end_skill_command()
    @skill_window.activate()
  end
  def on_hotkey_item_set_cancel()
    on_hotkey_set_cancel()
    @item_command.activate()
  end
  def on_hotkey_item_return_ok()
    on_hotkey_return_ok()
    end_item_command()
    @item_window.activate()
  end

  def on_hotkey_command_change
    start_hotkey_selection()
  end
  def on_hotkey_command_remove
    @hotkey_window.unequip_current()
    @hotkey_command.activate
  end
  def on_hotkey_command_cancel
    end_hotkey_command()
    @hotkey_window.activate
  end

  def on_hotkey_selection_cancel
    end_hotkey_selection()
    @hotkey_command.activate
  end
  def on_hotkey_selection_ok_s()
    @hotkey_window.equip_current(@skill_window.item)
    end_hotkey_selection()
    @hotkey_command.activate
  end
  def on_hotkey_selection_ok_i()
    @hotkey_window.equip_current(@item_window.item)
    end_hotkey_selection()
    @hotkey_command.activate
  end
  def on_hotkey_selection_prev_s
    hide_skill_window()
    show_item_window()
    @skill_window.deactivate()
    @item_window.activate()
  end
  def on_hotkey_selection_next_s
    on_hotkey_selection_prev_s() # // . x . Its pretty much the same thing
  end
  def on_hotkey_selection_prev_i
    hide_item_window()
    show_skill_window()
    @item_window.deactivate()
    @skill_window.activate()
  end
  def on_hotkey_selection_next_i
    on_hotkey_selection_prev_i() # // . x . Its pretty much the same thing
  end

  def on_status_cancel()
    end_status_view()
    show_command_window()
  end

  def on_options_cancel()
    end_options_view()
    show_command_window()
  end

  def on_rscommand_cancel()
    @rscommand_window.deactivate()
    _map.activate_cursor()
  end
  def on_rs_status_cancel()
    end_status_view()
    @status_window.set_handler(:cancel, method(:end_status_view))
    @rscommand_window.activate
  end
end
