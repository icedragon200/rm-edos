# Scene::Rogue (Show/Hide)
# // 03/05/2012
# // 03/05/2012
class Scene::Rogue < Scene::Base
  # // Command Window
  def show_command_window()
    open_and_activate_window( @command_window )
    s = @spriteset
    s.tween_viewports(
      s.viewport_rect.squeeze(
        anchor: 28, amount: 64
      ).xto_h(*Rect::SYM_ARGS).merge( :time => 30, :easer => :sine_out )
    )
  end
  def hide_command_window()
    close_and_deactivate_window( @command_window )
    @spriteset.restore_viewport( 30, :expo_out )
  end
  # // Help Window
  def show_help_window()
    @help_window.show.start_open
    wait_for_windows( @help_window )
  end
  def hide_help_window()
    @help_window.start_close
    wait_for_windows( @help_window )
    @help_window.hide
  end
  # // Show/Hide
  # // Refer to Scene::Base (Ex)
  mk_showhide_window :status_window
  mk_showhide_window :skill_window , :skill_command
  mk_showhide_window :item_window  , :item_command
  mk_showhide_window :equip_window , :equip_command, :eitem_window
  mk_showhide_window :hotkey_window, :hotkey_command
  mk_showhide_window :options_window
end
