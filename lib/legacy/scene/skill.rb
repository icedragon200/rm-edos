#
# EDOS/src/scene/Skill.rb
#   dm 19/05/2013
# vr 1.0.0
class Window::SkillCommand < Window::Command

  include REI::Mixin::UnitHost

  attr_reader :skill_window

  def initialize(x, y)
    super(x, y)
    @unit = nil
  end

  def window_width
    return 160
  end

  def visible_line_number
    return 4
  end

  def on_unit_change
    refresh
  end

  def make_command_list
    return unless @unit
    entity = @unit.entity
    entity.added_skill_types.sort.each do |stype_id|
      name = $data_system.skill_types[stype_id]
      add_command(name, :skill, true, stype_id)
    end
  end

  def update
    super
    @skill_window.stype_id = current_ext if @skill_window
  end

  def skill_window=(skill_window)
    @skill_window = skill_window
    update
  end

  def select_last
    entity = @unit.entity
    skill = entity.last_skill.object
    if skill
      select_ext(skill.stype_id)
    else
      select(0)
    end
  end

end

class Scene::Skill < Scene::ItemBase

  def start
    super
    create_help_window
    create_command_window
    create_status_window
    create_item_window
  end

  def create_help_window
    rect = Rect.new(0, 0, 0, 96) # // . x . To gain access to surface stuff
    rect = rect.expand!(anchor: 5, amount: 8)
    @help_window = Window::EquipHelp.new(
      @canvas.x, @canvas.y, @canvas.width, rect.height)
    @help_window.viewport = @viewport
    @help_window.set_unit(@unit)

    window_manager.add(@help_window)
  end

  def create_command_window
    wy = @help_window.y2
    @command_window = Window::SkillCommand.new(@help_window.x, wy)
    @command_window.viewport = @viewport
    @command_window.help_window = @help_window
    @command_window.set_unit(@unit)
    @command_window.set_handler(:skill,    method(:command_skill))
    @command_window.set_handler(:cancel,   method(:return_scene))
    @command_window.set_handler(:pagedown, method(:next_unit))
    @command_window.set_handler(:pageup,   method(:prev_unit))

    window_manager.add(@command_window)
  end

  def create_status_window
    y = @help_window.y2
    @status_window = Window::SkillStatus.new(@command_window.x2, y, @help_window.width-@command_window.width)
    @status_window.viewport = @viewport
    @status_window.set_unit(@unit)

    window_manager.add(@status_window)
  end

  def create_item_window
    wx = @canvas.x
    wy = @status_window.y2
    ww = @canvas.width
    wh = @canvas.y2 - wy
    @item_window = Window::SkillList.new(wx, wy, ww, wh)
    @item_window.set_mode(1) # // . x .
    @item_window.set_unit(@unit)
    @item_window.viewport = @viewport
    @item_window.help_window = @help_window
    @item_window.set_handler(:ok,     method(:on_item_ok))
    @item_window.set_handler(:cancel, method(:on_item_cancel))
    @command_window.skill_window = @item_window

    window_manager.add(@item_window)
  end

  def user
    @unit
  end
  #--------------------------------------------------------------------------
  # ● コマンド［スキル］
  #--------------------------------------------------------------------------
  def command_skill
    @item_window.activate
    @item_window.select_last
  end
  #--------------------------------------------------------------------------
  # ● アイテム［決定］
  #--------------------------------------------------------------------------
  def on_item_ok
    @unit.entity.last_skill.object = item
    determine_item
  end
  #--------------------------------------------------------------------------
  # ● アイテム［キャンセル］
  #--------------------------------------------------------------------------
  def on_item_cancel
    @item_window.unselect
    @command_window.activate
  end
  #--------------------------------------------------------------------------
  # ● アイテム使用時の SE 演奏
  #--------------------------------------------------------------------------
  def play_se_for_item
    Sound.play_use_skill
  end
  #--------------------------------------------------------------------------
  # ● アイテムの使用
  #--------------------------------------------------------------------------
  def use_item
    super
    @status_window.refresh
    @item_window.refresh
  end
  #--------------------------------------------------------------------------
  # ● アクターの切り替え
  #--------------------------------------------------------------------------
  def on_unit_change
    @command_window.set_unit(@unit)
    @status_window.set_unit(@unit)
    @item_window.set_unit(@unit)
    @command_window.activate
  end
end
