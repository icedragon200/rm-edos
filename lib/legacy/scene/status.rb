#
# EDOS/src/scene/status.rb
#   by IceDragon
#   dc 02/07/2013
#   dm 05/07/2013
# vr 2.0.0
class Scene::Status < Scene::MenuUnitBase

  ##
  # create_all_windows
  def create_all_windows
    super
    create_status_window
  end

  ##
  # create_status_window
  def create_status_window
    x = @canvas.x
    y = @canvas.y
    w = @canvas.width
    h = @canvas.height - @help_window.height - @title_window.height
    @status_window = Shell::MerioStatus.new(x, y, w, h)
    @status_window.set_unit(@unit)
    @status_window.set_handler(:cancel,   method(:return_scene))
    @status_window.set_handler(:pagedown, method(:next_unit))
    @status_window.set_handler(:pageup,   method(:prev_unit))
    window_manager.add(@status_window)
  end

  ##
  # on_unit_change
  def on_unit_change
    @status_window.set_unit(@unit)
    @status_window.activate
  end

end
