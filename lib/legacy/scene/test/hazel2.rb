#
# EDOS/src/scene/test/hazel2.rb
#   by IceDragon
class EventSystem

  def initialize
    @clients = []
    @events = []
  end

  def add_client(client)
    @clients << client
  end

  def add_event(event)
    @events << event
  end

  def update
    @events.each do |ev|
      @clients.each_with_object(ev, &:handle_event)
    end
    @events.clear
  end

end

class Scene::TestHazel2 < Scene::Base

  def start
    super
    @event_system = EventSystem.new
    @shell = Hazel::Shell::Base.allocate #.new(0, 0, 320, 240)
    @shell.extend(Hazel::Shell::Addons::Background)
    @shell.send(:initialize, 0, 0, 320, 240)
    button = @shell.add_widget(Hazel::Widget::Button)
    button.size.set!([32, 32, 1])
    button.refresh
    def button.tooltip
      "A Button"
    end
    @shell.refresh_widgets
    @shell.automove((Screen.content.width - @shell.width) / 2,
                    (Screen.content.height - @shell.height) / 2)
    @event_handler = Hazel::EventHandler.new
    @event_handler.set_handler([:mouse, :m_click]) do |event|
      mx, my = *event.params.first
      @shell.clear_automations(:moveto)
      x, y = mx - @shell.width / 2, my - @shell.height / 2
      @shell.automoveto(x, y)
    end
    @event_handler.set_handler([:mouse, :r_click]) do |event|
      return_scene
    end
    @event_system.add_client(@event_handler)
    @event_system.add_client(@shell)
    window_manager.add(@shell)
  end

  def terminate
    super
  end

  def update_basic
    super
    poll_events
    @event_system.update
  end

  def new_event(*args)
    @event_system.add_event(Hazel::Event.new(*args))
  end

  def poll_events
    ps = Mouse.pos
    new_event(:mouse, :l_click, ps) if Mouse.left_click?
    new_event(:mouse, :m_click, ps) if Mouse.middle_click?
    new_event(:mouse, :r_click, ps) if Mouse.right_click?
    new_event(:mouse, :move,    ps) if Mouse.moved?
  end

  def update
    super

  end

end