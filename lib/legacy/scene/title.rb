class Shell::About < Shell::Console

  def update
    super
    return unless active
    call_handler(:cancel) if(Input.trigger?(:B))
  end

end

# // 03/03/2012 ]
class Scene::Title < Scene::Base

  def start
    super
    scene_manager.clear
    create_background
    create_title_sprite
    create_all_windows
  end

  def create_all_windows
    create_command_window
    create_tail_sp
  end

  def create_command_window
    @command_window = Shell::MerioTitleCommand.new(0, 0) #Window::TitleCommand.new
    @command_window.set_handler(:new_game,    method(:command_new_game))
    @command_window.set_handler(:load_game,   method(:command_continue))
    @command_window.set_handler(:options,     method(:command_options))
    @command_window.set_handler(:shutdown,    method(:command_shutdown))
    @command_window.set_handler(:mapeditor,   method(:command_mapeditor))
    @command_window.set_handler(:bestiary,    method(:command_bestiary))
    @command_window.set_handler(:traptionary, method(:command_traptionary))
    @command_window.set_handler(:itemary,     method(:command_itemary))
    @command_window.set_handler(:skillary,    method(:command_skillary))
    @command_window.set_handler(:about,       method(:command_about)) # //02/19/2012

    @command_window.cx = @canvas.cx
    @command_window.y = @title_sprite.y2

    window_manager.add(@command_window)
  end

  def create_tail_sp
    @tail_sp = Sprite::MerioTextBox.new(nil, @canvas.width, Metric.ui_element_sml)
    @tail_sp.align_to!(anchor: 2, surface: @canvas)
    @tail_sp.merio_font_size = :small
    @tail_sp.set_text("Earthen vr #{Game::VERSION}", 2)
    @tail_sp.z = 1
  end

  def create_info_window
    w, h = @canvas.width * 0.95, @canvas.height * 0.9
    @info_window = Shell::About.new(0, 0, w, h)
    @info_window.align_to!(anchor: 5, surface: @canvas)

    rect = Rect.new(0,0,@info_window.width,@info_window.line_height)
    (["Earthen: Dawn Of Smiths vr #{Game::VERSION}",:line]).each_with_index do |s,i|
      if(s == :line)
        @info_window.artist.draw_horz_line(rect.y2)
        rect.y += 2
      else
        rect.y += rect.height
        @info_window.artist.draw_text(rect,s,1)
      end
    end

    @info_window.z = 999
    @info_window.start_open
    @info_window.set_handler(:cancel, method(:on_info_cancel))
    @info_window.activate

    window_manager.add(@info_window)
  end

  def create_title_sprite
    @title_sprite = MerioTitleGraphic.new(@viewport)
    @title_sprite.align_to!(anchor: 8, surface: @canvas)
  end

  def command_about
    create_info_window
    @command_window.hide.deactivate
  end

  def on_info_cancel
    dispose_info_window
    @command_window.show.activate
  end

  def terminate
    super
    @tail_sp.dispose
    @title_sprite.dispose
    dispose_background
  end

  def dispose_info_window
    @info_window.deactivate
    close_and_remove_window(@info_window)
  end

  def update_basic
    super
    update_background #if @background_sprite
  end

  def update
    super
    @title_sprite.update
  end

  def command_new_game
    scene_manager.goto(Scene::Game)
    scene_manager.call(Scene::PreNewGame)
    scene_manager.call(Scene::FileSelect)
  end

  def command_continue
    scene_manager.call(Scene::Load)
  end

  def command_shutdown
    scene_manager.goto(Scene::Shutdown)
  end

  def command_options
    scene_manager.call(Scene::Options)
  end

  def command_mapeditor
    scene_manager.call(Scene::MapEditor)
  end

  def command_bestiary
    scene_manager.call(Scene::Bestiary)
  end

  def command_traptionary
    scene_manager.call(Scene::Traptionary)
  end

  def command_itemary
    scene_manager.call(Scene::Itemary)
  end

  def command_skillary
    scene_manager.call(Scene::Skillary)
  end

end

