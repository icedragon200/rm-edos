# // 02/14/2012
# // 02/14/2012
module Shell::Addons::ContextMenu

  attr_reader :context_menu

  def init_shell_addons
    super
    init_context_menu
  end

  def update_shell_addons
    super
    update_context_menu
  end

  def dispose_shell_addons
    super
    dispose_context_menu
  end

  def init_context_menu
    @shell_register.register("Menu-Context", version: "1.0.0".freeze)

    @context_menu = context_menu_class.new(context_menu_x, context_menu_y)
    @context_menu.parent = self
    @context_menu.visible = false
    @context_menu.set_handler(:cancel, method(:cm_cancel))
    update_cm_pos
    set_cm_handlers
  end

  def set_cm_handlers
  end

  def cm_cancel
    activate
    @context_menu.deactivate
    @context_menu.hide
  end

  def update_cm_pos
    @context_menu.x = context_menu_x
    @context_menu.y = context_menu_y
    @context_menu.z = context_menu_z
  end

  def update_context_menu
    if Mouse.right_click? && mouse_in_window?
      deactivate
      @context_menu.activate
      @context_menu.show
      update_cm_pos
      @context_menu.x = Mouse.x
      @context_menu.y = Mouse.y
    end if self.active
    @context_menu.update
  end

  def dispose_context_menu
    @context_menu.dispose
  end

  def context_menu_x
    self.x
  end

  def context_menu_y
    self.y
  end

  def context_menu_z
    self.z + 100 + (self.viewport ? self.viewport.z : 0)
  end

  def context_menu_class
    Shell::ContextMenu
  end

end
