#
#
# vr 1.1.0
class Shell::Clock < Shell::Base

  include Shell::Addons::OwnViewport
  include Shell::Addons::Background
  include Shell::Addons::Contents

  def initialize(x, y, w=192, h=24)
    super(x, y, w, h)
    activate()
    update_time()
  end

  def glass_background_tone
    col = Palette['sys_green1']#Palette['sys_red1']
    col.to_tone
  end

  def standard_padding
    Metric.padding
  end

  def update
    super
    update_time() if self.active
  end

  def update_time
    t = Time.now
    timenow = [t.hour, t.min, t.sec]
    if @last_time != timenow
      @last_time = timenow
      refresh
    end
  end

  def refresh
    contents.clear

    a = @last_time
    #a[0] %= 12

    artist do |art|
      art.canvas.font.size = self.height

      rect = Rect.new(0, 0, contents.width, contents.font.size + 4)
      align = 1

      art.change_color(art.normal_color)
      art.draw_text(rect, format("%02d:%02d:%02d", *a), align)
    end
  end

end
