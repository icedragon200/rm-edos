# Shell::Console
# // 02/19/2012
# // 02/19/2012
class Shell::Console < Shell::Window

  include Shell::Addons::HandlerBase
  include Shell::Addons::Background

  def line_height
    contents.font.size + 2
  end

  def line_color
    contents.font.color
  end

  def create_contents()
    super()
    contents.font.set_style('console1')
  end

  def standard_padding
    Metric.padding
  end

end
