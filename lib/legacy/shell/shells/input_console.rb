# Shell::InputConsole
# // 03/01/2012
# // 03/01/2012
class Sprite::Caret < Sprite
end

class Shell::InputConsole < Shell::Console
  attr_reader :text
  def initialize(x,y)
    super(x,y,Graphics.width,24)
    @caret.bitmap = Bitmap.new(2,self.height-4)
    @caret.bitmap.fill_rect(@caret.bitmap.rect, Color::ConsoleText)
    @caret.oy = -2
    @index = 0
    @text  = []
    @_tmp  = nil
  end
  def init_internal()
    super()
    @caret = Sprite::Caret.new()
  end
  def _objs
    super + [@caret]
  end
  def dispose()
    @caret.dispose_all()
    super()
  end
  def update
    super()
    @caret.update()
    @caret.ox = -(contents_x+(@index*7))
    @caret.opacity = 57 + (198*Math.sin(Math::PI*(Graphics.frame_count%20)/20.0))
    update_input()
  end
  def update_input
    return unless(active)
    if Keyboard.trigger?(:ENTER)
      Sound.play_ok()
      @handler[:eval].call(@text.join("")) if handle?(:eval)
    elsif Keyboard.repeat?(:BACKSPACE)
      Sound.play_miss()
      @index = (@index - 1).max(0)
      @text.delete_at(@index)
      refresh()
    elsif Keyboard.repeat?(:LEFT)
      Sound.play_evasion()
      @index = (@index - 1).max(0)
    elsif Keyboard.repeat?(:RIGHT)
      Sound.play_evasion()
      @index = (@index+1).min(@text.size)
    elsif Keyboard.trigger?(:HOME)
      Sound.play_magic_evasion()
      @index = 0
    elsif Keyboard.trigger?(:END)
      Sound.play_magic_evasion()
      @index = @text.size
    else
      @_tmp = Keyboard.get_typing_s # // String[]
      unless(@_tmp.empty?())
        Sound.play_cursor()
        @text.insert(@index, *@_tmp)
        @index += @_tmp.size
        refresh()
      end
    end
  end
  def refresh()
    contents.clear()
    contents.draw_text(0,0,contents.width,contents.height,@text.join(""))
  end
  def clear()
    #(@last_texts ||= []) << @text.dup
    @text.clear()
    @index = 0
    refresh()
  end
end
