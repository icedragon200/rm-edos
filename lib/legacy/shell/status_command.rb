# Shell::RogueStatusCommand
# // 03/03/2012
# // 03/03/2012
class Shell::RogueStatusCommand < Shell::Command

  include Shell::Addons::Background

  class AddonBin_CRSC < WindowAddons::AddonBin[WindowAddons::Header]

    def header_text
      return "Commands"
    end

    def header_text_settings
      (hsh=super)["align"] = 1
      hsh
    end

    def header_cube()
      super.hset(self.x,nil,nil,self.width)
    end

  end

  def addon_bin
    AddonBin_CRSC
  end

  def initialize(status_window)
    super(0,0)
    @status_window = status_window
    self.viewport = @status_window.viewport
    deactivate()
  end

  def init_internal()
    super()
  end

  def standard_padding
    Metric.padding
  end

  def window_width
    adjust_w4window(148)
  end

  def refresh_command_list()
    clear_command_list()
    make_command_list()
    self.width  = window_width
    self.height = window_height
    refresh
    r = @gbackground_sprite.bitmap.rect
    refresh_glass_background if self.width != r.width || self.height != r.height
    quick_xyz_update
    select(0)
  end

  def make_command_list()
    add_command("Status",:status,!@battler.nil?)
    text = "Control " + (@battler.nil? ? 'N/A' : @battler.controlled_by.to_s.capitalize)
    add_command(text,:control,true) if(@battler && @battler.actor?)
  end
  def toggle_battler_control
    if @battler.controlled_by == :ai
      @battler.set_control(:player)
    elsif @battler.controlled_by == :player
      @battler.set_control(:ai)
    end
    @battler.update_turn_phase()
    refresh_command_list()
  end
  def quick_xyz_update()
    self.x2 = @status_window.x2
    self.y2 = @status_window.y
    self.z   = @status_window.z
  end
  def battler_changed?()
    return false if @battler == @status_window.battler
    @battler = @status_window.battler
    return true
  end
  def update()
    super()
    if battler_changed?()
      refresh_command_list()
      self.height = 0
    end
    quick_xyz_update()
    if self.active
      self.height = (self.height + window_height / 10.0).clamp(0,window_height)
    else
      self.height = (self.height - window_height / 10.0).clamp(0,window_height)
    end
  end
end
