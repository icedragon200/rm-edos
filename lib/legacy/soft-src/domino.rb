#
# EDOS/soft-src/domino.rb
#   dc 21/08/2013
#   dm 21/08/2013

class Domino

  include Comparable

  attr_reader :a, :b
  attr_reader :a_domino, :b_domino

  def initialize(a, b)
    @a, @b = a, b
    @a_domino, @b_domino = nil, nil
    reset
  end

  def flip
    @a, @b = @b, @a
  end

  def value
    @a + @b
  end

  def reset
    @a_domino = nil
    @b_domino = nil
  end

  def double?
    @a == @b
  end

  def double
    return @a if double?
    return nil
  end

  def get_side(side)
    case side
    when :a then a
    when :b then b
    end
  end

  def get_side_domino(side)
    case side
    when :a then a_domino
    when :b then b_domino
    end
  end

  def set_side_domino(side, domino)
    case side
    when :a then @a_domino = domino
    when :b then @b_domino = domino
    end
  end

  ##
  # can_place?(Domino domino, Symbol to_side, Symbol from_side)
  #   from_side refers to the provided Domino side, while to_side refers
  #   to the target domino side
  def can_place?(domino, to_side, from_side)
    get_side(to_side) == domino.get_side(from_side)
  end

  ##
  # place_on_side(Domino domino, Symbol to_side, Symbol from_side)
  def place_on_side(domino, to_side, from_side)
    set_side_domino(to_side, domino)
    domino.set_side_domino(from_side, self)
  end

  def find_side?(value)
    return :a if @a == value
    return :b if @b == value
    return nil
  end

  def to_s
    "[#{@a}|#{@b}]"
  end

  def to_a
    return @a, @b
  end

  def <=>(other)
    value <=> other.value
  end

end

class CPU

  attr_reader :hand
  attr_reader :id
  attr_accessor :name
  attr_accessor :log

  def initialize(id)
    @id = id
    @hand = []
    @name = "CPU#{id}"
  end

  def hand_draw(pile)
    @hand = Array.new(7) { pile.pop }
  end

  def can_play_on_first?(board)
    domino = board.first
    return @hand.find { |d| d.find_side?(domino.a)  }
  end

  def can_play_on_last?(board)
    domino = board.last
    return @hand.find { |d| d.find_side?(domino.b)  }
  end

  def can_play?(board)
    can_play_on_first?(board) || can_play_on_last?(board)
  end

  def try_play_first(board)
    domino_first  = board.first
    if domino = can_play_on_first?(board)
      side = domino.find_side?(domino_first.a)
      domino.flip if side == :a
      board.unshift(domino)
      @hand.delete(domino)
      if @log
        @log.puts "#{@name} PLAYED-ON-FIRST: #{domino.to_s}"
      end
      return true # played
    else
      return false
    end
  end

  def try_play_last(board)
    domino_last  = board.last
    if domino = can_play_on_last?(board)
      side = domino.find_side?(domino_last.b)
      domino.flip if side == :b
      board.push(domino)
      @hand.delete(domino)
      if @log
        @log.puts "#{@name} PLAYED-ON-LAST: #{domino.to_s}"
      end
      return true # played
    else
      return false # pass
    end
  end

  def act(board)
    if rand(2) == 0
      return true if try_play_first(board)
      return true if try_play_last(board)
    else
      return true if try_play_last(board)
      return true if try_play_first(board)
    end
    return false
  end

  def first_play(board)
    if domino = @hand.find { |d| d.double == 6 }
      board.push(domino)
      @hand.delete(domino)
      return true
    else
      return false
    end
  end

  def other_play(board)
    if domino = (@hand.find { |d| d.double } || @hand.sample)
      board.push(domino)
      @hand.delete(domino)
      return true
    else
      return false
    end
  end

end

@dominoes = [
  Domino.new(0, 0),
  Domino.new(1, 0),
  Domino.new(1, 1),
  Domino.new(1, 2),
  Domino.new(1, 3),
  Domino.new(1, 4),
  Domino.new(1, 5),
  Domino.new(1, 6),
  Domino.new(2, 0),
  Domino.new(2, 2),
  Domino.new(2, 3),
  Domino.new(2, 4),
  Domino.new(2, 5),
  Domino.new(2, 6),
  Domino.new(3, 0),
  Domino.new(3, 3),
  Domino.new(3, 4),
  Domino.new(3, 5),
  Domino.new(3, 6),
  Domino.new(4, 0),
  Domino.new(4, 4),
  Domino.new(4, 5),
  Domino.new(4, 6),
  Domino.new(5, 0),
  Domino.new(5, 5),
  Domino.new(5, 6),
  Domino.new(6, 0),
  Domino.new(6, 6),
]

@dominoes.each(&:reset)
@dominoes.sort!
p @dominoes.size, @dominoes.map(&:to_s)

@games = 0
@cpus = Array.new(4) { |i| CPU.new(i) }
@cpus[0].name = "Kana"
@cpus[1].name = "Catalina"
@cpus[2].name = "Scarlet"
@cpus[3].name = "LilStabby"
@cpus.shuffle!

@scores = Hash[@cpus.zip(Array.new(@cpus.size, 0))]

while @games < 10
  Dir.mkdir("log") unless Dir.exist?("log")
  @play_log = File.open("log/play#{@games}.log", "w+")
  @play_log.sync = true

  @turns = 0
  @pile = @dominoes.dup
  @pile.each(&:reset)
  @pile.shuffle!
  @board = Array.new

  @cpus.each do |c|
    c.log = @play_log
    c.hand_draw(@pile)
  end

  @cpus.each do |c|
    break if (@games == 0 ? c.first_play(@board) : c.other_play(@board))
  end

  @play_log.puts "GAME: #{@games}"
  @play_log.puts ""
  @cpus.cycle do |c|
    @play_log.puts "            TURN: #{@turns}"
    @play_log.puts ""
    sleep 0.01
    c.act(@board)
    @play_log.puts @board.map(&:to_s).join
    @play_log.puts ""
    if @cpus.all? { |c| !c.can_play?(@board) }
      @play_log.puts "!!! Game is a stalemate!"
      break
    end
    if cpu = @cpus.find { |c| c.hand.empty? }
      @play_log.puts "!!! #{cpu.name} has won!"
      @scores[cpu] ||= 0
      @scores[cpu] += 1
      break
    end
    @turns += 1
  end
  @games += 1
  @play_log.puts ""
  @play_log.puts "SCORE THUS FAR: #{Hash[@scores.map { |c, s| [c.name, s] }]}"
end

puts Hash[@scores.map { |c, s| [[c.id, c.name], s] }]