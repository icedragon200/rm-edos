
# Sprite::EquipIcon
# // 03/10/2012
# // 03/10/2012
# // By IceDragon (rpgmakerxace.com)
module Handler
  class SpriteEquip

    attr_reader :subject

    def initialize(subject)
      @subject    = subject
      @cube       = MACL::Cube.new(0, 0, 0, 24, 24, 0) # // Width, Height aren't used yet
      reset()
    end

    def reset()
      self.ox,self.oy = 0, 0
      self.icon_index = 0
      self.angle_abs  = 0
      self.opacity    = 255
      self.visible    = false
    end

    attr_accessor :ox, :oy
    attr_accessor :icon_index
    attr_accessor :angle_abs
    attr_accessor :opacity
    attr_accessor :visible

    def subject_angle()
      case @subject.direction
      when 2
        0
      when 4
        -90
      when 6
        90
      when 8
        180 # // Err why? pole... thats why
      else
        0
      end
    end

    def angle
      angle_abs + subject_angle
    end

    def auto_oxy()
      self.ox, self.oy = 24, 24
    end

    def auto_xyz()
      h = (@subject.height * 0.8).to_i
      case @subject.direction
      when 2
        self.x, self.y = 8, h
        self.z = 1
      when 4
        self.x, self.y = 8, h
        self.z = -1
      when 6
        self.x, self.y = @subject.width-8, h
        self.z = 1
      when 8
        self.x, self.y = @subject.width-8, h
        self.z = -1
      end
    end

    def x
      @cube.x
    end

    def y
      @cube.y
    end

    def z
      @cube.z
    end

    def width
      @cube.width
    end

    def height
      @cube.height
    end

    def x=(n)
      @cube.x = n
    end

    def y=(n)
      @cube.y = n
    end

    def z=(n)
      @cube.z = n
    end

    def width=(n)
      @cube.width = n
    end

    def height=(n)
      @cube.height = n
    end

    # / Actual Sprite x, y, z
    def screen_x
      @subject.x + self.x - 16
    end

    def screen_y
      @subject.y + self.y - 32
    end

    def screen_z
      @subject.z + self.z
    end

    ACTIONS = {}
    ACTIONS["basicattack"] = proc do |hnd|
      hnd.auto_oxy()
      hnd.auto_xyz()
      hnd.visible = true
      base_angle  = 30
      hnd.icon_index = hnd.subject.weapon_icon
      for i in 0..90
        hnd.angle_abs = base_angle + i
        Fiber.yield if i % 9 == 0 # // 10 frames
      end
      hnd.visible = false
    end

    def do_action(&action)
      reset()
      @fiber = Fiber.new { call_action(&action) }
    end

    def call_action(&action)
      action.call(self)
      @fiber = nil
    end

    def update()
      @fiber.resume if(@fiber)
    end

    def busy?()
      !(@fiber).nil?()
    end

    def wait(n)
      n.times do Fiber.yield ; end
    end

  end
end

class Sprite::EquipIcon < Sprite::Icon

  def initialize(viewport,subject)
    super(viewport)
    @subject = subject # // Handler::SpriteEquip*
    pre_init()
    update()
  end

  def pre_init()
  end

  def update()
    super()
    self.x       = @subject.screen_x
    self.y       = @subject.screen_y
    self.z       = @subject.screen_z
    self.ox      = @subject.ox
    self.oy      = @subject.oy
    self.angle   = @subject.angle
    self.opacity = @subject.opacity
    self.visible = @subject.visible
    change_icon_index(@subject.icon_index)
  end

  def change_icon_index(n)
    self.icon_index = n unless(self.icon_index==n)
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
