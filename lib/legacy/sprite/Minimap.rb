# Sprite::Minimap
#==============================================================================#
# ♥ Sprite::Minimap
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0
#
#==============================================================================#
class Sprite::Minimap < Sprite

  attr_reader :data

  def initialize(viewport=nil)
    super(viewport)
    #self.zoom_x = self.zoom_y = 0.5
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def data=(new_data)
    if @data != new_data
      @data = new_data
      refresh
    end
  end

  def refresh
    dispose_bitmap_safe
    tilesize = 16
    iso_width  = Isometric.calc_iso_width(@data.xsize, @data.ysize, tilesize)
    iso_height = Isometric.calc_iso_height(@data.xsize, @data.ysize, tilesize)

    self.bitmap = Bitmap.new(iso_width, iso_height + tilesize / 2)
    self.bitmap.fill_rect(self.bitmap.rect, Palette['white'])
    #self.bitmap.fill_rect( self.bitmap.rect, Palette['brown2'] )
    tbmp = Cache.system("minimap_tiles")
    #for rz in 0...@data.zsize
      for ry in 0...@data.ysize
        for rx in @data.xsize.pred.downto(0)
          x, y = rx, ry
          index = @data[x, y]#, rz]

          xo, yo = *Isometric.calc_screen_xy(x, y, tilesize, tilesize)

          yo += iso_height / 2
          #yo -= 16 * rz #if @data[x, y] == 0
          yo -= tilesize / 2

          self.bitmap.blt(xo, yo, tbmp,
            Rect.new(
              (index % 8 * tilesize), (index / 8 * tilesize), tilesize, tilesize)
          )
        end
      end
    #end
  end

  def scale=( new_scale )
    if @scale != new_scale
      @scale = new_scale
      refresh_scale
    end
  end

  def refresh_scale
    sc = @scale / 1.0
    self.zoom_x = self.zoom_y = sc
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
