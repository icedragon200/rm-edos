class Sprite::MousePos < Sprite::TextStrip

  attr_accessor :finetune

  def initialize(viewport=nil, mouse=Mouse, width=Graphics.width, height=24)
    super(viewport, width, height)
    @mouse = mouse
    @finetune = 1
    @last_pos = nil
  end

  def calc_pos
    [Mouse.x / finetune, Mouse.y / finetune]
  end

  def update
    super
    if @last_pos != (n = calc_pos)
      @last_pos = n
      self.text = "Mouse [X: %d Y: %d]" % @last_pos
      self.align = MACL::Surface::ALIGN_CENTER
      refresh
    end
  end

end
