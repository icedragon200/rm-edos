# Sprite::OptionCursor
# // 02/18/2012
# // 02/18/2012
class Sprite::OptionCursor < Sprite

  attr_accessor :active

  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system("Ani_FingerCursor")
    @sequencer  = IEI::Sequencer.new([0,1,2,1], 10)
    self.active = true
    update_src_rect
  end

  def update_src_rect
    @sequencer.update if self.active
    self.src_rect.set(0,@sequencer.value*13,18,13)
  end

  def update
    super
    update_src_rect
  end

end
