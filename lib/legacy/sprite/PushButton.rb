# Sprite::PushButton
# // 02/12/2012
# // 02/12/2012
class Sprite::PushButton < Sprite

  def initialize(viewport=nil,time=15)
    super(viewport)
    @time = @maxtime = time
    @index = 0
    self.bitmap = Cache.system("Ani_PushButton")
    update_src_rect()
  end

  def update_src_rect()
    self.src_rect.set(16*@index,0,16,16)
  end

  def update()
    super()
    @time = @time.pred
    if @time == 0
      @time = @maxtime
      @index = @index.succ.modulo(2)
      update_src_rect()
    end
  end

end
