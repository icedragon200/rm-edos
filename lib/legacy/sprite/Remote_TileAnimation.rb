# Remote_TileAnimation
# // 02/20/2012
# // 02/20/2012
class Remote_TileAnimation < RemoteAnimation

  def init_internal()
    super()
    @mx, @my = 0,0
  end

  attr_accessor :mx, :my

  def set_mxy(x,y)
    self.mx, self.my = x, y
  end

  def update()
    self.x = _map.adjust_x(@mx) * 32 #+ 16
    self.y = _map.adjust_y(@my) * 32 #+ 32
    super()
  end

end
