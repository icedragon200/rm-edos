#
# EDOS/src/sprite/scenetextstrip.rb
# vr 1.0.0
class Sprite::SceneTextStrip < Sprite

  attr_reader :text

  def initialize(viewport=nil, text="", align=0, size=:normal)
    super(viewport)
    @text  = text
    @align = align
    @size  = size

    h = case @size
        when :normal then Metric.ui_element
        when :small  then Metric.ui_element / 2
        else raise(ArgumentError, "Invalid size #{size}")
        end
    self.bitmap = Bitmap.new(Graphics.width, h)
    refresh
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def refresh
    bitmap = self.bitmap
      bitmap.clear
      bitmap.merio.draw_light_rect(bitmap.rect)
      font = bitmap.font
        style = case @size
                when :normal then 'merio_dark_h1_enb'
                when :small  then 'merio_dark_h3_enb'
                end
        font.set_style(style)
      rect = bitmap.rect.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                                  amount: Metric.contract)
      bitmap.draw_text(rect, @text, @align)
  end

end
