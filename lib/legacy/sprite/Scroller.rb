class Sprite::Scroller < Sprite

  attr_accessor :method_value, :method_max

  def initialize(viewport, parent)
    super(viewport)
    @parent       = parent
    self.method_value = parent.scroll_bar_value_method
    self.method_max   = parent.scroll_bar_max_method
    @last_value   = -1
    @last_max     = self.method_max.call
    @bar_sprite   = Sprite::ScrollBar.new(self.viewport)
    @bar_tween    = MACL::Tween.new([0, 0], [0, 0])
    self.type     = parent.scroll_bar_type
  end

  def viewport=( viewport )
    super( viewport )
    @bar_sprite.viewport = self.viewport
  end

  def dispose
    dispose_bitmap_safe
    @bar_sprite.dispose_all
    super
  end

  attr_reader :type

  def type=( new_type )
    if @type != new_type
      @type = new_type
      refresh
    end
  end

  def refresh
    redraw_base_bar
    redraw_scroll_bar
    update_position
    update_visible
  end

  def redraw_base_bar
    cube = @parent.scroll_bar_cube
    dispose_bitmap_safe
    case @type
    when :vert
      self.bitmap = Bitmap.new(cube.width, cube.depth)
    when :horz
      self.bitmap = Bitmap.new(cube.depth, cube.width)
    end
    @parent.scroll_base_draw(self.bitmap)
  end

  def redraw_scroll_bar
    @bar_sprite.bitmap.dispose if @bar_sprite.bitmap
    @last_max = [@method_max.call, 1].max
    xpad, ypad = 0, 0
    case @type
    when :vert
      bw = self.width
      bh = self.height / @last_max.to_f
      xpad = 1
    when :horz
      bw = self.width / @last_max.to_f
      bh = self.height
      ypad = 1
    else
      raise "Unknown Type: #@type"
    end
    bw, bh = bw.round(0).to_i, bh.round(0).to_i
    @bar_sprite.bitmap = Bitmap.new( bw, bh )
    @parent.scroll_bar_draw(@bar_sprite.bitmap, @type, [xpad, ypad])
  end

  def update_position
    cube = @parent.scroll_bar_cube
    self.x, self.y, self.z = cube.x, cube.y, cube.z
    @bar_sprite.x, @bar_sprite.y, @bar_sprite.z = self.x, self.y, self.z + 1
  end

  def update_visible
    @bar_sprite.visible = self.visible = @parent.scroll_bar_visible?
  end

  def update
    super
    update_visible
    return unless self.visible
    if @last_max != [@method_max.call, 1].max
      redraw_scroll_bar
    end
    if n = @method_value.call and @last_value != n
      @last_value = n
      case @type
      when :vert
        tx = 0
        ty = -([@last_value, 0].max * (self.height / @last_max.to_f))
      when :horz # Unbounded Horizontal, Top Horizontal, Bottom Horizontal
        tx = -([@last_value, 0].max * (self.width / @last_max.to_f))
        ty = 0
      end
      @bar_tween.set_and_reset(
        [@bar_sprite.ox,@bar_sprite.oy],[tx,ty],:expo_out,MACL::Tween.frame_to_sec(10)
      )
      #@bar_tween.scale_values( 100.0 )
    end
    @bar_tween.update
    @bar_sprite.ox, @bar_sprite.oy = *@bar_tween.values
  end

end
