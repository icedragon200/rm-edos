# Sprite::SpeechBubble
# // 02/09/2012
# // 02/09/2012
class Sprite::SpeechBubble < Sprite

  include Mixin::DrawTextEx

  def initialize(viewport, text)
    super(viewport)
    @text = text
    b = Bitmap.back_buffer
    b.font.set_style(standard_style)
    r = b.text_size(@text)
    self.bitmap = Bitmap.new((r.width + 16) + 16, 24)
    self.bitmap.font.set_style(standard_style)
  end

  def contents
    self.bitmap
  end

  def standard_style
    'window_header'
  end

  def process_escape_character(code, text, pos)
    case code.upcase
    when '$'
      @gold_window.open
    when '.'
      wait(15)
    when '|'
      wait(60)
    when '!'
      input_pause
    when '>'
      @line_show_fast = true
    when '<'
      @line_show_fast = false
    when '^'
      @pause_skip = true
    when 'W'
      wait(obtain_escape_param(text))
    else
      super
    end
  end

  def process_all_text
    #open_and_wait
    text = convert_escape_characters(@text)
    #pos = {}
    ax = (self.bitmap.width - self.bitmap.text_size(@text).width) / 2
    pos = {:x => ax, :y => 0, :new_x => 0, :height => calc_line_height(text)}
    #new_page(text, pos)
    @skip = false
    process_character(text.slice!(0, 1), text, pos) until(text.empty?)
  end

  def process_normal_character(c, pos)
    super
    wait_for_one_character
  end

  def update_show_fast
    @show_fast = true if Input.trigger?(:C)
  end

  def wait_for_one_character
    update_show_fast
    Fiber.yield unless(@show_fast || @line_show_fast)
  end

  def wait(n)
    n.times { Fiber.yield }
  end

end

class SpeechBubble < Shell

  attr_reader :current_wait_count

  def initialize( viewport, x, y, text )
    super(0, 0, 0, 0)
    @current_wait_count = 0
    @text = text
    @base_sprite = Sprite.new(nil)
    @text_sprite = Sprite::SpeechBubble.new(nil, @text)
    w, h = @text_sprite.bitmap.width, @text_sprite.bitmap.height
    bmp = @base_sprite.bitmap = Bitmap.new(w, h)
    rects = DrawExt::Helper.default_seg_rects('help')
    DrawExt.repeat_bitmap_multi_seg(
      bmp, bmp.rect, Cache.system("header_help(sprite)"), rects, 1)
    each_sprite { |s| s.ox, s.oy = s.bitmap.width / 2, s.bitmap.height+32 }
    self.viewport = viewport
    self.x = x
    self.y = y
    self.z = 100
    self.opacity = 0
    @fiber = Fiber.new { fiber_main }
  end

  def fadein
    self.opacity += 255 / 20
  end

  def fadeout
    self.opacity -= 255 / 20
  end

  def fiber_fadein
    while opacity < 255
      fadein
      Fiber.yield
    end
  end
  def fiber_fadeout
    while opacity > 0
      fadeout
      Fiber.yield
    end
  end
  def fiber_main
    fiber_fadein
    @text_sprite.process_all_text
    wait(60)
    fiber_fadeout
    wait(5)
    @fiber = nil
  end
  def wait(n)
    n.times { |i| @current_wait_count = n; Fiber.yield }
  end
  def done?
    @fiber == nil
  end
  def opacity=(n)
    super(n)
    each_sprite { |s| s.opacity = self.opacity }
  end
  def x=(n)
    super(n)
    each_sprite { |s| s.x = self.x }
  end
  def y=(n)
    super(n)
    each_sprite { |s| s.y = self.y }
  end
  def z=(n)
    super(n)
    @base_sprite.z = self.z - 1
    @text_sprite.z = self.z
  end
  def each_sprite
    [@base_sprite, @text_sprite].each { |s| yield s }
  end
  def dispose
    each_sprite { |s| s.dispose }
    super
  end
  def update
    super
    @base_sprite.update
    @text_sprite.update
    @fiber.resume if @fiber
  end
  def viewport=(n)
    super(n)
    each_sprite { |s| s.viewport = self.viewport }
  end
end
