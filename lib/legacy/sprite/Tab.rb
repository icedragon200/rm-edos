# // 02/06/2012
class Sprite::Tab < Sprite

  include Mixin::DrawTextEx

  def initialize(viewport=nil,text="",width=96,height=14)
    super(viewport)
    set(text,width,height)
  end

  def contents
    self.bitmap
  end

  attr_reader :text

  def text=(n)
    if @text != n
      @text = n
      refresh()
    end
  end

  def line_height
    self.height || super
  end

  def refresh()
    self.bitmap = Bitmap.new(@twidth, @theight)
    self.bitmap.gui_draw_tab()
    self.bitmap.font.set_style('window_header2')
    draw_text(32,6,self.bitmap.width-32,12,@text,1)
  end

  def set(text,width,height)
    @text    = text
    @twidth  = width
    @theight = height
    refresh()
  end

  def twidth=(n)
    if @twidth != n
      @twidth = n
      refresh()
    end
  end

  def theight=(n)
    if @theight != n
      @theight = n
      refresh()
    end
  end

end
