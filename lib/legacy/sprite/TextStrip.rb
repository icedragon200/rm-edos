#
# EDOS/src/sprite/TextStrip.rb
# vr 1.0.0
class Sprite::TextStrip < Sprite

  attr_accessor :text, :align

  def initialize(viewport, width, height)
    super(viewport)
    self.bitmap = Bitmap.new(width, height)
    @text = ""
    @align = 0
  end

  def set_text(text, align)
    if @text != text or @align != align
      @text, @align = text, align
      refresh
    end
    self
  end

  def refresh
    bmp = self.bitmap

    back_color = Palette['black-half']
    rect_back = bmp.rect.contract(anchor: 5, amount: 1)
    rect_text = bmp.rect

    bmp.clear
    bmp.fill_rect(rect_back, back_color)
      bmp.blur
    bmp.font.set_style('text_strip')
      bmp.draw_text(rect_text, @text, @align)
  end

  def dispose
    self.bitmap.dispose
    super
  end

end
