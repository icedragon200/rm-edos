#
#
# vr 1.0
class Sprite::WindowTail < Sprite

  def initialize(viewport=nil, parent)
    super(viewport)
    @parent = parent
    update_position
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def refresh
    dispose_bitmap_safe
    bmp = self.bitmap = Bitmap.new( @parent.tail_width, @parent.tail_height )

    rects = DrawExt::Helper.default_seg_rects('tail')
    DrawExt.repeat_bitmap_multi_seg(
      bmp, bmp.rect, @parent.footer_bitmap, rects, 1)

    update_position
    update_visible
  end

  def update_position
    self.x = @parent.tail_x
    self.y = @parent.tail_y
    self.z = @parent.tail_z
  end

  def update_visible
    self.visible = @parent.tail_visible?
  end

  def update_opacity
    self.opacity = @parent.tail_opacity
  end

  def update_viewport
    self.viewport = @parent.viewport
  end

end
