#
# EDOS/lib/sprite/element_cursor.rb
#
class Sprite::ElementCursor < Sprite::LoopAnimation

  attr_reader :colors
  attr_reader :tones

  def initialize(viewport=nil, a=(1..6))
    super(viewport, "heal5s", 0...12, 3)

    @_ignore_viewport_crop = true # SRRI patch

    @colors = a.map{ |i| Palette["element#{i}"].hset(:alpha => 255) }
    @tones  = @colors.map{ |c|
      tone = c.dup.hset(alpha: 255).blend.lighten(0.2).to_tone
      tone.gray = 0
      tone
    }
    @lindex = @index  = 0
    @counter= 0
    self.size = 48.0
    self.blend_type = 1
  end

  attr_reader :size
  def size=(n)
    @size = n.to_f
    self.zoom_x = self.zoom_y = @size / self.width
  end

  def elements_size
    @colors.size
  end

  attr_reader :index
  def index=(n)
    if @index != n
      @lindex = @index.modulo(elements_size)
      @index = n
      @counter = 0
    end
  end

  def pred_index(wrap=true)
    self.index = self.index.pred
    self.index = wrap ? self.index.modulo(elements_size) : self.index.clamp(0,elements_size-1)
  end

  def succ_index(wrap=true)
    self.index = self.index.succ
    self.index = wrap ? self.index.modulo(elements_size) : self.index.clamp(0,elements_size-1)
  end

  def ltone
    @tones[@lindex]
  end

  def ttone
    @tones[@index]
  end

  def lcolor
    @colors[@lindex]
  end

  def tcolor
    @colors[@index]
  end

  def update()
    super()
    @counter = (@counter + (255 / 20.0)).clamp(0, 255)
    self.tone  = ltone.lerp(ttone, @counter / 255.0)
    self.color = lcolor.lerp(tcolor, @counter / 255.0)
  end

end
