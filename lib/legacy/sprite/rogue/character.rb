# Sprite::RogueCharacter
#==============================================================================#
# ♥ Sprite::RogueCharacter
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/06/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/06/2011 V1.0
#
#==============================================================================#
class Sprite::RogueCharacter < Sprite::Character
  #include Mixin::Sprite_Effect
  def _map
    $game.rogue
  end
  def update_bitmap
    if graphic_changed?
      @tile_id = @character.tile_id
      @character_name = @character.character_name
      @character_index = @character.character_index
      @character_hue  = @character.character_hue
      if @tile_id > 0
        set_tile_bitmap
      else
        set_character_bitmap
      end
    end
  end
  def graphic_changed?
    @tile_id != @character.tile_id ||
    @character_name != @character.character_name ||
    @character_index != @character.character_index ||
    @character_hue != @character.character_hue
  end
  def set_character_bitmap
    self.bitmap = Cache.character(@character_name, @character_hue)
    sign = @character_name[/^[\!\$]./]
    if sign && sign.include?('$')
      @cw = bitmap.width / 3
      @ch = bitmap.height / 4
    else
      @cw = bitmap.width / 12
      @ch = bitmap.height / 8
    end
    self.ox = @cw / 2
    self.oy = @ch
  end
  def update
    super
    #setup_new_effect
    setup_new_animation
    update_effect
  end
  def init_visibility
    @character_visible = @character.alive?
    self.opacity = 0 unless @character_visible
  end
  def remove?
    @character.remove_sprite? #&& !effect?
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
