#
# EDOS/lib/sprite/window_header.rb
#   by IceDragon
class Sprite::WindowHeader < Sprite

  def initialize(viewport=nil, parent)
    super( viewport )
    @parent = parent
    update_position
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def refresh
    dispose_bitmap_safe
    cube  = @parent.header_cube
    sett  = @parent.header_text_settings
    text  = @parent.header_text
    font  = sett["font"]
    align = sett["align"]
    bmp = self.bitmap = Bitmap.new(cube.width, cube.height)

    rects = DrawExt::Helper.default_seg_rects('header')
    DrawExt.repeat_bitmap_multi_seg(
      bmp, bmp.rect, @parent.header_bitmap, rects, 1)

    bmp.font = font
    sw = ([0, 72, 36])[align]
    bmp.draw_text(36, 1, bmp.width - sw, bmp.height, text, align)

    update_position
    update_visible
  end

  def update_position
    cube = @parent.header_cube
    self.x, self.y, self.z = cube.x, cube.y, cube.z
  end

  def update_visible
    self.visible = @parent.header_visible?
  end

end