# Spriteset::Minimap
#==============================================================================#
# ♥ Spriteset_Minimap
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0
#
#==============================================================================#
class Spriteset::Minimap
  attr_reader :visible
  attr_reader :viewport1
  def initialize
    @viewport1 = Viewport.new( Graphics.width-72, Graphics.playheight-72, 72, 72 )
    @viewport1.z = 200
    refresh
    self.visible = true
    lock
    update
  end
  def rect
    @viewport1.rect
  end
  def set_rect( x, y=0, width=0, height=0 )
    x, y, width, height = *x.to_a if x.is_a?(Rect) || x.is_a?(Window) || x.is_a?(Array)
    @viewport1.rect.set( x, y, width, height )
    self
  end
  def visible=( visible )
    if visible != @visible
      @visible = visible
      @viewport1.visible = @visible
    end
  end
  def _map
    $game.system._map
  end
  def fit_to_screen
    g, s = Graphics.width, @minimap_sprite.width
    self.scale = g.to_f / s
    self
  end
  def scale=( new_scale )
    if @scale != new_scale
      @scale = new_scale
      refresh_scale
    end
  end
  def refresh_scale
    @minimap_sprite.scale = @scale
    @character_sprites.each { |c| c.scale = @scale }
  end
  def min_refresh
    @mm_width = @minimap_sprite.width
    @mm_height= @minimap_sprite.height
  end
  def refresh
    dispose_minimap
    dispose_characters
    create_minimap
    min_refresh
    create_characters
    update
    self
  end
  def create_minimap
    @minimap_sprite = Sprite::Minimap.new( @viewport1 )
    @minimap_sprite.data = _map.passage_table
    @minimap_sprite.z = 0
  end
  def create_characters
    @character_sprites = []
    _map.characters.each { |c|
      ch_sp = Sprite::MinimapCharacter.new( @viewport1, c )
      ch_sp.mm_width  = @mm_width
      ch_sp.mm_height = @mm_height
      @character_sprites << ch_sp
    }
    if $game.system.rogue?
      ch_sp = Sprite::MinimapCharacter.new( @viewport1, _map.cursor )
      ch_sp.mm_width  = @minimap_sprite.width
      ch_sp.mm_height = @minimap_sprite.height
      @character_sprites << ch_sp
      @cursor_sprite = ch_sp
    end
    @map_id = _map.map_id
  end
  def refresh_characters
    dispose_characters
    create_characters
  end
  def dispose
    dispose_minimap
    dispose_characters
    dispoe_viewports
  end
  def dispose_minimap
    @minimap_sprite.dispose if @minimap_sprite
    @minimap_sprite = nil
  end
  def dispose_characters
    @character_sprites.each { |c| c.dispose } if @character_sprites
    @character_sprites = nil
  end
  def dispoe_viewports
    @viewport1.dispose
  end
  def update
    update_characters
    update_viewports
  end
  def update_characters
    refresh_characters if @map_id != _map.map_id
    @character_sprites = @character_sprites.inject([]) { |result, sprite|
      sprite.update
      sprite.remove? ? sprite.dispose : result << sprite ; result
    }
  end
  def update_viewports
    @viewport1.update
    if @cursor_sprite
      scrollto(
        @cursor_sprite.x - ((@viewport1.rect.width - 16) / 2.0),
        @cursor_sprite.y - ((@viewport1.rect.height - 16) / 2.0)
      )
    end if @locked
  end
  def unlock
    @locked = false
  end
  def lock
    @locked = true
  end
  def scroll(d,n)
    case d
    when 2
      scroll_down(n)
    when 4
      scroll_left(n)
    when 6
      scroll_right(n)
    when 8
      scroll_up(n)
    end
  end
  def scroll_up(n)
    scrollto(@viewport1.ox, @viewport1.oy - n)
  end
  def scroll_down(n)
    scrollto(@viewport1.ox, @viewport1.oy + n)
  end
  def scroll_left(n)
    scrollto(@viewport1.ox - n, @viewport1.oy)
  end
  def scroll_right(n)
    scrollto(@viewport1.ox + n, @viewport1.oy)
  end
  def scrollto(x, y)
    @viewport1.ox = x.clamp(0, (@minimap_sprite.width-Graphics.width).max(0))
    @viewport1.oy = y.clamp(0, (@minimap_sprite.height-Graphics.height).max(0))
  end
end
#Isometric.run_iso_test
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
