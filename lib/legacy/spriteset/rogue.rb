# Spriteset::Rogue
#==============================================================================#
# ♥ Spriteset_Rogue
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/14/2011
# // • Data Modified : 12/14/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/14/2011 V1.0
#
#==============================================================================#
class Spriteset::Rogue < Spriteset::Map

  def create_all
    super
    create_items
    create_cursor
    create_ranges
    create_remote_animations
  end
  def create_shadow
  end
  def update_shadow
  end
  def dispose_shadow
  end
  # // 02/20/2012 [
  def create_move_cursor
  end
  def dispose_move_cursor
  end
  def update_move_cursor
  end
  def create_ranges
    @ranges = []
  end
  def create_remote_animations
    @remote_animations = Array.new(4) { Remote_TileAnimation.new }
  end
  def play_remote_anim(animation)
    a=@remote_animations.find{|a|!a.animation?}
    a.start_animation(animation)
    a
  end
  def play_remote_anim_by_id(anim_id)
    play_remote_anim($data_animations[anim_id])
  end
  # // 02/20/2012 ]
  def _map
    return $game.rogue
  end
  def create_tilemap
    super
    refresh_tilemap
  end
  def refresh_tilemap
    load_tileset
    @tilemap.flash_data = _map.flash_data
  end
  def create_characters
    @character_sprites = []
    _map.objects.each do |c|
      @character_sprites << Sprite::RogueCharacter.new( @viewport1, c )
    end
  end
  def create_items
    @item_sprites = []
    _map.items.each do |c|
      @item_sprites << Sprite::RogueItem.new( @viewport1, c )
    end
  end
  def refresh_items
    dispose_items
    create_items
  end
  def create_cursor
    @cursor_sprite = Sprite::RogueCursor.new( @viewport1, _map.cursor )
  end
  def refresh
    refresh_tilemap
    refresh_characters
    refresh_items
    @map_id = _map.map_id
    update
  end
  def dispose
    dispose_cursor
    dispose_items
    super
  end
  def dispose_cursor
    @cursor_sprite.dispose
  end
  def dispose_items
    @item_sprites.each { |sprite| sprite.dispose }
  end
  def update
    refresh if @map_id != _map.map_id
    update_items
    update_cursor
    super
  end
  def update_characters
    refresh_characters if @map_id != $game.rogue.map_id
    @character_sprites = @character_sprites.inject([]) { |result, sprite|
      sprite.update
      sprite.remove? ? sprite.dispose : result << sprite ; result
    }
  end
  def update_items
    if _map.items_need_refresh
      refresh_items
      _map.items_need_refresh = false
    end
    @item_sprites = @item_sprites.inject([]) { |result, sprite|
      sprite.update
      sprite.remove? ? sprite.dispose : result << sprite ; result
    }
  end
  def update_cursor
    @cursor_sprite.update
  end
  def update_viewports
    @viewport_tween.update
    @viewport_rect.set(*@viewport_tween.values)
    @viewport1.rect.set(@viewport_rect)
    @viewport2.rect.set(@viewport_rect)
    @viewport3.rect.set(@viewport_rect)
    super
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
