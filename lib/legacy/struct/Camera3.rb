#
# EDOS/src/struct/camera3.rb
#   by IceDragon
#   dc 03/04/2013
#   dm 03/04/2013
# vr 1.0.0
module ThreeDee
class Camera3

  attr_reader :pos, :rotation

  def initialize
    @pos = StarRuby::Vector3F.new
    @rotation = StarRuby::Vector3F.new
  end

  def x
    @pos.x
  end

  def y
    @pos.y
  end

  def z
    @pos.z
  end

  def x=(new_x)
    @pos.x = new_x
  end

  def y=(new_y)
    @pos.y = new_y
  end

  def z=(new_z)
    @pos.z = new_z
  end

  def pitch
    @rotation.x
  end

  def yaw
    @rotation.y
  end

  def roll
    @rotation.z
  end

  def pitch=(n)
    @rotation.x = n
  end

  def yaw=(n)
    @rotation.y = n
  end

  def roll=(n)
    @rotation.z = n
  end

  def to_options
    {
      camera_x: x,
      camera_y: y,
      camera_height: z,
      camera_yaw: yaw.degree_to_radian,
      camera_pitch: pitch.degree_to_radian,
      camera_roll: roll.degree_to_radian,
    }
  end

end
end
