#
# EDOS/core/test_core.rb
# vr 1.0.0
module TestCore

  extend MACL::Mixin::Log

  class TestCoreError < Exception
    #
  end

  class TestCoreCancelError < Exception
    #
  end

  # PIT
  class FrameRetrigger

    attr_reader :frames,
                :frames_max

    def initialize(frames, func)
      @frames = @frames_max = frames
      @func = func
    end

    def update
      @frames = (@frames + 1) % @frames_max
      @func.() if @frames == 0
    end

  end

  # PIT
  class FrameRepeat

    attr_reader :frames,
                :frames_max

    def initialize(frames, func)
      @frames = 0
      @frames_max = frames
      @func = func
    end

    def reset
      @frames = @frames_max
    end

    def update
      if @frames > 0
        @frames -= 1
        @func.(@frames)
      end
    end

  end

  TEST_CANCELLED = 0x02
  TEST_DISABLED  = 0x11 # disabled
  TEST_FAILED    = 0x01 # failed
  TEST_PASSED    = 0x00 # passed with no problsm
  TEST_PASSEDE   = 0x10 # passed but errors had to be suppressed

  TEST_TIME_SHORT  = Graphics.frame_rate * 2 # 2 seconds
  TEST_TIME_LONG   = Graphics.frame_rate * 5 # 5 seconds
  TEST_TIME_XLONG  = Graphics.frame_rate * 10 # 10 seconds
  TEST_TIME_X2LONG = Graphics.frame_rate * 20 # 20 seconds
  TEST_TIME_X3LONG = Graphics.frame_rate * 30 # 30 seconds

  # always make sure that tests return the correct result
  STRICT_TEST = true

  @message_hook = true
  @message_test = false

  def self.message_hook?
    !!@message_hook
  end

  def self.message_test?
    !!@message_test
  end

  def self._test_log
    return unless message_test?
    return unless log
    yield log
  end

  def self.valid_result?(res)
    return [TEST_DISABLED, TEST_FAILED, TEST_PASSED, TEST_PASSEDE].include?(res)
  end

  def self.run_all_test
    @message_hook = false
    tests = methods.map(&:to_s)
    tests.select! do |s| s.start_with?("test_") end
    tests.sort!
    result = {}

    # clean up
    get_drawables = -> do
      [Bitmap, SRRI::SoftSprite, Sprite, Window, Viewport, Plane].inject([]) do |r, klass|
      #[Interface::IDisposable].inject([]) do |r, klass|
        r.concat(ObjectSpace.each_object(klass).to_a)
        r.reject! do |o| o.kind_of?(Cache::CacheBitmap) end
        r
      end
    end

    testcore_filename = 'testcore_status.json'
    test_log = {}

    write_test_map = -> do
      File.write(testcore_filename, test_log.to_json)
    end

    read_test_map = -> do
      test_log = JSON.load(File.read(testcore_filename))
    end

    if testcore_filename
      if File.exists?(testcore_filename)
        read_test_map.()
      else
        tests.each do |test_name|
          test_log[test_name] = false
        end
      end
    end

    tests.each_with_index do |meth_name, i|
      next if test_log[meth_name]
      Main.update # So stuff can clear the screen and such
      old = get_drawables.()

      begin
        try_log { |l| l.puts("Executing test: %s" % meth_name) }
        time_then = Time.now
        res = self.send(meth_name)
        try_log { |l| l.puts "Test completed in %f sec" % (Time.now - time_then) }
        if STRICT_TEST && !valid_result?(res)
          puts method(meth_name).source_location.join(':')
          raise(TestCoreError, "=.= #{meth_name} did not return a valid TEST result")
        elsif !valid_result?(res)
          res = TEST_PASSED
        end
        result[meth_name] = res
      rescue SystemExit
        exit
      rescue TestCoreCancelError => ex
        result[meth_name] = TEST_CANCELLED
      rescue TestCoreError => ex
        raise ex
      rescue SRRI::Interrupts::SRRIBreak
        exit
      rescue(Exception) => ex
        if STRICT_TEST
          p ex
          puts ex.backtrace
          raise(TestCoreError, ">-<; #{meth_name} failed and did not handle the error")
        else
          result[meth_name] = TEST_FAILED
          p ex
        end
      end

      new_objs = get_drawables.() - old
      new_objs.reject!(&:disposed?)
      if STRICT_TEST && !new_objs.empty?
        remains = new_objs.map do |obj|
          obj.to_s + "\n" + obj.made_by[0]
        end.join("\n").indent!(2)
        msg = "(_ _); #{meth_name} did not clean up after it self, leaving:\n#{remains}"
        puts msg
        #raise(TestCoreError, msg)
      else
        new_objs.each do |obj|
          obj.dispose unless obj.disposed?
        end
      end

      test_log[meth_name] = result[meth_name]
      write_test_map.() if testcore_filename

      msg = case result[meth_name]
            when TEST_PASSED    then "[  PASSED  ]"#.colorize(:light_green)
            when TEST_PASSEDE   then "[PASSED ERR]"#.colorize(:yellow)
            when TEST_FAILED    then "[  FAILED  ]"#.colorize(:light_red)
            when TEST_CANCELLED then "[  CANCEL  ]"#.colorize(:pink)
            when TEST_DISABLED  then "[ DISABLED ]"#.colorize(:light_blue)
            else                     "[ UNKNOWN  ]"#.colorize(:white)
            end + " " + meth_name
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:test, msg])
      try_log { |log| log.puts(msg) }
    end

    all_with_value = proc do |hsh, tst|
      res = {}
      hsh.each_pair do |(k, v)|
        res[k] = v if v == tst
      end
      res
    end

    print_in_order = lambda do |hsh|
      STDERR.puts hsh.keys.sort
    end

    passed   = all_with_value.(result, TEST_PASSED)
    passede  = all_with_value.(result, TEST_PASSEDE)
    failed   = all_with_value.(result, TEST_FAILED)
    disabled = all_with_value.(result, TEST_DISABLED)

    STDERR.puts "#{passed.size} / #{result.size} tests passed"
    print_in_order.(passed)

    STDERR.puts "#{passede.size} / #{result.size} tests passed but with errors"
    print_in_order.(passede)

    STDERR.puts "#{failed.size} / #{result.size} tests passed but with errors"
    print_in_order.(failed)

    STDERR.puts "#{disabled.size} / #{result.size} tests where disabled"
    print_in_order.(disabled)

    if File.exists?(testcore_filename)
      File.delete(testcore_filename)
    end
  rescue TestCoreError => ex
    p ex
    puts ex.backtrace
    exit
  end

  def self._test_loop_(time=nil)
    loop do
      break if main_update
      yield

      if time
        time -= 1
        break if time <= 0
      end
    end
  end

  def self.main_update
    Main.update
    #raise TestCoreCancelError, "Cancelled" if Input.sr_trigger?(:keyboard, :f4)
    return Input.sr_trigger?(:keyboard, :escape)
  end

  def self.alias_class_method(newname, oldname)
    module_eval(%Q(
      class << self
        alias #{newname} #{oldname}
      end
    ))
  end

  def self.hook_tests
    methods.each do |meth|
      next unless meth.to_s.start_with?('test_')
      newmeth = "hookless_#{meth}"
      src = method(meth).source_location.join(?:)
      module_eval(%Q(
        class << self
          alias #{newmeth} #{meth}

          def #{meth}
            msg = %Q(Running #{meth} test from #{'\n'}#{src})
            log.puts msg if log && message_hook?
            return #{newmeth}
          end
        end
      ))
    end
  end

end

dir  = File.dirname(__FILE__)
path = File.join(dir, 'test_core')
file = File.join(path, '*.rb')

Dir[file].sort.each { |f| require_relative f }

TestCore.hook_tests