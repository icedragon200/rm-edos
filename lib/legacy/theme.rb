#
# EDOS/src/theme.rb
#
class Theme

  @@themes = {}
  @@current = nil

  def self.current
    return @@current
  end

  def self.set_current(name)
    @@current = @@themes[name]
  end

  class << self
    alias real_new new
    private :real_new
  end

  def self.new(name)
    theme = @@themes[name] = real_new(name)
    yield theme if block_given?
    return theme
  end

  attr_reader :name

  def initialize(name)
    @name = name.to_s
  end

  #attr_accessor :
end
