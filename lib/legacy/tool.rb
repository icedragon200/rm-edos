__END__
module EDOS
  module Tool

    def self.icon_viewer
      iconsize   = 24

      iconset    = Cache.system('iconset')

      iconset_sp = spr(nil)
      icon_sp    = spr(nil)
      index_sp   = spr(nil)

      iconset_sp.bitmap = iconset
      iconset_sp.src_rect.set(0, 0, iconset.width, gheight)
      icon_sp.bitmap = Bitmap.new(iconsize, iconsize)
      icon_sp.bitmap.draw_gauge_ext_sp4(colors: DrawExt::WHITE_BAR_COLORS)
      icon_sp.opacity = 128

      index_sp.bitmap = Bitmap.new(gwidth, 24)
      index_sp.align_to!(anchor: 1)
      pos = MACL::Pos3.new(0, 0)

      total_rows = iconset_sp.bitmap.height / iconsize
      visible_rows = iconset_sp.height / iconsize
      hvisible_rows = visible_rows / 2
      bottom_row = total_rows - hvisible_rows

      loop do
        Main.update
        row = pos.z #* iconsize
        current_row = [[row - hvisible_rows, 0].max, bottom_row].min
        iconset_sp.x = 0
        iconset_sp.src_rect.y = current_row * iconsize
        icon_sp.x = pos.x * iconsize
        icon_sp.y = (row - current_row) * iconsize

        if Input.dir4 != 0
          if Input.repeat?(:UP)
            pos.move_north(1)
          elsif Input.repeat?(:DOWN)
            pos.move_south(1)
          elsif Input.repeat?(:LEFT)
            pos.move_west(1)
          elsif Input.repeat?(:RIGHT)
            pos.move_east(1)
          end
          index_sp.bitmap.clear
          index_sp.bitmap.font.size = 16
          index_sp.bitmap.draw_text(index_sp.bitmap.rect,
                                    "X: %d Y: %d INDEX: %d" %
                                    [pos.x, pos.z, pos.x + pos.z * 16], 2)
        end
        break if Input.trigger?(:esc)
      end

      # cleanup
      icon_sp.dispose_all
      iconset_sp.dispose
      index_sp.dispose_all
      return 0
    end

  end
end
