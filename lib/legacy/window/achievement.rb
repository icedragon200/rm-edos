# Window::Achievement
#==============================================================================#
# ♥ Window::Achievement
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/27/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/27/2011 V1.0
#
#==============================================================================#
class Window::Achievement < Window::SmallText

  def initialize(x, y, achievement, text)
    super(x, y, Graphics.width / 1.5, 40)
    @_def_align = 1
    self.openness = 0
    @achievement = achievement
    set_text( text )
    open()
  end

  def standard_padding
    return Metric.padding
  end

  def refresh
    art = artist()
    contents.clear
    contents.font.set_style("simple_black")
    dx = 4
    dy = ((contents.height-24)/2)
    contents.blt( dx, dy,
      Cache.system("Gui_Symbols(Small)"), Rect.new( 24, 0, 24, 24 ) )
    art.change_color( art.normal_color )
    contents.font.color.darken!( 0.7 )
    art.draw_text( 24, 0, contents.width-32, 14, "Achievement", 0 )
    contents.font.set_style( :simple_black )
    contents.font.color.lighten!( 0.2 )
    art.draw_text( 24, 0, contents.width-32, 14, @achievement, 2 )
    contents.font.set_style( :simple_black )
    art.draw_text( 32, 12, contents.width-40, 12, @text, 1 )
  end

end
