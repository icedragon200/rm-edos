class Window::Base < Window

  class ContentBitmap < Bitmap ; end

  include Mixin::Ajar
  #include Mixin::ColorSkin
  include Mixin::ArtistHost
  include WindowManager::WindowClient

  def initialize(x, y, width, height)
    super
    self.windowskin = Window::SkinCache.bitmap("window")
    update_padding
    update_tone
    create_contents
    init_artist
    self.back_opacity = 255
    @opening = @closing = false
  end

  def dispose
    contents.dispose unless contents.disposed?
    super
  end

  def line_height
    return 24
  end

  def standard_padding
    return Metric.padding
  end

  def update_padding
    self.padding = standard_padding
  end

  def contents_width
    width - standard_padding * 2
  end

  def contents_height
    height - standard_padding * 2
  end

  def fitting_height(line_number)
    line_number * line_height + standard_padding * 2
  end

  def update_tone
    #self.tone.set($game.system.window_tone)
  end

  def create_contents
    contents.dispose
    if contents_width > 0 && contents_height > 0
      self.contents = ContentBitmap.new(contents_width, contents_height)
    else
      self.contents = ContentBitmap.new(1, 1)
    end
  end

  def update
    super
    update_tone
  end

  def show
    self.visible = true
    self
  end

  def hide
    self.visible = false
    self
  end

  def activate
    self.active = true
    self
  end

  def deactivate
    self.active = false
    self
  end

  include Mixin::CursorState
    alias :update_cursor_state :_update_cursor_state
    undef_method :_update_cursor_state

end
