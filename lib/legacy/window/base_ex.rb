# Window::Base (Ex)
# // 12/12/2011
# // 02/02/2012
class Window::Base

  include Mixin::CallbackHook

  [:viewport=, :opacity=, :visible=, :openness=,
    :x=, :y=, :z=, :ox=, :oy=, :width=, :height=].each do |sym|
    add_callback_hook(sym)
  end

  # ] \\ 02/19/2012
  # // Window Addon Patch
  alias :winad_initialize :initialize
  def initialize(*args,&block)
    winad_initialize(*args,&block)
    init_callbacks
    init_addons
  end

  alias :winad_dispose :dispose
  def dispose(*args,&block)
    dispose_addons
    winad_dispose(*args,&block)
  end

  alias :winad_update :update
  def update(*args,&block)
    winad_update(*args,&block)
    update_addons
  end

  def contents_x
    self.x + standard_padding
  end

  def contents_y
    self.y + standard_padding
  end

  def contents_rect
    Rect.new(contents_x, contents_y,
             self.width - standard_padding,
             self.height - standard_padding - padding_bottom)
  end

  def init_addons
    #
  end unless method_defined?(:init_addons)

  def dispose_addons
    #
  end unless method_defined?(:dispose_addons)

  def update_addons
    #
  end unless method_defined?(:update_addons)

  def win_busy?
    false
  end unless method_defined?(:win_busy?)

  def mouse_in_window?
    Mouse.in_area?(self.to_rect)
  end

  #include Mixin::ContentEffects
  #include Mixin::ActiveFading

  #alias :actfade_update :update
  #def update(*args, &block)
  #  actfade_update(*args, &block)
  #  update_active_fading
  #end

end
