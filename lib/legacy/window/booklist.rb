# Window::BookList
#==============================================================================#
# ♥ Window::BookList
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0 
#==============================================================================#
class Window::BookList < Window::Selectable
  
  AddonBin_WBL = WindowAddons::AddonBin[WindowAddons::ScrollBar]

  def addon_bin
    AddonBin_WBL
  end  

  def initialize( x, y )
    make_item_list()
    super( x, y, (Graphics.width/2.2).to_i, Graphics.height )
    self.windowskin = Cache.system( "Window02" )
    self.index = 0
    activate()
    refresh()
  end  

  def items
    @items
  end  

  def make_item_list
    @items = []
  end  

  def item()
    return items[index+1]
  end  

  def item_max
    return items.size - 1
  end  

  def draw_item( index )
    rect = item_rect( index )
    item = items[index+1]
    contents.font.set_style( :big_number )
    draw_text( rect, "%03d" % (item ? item.id : index+1) )
    rect.x += 32 ; rect.width -= 32
    contents.font.set_style( :paper_text )
    spacer = ?-
    wd = contents.text_size(spacer).width  
    name = item ? item.name : ""
    draw_text( rect, name.if_eql?( "", spacer * (rect.width / wd) ), 2 )
  end  

  def update_help
    @help_window.set_item( item )
  end

end

require_relative 'booklist/bestiary.rb'
require_relative 'booklist/itemary.rb'
require_relative 'booklist/skillary.rb'
require_relative 'booklist/traptionary.rb'
