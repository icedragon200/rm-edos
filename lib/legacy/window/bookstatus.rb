# Window::BookStatus
#==============================================================================#
# ♥ Window::BookStatus
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0
#==============================================================================#
class Window::BookStatus < Window::Base

  attr_reader :item

  def initialize()
    super( 0, 0, Graphics.width, Graphics.height )
    init_members()
    refresh()
  end

  def init_members()
  end

  def set_item( item )
    return if(@item == item)
    on_set_item( @item, item )
    @item = item
    refresh
  end

  def on_set_item( old_item, new_item )
  end

  def item_id()
    @item ? @item.id : 0
  end

  def item_name()
    @item ? @item.name : ""
  end

  def contents_width
    (width - standard_padding * 2) - 247
  end

  def refresh
    contents.clear()
    artist do |art|
      art.draw_block1( 0, 0 )
      art.draw_block2( 0, 28 )
    end
  end

  def standard_artist
    Artist::BookStatus
  end

end

class Artist::BookStatus < Artist

  def draw_book_border( x, y )
    bmp = Cache.system( "Book_Border" )
    contents.blt( x, y, bmp, bmp.rect )
    bmp
  end

  def draw_id_header( x, y, id )
    text = "No.%03d" % id
    contents.font.set_style( :default )
    contents.font.size = Font.default_size - 2
    contents.font.bold = true
    draw_text( x, y, contents.width, 24, text )
    contents.text_size( text )
  end

  def draw_block1( x, y )
    draw_id_header( x, y, item_id )
    draw_horz_line( y+24 )
  end

  def draw_block2( x, y )
    bmp = draw_book_border( x, y )
    contents.font.set_style( :simple_black )
    draw_text( x+7, y+3, bmp.width-14, 16, item_name, 1 )
    draw_horz_line( y+102 )
    bmp
  end

end

require_relative 'bookstatus/bestiary.rb'
require_relative 'bookstatus/itemary.rb'
require_relative 'bookstatus/skillary.rb'
require_relative 'bookstatus/traptionary.rb'

