# Window::TraptionaryStatus
#==============================================================================#
# ♥ Window::TraptionaryStatus
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/08/2012
# // • Data Modified : 01/08/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 01/08/2012 V1.0
#==============================================================================#
class Window::TraptionaryStatus < Window::BookStatus
  def init_members()
    super()
    @item = nil
    @character = Traptionary_Character.new()
    #@character.instance_variable_set( "@step_anime", true )
    @sprite = Sprite::RogueCharacter.new( nil, @character )
    @phases = [:on_trigger, :on_finish]
    @phase_index = 0
    @traps = []
  end
  def dispose
    @sprite.dispose
    super()
  end
  def viewport=(v)
    super(v)
    @sprite.viewport = v
  end
  def on_set_item( old_item, new_item )
    @character.set_trap( new_item.id )
    @character.set_sprite_effect_type( :appear )
  end
  def refresh()
    contents.clear()
    bmp = draw_book_border( 0, 28 )
    if @item
      gtrap = @character #@traps[@item.id]
      dy = 0
      draw_id_header( 0, 0, gtrap.id )
      dy += 24
      draw_horz_line( dy )
      @character.set_graphic( gtrap.character_name, gtrap.character_index )
      contents.font.set_style( :simple_black )
      #contents.font.size = Font.default_size - 8
      draw_text( 7, dy+7, bmp.width-14, 16, gtrap.name, 1 )
      contents.font.set_style( :default )
      draw_text( bmp.width+8, dy, contents.width-bmp.width-8, 20, "Class:", 0 )
      contents.font.set_style( :paper_text )
      contents.font.size = Font.default_size - 2
      contents.font.bold = true
      draw_text( bmp.width+64, dy+2, contents.width-bmp.width-64, 20, gtrap.class.name, 0 )
      contents.font.set_style( :default )
      draw_text( bmp.width+8, dy+24, contents.width-bmp.width-8, 20, "Level:", 0 )
      contents.font.set_style( :default )
      contents.font.size = Font.default_size - 2
      contents.font.bold = true
      draw_text( bmp.width+64, dy+26, contents.width-bmp.width-64, 20, gtrap.level, 0 )
      dy += 102
      draw_horz_line(dy)
      dy += 2
      contents.font.set_style( :default )
      draw_parameters( gtrap, 0, dy )
      dy += (24 * 8)
      draw_horz_line(dy)
      dy += 2
      contents.font.set_style( :simple_black )
      #contents.font.size = Font.default_size - 8
      text = convert_escape_characters(@item.description)
      pos = {:x => 0, :y => dy, :new_x => 0,
        :width => contents.width, :height => 20 }#calc_line_height(text)}
      process_character(text.slice!(0, 1), text, pos) until text.empty?
      #draw_text_ex( 0, dy, @item.description )
    end
    @character.sx = self.x + padding + ((bmp.width - 32) / 2)
    @character.sy = self.y + padding + ((bmp.height - 32) - 4) + 28
    bmp.dispose
    @phase_index = 0
    #@character.force_move_route( RPG::MoveRoute.new )
  end
  def draw_parameters(trap, x, y)
    8.times {|i| draw_actor_param(trap, x, y + line_height * i, i) }
  end
  def draw_actor_param( actor, x, y, param_id )
    contents.font.set_style( :paper_text )
    draw_text(x, y, contents.width, line_height, Vocab::param(param_id))
    contents.font.set_style( :default )
    draw_text(x + (contents.width-48), y, 36, line_height, actor.param(param_id), 2)
  end
  def update()
    super()
    unless @character.animating?
      @phase_index = (@phase_index + 1) % @phases.size
      #puts "Current Phase: #@phase_index #{@phases[@phase_index]}"
      case @phases[@phase_index]
      when :on_trigger
        @character.on_start_trigger()
      when :on_finish
        @character.on_finish_trigger()
      end #if @item
    end
    @character.update()
    @sprite.update()
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
