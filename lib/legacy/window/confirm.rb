class Window::Confirm < Shell::Command

  include Hazel::Shell::Addons::Background

  def initialize(x, y)
    super(x, y)
    @deco_title = add_decoration(Shell::Decoration::Title)
  end

  def set_title(title)
    @deco_title.set_text(title, 0)
  end

  def col_max
    2
  end

  def alignment
    1
  end

  def make_command_list
    add_command("OK",     :ok)
    add_command("Cancel", :cancel)
  end

  def update_help
    text =  case current_symbol
            when :ok     then "You will accept"
            when :cancel then "You will not accept"
            else              ""
            end
    @help_window.set_text(text)
  end

end
