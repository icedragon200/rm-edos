# Window::CraftOutput
class Window::CraftOutput < Window::Base

  class AddonBin_WCRO < WindowAddons::AddonBin[WindowAddons::Header]
    def header_text
      "Craft::Output"
    end
  end

  def addon_bin
    AddonBin_WCRO
  end

  def initialize( x, y )
    super( x, y, window_width, window_height )
    self.windowskin = Window::SkinCache.bitmap("window_craft")
    @item = nil
    refresh()
  end

  attr_reader :item

  def item=(n)
    if @item != n
      @item = n
      refresh()
    end
  end

  def clear
    @item = nil
    refresh()
  end

  def window_width
    256 + 20 + standard_padding * 2
  end

  def window_height
    36 + 40 + standard_padding * 2
  end

  def refresh()
    contents.clear
    bmp = Cache.system("craft_borders(window)")
    contents.blt( 2, 2, bmp, Rect.new( 0, 32*5, 32, 32 ) )
    dx = 2
    dy = 2
    item = @item
    contents.font.set_style( :simple_black )
    @artist.draw_item_icon( item, dx+4, dy+4 )
    dx += 32
    contents.font.size = Font.default_size - 6
    @artist.draw_box( dx, dy+14, contents.width-36, 18 )
    @artist.draw_text( dx, dy, contents.width-(dx), 12, item ? item.name : "" )
    dy += 12
    contents.font.size = Font.default_size - 8
    @artist.draw_text( dx+4, dy, contents.width, 12, item ? item.description : "" )
    dx = 2
    dy = 2+32+2
    cols = 4
    for i in 0...8
      @artist.draw_parameter( @battler, dx + ((i % cols) * 64), dy + ((i / cols) * 12), i )
    end
  end

end
