# Window::CursorInfo
#==============================================================================#
# ♥ Window::CursorInfo
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/26/2011
# // • Data Modified : 12/26/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/26/2011 V1.0
#
#==============================================================================#
class Window::CursorInfo < Window::Base

  def initialize(x, y, width, height)
    super(x, y, width, height)
    @cursor = _map.cursor
    self.windowskin = Window::SkinCache.bitmap("window_smallborder")
    self.arrows_visible = false
  end

  def _map
    return $game.rogue
  end

  def update()
    super()
    move(@cursor.screen_x + 16, @cursor.screen_y - 32, width, height) if @cursor.active
    if @cursor.active && (@last_x != @cursor.x ||
        @last_y != @cursor.y || @last_active != @cursor.active)
      self.z = 201
      crx, cry = @cursor.x, @cursor.y
      chs = _map.characters_xy( crx, cry ) + _map.items_xy( crx, cry )
      contents.clear()
      unless chs.empty?
        #open() unless open?
        start_open()
        c = chs[0]
        contents.font.set_style( :simple_black )
        #change_color( normal_color )
        #wd = contents.text_size(c.name).width
        #self.width = wd + standard_padding + 8
        @artist.draw_text( 4, 0, contents.width-8, 20, c.name )
        case c
        when Game::RogueCharacter
          @artist.draw_text( 4, 0, contents.width-8, 20, sprintf( "%s: %s", Vocab.level_a, c.level ), 2 )
          bw = (contents.width - 8) / 2
          @artist.draw_small_hp( c, 4, 16, bw )
          @artist.draw_small_mp( c, contents.width-4-bw, 16, bw )
        when Game::RogueItem
          @artist.draw_text( 4, 0, contents.width-8, 20, sprintf( "x: %s", c.item_number ), 2 )
        end
      else
        close() unless close? || opening_or_closing?
      end
      @last_x = crx
      @last_y = cry
    elsif !@cursor.active
      close() unless close? || opening_or_closing?
    end
    @last_active = @cursor.active
  end

  def standard_artist
    Artist::CursorInfo
  end

end

class Artist::CursorInfo < Artist

  def draw_small_hp( actor, x, y, width=32 )
    bw, bh = width, 6
    ext_draw_bar4(
      {
        :x      => x,
        :y      => y,
        :width  => bw,
        :height => bh,
        :rate   => actor.hp_rate,
      }.merge(DrawExt::HP1_BAR_COLORS)
    )
  end

  def draw_small_mp( actor, x, y, width=32 )
    bw, bh = width, 6
    ext_draw_bar4(
      {
        :x      => x,
        :y      => y,
        :width  => bw,
        :height => bh,
        :rate   => actor.mp_rate,
      }.merge(DrawExt::MP_BAR_COLORS)
    )
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
