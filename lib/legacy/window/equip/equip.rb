class Window::Equip < Window::Selectable

  include REI::Mixin::UnitHost

  def initialize( x, y, w=nil, h=nil )
    w ||= window_width
    h ||= window_height
    super(x,y,w,h)
    select(0)
  end

  def on_unit_change
    refresh4change
  end

  def col_max
    4
  end

  def spacing
    return 2
  end

  def item_width
    return 64 + 4#96 + 24 +4
  end

  def item_height
    return 64 + 4 #38 + 4
  end

  def window_height
    (item_height * 2) + standard_padding * 2
  end

  def window_width
    return ((item_width) * col_max) + (standard_padding * 2) + (spacing * 2)
  end

  def draw_item( index )
    rect = item_rect( index ).contract(anchor: 5, amount: 2)
    @artist.draw_entity_equip_at_ex(@unit.entity, index, rect, enable?(index))
  end

  def item(index=self.index)
    @unit ? @unit.entity.equips[index] : nil
  end

  def update
    super
    @item_window.slot_id = index if @item_window
  end

  def item_max
    @unit ? @unit.entity.equip_slots.size : 0
  end

  def enable?(index)
    @unit ? @unit.entity.equip_change_ok?(index) : false
  end

  def current_item_enabled?
    enable?(index)
  end

  def status_window=(status_window)
    @status_window = status_window
    call_update_help
  end

  def item_window=(item_window)
    @item_window = item_window
    update
  end

  def update_help
    super
    @help_window.set_item(item)
  end

  def equip_current(item)
    Sound.play_equip
    @unit.entity.change_equip(self.index,item)
    refresh4change()
  end

  def unequip_current
    equip_current(nil)
  end

  def optimize_equipments
    Sound.play_equip
    @unit.entity.optimize_equipments
    refresh4change
  end

  def clear_equipments
    @unit.entity.clear_equipments
    refresh4change
  end

  def refresh4change
    refresh
    @status_window.refresh if @status_window
    @item_window.refresh   if @item_window
    call_update_help
  end

end
