# Window::EquipItemStatus
# // 02/29/2012
# // 02/29/2012
class Window::EquipItemStatus < Window::Base

  def initialize(x, y, w=nil, h=nil)
    w ||= window_width
    h ||= window_height
    @item = default_item
    super(x,y,w,h)
    self.windowskin = Window::SkinCache.bitmap("window_smallborder")
  end

  def standard_padding
    10
  end

  def default_item
    nil
  end

  def window_width
    adjust_w4window(72)
  end

  def window_height
    fitting_height(8)
  end

  def line_height
    12
  end

  def reset_font_settings() # // Prevent Resetting . x .
  end

  def refresh()
    text = ""
    x, y = 0, 0
    dx, dy = 0, 0
    dh = line_height
    drawing_sandbox() do
      DB::Helper.item2param_texts(item).each_with_index do |a, i|
        text, = a
        dx = 0
        dy = y +(i*dh)
        enabled = a[1] > 0
        #puts text
        r = @artist.draw_parameter_text(dx,dy,i,enabled)
        r.offset!(anchor: 6, rate: 1.0)

        contents.font.set_style('simple_black')
        contents.font.bold = true
        contents.font.outline = false
        contents.font.name = ["ProggySmall"]
        @artist.draw_text_ex(r.x,r.y,text)
      end
    end # // Sandbox
  end

  attr_reader :item

  def item=(n)
    return if @item == n
    @item = n
    refresh()
  end

end
