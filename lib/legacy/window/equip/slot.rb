#
# EDOS/src/window/equip/slot.rb
#   dm 19/05/2013
# vr 1.0.0
class Window::EquipSlot < Window::Equip

  def initialize(*args, &block)
    super(*args, &block)
    select(-1)
  end

end
