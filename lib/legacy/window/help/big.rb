# Window::BigHelp
# // 03/01/2012
# // 03/01/2012
class Window::BigHelp < Window::Help2

  def initialize(x,y,w,h)
    w ||= window_width
    h ||= window_height
    super(x,y,w,h)
  end

  alias :refresh_h2 :refresh
  def refresh()
    item    = @item
    exitem  = ExDatabase.ex_equip_item?(item)

    dx, dy  = 0, 0
    dw = dh = contents.height

    rect  = Rect.new(dx,dy,dw,dh)

    rect2 = rect.dup
    rect2.contract!(anchor: MACL::Surface::ANCHOR_RIGHT, amount: rect2.width)
    rect2.width = contents.width - rect2.x
    rect2.height = 8 * 6
    rect2.x -= 1
    rect2.width += 1
    rect2.y2 = contents.height

    rect3 = rect2.contract(anchor: 5, amount: 4)

    rect4 = rect3.contract(anchor: 5, amount: 24)

    rect5 = Rect.new(0,dy+14,dh,24) # // Just the basis

    if(exitem)
      rect5.width = DrawExt.adjust_size4bar3(rect5.width,10,1,1)
      rect5.x = contents.width - rect5.width
      rect6 = rect5.dup
      rect6.y -= rect6.height
      rect6.height = 8
      rect6.y = contents.height - (6 * rect6.height) # // 6 Elements * 6px
      rect7 = Rect.new(rect2.x, rect2.y - 8, 56, 8)
      rect2.width -= rect6.width
    end

    art = artist
    merio = art.merio

    drawing_sandbox do
      contents.clear()
      merio.draw_dark_rect(rect)

      if(item)
        art.draw_icon_stretch(item.icon, rect, true)
        art.draw_item_name(item,rect3.x,dy,rect3.width,16)
        art.draw_horz_line(rect5.y,rect3.x,rect3.width)
        art.draw_item_description(item,rect3.x, rect3.y, rect3.width, 14)
        art.draw_item_parameters(item,rect4.x,dy+16,4)

        if(exitem)
          art.draw_item_exp(item,*rect5.to_a)
          art.draw_item_element_res(item, *rect6.to_a)
          art.draw_item_durability(item,rect7)
          drawing_sandbox() do
            contents.font.set_style('simple_black')

            rect_exp    = rect5
            rect_eleres = rect6
            rect_exp.height = rect_eleres.height = 12
            rect_exp.y -= rect_exp.height
            rect_eleres.y -= rect_eleres.height

            art.draw_text(rect_exp, "Experience", 1)
            art.draw_text(rect_eleres, "Element Residue", 1)
          end
        end

      end
    end # // Sandbox

    set_contents_fadein(force: true)

  end

end
