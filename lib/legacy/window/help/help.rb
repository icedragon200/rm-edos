#
# EDOS/src/windoe/help/help.rb
#   dc ??/??/201?
#   dm 17/06/2013
# vr 1.0.0
class Window::Help < Window::Base#Shell::Window#::Base

  include REI::Mixin::UnitHost

  attr_reader :item

  def initialize(x=0, y=0, width=Graphics.width, height=line_height+(standard_padding*2))
    super( x, y, width, height )
    @_def_align = 0
    #self.windowskin = Window::SkinCache.bitmap("window_console")
  end

  def standard_padding
    return 8
  end

  def set_text(text="", align=@_def_align, forced=false)
    if @text != text || @align != align || forced
      @text = text
      @align = align
      refresh()
    end
  end

  def clear
    set_text("")
  end

  def set_item(item)
    set_text(item ? item.description : "")
  end

  def refresh
    contents.clear
    contents.font.set_style('default')
    @artist.draw_text(0, 0, contents.width, contents.height, @text, @align)
  end

  def on_unit_change
    refresh
  end

end
