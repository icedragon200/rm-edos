class Window::Help2 < Window::Help

  def set_item( item )
    if @item != item
      @item = item
      refresh()
    end
  end

  def reset_font_settings()
    contents.font.set_style( :simple_black )
  end

  def calc_line_height(text, restore_font_size = true)
    return 12
  end

  alias :refresh_h1 :refresh
  def refresh()
    return refresh_h1 if @text
    contents.clear
    DrawExt.draw_box1(
      contents, contents.rect,
      Palette['brown1'], Palette['paper1'].hset(alpha: 198))

    item = @item
    contents.font.set_style( :simple_black )
    dx, dy = 0, 0

    artist do |art|
      bmp = Cache.system("help_itemborder(window)")
      contents.blt(dx, dy, bmp, bmp.rect)
      art.draw_item_icon( item, dx, dy+((contents.height-24)/2) )
      dx += 24
      contents.font.size = Font.default_size - 6
      if ExDatabase.equip_item?(item)
        art.draw_box( dx, dy+12, (contents.width-(4*64))-28, 12 )
      else
        art.draw_box( dx, dy+12, contents.width-28, 12 )
      end
      art.draw_item_name(item,dx,dy,contents.width-(dx))
      dy += 12
      art.draw_item_description(item,dx+4,dy,contents.width-28,14)
      dx = contents.width - (4*64)
      dy = 0
      art.draw_item_parameters(item,dx,dy,4)
      art.draw_item_exp(item,dx-130,dy,128,14)
      #art.draw_text_ex( dx+4, dy, item ? item.description : "" )
    end

    set_contents_fadein(force: true)
  end

  def clear
    @text = nil
    set_item(nil)
  end

  def update
    super
    update_contents_fx
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
