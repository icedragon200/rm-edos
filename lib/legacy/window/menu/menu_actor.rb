class Window::MenuUnit < Window::MenuStatus

  def initialize
    super(0, 0)
    self.visible = false
  end

  def process_ok
    party.target_unit = party.members[index] unless @cursor_all
    call_ok_handler
  end

  def select_last
    select($game.party.target_unit.entity.index || 0)
  end

  def select_for_item(item)
    @cursor_fix = item.for_user?
    @cursor_all = item.for_all?
    if @cursor_fix
      select(party.menu_unit.index)
    elsif @cursor_all
      select(0)
    else
      select_last
    end
  end

end
