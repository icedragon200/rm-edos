#
# EDOS/src/window/menu/menu_command.rb
#   by IceDragon
#   dc 02/07/2013
#   dm 02/07/2013
# vr 1.0.0
class Window::MenuCommand < Shell::Command

  include Shell::Addons::Background

  @@last_command_symbol = nil

  def initialize(x=0, y=0)
    super(x,y)
    select_last
    self.padding_bottom = 0
  end

  def standard_padding
    0
  end

  def item_height
    Metric.ui_element
  end

  def window_width
    return Graphics.width
    #(item_width + spacing) * col_max + standard_padding * 2
  end

  def window_height
    item_height + standard_padding * 2
  end

  def visible_line_number
    n = super
    return n + n % col_max
  end

  def make_command_list
    add_main_commands
    #add_formation_command
    add_original_commands
    add_save_command
    add_game_end_command
  end

  def add_main_commands
    add_command(Vocab::item,   :item,   main_commands_enabled)
    add_command(Vocab::skill,  :skill,  main_commands_enabled)
    add_command(Vocab::equip,  :equip,  main_commands_enabled)

    add_command("Arts",        :arts,   main_commands_enabled)
    add_command("Eq. Skill",   :equip_skill, main_commands_enabled)

    add_command(Vocab::status, :status, main_commands_enabled)
  end

  def add_formation_command
    add_command(Vocab::formation, :formation, formation_enabled)
  end

  def add_original_commands
  end

  def add_save_command
    add_command(Vocab::save, :save, save_enabled)
  end

  def add_game_end_command
    add_command(Vocab::game_end, :game_end)
  end

  def main_commands_enabled
    $game.party.exists
  end

  def formation_enabled
    $game.party.members.size >= 2 && !$game.system.formation_disabled
  end

  def save_enabled
    !$game.system.save_disabled
  end

  def process_ok
    @@last_command_symbol = current_symbol
    super
  end

  def select_last
    select_symbol(@@last_command_symbol)
  end

  def draw_item(index)
    rect = item_rect(index)
    com = @list[index]
    rid = DB.rogue_sym2id(com[:symbol])

    art = artist
    merio = contents.merio
    ### draw
    merio.draw_light_label(rect, com[:name], 1)
  end

  #def item_rect(index)
  #  rect = super(index)
  #  rect.y += spacing * (index / col_max)
  #  return rect
  #end

  def col_max
    return item_max.max(1)
  end

  def self.init_command_position
    @@last_command_symbol = nil
  end

end
