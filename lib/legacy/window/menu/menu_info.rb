class Window::MenuInfo < Shell::Window

  include Shell::Addons::Background

  def initialize(x, y, w, h)
    super(x, y, w, h)
    refresh
  end

  def standard_padding
    0
  end

  def refresh
    (bmp = contents).clear
    artist do |art|
      contents.merio.font_config(:light, :default, :enb)
      art.draw_currency_value($game.party.gold, Vocab.currency_unit,
                              0, 0, bmp.width - Metric.contract)
      refresh_playtime()
    end
  end

  def refresh_playtime
    crect = Rect.new(0, 0, contents.width - 192, contents.height)
    rect = crect.contract(anchor: MACL::Surface::ANCHOR_MIDDLE_CENTER,
                          amount: Metric.contract)
    artist do |art|
      contents.clear_rect(rect)
      #art.change_color(art.normal_color)
      contents.merio.font_config(:light, :default, :enb)
      art.draw_text(rect, @last_playtime)
    end
  end

  def update
    super

    if @last_playtime != (ply = $game.system.playtime_s)
      @last_playtime = ply.dup
      refresh_playtime
    end
  end

end
