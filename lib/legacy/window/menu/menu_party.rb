#
# EDOS/src/window/menu/menu_party.rb
#   dm 28/06/2013
# vr 1.0.0
class Window::MenuParty < Shell::WindowSelectable

  ### mixins
  #include Hazel::Shell::Addons::Background

  def initialize(x=0, y=0, w=window_width, h=window_height)
    super(x, y, w, h)
    @pending_index = 0
    refresh
    #activate
  end

  def make_decorations
    add_decoration(:scroll_bar)
  end

  def standard_padding
    0
  end

  def window_width
    Graphics.width - Metric.ui_element
  end

  def window_height
    #h = ((Graphics.height - 256) / item_height) * item_height
    h = item_height * 3
    return h + (standard_padding * 2)
  end

  def party
    $game.party
  end

  def members
    party.members
  end

  def item_max
    members.size
  end

  def item_width
    window_width - (standard_padding * 2)
  end

  def item_height
    Metric.ui_element * 2
  end

  def refresh
    super
    set_contents_fadein(force: true)
  end

  def draw_item(index)
    unit      = members[index] # REI::Unit

    rect = item_rect(index)
    #rect.contract!(anchor: 5, amount: Metric.contract)

    draw_status_style3(unit, rect)
  end

  def draw_status_style1(unit, rect)
    entity    = unit.entity    # REI::Entity
    character = unit.character # REI::Character
    art = artist # Artist

    contents.merio.draw_light_rect(rect)

    #rect.contract!(anchor: 5, amount: 1)
    #draw_text( rect.x + 4, rect.y + 4, rect.width, 24, actor.name )
    #draw_horz_line( rect.y + 24, rect.x + 4, rect.width-8, 2 )
    hw = rect.width / 2
    bw = rect.width / 4
    dx = rect.x
    dy = rect.y
    wd = art.draw_tiny_character(character: character, x: dx + 16, y: dy + 32).width

    contents.font.set_style('default')
    contents.merio.font_size = :small
    art.draw_unit_name(unit, [dx + wd, dy, 128, line_height])
    art.draw_entity_level(entity, [hw - 56, dy, 56, line_height])
    art.draw_entity_exp(entity, Rect.new(hw - (56 + 72), dy + 4, 72, 16))
    art.draw_entity_hp(entity, Rect.new(hw + (bw * 0), dy+4, bw, 16))
    art.draw_entity_mp(entity, Rect.new(hw + (bw * 1), dy+4, bw, 16))

    art.draw_horz_line( dy + 20, rect.x + 2, rect.width-4, 2 )
    dy += 24
    art.draw_entity_icons(entity, dx, dy, rect.width / 4 )
    dx = (rect.width / 4)
    contents.font.size = Font.default_size
    for i in 0...6
      art.draw_parameter(entity, dx + ((i % 8) * 64), dy + ((i / 8) * 12), 2+i)
    end
  end

  def draw_status_style2(unit, rect)
    entity    = unit.entity    # REI::Entity
    character = unit.character # REI::Character
    art = artist # Artist

    name_rect = rect.dup
    name_rect.width = 128
    name_rect.height = line_height
    name_rect.x += 32

    level_rect = name_rect.dup
      level_rect.y += level_rect.height
      level_rect.width = 96

    exp_rect   = MACL::Surface::Tool::squarify(rect)
      exp_rect.contract!(anchor: 5, amount: Metric.contract)
      exp_rect.x = name_rect.x2
      exp_rect.x += exp_rect.width
    hp_rect    = exp_rect.dup
      hp_rect.x += hp_rect.width
    mp_rect    = hp_rect.dup
      mp_rect.x += mp_rect.width

    ax = 4
    ay = (rect.height - 32) / 2

    # Background
    DrawExt.draw_box1(contents, rect,
                      [Palette['droid_dark_ui_enb'],
                      Palette['merio_blue']])
    # Unit Character Graphic
    art.draw_unit_graphic(unit, rect.x + 16 + ax, rect.y + 32 + ay)

    art.draw_unit_name(unit, name_rect)
    art.draw_entity_level(entity, level_rect)
    art.draw_entity_exp(entity,   exp_rect)
    art.draw_entity_hp(entity,    hp_rect)
    art.draw_entity_mp(entity,    mp_rect)
  end

  def draw_status_style3(unit, rect)
    ### constants
    sprd = Lacio::Spreadsheet
    label_anchor = MACL::Surface::ALIGN_CENTER
    ### other
    entity    = unit.entity    # REI::Entity
    character = unit.character # REI::Character
    ### spaces
    art = artist # Artist
    merio = art.merio
    font = contents.font
    ### variables
    nh = Metric.ui_element_mid
    rw = rect.width / 5 # row width
    rh = Metric.ui_element_sml    # row height
    ## rects
    rect_chunk1 = Rect.new(rect.x, rect.y, rw, rh)
    rect_chunk2 = rect_chunk1.step(6)
    rect_chunk3 = rect_chunk2.step(6)
    rect_chunk4 = rect_chunk3.step(6)
    rect_chunk5 = rect_chunk4.step(6)
    rect_chunk6 = Rect.new(rect_chunk2.x, rect.y + rect_chunk2.height * 4,
                           rect_chunk2.width * 2, rect.height - rect_chunk2.height * 4)
    rect_chunk7 = rect_chunk6.step(6)
    ## chunks:cellbanks

    ### drawing
    merio.snapshot do
      # setup
      merio.font_size = :micro
      ##
      # Name Block
      merio.snapshot do
        r = rect_chunk1.dup
        r2 = r.dup #r.contract(anchor: 5, amount: Metric.contract)
        rect_name = r2.dup
        rect_name.height *= 1.75
        rect_title = rect_name.dup
        rect_title.y += rect_name.height
        r.height *= 3
        rect_title.height = (r2.height * 3) - rect_name.height
        #
        er = Rect.new(r.x, r.y + r.height, r.width / 6, (rect.height - r.height))
        #merio.active_state = false
        #merio.draw_dark_rect(r)
        merio.font_size = :xlarge
        merio.font_config(:light, nil, :enb)
        #art.draw_text(rect_name, entity.name, 0)
        merio.snapshot do
          merio.change_main_palette("element#{entity.element_id}")
          merio.draw_light_label(rect_name, entity.name, 0)
        end
        merio.font_size = :default
        merio.font_config(:light, nil, :enb)
        #art.draw_text(rect_title, entity.title, 2)
        merio.draw_dark_label(rect_title, entity.title, 2)
        # element block
        for i in 0...6
          vertical = true
          align = 2
          ei = i + 1
          rate  = entity.element_rate(ei)
          mrate = (rate / 2.0)
          c     = art.element_color(ei)
          c     = c.add((mrate - 1.0).clamp(0.0, 1.0))
          colors = DrawExt.quick_bar_colors(c, 1)
          _,gr = art.draw_gauge_ext(er.step(6, i), mrate, colors, vertical, align)
          gr.height = 8
          merio.font_size = :nano
          merio.draw_dark_label(gr, rate.round(1).to_s)
        end
      end
      ##
      # Other Blocks
      merio.snapshot do
        merio.align_label = label_anchor
        ### contents
        ## info
        merio.snapshot do
          cb1 = sprd.chunk(rect_chunk2) do |spd|
            spd.row.col(1.0)       # Title
            spd.row.col(0.25, nil) # Element
            spd.row.col(0.25, nil) # Job
            spd.row.col(0.25, nil) # Level
          end
          # header
          merio.font_size = :medium
          # header
          merio.draw_light_label(cb1[0, 0], "Info")
          # labels
          merio.font_size = :micro
          merio.draw_light_label(cb1[1, 0], "Elem.")
          merio.draw_light_label(cb1[2, 0], "Job")
          merio.draw_light_label(cb1[3, 0], "Lvl")
          # content
          merio.snapshot do
            merio.change_main_palette("element#{entity.element.id}_i")
            merio.draw_dark_label(cb1[1, 1], entity.element.name)
          end
          merio.draw_dark_label(cb1[2, 1], entity.class.name)
          merio.draw_dark_label(cb1[3, 1], entity.level)
        end

        ## status
        merio.snapshot do
          cb2 = sprd.chunk(rect_chunk3) do |spd|
            spd.row.col(1.0)        # Title
            spd.row.col(0.25, nil) # HP
            spd.row.col(0.25, nil) # MP
            spd.row.col(0.25, nil) # XP
          end
          # header
          merio.font_size = :medium
          # header
          merio.draw_light_label(cb2[0, 0], "Status")
          # labels
          merio.font_size = :micro
          merio.draw_light_label(cb2[1, 0], "HP")
          merio.draw_light_label(cb2[2, 0], "MP")
          merio.draw_light_label(cb2[3, 0], "EXP")
          # content
          merio.global_expand do
            art.draw_entity_hp(entity, cb2[1, 1], true)
            art.draw_entity_mp(entity, cb2[2, 1], true)
            art.draw_entity_exp(entity, cb2[3, 1], true)
          end
        end

        ## parameters
        merio.snapshot do
          # cellbank
          cb3 = sprd.chunk(rect_chunk4) do |spd|
            spd.row.col(1.0) # Title
            # each row will be halved for key/value pairing
            spd.row.cols(2)  # STR, VIT
            spd.row.cols(2)  # INT, MEN
            spd.row.cols(2)  # AGI, LUK
          end
          # settings
          merio.font_size = :medium
          # header
          merio.draw_light_label(cb3[0, 0], "Parameters")
          # settings
          merio.font_size = :micro
          # content
          for iy in 0...3
            for ix in 0...2
              ii = iy * 2 + ix
              str = Vocab.param_a(ii + 2)
              stat = entity.param(ii + 2)
              merio.draw_pair_fmt(cb3[1 + iy, ix], str, stat, "%03d")
            end
          end
        end

        ## arts
        merio.snapshot do
          # cellbank
          cb4 = sprd.chunk(rect_chunk5) do |spd|
            spd.row.col(1.0) # Title
            spd.row.col(nil) # Art 1
            spd.row.col(nil) # Art 2
            spd.row.col(nil) # Art 3
          end
          # settings
          merio.font_size = :medium
          # header
          merio.draw_light_label(cb4[0, 0], "Arts")
          # settings
          merio.font_size = :micro
          # content
          entity.arts.each_with_index do |art, i|
            str = art ? art.name : '----'
            merio.draw_dark_label(cb4[1 + i, 0], str)
          end
        end

        ## equipment
        merio.snapshot do
          # cellbank
          cb5 = sprd.chunk(rect_chunk6) do |spd|
            spd.row.col(0.25, nil) # Title
          end
          # settings
          merio.font_size = :medium
          # header
          merio.snapshot do
            merio.change_main_palette('element0')
            merio.draw_light_label(cb5[0, 0], "Equips")
          end
          # content
          merio.draw_dark_rect(cb5[0, 1])
          sz = entity.equips.size
          cr = cb5[0, 1]
          r = Rect.new(cr.x, cr.y, 24, 24)
          entity.equips.each_with_index do |eq, i|
            orr = art.draw_item_icon(eq, r.x, r.y, true)
            r.step!(6)
          end
        end

        ## states
        merio.snapshot do
          # cellbank
          cb6 = sprd.chunk(rect_chunk7) do |spd|
            spd.row.col(0.25, nil) # Title
          end
          # settings
          merio.font_size = :medium
          # header
          merio.draw_light_label(cb6[0, 0], "States")
          # content
          merio.draw_dark_rect(cb6[0, 1])
          sz = entity.states.size
          cr = cb6[0, 1]
          r = Rect.new(cr.x, cr.y, 24, 24)
          entity.states.each_with_index do |st, i|
            orr = art.draw_item_icon(st, r.x, r.y, true)
            r.step!(6)
          end
        end

      end
    end
  end

  def process_ok
    super
    party.menu_unit = party.members[index]
  end

  def select_last
    if unit = party.menu_unit
      return unit.entity.index || 0
    end
    return 0
  end

  attr_reader :pending_index

  def pending_index=(index)
    last_pending_index = @pending_index
    @pending_index = index
    redraw_item(@pending_index)
    redraw_item(last_pending_index)
  end

  def update
    super
    update_contents_fx
  end

end