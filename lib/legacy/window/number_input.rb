require File.join(File.dirname(__FILE__), 'number_input_base')

class Window::NumberInput < Window::NumberInputBase

  attr_accessor :message_window

  def initialize(message_window)
    @message_window = message_window
    super(0, 0)
  end

  def start
    @digits_max = $game.message.num_input_digits_max
    @number = $game.variables[$game.message.num_input_variable_id]
    super()
  end

  def update_placement
    self.width = @digits_max * 20 + padding * 2
    self.height = fitting_height(1)
    self.x = (Graphics.width - width) / 2
    if @message_window.y >= Graphics.height / 2
      self.y = @message_window.y - height - 8
    else
      self.y = @message_window.y + @message_window.height + 8
    end
  end

  def process_ok
    Sound.play_ok
    $game.variables[$game.message.num_input_variable_id] = @number
    deactivate
    close
  end

  def process_cancel
  end

end
