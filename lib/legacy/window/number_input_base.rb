class Window::NumberInputBase < Window::Base

  attr_accessor :number, :digits_max

  def initialize(x, y)
    super(x, y, 0, 0)
    @number = 0
    @digits_max = 1
    @index = 0
    self.openness = 0
    deactivate
  end

  def start
    @number = [[@number, 0].max, 10 ** @digits_max - 1].min
    @index = 0
    update_placement
    create_contents
    refresh
    open
    activate

    return self
  end

  def update_placement
    self.width = @digits_max * 20 + padding * 2
    self.height = fitting_height(1)
  end

  def cursor_right(wrap)
    if @index < @digits_max - 1 || wrap
      @index = (@index + 1) % @digits_max
    end
  end

  def cursor_left(wrap)
    if @index > 0 || wrap
      @index = (@index + @digits_max - 1) % @digits_max
    end
  end

  def update
    super
    process_cursor_move
    process_digit_change
    process_handling
    update_cursor
  end

  def process_cursor_move
    return unless active
    last_index = @index
    cursor_right(Input.trigger?(:RIGHT)) if Input.repeat?(:RIGHT)
    cursor_left (Input.trigger?(:LEFT))  if Input.repeat?(:LEFT)
    Sound.play_cursor if @index != last_index
  end

  def process_digit_change
    return unless active
    if Input.repeat?(:UP) || Input.repeat?(:DOWN)
      Sound.play_cursor
      place = 10 ** (@digits_max - 1 - @index)
      n = @number / place % 10
      @number -= n * place
      n = (n + 1) % 10 if Input.repeat?(:UP)
      n = (n + 9) % 10 if Input.repeat?(:DOWN)
      @number += n * place
      refresh
    end
  end

  def process_handling
    return unless active
    return process_ok     if Input.trigger?(:C)
    return process_cancel if Input.trigger?(:B)
  end

  def process_ok
    Sound.play_ok
    deactivate
    close
  end

  def process_cancel
    Sound.play_cancel
    deactivate
    close
  end

  def item_rect(index)
    Rect.new(index * 20, 0, 20, line_height)
  end

  def refresh
    contents.clear
    @artist.change_color(@artist.normal_color)

    s = sprintf("%0*d", @digits_max, @number)

    @digits_max.times do |i|
      rect = item_rect(i)
      rect.x += 1
      @artist.draw_text(rect, s[i,1], 1)
    end
  end

  def update_cursor
    cursor_rect.set(item_rect(@index))
  end

end
