# Window::Options
#==============================================================================#
# ♥ Window::Options
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/29/2011
# // • Data Modified : 12/29/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/29/2011 V1.0
#
#==============================================================================#
class Window::Options < Window::Selectable

  class AddonBin_WO < WindowAddons::AddonBin[WindowAddons::Header]
    def header_text
      "Options"
    end
  end

  def addon_bin
    AddonBin_WO
  end

  MSG_SLIDER_ID = 0
  BGM_SLIDER_ID = 1
  BGS_SLIDER_ID = 2
  SE_SLIDER_ID  = 3
  SLIDER_NAME   = ["Message Speed", "Music", "Back.Sounds", "Sound Effects"]
  SLIDER_ICON   = [4, 1, 2, 3]

  def initialize
    @sliders = []
    super( 0, 0, window_width, window_height )
    self.windowskin = Window::SkinCache.bitmap("window_console")
    activate()
    refresh()
    self.index = 0
    handle = -> do
      @last_rate = @sliders[index].rate
      @sliders[index].activate
    end
    set_handler(:ok, handle)
  end

  def x=( x )
    super( x )
    refresh_x()
  end

  def y=( y )
    super( y )
    refresh_y
  end

  def z=( z )
    super( z )
    refresh_z
  end

  def viewport=( viewport )
    super( viewport )
    refresh_viewport()
  end

  def visible=( visible )
    super( visible )
    refresh_visible()
  end

  def openness=( openness )
    super( openness )
    refresh_visible()
  end

  def refresh_x
    @sliders.each_with_index { |s, i|
      s.x = contents_x + item_rect(i).contract(anchor: 5, amount: 2).x
    }
  end

  def refresh_y
    @sliders.each_with_index { |s, i|
      s.y = contents_y + item_rect(i).contract(anchor: 5, amount: 2).y + 14
    }
  end

  def refresh_z
    @sliders.each_with_index { |s, i|
      s.z = self.z + 1
    }
  end

  def refresh_xyz
    @sliders.each_with_index { |s, i|
      r = item_rect(i).contract(anchor: 5, amount: 2)
      s.x = contents_x + r.x
      s.y = contents_y + r.y + 14
      s.z = self.z + 1
    }
  end

  def refresh_visible()
    @sliders.each { |s| s.visible = self.visible && self.openness > 128 }
  end

  def refresh_viewport
    @sliders.each { |s| s.viewport = self.viewport }
  end

  def window_width()
    item_width + standard_padding * 2
  end
  def window_height()
    item_max * item_height + standard_padding * 2
  end
  def item_max
    4
  end
  def col_max
    1
  end
  def item_width
    200 #+ 48
  end
  def item_height
    24 + 14 + 4
  end
  def on_close()
    update_settings()
  end
  def update_settings
    $game.settings.set_bgm_volume_rate( @sliders[BGM_SLIDER_ID].rate )
    $game.settings.set_bgs_volume_rate( @sliders[BGS_SLIDER_ID].rate )
    $game.settings.set_se_volume_rate( @sliders[SE_SLIDER_ID].rate )
    $game.settings.set_message_speed_rate( @sliders[MSG_SLIDER_ID].rate )
    RPG::BGM.last.play
    RPG::BGS.last.play
  end
  def refresh()
    @sliders = []
    super()
    @sliders[BGM_SLIDER_ID].rate = $game.settings.bgm_volume_rate
    @sliders[BGM_SLIDER_ID].bar_color = Palette.sym_color( :sys_blue ).lighten( 0.2 )
    @sliders[BGS_SLIDER_ID].rate = $game.settings.bgs_volume_rate
    @sliders[BGS_SLIDER_ID].bar_color = Palette.sym_color( :sys_green ).lighten( 0.2 )
    @sliders[SE_SLIDER_ID].rate = $game.settings.se_volume_rate
    @sliders[SE_SLIDER_ID].bar_color = Palette.sym_color( :sys_red ).lighten( 0.2 )
    @sliders[MSG_SLIDER_ID].rate = $game.settings.message_speed_rate
    @sliders[MSG_SLIDER_ID].bar_color = Palette.sym_color( :sys_orange )
    @sliders[MSG_SLIDER_ID].min = 0.1
    pc = proc { @sliders[index].deactivate; activate; @last_rate=nil }
    pc2= proc { @sliders[index].rate = @last_rate; @sliders[index].deactivate; activate;@last_rate=nil }
    @sliders.each { |s|
      s.enable_input
      s.set_handler( :ok, pc )
      s.set_handler( :cancel, pc2 )
      s.set_handler( :on_change, method(:update_settings) )
    }
    refresh_xyz()
    #refresh_values()
  end
  def refresh_values
    @sliders.size.times { |i| refresh_value( i ) }
  end
  def refresh_value( i )
    r = item_rect(i) ; r.x = r.width - 32 ; r.width = 48 ; r.x -= 20
    r.y += r.height - 24 ; r.height = 24
    draw_slider_value( r, (100 * @sliders[i].rate).to_i )
  end
  def draw_item( i )
    rect = item_rect( i ).contract( 2 )
    @sliders[i] = GUIExt::Slider.new(
      self.viewport, :horz, 196,
      rect.x+standard_padding, rect.y+standard_padding+14, self.z + 1 )
    @sliders[i].bar_color = Color.new( 241, 194, 80 )
    @artist.gui_draw_option_header(
      :x     => rect.x+1,
      :y     => rect.y,
      :width => 192
    )
    contents.font.set_style('window_header2')
    @artist.draw_icon_sys(SLIDER_ICON[i],rect.x+28,rect.y+4)
    @artist.draw_text(rect.x+38,rect.y+1,172,14,SLIDER_NAME[i],0)
    #bmp = Cache.system("Option_Headers(Window)")
    #contents.blt( rect.x+1, rect.y, bmp, Rect.new(0, (SLIDER_INDEX[i])*14, bmp.width, 14) )
  end
  def draw_slider_value( rect, value )
    contents.clear_rect( rect )
    @artist.draw_text( rect, value, 2 )
  end
  def update()
    super()
    #t = 120.0
    @sliders.each_with_index { |s, i|
      s.update
    }
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
