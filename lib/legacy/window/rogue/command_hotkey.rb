# Window::RogueHotkeyCommand
# // 03/05/2012
# // 03/05/2012
class Window::RogueHotkeyCommand < Window::RogueObjCommand
  class AddonBin_WRHC < AddonBin_WRC
    def header_text
      return "Hotkey Options"
    end
  end  
  def addon_bin
    AddonBin_WRHC
  end
  def make_command_list()
    add_command("Change", :change)
    add_command("Remove", :remove)
  end  
  def col_max 
    2
  end 
  def item_width
    128
  end  
end  
