# Window::RogueObjCommand
#==============================================================================#
# ♥ Window::RogueCommand
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 01/02/2012
# // • Data Modified : 01/02/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::RogueObjCommand < Window::RogueCommand
  attr_reader :item
  def item=( new_item )
    if @item != new_item
      @item = new_item
      make_command_list()
    end  
  end
  attr_reader :battler
  def battler=( new_battler )
    return if(@battler == new_battler)
    @battler = new_battler
    make_command_list()
  end
  def actor();battler();end
  def actor=(n);self.battler=n;end  
  attr_reader :item_window
  def item_window=(n)
    @item_window = n
  end  
  def set_and_refresh( bat, item )
    @battler = bat
    @item = item
    make_command_list()
    refresh()
  end  
  def item_can?( item, op )
    return @battler ? @battler.item_can?( item, op ) : false
  end 
end  
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
