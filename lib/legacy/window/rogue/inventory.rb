# Window::RogueInventory
#==============================================================================#
# ♥ Window::RogueInventory
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/29/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::RogueInventory < Window::Inventory
  AddonBin_WRI = WindowAddons::AddonBin[WindowAddons::ScrollBar,WindowAddons::Header]
  class AddonBin_WRI
    def header_text
      return "Inventory"
    end
    def header_cube
      (c=super).hset(self.x+(self.width-c.width)/2,self.open_y-(14+40))
    end
  end
  def addon_bin
    AddonBin_WRI
  end
  def col_max
    8
  end
  def include?(item)
    return true if item == nil
    return false unless ExDatabase.item?(item)
    return true
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
