# Window::RogueLog
# // 02/05/2012
# // 02/05/2012
class Artist::RogueLog < Artist
  def reset_font_settings
    change_color(normal_color)
    contents.font.size    = 12
    contents.font.outline = true
    contents.font.bold    = false
    contents.font.italic  = false
  end
end  
class Window::RogueLog < Window::BattleLog 
  #def initialize(*args,&block)
  #  super
  #  self.x = 0
  #end  
  def standard_artist
    Artist::RogueLog
  end  
  
  def window_width
    (Graphics.playwidth / 2.7).to_i
  end
  def line_height
    14
  end  
  def update()
    super()
    self.y = Graphics.playheight - ((line_number + 1) * line_height)
    @back_sprite.y = self.y
  end  
end  
