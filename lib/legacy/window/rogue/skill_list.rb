# Window::RogueSkillList
#==============================================================================#
# ♥ Window::RogueSkillList
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/29/2011
# // • Data Modified : 12/30/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/30/2011 V1.0
#
#==============================================================================#
class Window::RogueSkillList < Window::SkillList
  AddonBin_WRSL = WindowAddons::AddonBin[WindowAddons::ScrollBar,WindowAddons::Header]
  class AddonBin_WRSL
    def header_text
      return "Magic"
    end
    def header_cube
      (c=super).hset(self.x+(self.width-c.width)/2,self.open_y-(14+40))
    end
  end
  def addon_bin
    AddonBin_WRSL
  end
end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
