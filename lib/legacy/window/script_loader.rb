#
# EDOS/src/window/script_loader.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
class Window::ScriptLoader < Shell::WindowSelectable

  include Hazel::Shell::Addons::Background

  def initialize
    @list = Dir.glob("./scripts/*.rb") - ['.', '..']
    @list.sort!

    super(0, 0, Graphics.width * 0.7, Graphics.height * 0.6)

    refresh
  end

  def current_item
    return @list[@index]
  end

  def item_max
    @list.size
  end

  def line_height
    14
  end

  def draw_item(index)
    artist.merio.snapshot do |merio|
      merio.font_config(contents.font, :oqlight, :default, :enb)
      artist.draw_text(item_rect_for_text(index), @list[index], 0)
    end
  end

end
