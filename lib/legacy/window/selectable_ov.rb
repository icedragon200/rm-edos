# Window::Selectable (Overwrites)
#==============================================================================#
# ♥ Window::Selectable (Overwrites)
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/13/2011
# // • Data Modified : 01/26/2012
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#==============================================================================#
class Window::Selectable < Window::Base

  def pred_index(wrap=true)
    self.index = self.index.pred
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end

  def succ_index(wrap=true)
    self.index = self.index.succ
    self.index = wrap ? self.index.modulo(item_max) : self.index.clamp(0,item_max-1)
  end

  def process_cursor_move()
    return unless cursor_movable? && !win_busy?
    last_index = @index
    cursor_down (Input.trigger?(:DOWN))  if Input.repeat?(:DOWN)
    cursor_up   (Input.trigger?(:UP))    if Input.repeat?(:UP)
    cursor_right(Input.trigger?(:RIGHT)) if Input.repeat?(:RIGHT)
    cursor_left (Input.trigger?(:LEFT))  if Input.repeat?(:LEFT)
    cursor_pagedown   if !handle?(:pagedown) && Input.trigger?(:R)
    cursor_pageup     if !handle?(:pageup)   && Input.trigger?(:L)
    Sound.play_cursor if @index != last_index
  end

  def process_handling()
    return unless open? && active && !win_busy?
    return process_ok       if ok_enabled?        && Input.trigger?(:C)
    return process_cancel   if cancel_enabled?    && Input.trigger?(:B)
    return process_pagedown if handle?(:pagedown) && Input.trigger?(:R)
    return process_pageup   if handle?(:pageup)   && Input.trigger?(:L)
  end

  def row_index()
    return (index / col_max)
  end

  include Mixin::SmoothCursor
  alias :update_cursor :smooth_update_cursor # overwrite original

  remove_method :process_handling
  remove_method :process_cursor_move

  include Mixin::MouseSelectable

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
