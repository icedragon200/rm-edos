#
#
#
class Window::EquipStatus < Window::RogueStatus

  def standard_draw_mode()
    3
  end

  def set_temp_actor(temp_actor)
    return if @temp_actor == temp_actor
    @temp_actor = temp_actor
    refresh
  end

end
