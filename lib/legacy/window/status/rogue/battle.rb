class Window::RogueStatus

  # // Battle Status
  def refresh_mode0(con_rect)
    return unless @unit
    entity = @unit.entity
    art = artist

    hw = con_rect.width / 2
    bw = con_rect.width / 4
    dx = con_rect.x
    dy = con_rect.y
    wd = art.draw_entity_element(entity, dx, dy, true).width
    contents.font.set_style( :default )
    art.draw_actor_name( entity, dx + wd, dy )
    art.draw_actor_level( entity, hw - 56, dy )

    rect_exp = Rect.new(hw - (56 + 72), dy + 4, 72, 16)
    rect_hp = Rect.new(hw + (bw * 0), dy + 4, bw, 16)
    rect_mp = Rect.new(hw + (bw * 1), dy + 4, bw / 2, 16)
    rect_wt = Rect.new(hw + (bw * 1) + (bw / 2), dy + 4, bw / 2, 16)

    art.draw_entity_exp(entity, rect_exp)
    art.draw_entity_hp(entity, rect_hp)
    art.draw_entity_mp(entity, rect_mp)
    art.draw_entity_wt(entity, rect_wt)

    dy += line_height
    art.draw_horz_line( dy )
    art.draw_actor_icons( entity, dx, dy, bw )
    dx = con_rect.x + bw
    contents.font.size = Font.default_size
    for i in 0...6
      art.draw_parameter( entity, dx+((i%8)*64), dy+((i/8)*12), 2+i )
    end
  end

end
