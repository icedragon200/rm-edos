class Window::RogueStatus

  # // Full Status
  def refresh_mode2(con_rect)
    return if @unit.nil?
    entity    = @unit.entity
    character = @unit.character
    art = artist

    dx, dy = con_rect.x, con_rect.y
    cw2 = con_rect.width / 2
    cw4 = con_rect.width / 4
    cw8 = con_rect.width / 8

    drawing_sandbox do
      r = art.draw_entity_element(entity, dx, dy)
      dx += 40
      #draw_actor_name( entity, dx, dy )
      contents.font.set_style('simple_black')

      clsname = "%s%s: %s" % [Vocab.level_a, entity.level, entity.class.name]

      art.draw_fract(dx,dy,entity.name,clsname,96,r.height/2)

      dx = con_rect.x + cw2
      dx += art.draw_element_icon(DB.element_id(:hp),dx,dy).width
      dx += art.draw_fract(dx, dy, entity.hp, entity.mhp,32,r.height/2).width+4
      dx += art.draw_element_icon(DB.element_id(:mp),dx,dy).width
      dx += art.draw_fract(dx, dy, entity.mp, entity.mmp,32,r.height/2).width+4
      dx += art.draw_element_icon(DB.element_id(:wt),dx,dy).width

      w, h = art.draw_fract(
        dx, dy, entity.wt, entity.mwt,32,r.height/2).to_a[2..3]

      dy = dy + h + 2
      art.draw_horz_line(dy)

      dy += 2
      eqs = entity.equips
      dx = con_rect.x
      cols = 4
      for i in 0...eqs.size
        art.draw_entity_equip_at_ex(
          entity, i, Rect.new(
            dx + ((i % cols) * 64), dy + ((i / cols) * 64), 64, 64))
      end

      sks = entity.skills
      skill, rect, set = nil, nil, nil

      dy2 = dy + (4 * 42)
      for i in 0...sks.size
        skill = sks[i]
        rect  = Rect.new(dx,dy2+(i*24),cw4-8,24)
        set   = entity._hot_keys.obj_set?(skill)
        skill_border_id = (set && !skill.nil?()) ? 4 : 0
        art.draw_skill_ex(skill, rect, skill_border_id, entity, true)
      end

      dx += cw4
      art.draw_entity_arts(entity, Rect.new(dx, dy2, cw4 - 8, 24))

      dx = con_rect.x + cw2
      art.draw_actor_parameters_ex(entity,dx,dy,cw8-8,14,1,2...8)

      dx, dy = dx + cw8, dy
      art.draw_entity_element_rates(entity, Rect.new(dx, dy, 24, 64), true, 2)

      dx, dy = con_rect.x, con_rect.height - 48
      art.draw_horz_line(dy)
    end # // drawing_sandbox()
  end

end
