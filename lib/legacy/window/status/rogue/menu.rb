class Window::RogueStatus

  # // Menu Status
  def refresh_mode3(con_rect)
    return unless @unit
    entity    = @unit.entity
    character = @unit.character

    art = artist()

    dx, dy = con_rect.x, con_rect.y

    drawing_sandbox do
      rect = art.draw_entity_element(entity, dx, dy, true)
      contents.font.set_style('simple_black')
      dspacing = 4
      dcols    = 3
      bw, bh= (con_rect.width-(dspacing*dcols)) / dcols, 16
      art.draw_fract(rect.x2, dy,
                     entity.name, entity.class.name,bw-rect.width,rect.height/2)
      art.draw_tiny_actor(character, rect.x2, dy + rect.height - 24, 24)
      art.draw_horz_line(
        dy+rect.height,0,bw)
      dy  += rect.height
      contents.font.set_style('default')

      dy  += (con_rect.height - rect.height) - (bh * 3)
      art.draw_entity_exp(entity, Rect.new(dx, dy, bw, bh), true)
      dy += bh
      art.draw_entity_hp(entity, Rect.new(dx, dy, bw, bh), true)

      dy += bh
      art.draw_entity_mp(entity, Rect.new(dx, dy, bw, bh), true)

      dy  = con_rect.y
      dx  = con_rect.x + bw + dspacing
      art.draw_actor_parameters_ex(entity,dx,dy,bw/2,14,2,2...8)
      dy  = con_rect.y + con_rect.height - 24
      art.draw_actor_icons(entity,dx+((bw%24)/2),dy,bw)
      dy  = con_rect.y
      dx  = con_rect.x + (bw + dspacing) * 2
      art.draw_entity_arts(entity,Rect.new(dx, dy, bw, 24))
    end
  end

end
