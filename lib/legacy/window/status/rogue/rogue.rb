class Window::RogueStatus < Shell::WindowSelectable #Window::Selectable

  include Shell::Addons::Background
  include REI::Mixin::UnitHost

  attr_reader :draw_mode

  def initialize(x, y, w=nil, h=nil)
    @draw_mode = standard_draw_mode # // 0-normal, 1-mini, 2-full, 3-menu
    w ||= window_width ; h ||= window_height
    super(x, y, w, h)
    self.padding_bottom = 0
    init_contents_fx
  end

  def standard_padding
    12
  end

  def line_height
    22
  end

  def update_padding_bottom
    return self
  end

  def contents_height
    self.height - standard_padding * 2
  end

  def standard_draw_mode
    0
  end

  def draw_mode=(new_draw_mode)
    @draw_mode = new_draw_mode
  end

  def set_draw_mode(new_draw_mode)
    if @draw_mode != new_draw_mode
      @draw_mode = new_draw_mode
      refresh_window_size
      refresh
    end
  end

  def on_unit_change
    refresh
  end

  def refresh_window_size
    self.width, self.height = window_width, window_height
    create_contents
  end

  def window_width
    return Graphics.width
  end

  def window_height
    case @draw_mode
    when 0 ; fitting_height(2)
    when 1 ; fitting_height(1)
    when 2 ; Graphics.height - standard_padding * 2
    when 3 ; fitting_height(4)
    end + standard_padding * 2
  end

  def set_position( pos )
    if @position != pos

      @position = pos

      case @position
      when 0 # // Top
        self.y = 0
      when 1 # // Middle
        self.y = (Graphics.height - self.height) / 2
      when 2 # // Bottom
        self.y = Graphics.height - self.height
      end

    end
  end

  def update
    super
    update_contents_fx
  end

  def refresh
    contents.clear
    #DrawExt.draw_box1(
    #  contents, contents.rect,
    #  Palette['brown1'], Palette['paper1'].hset(alpha: 198))

    con_rect = contents.rect.contract(anchor: 5, amount: 4)

    self.send("refresh_mode#@draw_mode", con_rect)

    set_contents_fadein(force: true)
  end

  def standard_artist
    Artist::RogueStatus
  end

  ## Shell::Addons::Simple_Background
  # draw_sbackground
  def draw_sbackground
    bmp = @sbackground_sprite.bitmap
    bmp.fill_rect(bmp.rect, Palette['droid_dark_ui_dis'])
    anchor_center = MACL::Surface::ANCHOR_MIDDLE_CENTER
    anchor_xcenter = MACL::Surface::ANCHOR_CENTER
    bmp.fill_rect(bmp.rect.contract(anchor: anchor_center, amount: 1), Palette['droid_dark_ui_enb'])
    bmp.fill_rect(bmp.rect.contract(anchor: anchor_center, amount: standard_padding), Palette['droid_light_ui_dis'])
    bmp.fill_rect(bmp.rect.contract(anchor: anchor_center, amount: standard_padding + 1), Palette['droid_dark_ui_enb'])
    rect = bmp.rect.hset(:height=>1).contract(anchor: anchor_xcenter, amount: standard_padding / 2)
  end

end
