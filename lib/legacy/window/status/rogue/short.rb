class Window::RogueStatus

  # // Short Status
  def refresh_mode1(con_rect)
    return unless @unit
    entity = @unit.entity
    art = artist

    bw = con_rect.width / 6
    dx, dy = con_rect.x, con_rect.y
    rele = art.draw_entity_element(entity, dx, dy, true)
    wd = rele.width
    art.draw_horz_line(rele.y2 - 2)

    contents.font.size = Font.default_size

    art.draw_actor_name(entity, dx + wd, dy)
    dx += wd + 96
    dy += 0
    rect = Rect.new(dx, dy, bw, 12)
    art.draw_entity_hp(entity, rect, false)
    rect.y += rect.height
    art.draw_entity_mp(entity, rect, false)

    return self
  end

end
