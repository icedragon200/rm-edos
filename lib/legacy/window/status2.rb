#
# EDOS/src/window/status2.rb
#   dm 19/05/2013
# vr 1.0.0
class Window::Status2 < Window::RogueStatus

  def initialize(unit, x=0, y=0)
    super(x, y)
    activate
    set_unit(unit)
  end

  def standard_draw_mode
    2
  end

end
