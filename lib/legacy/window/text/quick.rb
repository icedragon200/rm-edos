# Window::QuickText
#==============================================================================#
# ♥ Window::QuickText
#==============================================================================#
# // • Created By    : IceDragon
# // • Modified By   : IceDragon
# // • Data Created  : 12/27/2011
# // • Data Modified : 12/27/2011
# // • Version       : 1.0
#==============================================================================#
# ● Change Log
#     ♣ 12/27/2011 V1.0
#
#==============================================================================#
class Window::QuickText < Window::SmallText

  class AddonBin_WQT < WindowAddons::AddonBin[WindowAddons::Header]

    def header_text
      @parent.header_text
    end

    def header_cude
      super.hset(:width=>self.width-4)
    end

    def header_visible?
      return super && header_text
    end

  end

  def addon_bin
    AddonBin_WQT
  end

  def header_text()
    @header_text
  end

  def initialize( x, y, width, height, text=nil, header_text=nil )
    @header_text = header_text
    super( x, y, width, height )
    @_def_align = 0
    set_text( text ) if text
  end

  def standard_padding
    8
  end

  def refresh()
    contents.clear()
    contents.font.set_style( :simple_black )
    dx = 4
    dy = ((contents.height-24)/2)
    contents.blt(dx, dy,
      Cache.system("gui_symbols(small)"), Rect.new(0, 0, 24, 24))
    artist.draw_text(
      dx + 24, dy, contents.width, contents.height, @text, @align)
  end

end
#=■==========================================================================■=#
#                           // ● End of File ● //                              #
#=■==========================================================================■=#
