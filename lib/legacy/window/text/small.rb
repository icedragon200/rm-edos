#
#
#
class Window::SmallText < Window::Base

  attr_reader :text, :align

  def initialize( x, y, width=96, height=32 )
    super( x, y, width, height )
    self.windowskin = Window::SkinCache.bitmap("window_smallborder")
    @text = nil
    @align = nil
    @_def_align = 1
  end

  def standard_padding
    4
  end

  def set_text( text="", align=@_def_align, forced=false )
    if @text != text  || @align != align || forced
      @text = text
      @align = align
      refresh()
    end
  end

  def refresh()
    contents.clear()
    contents.font.set_style('simple_black')
    #contents.font.size = Font.default_size - 8
    artist.draw_text(
      contents.rect, @text, @align)
  end

end
