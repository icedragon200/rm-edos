# Window::TitleCommand
#==============================================================================
# ■ Window::TitleCommand
#------------------------------------------------------------------------------
# 　タイトル画面で、ニューゲーム／コンティニューを選択するウィンドウです。
#==============================================================================
class Window::TitleCommand < Window::Command

  class AddonBin_WTC < WindowAddons::AddonBin[
    WindowAddons::Header,
    #WindowAddons::WinButtons,
    WindowAddons::Mouse_MoveWindow]

    def header_text
      return "Title"
    end

    def header_cube
      cube = super
      cube.set(self.x, cube.y, cube.z,
               self.width - 2, cube.height, cube.depth)
               #self.width - winbuttons_width - 2, cube.height, cube.depth)
      return cube
    end

  end

  def addon_bin
    AddonBin_WTC
  end
  #--------------------------------------------------------------------------
  # ● オブジェクト初期化
  #--------------------------------------------------------------------------
  def initialize
    super(0, 0)
    update_placement
    select_symbol(:continue) if continue_enabled
    self.openness = 0
    open
  end
  def update_tone
    #self.tone.set($data_system.window_tone)
  end
  #--------------------------------------------------------------------------
  # ● ウィンドウ幅の取得
  #--------------------------------------------------------------------------
  def window_width
    return 160
  end
  #--------------------------------------------------------------------------
  # ● ウィンドウ位置の更新
  #--------------------------------------------------------------------------
  def update_placement
    self.x = (Graphics.width - width) / 2
    self.y = Graphics.height - self.height - 32
    #(Graphics.height * 1.6 - height) / 2
  end

  #--------------------------------------------------------------------------
  # ● コマンドリストの作成
  #--------------------------------------------------------------------------
  def make_command_list
    add_command(Vocab::new_game, :new_game)
    add_command(Vocab::continue, :continue, continue_enabled)
    #add_command("Map Editor"   , :mapeditor)
    #add_command("Bestiary"     , :bestiary)
    add_command("About"     , :about)
    #add_command("Traptionary"  , :traptionary)
    #add_command("Itemary"      , :itemary)
    #add_command("Skillary"     , :skillary)
    add_command(Vocab::shutdown, :shutdown)
  end
  #--------------------------------------------------------------------------
  # ● コンティニューの有効状態を取得
  #--------------------------------------------------------------------------
  def continue_enabled
    DataManager.any_save_file_exists?
  end

  def col_max
    return item_max
  end

end
