#
# EDOS/lib/lib.rb
#   by IceDragon
Thread.abort_on_exception = true
$LOAD_PATH.push("/home/icy/Dropbox/code/Git") # temp
$LOAD_PATH.push("/home/icy/Dropbox/code/Git/rm-macl/lib") # temp
$LOAD_PATH.push("/home/icy/Dropbox/code/Git/starruby/lib") # temp
require 'bundler/setup'
Bundler.require

require 'starruby'

require 'ostruct'
require 'set'
require 'zlib'
require 'yaml'

## rm-srri
require 'rm-srri/local'
require 'rm-srri/local-exp' # Expansion

## MACL
require 'rm-macl/core_ext'
require 'rm-macl/xpan'
require 'rm-macl/mixin'
require 'rm-macl/util'
require 'rm-macl/rgss3-ext'

Convert = MACL::Convert # export

## async
require 'kode-xchange/lib/core/async'

require_relative 'edos'
require_relative 'mixin'
require_relative 'mythryl'
require_relative 'palila'
require_relative 'drawext'
require_relative 'core'
require_relative 'module'
require_relative 'automation'
require_relative 'fiber_effect'
require_relative 'daemon'
require_relative 'data_model'
require_relative 'plane'
require_relative 'sprite'
require_relative 'spriteset'
require_relative 'hazel'
require_relative 'ui'
require_relative 'game'
require_relative 'controller'
require_relative 'scene'
require_relative 'resolution'
require_relative 'config'

begin
  require_relative 'test'
rescue => ex
  `subl3 "#{ex.backtrace[0].split(":")[0,2].join(":")}"`
  raise ex
end