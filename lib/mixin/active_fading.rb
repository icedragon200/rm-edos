#
# EDOS/lib/mixin/active_fading.rb
#   dm 15/06/2013
module Mixin
  module ActiveFading

    # // 02/27/2012 - Active Fading
    def update_active_fading
      return unless active_fading?
      active2fade? ? active_fadein : active_fadeout
    end

    def active_fadein
      self.opacity = (self.opacity+(255/active_fadein_time)).clamp(active_fade_min(),active_fade_max())
    end

    def active_fadeout
      self.opacity = (self.opacity-(255/active_fadein_time)).clamp(active_fade_min(),active_fade_max())
    end

    def active_fadein_time
      20.0
    end

    def active_fadeout_time
      20.0
    end

    def active_fade_min
      198
    end

    def active_fade_max
      255
    end

    def active_fading?
      false
    end

    def active2fade?
      active
    end

  end
end