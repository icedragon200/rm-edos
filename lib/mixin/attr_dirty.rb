module Mixin
  module AttrDirty

    def attr_dirty(src_var, *attrs)
      attrs.map do |sym|
        define_method("#{sym}_changed?") do
          src_obj = instance_variable_get(src_var)
          src_obj.send(sym) != instance_variable_get("@#{sym}")
        end
        "#{sym}_changed?"
      end
    end

  end
end