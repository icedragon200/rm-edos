# Mixin::DrawTextEx
# // 01/31/2012
# // 01/31/2012
module Mixin
  module DrawTextEx

    #include Mixin::ColorSkin

    def hp_cost_color
      Palette['sys_red2']
    end

    def line_height
      24
    end

    def text_size(str)
      contents.text_size(str)
    end

    def draw_text(*args)
      contents.draw_text(*args)
    end

    def draw_text_ex(x, y, text)
      reset_font_settings
      text = convert_escape_characters(text)
      pos = {:x => x, :y => y, :new_x => x, :height => calc_line_height(text)}
      process_character(text.slice!(0, 1), text, pos) until text.empty?
    end

    def reset_font_settings
      change_color(normal_color)
      contents.font.size = Font.default_size
      contents.font.bold = false
      contents.font.italic = false
    end

    def convert_escape_characters(text)
      result = text.to_s.dup
      result.gsub!(/\\/)            { "\e" }
      result.gsub!(/\e\e/)          { "\\" }
      result.gsub!(/\eV\[(\d+)\]/i) { $game.variables[$1.to_i] }
      result.gsub!(/\eV\[(\d+)\]/i) { $game.variables[$1.to_i] }
      result.gsub!(/\eN\[(\d+)\]/i) { actor_name($1.to_i) }
      result.gsub!(/\eP\[(\d+)\]/i) { party_member_name($1.to_i) }
      result.gsub!(/\eG/i)          { Vocab::currency_unit }
      result
    end

    def actor_name(n)
      actor = n >= 1 ? $game.actors[n] : nil
      actor ? actor.name : ""
    end

    def party_member_name(n)
      actor = n >= 1 ? $game.party.members[n - 1] : nil
      actor ? actor.name : ""
    end

    def process_character(c, text, pos)
      case c
      when "\n"   # 改行
        process_new_line(text, pos)
      when "\f"   # 改ページ
        process_new_page(text, pos)
      when "\e"   # 制御文字
        process_escape_character(obtain_escape_code(text), text, pos)
      else        # 普通の文字
        process_normal_character(c, pos)
      end
    end

    def process_normal_character(c, pos)
      text_width = text_size(c).width
      draw_text(pos[:x], pos[:y], text_width * 2, pos[:height], c)
      pos[:x] += text_width
    end

    def process_new_line(text, pos)
      pos[:x] = pos[:new_x]
      pos[:y] += pos[:height]
      pos[:height] = calc_line_height(text)
    end

    def process_new_page(text, pos)
    end

    def obtain_escape_code(text)
      text.slice!(/^[\$\.\|\^!><\{\}\\]|^[A-Z]+/i)
    end

    def obtain_escape_param(text)
      text.slice!(/^\[\d+\]/)[/\d+/].to_i rescue 0
    end

    def process_escape_character(code, text, pos)
      case code.upcase
      when 'K'
        change_color(kcolor(obtain_escape_param(text)))
      when 'C'
        change_color(text_color(obtain_escape_param(text)))
      when 'I'
        process_draw_icon(obtain_escape_param(text), pos)
      when '{'
        make_font_bigger
      when '}'
        make_font_smaller
      end
    end

    def process_draw_icon(icon_index, pos)
      draw_icon(icon_index, pos[:x], pos[:y])
      pos[:x] += 24
    end

    def make_font_bigger
      contents.font.size += 8 if contents.font.size <= 64
    end

    def make_font_smaller
      contents.font.size -= 8 if contents.font.size >= 16
    end

    def calc_line_height(text, restore_font_size = true)
      result = [line_height, contents.font.size].max
      last_font_size = contents.font.size
      text.slice(/^.*$/).scan(/\e[\{\}]/).each do |esc|
        make_font_bigger  if esc == "\e{"
        make_font_smaller if esc == "\e}"
        result = [result, contents.font.size].max
      end
      contents.font.size = last_font_size if restore_font_size
      result
    end

    def strip_escape_codes(text)
      t = text.gsub(/\\\w\[\d+\]/i, "")
      t
    end

  end
end