#
# EDOS/lib/mixin/font_style.rb
#   by IceDragon
#
# vr 1.3.2
# CHANGES
#   vr 1.3.2 [30/03/2013]
#     Clean up external code
#   vr 1.3.1 [09/03/2013]
#     Added #set
#           #set_mix_style
#           #reset_style
#           ::styles
#
#   vr 1.3.0
#     Style Hashes now support Procs as values, which will be evaluated
#     at refresh
module Mixin
  module FontStyle

    ### class_variables
    @@style = {}
    @@_properties = Hash[[:name, :color, :out_color,
                          :shadow_color, :shadow_conf].zip([true] * 5) +
                         [:size, :bold, :italic,
                          :shadow, :outline, :antialias].zip([false] * 6)]
    ### constants
    DEFAULT_STYLE = Hash[@@_properties.keys.zip([nil] * @@_properties.size)]

    ### mixin_functions
    ##
    # refresh_style -> self
    def refresh_style
      (@@_properties).each_pair do |sym, should_clone|
        n = @style[sym]
        n = if n.nil? then Font.send("default_#{sym}")
            elsif n.is_a?(Proc) then n.call(self)
            else n
            end
        n = n.dup if should_clone
        self.send("#{sym}=", n)
      end
    end

    ##
    # reset_style
    def reset_style
      @style = {}
      refresh_style
      return self
    end

    ##
    # set_style(String style_name)
    def set_style(style_name)
      FontStyle.validate_style_name(style_name)
      @style = @@style[style_name] || DEFAULT_STYLE
      set_custom_style(@style.dup)
      refresh_style
      return self
    end

    ##
    # set_style(Hash style_hash) -> style_hash
    def set_custom_style(style_hash)
      @style = style_hash
      return self
    end

    ##
    # append_style(Hash style_hash) -> self
    def append_style(style_hash)
      style_hash.each_pair do |k, v|
        @style[k] = v unless v.nil?
      end
      return self
    end

    ##
    # set_mix_style(*(Hash[] style_hashes))
    def set_mix_style(*style_hashes)
      reset_style
      style_hashes.each { |style_hash| append_style(style_hash) }
      return self
    end

    ### time_machine_functions
    ##
    # snapshot -> Font
    def snapshot
      (@snapshots ||= []).push(Mixin::FontStyle.copy_font_to(self, Font.new))
      if block_given?
        yield self
        return restore
      else
        return @snapshots[-1]
      end
    end

    ##
    # restore -> Font
    def restore
      raise(FontStyleError,
            "There are no snapshots for this Font!") if @snapshots.empty?
      font = @snapshots.pop
      Mixin::FontStyle.copy_font_to(font, self)
      return font
    end

    ##
    # set(Font other)
    #   Copies the other into the current font
    def set(other)
      Mixin::FontStyle.copy_font_to(other, self)
      self
    end

    ##
    # copy_to(Font dst)
    #   Copies this font into the dst Font
    def copy_to(dst)
      FontStyle.copy_font_to(self, dst)
    end

    ### class-level
    ##
    # ::copy_font_to(Font src_font, Font trg_font) => trg_font
    def self.copy_font_to(src_font, trg_font)
      FontStyle.assert_type(src_font)
      FontStyle.assert_type(trg_font)

      (@@_properties).each_pair do |sym, should_clone|
        begin
          n = src_font.send(sym)
          n = n.dup if should_clone
          trg_font.send("#{sym}=", n)
        rescue Exception => ex
          $stderr.puts("Error Ocurred while setting: #{sym} for #{trg_font}")
          raise ex
        end
      end

      return trg_font
    end

    ##
    # ::validate_style_name(String style_name)
    def self.validate_style_name(style_name)
      raise(TypeError,
            "use of Symbol for FontStyle is depreceated!"
            ) if style_name.is_a?(Symbol)
      return true
    end

    ##
    # ::new_style(String style_name, Hash properties) => new_style_hash
    def self.new_style(style_name, properties={})
      validate_style_name(style_name)
      style_hash = @@style[style_name] = DEFAULT_STYLE.dup

      yield style_hash if block_given?

      style_hash.merge!(properties)

      return style_hash
    end

    ##
    # ::get_style(String style_name) => style_hash
    def self.get_style(style_name)
      return @@style[style_name]
    end

    ##
    # ::styles
    def self.styles
      return @@style.keys
    end

  end # /FontStyle
end # /Mixin