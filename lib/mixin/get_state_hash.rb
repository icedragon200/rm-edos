#
# EDOS/lib/mixin/get_state_hash.rb
#   by IceDragon
# GetStateHash allows you to pull a list of attributes from the current
# Object, these attributes can be used to return the object to an original
# state, such as a previous position or previous opacity.
# The methods here are simply abstract and must be rewritten in the sub class.
module Mixin
  module GetStateHash

    ##
    # @return [Hash<Symbol, Object>]
    def get_state_h
      {
      }
    end

    ##
    # @param [Hash<Symbol, Object>] hsh
    def set_state_h(hsh)
      #
    end

  end
end