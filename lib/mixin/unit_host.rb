#
# EDOS/lib/mixin/unit_host.rb
#   by IceDragon
module Mixin
  module UnitHost

    attr_accessor :unit

    def set_unit(new_unit)
      if unit != new_unit
        self.unit = new_unit
        on_unit_change
      end
      self
    end

    def on_unit_change
      # overwrite in subclass
    end

  end
end