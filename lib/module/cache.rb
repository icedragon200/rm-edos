#
# EDOS/lib/module/Cache.rb
#   by IceDragon
#   dc ??/??/2012
#   dm 12/05/2013
# vr 1.1.0
#   EDOS revision of the RMVXA Cache Module
#   Supports fallback functions in the case that a graphics cannot be found.
module Cache

  GFX_ROOT = "media/graphics"

  ##
  # init
  def init
    @loader = {}
    @cache = {}
  end

  ##
  # include?(String key)
  def include?(key)
    @cache[key] && !@cache[key].disposed?
  end

  ##
  # clear
  def clear
    @cache ||= {}
    @cache.clear
    GC.start
  end

  ##
  # new_loader(Symbol sym, String dir)
  def new_loader(sym, dir, &fallback)
    dirpath = File.join(GFX_ROOT, dir)
    ## create_loader
    loader = lambda do |filename, hue=0|
      begin
        load_bitmap(dirpath, filename, hue)
      rescue Errno::ENOENT => ex
        if fallback
          fallback.(dirpath, filename, hue)
        else
          raise ex
        end
      end
    end
    ## set_loader
    @loader[sym] = [sym, dir, loader, fallback]
    define_method(sym, &loader)
  end

  ##
  # empty_bitmap
  def empty_bitmap
    CacheBitmap.new(1, 1)
  end

  ##
  # normal_bitmap(String path)
  def normal_bitmap(path)
    unless include?(path)
      @cache[path] = CacheBitmap.new(path)
      @cache[path].cache_key = path
    end
    @cache[path]
  end

  ##
  # hue_changed_bitmap(String path, Integer hue)
  def hue_changed_bitmap(path, hue=0)
    key = [path, hue]
    unless include?(key)
      @cache[key] = normal_bitmap(path).dup
      @cache[key].hue_change(hue)
    end
    @cache[key]
  end

  ##
  # load_bitmap
  def load_bitmap(folder_name, filename, hue=0)
    @cache ||= {}
    if filename.empty?
      empty_bitmap
    elsif hue != 0
      hue_changed_bitmap(File.join(folder_name, filename), hue)
    else
      normal_bitmap(File.join(folder_name, filename))
    end
  end

  extend self

  init

end
require_relative 'cache/loaders'
require_relative 'cache/sequence'