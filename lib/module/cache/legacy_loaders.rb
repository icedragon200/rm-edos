#
# EDOS/lib/module/cache/legacy_loaders.rb
#   by IceDragon
#   dc 08/06/2013
#   dm 08/06/2013
module Cache

  ### fallback_function
  char_fallback = ->(d, f, h) do
    STDERR.puts("Loading fallback character for %s" % f)
    if f[0, 2].include?("$")
      load_bitmap(d, "$#fallback", h)
    else
      load_bitmap(d, "#fallback", h)
    end
  end

  portrait_fallback = ->(d, f, h) do
    STDERR.puts("Loading fallback portrait for %s" % f)
    path = File.join(d, f)
    key = h == 0 ? path : [path, h]
    @cache[key] ||= Bitmap.new(256, 256)
  end

  ### loaders
  new_loader :animation,   "Animations"
  new_loader :battleback1, "Battlebacks1"
  new_loader :battleback2, "Battlebacks2"
  new_loader :battler,     "Battlers"
  new_loader :character,   "Characters", &char_fallback
  new_loader :face,        "Faces"
  new_loader :parallax,    "Parallaxes"
  new_loader :picture,     "Pictures"
  new_loader :portrait,    "Portraits",  &portrait_fallback
  new_loader :system,      "System"
  new_loader :tileset,     "Tilesets"
  new_loader :title1,      "Titles1"
  new_loader :title2,      "Titles2"

end