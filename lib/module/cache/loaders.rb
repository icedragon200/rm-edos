#
# EDOS/lib/module/cache/loaders.rb
#   by IceDragon
#   dc 08/06/2013
#   dm 08/06/2013
module Cache

  ### fallback_function
  char_fallback = ->(d, f, h) do
    STDERR.puts("Loading fallback character for %s" % f)
    if f[0, 2].include?("$")
      load_bitmap(d, "$#fallback", h)
    else
      load_bitmap(d, "#fallback", h)
    end
  end

  portrait_fallback = ->(d, f, h) do
    STDERR.puts("Loading fallback portrait for %s" % f)
    path = File.join(d, f)
    key = h == 0 ? path : [path, h]
    @cache[key] ||= Bitmap.new(256, 256)
  end

  face_fallback = ->(d, f, h) do
    STDERR.puts("Loading fallback face for %s" % f)
    path = File.join(d, f)
    key = h == 0 ? path : [path, h]
    @cache[key] ||= Bitmap.new(96 * 4, 96 * 2)
  end

  ### loaders
  #new_loader :animation,   "animation"
  #new_loader :parallax,    "parallaxe"
  #new_loader :picture,     "picture"
  new_loader :atlas,       "atlas"
  new_loader :character,   "character", &char_fallback
  new_loader :face,        "face",      &face_fallback
  new_loader :portrait,    "portraits", &portrait_fallback
  new_loader :system,      "iconset"
  new_loader :system,      "system"

end
