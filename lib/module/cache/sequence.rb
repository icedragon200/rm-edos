module Cache
  module Sequence

    @@cache = {}
    def self.normal(filename)
      name = "data/sequence/#{filename}.seq.rb"
      @@cache[name] ||= Animator::Builder.build(File.read(name))
    end

    def self.animation(filename)
      normal("animation/#{filename}")
    end

    def self.emo(filename)
      normal("emo/#{filename}")
    end

    def self.clear
      @@cache.clear
    end

  end
end