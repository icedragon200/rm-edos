#
# EDOS/lib/module/lacio.rb
#   by IceDragon
#   dc 19/06/2013
#   dm 19/06/2013
# Layout helper module, based on GTK's Layout Containers
# p.s
#   Lacio is latin meaning "lay"
module Lacio

  VERSION = "0.0.1".freeze

end

require_relative 'lacio/space_box'
require_relative 'lacio/spreadsheet'
require_relative 'lacio/accelerari'
require_relative 'lacio/drect'