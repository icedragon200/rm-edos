#
# EDOS/lib/module/lacio/drect.rb
#   by IceDragon
module Lacio
  class DRect < Rect

    # @return [Array<Integer, Integer, Integer, Integer>]
    attr_writer :padding #
    # @return [Array<Integer, Integer, Integer, Integer>]
    attr_writer :margin #
    # @return [Array<Integer, Integer, Integer, Integer>]
    attr_writer :border #

    def padding
      @padding ||= Vector4.new(0, 0, 0, 0)
    end

    def margin
      @margin ||= Vector4.new(0, 0, 0, 0)
    end

    def border
      @border ||= Vector4.new(0, 0, 0, 0)
    end

    def margin_rect
      x, y, z, w = *margin
      r = dup
      r.x += x
      r.y += y
      r.width -= z * 2
      r.height -= w * 2
      return r
    end

    def padding_rect
      x, y, z, w = *padding
      r = margin_rect
      r.x += x
      r.y += y
      r.width -= z * 2
      r.height -= w * 2
      return r
    end

    def border_rect
      x, y, z, w = *border
      r = margin_rect
      r.x -= x
      r.y -= y
      r.width += z * 2
      r.height += w * 2
      return r
    end

  end
end