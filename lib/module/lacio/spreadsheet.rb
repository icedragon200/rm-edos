#
# EDOS/lib/module/lacio/spreadsheet.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
module Lacio
  class Spreadsheet
    class CellBank

      attr_accessor :rows

      def initialize(rows)
        @rows = rows
      end

      ##
      # row(int rowi)
      def row(rowi)
        @rows[rowi]
      end

      ##
      # row_rect(int rowi)
      def row_rect(rowi)
        MACL::Surface.area_rect(row(rowi))
      end

      ##
      # cell(int rowi, int coli)
      def cell(rowi, coli)
        if @rows[rowi]
          if n = @rows[rowi][coli]
            return n
          else
            raise(ArgumentError, "invalid column #{coli} for row #{rowi}")
          end
        else
          raise(ArgumentError, "invalid row #{rowi}")
        end
      end

      alias :[] :cell

    end

    Row = Struct.new(:height, :columns)

    attr_accessor :rows
    attr_accessor :row_height

    def initialize
      @rows = []
      @row_height = 1.0
    end

    def clear
      @rows.clear
      self
    end

    def row(height=row_height)
      @rows.push(Row.new(height, []))
      self
    end

    def col(*objs)
      objs.each do |obj|
        columns = @rows[-1].columns
        rem = 1.0 - (columns.inject(:+) || 0)
        case obj
        # remaining space
        when nil
          ratio = rem
        # if the float value is less than 0, treat as a ratio of the remaining
        else
          ratio = obj < 0 ? rem * obj.abs : obj
        end
        columns.push(ratio)
      end
      self
    end

    def cols(count)
      raise(ArgumentError, "expected value greater than 0") if count <= 0
      ratio = 1.0 / count
      count.times { col(ratio) }
      self
    end

    def matrix(rows, cols)
      rem_ratio = 1 - (@rows.map { |h| h.height }.inject(:+) || 0)
      row_height = rem_ratio / rows
      rows.times do
        row(row_height)
        cols(cols)
      end
      self
    end

    ##
    # render(Rect rect)
    #   Rect#height is treated as the value for 1 row
    def render(rect)
      x, y, w, h = rect.to_a
      last_y = y
      new_rows = @rows.map do |hsh|
        last_x = x
        row_height = h * hsh.height
        columns = hsh.columns
        new_columns = columns.map do |ratio|
          col_width = w * ratio
          col_rect = Rect.new(last_x, last_y, col_width, row_height)
          last_x += col_width
          col_rect
        end
        last_y += row_height
        new_columns
      end
      return CellBank.new(new_rows)
    end

    def self.chunk(rect)
      spreadsheet = new
      yield spreadsheet
      cellbank = spreadsheet.render(rect)
      return cellbank
    end

  end

end
