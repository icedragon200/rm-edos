# Main
# // 02/18/2012
# // 02/18/2012
module Main

  class << self
    attr_reader :daemon_server
    attr_reader :server
    attr_reader :scene_manager
  end

  def self.init
    @server        = EDOS.server
    @daemon_server = EDOS.daemon_server
    @scene_manager = Mythryl::SceneManager.new
    @spriteset     = Spriteset::Main.new
    @parallel      = Thread.new do
      loop do
        @server.update
        sleep 3.0
      end
    end
  end

  def self.update
    @daemon_server.update
    @spriteset.update
    Graphics.update
    Mouse.update
    Keyboard.update
    Input.update
    #if Input.repeat?(:CTRL) && Input.repeat?(:f2)
    #  StarRuby::Game.current.fullscreen = !StarRuby::Game.current.fullscreen
    #end
  end

  def self.wait(frames)
    frames.times { |i| update }
  end

end