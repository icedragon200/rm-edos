#
# src/module/metric.rb
#   by IceDragon
# vr 1.0.1
#   Control all the system metrics such as spacing and scale
module Metric
  module Time

    def self.frame_to_sec(frms)
      frms / Graphics.frame_rate.to_f
    end

    def self.sec_to_frame(sec)
      Integer(sec * Graphics.frame_rate)
    end

    class << self
      alias :s2f :sec_to_frame
      alias :f2s :frame_to_sec
    end

  end

  # Based on the Android Specfication
  @scale = 1.0
  @ui_element       = 48 # 48dp
  @ui_element_mid   = 24 # 24dp
  @ui_element_sml   = 16 # 16dp
  @ui_icon          = 32 # 32dp
  @spacing          = 8  # 8dp
  @contract         = 4  # 4dp  # Used for Surface#contract
  @padding          = 4  # 4dp
  @icon_contract    = 8  # 8dp

  @scales = {
    tera:    4.00,
    giga:    3.00,
    mega:    2.50,
    h1:      2.00,
    h2:      1.90,
    h3:      1.80,
    h4:      1.70,
    h5:      1.60,
    h6:      1.50,
    xlarge:  1.40,
    large:   1.25,
    default: 1.00,
    medium:  0.90,
    small:   0.80,
    micro:   0.70,
    nano:    0.60,
    half:    0.50,
    pico:    0.40,
    atto:    0.30,
  }

  ###
  [:ui_element, :ui_element_sml, :ui_element_mid,
   :ui_icon, :spacing, :padding,
   :contract, :icon_contract].each do |sym|
    module_eval("def self.#{sym}; return (@#{sym} * @scale).to_i; end")
    module_eval("def self.#{sym}=(new_v); @#{sym} = new_v; end")
  end

  ##
  # ::ui_font_size(sym)
  def self.ui_font_size(sym)
    raise(ArgumentError,
          "invalid font size symbol #{sym}") unless @scales.has_key?(sym)
    Integer(Font.default_size * @scales[sym] * @scale)
  end

  ##
  # ::snap_scale
  def self.snap_scale
    old_scale = @scale
    yield self
    @scale = old_scale
  end

  ##
  # ::apply_scale(int n) -> int
  # ::apply_scale(float n) -> float
  # ::apply_scale(int n, Symbol scale_sym) -> int
  # ::apply_scale(float n, Symbol scale_sym) -> float
  #   Use this function to scale integers based on internal values
  def self.apply_scale(n, scale_sym=:default)
    v = @scale[scale_sym] * n * @scale
    case n
    when Integer then Integer(v)
    when Float   then Float(v)
    else              v # some other type O:
    end
  end

  class << self
    attr_accessor :scale
    attr_reader :scales
  end

end