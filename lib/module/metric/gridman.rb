#
# EDOS/lib/module/metric/gridman.rb
#   by IceDragon
class Metric::Gridman

  def self.new_from_dim(rect, cols, rows)
    w = rect.width / cols
    h = rect.height / rows
    return new(rect, Size2.new(w, h))
  end

  def initialize(rect, cell_size=nil)
    @src_rect = rect
    @cell_size = cell_size || Size2.new(Metric.ui_element, Metric.ui_element)
  end

  def col_max
    return @src_rect.width / @cell_size.width
  end

  def row_max
    return @src_rect.height / @cell_size.height
  end

  def col_max_width
    return @cell_size.width * col_max
  end

  def row_max_height
    return @cell_size.height * row_max
  end

  def col_x(n)
    return @src_rect.x + n * @cell_size.width
  end

  def row_y(n)
    return @src_rect.y + n * @cell_size.height
  end

  def col_rect(n)
    return Rect.new(col_x(n), row_y(0), @cell_size.width, row_max_height)
  end

  def row_rect(n)
    return Rect.new(col_x(0), row_y(n), col_max_width, @cell_size.height)
  end

  def cell_rect(x, y)
    return Rect.new(col_x(x), col_y(y), @cell_size.width, @cell_size.height)
  end

  def mcell_rect(x, y, w, h)
    return Rect.new(col_x(x), col_y(y),
                    @cell_size.width * w, @cell_size.height * h)
  end

end
