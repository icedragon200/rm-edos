#
# EDOS/lib/module/mouse.rb
#   by IceDragon
module Mouse

  BUTTONS = [:left, :right, :middle]

  class << self

    attr_accessor :position

    def init
      @button_counter = {}
      @button_counter.default = 0
      @position = Vector2.new(0, 0)
      @repeat_rate = Metric::Time.sec_to_frame(0.1666666666)
    end

    def x
      position.x
    end

    def y
      position.y
    end

    def z
      0xFFFF
    end

    def press?(key)
      @button_counter[key] > 0
    end

    def trigger?(key)
      @button_counter[key] == 1
    end

    def repeat?(key)
      counter = @button_counter[key]
      counter > 0 && (counter == 1 || counter % @repeat_rate == 0)
    end

    def update
      BUTTONS.each do |button|
        if Input.sr_press?(:mouse, button)
          @button_counter[button] += 1
        else
          @button_counter[button] = 0
        end
      end
      @position.replace(StarRuby::Input.mouse_location)
    end

  end

  init

end