#
# EDOS/lib/module/palette.rb
# vr 3.0.0
Palette = DrawExt::DrawExtPalette.new
Palette.name = 'system'

require_relative 'palette/edos-elements'
require_relative 'palette/gem'
require_relative 'palette/grays'
require_relative 'palette/merio'
require_relative 'palette/droid'
require_relative 'palette/droid-fast-state'
require_relative 'palette/monokai'
require_relative 'palette/sys'
require_relative 'palette/pokemon-4c'

Palette.instance_exec do

  # Basic
  set_color('transparent',   0,   0,   0,   0)
  set_color('black',         0,   0,   0, 255)
  set_color('white',       255, 255, 255, 255)

end

Palette.refresh_keys
#Palette.freeze_entries
#Palette.freeze