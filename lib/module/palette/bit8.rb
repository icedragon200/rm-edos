#
# EDOS/lib/module/palette/grays.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # 8Bit RGB Palette
  set_color('black',        Color.argb32(0xFF000000))
  set_color('white',        Color.argb32(0xFFFFFFFF))
  set_color('red',          Color.argb32(0xFFFF0000))
  set_color('green',        Color.argb32(0xFF00FF00))
  set_color('blue',         Color.argb32(0xFF0000FF))
  set_color('cyan',         Color.argb32(0xFF00FFFF))
  set_color('magenta',      Color.argb32(0xFFFF00FF))
  set_color('yellow',       Color.argb32(0xFFFFFF00))
  set_color('red_dark',     Color.argb32(0xFF800000))
  set_color('green_dark',   Color.argb32(0xFF008000))
  set_color('blue_dark',    Color.argb32(0xFF000080))
  set_color('cyan_dark',    Color.argb32(0xFF008080))
  set_color('magenta_dark', Color.argb32(0xFF800080))
  set_color('yellow_dark',  Color.argb32(0xFF808000))
end
