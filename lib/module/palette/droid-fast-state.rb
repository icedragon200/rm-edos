#
# EDOS/lib/module/palette/droid-fast-state.rb
#   by IceDragon
#   dc 06/07/2013
#   dm 06/07/2013
# vr 1.0.0
#   This is a specialized version of the regular droid palette
#   Since alpha blending is a slow operation, by removing the alpha
#   and recalculating the color, we can get a massive speed boost at the
#   sacrifice for alpha blending.
Palette.instance_exec do
  set_color('_droid_dark_ui_enb',  self['droid_dark_ui_enb'])
  set_color('_droid_dark_ui_dis',  self['droid_dark_ui_dis'])
  set_color('_droid_light_ui_enb', self['droid_light_ui_enb'])
  set_color('_droid_light_ui_dis', self['droid_light_ui_dis'])

  set_color('droid_dark_ui_enb',  0x33, 0x33, 0x33, 0xFF * 0.6).opaque!
  set_color('droid_dark_ui_dis',  0x33, 0x33, 0x33, 0xFF * 0.3).opaque!
  set_color('droid_light_ui_enb', 0xFF, 0xFF, 0xFF, 0xFF * 0.8).opaque!
  set_color('droid_light_ui_dis', 0xFF, 0xFF, 0xFF, 0xFF * 0.3).opaque!
end