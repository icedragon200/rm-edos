#
# EDOS/lib/module/palette/android.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  ##
  # Android Palette
  set_color('droid_blue_light',    51,  181,  229) #
  set_color('droid_purple_light', 170,  102,  204) #
  set_color('droid_green_light',  153,  204,    0) #
  set_color('droid_yellow_light', 255,  187,   51) #
  set_color('droid_red_light',    255,   68,   68) #
  set_color('droid_blue',           0,  153,  204) #
  set_color('droid_purple',       153,   51,  204) #
  set_color('droid_green',        102,  153,    0) #
  set_color('droid_yellow',       255,  136,    0) #
  set_color('droid_red',          204,    0,    0) #
  set_color('droid_dark',        0x22, 0x22, 0x22) #
  set_color('droid_light',       0xDD, 0xDD, 0xDD) #

  set_color('droid_dark_ui_enb',  0x33, 0x33, 0x33, 0xFF * 0.6)
  set_color('droid_dark_ui_dis',  0x33, 0x33, 0x33, 0xFF * 0.3)
  set_color('droid_light_ui_enb', 0xFF, 0xFF, 0xFF, 0xFF * 0.8)
  set_color('droid_light_ui_dis', 0xFF, 0xFF, 0xFF, 0xFF * 0.3)

  set_color('black-half',   0,   0,   0, 0x7F)
  set_color('white-half', 255, 255, 255, 0x7F)
end