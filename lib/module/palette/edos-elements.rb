#
# EDOS/lib/module/palette/edos-elements.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # EDOS Elements
  set_color('element0',  172, 143, 121, 255) # General / Neutral / No Element
  set_color('element1',  206, 102,  50, 255) # Fire
  set_color('element2',  129, 174, 209, 255) # Water
  set_color('element3',   51, 178, 155, 255) # Earth
  set_color('element4',  208, 176,  72, 255) # Air
  set_color('element5',  178, 215, 229, 255) # Light
  set_color('element6',   81,  64,  83, 255) # Shadow
  set_color('element7',  140,  24,  33, 255) # HP
  set_color('element8',   48,  56,  80, 255) # MP
end