#
# EDOS/lib/module/palette/edos-notes.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  ### edos-notes
  # EDOS Brown
  set_color('brown1',   92,  65,  40, 255)
  set_color('brown2',  129, 102,  79, 255)
  set_color('brown3',  134, 111,  79, 255)
  set_color('brown4',  147, 106,  67, 255)
  set_color('brown5',   88,  56,   8, 255)

  # EDOS Paper
  set_color('paper1',  193, 174, 132, 255)
  set_color('paper2',  207, 189, 146, 255)
  set_color('paper3',  232, 232, 208, 255)
  set_color('paper4',  217, 184, 139, 255)
  set_color('paper5',  250, 222, 172, 255)
  set_color('paper6',  255, 247, 233, 255)

  # EDOS Outline
  set_color('outline',   42,  16,   0, 255)
  set_color('purple' ,  109,  64,  59, 255)
end
