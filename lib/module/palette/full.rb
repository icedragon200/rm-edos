#
# EDOS/lib/module/palette/full.rb
# vr 2.0.5
require_relative 'bit8'
require_relative 'droid'
require_relative 'edos-elements'
require_relative 'edos-notes'
require_relative 'grays'
require_relative 'merio'
require_relative 'minecraft'
require_relative 'ncs'
require_relative 'sys'
require_relative 'monokai'
require_relative 'pokemon-4c'

Palette.instance_exec do

  # Transparent
  set_color('trans',   255, 255, 255,   0)

  # Console
  set_color('console_back', 0x00, 0x00, 0x00, 0xC0)
  set_color('console_text', 0xC0, 0xC0, 0xC0, 0xFF)

  # Text-White
  set_color('text-white', 0xE8, 0xE8, 0xD0)

  ###
  set_color('saffron', 221, 72, 20)
  set_color('cavern', 174, 167, 159)
  set_color('beet', 44, 0, 30)

end