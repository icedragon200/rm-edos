
Palette.instance_exec do

  set_color("ruby",     194,  55,  65, 255)
  set_color("emerald",   80, 200, 120, 255)
  set_color("sapphire",  15,  82, 186, 255)
  set_color("heliodor", 236, 233, 162, 255)

end