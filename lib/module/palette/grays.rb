#
# EDOS/lib/module/palette/grays.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # gray Palette
  set_color('gray1',   236, 236, 236, 255)
  set_color('gray2',   226, 226, 226, 255)
  set_color('gray3',   215, 215, 215, 255)
  set_color('gray4',   205, 205, 205, 255)
  set_color('gray5',   194, 194, 194, 255)
  set_color('gray6',   183, 183, 183, 255)
  set_color('gray7',   172, 172, 172, 255)
  set_color('gray8',   161, 161, 161, 255)
  set_color('gray9',   149, 149, 149, 255)
  set_color('gray10',  137, 137, 137, 255)
  set_color('gray11',  125, 125, 125, 255)
  set_color('gray12',  112, 112, 112, 255)
  set_color('gray13',   99,  99,  99, 255)
  set_color('gray14',   85,  85,  85, 255)
  set_color('gray15',   70,  70,  70, 255)
  set_color('gray16',   54,  54,  54, 255)
  set_color('gray17',   37,  37,  37, 255)
  set_color('gray18',   17,  17,  17, 255)
end
