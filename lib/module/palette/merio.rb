#
# EDOS/lib/module/palette/merio.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  ##
  # Merio Palette based on the Windows 8 Metro Palette
  set_color("merio_purple",  0xA2, 0x00, 0xFF)
  set_color("merio_magenta", 0xFF, 0x00, 0x97)
  set_color("merio_teal",    0x00, 0xAB, 0xA9)
  set_color("merio_lime",    0x8C, 0xBF, 0x26)
  set_color("merio_brown",   0xA0, 0x50, 0x00)
  set_color("merio_pink",    0xE6, 0x71, 0xB8)
  set_color("merio_orange",  0xF0, 0x96, 0x09)
  set_color("merio_blue",    0x1B, 0xA1, 0xE2)
  set_color("merio_red",     0xE5, 0x14, 0x00)
  set_color("merio_green",   0x33, 0x99, 0x33)
end
