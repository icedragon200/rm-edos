#
# EDOS/lib/module/palette/monokai.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # monokai
  set_color('clay',       0x27, 0x28, 0x22)
  set_color('clay-light', 0x47, 0x48, 0x42)
end
