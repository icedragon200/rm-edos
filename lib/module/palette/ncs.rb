#
# EDOS/lib/module/palette/ncs.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # NCS RGB
  set_color('ncs_white' ,  255, 255, 255)
  set_color('ncs_black' ,    0,   0,   0)
  set_color('ncs_green' ,    0, 159, 107)
  set_color('ncs_red'   ,  192,   2,  51)
  set_color('ncs_yellow',  255, 211,   0)
  set_color('ncs_blue'  ,    0, 135, 189)
end
