Palette.instance_exec do

  set_color("pkmn-normal1",  58,  51,  44)
  set_color("pkmn-normal2", 114,  97,  82)
  set_color("pkmn-normal3", 172, 168, 136)
  set_color("pkmn-normal4", 220, 216, 201)

  set_color("pkmn-grass1",  40,  61,  18)
  set_color("pkmn-grass2",  68, 117,  28)
  set_color("pkmn-grass3", 111, 199,  88)
  set_color("pkmn-grass4", 215, 237, 192)

  set_color("pkmn-water1",  18,  34,  56)
  set_color("pkmn-water2",  41,  74, 124)
  set_color("pkmn-water3",  96, 138, 202)
  set_color("pkmn-water4", 188, 207, 233)

  set_color("pkmn-fire1",  57,  27,  13)
  set_color("pkmn-fire2", 153,  70,  34)
  set_color("pkmn-fire3", 210, 136,  53)
  set_color("pkmn-fire4", 228, 201, 135)

  set_color("pkmn-bug1",  49,  56,  16)
  set_color("pkmn-bug2",  90, 104,  28)
  set_color("pkmn-bug3", 160, 184,  52)
  set_color("pkmn-bug4", 214, 226, 154)

  set_color("pkmn-rock1",  56,  33,  16)
  set_color("pkmn-rock2", 105,  63,  31)
  set_color("pkmn-rock3", 163, 104,  69)
  set_color("pkmn-rock4", 219, 190, 164)

  set_color("pkmn-ground1",  40,  35,   2)
  set_color("pkmn-ground2", 105,  91,  35)
  set_color("pkmn-ground3", 184, 159,  61)
  set_color("pkmn-ground4", 227, 215, 174)

  set_color("pkmn-electric1",  52,  45,   3)
  set_color("pkmn-electric2", 133, 115,  10)
  set_color("pkmn-electric3", 233, 201,  18)
  set_color("pkmn-electric4", 250, 239, 180)

  set_color("pkmn-flying1",  19,  55,  45)
  set_color("pkmn-flying2",  52, 146, 118)
  set_color("pkmn-flying3",  91, 196, 165)
  set_color("pkmn-flying4", 201, 237, 227)

  set_color("pkmn-poison1",  30,  21,  38)
  set_color("pkmn-poison2",  79,  55, 100)
  set_color("pkmn-poison3", 132,  94, 166)
  set_color("pkmn-poison4", 201, 184, 216)

  set_color("pkmn-psychic1",  61,  20,  40)
  set_color("pkmn-psychic2", 133,  44,  86)
  set_color("pkmn-psychic3", 199,  88, 140)
  set_color("pkmn-psychic4", 231, 182, 205)

  set_color("pkmn-fight1",  62,  24,  19)
  set_color("pkmn-fight2", 105,  38,  31)
  set_color("pkmn-fight3", 177,  65,  52)
  set_color("pkmn-fight4", 227, 170, 159)

  set_color("pkmn-ghost1",  18,  15,  21)
  set_color("pkmn-ghost2",  41,  34,  49)
  set_color("pkmn-ghost3",  64,  52,  78)
  set_color("pkmn-ghost4", 147, 128, 170)

  set_color("pkmn-ice1",  19,  54,  55)
  set_color("pkmn-ice2",  45, 132, 134)
  set_color("pkmn-ice3", 132, 208, 213)
  set_color("pkmn-ice4", 210, 239, 240)

  set_color("pkmn-steel1",  35,  41,  44)
  set_color("pkmn-steel2",  79,  92,  98)
  set_color("pkmn-steel3", 148, 168, 173)
  set_color("pkmn-steel4", 224, 227, 228)

  set_color("pkmn-dark1",  30,  26,  21)
  set_color("pkmn-dark2",  62,  53,  45)
  set_color("pkmn-dark3", 100,  88,  74)
  set_color("pkmn-dark4", 173, 158, 137)

  set_color("pkmn-dragon1",  14,  16,  27)
  set_color("pkmn-dragon2",  28,  36,  57)
  set_color("pkmn-dragon3",  55,  68, 111)
  set_color("pkmn-dragon4", 119, 141, 189)

end