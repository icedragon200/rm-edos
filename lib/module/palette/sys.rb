#
# EDOS/lib/module/palette/sys.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
Palette.instance_exec do
  # EDOS 1
  set_color('sys1_orange', 241, 194,  80, 255)
  set_color('sys1_blue',    18,  94, 128, 255)
  set_color('sys1_green',    0, 113,  61, 255)
  set_color('sys1_red',    141,   6,  22, 255)

  # EDOS 2
  set_color('sys2_orange',  224, 184,  96, 255)
  set_color('sys2_blue'  ,   68,  88, 129, 255)
  set_color('sys2_green' ,   62, 107,  46, 255)
  set_color('sys2_red'   ,  154,  63,  37, 255)

  # Sidewalk Chalk
  set_color('sys3_red',    0xED, 0x77, 0x77)
  set_color('sys3_green',  0xBC, 0xDF, 0x8A)
  set_color('sys3_blue',   0x94, 0xC0, 0xCC)
  set_color('sys3_yellow', 0xF5, 0xF9, 0xAD)
  set_color('sys3_orange', 0xFA, 0xD4, 0x8B)

  # system messages
  set_color('sys4_red',    0xFF, 0x94, 0x94)
  set_color('sys4_green',  0x94, 0xFF, 0xA6)
  set_color('sys4_blue',   0x94, 0xD6, 0xFF)
  set_color('sys4_yellow', 0xFF, 0xFB, 0x94)
  set_color('sys4_orange', 0xFF, 0xCE, 0x83)
end
