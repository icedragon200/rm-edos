#
# EDOS/lib/module/puppet.rb
#   by IceDragon
#   dc 03/04/2013
#   dm 03/04/2013
# vr 1.0.0
module Puppet

  class Sequence

    attr_accessor :id
    attr_reader :keyframes, :frame_index

    def initialize(id=0, keyframes)
      @id = 0
      @frame_index = 0
      @keyframes = keyframes
      @frame_count = @keyframes.size
    end

    def frame
      @frame_index >= 0 ? @keyframes[@frame_index] : nil
    end

    def wrap
      @frame_index %= @keyframes.size.max(1)
      frame
    end

    def prev
      @frame_index -= 1
      frame
    end

    def next
      @frame_index += 1
      frame
    end

    def reset
      @frame_index = 0
      frame
    end

  end

  class Sequence::Keyframe

    attr_accessor :image_index, :relative_move, :hotspot

    def initialize(image_index=nil, relative_move=nil, hotspot=nil)
      @image_index   = image_index
      @relative_move = relative_move
      @hotspot       = hotspot
    end

    def relative_move_x
      @relative_move.x
    end

    def relative_move_x=(n)
      @relative_move.x = n
    end

    def relative_move_y
      @relative_move.y
    end

    def relative_move_y=(n)
      @relative_move.y = n
    end

    def hotspot_x
      @hotspot.x
    end

    def hotspot_x=(n)
      @hotspot.x = n
    end

    def hotspot_y
      @hotspot.y
    end

    def hotspot_y=(n)
      @hotspot.y = n
    end

  end

  class Sequence::Maker

    attr_reader :frames

    def initialize
      @frames = []
    end

    def render
      src_frames = block_given? ? yield(@frames) : @frames
      prev_start_index = 0
      frame_start_index = []
      result_frames = []
      # FrameStruct [int delay, Symbol param, param_args[], Hash keyframe_args]
      # First fill the result_frames with duds and basic Keyframe(s)
      src_frames.each_with_index do |frm, i|
        delay, _, _, keyframe_args = frm
        delay.times do |_|
          keyframe = Sequence::Keyframe.new()
          keyframe_args.each_pair do |k ,v|
            keyframe.send(k.to_s + "=", v)
          end
          result_frames.push(keyframe)
        end
        frame_start_index[i] = prev_start_index
        prev_start_index += delay
      end
      # fine tune the Keyframe(s)
      src_frames.each_with_index do |frm, i|
        delay, param, param_args, _ = frm
        case param
        # TweenStruct [Easer easer, Array<Symbol> what,
        #              int start_frame, int end_frame]
        when :tween
          easer, what, start_frame, end_frame = param_args
          fstrt_index = frame_start_index[i + start_frame]
          fend_index  = frame_start_index[i + end_frame]
          fstrt, fend = result_frames[fstrt_index], result_frames[fend_index]
          p fstrt_index, fend_index
          pairs = what.map { |obj| [fstrt.send(obj), fend.send(obj)] }
          tween = MACL::Tween2.new(delay, easer, *pairs)
          tween_result = tween.to_a.map { |res| what.zip(res) }
          for x in 0...delay
            frm = result_frames[frame_start_index[i] + x]
            tween_result[x].each { |(key, val)| frm.send(key.to_s + "=", val) }
          end
        end
      end
      Sequence.new(0, result_frames)
    end

    def new_frame(delay, param, param_args, keyframe_args)
      frame = [delay, param, param_args, keyframe_args]
      @frames.push(frame)
      return frame
    end

    def new_frames(*frames)
      frames.each { |frm| new_frame(*frm) }
    end

    def repeat_frames(num)
      source = @frames.dup
      num.times do |i|
        @frames.concat(source.deep_clone)
      end
    end

  end

end
