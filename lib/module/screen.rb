#
# EDOS/lib/module/screen.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.1.0
module Screen

  ##
  # init
  def self.init
    @panel = Rect.new(0, 0, Graphics.width, Metric.ui_element_sml)
    @content = Rect.new(0, 0, Graphics.width, Graphics.height)
    @content.height -= @panel.height
    @menu_content = @content.dup
    @menu_content.height -= Metric.ui_element + Metric.ui_element_sml
    @menu_help = Rect.new(@menu_content.x, @menu_content.y2,
                          @menu_content.width, Metric.ui_element_sml)
    @menu_title = Rect.new(@menu_help.x, @menu_help.y2,
                           @menu_help.width, Metric.ui_element)
    ## panel to top
    #@content.y += @panel.height
    ## panel to bottom
    @panel.y = @content.y2

    @panel.freeze
    @menu_content.freeze
    @content.freeze
    @menu_help.freeze
    @menu_title.freeze
  end

  class << self
    attr_accessor :panel
    attr_accessor :content
    attr_accessor :menu_content
    attr_accessor :menu_help
    attr_accessor :menu_title
  end

end
