require 'rubygems'
require 'rbconfig'
require '/home/icy/Dropbox/code/Git/ruby-mkrf/lib/mkrf'

class Mkrf::Generator

  def pkg_config(name)
    cflags << ' '  + `pkg-config #{name} --cflags`.chomp
    ldshared << ' ' + `pkg-config #{name} --libs`.chomp
  end

end

def extension(name)
  Mkrf::Generator.new(name, ["*.c", "*.cpp"]) do |g|
    #g.cflags.gsub!(/\-O2/, '')
    g.cflags << ' -fpermissive'
    g.cflags << ' -std=c++11'
    g.cflags << " -I" << File.expand_path(File.join(File.dirname(__FILE__), "..", "include"))
    g.ldshared << ' -lstdc++'
    yield g
  end
end