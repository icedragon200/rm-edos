#ifndef MYTHRYL_MATRIX_H_
#define MYTHRYL_MATRIX_H_
#include "mythryl.hpp"

namespace Mythryl
{
  class Matrix
  {
   public:
    //
    uint32_t xsize;
    uint32_t ysize;
    int16_t** data;
    //
    Matrix();
    Matrix(uint32_t _xsize, uint32_t _ysize);
    ~Matrix();

    void fill(int16_t value);
    int16_t get(uint32_t x, uint32_t y);
    void set(uint32_t x, uint32_t y, int16_t value);
    void resize(uint32_t _xsize, uint32_t _ysize);
    void copy_matrix(Matrix* matrix);
   private:
    void free_data();
  };
}

extern "C"
{
  void Mythryl_Matrix_free(Mythryl::Matrix* pnt);
  VALUE rb_aMythryl_Matrix_alloc(VALUE obj);
  VALUE rb_fMythryl_Matrix_initialize(VALUE self, VALUE rb_v_x, VALUE rb_v_y);
  VALUE rb_fMythryl_Matrix_initialize_copy(VALUE self, VALUE org);
  VALUE rb_fMythryl_Matrix_fill(VALUE self, VALUE rb_v_v);
  VALUE rb_fMythryl_Matrix_get(VALUE self, VALUE rb_v_x, VALUE rb_v_y);
  VALUE rb_fMythryl_Matrix_set(VALUE self, VALUE rb_v_x, VALUE rb_v_y, VALUE rb_v_v);
  VALUE rb_fMythryl_Matrix_xsize(VALUE self);
  VALUE rb_fMythryl_Matrix_ysize(VALUE self);
  VALUE rb_fsMythryl_Matrix__load(VALUE self);
  VALUE rb_fMythryl_Matrix__dump(VALUE self);
  void Init_matrix();
}

#endif