#ifndef MYTHRYL_VECTOR_H_
#define MYTHRYL_VECTOR_H_
#include "mythryl.hpp"

namespace Mythryl
{
  class Vector
  {
   public:
    //
    uint32_t xsize;
    int16_t* data;
    //
    Vector();
    Vector(uint32_t _xsize);
    ~Vector();

    void fill(int16_t value);
    int16_t get(uint32_t x);
    void set(uint32_t x, int16_t value);
    void resize(uint32_t _xsize);
    void copy_vector(Vector* vector);
   private:
    void free_data();
  };
}

#endif