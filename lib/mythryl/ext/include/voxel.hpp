#ifndef MYTHRYL_VOXEL_H_
#define MYTHRYL_VOXEL_H_
#include "mythryl.hpp"

namespace Mythryl
{
  class Voxel
  {
   public:
    //
    uint32_t xsize;
    uint32_t ysize;
    uint32_t zsize;
    int16_t*** data;
    //
    Voxel();
    Voxel(uint32_t _xsize, uint32_t _ysize, uint32_t _zsize);
    ~Voxel();

    void fill(int16_t value);
    int16_t get(uint32_t x, uint32_t y, uint32_t z);
    void set(uint32_t x, uint32_t y, uint32_t z, int16_t value);
    void resize(uint32_t _xsize, uint32_t _ysize, uint32_t _zsize);
    void copy_voxel(Voxel* voxel);
   private:
    void free_data();
  };
}

#endif