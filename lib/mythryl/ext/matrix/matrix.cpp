#include "matrix.hpp"

namespace Mythryl
{
  Matrix::Matrix()
  {
    xsize = 1;
    ysize = 1;
    data = NULL;
  }

  Matrix::Matrix(uint32_t _xsize, uint32_t _ysize)
  {
    resize(_xsize, _ysize);
  }

  Matrix::~Matrix()
  {
    free_data();
  }

  void Matrix::free_data()
  {
    if (data) {
      for (uint32_t y = 0; y < ysize; y++) {
        delete [] data[y];
      }
      delete [] data;
    }
  }

  void Matrix::fill(int16_t value)
  {
    for (uint32_t y = 0; y < ysize; y++) {
      for (uint32_t x = 0; x < xsize; x++) {
        data[y][x] = value;
      }
    }
  }

  void Matrix::resize(uint32_t xs, uint32_t ys)
  {
    free_data();
    xsize = xs;
    ysize = ys;
    data = new int16_t*[ysize];
    for (uint32_t y = 0; y < ysize; y++) {
      data[y] = new int16_t[xsize];
    }
    fill(0);
  }

  void Matrix::copy_matrix(Matrix* src_matrix)
  {
    resize(src_matrix->xsize, src_matrix->ysize);
    for (uint32_t y = 0; y < ysize; y++) {
      for (uint32_t x = 0; x < xsize; x++) {
        data[y][x] = src_matrix->get(x, y);
      }
    }
  }

  int16_t Matrix::get(uint32_t x, uint32_t y)
  {
    return data[y % ysize][x % xsize];
  }

  void Matrix::set(uint32_t x, uint32_t y, int16_t value)
  {
    data[y % ysize][x % xsize] = value;
  }
}

extern "C"
{
  VALUE rb_cMythryl_Matrix;

  void Mythryl_Matrix_free(Mythryl::Matrix* pnt)
  {
    delete pnt;
  }

  VALUE rb_aMythryl_Matrix_alloc(VALUE obj)
  {
    Mythryl::Matrix* matrix = new Mythryl::Matrix();
    return Data_Wrap_Struct(obj, NULL, Mythryl_Matrix_free, matrix);
  }

  VALUE rb_fMythryl_Matrix_initialize(VALUE self, VALUE rb_v_x, VALUE rb_v_y)
  {
    uint32_t xsize;
    uint32_t ysize;
    Mythryl::Matrix* matrix;
    xsize = NUM2INT(rb_v_x);
    ysize = NUM2INT(rb_v_y);
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    matrix->resize(xsize, ysize);
    return Qnil;
  }

  VALUE rb_fMythryl_Matrix_initialize_copy(VALUE self, VALUE org)
  {
    Mythryl::Matrix* self_matrix;
    Mythryl::Matrix* org_matrix;
    Data_Get_Struct(self, Mythryl::Matrix, self_matrix);
    Data_Get_Struct(org, Mythryl::Matrix, org_matrix);
    self_matrix->copy_matrix(org_matrix);
    return Qnil;
  }

  VALUE rb_fMythryl_Matrix_fill(VALUE self, VALUE rb_v_v)
  {
    Mythryl::Matrix* matrix;
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    matrix->fill(NUM2INT(rb_v_v));
    return Qnil;
  }

  VALUE rb_fMythryl_Matrix_get(VALUE self, VALUE rb_v_x, VALUE rb_v_y)
  {
    uint32_t x;
    uint32_t y;
    Mythryl::Matrix* matrix;
    x = NUM2INT(rb_v_x);
    y = NUM2INT(rb_v_y);
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    return INT2NUM((uint32_t)matrix->get(x, y));
  }

  VALUE rb_fMythryl_Matrix_set(VALUE self, VALUE rb_v_x, VALUE rb_v_y, VALUE rb_v_v)
  {
    uint32_t x;
    uint32_t y;
    int16_t v;
    Mythryl::Matrix* matrix;
    x = NUM2INT(rb_v_x);
    y = NUM2INT(rb_v_y);
    v = NUM2INT(rb_v_v);
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    matrix->set(x, y, v);
    return Qnil;
  }

  VALUE rb_fMythryl_Matrix_xsize(VALUE self)
  {
    Mythryl::Matrix* matrix;
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    return INT2NUM(matrix->xsize);
  }

  VALUE rb_fMythryl_Matrix_ysize(VALUE self)
  {
    Mythryl::Matrix* matrix;
    Data_Get_Struct(self, Mythryl::Matrix, matrix);
    return INT2NUM(matrix->ysize);
  }

  VALUE rb_fsMythryl_Matrix__load(VALUE self)
  {
    return Qnil;
  }

  VALUE rb_fMythryl_Matrix__dump(VALUE self)
  {
    return Qnil;
  }

  void Init_matrix()
  {
    VALUE rb_mMythryl;
    rb_mMythryl = rb_define_module("Mythryl");
    rb_cMythryl_Matrix = rb_define_class_under(rb_mMythryl, "Matrix", rb_cObject);

    rb_define_alloc_func(rb_cMythryl_Matrix, rb_aMythryl_Matrix_alloc);
    rb_define_singleton_method(rb_cMythryl_Matrix, "_load", rb_fsMythryl_Matrix__load, 1);
    rb_define_method(rb_cMythryl_Matrix, "_dump", rb_fMythryl_Matrix__dump, 1);

    rb_define_private_method(rb_cMythryl_Matrix, "initialize", rb_fMythryl_Matrix_initialize, 2);
    rb_define_private_method(rb_cMythryl_Matrix, "initialize_copy", rb_fMythryl_Matrix_initialize_copy, 1);

    rb_define_method(rb_cMythryl_Matrix, "fill", rb_fMythryl_Matrix_fill, 1);
    rb_define_method(rb_cMythryl_Matrix, "get",  rb_fMythryl_Matrix_get, 2);
    rb_define_method(rb_cMythryl_Matrix, "set",  rb_fMythryl_Matrix_set, 3);

    rb_define_method(rb_cMythryl_Matrix, "xsize", rb_fMythryl_Matrix_xsize, 0);
    rb_define_method(rb_cMythryl_Matrix, "ysize", rb_fMythryl_Matrix_ysize, 0);

    rb_define_alias(rb_cMythryl_Matrix, "[]", "get");
    rb_define_alias(rb_cMythryl_Matrix, "[]=", "set");
  }
}