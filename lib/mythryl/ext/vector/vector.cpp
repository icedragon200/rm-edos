#include "vector.hpp"

namespace Mythryl
{
  Vector::Vector()
  {
    xsize = 1;
    data = NULL;
  }

  Vector::Vector(uint32_t _xsize)
  {
    resize(_xsize);
  }

  Vector::~Vector()
  {
    free_data();
  }

  void Vector::free_data()
  {
    if (data) {
      delete [] data;
    }
  }

  void Vector::fill(int16_t value)
  {
    for (uint32_t x = 0; x < xsize; x++) {
      data[x] = value;
    }
  }

  void Vector::resize(uint32_t xs)
  {
    free_data();
    xsize = xs;
    data = new int16_t[xsize];
    fill(0);
  }

  void Vector::copy_vector(Vector* src_vector)
  {
    resize(src_vector->xsize);
    for (uint32_t x = 0; x < xsize; x++) {
      data[x] = src_vector->get(x);
    }
  }

  int16_t Vector::get(uint32_t x)
  {
    return data[x % xsize];
  }

  void Vector::set(uint32_t x, int16_t value)
  {
    data[x % xsize] = value;
  }
}

extern "C"
{
  VALUE rb_cMythryl_Vector;

  void Mythryl_Vector_free(Mythryl::Vector* pnt)
  {
    delete pnt;
  }

  VALUE rb_aMythryl_Vector_alloc(VALUE obj)
  {
    Mythryl::Vector* vector = new Mythryl::Vector();
    return Data_Wrap_Struct(obj, NULL, Mythryl_Vector_free, vector);
  }

  VALUE rb_fMythryl_Vector_initialize(VALUE self, VALUE rb_v_x)
  {
    uint32_t xsize;
    Mythryl::Vector* vector;
    xsize = NUM2INT(rb_v_x);
    Data_Get_Struct(self, Mythryl::Vector, vector);
    vector->resize(xsize);
    return Qnil;
  }

  VALUE rb_fMythryl_Vector_initialize_copy(VALUE self, VALUE org)
  {
    Mythryl::Vector* self_vector;
    Mythryl::Vector* org_vector;
    Data_Get_Struct(self, Mythryl::Vector, self_vector);
    Data_Get_Struct(org, Mythryl::Vector, org_vector);
    self_vector->copy_vector(org_vector);
    return Qnil;
  }

  VALUE rb_fMythryl_Vector_fill(VALUE self, VALUE rb_v_v)
  {
    Mythryl::Vector* vector;
    Data_Get_Struct(self, Mythryl::Vector, vector);
    vector->fill(NUM2INT(rb_v_v));
    return Qnil;
  }

  VALUE rb_fMythryl_Vector_get(VALUE self, VALUE rb_v_x)
  {
    uint32_t x;
    Mythryl::Vector* vector;
    x = NUM2INT(rb_v_x);
    Data_Get_Struct(self, Mythryl::Vector, vector);
    return INT2NUM((uint32_t)vector->get(x));
  }

  VALUE rb_fMythryl_Vector_set(VALUE self, VALUE rb_v_x, VALUE rb_v_v)
  {
    uint32_t x;
    int16_t v;
    Mythryl::Vector* vector;
    x = NUM2INT(rb_v_x);
    v = NUM2INT(rb_v_v);
    Data_Get_Struct(self, Mythryl::Vector, vector);
    vector->set(x, v);
    return Qnil;
  }

  VALUE rb_fMythryl_Vector_xsize(VALUE self)
  {
    Mythryl::Vector* vector;
    Data_Get_Struct(self, Mythryl::Vector, vector);
    return INT2NUM(vector->xsize);
  }

  VALUE rb_fsMythryl_Vector__load(VALUE self)
  {
    return Qnil;
  }

  VALUE rb_fMythryl_Vector__dump(VALUE self)
  {
    return Qnil;
  }

  void Init_vector()
  {
    VALUE rb_mMythryl;
    rb_mMythryl = rb_define_module("Mythryl");
    rb_cMythryl_Vector = rb_define_class_under(rb_mMythryl, "Vector", rb_cObject);

    rb_define_alloc_func(rb_cMythryl_Vector, rb_aMythryl_Vector_alloc);

    rb_define_singleton_method(rb_cMythryl_Vector, "_load", rb_fsMythryl_Vector__load, 1);
    rb_define_method(rb_cMythryl_Vector, "_dump", rb_fMythryl_Vector__dump, 1);

    rb_define_private_method(rb_cMythryl_Vector, "initialize", rb_fMythryl_Vector_initialize, 1);
    rb_define_private_method(rb_cMythryl_Vector, "initialize_copy", rb_fMythryl_Vector_initialize_copy, 1);

    rb_define_method(rb_cMythryl_Vector, "fill", rb_fMythryl_Vector_fill, 1);
    rb_define_method(rb_cMythryl_Vector, "get",  rb_fMythryl_Vector_get, 1);
    rb_define_method(rb_cMythryl_Vector, "set",  rb_fMythryl_Vector_set, 2);

    rb_define_method(rb_cMythryl_Vector, "xsize", rb_fMythryl_Vector_xsize, 0);

    rb_define_alias(rb_cMythryl_Vector, "[]", "get");
    rb_define_alias(rb_cMythryl_Vector, "[]=", "set");
    rb_define_alias(rb_cMythryl_Vector, "size", "xsize");
  }
}