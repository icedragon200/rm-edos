#include "voxel.hpp"

namespace Mythryl
{
  Voxel::Voxel()
  {
    xsize = 1;
    ysize = 1;
    zsize = 1;
    data = NULL;
  }

  Voxel::Voxel(uint32_t _xsize, uint32_t _ysize, uint32_t _zsize)
  {
    resize(_xsize, _ysize, _zsize);
  }

  Voxel::~Voxel()
  {
    free_data();
  }

  void Voxel::free_data()
  {
    if (data) {
      for (uint32_t z = 0; z < zsize; z++) {
        for (uint32_t y = 0; y < ysize; y++) {
          delete [] data[z][y];
        }
        delete [] data[z];
      }
      delete [] data;
      data = NULL;
    }
  }

  void Voxel::fill(int16_t value)
  {
    for (uint32_t z = 0; z < zsize; z++) {
      for (uint32_t y = 0; y < ysize; y++) {
        for (uint32_t x = 0; x < xsize; x++) {
          data[z][y][x] = value;
        }
      }
    }
  }

  void Voxel::resize(uint32_t xs, uint32_t ys, uint32_t zs)
  {
    free_data();
    xsize = xs;
    ysize = ys;
    zsize = zs;
    data = new int16_t**[zsize];
    for (uint32_t z = 0; z < zsize; ++z) {
      data[z] = new int16_t*[ysize];
      for (uint32_t y = 0; y < ysize; ++y) {
        data[z][y] = new int16_t[xsize];
      }
    }
    fill(0);
  }

  void Voxel::copy_voxel(Voxel* src_voxel)
  {
    resize(src_voxel->xsize, src_voxel->ysize, src_voxel->zsize);
    for (uint32_t z = 0; z < zsize; z++) {
      for (uint32_t y = 0; y < ysize; y++) {
        for (uint32_t x = 0; x < xsize; x++) {
          data[z][y][x] = src_voxel->get(x, y, z);
        }
      }
    }
  }

  int16_t Voxel::get(uint32_t x, uint32_t y, uint32_t z)
  {
    return data[z % zsize][y % ysize][x % xsize];
  }

  void Voxel::set(uint32_t x, uint32_t y, uint32_t z, int16_t value)
  {
    data[z % zsize][y % ysize][x % xsize] = value;
  }
}

extern "C"
{
  VALUE rb_cMythryl_Voxel;

  void Mythryl_Voxel_free(Mythryl::Voxel* pnt)
  {
    delete pnt;
  }

  VALUE rb_aMythryl_Voxel_alloc(VALUE obj)
  {
    Mythryl::Voxel* voxel = new Mythryl::Voxel();
    return Data_Wrap_Struct(obj, NULL, Mythryl_Voxel_free, voxel);
  }

  VALUE rb_fMythryl_Voxel_initialize(VALUE self,
                                     VALUE rb_v_x, VALUE rb_v_y, VALUE rb_v_z)
  {
    uint32_t xsize;
    uint32_t ysize;
    uint32_t zsize;
    Mythryl::Voxel* voxel;
    xsize = NUM2INT(rb_v_x);
    ysize = NUM2INT(rb_v_y);
    zsize = NUM2INT(rb_v_z);
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    voxel->resize(xsize, ysize, zsize);
    return Qnil;
  }

  VALUE rb_fMythryl_Voxel_initialize_copy(VALUE self, VALUE org)
  {
    Mythryl::Voxel* self_voxel;
    Mythryl::Voxel* org_voxel;
    Data_Get_Struct(self, Mythryl::Voxel, self_voxel);
    Data_Get_Struct(org, Mythryl::Voxel, org_voxel);
    self_voxel->copy_voxel(org_voxel);
    return Qnil;
  }

  VALUE rb_fMythryl_Voxel_fill(VALUE self, VALUE rb_v_v)
  {
    Mythryl::Voxel* voxel;
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    voxel->fill(NUM2INT(rb_v_v));
    return Qnil;
  }

  VALUE rb_fMythryl_Voxel_get(VALUE self,
                              VALUE rb_v_x, VALUE rb_v_y, VALUE rb_v_z)
  {
    uint32_t x;
    uint32_t y;
    uint32_t z;
    Mythryl::Voxel* voxel;
    x = NUM2INT(rb_v_x);
    y = NUM2INT(rb_v_y);
    z = NUM2INT(rb_v_z);
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    return INT2NUM((uint32_t)voxel->get(x, y, z));
  }

  VALUE rb_fMythryl_Voxel_set(VALUE self,
                              VALUE rb_v_x, VALUE rb_v_y, VALUE rb_v_z,
                              VALUE rb_v_v)
  {
    uint32_t x;
    uint32_t y;
    uint32_t z;
    int16_t v;
    Mythryl::Voxel* voxel;
    x = NUM2INT(rb_v_x);
    y = NUM2INT(rb_v_y);
    z = NUM2INT(rb_v_z);
    v = NUM2INT(rb_v_v);
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    voxel->set(x, y, z, v);
    return Qnil;
  }

  VALUE rb_fMythryl_Voxel_xsize(VALUE self)
  {
    Mythryl::Voxel* voxel;
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    return INT2NUM(voxel->xsize);
  }

  VALUE rb_fMythryl_Voxel_ysize(VALUE self)
  {
    Mythryl::Voxel* voxel;
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    return INT2NUM(voxel->ysize);
  }

  VALUE rb_fMythryl_Voxel_zsize(VALUE self)
  {
    Mythryl::Voxel* voxel;
    Data_Get_Struct(self, Mythryl::Voxel, voxel);
    return INT2NUM(voxel->zsize);
  }

  VALUE rb_fsMythryl_Voxel__load(VALUE self)
  {
    return Qnil;
  }

  VALUE rb_fMythryl_Voxel__dump(VALUE self)
  {
    return Qnil;
  }

  void Init_voxel()
  {
    VALUE rb_mMythryl;
    rb_mMythryl = rb_define_module("Mythryl");
    rb_cMythryl_Voxel = rb_define_class_under(rb_mMythryl, "Voxel", rb_cObject);

    rb_define_alloc_func(rb_cMythryl_Voxel, rb_aMythryl_Voxel_alloc);
    rb_define_singleton_method(rb_cMythryl_Voxel, "_load", rb_fsMythryl_Voxel__load, 1);
    rb_define_method(rb_cMythryl_Voxel, "_dump", rb_fMythryl_Voxel__dump, 1);

    rb_define_private_method(rb_cMythryl_Voxel, "initialize", rb_fMythryl_Voxel_initialize, 3);
    rb_define_private_method(rb_cMythryl_Voxel, "initialize_copy", rb_fMythryl_Voxel_initialize_copy, 1);

    rb_define_method(rb_cMythryl_Voxel, "fill", rb_fMythryl_Voxel_fill, 1);
    rb_define_method(rb_cMythryl_Voxel, "get",  rb_fMythryl_Voxel_get, 3);
    rb_define_method(rb_cMythryl_Voxel, "set",  rb_fMythryl_Voxel_set, 4);

    rb_define_method(rb_cMythryl_Voxel, "xsize", rb_fMythryl_Voxel_xsize, 0);
    rb_define_method(rb_cMythryl_Voxel, "ysize", rb_fMythryl_Voxel_ysize, 0);
    rb_define_method(rb_cMythryl_Voxel, "zsize", rb_fMythryl_Voxel_zsize, 0);

    rb_define_alias(rb_cMythryl_Voxel, "[]", "get");
    rb_define_alias(rb_cMythryl_Voxel, "[]=", "set");
  }
}