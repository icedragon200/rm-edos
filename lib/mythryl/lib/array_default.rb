#
# EDOS/lib/mythryl/lib/array_default.rb
#   ed IceDragon
# http://stackoverflow.com/questions/5324654/can-i-create-an-array-in-ruby-with-default-values
class ArrayDefault < Array

  attr_accessor :default
  attr_accessor :default_proc

  def [](index)
    if index.is_a? Range
      index.map { |i| self[i] }
    else
      fetch(index) { default_proc ? default_proc.call(index) : default }  # default to 0 if no element at index; will not cause auto-extension of array
    end
  end

end