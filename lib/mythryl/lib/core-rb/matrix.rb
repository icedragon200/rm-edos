#
# EDOS/lib/mythryl/matrix.rb
#   by IceDragon
# 2D Data Object
module Mythryl
  class Matrix

    attr_accessor :wrap_index

    attr_reader :xsize
    attr_reader :ysize

    def initialize(xsize, ysize, wrap_index=false)
      @xsize, @ysize = xsize, ysize
      @data = Array.new(ysize) { Array.new(xsize, 0) }
      @wrap_index = wrap_index
    end

    def adjust_index(x, y)
      if @wrap_index
        return x % xsize, y % ysize
      else
        raise IndexError, "x is out of range (0...#{xsize})" if x >= xsize || x < 0
        raise IndexError, "y is out of range (0...#{ysize})" if y >= ysize || y < 0
        return x, y
      end
    end

    def get(x, y)
      x, y = adjust_index(x, y)
      @data[y][x]
    end

    def set(x, y, v)
      x, y = adjust_index(x, y)
      @data[y][x] = v
    end

    alias :[] :get
    alias :[]= :set

    private :adjust_index

  end
end