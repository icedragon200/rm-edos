#
# EDOS/lib/mythryl/vector.rb
#   by IceDragon
# 1D Data Object
module Mythryl
  class Vector

    attr_accessor :wrap_index

    attr_reader :xsize

    def initialize(xsize, wrap_index=false)
      @xsize = xsize
      @data = Array.new(xsize, 0)
      @wrap_index = wrap_index
    end

    def adjust_index(x)
      if @wrap_index
        return x % xsize
      else
        raise IndexError, "x is out of range (0...#{xsize})" if x >= xsize || x < 0
        return x
      end
    end

    def get(x)
      x = adjust_index(x)
      @data[x]
    end

    def set(x, v)
      x = adjust_index(x)
      @data[x] = v
    end

    alias :[] :get
    alias :[]= :set

    private :adjust_index

  end
end