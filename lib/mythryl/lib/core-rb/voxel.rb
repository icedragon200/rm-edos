#
# EDOS/lib/mythryl/voxel.rb
#   by IceDragon
# 3D Data Object
module Mythryl
  class Voxel

    attr_accessor :wrap_index

    attr_reader :xsize
    attr_reader :ysize
    attr_reader :zsize

    def initialize(xsize, ysize, zsize, wrap_index=false)
      @xsize, @ysize, @zsize = xsize, ysize, zsize
      @data = Array.new(zsize) { Array.new(ysize) { Array.new(xsize, 0) } }
      @wrap_index = wrap_index
    end

    def adjust_index(x, y, z)
      if @wrap_index
        return x % xsize, y % ysize, z % zsize
      else
        raise IndexError, "x is out of range (0...#{xsize})" if x >= xsize || x < 0
        raise IndexError, "y is out of range (0...#{ysize})" if y >= ysize || y < 0
        raise IndexError, "z is out of range (0...#{zsize})" if z >= zsize || z < 0
        return x, y, z
      end
    end

    def get(x, y, z)
      x, y, z = adjust_index(x, y, z)
      @data[z][y][x]
    end

    def set(x, y, z, v)
      x, y, z = adjust_index(x, y, z)
      @data[z][y][x] = v
    end

    alias :[] :get
    alias :[]= :set

    private :adjust_index

  end
end