#
# EDOS/lib/mythryl/scene_manager.rb
#   by IceDragon
# The Mythryl::SceneManager aims to replace the regular
module Mythryl
  class SceneManager

    attr_reader :scene
    attr_reader :background_bitmap

    def initialize
      @scene = nil
      @stack = []
      @background_bitmap = nil
    end

    def run
      @scene.main while @scene
    end

    def scene_is?(scene_klass)
      @scene.instance_of?(scene_klass)
    end

    def goto(scene_klass)
      @scene = scene_klass.new(self)
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:scene, "GOTO: #{@scene.to_s}"])
    end

    def call(scene_klass)
      @stack.push(@scene) # stash the current Scene
      @scene = scene_klass.new(self)
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:scene, "CALL: #{@scene.to_s}"])
    end

    def return
      @scene = @stack.pop
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, [:scene, "RETURN: #{@scene.to_s}"])
    end

    def recall
      goto(@scene.class)
    end

    def clear
      @stack.clear
    end

    def exit
      @scene = nil
    end

    def snapshot_for_background
      if @background_bitmap && !@background_bitmap.disposed?
        @background_bitmap.dispose
        @background_bitmap = nil
      end
      @background_bitmap = Graphics.snap_to_bitmap
      #@background_bitmap.blur
    end

  end
end