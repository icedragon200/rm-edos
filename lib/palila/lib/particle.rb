#
# src/palila/particle.rb
#   by IceDragon
# vr 0.10
class Palila::Particle

  attr_accessor :location
  attr_reader :life, :lifespan

  def initialize(loc_vec)
    @location = loc_vec.dup
    init
  end

  def init
    @acceleration = StarRuby::Vector2F.new(0, 0)
    @velocity = StarRuby::Vector2F.new(0, 0)
    init_lifespan
  end

  def init_lifespan
    @life = @lifespan = 255
  end

  def kill!
    @life = 0
  end

  def x
    return @location.x
  end

  def y
    return @location.y
  end

  def update
    return false if @life <= 0
    @velocity.add!(@acceleration)
    @location.add!(@velocity)
    @life -= 1
  end

  def dead?
    return @life <= 0
  end

end
