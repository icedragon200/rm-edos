#
# src/palila/particle_system.rb
#   by IceDragon
# vr 0.10
class Palila::ParticleSystem

  attr_accessor :location
  attr_reader :particles

  def initialize
    @location = StarRuby::Vector2F.new(0, 0)
    @particles = []
  end

  def x
    return @location.x
  end

  def y
    return @location.y
  end

  def kill!
    @particles.each(&:kill!)
  end

  def add_particle(particle_klass)
    particle = particle_klass.new(@location)
    @particles.push(particle)
    return particle
  end

  def update
    update_particles
  end

  def update_particles
    if block_given?
      @particles.reject! do |particle|
        particle.update
        yield particle unless particle.dead?
        particle.dead?
      end
    else
      @particles.reject! do |particle|
        particle.update
        particle.dead?
      end
    end
  end

  def particles?
    return !@particles.empty?
  end

end
