#
# EDOS/lib/palila/util.rb
#   dc 26/05/2013
#   dm 26/05/2013
# vr 1.1.0
module Palila
  module Util

    ### constants
    RANDF_I = 0xFFFF
    RANDF_F = RANDF_I.to_f

    ##
    # range_random(Integer min, Integer max)
    def range_random(min, max)
      return min + rand(max - min + 1) #- max
    end

    ##
    # range_randomf(Float min, Float max)
    def range_randomf(min, max)
      return min + rand((max - min) * RANDF_I) / RANDF_F
    end

    ##
    # lerp(Object a, Object b, Float d)
    def lerp(a, b, d)
      return a + d * (b - a)
    end

    ##
    # factorial(n)
    def factorial(n)
      if n == 0 then
        return 1
      else
        return n * factorial(n - 1)
      end
    end

    ##
    # sum(Array<Integer> ns*)
    def sum(*ns)
      return ns.inject(&:+)
    end

    ##
    # average(Array<Integer> ns*)
    def average(*ns)
      return ns.inject(&:+) / ns.size
    end

    ##
    # clamp(Integer n, Integer mn, Integer mx)
    def clamp(n, mn, mx)
      if    n < mn then n = mn
      elsif n > mx then n = mx end
      return n
    end

    ##
    # clamp_wrap(Integer n, Integer mn, Integer mx)
    def clamp_wrap(n, mn, mx)
      if    n < mn then n = mx
      elsif n > mx then n = mn end
      return n
    end

    ##
    # min(Array<Integer> ns*)
    def min(*ns)
      return ns.min
    end

    ##
    # max(Array<Integer> ns*)
    def max(*ns)
      return ns.max
    end

    ##
    # mid(Array<Integer> ns*)
    def mid(*ns)
      half_index = ns.size / 2
      return ns.sort[half_index]
    end

    ##
    # step(Integer n, Integer stepn)
    def step(n, stepn)
      loop do
        yield n
        n += step
      end
      return n
    end

    ##
    # zero?(Integer n)
    def zero?(n)
      return n == 0
    end

    ##
    # negative?(Integer n)
    def negative?(n)
      return n < 0
    end

    ##
    # positive?(Integer n)
    def positive?(n)
      return n > 0
    end

    ##
    # signum(Integer n)
    def signum(n)
      return n <=> 0
    end

    ##
    # signum_inv(Integer n)
    def signum_inv(n)
      return -(n <=> 0)
    end

    ##
    # wall(Integer a, Integer b)
    def wall(a, b)
      return a > b ? (b - a) % b : a
    end

    ##
    # reflect(Integer a, Integer b1, Integer b2)
    def reflect(a, b1, b2)
      d = (b2 - b1).abs
      return ((a / d) % 2) == 0 ? b1 + (a % d) : b2 - (a % d)
    end

    extend self

  end
end