#
# src/palila/palila.rb
#   by IceDragon
# vr 0.1.1
#
# Palila
#   rm-srri Simple Physics
module Palila

  VERSION = "0.1.1".freeze

end

#%w(palila util vector2 vector2-exp particle particle_system).each do |fn|
%w(util particle particle_system).each do |fn|
  require(File.dirname(__FILE__) + '/lib/' + fn)
end
