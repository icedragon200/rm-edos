#
# EDOS/core/resolution.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 02/02/2013
#   dm 05/05/2013
# vr 1.1.0
#
# INSTRUCTIONS
#   Simply uncomment the resolution you wish to use
#
# DICTIONARY
#   Uncomment - Remove the # before the line
module EDOS

  ppi = 72 # pixel per inch

### SETUP RESOLUTION TABLE
  RESOLUTION = {
    # default
    "default" => [544, 416],
    # A Paper - Landscape

    "4A0" => [93.6 * ppi, 66.2 * ppi], # 4A0
    "2A0" => [66.2 * ppi, 46.8 * ppi], # 2A0
    "A0"  => [46.8 * ppi, 33.1 * ppi], # A0
    "A1"  => [33.1 * ppi, 23.4 * ppi], # A1
    "A2"  => [23.4 * ppi, 16.5 * ppi], # A2
    "A3"  => [16.5 * ppi, 11.7 * ppi], # A3
    "A4"  => [11.7 * ppi, 8.3 * ppi],  # A4
    "A5"  => [8.3 * ppi, 5.8 * ppi],   # A5
    "A6"  => [5.8 * ppi, 4.1 * ppi],   # A6
    "A7"  => [4.1 * ppi, 2.9 * ppi],   # A7
    "A8"  => [2.9 * ppi, 2.0 * ppi],   # A8
    "A9"  => [2.0 * ppi, 1.5 * ppi],   # A9
    "A10" => [1.5 * ppi, 1.0 * ppi],   # A10
    # SNES
    "snes progessive1" => [256, 224], # Progessive 1
    "snes progessive2" => [512, 224], # Progessive 2
    "snes progessive3" => [256, 239], # Progessive 3
    "snes progessive4" => [512, 239], # Progessive 4
    "snes interlace1"  => [512, 448], # Interlace 1
    "snes interlace2"  => [512, 478], # Interlace 2
    # GBA
    "gba" => [240, 160],
    # Nintendo DS
    "nintendo ds"      => [256, 192], # DS Original
    "nintendo ds xl"   => [320, 240], # DS XL / 3DS
    "nintendo 3ds top" => [800, 240], # 3DS Top
    # Standard
    "standard" => [640, 480],
    # EDOS default
    "edos default"  => [608, 444],
    "edos wide"     => [720, 400],
    "edos 16x"      => [320, 240],
    "edos2 default" => [800, 600],
    # ???
    "???1" => [600, 338],
    # ???2
    "???2" => [660, 372],
    # PSP
    "psp render" => [480, 272], # render
    "psp normal" => [480, 320], # normal
    "psp output" => [720, 480], # output
    # 16:9 Aspect Ratio
    "16:9 240p"  => [428, 240],   # 240p
    "16:9 320p"  => [570, 320],   # 320p
    "16:9 480p"  => [854, 480],   # 480p
    "16:9 720p"  => [1280, 720],  # 720p
    "16:9 1080p" => [1920, 1080], # 1080p
    # Computer Display Standards
    "comp qqvga"       => [160, 120], # QQVGA
    "comp ???1"        => [160, 128], # ???
    "comp ???2"        => [160, 144], # ???
    "comp hqvga"       => [240, 160], # HQVGA
    "comp qvga"        => [320, 240], # QVGA
    "comp wqvga"       => [480, 272], # WQVGA
    "comp tv low-end"  => [140, 192], # TV Computer low-end
    "comp tv typical"  => [320, 200], # TV Computer typical
    "comp tv high-end" => [640, 256], # TV Computer high-end
    "comp st colour"   => [640, 200], # ST Colour
    "comp st mono"     => [640, 400], # ST Mono
    "comp i/ni"        => [720, 480], # Video Monitor I/NI
  }

### SETUP RESOLUTION FX TABLE
  RESOLUTION_FX = {
    "null"        => ->(w, h) { [w, h] },
    # Aspect Ratio-fy
    "3:2"         => ->(w, h) { [h * ( 3.0 /  2), h] }, # 3:2
    "4:3"         => ->(w, h) { [h * ( 4.0 /  3), h] }, # 4:3
    "16:9"        => ->(w, h) { [h * (16.0 /  9), h] }, # 16:9
    "16:10"       => ->(w, h) { [h * (16.0 / 10), h] }, # 16:10
    # flip resolution
    "flip"        => ->(w, h) {  [h, w] },
    # square
    "sq sml"      => ->(w, h) {  w = h = [w, h].min; [w, h] }, # small
    "sq lrg"      => ->(w, h) {  w = h = [w, h].max; [w, h] }, # largest
    # dual screen
    "dual horz"   => ->(w, h) {  w *= 2; [w, h] }, # horizontal
    "dual vert"   => ->(w, h) {  h *= 2; [w, h] }, # vertical
    # add
    "w+16"        => ->(w, h) { w += 16; [w, h] }, #
    "w+24"        => ->(w, h) { w += 24; [w, h] }, #
    "w+32"        => ->(w, h) { w += 32; [w, h] }, #
    "h+16"        => ->(w, h) { h += 16; [w, h] }, #
    "h+24"        => ->(w, h) { h += 24; [w, h] }, #
    "h+32"        => ->(w, h) { h += 32; [w, h] }, #
    # sub
    "w-16"        => ->(w, h) { w -= 16; [w, h] }, #
    "w-24"        => ->(w, h) { w -= 24; [w, h] }, #
    "w-32"        => ->(w, h) { w -= 32; [w, h] }, #
    "h-16"        => ->(w, h) { h -= 16; [w, h] }, #
    "h-24"        => ->(w, h) { h -= 24; [w, h] }, #
    "h-32"        => ->(w, h) { h -= 32; [w, h] }, #
    # scale
    # look down \/
  }

  [0.10, 0.25, 0.30, 0.33, 0.40, 0.50,
   0.60, 0.66, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 0.99].each do |f|
    RESOLUTION_FX["scale %0.2fx" % f] = ->(w, h) { [w * f, h * f] }
    RESOLUTION_FX["scale %0.2fx horz" % f] = ->(w, h) { [w * f, h] }
    RESOLUTION_FX["scale %0.2fx vert" % f] = ->(w, h) { [w, h * f] }
  end

  [1, 2, 3, 4, 5, 8, 10, 16, 24, 32].each do |f|
    RESOLUTION_FX["scale %dx" % f] = ->(w, h) { [w * f.to_f, h * f.to_f] }
    RESOLUTION_FX["scale %dx horz" % f] = ->(w, h) { [w * f.to_f, h] }
    RESOLUTION_FX["scale %dx vert" % f] = ->(w, h) { [w, h * f.to_f] }
  end

  def self.apply_resolution_fx(w, h, *fx)
    fx.each { |nm| w, h = RESOLUTION_FX[nm].(w, h) }; return [w, h]
  end

  ##
  # ::change_resolution(String preset_name, ...Array<String> fx)
  # ::change_resolution(Array<Integer>[width, height] reso, ...Array<String> fx)
  def self.change_resolution(param, *fx)
    w, h = param.is_a?(String) ? RESOLUTION[param] : param
    w, h = apply_resolution_fx(w, h, *fx)
    Graphics.resize_screen(w.to_i, h.to_i)
  end

end