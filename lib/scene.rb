#
# EDOS/lib/scenes.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
module Scene
end

dir = File.dirname(__FILE__)
Dir.glob(File.join(dir, 'scene', '*.rb')).sort.each do |fn|
  require fn
end
