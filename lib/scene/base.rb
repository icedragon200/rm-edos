#
# EDOS/lib/scene/base.rb
#   by IceDragon
require_relative 'pre_base'
class Scene::Base < Scene::PreBase

  attr_reader :canvas
  attr_reader :window_manager

  def start
    super
    create_canvas
    create_window_manager
  end

  def terminate
    super
    dispose_window_manager
  end

  def update_basic
    super
    update_window_manager
  end

  def update_for_wait
    update_basic
  end

  def create_canvas
    @canvas = Rect.new(@viewport.rect)
  end

  def create_background(viewport=@viewport)
    @background = Sprite::MerioBackground.new(viewport, @canvas.width, @canvas.height)
    @background.z = -10
  end

  def create_window_manager
    @window_manager = WindowManager.new(viewport)
  end

  def dispose_background
    @background.dispose if @background
  end

  def dispose_window_manager
    @window_manager.dispose
  end

  def update_background
    #
  end

  def update_window_manager
    @window_manager.update
  end

  def wait(duration)
    duration.times do |i|
      update_for_wait
    end
  end

  def wait_do(duration)
    duration.times do |i|
      update_for_wait
      yield i if block_given?
    end
  end

  def wait_for_windows(*windows)
    update_for_wait while windows.any?(&:ajar?)
  end

  private :canvas
  private :window_manager

  # // 03/01/2012
  def pop_console_input
    input = Shell::InputConsole.new(0,0)
    input.z = 9999
    input.align_to!(anchor: 20, surface: @canvas)
    input.activate
    eval_handler = ->(text) do
      begin
        intp = (respond_to?(:_map) && _map) ? _map.interpreter : nil
        eval(text)
      rescue(Exception) => ex
        Input.update
        Keyboard.update
        puts("Eval failed: #{ex.message}")
        input.activate()
        #return
      else
        input.clear()
      end
    end
    input.set_handler(:eval, eval_handler)
    Input.update()
    create_dim_background()
    @dim_background_sprite.z = 9899
    @dim_background_sprite.z = input.z - 100
    #eval_handler.call(gets)
    loop do
      Graphics.update()
      Keyboard.update()
      input.update()
      break if Keyboard.trigger?(:ESC)
    end
    dispose_dim_background()
    Graphics.update()
    Input.update()
    Keyboard.update()
    input.dispose()
  end

  def self.mk_dispose_window(*names)
    names.each do |n|
      var = "@#{n}"
      define_method("dispose_"+n.to_s) do
        remove_window(instance_variable_get(var))
        instance_variable_set(var,nil)
      end
    end
  end

  def self.mk_showhide_window(*names)
    names.each do |n|
      var = "@#{n}"
      define_method("show_"+n.to_s) do
        open_and_activate_window(instance_variable_get(var))
      end
      define_method("hide_"+n.to_s) do
        close_and_deactivate_window(instance_variable_get(var))
      end
    end
  end

end