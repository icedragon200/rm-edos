#
# EDOS/lib/scene/loading.rb
#   by IceDragon
require_relative 'base'
class Scene::Loading < Scene::Base

  def start
    super
    @ticks = 0
    create_loading_sprite

    @done_loading = false
    @prep_thread = Thread.new { do_loading }
    @prep_thread.abort_on_exception = true
  end

  def create_loading_sprite
    @loading_sprite = Sprite.new
    almask = StarRuby::Texture::MASK_ALPHA
    b = Bitmap.new("media/spritesheet/block_loader_24f")
    @loading_sprite.bitmap = Bitmap.new(b.width, b.height)
    #@loading_sprite.bitmap.fill(Color.new(198, 198, 198))
    case rand(3)
    when 0
      @loading_sprite.bitmap.fill(Palette['droid_blue_light'])
    when 1
      @loading_sprite.bitmap.fill(Palette['droid_red_light'])
    when 2
      @loading_sprite.bitmap.fill(Palette['droid_green_light'])
    end
    @loading_sprite.bitmap.texture.mask(0, 0, almask, b.texture, b.rect, almask)
    b.dispose
    ##
    update_loading_sprite
    @loading_sprite.align_to!(anchor: 5, surface: canvas)
    @loading_sprite.opacity = 0
  end

  def terminate
    dispose_loading_sprite
    super
  end

  def dispose_loading_sprite
    @loading_sprite.dispose_all
  end

  def finish_loading
    sleep 0.5
    @done_loading = true
  end

  def do_loading
    #
  end

  def update
    if @done_loading && @loading_sprite.opacity <= 0
      return_scene
    end
    super
    update_loading_sprite
    @ticks += 1
  end

  def update_loading_sprite
    w = @loading_sprite.bitmap.width / 24
    h = @loading_sprite.bitmap.height
    d = Metric::Time.sec_to_frame(0.03333333333333333)
    @loading_sprite.src_rect.set(w * ((@ticks / d) % 24), 0, w, h)
    if @done_loading
      @loading_sprite.opacity -= 255.0 / Graphics.frame_rate
    else
      @loading_sprite.opacity += 255.0 / Graphics.frame_rate
    end
    @loading_sprite.update
  end

end