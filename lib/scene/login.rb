#
# EDOS/lib/scene/login.rb
#   by IceDragon
# Earthen doesn't really require a login scene, but its simply used for the
# final startup process
class Scene::Login < Scene::Base

  def start
    super
    @sprite = Sprite.new
    @sprite.bitmap = Bitmap.render_text(DrawExt::Merio.new_font(:light, :h1, :enb), 'Hello World', 1)
    @sprite.opacity = 0
    @sprite.align_to!(anchor: 28, surface: canvas)
    #@sprite.align_to!(anchor: 46, surface: canvas)
    @sprite.anchor_oxy!(anchor: 5)
    #@sprite.x += @sprite.ox
    #@sprite.y += @sprite.oy
    window_manager.add(@sprite)
    @pristine_state_h = @sprite.get_state_h
    #puts Mythryl::DataModel::Weapon.new(id: 1, features: [Mythryl::DataModel::BaseItem::Feature.new]).to_yaml
    @ticks = 0
  end

  def update
    super
    #@sprite.zoom_x = (@ticks.reflect(0, 30) - 15) / 15.0
    #@ticks += 1
    if Input.trigger?(:C)
      @sprite.remove_automation_by_id(@top_chain.id) if @top_chain
      @sprite.set_state_h(@pristine_state_h)
      @sprite.automata do |l|
        @top_chain = l.chained.automata do |lc|
          lc.parallel.automata do |lp|
            lp.move([0, 0], [canvas.width/2, 0], 30, :sine_in)
            lp.fade(0, 200, 30, :linear)
          end
          lc.parallel.automata do |lp|
            lp.move([0, 0], [@sprite.width, 0], 90, :linear)
            lp.chained.automata do |lc2|
              lc2.parallel.automata do |lp2|
                lp2.zoom([1, 1], [1.25, 1.25], 30, :back_in)
                lp2.fade(200, 255, 30, :linear)
              end
              lc2.time(30)
              lc2.parallel.automata do |lp2|
                lp2.zoom([1.25, 1.25], [1, 1], 30, :back_out)
                lp2.fade(255, 200, 30, :linear)
              end
            end
          end
          lc.parallel.automata do |lp|
            lp.move([0, 0], [canvas.width, 0], 30, :sine_out)
            lp.fade(200, 0, 30, :linear)
          end
        end
        #File.write('automata.yaml', @top_chain.static_render(StructSprite).to_yaml)
        #@top_chain.reset
      end
    end
  end

end