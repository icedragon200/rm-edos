#
# EDOS/lib/scene/map.rb
#   by IceDragon
class Scene::Map < Scene::Base

  def start
    super
    data_map = Mythryl::DataModel::Map.new(width: 32, height: 32)
    data_map.data.fill(96 + 64 + 22)
    @camera = Camera.new
    @world = Game::World.new
    @map = Game::Map.new(@world)
    @map.setup(data_map)
    @character = Game::Character.new
    #@character.change_character("Actor050000", 0)
    @character.change_character("new-golem", 0)
    @character.moveto(@map.width / 2, @map.height / 2, 0)
    @character.z = 120
    @map.add_character(@character)
    @camera.follow_target = @character
    @spriteset = Spriteset::Map.new
    @spriteset.map = @map
    @spriteset.camera = @camera
    @controller = Controller::CharacterPlayer.new(@character)
    @controller.activate

    @debug_position = Sprite.new(nil)
    @debug_position.bitmap = Bitmap.new(Screen.content.width, Metric.ui_element)
  end

  def update
    #if Mouse.press?(:left)
    #  pos = @camera.mouse_position
    #  @character.moveto(pos.x - @character.screen_ox,
    #                    pos.y - @character.screen_oy + 4,
    #                    0)
    #end
    @character.emo_id = "alert"        if Input.trigger?(:q)
    @character.emo_id = "anger"        if Input.trigger?(:w)
    @character.emo_id = "bulb"         if Input.trigger?(:e)
    @character.emo_id = "cobweb"       if Input.trigger?(:r)
    @character.emo_id = "confused"     if Input.trigger?(:t)
    @character.emo_id = "death"        if Input.trigger?(:y)
    @character.emo_id = "double_sweat" if Input.trigger?(:u)
    @character.emo_id = "fry"          if Input.trigger?(:i)
    @character.emo_id = "heart"        if Input.trigger?(:o)
    @character.emo_id = "heart_break"  if Input.trigger?(:p)
    @character.emo_id = "music"        if Input.trigger?(:a)
    @character.emo_id = "panic"        if Input.trigger?(:s)
    @character.emo_id = "paw"          if Input.trigger?(:d)
    @character.emo_id = "query"        if Input.trigger?(:f)
    @character.emo_id = "silent"       if Input.trigger?(:g)
    @character.emo_id = "speak"        if Input.trigger?(:h)
    @character.emo_id = "sweat"        if Input.trigger?(:j)
    @character.emo_id = "z"            if Input.trigger?(:k)
    @character.emo_id = "speak_loop"   if Input.trigger?(:l)
    @character.emo_id = "null"         if Input.trigger?(:n)
    @character.aftershadow_timeout = 15 if Input.trigger?(:m)
    @character.hp -= 1 if Input.press?(:minus)
    @character.hp += 1 if Input.press?(:equals)
    @character.mp -= 1 if Input.press?(:openbrackets)
    @character.mp += 1 if Input.press?(:closebrackets)

    @controller.update
    @map.update
    @world.update
    @camera.update
    @spriteset.update
    super
    pos = @character.position
    @debug_position.bitmap.clear
    @debug_position.bitmap.draw_text(@debug_position.bitmap.rect, "#{pos.x.to_i}, #{pos.y.to_i}, #{pos.z.to_i}", 1)
  end

end