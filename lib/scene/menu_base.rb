#
# EDOS/lib/scene/menu_base.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
require_relative 'base'
class Scene::MenuBase < Scene::Base

  attr_reader :menu_canvas

  ##
  # start
  def start
    super
    create_menu_canvas
    create_background
    window_manager.setup do |_|
      create_all_windows
    end
  end

  ##
  # terminate
  def terminate
    super
    dispose_background
  end

  ##
  # update
  def update
    update_background
    super
  end

  ##
  # title_text -> String
  def title_text
    "<UNNAMMED>"
  end

  def create_menu_canvas
    @menu_canvas = Screen.menu_content.dup
  end
  ##
  # create_all_windows
  def create_all_windows
    create_title_window
    create_help_window
  end

  ##
  # create_title_window
  def create_title_window
    @title_window = Sprite::MerioTitle.new(@viewport, title_text)
    @title_window.x = Screen.menu_title.x
    @title_window.y = Screen.menu_title.y
    #@title_window.align_to!(anchor: 2, surface: @canvas)
    window_manager.add(@title_window)
  end

  ##
  # create_help_window
  def create_help_window
    #Window::Help.new(0, 0, Graphics.width, 48)
    @help_window = Sprite::MerioHelp.new(@viewport, @canvas.width)
    @help_window.x = Screen.menu_help.x
    @help_window.y = Screen.menu_help.y
    #@help_window.align_to!(anchor: 2, surface: @canvas)
    #@help_window.y -= @title_window.height
    window_manager.add(@help_window)
  end

  private :menu_canvas

end

class Scene::MenuUnitBase < Scene::MenuBase

  def start
    @unit = $game.party.menu_unit
    super
  end

  def next_unit
    @unit = $game.party.menu_unit_next
    on_unit_change
  end

  def prev_unit
    @unit = $game.party.menu_unit_prev
    on_unit_change
  end

  ##
  # on_unit_change
  #   Overwrite this function in a subclass to react on unit changes.
  #   This function is normally used to refresh windows which rely on a
  #   Unit
  def on_unit_change
    #
  end

end