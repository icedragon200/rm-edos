#
# EDOS/lib/scene/pre_base.rb
#   by IceDragon
class Scene::PreBase

  ### instance_attributes
  attr_reader :viewport
  attr_reader :scene_manager

  ##
  # initialize(SceneManager scene_manager)
  def initialize(scene_manager)
    @scene_manager = scene_manager
  end

  def main
    start
    post_start
    update until scene_changing?
    pre_terminate
    terminate
  end

  def start
    create_viewport
  end

  def post_start
    perform_transition
    Input.update
  end

  def scene_changing?
    scene_manager.scene != self
  end

  def update_basic
    Main.update
  end

  def update
    update_basic
  end

  def pre_terminate
  end

  def terminate
    Graphics.freeze
    dispose_main_viewport
  end

  def perform_transition
    Graphics.transition(transition_speed)
  end

  def transition_speed
    return 10
  end

  def create_viewport
    @viewport = Viewport.new(*Screen.content.to_a)
    @viewport.z = 0
  end

  def dispose_main_viewport
    @viewport.dispose
  end

  def update_all_windows
    instance_variables.each do |varname|
      ivar = instance_variable_get(varname)
      ivar.update if ivar.is_a?(Window)
    end
  end

  def dispose_all_windows
    instance_variables.each do |varname|
      ivar = instance_variable_get(varname)
      ivar.dispose if ivar.is_a?(Window)
    end
  end

  def return_scene
    scene_manager.return
  end

  private :viewport

end