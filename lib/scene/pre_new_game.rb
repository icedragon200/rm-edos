#
# EDOS/lib/scene/pre_new_game.rb
#
require_relative 'loading'
class Scene::PreNewGame < Scene::Loading

  def start
    unless DataManager.last_savefile_index
      scene_manager.clear
      return scene_manager.goto(Scene::Title)
    end
    super
  end

  def do_loading
    DataManager.create_game_objects
    for i in 1..9
      $game.party.add_unit(i)
    end
    $game.party.setup_debug_party
    $game.system.savefile_index = DataManager.last_savefile_index
    $game.system.save_background_index = 1
    Graphics.frame_count = 0
    DataManager.save_game($game.system.savefile_index)
    finish_loading
  end

end