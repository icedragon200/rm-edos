#
# EDOS/lib/scene/shutdown.rb
#
require_relative 'base'
class Scene::Shutdown < Scene::Base

  def main
    scene_manager.clear
    scene_manager.exit
  end

end