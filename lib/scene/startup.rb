#
# src/scene/startup.rb
#
require_relative 'loading'
class Scene::Startup < Scene::Loading

  def do_loading
    #StarRuby::Game.current.fullscreen = true
    Main.update # refresh screen buffer
    #DataManager.init
    EDOS.on_startup
    p "Startup Complete!"
    finish_loading
  end

end