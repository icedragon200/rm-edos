#
# EDOS/lib/scene/test_base.rb
#   by IceDragon
#
require_relative 'menu_base'
class Scene::TestBase < Scene::MenuBase

  attr_reader :help_window
  attr_reader :title_window

  def start
    ### TEMP
    unless $game
      DataManager.create_game_objects
      for i in 1..9
        $game.party.add_unit(i)
      end
      $game.party.setup_debug_party
    end
    ###
    super
    @test_timer = Timer.new(0)
    @test_timer.deactivate
  end

  def update
    @test_timer.update
    super
    return_scene if @test_timer.idle? && @test_timer.active? || Input.sr_trigger?(:keyboard, :escape)
  end

  def stopwatch
    time = Time.now
    yield
    time_now = Time.now
    return "Time Taken: #{(time_now.to_f - time.to_f).round(4)}sec"
  end

  class << self
    def test_name
      self.name.snakecase.split('/').last
    end
  end

  private :help_window
  private :title_window

end