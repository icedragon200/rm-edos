class Sprite
  class Sheet < Sprite
  end
end
dir = File.dirname(__FILE__)
Dir.glob(File.join(dir, 'sprite', '*.rb')).sort.each do |fn|
  require fn
end