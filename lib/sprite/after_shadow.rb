class Sprite::AfterShadow < Sprite

  attr_reader :timeout
  attr_reader :maxtimeout

  def initialize(viewport, parent, timeout)
    super(viewport)
    self.bitmap = parent.bitmap
    copy(parent)
    @base_opacity = self.opacity
    @timeout = @maxtimeout = timeout.to_i
  end

  def timedout?
    @timeout < 0
  end

  def update
    super
    self.opacity = @base_opacity * @timeout / @maxtimeout
    @timeout -= 1
  end

end