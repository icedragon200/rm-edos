#
# EDOS/lib/sprite/balloon.rb
#   by IceDragon
class Sprite::Balloon < Sprite::Sheet

  def initial_cell_size
    Size2.new(32, 32)
  end

  def sheet_columns
    8
  end

  def sheet_bitmap
    Cache.system("balloon")
  end

end