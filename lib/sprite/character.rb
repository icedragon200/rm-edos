#
# EDOS/lib/sprite/character.rb
#   by IceDragon
class Sprite::Character < Sprite::Base

  extend Mixin::AttrDirty
  private *(attr_dirty :@character, :character_name, :character_index, :character_hue)

  def initialize(viewport: nil, character: nil)
    super(viewport)
    @ticks = 0
    @character = character
    init_character
    init_gauges
    init_shadow
    init_emo
    update
  end

  def init_character
    @character_name  = @character.character_name
    @character_hue   = @character.character_hue
    @character_index = @character.character_index
    refresh_bitmap
  end

  def init_gauges
    @gauges = []
    @gauges << UI::HpGauge.new(viewport) # HP
    @gauges << UI::MpGauge.new(viewport) # MP
  end

  def init_shadow
    @sprite_shadow = Sprite::CharacterShadow.new(self.viewport)
  end

  def init_emo
    @sprite_emo = Sprite::Emo.new(self.viewport, 0)
    @sprite_emo.visible = false
  end

  def dispose
    dispose_gauges
    dispose_shadow
    dispose_emo
    super
  end

  def dispose_gauges
    @gauges.each(&:dispose)
  end

  def dispose_shadow
    @sprite_shadow.dispose
  end

  def dispose_emo
    @sprite_emo.dispose
  end

  def update
    if @character
      update_bitmap
      update_character_index
      update_src_rect
      update_position
      update_gauges
      update_shadow
      update_emo
    end
    super
    @ticks += 1
  end

  def update_bitmap
    if character_name_changed? || character_hue_changed?
      @character_name  = @character.character_name
      @character_hue   = @character.character_hue
      refresh_bitmap
    end
  end

  def update_character_index
    character_index_changed?
  end

  def update_src_rect
    frame_index = @character.frame_index
    frame_cols  = @character.frame_cols
    frame_size  = @character.frame_size
    self.src_rect.set((frame_index % frame_cols) * frame_size.width,
                      (frame_index / frame_cols) * frame_size.height,
                      frame_size.width, frame_size.height)
  end

  def update_position
    self.x = @character.screen_x
    self.y = @character.screen_y
    self.z = @character.screen_z
    self.ox = @character.screen_ox
    self.oy = @character.screen_oy
  end

  def update_gauges
    @gauges.each do |g|
      g.resize(6, self.src_rect.height)
    end
    hp_gauge = @gauges[0]
    mp_gauge = @gauges[1]
    hp_gauge.rate = @character.hp_rate
    mp_gauge.rate = @character.mp_rate
    #gauge_h = hp_gauge.height
    char_h = src_rect.height
    #mp_gauge.x  = hp_gauge.x = self.x
    #mp_gauge.z  = hp_gauge.z = self.z
    #mp_gauge.y = self.y
    #hp_gauge.y = self.y - char_h - gauge_h * 2
    mp_gauge.z = hp_gauge.z = self.z
    hp_gauge.y = self.y - hp_gauge.oy
    mp_gauge.y = self.y - mp_gauge.oy
    hp_gauge.x = self.x - self.ox - hp_gauge.width / 2
    mp_gauge.x = self.x + self.ox - mp_gauge.width / 2
    @gauges.each(&:update)
  end

  def update_shadow
    shadow_pos   = @character.screen_shadow_position
    shadow_scale = @character.screen_shadow_scale
    @sprite_shadow.x = shadow_pos.x
    @sprite_shadow.y = shadow_pos.y
    @sprite_shadow.z = shadow_pos.z
    @sprite_shadow.ox = ox
    @sprite_shadow.oy = oy
    @sprite_shadow.zoom_x = shadow_scale.x
    @sprite_shadow.zoom_y = shadow_scale.y
    @sprite_shadow.opacity = @character.screen_shadow_opacity
    @sprite_shadow.update
  end

  def update_emo
    @sprite_emo.x = self.x
    @sprite_emo.y = self.y - self.src_rect.height
    @sprite_emo.z = self.z
    @sprite_emo.ox = self.ox
    @sprite_emo.oy = self.oy
    if id = @character.emo_id
      @sprite_emo.emo_id = id
      @character.emo_id = nil
    end
    @sprite_emo.update
  end

  def refresh_bitmap
    self.bitmap = Cache.character(@character_name, @character_hue)
    update_src_rect
  end

  def footstep_timeout
    @character && @character.footstep_timeout
  end

  def aftershadow_timeout
    @character && @character.aftershadow_timeout
  end

  def spawn_aftershadow?
    (@ticks % @character.aftershadow_frequency).zero? && aftershadow_timeout
  end

  def spawn_footstep?
    @character.foostep_flag && footstep_timeout
  end

end