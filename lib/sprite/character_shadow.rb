class Sprite::CharacterShadow < Sprite::Base

  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system('shadow')
  end

end