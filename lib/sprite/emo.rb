class Sprite::Emo < Sprite::Sheet

  attr_reader :emo_id

  def reset
    clear_automations
    self.opacity = 255
    self.zoom_x = self.zoom_y = 0.5
  end

  def initial_cell_size
    Size2.new(33, 33)
  end

  def sheet_columns
    6
  end

  def sheet_bitmap
    Cache.system("emo0")
  end

  def emo_id=(new_id)
    reset
    @animator = Cache::Sequence.emo(new_id).to_animator
  end

  def update
    super
    if @animator
      @animator.update
      self.index = @animator.index
      if action_data = @animator.env[:action]
        @animator.env.delete(:action)
        act, *params = action_data
        case act
        when :fadein_forced
          self.opacity = 0
          act = :fadein
        when :fadeout_forced
          self.opacity = 255
          act = :fadeout
        end
        case act
        when :fadeout       then automata { |l| l.fade(self.opacity, 0,   Metric::Time.s2f(params.first)) }
        when :fadein        then automata { |l| l.fade(self.opacity, 255, Metric::Time.s2f(params.first)) }
        end
      end
      if @animator.done?
        @animator = nil
        self.visible = false
      else
        self.visible = true
      end
    end
  end

end