#
# EDOS/lib/sprite/icon.rb
#   by IceDragon
class Sprite::Icon < Sprite::Sheet

  def initial_cell_size
    Size2.new(24, 24)
  end

  def sheet_bitmap
    Cache.system("Iconset")
  end

  def sheet_columns
    16
  end

end