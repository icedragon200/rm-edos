#
# EDOS/lib/sprite/item_icon.rb
#   by IceDragon
class Sprite::ItemIcon < Sprite::Icon

  attr_reader :item

  def initialize(viewport=nil, item=nil)
    super(viewport, 0)
    self.item = item
  end

  def default_sheet_name
    'Iconset'
  end

  def sheet_bitmap
    Cache.system(@item ? @item.iconset_name : default_sheet_name)
  end

  def item=(n)
    return if @item == n
    force_item(n)
  end

  def force_item(item)
    @item = n
    self.index = @item ? @item.icon_index : 0
  end

end