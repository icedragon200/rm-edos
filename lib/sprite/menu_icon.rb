#
# EDOS/lib/sprite/menu_icon.rb
#   by IceDragon
class Sprite::MenuIcon < Sprite::Sheet

  def initial_cell_size
    Size2.new(40, 40)
  end

  def sheet_bitmap
    Cache.system('menu-icons/menu-icons')
  end

  attr_null_writer :viewport

end