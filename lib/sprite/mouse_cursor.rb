#
# EDOS/lib/sprite/mouse_cursor.rb
#   by IceDragon
class Sprite::MouseCursor < Sprite

  ##
  # @return [Integer]
  attr_reader :cursor_index

  ##
  # initialize(Viewport viewport)
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Cache.system("mouse_cursors")
    self.cursor_index = 0
  end

  def cursor_index=(n)
    @cursor_index = n
    #self.src_rect.set(0,32*@cursor_index, 32, 32)
  end

  def update
    self.x = Mouse.x
    self.y = Mouse.y
    self.z = Mouse.z
    super
  end

end