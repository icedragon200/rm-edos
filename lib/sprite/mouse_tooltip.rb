# Sprite::MouseTooltip
# // 02/03/2012
# // 02/03/2012
class Sprite::Tooltip < Sprite ; end

module MouseCore
  class Tooltip

    def initialize(mouse)
      @mouse = mouse
      @text = nil
    end

    def set_text(text)
      if @text != text
        @text = text
        @mouse.tooltip_sprite.set_text(@text)
      end
    end

  end

  class << self

    attr_reader :tooltip
    attr_reader :tooltip_sprite

    def init_tooltip
      @tooltip = Tooltip.new(self)
      @tooltip_sprite = Sprite::MouseTooltip.new(nil, "")
    end

    def dispose_tooltip_sprite
      @tooltip_sprite.dispose unless @tooltip_sprite.disposed? if @tooltip_sprite
      @tooltip_sprite = nil
    end

    def update_tooltip_sprite
      @tooltip_sprite.update if @tooltip_sprite
    end

  end

  class Sprite::MouseTooltip < Sprite::Tooltip

    SIGNUM_POS = Array.new(10)
    SIGNUM_POS[7] = [-1,-1] ; SIGNUM_POS[8] = [ 0,-1] ; SIGNUM_POS[9] = [+1,-1]
    SIGNUM_POS[4] = [-1, 0] ; SIGNUM_POS[5] = [ 0, 0] ; SIGNUM_POS[6] = [+1, 0]
    SIGNUM_POS[1] = [-1,+1] ; SIGNUM_POS[2] = [ 0,+1] ; SIGNUM_POS[3] = [+1,+1]

    def initialize(*args, &block)
      super(*args, &block)
      EDOS.server.listen_to(self, EDOS::CHANNEL_EDOS_TOOL)
      self.opacity = 0
    end

    def dispose
      EDOS.server.stop_listening_to(self, EDOS::CHANNEL_EDOS_TOOL)
      super
    end

    def x_signum
      Mouse.x <=> Graphics.width / 2
    end

    def y_signum
      Mouse.y <=> Graphics.height / 2
    end

    def signum_pos
      case [x_signum,y_signum]
      when SIGNUM_POS[3], SIGNUM_POS[6], SIGNUM_POS[9] # // X Right
        return -(width + 4), 0
      when SIGNUM_POS[2], SIGNUM_POS[5], SIGNUM_POS[8] # // X Center
        return 22, 0
      when SIGNUM_POS[1], SIGNUM_POS[4], SIGNUM_POS[7] # // X Left
        return 22, 0
      end
    end

    def update
      sx, sy = signum_pos
      self.x = Mouse.x + sx
      self.y = Mouse.y + sy
      self.z = Mouse.z + 1
      super
    end

    def receive_data(from, data)
      set_text(data)
    end

  end
end


