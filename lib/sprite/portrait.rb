#
# EDOS/lib/sprite/Portrait.rb
#   by IceDragon
#   dc 09/06/2013
#   dm 09/06/2013
# vr 1.0.0
class Sprite::Portrait < Sprite

  ### instance_variables
  attr_accessor :portrait_hue
  attr_accessor :portrait_name

  ##
  # initialize(Viewport viewport)
  def initialize(viewport=nil)
    super(viewport)
    @portrait_name = ''
    @portrait_hue  = 0
  end

  ##
  # refresh
  def refresh
    if @portrait_name && !@portrait_name.empty?
      self.bitmap = Cache.portrait(@portrait_name, @portrait_hue)
    else
      self.bitmap = nil
    end
  end

end