#
# EDOS/lib/sprite/icon.rb
#   by IceDragon
class Sprite::Sheet < Sprite

  ##
  # @return [Integer] index
  attr_reader :index
  attr_reader :cell_size

  def initialize(viewport=nil, index=0)
    super(viewport)
    @cell_size = initial_cell_size
    self.index = index
  end

  def initial_cell_size
    Size2.new(24, 24)
  end

  def sheet_columns
    16
  end

  def sheet_bitmap
    Cache.system("Iconset")
  end

  def index=(index)
    @index = index
    self.bitmap = sheet_bitmap
    src_rect.set(@cell_size.width  * (@index % sheet_columns),
                 @cell_size.height * (@index / sheet_columns),
                 @cell_size.width, @cell_size.height)
  end

end