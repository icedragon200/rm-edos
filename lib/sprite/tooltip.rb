#
# EDOS/lib/sprite/tooltip.rb
#   by IceDragon
class Sprite::Tooltip < Sprite

  def initialize(viewport=nil, text="")
    super(viewport)
    @mtime = Graphics.frame_rate * 2
    @time = 0
    set_text(text)
  end

  def set_text(text)
    @time = @mtime
    return unless @text != text
    self.opacity = 0
    @text = text
    refresh
  end

  def clear
    set_text("")
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def refresh
    dispose_bitmap_safe
    font = DrawExt::Merio.new_font(:light, :micro, :enb)
    w, h = *font.text_size(@text)
    bmp = self.bitmap = Bitmap.new(w + 8, h + 8)
    bmp.font.set(font)
    bmp.merio.draw_dark_rect(bmp.rect)
    bmp.draw_text(bmp.rect, @text, 1)
  end

  def fadein
    return unless self.opacity < 255
    self.opacity += 255.0 / Metric::Time.sec_to_frame(0.12)
  end

  def fadeout
    return unless self.opacity > 0
    self.opacity -= 255.0 / Metric::Time.sec_to_frame(0.12)
  end

  def update
    super
    (@time = @time.pred.max(0)) > 0 ? fadein : fadeout
  end

end