#
# EDOS/lib/sprite/portrait.rb
#   by IceDragon
class Sprite::UnitPortrait < Sprite::Portrait

  ### mixins
  include Mixin::UnitHost

  ##
  # initialize(Viewport viewport, REI::Unit* unit)
  def initialize(viewport=nil, unit=nil)
    super(viewport)
    @unit = unit
    refresh
  end

  ##
  # refresh
  def refresh
    if @unit
      character = @unit.character
      @portrait_name = character.portrait_name
      @portrait_hue  = character.portrait_hue
    else
      @portrait_name = ''
      @portrait_hue  = 0
    end
    super
  end

  ##
  # on_unit_change
  def on_unit_change
    refresh
  end

end