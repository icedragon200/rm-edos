module Spriteset
end

dir = File.dirname(__FILE__)
Dir.glob(File.join(dir, 'spriteset', '*.rb')).sort.each do |fn|
  require fn
end