#
# EDOS/lib/spriteset/main.rb
#   by Icedragon
class Spriteset::Main

  def initialize
    @mouse_sprite = Sprite::MouseCursor.new
  end

  def dispose
    @mouse_sprite.dispose
  end

  def update
    @mouse_sprite.update
  end

end