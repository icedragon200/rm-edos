#
# EDOS/lib/spriteset/map.rb
#   by IceDragon
class Spriteset::Map

  attr_accessor :camera
  attr_reader :map

  def initialize
    create_viewports
    create_tilemap
    create_grid
    create_characters
    create_aftershadows
    create_popups
  end

  def dispose
    dispose_popups
    dispose_aftershadows
    dispose_characters
    dispose_grid
    dispose_tilemap
    dispose_viewports
  end

  def update
    update_viewports
    update_tilemap
    update_grid
    update_characters
    update_aftershadows
    update_popups
  end

  def create_viewports
    @viewport = Viewport.new
  end

  def create_tilemap
    @tilemap = SRRI::Chipmap.new(@viewport)
    @tilemap.tilesize = 32
    @tilemap.tile_columns = 32
    @tilemap.tile_bitmap = Cache.atlas("base_out_atlas") #{}"soundsheett_vFinal")
  end

  def create_grid
    @grid = Sprite.new(@viewport)
  end

  def create_characters
    @characters = []
  end

  def create_aftershadows
    @aftershadows = []
  end

  def create_popups
    @popups = []
  end

  def add_aftershadow(parent, timeout)
    @aftershadows << Sprite::AfterShadow.new(@viewport, parent, timeout)
  end

  def dispose_popups
    @popups.each(&:dispose)
  end

  def dispose_tilemap
    @tilemap.dispose
  end

  def dispose_grid
    @grid.dispose
  end

  def dispose_characters
    @characters.each(&:dispose)
  end

  def dispose_aftershadows
    @aftershadows.each(&:dispose)
  end

  def dispose_viewports
    @viewport.dispose
  end

  def refresh_tilemap
    if @map
      @tilemap.map_data    = @map.map_data
      @tilemap.shadow_data = @map.shadow_data
      @tilemap.flash_data  = @map.flash_data
      @tilemap.visible = true
      @tilemap.refresh
      @tilemap.reveal
    else
      @tilemap.map_data    = nil
      @tilemap.shadow_data = nil
      @tilemap.flash_data  = nil
      @tilemap.visible = false
    end
  end

  def refresh_characters
    dispose_characters
    @characters.clear
    @map.characters.each do |char|
      @characters << Sprite::Character.new(viewport: @viewport, character: char)
    end
  end

  def update_viewports
    if @camera
      @viewport.ox = @camera.x
      @viewport.oy = @camera.y
    else
      @viewport.ox = 0
      @viewport.oy = 0
    end
  end

  def update_tilemap
    if (n=@map.mod_id) != @map_update_id
      changes = @map.mod_changes
      refresh_tilemap    if changes.delete(:map)
      refresh_characters if changes.delete(:characters)
    end
    @tilemap.update
  end

  def update_grid
    if !@grid.bitmap || (@tilemap.width_abs != @grid.bitmap.width ||
                         @tilemap.height_abs != @grid.bitmap.height)
      @grid.bitmap = Bitmap.new(@tilemap.width_abs, @tilemap.height_abs)
      grid_color = Palette['black'].dup.hset(alpha: 128)
      @tilemap.data_ysize.times do |y|
        gy = y * 32
        @tilemap.data_xsize.times do |x|
          gx = x * 32
          @grid.bitmap.fill_rect(gx + 31, gy +  0,  1, 32, grid_color)
          @grid.bitmap.fill_rect(gx +  0, gy + 31, 32,  1, grid_color)
        end
      end
      @grid.bitmap.blur
    end
    @grid.update
  end

  def update_characters
    @characters.each do |s|
      s.update
      add_aftershadow(s, s.aftershadow_timeout) if s.spawn_aftershadow?
    end
  end

  def update_aftershadows
    cleanup = []
    @aftershadows.each do |s|
      (cleanup << s; next) if s.timedout?
      s.update
    end
    unless cleanup.empty?
      cleanup.each(&:dispose)
      @aftershadows -= cleanup
    end
  end

  def update_popups
    cleanup = []
    @popups.each do |s|
      (cleanup << s; next) if s.done?
      s.update
    end
    unless cleanup.empty?
      cleanup.each(&:dispose)
      @popups -= cleanup
    end
  end

  def map=(new_map)
    if @map != new_map
      @map = new_map
      refresh_tilemap
      refresh_characters
    end
  end

  private :create_tilemap
  private :dispose_tilemap
  private :update_tilemap

end