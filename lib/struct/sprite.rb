class StructSprite

  attr_accessor :viewport
  attr_accessor :bitmap
  attr_accessor :visible
  attr_accessor :opacity
  attr_accessor :x
  attr_accessor :y
  attr_accessor :z
  attr_accessor :ox
  attr_accessor :oy
  attr_accessor :zoom_x
  attr_accessor :zoom_y
  attr_accessor :angle
  attr_accessor :src_rect
  attr_accessor :color
  attr_accessor :tone
  attr_accessor :blend_type
  attr_accessor :bush_depth
  attr_accessor :bush_opacity
  attr_accessor :wave_amp
  attr_accessor :wave_speed
  attr_accessor :wave_phase
  attr_accessor :wave_length

  def initialize(viewport=nil)
    @viewport = viewport
    @bitmap   = nil
    @visible = true
    @opacity = 255
    @x, @y, @z = 0, 0, 0
    @ox, @oy   = 0, 0
    @zoom_x, @zoom_y = 1.0, 1.0
    @angle = 0
    @src_rect = Rect.new(0, 0, 0, 0)
    @color = Color.new(0, 0, 0, 0)
    @tone  = Tone.new(0, 0, 0, 0)
    @blend_type = 0
    @bush_depth   = 0
    @bush_opacity = 0
    @wave_amp    = 0
    @wave_speed  = 0
    @wave_phase  = 0
    @wave_length = 0
  end

  def width
    @src_rect.width
  end

  def height
    @src_rect.height
  end

  def bitmap=(bmp)
    @bitmap = bmp
    @src_rect = @bitmap ? @bitmap.rect.dup : Rect.new(0, 0, 0, 0)
  end

  def viewport=(new_viewport)
    @viewport = new_viewport
  end

  def src_rect=(srect)
    @src_rect = srect || @bitmap ? @bitmap.rect.dup : nil
  end

  def visible=(vis)
    @visible = !!vis
  end

  def x=(new_x)
    @x = new_x.to_i
  end

  def y=(new_y)
    @y = new_y.to_i
  end

  def z=(new_z)
    @z = new_z.to_i
  end

  def ox=(new_ox)
    @ox = new_ox.to_i
  end

  def oy=(new_oy)
    @oy = new_oy.to_i
  end

  def zoom_x=(new_zoom_x)
    @zoom_x = new_zoom_x.to_f
  end

  def zoom_y=(new_zoom_y)
    @zoom_y = new_zoom_y.to_f
  end

  def angle=(new_angle)
    @angle = new_angle.to_i
  end

  def wave_amp=(new_wave_amp)
    @wave_amp = new_wave_amp.to_i
  end

  def wave_length=(new_wave_length)
    @wave_length = new_wave_length.to_i
  end

  def wave_speed=(new_wave_speed)
    @wave_speed = new_wave_speed.to_i
  end

  def wave_phase=(new_wave_phase)
    @wave_phase = new_wave_phase.to_i
  end

  def mirror=(new_mirror)
    @mirror = !!new_mirror
  end

  def bush_depth=(new_bush_depth)
    @bush_depth = new_bush_depth.to_i
  end

  def bush_opacity=(new_bush_opacity)
    @bush_opacity = [[new_bush_opacity.to_i, 0].max, 255].min
  end

  def opacity=(new_opacity)
    @opacity = [[new_opacity.to_i, 0].max, 255].min
  end

  def blend_type=(new_blend_type)
    @blend_type = new_blend_type.to_i
  end

  def color=(new_color)
    @color = new_color
  end

  def tone=(new_tone)
    @tone = new_tone
  end

end