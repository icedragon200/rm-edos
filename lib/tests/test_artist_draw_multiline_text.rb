module TestCore::Scene
  class Artist_DrawMultilineString < Scene::TestBase

    def start
      super
      multiline_str = <<_EOF_
Multiline String test
  This is a multiline string
  Basically we are making use of the String#each_line
  function and writing them using Bitmap#draw_text
  So far we should have 5 lines :D
_EOF_
      window = Window::Base.new(*menu_canvas.to_a)
      window.artist.draw_multiline_text(window.to_rect, 24, multiline_str, 0)
      add_window(window)
    end

    def title_text
      "Multiline Test"
    end

  end
end