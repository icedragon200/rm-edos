module TestCore::Scene
  class Aseyran_CardGenerate < Scene::TestBase

    def start
      super
      card = Aseyran::Card.new

      @sp = Sprite.new
      @sp.bitmap = draw_card(card)
      @sp.align_to!(anchor: 5, surface: menu_canvas)
      add_window(sp)
    end

    def draw_card(_card_)
      card_rect        = Rect.new(0, 0, 162, 198)
      card_top_rect    = Rect.new(0, 0, 162, 120)
      card_bottom_rect = Rect.new(0, 120, 162, 78)
      card_title_rect  = Rect.new(0, 0, 162, 24)
      cost_rect        = Rect.new(0, 0, 24, 24)
      element_rect     = Rect.new(162-24, 0, 24, 24)
      name_rect        = Rect.new(24, 0, 162-48, 24)
      id_rect          = Rect.new(0, 24, 16, 96)
      id2_rect         = Rect.new(162-16, 24, 16, 96) # unused
      image_rect       = Rect.new(16, 24, 162-32, 96)
      effect_rect      = Rect.new(0, image_rect.y2, 24, 198-image_rect.y2)
      equip_rect       = Rect.new(162-24, image_rect.y2, 24, 198-image_rect.y2)
      content_rect     = Rect.new(24, image_rect.y2, 162-48, 198-image_rect.y2)
      param_rect       = content_rect.dup; param_rect.height /= 3
      mana_rect        = param_rect.dup; mana_rect.y += param_rect.height
      note_rect        = mana_rect.dup; note_rect.y += note_rect.height

      card_bitmap = Bitmap.new(card_rect.width, card_rect.height)
      card_bitmap.fill_rect(card_rect, Palette['white'])
      card_bitmap.blend_fill_rect(card_top_rect,    Palette['droid_blue'])
      card_bitmap.blend_fill_rect(card_bottom_rect, Palette['droid_blue_light'])
      card_bitmap.blend_fill_rect(element_rect,     Palette['droid_dark_ui_enb'])
      card_bitmap.blend_fill_rect(cost_rect,        Palette['droid_dark_ui_enb'])
      card_bitmap.blend_fill_rect(name_rect,        Palette['droid_dark_ui_dis'])
      card_bitmap.blend_fill_rect(id_rect,          Palette['droid_dark_ui_dis'])
      card_bitmap.blend_fill_rect(id2_rect,         Palette['droid_dark_ui_dis'])
      card_bitmap.blend_fill_rect(image_rect,       Palette['droid_dark_ui_enb'])
      card_bitmap.blend_fill_rect(content_rect,     Palette['droid_dark_ui_dis'])
      card_bitmap.blend_fill_rect(param_rect,       Palette['droid_dark_ui_enb'])
      card_bitmap.blend_fill_rect(mana_rect,        Palette['droid_dark_ui_dis'])
      card_bitmap.blend_fill_rect(note_rect,        Palette['droid_light_ui_enb'])
      #rect = Rect.new(card_bitmap.rect.x2 - 24, 0, 24, 24)
      #card_bitmap.fill_rect(rect, Palette['droid_blue'])

      card_bitmap
    end

    def terminate
      @sp.dispose_bitmap_safe
      super
    end

    def title_text
      "Aseyran : Card Generator"
    end

  end
end