module TestCore::Scene
  class Automation_MovetoOffsetOsc < Scene::TestBase

    def start
      super
      @sp = Sprite.new
      @sp.bitmap = Bitmap.new(32, 32)
      @sp.bitmap.draw_gauge_ext
      @sp.align_to!(anchor: 5, surface: Screen.content)

      auto_hover = Automation::MovetoOffsetOsc.new([0, 0], [0, 8], 30, :sine_out)
      @sp.add_automation(auto_hover)
    end

    def terminate
      @sp.dispose_all
      super
    end

    def update
      super
      @sp.update
    end

    def title_text
      "Automation : Moveto Offset Osc"
    end

  end
end