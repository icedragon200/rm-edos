#
# EDOS/core/test_core/test_barcode_generator.rb
#   by IceDragon
module TestCore::Scene
  class BarcodeGenerator < Scene::TestBase

    def start
      super
      str = #"IEML3-LD"
            "IEML3-WD"
      bytes = str[0, 8].bytes
      bit_rows = bytes.map { |byte| ("%08b" % byte).split('').map(&:to_i) }
      bitsize = Size2.new(1, 1)
      #p bytes, bit_rows
      @sp = Sprite.new(nil)
      @sp.bitmap = Bitmap.new(8 * bitsize.width, 8 * bitsize.height)
      @sp.bitmap.fill(Palette['white'])
      for y in 0...bit_rows.size
        row = bit_rows[y]
        for x in 0...row.size
          bit = row[x]
          r = Rect.new(x * bitsize.width, y * bitsize.height,
                       bitsize.width, bitsize.height)
          if bit == 1
            @sp.bitmap.fill_rect(r, Palette['black'])
          end
        end
      end
      #@sp.bitmap.texture.save(str + "-bit-barcode.png")
    end

    def terminate
      @sp.dispose_bitmap_safe
      super
    end

    def title_text
      "Barcode Generator"
    end

  end
end