#
# EDOS/core/test_core/test_bitmap_blur.rb
#   by IceDragon
#   This test, will try to blur a 512x512 px Bitmap 128 times and benchmark the
#   result.
module TestCore::Scene
  class Bitmap_Blur < Scene::TestBase

    def start
      super
      @sp = Sprite.new
      bmp = sp.bitmap = Bitmap.new(512, 512)
      DrawExt.draw_gauge_ext_sp1(bmp, bmp.rect, 1.0, DrawExt::BLUE_BAR_COLORS)
      blur_times = 128

      _test_log do |log|
        log.fputs_bang "Blurring a #{bmp.width} x #{bmp.height} #{bmp.class}: #{blur_times} times"
        log.fputs_result(num)
      end

      num = stopwatch { blur_times.times { sp.bitmap.blur } }
      add_window(sp)
    end

    def terminate
      @sp.dispose_bitmap_safe
      super
    end

    def title_text
      "Bitmap#blur"
    end

  end
end