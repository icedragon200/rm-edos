module TestCore::Scene
  class Bitmap_BucketFill < Scene::TestBase

    def start
      return #TEST_DISABLED
      super
      @sp = Sprite.new
      @sp.bitmap = Bitmap.new(Graphics.width / 8, Graphics.height / 8)
      @sp.bitmap.fill_rect(@sp.bitmap.rect, Palette['brown2'])
      rect = @sp.bitmap.rect
      rect.width /= 2
      rect.height /= 2
      @sp.bitmap.fill_rect(rect, Palette['brown3'])

      @sp.zoom_x = Graphics.width.to_f / @sp.width
      @sp.zoom_y = Graphics.height.to_f / @sp.height

      for i in 0...@sp.bitmap.height
        next unless i % 2 == 0
        @sp.bitmap.fill_rect 0,i,@sp.bitmap.width,1,Palette['brown3']
      end

      for i in 0...@sp.bitmap.width
        next unless i % 2 == 0
        @sp.bitmap.fill_rect i,0,1,@sp.bitmap.height,Palette['brown3']
      end

      @sp.bitmap.fill 6,6 do
        #Main.update
        #@sp.update
      end

      @sp.bitmap.fill 6,6,Palette['brown2'] do
        #Main.update
        #@sp.update
      end

      @sp.bitmap.fill 6,6,Palette['black'] do
        #Main.update
        #@sp.update
      end
    end

    def terminate
      @sp.dispose_bitmap_safe if @sp
      super
    end

    def title_text
      'Bitmap#bucket_fill'
    end

  end
end