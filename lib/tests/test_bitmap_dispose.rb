module TestCore::Scene
  class Bitmap_Dispose < Scene::TestBase

    def start
      super
      bmp = Bitmap.new(24, 24)
      bmp.dispose
      return_scene
    end

    def title_text
      "Bitmap#dispose"
    end

  end
end