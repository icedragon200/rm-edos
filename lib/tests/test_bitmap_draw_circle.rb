module TestCore::Scene
  class Bitmap_DrawCircle < Scene::TestBase

    def start
      return #TEST_DISABLED
      super
      # DrawCircle
      sprite1 = Sprite.new
      sprite2 = Sprite.new
      @bmp = Bitmap.new(256,256)
      sprite1.bitmap = @bmp
      sprite2.bitmap = @bmp
      cx, cy = 256/2, 256/2
      circ = MACL::Geometry::Circle.new(cx, cy, 256*0.45)
      last_point = circ.calc_xy_from_angle(0)
      point = last_point
      circ.cycle_360(1) do |n|
        point = circ.calc_xy_from_angle(n)
        @bmp.draw_line(point, last_point, Palette['white'])
        last_point = point
      end
      # @bmp.fill(cx,cy,Palette[:sys_orange1])
      last_point = MACL::Point.new(cx.to_i, cy.to_i)
      r = 0
      [54, 90, 114.12, 72, 29.88].each do |n|
        point = circ.calc_xy_from_angle(r + n)
        r += n
        @bmp.draw_line(point, last_point, Palette['white'])
      end
      @bmp2 = @bmp.dup
      2.times do |i|
        @bmp.blt(0, i+1, @bmp2, @bmp2.rect, 255 - i / 2.to_f * 255)
      end
      @bmp2.dispose
      sprite2.y += 16
      sprite2.zoom_y = sprite.zoom_y = 1.0
      add_window(sprite1)
      add_window(sprite2)
    end

    def terminate
      @bmp.dispose if @bmp
      super
    end

    def title_text
      'Bitmap#draw_circle'
    end

  end
end
