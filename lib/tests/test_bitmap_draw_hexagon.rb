module TestCore::Scene
  class Bitmap_DrawHexagon < Scene::TestBase

    def start
      super
      @bmp = Bitmap.new(Screen.content.width, Screen.content.height)
      @bmp.fill(Palette['white'])
      bmpr = bmp.rect
      d = 16
      p = d / 2
      r = Rect.new(p, p, d, d)
      vx = Vector2(r.width * Math.sqrt(3) / 4, 0)
      vy = Vector2(0, r.height * Math.sqrt(3) / 2.3333333)
      color1 = Palette['droid_blue_light']
      color2 = Palette['droid_blue']
      color3 = Palette['droid_light_ui_enb']
      color4 = Palette['droid_light_ui_dis']
      wc = (@bmp.width / vx.x.to_i) / 2
      tthen = Time.now
      (@bmp.height / vy.y.to_i).times do |y|
        wc.times do |x|
          color = [color1, color2, color3, color4][(y % 2 - x % 4) % 4]
          v = vx * 2 * x
          v += vy * y + vx * (y % 2)
          #v = vx * [x, 1] * [2, 1] - vy * [1, y] * [1, 1 + y % 2]
          @bmp.draw_fill_hex(r + v, color)
        end
      end
      puts Time.now - tthen
      #bmp.draw_fill_hex(r, color1)
      #bmp.draw_fill_hex(r + vx * 2, color2)
      #bmp.draw_fill_hex(r - vx * 2, color2)
      #bmp.draw_fill_hex(r + vx + vy, color2)
      #bmp.draw_fill_hex(r - vx + vy, color2)
      #bmp.draw_fill_hex(r + vx - vy, color2)
      #bmp.draw_fill_hex(r - vx - vy, color2)
      sp = Sprite.new
      sp.bitmap = @bmp
      add_window(sp)
    end

    def terminate
      @bmp.dispose
      super
    end

    def title_text
      'Bitmap#draw_hexagon'
    end

  end
end