module TestCore::Scene
  class Bitmap_DrawGradientFillRect01 < Scene::TestBase

    def start
      return unless Bitmap.method_defined?(:rb_gradient_fill_rect)
      super
      sp = Sprite.new
      @bmp = sp.bitmap = Bitmap.new(256, 48)
      sp2 = Sprite.new
      @bmp2 = sp2.bitmap = bmp.dup
      color1, color2 = Color.new(0, 0, 0, 255), Color.new(255, 255, 255, 0) #Palette['sys_red1'], Palette['sys_red2']
      vertical = false

      res1 = stopwatch do
        @bmp.rb_gradient_fill_rect(0, 0, @bmp.width, @bmp.height, color1, color2, vertical)
      end

      res2 = stopwatch do
        @bmp2.gradient_fill_rect(0, 0, @bmp2.width, @bmp2.height, color1, color2, vertical)
      end

      result = compare_texture(bmp.texture, bmp2.texture)

      _test_log do |log|
        log.fputs_result(res1)
        log.fputs_result(res2)
        log.fputs_result("#{100 * result.count(true) / result.size.to_f}% of the bitmap matches")
      end

      sp.align_to!(anchor: 7)
      sp2.align_to!(anchor: 9)

      _test_loop_(TEST_TIME_SHORT) do
        sp.update
        sp2.update
      end
    end

    def terminate
      @bmp.dispose
      @bmp2.dispose
      super
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 1'
    end

  end
end