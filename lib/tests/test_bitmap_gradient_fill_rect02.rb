module TestCore::Scene
  class Bitmap_DrawGradientFillRect02 < Scene::TestBase

    def start
      super
      @sp = Sprite.new
      bmp = @sp.bitmap = Bitmap.new(Graphics.width, Graphics.height / 2)

      DrawExt.draw_gauge_specia2(bmp, bmp.rect, 1.0,
                                 [Palette['sys1_red'], Palette['sys1_red'],
                                 Palette['sys2_red'], Palette['sys2_red'],
                                 Color.new(255, 255, 255, 41)])

      i = 0
      # color-pairs
      colors = [
        [Palette['sys1_red'], Palette['sys1_red']],
        [Palette['sys2_red'], Palette['sys2_red']],
        [Palette['sys1_green'], Palette['sys1_green']],
        [Palette['sys2_green'], Palette['sys2_green']],
        [Palette['sys1_blue'], Palette['sys1_blue']],
        [Palette['sys2_blue'], Palette['sys2_blue']]
      ]

      colors.each { |(c1, c2)| c2.blend.darken!(0.3) }
    end

    def udpate
      if Graphics.frame_count % 15 == 0
        i += 1
        rect = sp.bitmap.rect.dup
        rect.height /= 2
        col1, col2 = *colors[i % colors.size]
        @sp.bitmap.gradient_fill_rect(rect, col1, col2, true)
        DrawExt.draw_highlight_flat(sp.bitmap, sp.bitmap.rect, Color.new(255, 255, 255, 41))
        rect.y += rect.height
        @sp.bitmap.gradient_fill_rect(rect, col2, col1, true)
      end
      super
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end