module TestCore::Scene
  class Bitmap_DrawGradientFillRect03 < Scene::TestBase

    def start
      super
      @sp = Sprite.new
      @sp.bitmap = Bitmap.new(gwidth, gheight)
      r = sp.bitmap.rect.dup
      r /= 2
      @sp.bitmap.gradient_fill_rect(r, Palette['droid_blue_light'], Palette['black'], true)
      r.x += r.width
      @sp.bitmap.gradient_fill_rect(r, Palette['droid_green_light'], Palette['black'], true)
      r.y += r.height
      @sp.bitmap.gradient_fill_rect(r, Palette['droid_green_light'], Palette['black'], false)
      r.x -= r.width
      @sp.bitmap.gradient_fill_rect(r, Palette['droid_blue_light'], Palette['black'], false)
      add_window(@sp)
    end

    def terminate
      @sp.dispose_bitmap_safe
      super
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 3'
    end

  end
end