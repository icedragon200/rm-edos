#
# EDOS/core/test_core/test-bitmap#round_fill_rect.rb
#   dc 03/07/2013
#   dm 03/07/2013
# vr 1.0.0
module TestCore::Scene
  class Bitmap_RoundFillRect < Scene::TestBase

    def start
      super
      sp = Sprite.new
      sp.bitmap = Bitmap.new(Screen.content.width, Screen.content.height)
      @bmp = sp.bitmap
      #sp.bitmap.fill(Palette['merio_blue'])
      r = Rect.new(0, 0, sp.bitmap.width / 4, sp.bitmap.height / 6)
      ['droid_light_ui_enb', 'droid_light_ui_dis',
       'droid_dark_ui_enb', 'droid_dark_ui_dis'].each_with_index do |s, i|
        c = Palette[s]
        rr = r.dup
        rr.x = i * rr.width
        @bmp.fill_rect(rr.step!(2), c)
        @bmp.round_fill_rect(rr.step!(2), c)
        @bmp.blend_fill_rect(rr.step!(2), c)
        @bmp.round_blend_fill_rect(rr.step!(2), c)
        @bmp.merio.draw_fill_rect(rr.step!(2), c)
      end
      add_window(sp)
    end

    def terminate
      @bmp.dispose
      super
    end

    def update
      EDOS.server.send_data(EDOS::CHANNEL_EDOS_NOTE, @bmp.get_pixel(Mouse.x, Mouse.y).to_a.join(", "))
      super
    end

    def title_text
      'Bitmap#round_fill_rect'
    end

  end
end