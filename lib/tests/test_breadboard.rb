module TestCore::Scene
  class Breadboard < Scene::TestBase

    def start
      return # TEST_DISABLED # skip this test
      super
      cell_size = 64
      dot_size = 4
      board = Table.new 6,6
      board_grid = MACL::Grid.new board.xsize,board.ysize,cell_size,cell_size
      txts = Array.new(board_grid.area1) do ['atk','def','agi','mat','mdf','luk'].sample end

      # //
      str = %Q(
        draw_box: box(0% 0% 100% 100%), #{Palette['brown1'].to_vectrix}
        draw_box: box(5% 5% 90% 90%)  , #{Palette['brown2'].to_vectrix}
        draw_box: box(10% 10% 80% 80%), #{Palette['brown1'].to_vectrix}
        draw_box: box(20% 20% 60% 60%), #{Palette['brown2'].to_vectrix}
        draw_box: box(40% 40% 20% 20%), #{Palette['black'].to_vectrix}
        draw_box: box(45% 45% 10% 10%), #{Palette['brown1'].to_vectrix}
      )
      #  draw_line: pos(7% 7%), pos(93% 7%), #{Palette['black'].to_vectrix}, int(3)
      #)
      #puts str
      vectrix = MACL::Vectrix.from_str(str)
      block_bmp = vectrix.to_bitmap(*[board_grid.cell_width] * 2)
      #block_bmp.fill_rect block_bmp.rect, Palette['brown1']
      #block_bmp.fill_rect block_bmp.rect.contract(1), Palette['brown2']
      #block_bmp.fill_rect block_bmp.rect.contract((board_grid.cell_width-dot_size)/2), Palette['black']
      str = %Q(
        draw_box: box(30% 15% 40% 70%), #{Palette["gray17"].to_vectrix}
        draw_box: box(35% 15% 30% 70%), #{Palette["gray15"].to_vectrix}
        draw_box: box(40% 15% 20% 70%), #{Palette["gray13"].to_vectrix}
        draw_box: box(42% 20% 16% 2%), #{Palette['sys_red1'].to_vectrix}
        draw_box: box(42% 75% 16% 2%), #{Palette['sys_green1'].to_vectrix}
      )
      vectrix = MACL::Vectrix.from_str(str)
      connector_bmp = vectrix.to_bitmap(cell_size, cell_size*2)
      #rect = connector_bmp.rect.contract(cell_size*0.8/2,1)
      #rect.contract!(anchor: 28, amount: connector_bmp.height * 0.5 / 2.0)
      #connector_bmp.fill_rect rect,Palette["gray17"]

      board_bmp = Bitmap.new(board_grid.width, board_grid.height)
      board.iterate do |n,x,y|
        rect  = board_grid.cell_r x,y
        index = board_grid.xy2index x,y
        board_bmp.blt rect.x,rect.y,block_bmp,block_bmp.rect
        board_bmp.font.size = 12
        rect.height /= 2
        rect.offset!(anchor: 2, rate: 1.0)
        board_bmp.draw_text rect,txts[index],1
      end

      sp = Sprite.new
      sp.bitmap = board_bmp
      sp.salign! 5
      connector_sprs = [Sprite.new]
      connector_sprs.invoke :bitmap=, connector_bmp
      connector_sprs.invoke :z=, 200
      Mouse.init

      _test_loop_(TEST_TIME_LONG) do
        sp.update
        connector_sprs.each do |s| s.cx,s.cy = Mouse.x, Mouse.y end
      end

      return TEST_PASSED
    end

    def title_text
      'Breadboard'
    end

  end
end