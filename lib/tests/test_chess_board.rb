#
# EDOS/core/test_core/test-chess_board.rb
# vr 1.0.0
module TestCore::Scene
  class ChessBoard < Scene::TestBase

    def start
      return #TEST_DISABLED # Pos3 has been removed from the MACL
      super
      #p rule = Minigame::Chess.move_rule_str_by_role(Chess::RolePawn)
      #p move_rules = Minigame::Chess.move_rule_str_to_move_rules(rule)
      #p move_rules.map(&:to_s).join(":")
      #p move_rules.map { |mv| mv.calc_pos(MACL::Pos3::NORTH) }
      #p move_rules.map { |mv| mv.calc_pos_a(MACL::Pos3::NORTH) }.flatten.uniq
      #exit

      # settings
      tilesize = 32
      colors = [DrawExt::WHITE_BAR_COLORS, DrawExt::BLACK_BAR_COLORS]
      board_w = tilesize * 8
      board_h = tilesize * 8

      fieldPiece  = Minigame::Chess::FieldPiece
      piecePawn   = Minigame::Chess::Pawn
      pieceRook   = Minigame::Chess::Rook
      pieceKnight = Minigame::Chess::Knight
      pieceBishop = Minigame::Chess::Bishop
      pieceQueen  = Minigame::Chess::Queen
      pieceKing   = Minigame::Chess::King
      #
      cursor_pos = Point3.new(0, 0, 0)
      white_pieces = [fieldPiece.new(piecePawn).set_pos(0, 1),
                      fieldPiece.new(piecePawn).set_pos(1, 1),
                      fieldPiece.new(piecePawn).set_pos(2, 1),
                      fieldPiece.new(piecePawn).set_pos(3, 1),
                      fieldPiece.new(piecePawn).set_pos(4, 1),
                      fieldPiece.new(piecePawn).set_pos(5, 1),
                      fieldPiece.new(piecePawn).set_pos(6, 1),
                      fieldPiece.new(piecePawn).set_pos(7, 1),
                      fieldPiece.new(pieceRook).set_pos(0, 0),
                      fieldPiece.new(pieceKnight).set_pos(1, 0),
                      fieldPiece.new(pieceBishop).set_pos(2, 0),
                      fieldPiece.new(pieceQueen).set_pos(3, 0),
                      fieldPiece.new(pieceKing).set_pos(4, 0),
                      fieldPiece.new(pieceBishop).set_pos(5, 0),
                      fieldPiece.new(pieceKnight).set_pos(6, 0),
                      fieldPiece.new(pieceRook).set_pos(7, 0)
                      ]
      white_pieces.each do |fpiece|
        fpiece.team = Minigame::Chess::TEAM_WHITE
        fpiece.pos.orientation = MACL::Pos3::SOUTH
      end

      black_pieces = white_pieces.map do |fpiece|
        fieldPiece.new(fpiece.piece).set_pos(7 - fpiece.x, 7 - fpiece.y)
      end
      black_pieces.each do |fpiece|
        fpiece.team = Minigame::Chess::TEAM_BLACK
        fpiece.pos.orientation = MACL::Pos3::NORTH
      end

      pieces = white_pieces + black_pieces

      field = Array.new(8) { Array.new(8, nil) }
      move_field = Array.new(8) { Array.new(8, nil) }

      # Graphics
      viewport = Viewport.new((gwidth - board_w) / 2, Metric.ui_element,
                      board_w, board_h)
      board_sp = Sprite.new(viewport)
      board_sp.bitmap = Bitmap.new(board_w, board_h)
      board_sp.z = 0

      board_overlay_sp = Sprite.new(viewport)
      board_overlay_sp.bitmap = Bitmap.new(board_w, board_h)
      board_overlay_sp.z = 1
      board_overlay_sp.opacity = 128

      cursor_sp = Sprite.new(viewport)
      cursor_sp.bitmap = Bitmap.new(tilesize, tilesize)
      cursor_sp.z = 100

      pos_detail_sp = Sprite.new(nil)
      pos_detail_sp.bitmap = Bitmap.new(gwidth, Metric.ui_element_sml)
      pos_detail_sp.y2 = Graphics.rect.y2
      pos_detail_sp.z = 0xFFFF

      piece_sps = pieces.map do |fpiece|
        sp = Minigame::Chess::Sprite::Piece.new(viewport, fpiece)
        sp.z = 50
        sp
      end

      # drawing
      for y in 0...8
        for x in 0...8
          r = rct(x * tilesize, y * tilesize, tilesize, tilesize)
          i = (x + y) % colors.size
          DrawExt.draw_gauge_ext_sp4(board_sp.bitmap, r, 1.0, colors[i])
        end
      end

      DrawExt.draw_gauge_ext_sp4(cursor_sp.bitmap, cursor_sp.bitmap.rect,
                                 1.0, DrawExt::BLUE_BAR_COLORS)
      cursor_sp.opacity = 0x80

      # tmp
      old_pos = nil
      active_move_a = nil
      last_fpiece = nil

      # funcs
      clear_move_field = -> do
        move_field.each { |row| row.fill(nil) }
      end

      set_move_field = ->(a, n) do
        a.each { |pos| move_field[pos.z][pos.x] = n }
      end

      refresh_overlay = -> do
        board_overlay_sp.bitmap.clear
        if active_move_a
          active_move_a.each do |pos|
            r = rct(pos.x * tilesize, pos.z * tilesize, tilesize, tilesize)
            DrawExt.draw_gauge_ext_sp4(board_overlay_sp.bitmap, r, 1.0,
                                       DrawExt::RUBY_BAR_COLORS)
          end
        end
      end

      set_fpiece_in_field = ->(fpiece, x, y) do
        field[y][x] = fpiece
      end

      move_fpiece = ->(fpiece, new_x, new_y) do
        set_fpiece_in_field.(nil, fpiece.x, fpiece.y)
        fpiece.move(new_x, new_y)
        set_fpiece_in_field.(fpiece, fpiece.x, fpiece.y)
      end

      restrict_move_a = ->(move_a) do
        move_a.reject do |pos|
          pos.x < 0 || pos.x > 7 || pos.z < 0 || pos.z > 7 ||
          field[pos.z][pos.x]
        end
      end

      pieces.each { |fpiece| set_fpiece_in_field.(fpiece, fpiece.x, fpiece.y) }

      _test_loop_(TEST_TIME_LONG) do
        # The 3D mapping has its (y) inversed, so when we UP, the 3d pos actually
        # means down and vice versa
        # another option would be to use the (z) as (y)
        if Input.repeat?(:UP)
          cursor_pos.move_north(1)
        elsif Input.repeat?(:DOWN)
          cursor_pos.move_south(1)
        elsif Input.repeat?(:LEFT)
          cursor_pos.move_west(1)
        elsif Input.repeat?(:RIGHT)
          cursor_pos.move_east(1)
        end

        if Input.trigger?(:A)
          puts field.map { |a| a.map { |p| p ? p.team_sym : "." }.join("") }.join("\n")
        end

        if Input.trigger?(:C)
          if active_move_a && last_fpiece
            if move_field[cursor_pos.z][cursor_pos.x]
              move_fpiece.(last_fpiece, cursor_pos.x, cursor_pos.z)
              active_move_a = nil
            else
              Sound.play_buzzer
            end
          else
            fpiece = field[cursor_pos.z][cursor_pos.x]
            if fpiece
              last_fpiece = fpiece
              active_move_a = fpiece.calc_move_a
              active_move_a = restrict_move_a.(active_move_a)
              active_move_a = nil if active_move_a.empty?
            else
              last_fpiece = nil
              active_move_a = nil
              Sound.play_buzzer
            end
          end
          if active_move_a
            set_move_field.(active_move_a, true)
          else
            clear_move_field.()
          end
          refresh_overlay.()
        end

        # restrict movement
        cursor_pos.x %= 8
        cursor_pos.z %= 8

        cursor_sp.x = cursor_pos.x * tilesize
        cursor_sp.y = cursor_pos.z * tilesize

        if cursor_pos != old_pos
          old_pos = cursor_pos.dup
          rect = pos_detail_sp.bitmap.rect.dup
          rect.contract!(anchor: 5, amount: Metric.contract)
          pos_detail_sp.bitmap.clear
          pos_detail_sp.bitmap.draw_text(rect, "x: #{old_pos.x} y: #{old_pos.z}", 1)
        end

        piece_sps.each(&:update)
      end

      piece_sps.each(&:dispose)
      viewport.dispose
      board_sp.dispose_all
      cursor_sp.dispose_all
      board_overlay_sp.dispose_all
      pos_detail_sp.dispose_all
    end

    def title_text
      'Chess'
    end

  end
end