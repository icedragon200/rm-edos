module TestCore::Scene
  class Chipmunk01 < Scene::TestBase

    def start
      super
      character = Struct.new(:body, :shapes)
      viewport = nil
      @space = CP::Space.new
      @space.damping = 0.08
      body = CP::Body.new(1.0, 15.0)
      body.p = vec2(0.0, 0.0)
      body.v = vec2(0.0, 0.0)
      verts = [vec2(-0.5, -0.5), vec2(-0.5, 0.5), vec2(0.5, 0.5), vec2(0.5, -0.5)]
      shape = CP::Shape::Poly.new(body, verts, vec2(0.0, 0.0))
      @char = character.new(body, [shape])
      @space.add_body(body)
      @char.shapes.each { |s| @space.add_shape(s) }
      ###
      @sp = Sprite.new(viewport)
      @sp.bitmap = Bitmap.new(16, 16)
      @sp.bitmap.draw_gauge_ext(rate: 0.9)
      @sp.anchor_oxy!(anchor: 5)
      @sp.align_to!(anchor: 5)
      body.p = vec2(@sp.x, @sp.y)
      @dt = 1.0 / Graphics.frame_rate
      #
      loop do
        Main.update
        if Input.press?(:LEFT)
          @char.body.t -= 80
        elsif Input.press?(:RIGHT)
          @char.body.t += 80
        end
        if Input.press?(:UP)
          @char.body.apply_force(-(@char.body.a.radians_to_vec2 * 200), vec2(0.0, 0.0))
        elsif Input.press?(:DOWN)
          @char.body.apply_force((@char.body.a.radians_to_vec2 * 200), vec2(0.0, 0.0))
        end
        @space.step(@dt)
        @char.body.reset_forces
        pos = @char.body.p
        @sp.angle = @char.body.a.to_degree
        @sp.moveto(pos.x, pos.y)
      end
    end

  end
end