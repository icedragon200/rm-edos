module TestCore::Scene
  class Chipmunk02 < Scene::TestBase

    def start
      @space = CP::Space.new
      @space.gravity = vec2(0, 98)
      body = CP::Body.new(1, 15)
      verts = [vec2(-1, -1), vec2(-1, 1), vec2(1, 1), vec2(1, -1)]
      shape = CP::Shape::Poly.new(body, verts, vec2(0, 0))
      shape.e = 0.5
      shape.u = 1.0

      wall_body = CP::Body.new(Float::MAX, Float::MAX)
      wall_verts = [vec2(-8, -1), vec2(-8, 1), vec2(8, 1), vec2(8, -1)]
      wall_shape = CP::Shape::Poly.new(wall_body, wall_verts, vec2(0, 0))
      wall_shape.e = 1.0
      wall_shape.u = 1.0

      sp = Sprite.new(nil)
      sp.bitmap = Bitmap.new(16, 16)
      sp.bitmap.draw_gauge_ext
      sp.ox = sp.bitmap.width / 2

      sp_wall = Sprite.new
      sp_wall.bitmap = Bitmap.new(16 * 8, 16)
      sp_wall.bitmap.draw_gauge_ext
      sp_wall.ox = sp_wall.bitmap.width / 2
      delta = 1.0 / Graphics.frame_rate

      body.p = vec2(12, 8)
      wall_body.p = vec2(8, 24)

      @space.add_body(body)
      #@space.add_body(wall_body)

      @space.add_shape(shape)
      @space.add_static_shape(wall_shape)

      loop do
        Main.update
        if Mouse.left_press?
          body.p.x = Mouse.x / 8
          body.p.y = Mouse.y / 8
          body.reset_forces
        end
        @space.step(delta)
        pos = body.p
        pos_wall = wall_body.p
        sp.angle = wall_body.a.to_degree
        sp_wall.angle = wall_body.a.to_degree
        sp.moveto(pos.x * 8, pos.y * 8)
        sp_wall.moveto(pos_wall.x * 8, pos_wall.y * 8)
      end
    end

  end
end