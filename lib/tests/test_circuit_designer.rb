#
# EDOS/core/test_core/test-circuit_designer.rb
#   by IceDragon
#   dc 22/04/2013
#   dm 22/04/2013
# vr 0.0.1
module TestCore::Scene
  class CircuitDesigner < Scene::TestBase

    ## WIP
    # ::test_circuit_designer
    def start
      return TEST_DISABLED unless defined?(Cirkit)
      super
      # Settings
      gui_height = 5.mm.to_i
      block_size = 5.mm.to_i #1.cm.to_i # 32.px
      grid_back_color = Palette['gray16']
      grid_line_color = Palette['black']
      z_order = {
        grid:          0x01,
        circuit:       0x02,
        circuit_ghost: 0x03,
        grid_cursor:   0xFF,
        gui:           0x100,
      }

      # Variables
      components = []
      logs = []

      mouse_state      = :none
      org_pos          = nil
      placement        = nil #[nil, nil]
      last_pos         = nil #[nil, nil]
      last_fine_pos    = nil #[nil, nil]
      active_component = nil
      grabbed_pos      = nil
      skip_click       = false

      # Sprite
      grid_sp                  = Sprite.new(nil)
      grid_cursor_sp           = Sprite.new(nil)
      mouse_pos_sp             = Sprite::MousePos.new(nil, Mouse, gwidth/2, gui_height)
      circuit_overlay_sp       = Sprite.new(nil)
      circuit_ghost_overlay_sp = Sprite.new(nil)
      help_sp                  = Sprite::TextStrip.new(nil, gwidth/2, gui_height)
      message_log_sp           = Sprite::TextStrip.new(nil, gwidth/2, gui_height)
      mouse_info_sp            = Sprite.new(nil)

      # Bitmap
      grid_bmp = grid_sp.bitmap = bmp(gwidth, gheight)
        grid_bmp.fill(grid_back_color)
        grid_line_color.alpha = 178
        for y in 0..(grid_bmp.height / block_size)
          grid_bmp.blend_fill_rect(0, y * block_size,
                                   grid_bmp.width, 1, grid_line_color)
        end
        for x in 0..(grid_bmp.width / block_size)
          grid_bmp.blend_fill_rect(x * block_size, 0,
                                   1, grid_bmp.height, grid_line_color)
        end
        grid_bmp.blur
        grid_bmp.noise

      grid_cursor_bmp = grid_cursor_sp.bitmap = bmp(block_size, block_size)
        grid_cursor_bmp.draw_gauge_ext

      circuit_overlay_bmp = circuit_overlay_sp.bitmap = bmp(grid_sp.width, grid_sp.height)
      circuit_ghost_overlay_bmp = circuit_ghost_overlay_sp.bitmap = bmp(grid_sp.width, grid_sp.height)

      mouse_info_bmp = mouse_info_sp.bitmap = bmp(192, gui_height)

      # Position
      mouse_pos_sp.align_to!(anchor: 7)
      message_log_sp.align_to!(anchor: 9)
      help_sp.align_to!(anchor: 3)

      grid_sp.z                  = z_order[:grid]
      circuit_overlay_sp.z       = z_order[:circuit]
      circuit_ghost_overlay_sp.z = z_order[:circuit_ghost]
      grid_cursor_sp.z           = z_order[:grid_cursor]
      mouse_pos_sp.z             = z_order[:gui]
      help_sp.z                  = z_order[:gui]
      message_log_sp.z           = z_order[:gui]
      mouse_info_sp.z            = z_order[:grid_cursor]

      # Properties
      mouse_pos_sp.finetune = block_size
      circuit_ghost_overlay_sp.opacity = 0x80
      circuit_ghost_overlay_sp.visible = false


      # Functions
      add_component = ->(comp) do
        components.unshift(comp)
      end

      log = ->(msg) do
        logs.push(msg)
        message_log_sp.text = logs[-1]
        message_log_sp.align = MACL::Surface::ALIGN_CENTER
        message_log_sp.refresh
      end

      refresh = -> do
        circuit_overlay_bmp.clear
        components.reverse_each do |comp|
          draw_component(circuit_overlay_bmp, comp)
        end
      end

      clear_components = -> do
        circuit_overlay_sp.bitmap.clear
        circuit_ghost_overlay_bmp.clear
        components.clear
      end

      refresh_mouse_info = -> do
        mouse_info_bmp.clear
        #mouse_info_bmp.font.outline = true
        mouse_info_bmp.font.size = 16
        if active_component
          str = "X1: %d Y1: %d X2: %d Y2: %d" % active_component.to_sa
          mouse_info_bmp.draw_text(mouse_info_bmp.rect,
                                   str, MACL::Surface::ALIGN_CENTER)
        end
      end

      refresh_circuit_ghost_overlay = -> do
        circuit_ghost_overlay_bmp.clear
        if active_component
          draw_component(circuit_ghost_overlay_bmp, active_component)
        end
      end

      on_mouse_state_change = -> do
        help_sp.text  = "STATE: %s" % mouse_state.to_s
        help_sp.align = MACL::Surface::ALIGN_CENTER
        help_sp.refresh
      end

      on_mouse_moved = -> do
        mouse_info_sp.x = Mouse.x + 16
        mouse_info_sp.y = Mouse.y
      end

      on_mouse_fine_moved = -> do
        if active_component
          mx, my = Mouse.calc_finetune_pos(block_size)
          case mouse_state
          when :move
            active_component.freeform_do(false) do |surf|
              surf.x = mx - grabbed_pos[0]
              surf.y = my - grabbed_pos[1]
            end
            refresh_circuit_ghost_overlay.()
            refresh_mouse_info.()
          when :draw
            active_component.freeform_do(true) do |surf|
              if mx < placement[0]
                surf.x  = mx
                surf.x2 = placement[0]
              else
                surf.x  = placement[0]
                surf.x2 = mx
              end
              if my < placement[1]
                surf.y  = my
                surf.y2 = placement[1]
              else
                surf.y  = placement[1]
                surf.y2 = my
              end
            end
            refresh_circuit_ghost_overlay.()
            refresh_mouse_info.()
          end
        end
      end

      change_mouse_state = ->(new_state) do
        mouse_state = new_state
        on_mouse_state_change.()
      end

      b            = Cache.normal_bitmap('spritesheet/Batch32x')
      iconset      = Hazel::Onyx::Struct::Iconset.new(b, 32, 32, 16, nil)
      icon_refresh = Hazel::Onyx::Struct::Icon.new(iconset, 239)
      icon_clear   = Hazel::Onyx::Struct::Icon.new(iconset, 42)

      button_size = 40
      button_refresh = Hazel::Widget::Button.new(0, 0, button_size, button_size)
        button_refresh.add_event_handle(:mouse_left_click) do
          Sound.play_ex('button')
          refresh.()
          change_mouse_state.(:none)
          skip_click = true
          log.("Refreshing Workarea")
        end

        #button_refresh.label = 'Refresh'
        button_refresh.icon = icon_refresh

      button_clear = Hazel::Widget::Button.new(button_refresh.surface.x2, 0, button_size, button_size)
        button_clear.add_event_handle(:mouse_left_click) do
          Sound.play_ex('button')
          clear_components.()
          change_mouse_state.(:none)
          skip_click = true
          log.("All components cleared")
        end

        #button_clear.label = 'Clear'
        button_clear.icon = icon_clear

      widgets = [button_refresh, button_clear]
      MACL::Surface::Tool.anchor_surfaces(anchor: 1, src_surface: Graphics.rect,
                                          surfaces: widgets)
      widgets.each { |w| w.z = z_order[:gui] }
      onyx_spriteset = Hazel::Onyx::Spriteset_Components.new(widgets).refresh

      _test_loop_(TEST_TIME_LONG) do
        skip_click = false
        onyx_spriteset.update
        grid_cursor_sp.x = (Mouse.x / block_size) * block_size
        grid_cursor_sp.y = (Mouse.y / block_size) * block_size
        # logical update
        if !skip_click
          if Mouse.left_click?
            case mouse_state
            when :none
              active_component = MACL::Surface.new(0, 0, 0, 0)
              placement = Mouse.calc_finetune_pos(block_size)
              active_component.x, active_component.y = placement
              active_component.freeform = true
              change_mouse_state.(:draw)
              circuit_ghost_overlay_sp.visible = true
              mouse_info_sp.visible = true
              log.("Drawing")
            when :draw
              circuit_ghost_overlay_bmp.clear
              circuit_ghost_overlay_sp.visible = false
              mouse_info_sp.visible = false
              add_component.(active_component)
              active_component = nil
              change_mouse_state.(:none)
              refresh.()
              log.("Draw Complete")
            end
          elsif Mouse.right_click?
            case mouse_state
            when :draw
              circuit_ghost_overlay_bmp.clear
              circuit_ghost_overlay_sp.visible = false
              active_component = nil
              change_mouse_state.(:none)
              refresh.()
              log.("Draw Cancelled")
            when :move
              active_component.set(*org_pos)
              add_component.(active_component)
              active_component = nil
              grabbed_pos = nil
              change_mouse_state.(:none)
              circuit_ghost_overlay_sp.visible = false
              mouse_info_sp.visible = false
              refresh.()
              log.("Move Cancelled")
            end
          elsif Mouse.middle_click?
            case mouse_state
            when :none
              x, y = Mouse.pos
              comp = components.find do |c|
                x.between?(c.x, c.x2) && y.between?(c.y, c.y2)
              end
              if comp
                active_component = comp
                components.delete(active_component)
                org_pos = active_component.to_sa
                grabbed_pos = Mouse.calc_finetune_pos(block_size)
                grabbed_pos[0] -= active_component.x
                grabbed_pos[1] -= active_component.y
                change_mouse_state.(:move)
                circuit_ghost_overlay_sp.visible = true
                mouse_info_sp.visible = true
                refresh.()
                refresh_circuit_ghost_overlay.()
                log.("Moving")
              end
            when :move
              add_component.(active_component)
              active_component = nil
              grabbed_pos = nil
              change_mouse_state.(:none)
              circuit_ghost_overlay_sp.visible = false
              mouse_info_sp.visible = false
              refresh.()
              log.("Move Complete")
            end
          end
        end

        if last_pos != (n = [Mouse.x, Mouse.y])#
          last_pos = n
          on_mouse_moved.()
        end

        if last_fine_pos != (n = Mouse.calc_finetune_pos(block_size))
          last_fine_pos = n
          on_mouse_fine_moved.()
        end

        circuit_ghost_overlay_sp.update
        circuit_overlay_sp.update
        grid_cursor_sp.update
        grid_sp.update
        mouse_pos_sp.update
      end

      circuit_ghost_overlay_sp.dispose_all
      circuit_overlay_sp.dispose_all
      grid_cursor_sp.dispose_all
      grid_sp.dispose_all
      mouse_pos_sp.dispose
      onyx_spriteset.dispose

      return TEST_PASSED
    end

    def title_text
      'Circuit Designer'
    end

  end
end