module TestCore::Scene
  class Cirkit_DrawCircuit < Scene::TestBase

    def start
      return TEST_DISABLED unless defined?(Cirkit)
      super
      Cirkit.draw_circuit(:layout, true)
      Cirkit.draw_circuit(:wiring, true)

      sp = Sprite.new(nil)
      bmps = [Bitmap.new('circuit-layout-base'), Bitmap.new('circuit-wiring-base')]
      i = 0
      li = -1

      _test_loop_(TEST_TIME_LONG) do
        if Input.repeat?(:LEFT)
          i = (i - 1) % bmps.size
        elsif Input.repeat?(:RIGHT)
          i = (i + 1) % bmps.size
        end
        if li != i
          li = i
          sp.bitmap = bmps[i]
        end
      end

      sp.dispose
      bmps.each(&:dispose)

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end