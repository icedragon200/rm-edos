module TestCore::Scene
  class ColorBlend < Scene::TestBase

    def start
      super
      blends = Color.instance_methods.map(&:to_s).select { |s| s.start_with?('blend_') }
      blends.reject! { |s| s.end_with?('!') }
      p blends

      sp = spr(nil)
      sp.bitmap = bmp(gwidth, gheight)

      sz = 64
      cols = sp.bitmap.width / sz
      rows = sp.bitmap.height / sz

      src_color = Palette['droid_blue_light']

      blends.each_with_index do |blnd, i|
        rect = rct(i % cols * sz, i / cols * sz, sz, sz)
        color = src_color.send(blnd, 0.1)
        p color.to_s
        sp.bitmap.font.size = 12
        sp.bitmap.fill_rect(rect, color)
        sp.bitmap.draw_text(rect, blnd.gsub('blend_', ''), 1)
      end

      _test_loop_(TEST_TIME_LONG) do
        sp.update
      end

      sp.dispose_all

      return TEST_PASSED
    end

    def title_text
      'Color Blend'
    end

  end
end