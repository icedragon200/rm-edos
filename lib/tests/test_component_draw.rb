module TestCore::Scene
  class ComponentDraw < Scene::TestBase

    def start
      super
      #["=--",
      # ".--",
      # "=--"]
      component = Struct.new(:cx, :cy, :width, :height, :data)
      thing = component.new(2, 2, 5, 5, [0, 1, 1, 1, 0,
                                         0, 1, 0, 1, 0,
                                         1, 1, 0, 1, 1,
                                         1, 1, 0, 1, 1,
                                         1, 1, 0, 1, 0])

      bitsize = 16
      w, h = 10, 10
      color = Color.new(168, 120, 72)
      color_base = Palette['gray17']
      board_bmp = Bitmap.new(bitsize * w, bitsize * h)
      for y in 0...h
        for x in 0...w
          DrawExt.draw_gauge_base_sp1(board_bmp,
                                      rect: [x * bitsize, y * bitsize, bitsize, bitsize],
                                      colors: DrawExt.quick_gauge_colors(color_base, color))
        end
      end

      board_sp = Sprite.new
      board_sp.bitmap = board_bmp

      comp_sp = Sprite.new
      comp_sp.bitmap = nil

      draw_component = ->(comp) do
        comp_sp.bitmap.dispose if comp_sp.bitmap
        comp_sp.bitmap = Bitmap.new(comp.width * bitsize, comp.height * bitsize)
        comp_bmp = comp_sp.bitmap

        comp_color_base = Palette['gray17']
        comp_color = Palette['droid_blue']

        for y in 0...comp.height
          for x in 0...comp.width
            i = thing.data[x + (y * comp.width)]
            if i > 0
              DrawExt.draw_gauge_base_sp1(comp_bmp,
                                          rect: [x * bitsize, y * bitsize, bitsize, bitsize],
                                          colors: DrawExt.quick_gauge_colors(comp_color_base, comp_color))
            end
            #comp_bmp.fill_rect(x * bitsize, y * bitsize, bitsize, bitsize, )
          end
        end
      end

      draw_component.(thing)

      _test_loop_(TEST_TIME_LONG) do
        board_sp.update
        comp_sp.update
      end

      board_sp.dispose_all
      comp_sp.dispose_all

      return TEST_PASSED
    end

    def title_text
      'Component Draw'
    end

  end
end