module TestCore::Scene
  class Dll1 < Scene::TestBase

    def start
      super
      return TEST_DISABLED
      # DllTest
      #s = "\0" * 256
      #Win32API.new("TestDll3", "SomeFunction", "p", "i").call("Cheese")
      #puts Win32API.new("TestDll3", "Cheese", "", "i").call() + 1
      b = Bitmap.new(24, 24)
      b.fill_rect(b.rect, Palette['black'])
      puts b.get_pixel(0,0)
      rc = Win32API.new("IcyBitmapEx", "Bitmap_Recolor", "lll", "l")
      i = rc.call(b.object_id, Palette['black'].to_l, Palette['white'].to_l )
      puts i
      puts b.get_pixel(0,0)
      s = Sprite.new(nil)
      s.bitmap = Bitmap.new(132, 132)
      s.bitmap.ext_repeat_bmp(
        :draw_bmp => Cache.system("Craft_Borders(Window)"),
        :rect => Rect.new(0,0,32,32)
      )
      s.bitmap.ext_clear_round!( Rect.new(32, 32, 32, 64) )

      _test_loop_(TEST_TIME_LONG) do
        Graphics.update
        s.update
      end

      return TEST_PASSED
    end

    def title_text
      'Dll1'
    end

  end
end