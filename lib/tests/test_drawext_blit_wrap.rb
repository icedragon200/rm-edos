module TestCore::Scene
  class DrawExt_BlitWrap < Scene::TestBase

    def start
      super
      sp = Sprite.new
      bmp = sp.bitmap = Bitmap.new(Graphics.rect.width, Graphics.rect.height)
      bmp = bmp.dup

      rect = bmp.rect.dup
      rect2 = rect.dup
      rect.width /= 4
      rect.height /= 4

      rect2.width /= 4
      rect2.height /= 4
      rect2.y += rect2.height

      bmp.fill_rect(rect, Palette['sys1_blue'])
      rect.x += rect.width
      bmp.fill_rect(rect, Palette['sys1_red'])
      bmp.fill_rect(rect2, Palette['sys1_orange'])
      rect2.x += rect2.width
      bmp.fill_rect(rect2, Palette['sys1_green'])

      drect = Rect.new(0, 0, Graphics.width / 2, Graphics.height / 2)
      #drect2 = drect.dup
      #drect2.y += drect2.height

      #bmp.fill_rect(drect2, Palette['sys_green1'])
      mx, my = 0, 0
      last_x, last_y = 0, 0
      _test_loop_(TEST_TIME_LONG) do
        last_x = Mouse.x
        last_x %= drect.width

        last_y = Mouse.y
        last_y %= drect.height

        if last_x != mx or last_y != my
          mx = last_x
          my = last_y
          sp.bitmap.clear
          #sp.bitmap.clear_rect(drect)
          DrawExt.blit_wrap(sp.bitmap, drect, mx, my, bmp, drect)
        end
      end

      sp.dispose_all
      bmp.dispose

      return TEST_PASSED
    end

    def title_text
      'DrawExt::blit_wrap'
    end

  end
end