module TestCore::Scene
  class DrawExt_DrawGauge < Scene::TestBase

    def start
      super
      sprites = []

      # flat
      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(96, 96)

      color_padding = Palette['droid_dark_ui_enb']
      color_content = Palette['droid_light_ui_enb']
      padding = DrawExt.default_padding.dup
      padding.set(2, 2, 2, 2)

      rect = bmp.rect

      DrawExt.draw_padded_rect_flat(bmp, rect, [color_padding, color_content],
                                    padding)

      sprites << sp

      # gradient (vertical=false)
      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(96, 96)

      color_padding1 = Palette['droid_dark_ui_dis']
      color_padding2 = Palette['droid_dark_ui_enb']
      color_content1 = Palette['droid_light_ui_dis']
      color_content2 = Palette['droid_light_ui_enb']

      vertical = false

      padding = DrawExt.default_padding.dup

      padding.set(4, 4, 4, 4)

      rect = bmp.rect

      DrawExt.draw_padded_rect_gradient(bmp, rect,
                                        [color_padding1, color_padding2,
                                         color_content1, color_content2],
                                        vertical, padding)
      sprites << sp

      # gradient (vertical=true)
      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(96, 96)

      color_padding1 = Palette['sys1_red']
      color_padding2 = Palette['sys2_red']
      color_content1 = Palette['sys1_blue']
      color_content2 = Palette['sys2_blue']

      vertical = true

      padding = DrawExt.default_padding.dup

      #padding.set(8, 2, 8, 2)

      rect = bmp.rect

      DrawExt.draw_padded_rect_gradient(bmp, rect,
                                        [color_padding1, color_padding2,
                                        color_content1, color_content2],
                                        vertical, padding)
      sprites << sp


      # flat_multi
      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(96, 96)

      vertical = true

      padding = DrawExt.default_padding.dup

      padding.set(8, 6, 4, 2)

      rect = bmp.rect

      color_paddings = [Palette['sys1_red'], Palette['sys1_blue'], Palette['sys1_red'], Palette['sys1_orange']]

      DrawExt.draw_padded_rect_flat_multi(bmp, rect, [color_paddings, color_content], padding)

      sprites << sp
      #

      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(96, 24 * 4)

      color = Palette['sys1_green']
      vert = false
      rect = Rect.new(0, 0, bmp.width, 48)
      DrawExt.draw_gauge_flat(bmp, rect, 1.0, [color], vert, false)
      rect.y = rect.y2
      DrawExt.draw_gauge_flat_highlight(bmp, rect, 0.5,
                                        [color, Color.new(255, 255, 255, 78)],
                                        vert, false)

      sprites << sp
      #

      sp = Sprite.new()
      bmp = sp.bitmap = Bitmap.new(24 * 4, 96)

      color = Palette['sys1_blue']
      vert = true
      rect = Rect.new(0, 0, 48, bmp.height)
      DrawExt.draw_gauge_flat(bmp, rect, 1.0, [color], vert, false)
      rect.x = rect.x2
      DrawExt.draw_gauge_flat_highlight(bmp, rect, 0.5,
                                        [color, Color.new(255, 255, 255, 78)],
                                        vert, false)

      sprites << sp

      ###
      tile_surfaces(sprites)

      _test_loop_(TEST_TIME_SHORT) do
        sprites.each(&:update)
      end

      sprites.each do |s|
        s.bitmap.dispose
        s.dispose
      end

      return TEST_PASSED
    end

    def title_text
      'DrawExt#draw_gauge*'
    end

  end
end