module TestCore::Scene
  class DrawExt_DrawGlass < Scene::TestBase

  def self.test_draw_glass
    colors = DrawExt::TRANS_BAR_COLORS

    bmp = Bitmap.new(64, 64)
    rect = bmp.rect

    DrawExt.draw_gauge_specia2(
      bmp, rect, 1.0,
      [colors[:bar_outline1], colors[:bar_outline2],
      colors[:bar_inline1], colors[:bar_inline2],
      colors[:bar_highlight].dup.hset(alpha: 128)]
    )

    bmp2 = Cache.picture("smuge01")
    bmp.stretch_blt(
      bmp.rect,
      bmp2,
      bmp2.rect,
      42
    )

    bmp2.dispose
    #bmp.texture.save("glas.png")
    bmp.dispose

    return TEST_PASSED
  end

    def title_text
      'DrawExt::draw_glass'
    end

  end
end