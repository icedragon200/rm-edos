module TestCore::Scene
  class DrawExt_DrawRuler < Scene::TestBase

    def start
      super
      canvas = Screen.content
      title_sp = Sprite::MerioTitle.new(nil, "Draw Ruler", 0)
      title_sp.align_to!(anchor: 2, surface: canvas)
      help_sp = Sprite::MerioHelp.new(nil)
      help_sp.y2 = title_sp.y
      help_sp.set_text("[ESC]: finish test | [<]: decrease gauge | [>]: increase gauge")

      sp = Sprite.new
      wd = canvas.width
      hg = Metric.ui_element
      sp.bitmap = Bitmap.new(wd, hg)
      sp.y2 = help_sp.y

      vertical = false

      div16_c = Palette['black'].hset(alpha: 255 * 0.8)
      div8_c  = Palette['black'].hset(alpha: 255 * 0.6)
      div4_c  = Palette['black'].hset(alpha: 255 * 0.4)
      div2_c  = Palette['black'].hset(alpha: 255 * 0.2)

      divs = {
        2 => {
          l: 12,
          c: div2_c
        },
        4 => {
          l: 16,
          c: div4_c
        },
        8 => {
          l: 20,
          c: div8_c
        },
        16 => {
          l: 24,
          c: div16_c
        }
      }
      sp.x = (Graphics.width - sp.width) / 2

      rate = 1.0

      draw_gauge = proc do |r|
        cols = DrawExt::HP1_BAR_COLORS
        cols = DrawExt::HP2_BAR_COLORS if r < 0.50
        cols = DrawExt::HP3_BAR_COLORS if r < 0.15

        rect = sp.bitmap.rect.dup

        sp.bitmap.clear_rect(rect)

        DrawExt.draw_padded_rect_gradient(
          sp.bitmap, rect,
          [cols[:base_outline1], cols[:base_outline2],
          cols[:base_inline1], cols[:base_inline2]]
        )

        rect.contract!(anchor: 5, amount: 1)

        DrawExt.draw_gauge_specia2(
          sp.bitmap, rect, r,
          [cols[:bar_outline1], cols[:bar_outline2],
          cols[:bar_inline1], cols[:bar_inline2],
          cols[:bar_highlight]],
          vertical, false
        )

        rect1 = rect.dup

        #rect1.height /= 2
        #rect2 = rect1.dup
        #rect2.y += rect2.height
        rule_repeat = rect1.width / 16
        srct = Rect.new(0, 0, wd, hg)
        trct = Rect.new(0, 0, 16 * rule_repeat, rect1.height)
        new_divs = DrawExt.calc_rescale_rule_div(divs, srct, trct)
        DrawExt.draw_ruler(sp.bitmap, rect1, new_divs, vertical, 2)
        #DrawExt.draw_ruler(sp.bitmap, rect2, false, false, new_divs)
      end

      draw_gauge.(rate)

      _test_loop_(TEST_TIME_LONG) do
        sp.update
        title_sp.update
        help_sp.update
        if Input.press?(:LEFT)
          rate -= 0.01
          rate = rate.clamp(0.0, 1.0)
          draw_gauge.(rate)
        elsif Input.press?(:RIGHT)
          rate += 0.01
          rate = rate.clamp(0.0, 1.0)
          draw_gauge.(rate)
        end
      end

      sp.dispose_all
      title_sp.dispose
      help_sp.dispose

      return TEST_PASSED
    end

    def title_text
      'DrawExt::draw_ruler'
    end

  end
end