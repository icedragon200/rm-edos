module TestCore::Scene
  class DrawExt_GaugeColors < Scene::TestBase

    def super
      return #TEST_DISABLED
      super
      screen_bitmap = bmp(gwidth, gheight)
      Palette.entries.each do |(k, v)|
        screen_bitmap.clear
        color1 = v #Palette['droid_blue_light']
        color2 = v #Palette['sys_blue1']
        base_color1 = Palette['gray2']
        base_color2 = base_color1
        consts = DrawExt.constants.select { |n| n.to_s.start_with?("STYLER_") }
        prev_style = DrawExt::STYLE_DEFAULT

        consts.each_with_index do |const_name, i|
          style = DrawExt.const_get(const_name)
          colors1 = DrawExt.quick_gauge_colors(color1, base_color1, style, prev_style)
          #colors2 = DrawExt.quick_gauge_colors(color2, base_color2, style, prev_style)
          #colors = DrawExt.half_mix_gauge_colors(colors1, colors2)
          colors = DrawExt.patch_gauge_colors(colors1)

          cols = consts.size
          rows = 6

          cols, rows = rows, cols

          ni = i
          (1..6).each do |gi|
            rect = screen_bitmap.rect.dup
            rect.width /= cols
            rect.height /= rows
            rect.x += (ni / rows) * rect.width
            rect.y += (ni % rows) * rect.height
            #rect.x += (i % cols) * rect.width
            #rect.y += (i / cols) * rect.height
            r = 0.2 + 0.8 * (i.to_f / consts.size)
            DrawExt.send("draw_gauge_ext_sp#{gi}", screen_bitmap, rect, r, colors)
            ni += consts.size
          end

          prev_style = style
          #Graphics.freeze
          #Graphics.transition(15)
          #Main.wait(15)
        end
        Dir.mkdir("gauge_tests") unless Dir.exist?("gauge_tests")
        screen_bitmap.texture.save("gauge_tests/#{k}.png")
      end
      return TEST_PASSED
    end

    def title_text
      'DrawExt::gauge_colors'
    end

  end
end