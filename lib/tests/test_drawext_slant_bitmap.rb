module TestCore::Scene
  class DrawExt_SlantBitmap < Scene::TestBase

    def start
      super
      colors = DrawExt::HP2_BAR_COLORS
      grect = Rect.new(0, 0, 128, 16)
      wslant = 4
      hslant = 8

      bitmap = Bitmap.new(grect.width + wslant, grect.height + hslant)
      sp = Sprite.new
      sp.bitmap = bitmap
      sp.align_to!(anchor: 5, surface: canvas)

      _test_loop_(TEST_TIME_LONG) do
        sp.update

        #if Graphics.frame_count % 2 == 0
          bitmap.clear
          DrawExt.draw_gauge_ext(bitmap, grect, Graphics.frame_count % 60 / 60.0, colors)
          DrawExt.slant_bitmap(bitmap, grect, false, wslant, 2)
          DrawExt.slant_bitmap(bitmap, grect, true, hslant, 2)
        #end
      end

      sp.dispose
      bitmap.dispose

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end