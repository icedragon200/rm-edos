module TestCore::Scene
  class ColorBlend < Scene::TestBase

    def start
      super
      bmp = Bitmap.new(128, 32)
      cols = 16
      w = bmp.width / cols
      cols.times do |i|
        bmp.draw_gauge_ext(rect: Rect.new(i * w, 0, w, bmp.height),
                           colors: DrawExt.quick_bar_colors(Palette.entries.map(&:last).sample),
                           rate: 1.0)
      end
      sp = Sprite.new(nil)
      sp.bitmap = Bitmap.new(Screen.content.width, Screen.content.height)
      sp.bitmap.blt(32, 32, bmp, bmp.rect)
      v1 = Point2([32, 64])
      v2 = v1 - [16, 0]
      DrawExt.trapeze(sp.bitmap, v1, v2, bmp, bmp.rect)
      v1 = Point2([32, 96])
      v2 = v1 + [0, 16]
      DrawExt.trapeze(sp.bitmap, v1, v2, bmp, bmp.rect)
      v1 = Point2([32, 128 + 32])
      v2 = v1 + [16, 16]
      DrawExt.trapeze(sp.bitmap, v1, v2, bmp, bmp.rect)
      _test_loop_(TEST_TIME_LONG) do
        sp.update
      end
      sp.dispose_all
      bmp.dispose
      return TEST_PASSED
    end

    def title_text
      'DrawExt::trapeze'
    end

  end
end