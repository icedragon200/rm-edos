module TestCore::Scene
  class EntityViewer < Scene::TestBase

    def self.test_entity_viewer
      return TEST_DISABLED
      entity = $data_actor[21]
      sp = Sprite.new(nil)
      sp.bitmap = Bitmap.new(Graphics.width, Graphics.height)
      sp.bitmap.fill(Palette['droid_dark_ui_dis'])
      rows = sp.bitmap.height / 24

      draw_attr = ->(bmp, rect, label, value) do
        tr = Convert.Rect(rect)
        tr.contract!(anchor: 5, amount: Metric.contract / 2)
        bmp.fill_rect(tr, Palette['droid_dark_ui_enb'])
        tr.contract!(anchor: 5, amount: Metric.contract / 2)
        #bmp.blur
        bmp.font.size = 16
        bmp.font.bold = true
        bmp.font.outline = false
        bmp.font.color = Palette['merio_blue']
        bmp.draw_text(tr, label, 0)
        bmp.font.size = 16
        bmp.font.bold = false
        bmp.font.outline = false
        bmp.font.color = Palette['white']
        bmp.draw_text(tr, value, 2)
      end

      %w(id name icon_index tags description features note nickname class_id equips
         initial_level max_level
         character_name character_index character_hue
         face_name face_index face_hue).each_with_index do |atr, i|
        value = entity.respond_to?(atr) ? entity.send(atr).inspect : nil.inspect
        w = sp.bitmap.width / 2
        h = 24
        draw_attr.(sp.bitmap, [(i / rows) * w, (i % rows) * h, w, h], atr, value)
      end

      _test_loop_(TEST_TIME_LONG) do

      end

      sp.dispose_all

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end