#
# EDOS/core/test_core/test-falling_blocks.rb
#   by IceDragon
#   dc 30/05/2013
#   dm 30/05/2013
# vr 0.0.1
module TestCore::Scene
  class FallingBlocks < Scene::TestBase

    def start
      super
      gui_height = 32
      blocksize = 32
      rows = canvas.height / blocksize

      merio_colors = Palette.select { |(s, _)| s.to_s.start_with?("merio_") }
      block_bmps = merio_colors.map do |(_, c)|
        bmp = Bitmap.new(blocksize, blocksize)
        DrawExt.draw_gauge_ext_sp1(bmp, rect: bmp.rect,
                                   colors: DrawExt.quick_bar_colors(c))
        bmp
      end

      player = StarRuby::Vector2I.new(0, 0)
      player.y = rows - 1
      blocks = []
      distance = 0

      sprites = []
      player_sp = Sprite.new(nil)
      player_sp.bitmap = Bitmap.new(blocksize, blocksize)
      DrawExt.draw_gauge_ext_sp4(player_sp.bitmap)

      score_sp = Sprite::TextStrip.new(nil, canvas.width / 2, gui_height)
      freq_sp = Sprite::TextStrip.new(nil, canvas.width / 2, gui_height)
      freq_sp.x = score_sp.x2

      ticks = 0

      hit_player = false

      freq = nil
      blok_freq = nil

      add_block = ->(x, y=0) do
        blok = StarRuby::Vector2I.new(x, y)
        sp = Sprite.new(nil)
        sp.bitmap = block_bmps.sample
        blocks << [blok, sp]
      end

      remove_block = ->(blok) do
        blocks.reject! { |(b, s)| (s.dispose; true) if b == blok }
      end

      change_freq = ->(new_freq) do
        freq = new_freq
        blok_freq = freq * 4
        freq_sp.set_text("Frequency: %d" % freq, 1)
      end

      change_freq.(7)

      _test_loop_(TEST_TIME_LONG) do
        if ticks % blok_freq == 0
          xs = [-1, 0, 1]
          add_block.(xs.pick!, -rows)
          add_block.(xs.pick!, -rows) if rand(10) < 5
        end
        if ticks % freq == 0
          distance += 1
          #player.y += 1
          blocks.each do |(b, _)|
            b.y += 1
            if player.x == b.x && player.y == b.y
              puts "Player got wacked!"
              hit_player = true
              break
            end
          end
          score_sp.set_text("Score: #{distance.to_s}", 1)
          break if hit_player
        end

        if Input.repeat?(:UP)
          change_freq.(freq + 1)
        elsif Input.repeat?(:DOWN)
          change_freq.(freq - 1)
        end

        if Input.repeat?(:LEFT)
          if player.x > -1
            player.x -= 1
          end
        elsif Input.repeat?(:RIGHT)
          if player.x < 1
            player.x += 1
          end
        end

        player_sp.x = blocksize + player.x * blocksize
        player_sp.y = player.y * blocksize

        blocks.each do |(blok, sp)|
          sp.x = blocksize + blok.x * blocksize
          sp.y = blok.y * blocksize
          if blok.y > rows
            remove_block.(blok)
          end
        end
        ticks += 1
      end

      puts "Dodged for #{distance} blocks"

      blocks.each do |(_, sp)|
        sp.dispose
      end
      player_sp.dispose_all
      freq_sp.dispose
      score_sp.dispose
      block_bmps.each(&:dispose)

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end