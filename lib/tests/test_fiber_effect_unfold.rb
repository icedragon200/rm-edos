#
# EDOS/core/test_core/test_fiber_effect_unfold.rb
#   by IceDragon (mistdragon100@gmail.com)
module TestCore::Scene
  class FiberEffectUnfold < Scene::TestBase

    def start
      super
      sp1       = Sprite.new(nil)
      sp2       = Sprite.new(nil)
      spacer_sp = Sprite.new(nil)

      main_text = "Liminjus"
      sub_text = "Demiscu Forest - South"

      #h = Metric.ui_element_sml * 1.5
      con = Metric.contract
      wadd = Metric.contract * 2

      font = Font.new

      ##
      fs_nm = "merio_light_large_enb"
      font.set_style(fs_nm)
      w, h1 = font.text_size(main_text)
      sp1.bitmap = Bitmap.new(w + wadd, h1 + wadd)
      sp1.bitmap.font.set_style(fs_nm)
      sp1.bitmap.draw_text(sp1.bitmap.rect.contract(anchor: 5, amount: con), main_text, 0)

      ##
      fs_nm = "merio_light_medium_enb"
      font.set_style(fs_nm)
      w, h2 = font.text_size(sub_text)
      sp2.bitmap = Bitmap.new(w + wadd, h2 + wadd)
      sp2.bitmap.font.set_style(fs_nm)
      sp2.bitmap.draw_text(sp2.bitmap.rect.contract(anchor: 5, amount: con), sub_text, 0)

      ##
      spacer_sp.bitmap = Bitmap.new(1, h1 * 2)
      spacer_sp.bitmap.merio.draw_light_rect(spacer_sp.bitmap.rect)

      spacer_sp.x = sp1.x2
      sp2.x  = spacer_sp.x2
      sp1.cy = spacer_sp.cy
      sp2.cy = spacer_sp.cy

      spacebox = Lacio::SpaceBox.new([sp1, sp2, spacer_sp])
      spacebox.align_to!(anchor: 5, surface: canvas)

      #sp1.x = (Graphics.width - (sp1.width + sp2.width + spacer_sp.width)) / 2
      #sp1.y = (Graphics.height - spacer_sp.height) / 2

      unfold_fx = EDOS::FiberEffect::Unfold.new
      unfold_fx.sp1 = sp1
      unfold_fx.sp2 = sp2
      unfold_fx.spacer = spacer_sp
      unfold_fx.run

      _test_loop_(TEST_TIME_LONG) do
        #spacebox.moveto(Mouse.x, Mouse.y, 0)
        if unfold_fx
          unfold_fx.update
          if Input.trigger?(:C)
            unfold_fx.rerun
          end
        end
      end

      sp1.dispose_all
      sp2.dispose_all
      spacer_sp.dispose_all

      unfold_fx = nil

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end