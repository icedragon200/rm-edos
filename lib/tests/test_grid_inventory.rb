class RPG::BaseItem

  class Gridmap

    attr_reader :data

    def initialize
      @data = [[0, 0]]
      @width = nil
      @height = nil
    end

    def clear
      @data.clear
      refresh
    end

    def width
      @width ||= begin
        min_x = @data.min_by { |(x, y)| x }[0]
        max_x = @data.max_by { |(x, y)| x }[0]
        1 + (max_x - min_x).abs
      end
    end

    def height
      @height ||= begin
        min_y = @data.min_by { |(x, y)| y }[1]
        max_y = @data.max_by { |(x, y)| y }[1]
        1 + (max_y - min_y).abs
      end
    end

    def refresh
      @width = nil
      @height = nil
      width()
      height()
    end

  end

  def gridmap
    @gridmap ||= Gridmap.new
  end

end

module TestCore::Scene
  class GridInventory < Scene::TestBase

    def self.test_grid_inventory
      cell_size = Rect.new(0, 0, 24, 24)
      make_grid_image = ->(item) do
        gridmap = item.gridmap
        width  = gridmap.width
        height = gridmap.height
        bmp = Bitmap.new(cell_size.width * width, cell_size.height * height)
        item.gridmap.data.each do |(x, y)|
          bmp.draw_gauge_ext(rect: [x * cell_size.width, y * cell_size.height, cell_size.width, cell_size.height])
        end
        bmp
      end

      item1 = RPG::BaseItem.new
      item1.name = "ThatItem"
      item1.id = 1
      item1.icon.index = 24
      inventory = [item1]

      canvas = Screen.content
      sp = Sprite.new(nil)
      sp_item = Sprite.new(nil)


      sp.bitmap = Bitmap.new(canvas.width, canvas.height)
      art = Artist.new(sp.bitmap)
      art.merio do |m|
        m.draw_fixed_grid([0, 0, cell_size.width, cell_size.height],
                          12, 12, :dark, :enb)
      end

      sp_item.bitmap = make_grid_image.(item1)


      _test_loop_(TEST_TIME_LONG) do
        sp.update
        x, y = Mouse.calc_finetune_pos(cell_size.width)
        sp_item.x = x
        sp_item.y = y
        EDOS.server.send_data(EDOS::CHANNEL_EDOS_TOOL, item1.name)
      end

      sp.dispose_all
      sp_item.dispose_all

      return TEST_PASSED
    end

    def title_text
      'Grid Inventory'
    end

  end
end