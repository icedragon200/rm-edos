module TestCore

  def self.test_hopping_block
    ### constants
    viewport = nil
    cell_size = Size3.new(24, 24, 24)
    ### Functions
    lerp = lambda do |x, y, d|
      x + (y - x) * d
    end
    make_grid_bitmap = lambda do |w, h|
      bmp = Bitmap.new(cell_size.width * w, cell_size.height * h)
      h.to_i.times do |y|
        w.to_i.times do |x|
          bmp.draw_gauge_ext(rect: [x * cell_size.width, y * cell_size.height,
                                    cell_size.width, cell_size.height])
        end
      end
      bmp
    end
    ### Canvas
    canvas = Screen.content
    #### Allocation
    ### variables
    ltime         = 0.0
    ldelta        = 1.0 / 20#Graphics.frame_rate
    prj_point     = Point3.new(0, 0, 0)
    prj_src_point = Point3.new(0, 0, 0)
    prj_trg_point = Point3.new(0, 0, 0)
    ## Title
    title_sp = Sprite::MerioTitle.new(viewport, "Projectile", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    ## Help
    help_sp = Sprite::MerioHelp.new(viewport)
    help_sp.y2 = title_sp.y
    ## Sprites
    sprite_board = Sprite.new(viewport)
    sprite_proj  = Sprite.new(viewport)
    ## Bitmap
    h = canvas.height - help_sp.height - title_sp.height
    sprite_board.bitmap = make_grid_bitmap.(canvas.width / cell_size.width, h / cell_size.height)
    sprite_proj.bitmap  = Bitmap.new(cell_size.width, cell_size.height)
    sprite_proj.bitmap.draw_gauge_ext(colors: DrawExt::BLUE_BAR_COLORS)
    sprite_proj.anchor_oxy!(anchor: 5)
    ###
    help_sp.set_text("[ESC] quit test | [Mouse.left] Move block")
    ### Main-Loop
    _test_loop_(TEST_TIME_LONG) do
      ## mouse position
      mx, my = Mouse.calc_finetune_pos(cell_size)
      ## input
      if Mouse.left_click?
        prj_src_point = prj_point.dup
        prj_trg_point.x = mx / cell_size.width
        prj_trg_point.y = my / cell_size.height
        ltime = 0
      end
      ## delta
      ltime = [[ltime + ldelta, 0.0].max, 1.0].min
      ## lerp
      prj_point = lerp.(prj_src_point, prj_trg_point, ltime) #if ltime
      prj_point.z = 4 * Math.sin(ltime * Math::PI) #lerp.()
      ##
      sprite_proj.x = sprite_proj.ox + prj_point.x * cell_size.width
      sprite_proj.y = sprite_proj.oy + prj_point.y * cell_size.height - prj_point.z * cell_size.depth
      sprite_proj.zoom_x = sprite_proj.zoom_y = 1 + (prj_point.z / 2.0)
      ##
      title_sp.update
      help_sp.update
    end
    ### De-Allocation
    sprite_board.dispose_all
    sprite_proj.dispose_all
    help_sp.dispose
    title_sp.dispose
    ### Test-Result
    return TEST_PASSED
  end

end