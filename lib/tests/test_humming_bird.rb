#
# EDOS/core/test_core/test_humming_bird.rb
#   by IceDragon
module TestCore::Scene
  class HummingBird < Scene::TestBase

    def start
      super
      @message_server = MACL::HummingBird.new
      @message_shell = Shell::MerioMessage.new
      @message_shell.message_server = @message_server
    end

    def title_text
      'HummingBird: Message System'
    end

  end
end