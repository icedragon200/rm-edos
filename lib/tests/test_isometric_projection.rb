#
# EDOS/core/test_core/test_isometric_projection.rb
#
module TestCore

  Point = Struct.new(:x, :y, :z)

  def self.test_isometric_projection
    src_bmp = Cache.atlas("iso-64x64-building_2")

    calc_iso_src_rect_a = lambda do |index|
      [(index % 10) * 64 , (index / 10) * 64, 64, 64]
    end

    pos_a_to_iso_a = lambda do |x, y, z|
      rx = 0
      ry = 0
      rx += x / 2.0
      ry += x / 4.0
      rx -= y / 2.0
      ry += y / 4.0
      ry -= z / 2.0
      [rx, ry]
    end

    pos_to_iso_a = lambda do |point|
      pos_a_to_iso_a.(point.x, point.y, point.z)
    end

    ortho_point = Point.new(0, 0, 0)

    sp_back = Sprite.new
    sp = Sprite.new

    b = sp_back.bitmap = Bitmap.new(Screen.content.width, Screen.content.height)
    #wd = (b.width / 64)
    #hg = (b.height / 64)
    map_data = Table.new(10, 10, 4)
    height_map = Table.new(10, 10, 4)
    map_data.fill!(-1)
    map_data.set_layer!(0, [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 2, 4, 3, 2, 4, 0,
      0, 0, 2, 4, 3, 2, 4, 3, 2, 0,
      0, 0, 3, 2, 4, 3, 2, 4, 3, 0,
      0, 0, 4, 3, 2, 4, 3, 2, 4, 0,
      0, 0, 2, 4, 3, 2, 4, 3, 2, 0,
      0, 0, 3, 2, 4, 3, 2, 4, 3, 0,
      0, 0, 4, 3, 2, 4, 3, 2, 4, 0,
      0, 0, 2, 4, 3, 2, 4, 3, 2, 0,
      0, 0, 3, 2, 0, 0, 0,54, 0, 0,
    ])
    map_data.set_layer!(1, [
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,21,
     -1,-1,-1,-1,-1,-1,-1,-1,21,26,
     -1,-1,-1,-1,-1,-1,-1,-1,26,21,
     -1,-1,-1,-1,-1,-1,21,27,21,26,
     -1,-1,-1,-1,-1,21,27,25,27,21,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    ])
    map_data.set_layer!(2, [
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,63,
     -1,-1,-1,-1,-1,-1,-1,-1,63,14,
     -1,-1,-1,-1,-1,-1,-1,29,14,14,
     -1,-1,-1,-1,-1,-1,62,13,29,14,
     -1,-1,-1,-1,-1,62,13,13,13,29,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    ])
    height_map = Table.new(10, 10, 4)
    height_map.fill!(0)
    height_map.set_layer!(0, [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1, 0, 0, 0, 0, 0,-1,
      0,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    ])
    height_map.set_layer!(1, [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 2, 2, 2, 0,
      0, 0, 0, 0, 0, 0, 2, 2, 2, 0,
      0, 0, 0, 0, 0, 0, 2, 2, 2, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ])
    height_map.set_layer!(2, [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
      0, 0, 0, 0, 0, 1, 3, 3, 3, 1,
      0, 0, 0, 0, 0, 1, 3, 4, 3, 1,
      0, 0, 0, 0, 0, 1, 3, 3, 3, 1,
      0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ])
    xo = (b.width / 64 / 2) * 64
    map_data.zsize.times do |z|
      map_data.ysize.times do |y|
        map_data.xsize.times do |x|
          tile_id = map_data[x, y, z]
          next if tile_id < 0
          rect_a = calc_iso_src_rect_a.(tile_id)
          dx, dy = pos_a_to_iso_a.(x, y, height_map[x, y, z])
          b.blt(xo + dx * 64, dy * 64, src_bmp, rect_a)
          Main.wait(1)
        end
      end
    end

    tile_index = 0

    sp.bitmap = src_bmp
    sp.src_rect.set(*calc_iso_src_rect_a.(tile_index))

    _test_loop_(TEST_TIME_LONG) do
      if Input.repeat?(:p)
        tile_index += 1
        sp.src_rect.set(*calc_iso_src_rect_a.(tile_index))
      elsif Input.repeat?(:o)
        tile_index -= 1
        sp.src_rect.set(*calc_iso_src_rect_a.(tile_index))
      end

      if Input.repeat?(:L)
        ortho_point.z -= 1
      elsif Input.repeat?(:R)
        ortho_point.z += 1
      end

      if Input.repeat?(:RIGHT)
        ortho_point.x += 1
      elsif Input.repeat?(:LEFT)
        ortho_point.x -= 1
      end

      if Input.repeat?(:UP)
        ortho_point.y -= 1
      elsif Input.repeat?(:DOWN)
        ortho_point.y += 1
      end
      x, y = pos_to_iso_a.(ortho_point)
      sp.x, sp.y = (x * 64), (y * 64)
    end

    sp.dispose_all
    sp_back.dispose_all

    return TEST_PASSED
  end

end
