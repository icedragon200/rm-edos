#
# EDOS/core/test_core/test_lacio_spreadsheet.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
module TestCore

  def self.test_lacio_spreadsheet
    require 'pp'
    cellbank = Lacio::Spreadsheet.chunk(Rect.new(0, 0, 192, 64)) do |spd|
      spd.row.col(nil) # title
      spd.row.cols(4) #
      spd.row.col(0.5, 0.20, nil)
      pp spd
    end

    pp cellbank
    pp cellbank.cell(0, 0)
    pp cellbank.cell(1, 0)
    pp cellbank.cell(2, 0)
    pp cellbank.row(2)

    return TEST_PASSED
  end

end
