#
# EDOS/core/test_core/test-macl_geometry_path#follow.rb
#
module TestCore

  def self.test_macl_geometry_path
    canvas = Screen.content
    i = 0
    t = 560.0
    sp = spr(nil)
    sp.bitmap = Cache.system('ball')
    sp.anchor_oxy!(anchor: 0x222)
    path = MACL::Geometry::Path.new(pnt(sp.ox, sp.oy),
                                    pnt(canvas.width - sp.ox, canvas.height / 2),
                                    pnt(canvas.width / 2, canvas.height - sp.oy))

    _e_quad = MACL::Easer::Quad
    _test_loop_(TEST_TIME_LONG) do
      if Input.trigger?(:C)
        i = 0
      end
      if i < t
        i += 1
        x, y = *path.path_lerp(i / t, _e_quad::Out)
        sp.x = x
        sp.y = y
        #p sp.to_a
      end
    end

    sp.dispose_all

    return TEST_PASSED
  end

end
