module TestCore

  def test_macl_matrix
    matrix = MACL::Matrix.new([4, 4, 4], 0).dec.dec!
    matrix2 = MACL::Matrix.new([4, 4, 4], 3).dec!
    matrix3 = MACL::Matrix.new([2, 2, 4], 4).dec!

    matrix2.add_at!([0, 0, 0], matrix3, [0, 0, 0], [2, 2, 4])
    matrix.add!(matrix2)

    puts matrix.data.each_slice(4).to_a.map(&:inspect).join("\n")

    return TEST_PASSED
  end

end
