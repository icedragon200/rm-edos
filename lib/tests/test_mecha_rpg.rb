#
# EDOS/core/test_core/test-mecha_rpg.rb
#   by IceDragon
#   dc 05/04/2013
#   dm 05/04/2013
# vr 1.0.0
#$LOAD_PATH.push("/home/icy/Dropbox/code/Git")
#require 'RGSS3-MACL/MACL.rb'
#require_relative '../../src/module/EDOS.rb'

module TestCore
  class ShellMechaHud < Hazel::Shell::Window

    include Hazel::Shell::Addons::Background
    include Mixin::UnitHost

    def initialize(x, y, w, h)
      super(x, y, w, h)
    end

    def standard_padding
      Metric.padding
    end

    def on_unit_change
      refresh
    end

    def refresh
      bmp = contents
      bmp.clear
      if @unit
        entity = @unit.entity
        merio = artist.merio
        merio.snapshot
        merio.font_size = :micro
        artist do |art|
          gauge_x = bmp.width / 2
          gauge_h = bmp.height / 3
          art.draw_unit_name(entity, art.line_rect(0, 0, bmp.width / 2))
          art.draw_entity_hp(entity, [gauge_x, gauge_h * 0, 192, gauge_h])
          art.draw_entity_ap(entity, [gauge_x, gauge_h * 1, 192, gauge_h])
          art.draw_entity_wt(entity, [gauge_x, gauge_h * 2, 192, gauge_h])
        end
        merio.restore
      end
    end

  end
  class Gauge

    attr_reader :pos, :battler, :value

    def initialize(pos, battler, sizes=[16, 4], &handle)
      @pos = pos
      @battler = battler
      @handle = handle

      colors = DrawExt::HP1_BAR_COLORS
      @base = Sprite.new(nil)
      @base.bitmap = Bitmap.new(sizes[0], sizes[1])
      r = @base.bitmap.draw_gauge_base(colors: colors)
      @bar = Sprite.new(nil)
      @bar.bitmap = Bitmap.new(@base.width, @base.height)
      @bar.bitmap.draw_gauge_bar(rect: r, colors: colors)
      @disposed = false
    end

    def disposed?
      !!@disposed
    end

    def dispose
      @base.dispose_all
      @bar.dispose_all
      @disposed = true
    end

    def update
      @value = @handle.(@battler)
      @bar.src_rect.width = @bar.bitmap.width * @value
      @base.x = @bar.x = pos.x - pos.ox
      @base.y = @bar.y = pos.y #- pos.oy - @base.height
      @bar.z = 1 + (@base.z = pos.z)
    end

  end

  def self.test_mecha_rpg
    canvas = Screen.content
    # structure > energy > flow > structure
    elem_s = Mecha::Element.new(0, "Skarboss") # Element of Structure
    elem_e = Mecha::Element.new(1, "Ethodium") # Element of Energy
    elem_f = Mecha::Element.new(2, "Oxiliphe") # Element of Flow/Life

    elem_table = Mecha::ElementTable.new(3)
    # 2x
    elem_table.set_rate(elem_s, elem_e, 2.0)
    elem_table.set_rate(elem_e, elem_f, 2.0)
    elem_table.set_rate(elem_f, elem_s, 2.0)
    # 0.5x
    elem_table.set_rate(elem_e, elem_s, 0.5)
    elem_table.set_rate(elem_f, elem_e, 0.5)
    elem_table.set_rate(elem_s, elem_f, 0.5)
    # 1.0x
    elem_table.set_rate(elem_s, elem_s, 1.0)
    elem_table.set_rate(elem_e, elem_e, 1.0)
    elem_table.set_rate(elem_f, elem_f, 1.0)

    # constant
    wt_per_turn = 500 * 12
    gui_height = 24

    # variable
    wt_turn = 0
    made_action = false
    action_complete = false
    action = nil
    wait = 0
    unit_index = 0
    cursor_mode = false

    cursor = StarRuby::Vector2I.new(0, 0)

    entity     = Mecha::Entity.new
    character  = Mecha::Character.new
    unit       = Mecha::Unit.new(entity, character)
    units = [unit, unit.deep_clone]
    units[0].entity.change_name("Mecha 1")
    units[1].entity.change_name("Mecha 2")
    units[-1].character.moveto(1, 1)
    unit = nil # used later down for the current active unit

    ## message_server
    server = EDOS::Server.new
    server.add_channel('log', 'log')
    listener = EDOS::Server::CallbackListener.new do |channel, data|
      puts data
    end
    server.listen_to(listener, 'log')

    ## state_machine
    turn_control = Mecha::TurnControl.new(server)
    turn_control.add_callback(:on_change_state) do |state, old_state|
      puts "State: #{old_state} >> #{state}"
    end

    ## table
    field = Mecha::Field.new(10, 10)

    ### visual code
    chipmap = SRRI::Chipmap.new
    tilesize = chipmap.tilesize = 24
    chipmap.tile_columns = 2
    chipmap.x = 0
    chipmap.y = 24
    chipmap.z = 0

    # prepare tile_bitmap
    b = chipmap.tile_bitmap = Bitmap.new(tilesize * 2, tilesize * 2)
    r = b.rect / 2
    r.x = r.width
    DrawExt.draw_gauge_ext_sp4(b, r, 1.0, DrawExt.quick_bar_colors(Palette['gray17']))
    r.x = 0
    r.y = r.height
    DrawExt.draw_gauge_ext_sp4(b, r, 1.0, DrawExt.quick_bar_colors(Palette['sys1_green']))

    # set map_data
    d = chipmap.map_data = field.data
    chipmap.shadow_data  = field.shadow_data
    chipmap.flash_data   = field.flash_data
    r = d.rect
    d.fill_rect(r, 1)
    d.fill_rect(r.contract(anchor: 5, amount: 1), 2)
    d.bucket_fill2(2, 2, 2)
    print_table(d)

    # finally refresh the chipmap
    begin
      hspan = tilesize * d.xsize
      vspan = tilesize * d.ysize
      chipmap.refresh.viewrect.set(0, 0, hspan, vspan)
    end
    #chipmap.rect.align_to!(anchor: 5, surface: canvas) # not implemented ;-;
    #print_table.(chipmap.map_data)

    # Cursor Sprite
    cursor_sp = Sprite.new(nil)
    cursor_sp.bitmap = Bitmap.new(tilesize + 2, tilesize + 2)
    cursor_sp.ox = (cursor_sp.bitmap.width - tilesize) / 2
    cursor_sp.oy = (cursor_sp.bitmap.height - tilesize) / 2
    begin
      r = cursor_sp.bitmap.rect
      cursor_sp.bitmap.fill_rect(r, Palette['droid_blue_light'])
      cursor_sp.bitmap.clear_rect(r.contract(anchor: 5, amount: 1))
    end
    cursor_sp.z = 100

    # Characters
    char_sps = []
    gauges   = []
    units.each do |un|
      char_sp = Sprite.new(nil)
      char_sp.bitmap = Bitmap.new(tilesize, tilesize)
      DrawExt.draw_gauge_ext_sp4(char_sp.bitmap,
                                 colors: DrawExt.quick_bar_colors(Palette['droid_blue']))
      gg = Gauge.new(char_sp, un.entity, [tilesize, 4]) { |b| b.hp_rate }
      char_sps.push(char_sp)
      gauges.push(gg)
    end

    # GUI
    w = canvas.width
    turn_sp  = Sprite::TextStrip.new(nil, w / 2, gui_height)
    state_sp = Sprite::TextStrip.new(nil, w / 2, gui_height)
    state_sp.align_to!(anchor: 9, surface: canvas)
    hud_shell = ShellMechaHud.new(0, 0, w, 48)
    hud_shell.align_to!(anchor: 2, surface: canvas)

    # Function
    change_unit = ->(new_unit) do
      hud_shell.set_unit(unit = new_unit)
    end

    repeat_dir4 = -> do
      if    Input.repeat?(:DOWN)  then 2
      elsif Input.repeat?(:LEFT)  then 4
      elsif Input.repeat?(:RIGHT) then 6
      elsif Input.repeat?(:UP)    then 8
      else                             0
      end
    end

    change_wait = ->(new_wait) do
      wait = new_wait
    end

    wait_short = -> { change_wait.(15) }

    handle_wait = -> do
      (wait > 0 ? (wait -= 1) : wait) == 0
    end

    handle_wt = -> do
      entities = units.map(&:entity)
      lowest_wt_entity = entities.min_by(&:wt)
      wt = lowest_wt_entity.wt
      entities.each_with_object(wt, &:dec_wt)
      wt_turn -= wt
      lowest_wt_entity.unit
    end

    ## callback_handle
    turn_control.add_callback(:on_change_state) do |state, _|
      sym = ("on_" + state.to_s).to_sym
      units.each(&sym)
      state_sp.set_text("State: %s" % state, 1)
    end

    turn_control.add_callback(:on_change_turn) do |turn, old_turn|
      turn_sp.set_text("Turn: %d" % turn, 1)
    end

    ## state_machine fiber
    fiber = Fiber.new { turn_control.main }
    fiber.resume

    # Main Loop
    _test_loop_(TEST_TIME_LONG) do
      if Input.trigger?(:B)
        if cursor_mode = !cursor_mode
          # it was activated !
        else
          # it was de-activated
          cursor.x = unit.character.x
          cursor.y = unit.character.y
        end
      end
      ## logic_update
      st = turn_control.state
      # if wait has been completed then you can handle a new state from the
      # state machine
      if handle_wait.()
        case st
        when :battle_start
          fiber.resume(true)
        when :turn_start
          # Unit index remains the same with the WT system
          #unit_index
          wt_turn = wt_per_turn
          # Unit index is reset every turn start, since every unit had acted
          #unit_index = 0
          fiber.resume(true)
          wait_short.()
        when :unit_start
          change_unit.(handle_wt.())
          made_action = false
          cursor.x = unit.character.x
          cursor.y = unit.character.y
          fiber.resume(true)
          wait_short.()
        when :unit_make_action
          if (d = repeat_dir4.()) > 0
            action = [:move, d]
            made_action = true
          end unless cursor_mode
          if made_action
            action_complete = false
            fiber.resume(true)
          end
        when :unit_action_start
          fiber.resume(true)
        when :unit_action
          if action
            what, param = action
            case what
            when :move
              unit.character.move_straight(d)
            end
            action_complete = true
          else
            action_complete = true
          end
          fiber.resume(true) if action_complete
        when :unit_action_end
          fiber.resume(true)
        when :unit_next_action
          made_action = false
          action_complete = false
          fiber.resume(false) # we cannot make another action, so just break
        when :unit_end
          fiber.resume(true)
        when :next_unit
          unit.entity.reset_wt if unit

          # Turn ends once the turn ticks have reached 0, regardless of unit
          unit_index = unit_index + 1 % units.size
          fiber.resume(wt_turn > 0)
          # Turn ends once all units have completed there action
          #unit_index = (unit_index + 1)
          #if unit_index < units.size
          #  fiber.resume(true) # there is still a unit left
          #else
          #  fiber.resume(false) # There are no more units
          #end
        when :turn_end
          # nothing to be done here
          fiber.resume(true)
        when :next_turn
          fiber.resume(true) # continue to next turn
        when :battle_end
          # nothing to be done here
          fiber.resume(true)
        end
      end

      if cursor_mode
        if (n = repeat_dir4.()) > 0
          anchor = MACL::Surface::Tool.obj_to_anchor(n)
          vec = MACL::Surface::Tool::ANCHOR_VECTOR[anchor]
          cursor.add!(vec.to_a)
        end
      end

      server.update

      # Graphic Update
      #cursor_sp.visible = !unit.nil?
      chipmap.update

      units.each_with_index do |un, i|
        char_sp = char_sps[i]
        gauge = gauges[i]
        char_sp.x = chipmap.x + un.character.screen_x(tilesize)
        char_sp.y = chipmap.y + un.character.screen_y(tilesize)
        char_sp.z = chipmap.z + un.character.screen_z(tilesize)
        char_sp.update
        gauge.update
      end
      cursor_sp.x = chipmap.x + cursor.x * tilesize
      cursor_sp.y = chipmap.y + cursor.y * tilesize

      turn_sp.update
      state_sp.update
      cursor_sp.update
    end

    turn_sp.dispose
    state_sp.dispose
    cursor_sp.dispose_all
    hud_shell.dispose
    chipmap.dispose
    gauges.each(&:dispose)
    char_sps.each(&:dispose)

    return TEST_PASSED
  end

end
