module TestCore

  def self.test_merio_draw
    canvas = Screen.content
    sz = Metric.ui_element
    h = Metric.ui_element_sml
    sp = Sprite.new(nil)
    sp.bitmap = Bitmap.new(canvas.width, canvas.height)
    title_sp = Sprite::MerioTitle.new(nil, "DrawExt#merio", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    title_sp.z = 1
    font_names = Dir.glob("fonts/*.{TTF,OTF,ttf,otf}").map { |f| File.basename(f) }
    font_name_index = 0
    palettes = DrawExt::Merio.palettes.to_a
    palette_index = 0

    merio = DrawExt::Merio::DrawContext.wrap(sp.bitmap)

    font_name = merio.font_name

    redraw_func = -> do
    merio.snapshot do
      #title_sp.refresh

      merio.font_size = :micro
      font_name_index %= font_names.size
      palette_index %= palettes.size
      merio.font_name = [font_names[font_name_index]]
      nm, pal = palettes[palette_index]
      txtpalnm = pal.text_palette_suggestion
      tpal = DrawExt::Merio.palettes[txtpalnm]
      #tpal = if nm.end_with?("_i")
      #  DrawExt::Merio.palette['default_i']
      #else
      #  DrawExt::Merio.palette['default']
      #end
      merio.main_palette = pal
      merio.txt_palette = tpal

      palette_name = nm

      bmp = sp.bitmap
      bmp.clear
      r  = Rect.new(0, 0, sz * 2, h)
      r2 = Rect.new(sz * 2, 0, sz, sz)
      r3 = Rect.new(gwidth / 2, 0, sz * 2, h)
      r4 = r3.dup; r4.width, r4.height = r4.height, r4.width; r4.x += r3.width

      fl_r = bmp.rect.dup
      fl_r.height -= title_sp.height
      bmp.fill_rect(fl_r, Palette['gray12'])
      #bmp.noise

      ## merio switch test
      merio.draw_switch(r, false)
      merio.draw_switch(r.step!(2), true)
      merio.draw_switch_w_label(r.step!(2),  false, "OFF", "ON")
      merio.draw_switch_w_label(r.step!(2),  true,  "OFF", "ON")
      merio.draw_switch_w_label1(r.step!(2), false, "OFF", "ON")
      merio.draw_switch_w_label1(r.step!(2), true,  "OFF", "ON")
      merio.draw_switch_w_label2(r.step!(2), false, "OFF", "ON")
      merio.draw_switch_w_label2(r.step!(2), true,  "OFF", "ON")

      ## merio checkbox
      merio.draw_checkbox(r.step!(2), false)
      merio.draw_checkbox(r.step!(2), true)

      ## merio pair test
      merio.draw_pair(r.step!(2), "NUM", 12)
      merio.draw_light_label(r.step!(2), "Status")
        #merio.glob_gauge_palette = merio.palettes['red_on_default_i']
      merio.draw_label_w_gauge(r.step!(2), "HP", 0.88)
        #merio.glob_gauge_palette = merio.palettes['blue_on_default_i']
      merio.draw_label_w_gauge(r.step!(2), "MP", 0.38, false, 2)
      merio.draw_pair_fmt(r.step!(2), "ATK", 7)
      merio.draw_pair_fmt(r.step!(2), "DEF", 5, "%04d")
      merio.draw_dark_label(r.step!(2), "Magic")

      ## merio tile test
      merio.draw_light_tile(r2, 1, 1)
      merio.draw_dark_tile(r2.step!(2, 2), 2, 1)
      merio.draw_light_tile(r2.step!(2, 2), 3, 1)
      merio.draw_dark_tile(r2.step!(8, 4).step!(6, 2), 1, 2)
      merio.draw_light_tile(r2.step!(6, 2), 1, 3)

      ## merio gauge test
      #merio.glob_gauge_palette = merio.palettes['green_on_default_i']
      # horizontal
      3.times do |i|
        merio.draw_gauge2(r3.step!(2), 0.00, false, i)
        merio.draw_gauge2(r3.step!(2), 0.25, false, i)
        merio.draw_gauge2(r3.step!(2), 0.50, false, i)
        merio.draw_gauge2(r3.step!(2), 0.75, false, i)
        merio.draw_gauge2(r3.step!(2), 1.00, false, i)
      end

      3.times do |i|
        merio.draw_gauge2(r4.step!(6), 0.00, true, i)
        merio.draw_gauge2(r4.step!(6), 0.25, true, i)
        merio.draw_gauge2(r4.step!(6), 0.50, true, i)
        merio.draw_gauge2(r4.step!(6), 0.75, true, i)
        merio.draw_gauge2(r4.step!(6), 1.00, true, i)
        #r4.step!(2).step!(4, 5)
      end

      merio.font_config(nil, :default, nil)
      bmp.font.outline = false
      r = bmp.rect.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                            amount: Metric.contract)
      r.height = Metric.ui_element_sml
      r.y2 = bmp.rect.y2
      r.y2 -= title_sp.height
      r.y2 -= r.height * 3
      bmp.draw_text(r, "Font: %s" % bmp.font.name[0], 2)
      bmp.draw_text(r.step(8, 1), "Text Palette: %s" % txtpalnm, 2)
      bmp.draw_text(r.step(8, 2), "Palette: %s" % palette_name, 2)

      r.step!(2)
      [("A".."Z").to_a, ("a".."z").to_a, (0..9).to_a].each_with_index do |a, i|
        bmp.draw_text(r, a.join(" "), 0)
        r.step!(2)
      end

    end
    end

    redraw_func.()

    flag_redraw = false

    _test_loop_(nil) do
      if Input.repeat?(:LEFT)
        palette_index -= 1
        flag_redraw = true
      elsif Input.repeat?(:RIGHT)
        palette_index += 1
        flag_redraw = true
      end

      if Input.repeat?(:UP)
        font_name_index -= 1
        flag_redraw = true
      elsif Input.repeat?(:DOWN)
        font_name_index += 1
        flag_redraw = true
      end

      if flag_redraw
        flag_redraw = false
        redraw_func.()
      end
      title_sp.update
    end

    ### free
    title_sp.dispose
    sp.dispose_all

    ###
    #DrawExt::Merio.font_name = font_name

    return TEST_PASSED
  end

end
