module TestCore::Scene
  class Merio_Gauges < Scene::TestBase

    def start
      super
      gauge_width  = Metric.ui_element #menu_canvas.width
      gauge_height = Metric.ui_element #Metric.ui_element_sml
      gauge_count  = Integer(menu_canvas.width / gauge_width) *
                     Integer(menu_canvas.height / gauge_height)
      @gauges = gauge_count.times.map do |i|
        g = MerioGauge.new(nil)
        g.resize_gauge(gauge_width, gauge_height)
        g.x = 0
        g.y = 0
        #g.align_to!(anchor: 46, surface: menu_canvas)
        #g.y = menu_canvas.y + i * g.height
        g.update
        g.rate = 0.10 + rand
        window_manager.add(g)
        g
      end

      MACL::Surface::Tool.tile_surfaces(@gauges, menu_canvas)
      @gauges.each { |g| g.x += g.width / 2 }
      #pkmn_palettes_names = DrawExt.gauge_palettes.keys.select { |s| s.start_with?("pkmn") }
      #pkmn_palettes_names.each_with_index do |s, i|
      #  @gauges[i].gauge_colors = s
      #end
      @gauges[ 0].gauge_colors = DrawExt.gauge_palettes['red']
      @gauges[ 1].gauge_colors = DrawExt.gauge_palettes['green']
      @gauges[ 2].gauge_colors = DrawExt.gauge_palettes['blue']
      @gauges[ 3].gauge_colors = DrawExt.gauge_palettes['yellow']
      @gauges[ 4].gauge_colors = DrawExt.gauge_palettes['hp']
      @gauges[ 5].gauge_colors = DrawExt.gauge_palettes['mp']
      @gauges[ 6].gauge_colors = DrawExt.gauge_palettes['ap']
      @gauges[ 7].gauge_colors = DrawExt.gauge_palettes['wt']
      @gauges[ 8].gauge_colors = DrawExt.gauge_palettes['exp']
      ##
      @gauges[ 9].gauge_colors = DrawExt.gauge_palettes['element0']
      @gauges[10].gauge_colors = DrawExt.gauge_palettes['element1']
      @gauges[11].gauge_colors = DrawExt.gauge_palettes['element2']
      @gauges[12].gauge_colors = DrawExt.gauge_palettes['element3']
      @gauges[13].gauge_colors = DrawExt.gauge_palettes['element4']
      @gauges[14].gauge_colors = DrawExt.gauge_palettes['element5']
      @gauges[15].gauge_colors = DrawExt.gauge_palettes['element6']
      @gauges[16].gauge_colors = DrawExt.gauge_palettes['element7']
      @gauges[17].gauge_colors = DrawExt.gauge_palettes['element8']
      ##
      @gauges[18].gauge_colors = DrawExt.gauge_palettes['keyboard']
    end

    def update
      super
      if Input.trigger?(:C)
        @gauges.each do |g|
          g.rate = 0.10 + rand
        end
      end
    end

    def title_text
      "Merio : Gauges"
    end

  end
end