#
# EDOS/core/test_core/test-merio_inventory.rb
#   by IceDragon
#   dc 02/04/2013
#   dm 02/04/2013
# vr 1.0.0
module TestCore::Scene
  class Merio_Inventory < Scene::TestBase

    def start
      super
      ### constants
      @unit = $game.party
      @cols = 14 # inventory rows (actually halved since the block size is x2 wide)
      @rows = 6  # inventory cols
      @tab_half = false # has the scrollbar passed the half mark
      ### functions

      ##
      @placeholder_sp = Sprite::MerioSpacer.new(viewport)
      @placeholder_sp.align_to!(anchor: 8, surface: canvas)
      #placeholder_sp

      r = Rect.new(0, 0, Metric.ui_element * @cols, Metric.ui_element * @rows)
      #r.expand!(anchor: 5, amount: Metric.contract)
      r.x = 0
      r.y = 0
      @inventory_shell = Shell::MerioInventory.new(r)
      @inventory_shell.help_window = help_window
      @inventory_shell.set_unit(@unit)
      @inventory_shell.activate
      @inventory_shell.select(0)
      @inventory_shell.y2 = help_window.y
      ## debug
      #inventory_shell.shell_callback.callback_log = STDOUT

      w, h = Graphics.width - @inventory_shell.width, @inventory_shell.height
      @scrollbar = MerioScrollBar.new(viewport, w, h)
      @scrollbar.x = @inventory_shell.x2
      @scrollbar.y = @inventory_shell.y

      @inventory_shell.add_callback(:index=) do |win, *_, &_|
        @scrollbar.set_index_max(win.row_max)
        @scrollbar.set_index(win.row_index)
      end

      ### refresh
      @inventory_shell.index = 0
      ### z_order
      @inventory_shell.z = 3
      @scrollbar.z = 0
      @placeholder_sp.z = 0

      window_manager.add(@inventory_shell)
      window_manager.add(@placeholder_sp)
      window_manager.add(@scrollbar)
    end

    def title_text
      "Merio : Inventory"
    end

  end
end