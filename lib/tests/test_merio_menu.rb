module TestCore::Scene
  class Merio_Menu < Scene::TestBase

    def start
      super
      ### constants
      canvas = Screen.content
      ###
      sp = Sprite.new
      sp.x = canvas.x
      sp.y = canvas.y
      sp.z = -100
      status_sprite = Sprite.new
      title_sp = Sprite::MerioTitle.new(nil, "Menu", 0)
      title_sp.align_to!(anchor: 2, surface: canvas)
      help_sp = Sprite::MerioHelp.new(nil, canvas.width)
      help_sp.y2 = title_sp.y

      status_bmp = status_sprite.bitmap =
        Bitmap.new(canvas.width, Metric.ui_element_sml)

      bmp = sp.bitmap = Bitmap.new(canvas.width, canvas.height-title_sp.height)
      bmp.font.outline = false
      bmp.font.color = Palette['droid_dark_ui_enb']

      #bmp.fill_rect(bmp.rect, Palette['droid_dark'])
      bmp_fille = Cache.picture("tactile_noise")
      DrawExt.repeat_bmp(bmp, bmp.rect, bmp_fille, bmp_fille.rect)

      rect_navi = sp.bitmap.rect
      rect_navi.height = Metric.ui_element

      rect_info = title_sp.to_rect
      #rect_info = rect_navi.dup
      #rect_info.height = Metric.ui_element
      #rect_info.align_to!(anchor: 2, surface: bmp.rect)

      #rect_help.y2 = rect_info.y

      ### functions
      draw_section = ->(trg_bmp, rect) do
        trg_bmp.merio.draw_light_rect(rect)
        #trg_bmp.blend_fill_rect(rect, Palette['droid_light_ui_enb'])
        #trg_bmp.blend_fill_rect(
        #  rect.x, rect.y2 - 1, rect.width, 1, Palette['droid_dark_ui_enb'])
      end

      status_sprite.x = help_sp.x
      status_sprite.y = help_sp.y

      command_strs = ["Item", "Skill", "Equip", "Status", "Books", "System"]
      w, h = rect_navi.width / command_strs.size, rect_navi.height

      draw_section.(bmp, rect_navi) # Navigation
      #bmp.blend_fill_rect(rect_help, Palette['droid_dark_ui_enb'])
      #draw_section.(bmp, rect_help) # Info
      #draw_section.(bmp, rect_info) # Info

      command_sprs = []

      index = 0
      event_stack = []

      command_strs.each_with_index do |str, i|
        r = Rect.new(rect_navi.x + i * w, canvas.y, w, h)
        r.contract!(anchor: 5, amount: Metric.contract)

        s = Sprite.new(@viewport)

        bp = s.bitmap = Bitmap.new(r.width, r.height)

        bp.blend_fill_rect(
          Metric.contract, bp.rect.y2 - Metric.contract,
          bp.rect.width - Metric.contract * 2, Metric.contract,
          Palette['droid_dark_ui_dis'])
        bp.fill_rect(0, 0, 1, r.height, Palette['droid_dark_ui_dis'])
        bp.fill_rect(r.width-1, 0, 1, r.height, Palette['droid_dark_ui_dis'])

        bp.font.set_style('merio_dark_large_enb')
        bp.draw_text(0, 0, r.width, r.height, str, 1)

        s.x, s.y = r.x, r.y
        #widget = Hazel::Widget::Button.new(r.x, r.y, r.width, r.height)

        std_tone = Tone.new
        trg_tone_abs = Palette['droid_blue'].hset(alpha: 0).to_tone
        trg_tone = s.tone

        stt = mstt = 10

        gear = Clockwork::Gearwork.new
        sm = Clockwork::StateMachine.new(->(new_state) do
                                          gear.set_state(new_state)
                                        end)
        sm.set_transition(    :focus => :animate,
                          :not_focus => :animate,
                            :animate => :rest)

        gear.add_callback(:focus) do
          stt = mstt
          trg_tone = trg_tone_abs.dup
          sm.transition
        end

        gear.add_callback(:not_focus) do
          stt = mstt
          trg_tone = std_tone.dup
          sm.transition
        end

        gear.add_callback(:animate) do
          s.tone.lerp!(trg_tone, 1 - stt / mstt.to_f)
          stt -= 1 if stt > 0
          sm.transition if stt <= 0
        end

        gear.activate

        command_sprs << s
        event_stack.push(Clockwork::Automata.new(sm: sm, gear: gear))
      end

      event_stack[index].set_state(:focus)

      bmp.font.set_style("merio_dark_h1_enb")
      bmp.draw_text(
        rect_info.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                           amount: Metric.contract), "Menu")

      unit = $game.party.members[0]
      entity = unit.entity
      entity.hp = (entity.mhp * 0.4).to_i
      entity.mp = (entity.mmp * 0.4).to_i

      menu_status = Shell::MerioMenuStatus.new(rect_navi.cx,
                                               canvas.y + rect_navi.height,
                                               rect_navi.width / 2,
                                               canvas.height - rect_info.height - rect_navi.height - help_sp.height)
      portrait_sp = Sprite::UnitPortrait.new(nil, unit)
      portrait_sp.x = 0
      portrait_sp.y2 = help_sp.y

      time_sp = Sprite::MerioTime.new
      time_sp.align_to!(anchor: 3, surface: canvas)

      menu_status.set_unit(unit)

      last_index = index

      status_tip = [
                      "View Party Inventory",
                      "View Party Member Skills",
                      "View Party Member Armanents",
                      "View Party Member Status",
                      "View Party Books",
                      "View System Commands"
                    ]

      update_help = -> do
        help_sp.set_text(status_tip[last_index], 0)
      end

      navi_clockworks = []

      sp_n1 = Sprite.new
      sp_n1.bitmap = Cache.system("menu-icons/navi-prev")

      sp_n1.x = menu_status.contents_x
      sp_n1.y = menu_status.contents_y

      sp_n2 = Sprite.new
      sp_n2.bitmap = Cache.system("menu-icons/navi-next")

      sp_n2.x = menu_status.contents_x + menu_status.contents_width - sp_n2.width
      sp_n2.y = menu_status.contents_y

      [sp_n1, sp_n2].each do |s|
        std_tone = Tone.new
        trg_tone_abs = Palette['droid_blue_light'].hset(alpha: 0).to_tone
        trg_tone = s.tone

        stt = mstt = 10

        gear = Clockwork::Gearwork.new
        sm = Clockwork::StateMachine.new(->(new_state) do
                                          gear.set_state(new_state)
                                        end)
        sm.set_transition(    :focus => :animate,
                          :not_focus => :animate,
                            :animate => :rest)

        gear.add_callback(:focus) do
          stt = mstt
          trg_tone = trg_tone_abs.dup
          sm.transition
        end

        gear.add_callback(:not_focus) do
          stt = mstt
          trg_tone = std_tone.dup
          sm.transition
        end

        gear.add_callback(:animate) do
          s.tone.lerp!(trg_tone, 1 - stt / mstt.to_f)
          stt -= 1 if stt > 0
          sm.transition if stt <= 0
        end

        gear.activate
        navi_clockworks.push(Clockwork::Automata.new(sm: sm, gear: gear))
      end

      member_index = 0
      last_member_index = 0

      update_navi = -> do
        navi_clockworks[0].set_state(member_index > 0 ? :focus : :not_focus)
        navi_clockworks[1].set_state(member_index < $game.party.members.size-1 ? :focus : :not_focus)
      end

      change_unit = ->(new_unit) do
        portrait_sp.set_unit(new_unit)
        menu_status.set_unit(new_unit)
        update_navi.()
      end

      update_help.()
      update_navi.()

      _test_loop_(TEST_TIME_LONG) do
        time_sp.update
        title_sp.update
        sp.update
        menu_status.update
        event_stack.each(&:update)
        navi_clockworks.each(&:update)

        if Input.repeat?(:RIGHT)
          index += 1
          index %= command_sprs.size
        elsif Input.repeat?(:LEFT)
          index -= 1
          index %= command_sprs.size
        end

        if Input.repeat?(:R)
          if member_index < $game.party.members.size - 1
            member_index += 1
            menu_status.switch = +1
          else
            Sound.play_buzzer
          end
          #member_index %= $game.party.members.size
        elsif Input.repeat?(:L)
          if member_index > 0
            member_index -= 1
            menu_status.switch = -1
          else
            Sound.play_buzzer
          end
          #member_index %= $game.party.members.size
        end

        if last_member_index != member_index
          last_member_index = member_index
          unit = $game.party.members[last_member_index]

          change_unit.(unit)
          Sound.play_ex('cursor2')
        end

        if last_index != index

          event_stack[last_index].set_state(:not_focus)
          event_stack[index].set_state(:focus)
          last_index = index

          update_help.()
          Sound.play_cursor
        end
      end

      sp.dispose_all
      sp_n1.dispose_all
      sp_n2.dispose_all
      time_sp.dispose
      title_sp.dispose
      help_sp.dispose
      portrait_sp.dispose
      status_sprite.dispose_all

      command_sprs.each(&:dispose_all)

      menu_status.dispose
    end

  end
end