#
# EDOS/core/test_core/test-merio_pokemon_battle.rb
#
module TestCore

  module Pokemon

    class DatabaseObject

      attr_accessor :id
      attr_accessor :name

      def initialize(opts={})
        opts.each { |k, v| self.send(k.to_s + "=", v) }
      end

    end

    ##
    # id_s - ID String
    #   This is used to obtain the correct color palette for the Type
    class Type < DatabaseObject

      attr_accessor :id_s

    end

    class Move < DatabaseObject

      attr_accessor :mpp  # Max PP
      attr_accessor :type # Like Element
      attr_accessor :dm_type # Phy or Sp

    end

    class GameMove

      attr_reader :name
      attr_reader :pp
      attr_reader :mpp
      attr_reader :type
      attr_reader :dm_type

      def initialize
        @name        = ''
        @pp          = 1
        @mpp         = 1
        @type        = 0
        @dm_type = 0
      end

      def setup(data_move)
        @name    = data_move.name
        @mpp     = data_move.mpp
        @type    = data_move.type
        @dm_type = data_move.dm_type
        restore_pp
        return self
      end

      def pp=(new_pp)
        @pp = [[0, new_pp].max, @mpp].min
      end

      def restore_pp
        self.pp = @mpp
      end

      def pp_a
        return pp, mpp
      end

      def pp_r
        return pp / mpp.to_f
      end

    end

    normal   = Type.new(id_s: "normal",   name: "Normal")
    grass    = Type.new(id_s: "grass",    name: "Grass")
    water    = Type.new(id_s: "water",    name: "Water")
    fire     = Type.new(id_s: "fire",     name: "Fire")
    bug      = Type.new(id_s: "bug",      name: "Bug")
    rock     = Type.new(id_s: "rock",     name: "Rock")
    ground   = Type.new(id_s: "ground",   name: "Ground")
    electric = Type.new(id_s: "electric", name: "Electric")
    flying   = Type.new(id_s: "flying",   name: "Flying")
    poison   = Type.new(id_s: "poison",   name: "Poison")
    psychic  = Type.new(id_s: "psychic",  name: "Psychic")
    fight    = Type.new(id_s: "fight",    name: "Fight")
    ghost    = Type.new(id_s: "ghost",    name: "Ghost")
    ice      = Type.new(id_s: "ice",      name: "Ice")
    steel    = Type.new(id_s: "steel",    name: "Steel")
    dark     = Type.new(id_s: "dark",     name: "Dark")
    dragon   = Type.new(id_s: "dragon",   name: "Dragon")

    @type_table = {
      1  => normal,
      2  => grass,
      3  => water,
      4  => fire,
      5  => bug,
      6  => rock,
      7  => ground,
      8  => electric,
      9  => flying,
      10 => poison,
      11 => psychic,
      12 => fight,
      13 => ghost,
      14 => ice,
      15 => steel,
      16 => dark,
      17 => dragon,
    }

    @move_table = {
      1 => Move.new(name: 'Tackle',    mpp: 35, type: normal, dm_type: :phy),
      2 => Move.new(name: 'Growl',     mpp: 40, type: normal, dm_type: :phy),
      3 => Move.new(name: 'Ember',     mpp: 35, type: fire,   dm_type: :phy),
      4 => Move.new(name: 'Water Gun', mpp: 35, type: water,  dm_type: :phy)
    }

    def self.get_move_by_id(id)
      return @move_table[id]
    end

  end

  class SpriteMove < Sprite

    attr_accessor :gmove
    attr_accessor :index

    def initialize(viewport)
      super(viewport)
      @index = 0
      self.bitmap = Bitmap.new(Metric.ui_element * 2, Metric.ui_element_mid * 1.5)
    end

    def set_gmove(gmove)
      if @gmove != gmove
        @gmove = gmove
        refresh
      end
      return self
    end

    def refresh
      merio = bitmap.merio
      merio.bitmap.clear
      #merio.fill_type = :smooth_round
      merio.border_size = 1
      rect_base = merio.bitmap.rect
      rect_top = rect_base.contract(anchor: 5, amount: merio.border_size)
      rect_top.height /= 2
      rect_name = rect_top.dup
      rect_name.width -= Metric.ui_element_sml
      rect_id = rect_name.dup
      rect_id.width = Metric.ui_element_sml
      rect_name.x += rect_id.width
      rect_type = rect_top.dup
      rect_type.width /= 2
      rect_type.step!(2)
      rect_pp = rect_type.step(6)
      rect_pp_text  = rect_pp.dup
      rect_pp_text.height /= 2
      rect_pp_gauge = rect_pp_text.step(2)
      ###
      merio.snapshot do
        merio.draw_dark_rect(rect_base)
        if @gmove
          ## Type
          pal_name = "pkmn-#{@gmove.type.id_s}"
          merio.change_main_palette(pal_name)
          merio.txt_palette = pal_name
          merio.snapshot do
            merio.draw_dark_rect(rect_type)
            merio.font_config(:light, :micro, :enb)
            merio.bitmap.draw_text(rect_type, @gmove.type.name, 1)
          end
          ## ID
          merio.snapshot do
            merio.change_main_palette(nil)
            merio.draw_gauge2(rect_id, @gmove.pp_r)
          end
          merio.font_config(:dark, :micro, :enb)
          merio.bitmap.draw_text(rect_id, index + 1, 1)
          ## Name
          merio.font_config(:light, :small, :enb)
          merio.bitmap.draw_text(rect_name, @gmove.name, 1)
          ### PP
          merio.snapshot do
            merio.change_main_palette(pal_name)
            merio.draw_gauge2(rect_pp_gauge, @gmove.pp_r)
          end
          merio.font_config(:light, :micro, :enb)
          merio.bitmap.draw_text(rect_pp_text, '%d / %d' % @gmove.pp_a, 1)
        end
      end
    end

  end

  def self.test_merio_pokemon_battle
    ### constants
    viewport = nil
    moves = [
      Pokemon::GameMove.new.setup(Pokemon.get_move_by_id(1)),
      Pokemon::GameMove.new.setup(Pokemon.get_move_by_id(2)),
      Pokemon::GameMove.new.setup(Pokemon.get_move_by_id(3)),
      Pokemon::GameMove.new.setup(Pokemon.get_move_by_id(4))
    ]
    move_count_max = 4
    move_cols = 4
    ### Canvas
    canvas = Screen.content
    canvas_content = Screen.menu_content
    ### Allocation
    ## Title
    title_sp = Sprite::MerioTitle.new(viewport, "Pokemon Battle", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    ## Help
    help_sp = Sprite::MerioHelp.new(viewport)
    help_sp.y2 = title_sp.y
    ##
    move_sps = Array.new(move_count_max) { SpriteMove.new(viewport) }
    move_sps.each_with_index do |s, i|
      s.index = i
      s.x = i % move_cols * s.width
      s.y = i / move_cols * s.height
      s.set_gmove(moves[i])
    end
    moves_box = Lacio::SpaceBox.new(move_sps)
    moves_box.align_to!(anchor: 3, surface: canvas_content)
    ### z_order
    title_sp.z = 0
    help_sp.z  = 0

    ### functions

    ### Main-Loop
    _test_loop_(nil) do
      #if Mouse.left_click?
      #  a1 = Automation::Move.new([0, 0], [0, -32], Metric::Time.sec_to_frame(0.25))
      #  a2 = Automation::Move.new([0, 0], [0, 32], Metric::Time.sec_to_frame(0.25))
      #  chained = Automation::Chained.new(a1, a2)
      #  moves_box.add_automation(chained)
      #end
      ##
      title_sp.update
      help_sp.update
      move_sps.each(&:update)
      moves_box.update
    end
    ### De-Allocation
    help_sp.dispose
    move_sps.each(&:dispose)
    title_sp.dispose
    moves_box.dispose
    ### Test-Result
    return TEST_PASSED
  end

end