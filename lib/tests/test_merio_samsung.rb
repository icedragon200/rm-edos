#
# EDOS/core/test_core/test_merio_samsung.rb
#   by IceDragon
# This Test mimics a Samsung cell phone UI
module TestCore::Scene
  class Merio_Samsung < Scene::TestBase

    def start
      super
      canvas = Screen.content
      back_sp = Sprite.new(nil)
      back_sp.bitmap = Bitmap.new(canvas.width, Metric.ui_element)

      timedate_sp = Sprite::MerioTimeDate.new(nil)
      title_sp = Sprite::MerioTitle.new(nil, "Merio Samsung TimeStrip", 0)
      title_sp.align_to!(anchor: 2, surface: canvas)

      back_shade = timedate_sp.inverted ? :light : :dark

      begin
        bmp = back_sp.bitmap
        bmp.blend_fill_rect(bmp.rect, Palette["droid_#{back_shade}_ui_enb"])
      end

      move = ->(x, y, z) do
        back_sp.x = timedate_sp.x = x
        back_sp.y = timedate_sp.y = y
        timedate_sp.z = 1 + (back_sp.z = z)
      end

      move.(0, Metric.ui_element_sml, 0)

      ticks = 0

      _test_loop_(TEST_TIME_LONG) do
        back_sp.update
        title_sp.update
        timedate_sp.update
        ticks += 1
      end

      title_sp.dispose
      back_sp.dispose_all
      timedate_sp.dispose
    end

  end
end