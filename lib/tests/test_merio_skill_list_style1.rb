#
# EDOS/core/test_core/test-merio_skill_list_style1.rb
#
module TestCore::Scene
  class Merio_SkillList_Style1 < Scene::TestBase

    def start
      super
      ### constants
      @unit_index = 0
      @unit = $game.party.members[@unit_index]
      $game.actors.each do |actor| next unless actor
        actor.entity.title = actor.entity.class.name
      end
      #$game.actors[1].entity.title = "Fighter"
      #$game.actors[2].entity.title = "Witch"
      #$game.actors[3].entity.title = "Squiress"
      $data_skills.select { |s| s && !s.name.empty? }.each do |skill|
        $game.party.members.each do |gunit|
        #if skill.tags.include?("elemental")
          gunit.entity.learn_skill(skill.id)
          #unit.entity.magiclearn_learn_skill(skill.id)
          gunit.entity.change_magiclearn(skill.id, rand(101))#100)
        end
        #end
      end
      @cols = 14 # inventory rows (actually halved since the block size is x2 wide)
      @rows = 6  # inventory cols
      @tab_half = false # has the scrollbar passed the half mark
      ### Allocation
      ## Placeholder
      @status_shell = Shell::MerioStatusSmall.new(0, 0)
      @status_shell.align_to!(anchor: 8, surface: canvas)
      s = @status_shell.to_rect.contract(anchor: 46, amount: Metric.contract)
      ## Navigation
      @navi_icons = [Sprite::MenuIcon.new(nil, 5),
                     Sprite::MenuIcon.new(nil, 6)]
      @navi_icons[0].align_to!(anchor: 4, surface: s)
      @navi_icons[1].align_to!(anchor: 6, surface: s)
      ## Item List Shell
      r = Rect.new(0, 0, Metric.ui_element * @cols, Metric.ui_element * @rows)
      @skill_list_shell = Shell::MerioSkillList.new(r)
      @skill_list_shell.help_window = help_window
      @skill_list_shell.set_unit(@unit)
      @skill_list_shell.activate
      @skill_list_shell.select(0)
      @skill_list_shell.y2 = help_window.y
      ##
      w, h = Graphics.width - @skill_list_shell.width, @skill_list_shell.height
      @scrollbar = MerioScrollBar.new(nil, w, h)
      @scrollbar.x = @skill_list_shell.x2
      @scrollbar.y = @skill_list_shell.y

      old_row_index = nil
      @skill_list_shell.add_callback(:index=) do |win, *_, &_|
        @scrollbar.set_index_max(win.row_max)
        @scrollbar.set_index(win.row_index)
      end

      ### refresh
      @skill_list_shell.index = 0
      ### z_order
      @skill_list_shell.z = 3
      @scrollbar.z = 0
      @status_shell.z = 0
      @navi_icons.each_with_object(4, &:z=)
      ### functions
      change_unit_index = lambda do |new_index|
        @unit_index = new_index % $game.party.members.size
        @unit = $game.party.members[@unit_index]
        @skill_list_shell.set_unit(@unit)
        @status_shell.set_unit(@unit)
        @skill_list_shell.activate
      end

      flash_tone = Palette['droid_blue'].to_tone
      flast_tone_duration = 15

      prev_unit = lambda do
        @navi_icons[0].tone_flash(flash_tone, flast_tone_duration)
        change_unit_index.(@unit_index.pred)
      end
      next_unit = lambda do
        @navi_icons[1].tone_flash(flash_tone, flast_tone_duration)
        change_unit_index.(@unit_index.next)
      end
      @skill_list_shell.set_handler(:pageup, prev_unit)
      @skill_list_shell.set_handler(:pagedown, next_unit)
      change_unit_index.(0)
      ### Main-Loop
      window_manager.add(@status_shell)
      window_manager.add(@skill_list_shell)
      window_manager.add(@scrollbar)
      @navi_icons.each { |i| window_manager.add(i) }
    end

    def title_text
      "Merio : Skill List [Style 1]"
    end

  end
end