module TestCore::Scene
  class Merio_SkillList_Style2 < Scene::TestBase

    class SkillBox < Lacio::SpaceBox

      attr_accessor :skill
      attr_reader :viewport
      attr_accessor :async_task

      def initialize(viewport, skill)
        @skill = skill
        @viewport = viewport
        @base_sp = Sprite.new(viewport)
        @text_sp = Sprite.new(viewport)
        super([@base_sp, @text_sp])
        @async_task = refresh
      end

      def viewport=(new_viewport)
        @viewport = new_viewport
        @base_sp.viewport = @viewport
        @text_sp.viewport = @viewport
      end

      def refresh
        ##
        br = Rect.new(0, 0, 144, 24) #192, 24)
        br2 = br.contract(anchor: 5, amount: 1)
        r1 = Rect.new(br2.x, br2.y, 40 + 8, br2.height)
        r2 = Rect.new(br2.x, br2.y, 64, br2.height)
        r2.align_to!(anchor: 6, surface: br2)
        r3 = Rect.new(r1.width - 16, r1.y, br2.width - (r1.width - 16) - (r2.width - 16), br2.height)
        ##
        tmp_bmp = Bitmap.new(br.width, br.height)
        base_bmp = @base_sp.bitmap = tmp_bmp.dup
        text_bmp = @text_sp.bitmap = tmp_bmp.dup
        text_art = Artist.new(text_bmp)
        blend_type = StarRuby::Texture::BLEND_ALPHA
        base_bmp.round_blend_fill_rect(br, Palette['droid_dark_ui_dis'],  blend_type, 8)
        base_bmp.round_blend_fill_rect(r1, Palette['droid_dark_ui_enb'],  blend_type, 8)
        base_bmp.round_blend_fill_rect(r2, Palette['droid_dark_ui_enb'],  blend_type, 8)
        base_bmp.round_blend_fill_rect(r3, Palette['droid_light_ui_enb'], blend_type, 8)
        tmp_bmp.round_blend_fill_rect(r1, Palette["element#{skill.element_id}"],  blend_type, 8)
        #base_bmp.clear_rect([0, 0, 8, tmp_bmp.width])
        base_bmp.blt(0, 0, tmp_bmp, [0, 0, 8, tmp_bmp.height])
        tmp_bmp.clear
        text_bmp.merio.snapshot do |m|
          m.font_config(:dark, :small, :enb)
          text_bmp.draw_text(r3, skill ? skill.name : "", 1)
          m.font_config(:light, :small, :enb)
          text_bmp.draw_text(r2.contract(anchor: 46, amount: Metric.contract), skill.calc_mp_cost(nil), 2)
          text_art.draw_icon(skill ? skill.icon : 0, r1.x + 8, r2.y)
        end
        tmp_bmp.dispose
      end

      async :refresh

    end

    def start
      super
      sps = []
      $data_skills.compact.each do |skill|
        sp = SkillBox.new(viewport, skill)
        window_manager.add(sp)
        sps << sp
      end
      sps.each { |sp| sp.async_task.await }
      MACL::Surface::Tool.tile_surfaces(sps, menu_canvas)
    end

    def title_text
      "Merio : Skill List [Style 2]"
    end

  end
end