#
# EDOS/core/test_core/test_merio_skill_list_style3.rb
#   by IceDragon
class Hazel::Shell::MerioSkillList_Style3 < UI::ShellMerioItemList

  def item_width
    Metric.ui_element #* 3
  end

  def item_height
    Metric.ui_element #* 3
  end

  def make_item_list
    @items = @unit ? @unit.entity.all_skills : []
  end

  def change_palette_color_by_item(merio, item)
    if item.nil?
      merio.change_main_palette('black')
    else
      merio.change_main_palette("element#{item.element_id}")
    end
  end

  ##
  # draw_item(Integer index)
  def draw_item(index)
    ### shorthand
    bmp = contents
    ### rect
    rect = item_rect(index)
    rect_content = rect.contract(anchor: 5, amount: Metric.contract / 2)
    rect_icon = rect_content.dup
    rect_icon.width = rect_icon.height = 24
    rect_icon.align_to!(anchor: 46, surface: rect_content)
    rect_name = rect_content.dup
    rect_name.y = rect_icon.y2
    rect_name.height = 16

    rect_gauge = Rect.new(rect_content.x, rect_content.y2 - 6, rect_content.width, 6)
    ###
    item = items[index]
    ###
    item_name = item ? item.name : ""
    item_r = item ? entity.get_magiclearn_r(item.id) : 0.0
    item_eq = item ? entity.equipped_skill?(item.id) : false
    ### drawing
    artist do |art|
      art.merio.snapshot do |mer|
        # preparation
        if item_r >= 1.0
          mer.change_main_palette("element#{item.element_id}_#{item_eq ? "enb" : "dis"}")
        else
          mer.change_main_palette(item_eq ? 'default' : 'clay')
        end
        mer.txt_palette = mer.main_palette.text_palette_suggestion
        mer.draw_light_rect(rect)
        ## draw_icon
        # this sections draws the small rectangle behind the icon
        mer.snapshot do |sub_mer|
          change_palette_color_by_item(sub_mer, item)
          sub_mer.fill_type = :blend_round
          sub_mer.draw_dark_rect(rect_icon)
        end
        if item
          art.draw_item_icon(item, rect_icon.x, rect_icon.y)
          mer.font_config(:dark, :micro, :enb)
          art.draw_text(rect_name, item_name, 0)
          DrawExt.snapshot do |dext|
            art.draw_gauge_ext(rect: rect_gauge, rate: item_r,
                               colors: DrawExt.quick_bar_colors(Palette["element#{item.element_id}"]))
          end
        end
      end # merio
    end # artist
  end

end

module TestCore::Scene
  class Merio_SkillList_Style3 < Scene::TestBase

    def start
      super
      ### constants
      @unit_index = 0
      @unit = $game.party.members[@unit_index]
      $game.actors.each do |actor| next unless actor
        actor.entity.title = actor.entity.class.name
      end
      #$game.actors[1].entity.title = "Fighter"
      #$game.actors[2].entity.title = "Witch"
      #$game.actors[3].entity.title = "Squiress"
      $data_skills.select { |s| s && !s.name.empty? }.each do |skill|
        $game.party.members.each do |gunit|
        #if skill.tags.include?("elemental")
          gunit.entity.learn_skill(skill.id)
          #unit.entity.magiclearn_learn_skill(skill.id)
          gunit.entity.change_magiclearn(skill.id, rand(101))#100)
        end
        #end
      end
      ### Allocation
      ## Status
      @status_shell = Hazel::Shell::MerioStatusSmall.new(0, 0)
      @status_shell.align_to!(anchor: 8, surface: canvas)
      s = @status_shell.to_rect.contract(anchor: 46, amount: Metric.contract)
      ## Navigation
      @navi_icons = [Sprite::MenuIcon.new(nil, 5),
                     Sprite::MenuIcon.new(nil, 6)]
      @navi_icons[0].align_to!(anchor: 4, surface: s)
      @navi_icons[1].align_to!(anchor: 6, surface: s)
      ##
      r = menu_canvas.dup
      r.width  -= Metric.ui_element
      r.height -= Metric.ui_element * 2
      @skill_list_shell = Hazel::Shell::MerioSkillList_Style3.new(r)
      @skill_list_shell.help_window = help_window
      @skill_list_shell.set_unit(@unit)
      @skill_list_shell.activate
      @skill_list_shell.select(0)
      @skill_list_shell.y = @status_shell.y2
      ##
      w, h = @skill_list_shell.width, Metric.ui_element
      @scrollbar_horz = MerioScrollBar.new(nil, w, h, 1)
      @scrollbar_horz.x = @skill_list_shell.x
      @scrollbar_horz.y = @skill_list_shell.y2
      w, h = Metric.ui_element, @skill_list_shell.height
      @scrollbar_vert = MerioScrollBar.new(nil, w, h, 0)
      @scrollbar_vert.x = @skill_list_shell.x2
      @scrollbar_vert.y = @skill_list_shell.y

      @spacer = Sprite::MerioSpacer.new(nil, Metric.ui_element, Metric.ui_element)
      @spacer.x = @scrollbar_horz.x2
      @spacer.y = @scrollbar_vert.y2

      old_row_index = nil
      @skill_list_shell.add_callback(:index=) do |win, *_, &_|
        @scrollbar_horz.set_index_max(win.col_max)
        @scrollbar_horz.set_index(win.col_index)
        @scrollbar_vert.set_index_max(win.row_max)
        @scrollbar_vert.set_index(win.row_index)
      end

      ### z_order
      @skill_list_shell.z = 3
      @scrollbar_horz.z = 0
      @scrollbar_vert.z = 0
      @status_shell.z = 0
      @navi_icons.each_with_object(4, &:z=)
      ### refresh
      @skill_list_shell.index = 0
      ### functions
      change_unit_index = lambda do |new_index|
        @unit_index = new_index % $game.party.members.size
        @unit = $game.party.members[@unit_index]
        @skill_list_shell.set_unit(@unit)
        @status_shell.set_unit(@unit)
        @skill_list_shell.activate
      end

      flash_tone = Palette['droid_blue'].to_tone
      flast_tone_duration = 15

      prev_unit = lambda do
        @navi_icons[0].tone_flash(flash_tone, flast_tone_duration)
        change_unit_index.(@unit_index.pred)
      end
      next_unit = lambda do
        @navi_icons[1].tone_flash(flash_tone, flast_tone_duration)
        change_unit_index.(@unit_index.next)
      end
      @skill_list_shell.set_handler(:pageup, prev_unit)
      @skill_list_shell.set_handler(:pagedown, next_unit)
      change_unit_index.(0)
      ### Main-Loop
      window_manager.add(@status_shell)
      window_manager.add(@skill_list_shell)
      window_manager.add(@scrollbar_horz)
      window_manager.add(@scrollbar_vert)
      window_manager.add(@spacer)
      @navi_icons.each { |i| window_manager.add(i) }
    end

    def title_text
      "Merio : Skill List [Style 3]"
    end

  end
end