#
# EDOS/core/test_core/test-merio_title.rb
#   dc 19/06/2013
#   dm 19/06/2013
module TestCore

  def self.test_merio_title
    #bgm = RPG::BGM.new("Kii(TutorialVer)", 100, 100)
    ### functions
    half_slice_bmp_horz = ->(bmp) do
      h1 = bmp.height / 2
      h2 = bmp.height - h1
      bmp1 = Bitmap.new(bmp.width, h1)
      bmp2 = Bitmap.new(bmp.width, h2)
      bmp1.blt(0, 0, bmp, Rect.new(0, 0, bmp.width, h1))
      bmp2.blt(0, 0, bmp, Rect.new(0, h1, bmp.width, h2))

      return bmp1, bmp2
    end
    ### variables
    ticks = 0
    state = :null
    ### canvas
    canvas_abs = Screen.content
    canvas = canvas_abs.contract(anchor: 5, amount: Metric.ui_element_sml)
    ### font_config
    copyright  = DrawExt::Merio.new_font(:oqlight, :default, :enb)
    pressstart = DrawExt::Merio.new_font(:light, :giga, :enb)

    #copyright_bmp = Bitmap.render_text(copyright, "© Turtle Factory 2013. All rights reserved.")
    copyright_bmp = Bitmap.render_text(copyright, "Turtle Factory 2013. All rights reserved.")
    pressstart_bmp = Bitmap.render_text(pressstart, "Press Start")
    press1_bmp, press2_bmp = half_slice_bmp_horz.(pressstart_bmp)

    background_sp = Sprite::MerioBackground.new(nil, canvas_abs.width, canvas_abs.height)
    copyright_sp  = Sprite.new(nil)
    pressstart_sp = Sprite.new(nil)
    press1_sp = Sprite.new(nil)
    press2_sp = Sprite.new(nil)

    menu_command = Shell::MerioTitleCommand.new(0, 0)

    copyright_sp.bitmap  = copyright_bmp

    pressstart_sp.bitmap = pressstart_bmp
    press1_sp.bitmap = press1_bmp
    press2_sp.bitmap = press2_bmp

    ### position_setup 1
    background_sp.x = canvas_abs.x
    background_sp.y = canvas_abs.y
    pressstart_sp.cy = canvas.cy
    pressstart_sp.cx = canvas.cx
    press1_sp.x = pressstart_sp.x
    press1_sp.y = pressstart_sp.y
    press2_sp.x = press1_sp.x
    press2_sp.y = press1_sp.y2

    press1_sp.visible = false
    press2_sp.visible = false

    menu_command.hide

    title = MerioTitleGraphic.new(nil)
    title.align_to!(anchor: 8, surface: canvas)
    copyright_sp.align_to!(anchor: 2, surface: canvas)

    ### position_setup 2
    menu_command.cx = canvas.cx
    menu_command.y = title.y2

    #bgm.play

    fx_time = 120

    fader1 = EDOS::FX::Fade.new
    fader2 = EDOS::FX::Fade.new
    fader1.time = fader2.time = fx_time
    fader1.sp = press1_sp
    fader2.sp = press2_sp

    half_slicer = EDOS::FX::HalfSlice.new
    half_slicer.time = fx_time
    half_slicer.sp1 = press1_sp
    half_slicer.sp2 = press2_sp
    half_slicer.slice_size = press1_bmp.width / 4

    position_press = ->() do
      press1_sp.x = pressstart_sp.x
      press1_sp.y = pressstart_sp.y
      press2_sp.x = press1_sp.x
      press2_sp.y = press1_sp.y2
    end

    start_press_fx = ->() do
      press1_sp.visible = true
      press2_sp.visible = true
      pressstart_sp.visible = false
      fader1.run
      fader2.run
      half_slicer.run
    end

    forward_press_fx = ->() do
      p "forward"
      fader1.fade_type = fader2.fade_type = :out
      half_slicer.sp1 = press1_sp
      half_slicer.sp2 = press2_sp

      position_press.()
      state = :to_menu
      start_press_fx.()
    end

    reverse_press_fx = ->() do
      p "reverse"
      fader1.fade_type = fader2.fade_type = :in
      half_slicer.sp1 = press2_sp
      half_slicer.sp2 = press1_sp

      position_press.()

      press1_sp.x -= half_slicer.slice_size
      press2_sp.x += half_slicer.slice_size
      state = :to_press
      start_press_fx.()
    end

    update_press_fx = ->() do
      fader1.update
      fader2.update
      half_slicer.update
      !(fader1.done? && fader2.done? && half_slicer.done?)
    end

    _test_loop_(TEST_TIME_LONG) do
      if Input.trigger?(:C) #&& !menu_activate && !menu_active
        forward_press_fx.()
      elsif Input.trigger?(:B)
        reverse_press_fx.()
      end

      if state == :to_menu || state == :to_press
        unless update_press_fx.()
          if state == :to_menu
            state = :menu
            menu_command.show.activate.select(0)
            press1_sp.hide
            press2_sp.hide
          elsif state == :to_press
            state = :press
            pressstart_sp.show
            menu_command.hide.deactivate
            press1_sp.hide
            press2_sp.hide
          end
        end
      elsif state == :menu
        menu_command.update
      else
        pressstart_sp.opacity = (ticks * 4).reflect(0, 256)
      end

      title.update
      ticks += 1
    end

    title.dispose
    menu_command.dispose
    background_sp.dispose
    copyright_sp.dispose_all
    pressstart_sp.dispose_all
    press1_sp.dispose_all
    press2_sp.dispose_all

    return TEST_PASSED
  end

end