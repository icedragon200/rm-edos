module TestCore::Scene
  class MousePos < Scene::TestBase

    ##
    # TODO
    def start
      background_sp = spr(nil)
      background_sp.bitmap = Bitmap.new(gwidth, gheight)
      background_sp.bitmap.fill_rect(background_sp.bitmap.rect,
                                     Color.rgb24(0xFFFFFF))
      sp = spr(nil)
      sp.bitmap = Bitmap.new("Graphics/Windows/window_craft.png")

      mouse_tip = Sprite.new(nil)
      mouse_tip.bitmap = Bitmap.new(192, 24)
      mouse_tip.bitmap.font.set_style('simple_black')

      _test_loop_(TEST_TIME_LONG) do
        mouse_tip.x = Mouse.x
        mouse_tip.y = Mouse.y
        mouse_tip.bitmap.clear
        if Mouse.x.between?(0, sp.bitmap.width-1) &&
           Mouse.y.between?(0, sp.bitmap.height-1)
          mouse_tip.bitmap.draw_text(mouse_tip.bitmap.rect,
                                     sp.bitmap.get_pixel(Mouse.x,
                                                         Mouse.y).to_a.inspect)
        end
      end

      mouse_tip.dispose_all
      sp.dispose_all
      background_sp.dispose_all

      return TEST_PASSED
    end

  end
end