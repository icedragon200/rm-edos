module TestCore

  def self.test_new_character
    canvas = Screen.content
    chipmap = SRRI::Chipmap.new
    tilesize = chipmap.tilesize = 16
    chipmap.tile_columns = 2

    scale = tilesize / 8
    s = StarRuby::Texture.load('meh/ninja.png')
    t = StarRuby::Texture.new(s.width * scale, s.height * scale)
    t.render_texture(s, 0, 0, scale_x: scale, scale_y: scale)
    t.save('meh/ninja-big.png')
    t.dispose; s.dispose

    sp = spr(nil)
    sp.z = 1
    sp.bitmap = Cache.normal_bitmap('meh/ninja-big.png')
    sp.src_rect.set(0, 0, tilesize, tilesize)
    sp.ox = tilesize / 2
    sp.oy = tilesize
    anim = [0, 1, 2, 1]

    char = v2i(0, 0)
    vischar = v2f(0, 0)
    camera = v2i(0, 0)
    unit = $game.actors[1]
    gg = Gauge.new(sp, unit) { |b| b.entity.hp_rate }

    # prepare tile_bitmap
    b = chipmap.tile_bitmap = Bitmap.new(tilesize * 2, tilesize * 2)
    r = b.rect / 2
    r.x = r.width
    DrawExt.draw_gauge_ext_sp4(b, r, 1.0, DrawExt.quick_bar_colors(Palette['gray17']))
    r.x = 0
    r.y = r.height
    DrawExt.draw_gauge_ext_sp4(b, r, 1.0, DrawExt.quick_bar_colors(Palette['sys1_green']))

    # set map_data
    d = chipmap.map_data = Table.new(20, 20)
    r = d.rect
    d.fill_rect(r, 0)
    d.fill_rect(r.contract(anchor: 5, amount: 1), 1)
    d.bucket_fill2(2, 2, 2)

    # finally refresh the chipmap
    hspan = tilesize * 7 #([gwidth / tilesize, d.xsize].min) * tilesize
    vspan = tilesize * 7 #([gheight / tilesize, d.ysize].min) * tilesize
    chipmap.refresh.viewrect.set(0, 0, hspan, vspan)
    print_table(chipmap.map_data)

    ticks = 0

    shell = Shell::SNHud.new(0, 0, gwidth, 70)
    shell.battler = unit.entity
    shell.align_to!(anchor: 2, surface: canvas)
    shell.refresh
    shell.deactivate

    turn = 0
    can_act = 0
    next_turn = -> do
      can_act = 30
      turn += 1
    end

    _test_loop_(TEST_TIME_LONG) do
      shell.update
      if Input.trigger?(:C)
        unit.entity.hp -= 1
      end
      if can_act == 0
        last_pos = [char.x, char.y]
        if Input.repeat?(:LEFT)
          char.x -= 1
        elsif Input.repeat?(:RIGHT)
          char.x += 1
        elsif Input.repeat?(:UP)
          char.y -= 1
        elsif Input.repeat?(:DOWN)
          char.y += 1
        end
        next_turn.() if last_pos != [char.x, char.y]
      end
      if char.x < 0
        char.x = 0
      elsif char.x >= d.xsize
        char.x = d.xsize - 1
      end
      if char.y < 0
        char.y = 0
      elsif char.y >= d.ysize
        char.y = d.ysize - 1
      end

      move_rate = 1.0 / 10

      if char.x > vischar.x
        vischar.x = [vischar.x + move_rate, char.x].min
      elsif char.x < vischar.x
        vischar.x = [vischar.x - move_rate, char.x].max
      end

      if char.y > vischar.y
        vischar.y = [vischar.y + move_rate, char.y].min
      elsif char.y < vischar.y
        vischar.y = [vischar.y - move_rate, char.y].max
      end

      chipmap.update
      gw = (hspan / tilesize) * tilesize
      gh = (vspan / tilesize) * tilesize
      hw = ((hspan / tilesize) / 2) * tilesize
      hh = ((vspan / tilesize) / 2) * tilesize
      camera.x = [[vischar.x * tilesize - hw, 0].max, chipmap.width_abs - gw].min.to_i
      camera.y = [[vischar.y * tilesize - hh, 0].max, chipmap.height_abs - gh].min.to_i
      chipmap.ox = camera.x
      chipmap.oy = camera.y
      sp.x = sp.ox + (vischar.x * tilesize) - camera.x
      sp.y = sp.oy + (vischar.y * tilesize) - camera.y
      sp.src_rect.x = tilesize * anim[(ticks / tilesize) % anim.size]
      gg.update
      ticks += 1
      can_act -= 1 if can_act > 0
    end

    chipmap.tile_bitmap.dispose
    chipmap.dispose
    shell.dispose
    sp.dispose_all

    return TEST_PASSED
  end

end