# Notetag Test

module TestCore

  def self.test_notetag_parsers
    return TEST_DISABLED

    puts Notetag.parse_dtstr("int:100").inspect
    puts Notetag.parse_dtstr("a-str:hello,world").inspect
    puts Notetag.parse_dtstr("bool:yes").inspect
    puts Notebox.parse_dtstr("value: x",:str, false)
    puts Notebox.parse_dtstr("key: 2",:nil, false)[1][0] + 3

    note = %Q{
    <test>
    Stuff and shit
    </test>
    <test2>
    Stuff and stufffffff
    </test2>
    }
    puts IEI::Core.get_note_folders(IEI::Core.mk_notefolder_tags("test"), note)
  end

end