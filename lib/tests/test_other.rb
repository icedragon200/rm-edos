    #test_sprite_oxy
    #test_surface_anchor

=begin
    count = 0 #Graphics.height / 32
    sprites = count.times.map do |i|
      sp = Sprite.new()
      sp.bitmap = Bitmap.new("./graphics/system/ball.png")
      sp.ox = sp.bitmap.width / 2
      sp.oy = sp.bitmap.height / 2
      x, y = 0, 16 + 32 * i
      tween = MACL::Tween::Osc.new(
        16, Graphics.width - 16,
        [:sine_in, :sine_out], MACL::Tween.frame_to_sec(80 + 10 * i / count.to_f)
      )
      #sp.bitmap.blur
      #sp.opacity = 128
      #sp.color.set(244, 128, 128, 90)
      #sp.zoom_x = sp.zoom_y = 2.0
      [sp, [x, y], tween]
    end

    viewport = Viewport.new(32, 32, Graphics.width - 64, Graphics.height - 64)
    plane = Plane.new(viewport)
    plane.bitmap = Bitmap.new("graphics/system/menubackground2.png")
    plane.z = -1
    #Database.load
    item = RPG::Item.new()
    item.initialize_add
    item.name = "SomeItem"
    item.icon.index = 128

    #window = Window::ChestItems.new(16, 32, [[item, 12]], "Chest Contained")#Window::Base.new(0, 0, 128, 128)
    window = Window::Base.new(16, 16, 32, 32)
    #window = Window::Help.new(0, 0, Graphics.width, 64)
    #window = Window::ColorSelect.new(0, 0)

    #window.x = (Graphics.width - window.width) / 2
    #window.y = (Graphics.height - window.height) / 2
    #p window.instance_variable_get("@addon_bin").active_addons
    window.z = 12

    #window.set_text("Awes")
    #window.contents.fill_rect(0, 0, 128, 16, Color.new(0, 0, 0, 255))
    #window.contents.draw_text(0, 0, 128, 32, "Hello")

    #blur_sprite = Sprite.new()
    #blur_sprite.bitmap = Bitmap.new("Graphics/System/Ball.png") #Bitmap.new(32, 32)
    #blur_sprite.bitmap.fill_rect(2, 2, 28, 28, Color.new(255, 255, 255, 255))
    #blur_sprite.bitmap.blur

    #sp = Sprite.new()
    #sp.bitmap = Bitmap.new("./graphics/system/iconset.png")

    sp = Sprite.new()
    sp.bitmap = Bitmap.new(Graphics.width, 32)
    sp.bitmap.gradient_fill_rect(
      sp.bitmap.rect, Color.new(64, 64, 64), Color.new(255, 255, 255), true)
    loop do
      plane.ox += 1
      sprites.each do |(sp, (x, y), tween)|
        tween.update
        x = tween.value
        sp.x = x.round
        sp.y = y.round
        sp.update
      end

      Graphics.update
      Input.update
      window.update
    end
=end
    #sp = Sprite.new

    #sp.bitmap = CacheExt.bitmap("win_buttons")
    #loop do
      #sp.update
      #Graphics.update
      #Input.update
    #end