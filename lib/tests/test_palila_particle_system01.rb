module TestCore

  def self.test_palila_particle_system_01
    canvas = Screen.content
    particle_sys = Palila::ParticleSystem.new
    vec = particle_sys.location
    vec.x = Graphics.rect.cx
    vec.y = 32

    title_sp = Sprite::MerioTitle.new(nil, "Palila Particles", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    sp = Sprite.new
    bmp = sp.bitmap = Bitmap.new(Graphics.width, Graphics.height)

    #color_list = [
    #  'white', 'sys_blue2', 'sys_blue1', 'sys_red1', 'gray16'
    #].map(&Palette.method(:[]))

    col_count = 4
    col_countf = col_count.to_f
    col  = Palette['droid_blue']#Color.new(240, 246, 182)#Palette['sys_blue1'].lighten(0.3)
    col2 = Palette['black']#Palette['droid_blue_light']#Color.new(194, 127, 49)#Palette['sys_red1'].lighten(0.3)
    color_list = col_count.times.map do |i|
      col.lerp(col2, i / col_countf)
    end

    particle_size = Rect.new(0, 0, 1, 1)

    particle_count = 8 #0xFF / 3

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update

      if(vec.x != Mouse.x || vec.y != Mouse.y)
        vec.x = Mouse.x
        vec.y = Mouse.y
        particle_count.times do
          particle_sys.add_particle(Particle::MenuSpark)
        end
      end

      if Mouse.left_press?
        (particle_count * 4).times do
          particle_sys.add_particle(Particle::MenuSpark)
        end
      end
      #elsif Mouse.right_click?
      #  puts "%s - %s: %s - %s" % [vec.x, vec.y, Mouse.x, Mouse.y]
      #end
      #if Graphics.frame_count % 8 == 0
      #  2.times do particle_sys.add_particle(Particle::MenuSpark) end
      #end

      bmp.clear
      particle_sys.update_particles do |particle|
        particle_size.x = particle.x
        particle_size.y = particle.y

        r = particle.life / particle.lifespan.to_f
        col = color_list[((1 - r) * color_list.size).to_i]

        col.alpha = r * 255
        bmp.fill_rect(particle_size, col)
      end
      #bmp.blur
    end

    bmp.dispose
    sp.dispose
    title_sp.dispose

    return TEST_PASSED
  end

end