module TestCore

  def self.test_palette

    sp = Sprite.new
    sp.bitmap = Cache.system("palette")

    _test_loop_(TEST_TIME_SHORT) do
      sp.update
    end

    sp.dispose

    return TEST_PASSED
  end

end
