module TestCore

  def self.test_path_tracking
    return TEST_DISABLED
    mark_null   = 00000000
    mark_tl     = 00000001 # top left
    mark_tr     = 00000010 # top right
    mark_bl     = 00000100 # bottom left
    mark_br     = 00001000 # bottom right
    mark_top    = mark_tl | mark_tr
    mark_bottom = mark_bl | mark_br
    mark_left   = mark_tl | mark_bl
    mark_right  = mark_tr | mark_br
    mark_all    = 00001111
    mark_corner = 00010000 # is a corner
    mark_line   = 00100000 # is a linear path

    node_index = {
      mark_null             => 6, # unused
      mark_corner | mark_tl => 0,
      mark_corner | mark_tr => 1,
      mark_corner | mark_bl => 4,
      mark_corner | mark_br => 5,
      mark_line | 00000000  => 3,
      mark_line | 00000001  => 2
    }

    node_rect = {}
    node_index.each_pair do |k, v|
      node_rect[k] = Rect.new((v % 4) * 32, (v / 4) * 32, 32, 32)
    end

    node_bmp = Cache.system('range-path/orange.png')


    pos = MACL::Pos3.new

    sp = spr(nil)
    path_bmp = sp.bitmap = Bitmap.new(gwidth, gheight)

    path = []

    determine_node = ->(n0, n1, n2) do
      i = mark_null
      # new node is to the right of the old node
      if    n1.x > n2.x
        i |= 00101
      # new node is to the left of the old node
      elsif n1.x < n2.x
        i |= 01010
      end
      # new node is to the bottom of the old node
      if    n1.y > n2.y
        i |= 01100
      # new node is to the top of the old node
      elsif n1.y < n2.y
        i |= 00011
      end
      path_bmp.blt(n1.x * node_size, n1.y * node_size, node_bmp, node_rect[i])
    end

    record_node = -> do
      path.push(pos.dup)
    end

    _test_loop_(TEST_TIME_LONG) do
      Main.update
      if    Input.repeat?(:LEFT)
        pos.move_east(1)
      elsif Input.repeat?(:RIGHT)
        pos.move_west(1)
      elsif Input.repeat?(:UP)
        pos.move_north(1)
      elsif Input.repeat?(:DOWN)
        pos.move_south(1)
      end

      if Input.trigger?(:B)
        path.clear
        path_bmp.clear
      end
    end

    sp.dispose_all

    return TEST_PASSED
  end

end
