module TestCore

  def self.test_polygon_01
    canvas = Screen.content
    polygon = MACL::Geometry::Circle.new(0,0,64)#Polygon.new( 32, 0, 0, 64, 64 )
    polygon.align_to!(anchor: 5)

    title_sp = Sprite::MerioTitle.new(nil, "Polygon Test 01", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)

    sp = Sprite.new(nil)
    sp.bitmap = Cache.system("ball")

    i = 0
    _test_loop_(TEST_TIME_XLONG) do
      sp.x, sp.y = *polygon.calc_point_from_angle(i)
      i = i.succ.modulo(360)
    end

    sp.dispose
    title_sp.dispose

    return TEST_PASSED
  end

end

# PolygonTest
__END__
#=begin

#=end
=begin
Mouse.init
s = Sprite.new(nil)
s.bitmap = Cache.system("CommandWheel_Background")
s.salign!(5)
polygon = Oval.new(s.cx,s.cy,s.width/2,s.height/2)
ls = Sprite.new(nil)
ls.bitmap = Bitmap.new(Graphics.width, 32)
@last_text = nil
balla = Sprite.new()
balla.bitmap = Cache.system("BallArrow")
balla.x, balla.y = s.cx, s.cy
balla.ox = 16
balla.oy = 16
loop do
  Main.update
  s.update
  s.opacity = polygon.in_circ_area?(Mouse.x, Mouse.y) ? 255 : 128
  dx, dy = Vector.xy_dist_from(polygon.cx, polygon.cy, Mouse.x, Mouse.y)
  t = "dist: #{dx} || #{dy} pole: #{dx.unary} || #{dy.unary}"
  balla.angle = polygon.calc_angle(Mouse.x, Mouse.y) + 90
  if @last_text != t
    ls.bitmap.clear
    ls.bitmap.draw_text(ls.to_rect, t, 1)
    @last_text = t
  end
end
=end
