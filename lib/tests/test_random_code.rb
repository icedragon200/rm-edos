module TestCore::Scene
  class RandomCode < Scene::TestBase

    def start
      super
      @bmp = Cache.character('$chika')
      @palette = @bmp.palette
      @sprites = Array.new(@palette.size) { Sprite.new(viewport) }
      @palette.each_with_index do |col, i|
        sp = @sprites[i]
        bmp = sp.bitmap = Bitmap.new(24, 24)
        bmp.merio.draw_fill_rect(bmp.rect, col)
      end

      MACL::Surface::Tool.tile_surfaces(sprites, @canvas)
    end

    def terminate
      @sprites.each(&:dispose_all)
      @sprites = nil
      super
    end

    def update
      @sprites.each(&:update)
      super
    end

  end
end