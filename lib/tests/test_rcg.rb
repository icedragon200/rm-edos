module TestCore

  def self.test_rcg
    return TEST_DISABLED

    bmp = RCG::Bitmap.new(RCG::Reader.read_rcgt(File.read("rcg/example.rcgt")))
    #Bitmap.new(256,256)
    #circle = Circle.new(128,128,98)
    #bmp.draw_circle(circle,Palette['sys_orange1'],true)
    spr = Plane.new()
    spr.bitmap = bmp

    #spr.salign!(5)

    _test_loop_(TEST_TIME_SHORT) do
      spr.update
    end

    spr.dispose

    return TEST_PASSED
  end

end

