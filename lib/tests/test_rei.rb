# REI - Basic Tests
class Sprite_RangeCircle < Sprite

  def initialize(viewport)
    super(viewport)
    self.bitmap = Cache.system('RangeCircle')
    self.ox = self.bitmap.width / 2.0
    self.oy = self.bitmap.height / 2.0
    self.color.set 0,0,0,198
    self.tone.set 0,255,128
  end

  def range=(n)
    self.zoom_x = self.zoom_y = n / self.width.to_f
  end

end

module TestMap

end

class << TestMap

  def round_x(x)
    x % (Graphics.width / 32)
  end

  def round_y(y)
    y % (Graphics.height / 32)
  end

end

class Scene::CharTest < Scene::Base

  def start
    super
    @character = REI::Container::Character.new
    vis = @character.visual
    #vis.change_character 'Actor4', 0
    @range_sprite = Sprite_RangeCircle.new @viewport
    @range_sprite.z = -1
    @range_sprite.range = 96
    @sprite = REI::Sprite::Character.new @viewport,@character
    @character.x = Graphics.width / 32 / 2
    @character.y = Graphics.height / 32 / 2
    @character.visual.refresh :pos
  end

  def update_basic
    super
    @character.update
    @sprite.update
    @range_sprite.x = @sprite.x
    @range_sprite.y = @sprite.y
  end

  def update
    super
    @character.battler.hp = @character.battler.mhp if Input.trigger? :C
    @character.battler.hp -= 1 if @character.battler.hp > 0 #if Graphics.frame_count % 3 == 0
    @character.battler.dec_wt 12
    if @character.battler.wt_done?
      unless @jumped
        @jumped = true
        #@character.jump
        #pop_quick_text_c 'Ready!' #unless EDoS::Kernel.active_pop?
      end
      if (n=Input.dir4) > 0
        @character.move_straight n
        @character.battler.reset_wt
        @jumped = false
      end unless @character.moving?
    end
  end

end

module TestCore

  def self.test_rei_character
    return TEST_DISABLED # do not do this test
    scene = Scene::CharTest.new
    _test_loop_(TEST_TIME_LONG) do
      scene.start
      scene.post_start
      scene.update until true == false
      scene.pre_terminate
      scene.terminate
    end
  end

end
