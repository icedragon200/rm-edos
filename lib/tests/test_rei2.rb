module TestCore

  def self.test_rei2
    return TEST_DISABLED if REI::VERSION < "2.0.0"
    ###
    canvas = Screen.content
    ##
    viewport = Viewport.new(canvas)
    ###
    camera = StarRuby::Vector3F.new(0, 0, 0)
    ###
    entity_cursor = REI::Entity.new do |ent|
      ent.add_component(:position)
      ent.add_component(:position_ease).timemax = 7
      ent.add_component(:size)
      ent.add_component(:motion)
      ent.add_component(:character)
      ent.add_component(:event_server)
    end
    cursor = REI::Unit.new(entity_cursor, entity_cursor.comp(:character))
    unit = REI.create_unit_by_type(:actor)
    entity = unit.entity
    entity.comp(:name).setup_name(:entity, "Player").setup_title(:title, "Knight")
    evs = entity.comp(:event_server)
    p entity.components.keys
    ###
    map = MapManager.load_map(0)
    layers = map.layers
    layer_maps = layers.size.times.map do |z|
      SRRI::Chipmap.new do |tm|
        layer = layers[z]
        layer_data = layer.data
        data = Table.new(map.width, map.height)
        data.ysize.times do |y|
          data.xsize.times do |x|
            data[x, y] = layer_data[x + y * data.xsize] - 1
          end
        end
        tm.map_data = data
        tm.tile_columns = 1024 / 32
        tm.tile_bitmap = Cache.atlas("terrain_atlas")
        tm.viewport = viewport
      end
    end
    ###
    sp_char = Sprite.new(viewport)
    sp_char.bitmap = Bitmap.new(32, 32)
    sp_char.bitmap.draw_gauge_ext
    sp_cursor = Sprite.new(viewport)
    sp_cursor.bitmap = Bitmap.new(36, 36)
    sp_cursor.bitmap.draw_gauge_ext(colors: DrawExt::BLUE_BAR_COLORS)
    sp_cursor.bitmap.clear_rect(sp_cursor.bitmap.rect.contract(anchor: 5, amount: 2))
    sp_cursor.ox = 2
    sp_cursor.oy = 2
    ###
    shell_status = Shell::MerioStatus.new(0, 0, canvas.width, 64)
    shell_status.align_to!(anchor: 2, surface: canvas)
    shell_status.set_unit(unit)
    ###
    camera_swease = Sweasel.new(value: Vector3(0, 0, 0), time: 20, easer: :sine_out)
    camera_needs_update = false
    ### used to clamp the camera to the edge of the screen
    sw = canvas.width / 32
    sh = canvas.height / 32
    hsw = canvas.width / 32 / 2
    hsh = canvas.height / 32 / 2
    surface = Rect.new(0, 0, map.width - sw, map.height - sh)
    ### main-loop
    _test_loop_(TEST_TIME_LONG) do
      #p evs
      pos = cursor.entity.comp(:position_ease)
      if !pos.busy?
        if (dir = Input.dir8) > 0
          cursor.entity.comp(:motion).move_straight_dir(dir, 1)
          camera_needs_update = true
        end
      end
      cursor.update
      entity.update
      if camera_needs_update
        vec = Vector3(*pos)
        vec.x -= hsw
        vec.y -= hsh
        MACL::Surface::Tool.surface_vec_clamp(surface, vec)
        camera_swease.target_value = vec * 32
        camera_needs_update = false
      end
      camera_swease.update
      camera.x, camera.y, = *camera_swease.value
      viewport.ox = camera.x
      viewport.oy = camera.y
      sp_cursor.x = cursor.character.screen_x
      sp_cursor.y = cursor.character.screen_y
      sp_cursor.z = cursor.character.screen_z
      sp_char.x = unit.character.screen_x
      sp_char.y = unit.character.screen_y
      sp_char.z = unit.character.screen_z
      layer_maps.each do |layer|
        layer.update
      end
    end
    ### free
    layer_maps.each(&:dispose)
    sp_cursor.dispose_all
    sp_char.dispose_all
    shell_status.dispose
    ### result
    return TEST_PASSED
  end

end