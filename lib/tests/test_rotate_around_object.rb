module TestCore

  def self.test_rotate_around_object
    canvas = Screen.content

    title_sp = Sprite::MerioTitle.new(nil, "Orbit Test", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)

    sp_central = Sprite.new(nil)
    sp_central.bitmap = Cache.system('ball')
    sp_central.ox = sp_central.width / 2
    sp_central.oy = sp_central.height / 2

    planets = []
    planet_k = Struct.new(:a, :r1, :r2, :sp)
    add_planet = lambda do
      sp2 = Sprite.new(nil)
      sp2.bitmap = Cache.system('ball')
      sp2.ox = sp2.width / 2
      sp2.oy = sp2.height / 2
      planets << planet_k.new(0, 96, 96, sp2)
    end

    remove_planet = lambda do
      planet = planets.pop
      planet.sp.dispose if planet
    end

    sp_central.align_to!(anchor: 5, surface: canvas)

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      if Input.press?(:t)
        planets.each do |planet|
          planet.r1 += 1
        end
      elsif Input.press?(:g)
        planets.each do |planet|
          planet.r1 -= 1
        end
      elsif Input.press?(:y)
        planets.each do |planet|
          planet.r2 += 1
        end
      elsif Input.press?(:h)
        planets.each do |planet|
          planet.r2 -= 1
        end
      elsif Input.press?(:r)
        planets.each do |planet|
          planet.r2 = planet.r1 = [planet.r2, planet.r1].min
        end
      elsif Input.press?(:UP)
        planets.each do |planet|
          planet.r1 += 1
          planet.r2 += 1
        end
      elsif Input.press?(:DOWN)
        planets.each do |planet|
          planet.r1 -= 1
          planet.r2 -= 1
        end
      end
      if Input.repeat?(:RIGHT)
        add_planet.()
      elsif Input.repeat?(:LEFT)
        remove_planet.()
      end
      if Mouse.left_press?
        sp_central.x = Mouse.x
        sp_central.y = Mouse.y
      elsif Mouse.right_click?
        sp_central.align_to!(anchor: 5, surface: canvas)
      end
      planets.each do |planet|
        rad = planet.a * Math::PI / 180
        planet.sp.x = sp_central.x + Math.cos(rad) * planet.r1
        planet.sp.y = sp_central.y + Math.sin(rad) * planet.r2
        planet.a += 1
        planet.a %= 360
      end
    end

    title_sp.dispose
    sp_central.dispose
    planets.each { |planet| planet.sp.dispose }

    return TEST_PASSED
  end
end
