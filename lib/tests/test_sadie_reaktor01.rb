module TestCore

  def self.test_sadie_reaktor01
    return TEST_DISABLED

    src_reaktor = Sadie::Reaktor.new(1)
    mix_reaktor = Sadie::MixerReaktor.new(1)
    c_reaktor1  = Sadie::CounterReaktor.new(1)
    c_reaktor2  = Sadie::CounterReaktor.new(1)
    c_reaktor3  = Sadie::CounterReaktor.new(1)
    ev_reaktor1 = Sadie::EventReaktor.new(0)
    ev_reaktor2 = Sadie::EventReaktor.new(0)
    ev_reaktor3 = Sadie::EventReaktor.new(0)

    c_reaktor1.counter_max = 15
    c_reaktor1.reset_counter!

    c_reaktor2.counter_max = 10
    c_reaktor2.reset_counter!

    c_reaktor3.counter_max = 5
    c_reaktor3.reset_counter!

    src_reaktor.connect(c_reaktor1, channel: 0)
    c_reaktor1.connect(c_reaktor2, channel: 0)
    c_reaktor1.connect(ev_reaktor1, channel: 0)
    c_reaktor2.connect(c_reaktor3, channel: 0)
    c_reaktor2.connect(ev_reaktor2, channel: 0)
    c_reaktor3.connect(ev_reaktor3, channel: 0)

    sp1 = Sprite.new
    sp2 = Sprite.new
    sp3 = Sprite.new
    bmp = sp1.bitmap = sp2.bitmap = sp3.bitmap = Bitmap.new(32, 32)
    bmp.draw_gauge_ext(bmp.rect, 1.0, DrawExt::GREEN_BAR_COLORS)

    sp2.x = sp1.rect.x2
    sp3.x = sp2.rect.x2

    ev_reaktor1.set_event_handle('i0') do
      sp1.visible = !sp1.visible
    end
    ev_reaktor2.set_event_handle('i0') do
      sp2.visible = !sp2.visible
    end
    ev_reaktor3.set_event_handle('i0') do
      sp3.visible = !sp3.visible
    end

    _test_loop_(TEST_TIME_X2LONG) do
      src_reaktor.emit(0)
    end

    sp1.dispose
    sp2.dispose
    sp3.dispose
    bmp.dispose

    return TEST_PASSED
  end

end
