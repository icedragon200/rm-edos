#
# EDOS/core/test_core/test-sadie_reaktor02.rb
#   dm 22/06/2013
# vr 1.0.0
module TestCore

  def self.test_sadie_reaktor02
    return TEST_DISABLED unless defined?(Sadie) && defined?(Sadie::Reaktor)
    cRetrigger = ProtoTyperz::FrameRetrigger
    cRepeat    = ProtoTyperz::FrameRepeat

    cCallback  = Sadie::Reaktor::Passive
    cCapacitor = Sadie::Reaktor::Capacitor
    cCounter   = Sadie::Reaktor::Counter
    cEmitter   = Sadie::Reaktor::Emitter
    cBusbar    = Sadie::Reaktor::Busbar
    cRelay     = Sadie::Reaktor::Relay
    cFloodgate = Sadie::Reaktor::Floodgate
    cDrain     = Sadie::Reaktor::Drain

    cap = 4
    emitter = cEmitter.new
      emitter.energy.value = 1
    busbar = cBusbar.new
    capacitor = cCapacitor.new
      capacitor.charge_pull_min = 1
      capacitor.charge_pull_max = 1
      capacitor.charge_foot = 0
      capacitor.charge_ceil = cap
    relay = cRelay.new
      relay.coil_trigger_energy = cap
    counter = cCounter.new
    floodgate = cFloodgate.new
      floodgate.flood_trigger_energy = cap
    drain = cDrain.new

    # connections
    emitter.connect(emitter.port(:output), busbar.port(:common))
    busbar.connect(busbar.port(:feed1), capacitor.port(:input))
    busbar.connect(busbar.port(:feed2), relay.port(:common))
    capacitor.connect(capacitor.port(:output), relay.port(:coil_in))
    relay.connect(relay.port(:nc), counter.port(:add))
    relay.connect(relay.port(:no), counter.port(:reset))
    relay.connect(relay.port(:coil_out), floodgate.port(:input))
    floodgate.connect(floodgate.port(:output), drain.port(:input))

    emitter_sp = Sadie::Sprite::Emitter.new(nil, emitter)
      emitter_sp.x = 0
      emitter_sp.y = 32
    relay_sp = Sadie::Sprite::Relay.new(nil, relay)
      relay_sp.x = emitter_sp.x2
      relay_sp.y = emitter_sp.y
    counter_sp = Sadie::Sprite::Counter.new(nil, counter)
      counter_sp.x = relay_sp.x2
      counter_sp.y = relay_sp.y - 16
    floodgate_sp = Sadie::Sprite::Floodgate.new(nil, floodgate)
      floodgate_sp.x = relay_sp.x2
      floodgate_sp.y = relay_sp.y + 16
    drain_sp = Sadie::Sprite::Drain.new(nil, drain)
      drain_sp.x = floodgate_sp.x2
      drain_sp.y = floodgate_sp.y

    retrigger = cRetrigger.new(30, -> { emitter.trigger })

    sp_footer = Sprite::MerioTitle.new(nil, "Sadie's Reaktor")
    sp_footer.align_to!(anchor: 2, surface: Screen.content)

    sprites = [emitter_sp, counter_sp, relay_sp, floodgate_sp, drain_sp]

    _test_loop_(TEST_TIME_LONG) do
      sp_footer.update
      retrigger.update
      sprites.each(&:update)
    end

    sp_footer.dispose
    sprites.each(&:dispose)

    return TEST_PASSED
  end

end
