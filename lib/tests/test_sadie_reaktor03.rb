class SpriteReaktorBase < Sprite

  attr_reader :reaktor

  def initialize(viewport, reaktor)
    super(viewport)
    @reaktor = reaktor
    init_bitmap
    refresh
  end

  def dispose
    dispose_bitmap_safe
    super
  end

  def init_bitmap
    size = Graphics.width / 5
    self.bitmap = Bitmap.new(size, size)
  end

  def refresh
    bmp = self.bitmap
    font = bmp.font
    font.outline = false
    rect = bmp.rect
    rect_name = rect.dup; rect_name.height = Metric.ui_element
    rect.y      += rect_name.height
    rect.height -= rect_name.height
    inputs  = @reaktor.inputs
    outputs = @reaktor.outputs
    inputl  = inputs.size
    outputl = outputs.size
    wi = wo = width / 4
    hi = rect.height / [1, inputl].max
    ho = rect.height / [1, outputl].max
    xi = rect.x         # Input   X-Coord
    xo = rect.x2 - wo   # Output  X-Coord
    yi = rect.y         # Input   Y-Coord
    yo = rect.y         # Outputs Y-Coord

    bmp.clear
    pal = MACL::Book.entries_lookup(Palette.entries, word: 'droid',
                                                     words: ['red', 'green', 'blue', 'orange', 'yellow', 'purple'],
                                                     exword: 'light')
    #pal = MACL::Book.entries_lookup(Palette.entries, word: 'sys', words: ['1'])
    #pal = MACL::Book.entries_lookup(Palette.entries, word: 'merio')

    color  = pal.values.sample
    colorn = color.blend.lighten(0.1)
    colors1 = DrawExt.quick_bar_colors(color)
    colors2 = DrawExt.quick_bar_colors(colorn)
    DrawExt.draw_gauge_ext_sp4(bmp, rect, 1.0, colors1)
    DrawExt.draw_gauge_ext_sp5(bmp, rect_name, 1.0, colors2)

    bmp.font.snapshot do
      nm = @reaktor.name
      font.size = 16
      rn = rect_name; rn.height /= 2
      bmp.draw_text(rn, nm, 1)
    end

    con = Metric.contract / 2
    inputs.each_with_index do |port, i|
      r = Rect.new(xi, yi + hi * i, wi, hi)
      lr = r.clone; lr.x += lr.width
      rr = r.contract!(anchor: 5, amount: con)
      bmp.blend_fill_rect(rr, Palette['droid_dark_ui_enb'])
      bmp.font.snapshot do
        bmp.font.size = 16
        bmp.draw_text(r, i, MACL::Surface::ALIGN_CENTER)
        bmp.font.size = 14
        bmp.draw_text(lr, port.name, MACL::Surface::ALIGN_LEFT)
      end
    end

    outputs.each_with_index do |port, i|
      r = Rect.new(xo, yo + ho * i, wo, ho)
      lr = r.clone; lr.x -= lr.width
      rr = r.contract!(anchor: 5, amount: con)
      bmp.blend_fill_rect(rr, Palette['droid_dark_ui_enb'])
      bmp.font.snapshot do
        bmp.font.size = 16
        bmp.draw_text(r, i, MACL::Surface::ALIGN_CENTER)
        bmp.font.size = 14
        bmp.draw_text(lr, port.name, MACL::Surface::ALIGN_RIGHT)
      end
    end
  end

end

module TestCore

  def self.test_sadie_reaktor03
    return TEST_DISABLED unless defined?(Sadie) && defined?(Sadie::Reaktor)
    sprs = []
    Sadie::Reaktor.reaktors.each do |reaktor|
      sprs.push(SpriteReaktorBase.new(nil, reaktor.new))
    end

    MACL::Surface::Tool.tile_surfaces(sprs)

    _test_loop_(TEST_TIME_LONG) do
      sprs.each(&:update)
    end

    sprs.each(&:dispose)

    return TEST_PASSED
  end

end
