#
# EDOS/core/test_core/test-sadie_reaktor04.rb
#   by IceDragon
#   dc 31/05/2013
#   dm 31/05/2013
module TestCore

  def self.test_sadie_reaktor04
    return TEST_DISABLED unless defined?(Sadie) && defined?(Sadie::Reaktor)
    return TEST_DISABLED # regardless
    require 'sadie/easy.rb'
    Sadie::EasyReaktor.easy do |er|
      emitter = er.emt
      emitter.connect(emitter.psc("output"), 0)
    end
    ## TODO
    # Test all Sadie Reaktors
    #  Finish the ReaktorNetwork
    #  Create Reaktor Event System
    #  Create Reaktor Piping system
    return TEST_PASSED
  end

end
