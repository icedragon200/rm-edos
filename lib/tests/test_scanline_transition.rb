# Test2
__END__
Mouse.init

def scanline_transition(bmp, *bmps)
  i = 0
  r = nil
  ind = 0
  cbmp = bmps[ind]
  loop do
    r = Rect.new(0, i, bmp.width, 1)
    bmp.clear_rect(r)
    bmp.blt(0, i, cbmp, r)
    i += 1
    Fiber.yield
    #3.times { Fiber.yield }
    if(i >= bmp.height)
      i = 0
      ind = (ind + 1) % bmps.size
      cbmp = bmps[ind]
    end
  end
end

bmp = Bitmap.new(*Graphics.rect.xto_a(:width,:height))
bmps = []
c = DrawExt::TRANS_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::RED_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::GREEN_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::BLUE_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::YELLOW_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::WHITE_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
c = DrawExt::BLACK_BAR_COLORS
bmps << CacheExt.make_button("",c,bmp.width,bmp.height)
fiber = Fiber.new { scanline_transition(bmp,*bmps) }
sp = Sprite.new()
sp.bitmap = bmp
sp.center_xy()

loop do
  Main.update()
  fiber.resume
end
