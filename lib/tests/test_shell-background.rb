# SimpleBackgroundTest
class Hazel::Shell::Simple_Background_Test < Hazel::Shell::WindowSelectable

  def item_max
    15
  end

  def col_max
    1
  end

  include Hazel::Shell::Addons::Background

end

module TestCore

  def self.test_shell_background
    rect = Graphics.rect.contract(anchor: 5, amount: 48)
    container = Hazel::Shell::Simple_Background_Test.new(rect)
    container.activate

    _test_loop_(TEST_TIME_LONG) do
      container.update
    end

    container.dispose

    return TEST_PASSED
  end

end
