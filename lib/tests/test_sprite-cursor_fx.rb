# SpriteTest
class Sprite::CursorFX < Sprite

  attr_accessor :frame

  def initialize(viewport=nil)
    super( viewport )
    self.bitmap = Cache.system("cursor_effect")
    @frame = 0
    @time  = @time_max = 10
    update
  end

  def update()
    super()
    @time -= 1
    if @time <= 0
      @time = @time_max
      @frame = (@frame - 1) % 4
    end
    self.src_rect.set( 32 * @frame, 0, 32, 32 )
  end

end

module TestCore

  def self.test_sprite_cursor_fx
    sps = []

    for i in 0...(18 * 14)
      s = Sprite::CursorFX.new( nil )
      s.x = (i % 18) * 32
      s.y = (i / 18) * 32
      s.frame = i % 4
      sps << s
    end

    _test_loop_(TEST_TIME_LONG) do
      sps.each { |s| s.update }
    end

    sps.each(&:dispose)

    return TEST_PASSED
  end

end
