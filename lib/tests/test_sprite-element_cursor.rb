#
# EDOS/core/test_core/test-sprite-element_cursor.rb
# vr 1.0.1
module TestCore

  def self.test_sprite_element_cursor
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Element Cursor")
    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.y2 = title_sp.y

    s = Sprite::ElementCursor.new
    s.align_to!(anchor: 5, surface: canvas)
    w = 48 * s.elements_size
    x = (canvas.width - w) / 2
    target_values = (0...s.elements_size).map { |i| [x + 48 * i, s.y] }

    last_index = -1

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      s.update

      if Input.trigger?(:LEFT)
        s.pred_index
      elsif Input.trigger?(:RIGHT)
        s.succ_index
      end

      tx, ty = values = target_values[s.index]

      if s.x > tx    then s.x = (s.x - (Graphics.width / 60.0)).max(values[0])
      elsif s.x < tx then s.x = (s.x + (Graphics.width / 60.0)).min(values[0])
      end
      if s.y > ty    then s.y = (s.y - (Graphics.width / 60.0)).max(values[1])
      elsif s.y < ty then s.y = (s.y + (Graphics.width / 60.0)).min(values[1])
      end

      if last_index != s.index
        last_index = s.index
        i, id = last_index, last_index + 1
        fmt = "index: %d | element_id %d | element: %s"
        help_sp.set_text(fmt % [i, id, Vocab.element(id)])
      end
    end

    s.dispose
    help_sp.dispose
    title_sp.dispose

    return TEST_PASSED
  end

end
