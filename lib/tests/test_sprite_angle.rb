module TestCore

  def self.test_sprite_angle
    sp = Sprite.new()
    sp.bitmap = Bitmap.new(64, 64)
    sp.ox = sp.width / 2
    sp.oy = sp.height / 2

    DrawExt.draw_gauge_ext_sp5(sp.bitmap, sp.bitmap.rect, 1.0)

    sp.align_to!(anchor: 5)

    sp_angle = Sprite.new
    sp_angle.bitmap = Bitmap.new(192, 24)
    sp_angle.align_to!(anchor: 8)

    target_angle = 0
    last_angle = 0

    _test_loop_(TEST_TIME_LONG) do

      sp.update
      if Input.trigger?(:C)
        sp.x, sp.ox = sp.ox, sp.x
        sp.y, sp.oy = sp.oy, sp.y
        _test_log do |log|
          log.puts "Flipped: x #{sp.x}, y #{sp.y}, ox #{sp.ox}, oy #{sp.oy}"
        end
      end
      if Input.press?(:LEFT)
        target_angle += -1
      elsif Input.press?(:RIGHT)
        target_angle += 1
      elsif Input.trigger?(:DOWN)
        target_angle = 0
      end

      #target_angle = #%= 360

      if last_angle != target_angle
        #puts "Rotated to #{target_angle}"
        last_angle = target_angle
      end

      if target_angle > sp.angle
        sp.angle = [target_angle, sp.angle + 1].min
      elsif target_angle < sp.angle
        sp.angle = [target_angle, sp.angle - 1].max
      end

      sp_angle.bitmap.clear
      sp_angle.bitmap.draw_text(0, 0, 192, 24, "Angle #{sp.angle}")
    end

    sp_angle.dispose_all
    sp.dispose_all

    return TEST_PASSED
  end

end
