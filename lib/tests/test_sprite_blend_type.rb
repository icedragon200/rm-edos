module TestCore

  def self.test_sprite_blend_type
    pla = pln(nil)
    pla.bitmap = Cache.system("menubackground2")

    sp = spr(nil)
    sp.bitmap = Cache.animation("ice10", 0)
    sp.blend_type = 4

    blend_types = [0, 1, 2, 3, 4]
    blend_type_index = 0

    _test_loop_(TEST_TIME_LONG) do
      if Input.repeat?(:LEFT) || Input.repeat?(:UP)
        blend_type_index = (blend_type_index - 1) % blend_types.size
      elsif Input.repeat?(:RIGHT) || Input.repeat?(:DOWN)
        blend_type_index = (blend_type_index + 1) % blend_types.size
      end
      sp.blend_type = blend_types[blend_type_index]
    end

    pla.dispose_all
    sp.dispose_all

    return TEST_PASSED
  end

end
