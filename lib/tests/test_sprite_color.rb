module TestCore

  def self.test_sprite_color
    canvas = Screen.content
    back_sp = Sprite.new
    back_sp.bitmap = Bitmap.new(canvas.width, canvas.height)
    back_sp.bitmap.fill(Palette['white'])
    sp = Sprite.new()
    sp.bitmap = Cache.system('balloon')#Bitmap.new(128, 128)
    sp.align_to!(anchor: 5, surface: canvas)
    #sp.bitmap.draw_gauge_ext
    sp.color = Palette['droid_blue_light']
    _test_loop_(TEST_TIME_LONG) do
      sp.update
    end
    sp.dispose
    back_sp.dispose_all
    return TEST_PASSED
  end

end