module TestCore

  def self.test_sprite_draw
    ### Canvas
    canvas = Screen.content
    viewport = Viewport.new(0, 0, 256, 256)
    viewport.rect.align_to!(anchor: 5)
    ### Allocation
    ## Title
    title_sp = Sprite::MerioTitle.new(viewport, "Sprite draw", 0)
    ## Help
    help_sp = Sprite::MerioHelp.new(viewport)
    sp = Sprite.new(viewport)
    plane = Plane.new(viewport)
    plane.bitmap = Cache.system("menubackground2")
    sp.bitmap = Cache.system("ball")

    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp.y2 = title_sp.y
    plane.z = -12
    sp.ox = sp.bitmap.width / 2
    sp.oy = sp.bitmap.height / 2
    #sp.zoom_x = sp.zoom_y = 0.5

    log_sp = Sprite.new(nil)
    begin
      bmp = log_sp.bitmap = Bitmap.new(gwidth, Metric.ui_element)
      bmp.draw_text(bmp.rect, viewport.rect.to_s, 2)
    end

    #tween = MACL::Tween::Sequencer.new
    #tween.add_tween(
    #  [-32, -32], [256 + 32, 256 + 32], :linear, MACL::Tween.frame_to_sec(90))
    #tween.add_tween(
    #  [256 + 32, 256 + 32], [-32, -32], :linear, MACL::Tween.frame_to_sec(90))


    last_pos = []
    _test_loop_(TEST_TIME_LONG) do
      #tween.update
      #sp.x, sp.y = tween.values
      sp.x = Mouse.x - viewport.rect.x
      sp.y = Mouse.y - viewport.rect.y

      if last_pos != (pn = [sp.x, sp.y])
        last_pos = pn
        hr = bmp.rect.dup; hr.height /= 2
        log_sp.bitmap.clear
        log_sp.bitmap.draw_text(hr, last_pos.inspect, 2)
        log_sp.bitmap.draw_text(hr.step!(2), viewport.rect.to_s, 2)
      end
     #plane.oy += 1
    end
    ### deallocation
    title_sp.dispose
    help_sp.dispose
    plane.dispose_all
    sp.dispose_all
    viewport.dispose
    ###
    return TEST_PASSED
  end

end
