#
# EDOS/core/test_core/test-sprite_draw2.rb
#   by IceDragon
#   dc 13/06/2013
#   dm 13/06/2013
class Sprite::GraphFPS < Sprite

  def initialize(viewport)
    super(viewport)
    self.bitmap = Bitmap.new(Graphics.width, Graphics.height / 2)
    @history = []
    @spacial = 60
    @tick = 0
  end

  def dispose
    self.bitmap.dispose
    super
  end

  def refresh
    bmp = self.bitmap
    bmp.clear
    bmp.fill(Palette['droid_dark_ui_enb'])
    w = bmp.width / @spacial
    h = bmp.height
    frm_rate = Graphics.frame_rate.to_f
    gauge_color = Palette['droid_red_light']
    values = @history
    values.each_with_index do |v, i|
      gh = Integer(h * (v.to_f / frm_rate))
      bmp.fill_rect(i * w + 2, h - gh, w - 4, gh, gauge_color)
    end
  end

  def update
    super
    @history << Graphics.fps
    if @history.size > @spacial
      @history.shift until @history.size <= @spacial
    end
    refresh if @tick % 2 == 0
    @tick += 1
  end

end

module TestCore

  def self.test_sprite_draw2
    canvas = Screen.content
    viewport = Viewport.new(canvas)
    viewport.rect.contract!(anchor: 5, amount: Metric.contract)

    w = viewport.width / 40
    h = w

    bmp = Bitmap.new(w, h)
    bmp.merio.draw_fill_rect(bmp.rect, Palette['droid_blue_light'])

    background_sp = Sprite.new
    background_sp.bitmap = Bitmap.new(gwidth, gheight)
    background_sp.bitmap.fill(Palette['droid_light_ui_enb'])

    sprites = []
    400.times do
      sp = Sprite.new(viewport)
      sp.bitmap = bmp
      sprites.push(sp)
    end
    tile_surfaces(sprites)

    graph_sp = Sprite::GraphFPS.new(nil)
    graph_sp.align_to!(anchor: 2)

    _test_loop_(TEST_TIME_LONG) do
      for sp in sprites
        sp.update
        sp.x += 1
        sp.x %= viewport.rect.width
      end
      graph_sp.update
    end

    sprites.each(&:dispose_all)
    background_sp.dispose_all
    graph_sp.dispose
    viewport.dispose

    return TEST_PASSED
  end

end
