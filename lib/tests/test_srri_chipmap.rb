module TestCore

  def self.test_srri_chipmap
    tb = Table.new(8, 8)

    chipmap = SRRI::Chipmap.new
    chipmap.tilesize = 16
    chipmap.width = tb.xsize * chipmap.tilesize
    chipmap.height = tb.ysize * chipmap.tilesize
    chipmap.map_data = tb
    chipmap.tile_bitmap = Cache.tileset('dirt-tiles-fixed')
    chipmap.tile_columns = chipmap.tile_bitmap.width / chipmap.tilesize
    chipmap.refresh

    _test_loop_(TEST_TIME_LONG) do
      chipmap.update
    end

    chipmap.dispose

    return TEST_PASSED
  end

end
