module TestCore

  def self.test_srri_srsprite
    return TEST_DISABLED

    w, h = 24, 24
    cols = Graphics.width / w
    rows = Graphics.height / h

    colors = [Palette['sys1_blue'], Palette['sys1_green']]

    sps = []
    for y in 0...rows
      for x in 0...cols
        i = x + (y * cols)
        sp = SRRI::SoftSprite.new
        bmp = sp.bitmap = Bitmap.new(w, h)
        bmp.fill_rect(bmp.rect, colors[i % colors.size])

        sp.x = x * w
        sp.y = y * h
        #sp.z = i

        sps.push(sp)
      end
    end

    _test_loop_(TEST_TIME_LONG) do
      sps.each(&:update)
    end

    sps.each(&:dispose_all)

    return TEST_PASSED
  end

end
