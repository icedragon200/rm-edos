module TestCore

  def self.test_srri_window_arrows
    window = Window::Base.new(0, 0, 320, 320)
    window.width  = 160
    window.height = 160

    window.ox += 80
    window.oy += 80
    window.pause = true

    sp = spr(nil)
    sp.bitmap = window.windowskin
    sp.align_to!(anchor: 9)

    _test_loop_(TEST_TIME_LONG) do
      window.update
      case Input.dir4
      when 2
        window.oy += 1
      when 8
        window.oy -= 1
      end
    end

    window.dispose
    sp.dispose

    return TEST_PASSED
  end

end
