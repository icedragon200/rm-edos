module TestCore

  def self.test_starruby_transition
    stack = Dir.glob('Chapter2/*.png').sort.map { |fn| bmp(fn) }
    return TEST_FAILED if stack.empty?
    trans = stack.map do |bmp|
      b = bmp.dup
      #DrawExt.reduce(b, b.rect, 2)
      #DrawExt.noise(b, b.rect, 1.0, true)
      4.times { b.blur }
      trans = StarRuby::Transition.from_texture(b.texture)
      b.dispose
      #trans.invert!
      trans
    end
    w, h = stack[0].width, stack[0].height
    bitmap = bmp(w, h)
    #transition_bmp = bmp(bmp1.width, bmp1.height)
    #transition_bmp.gradient_fill_rect(transition_bmp.rect,
                                      #Color.new(0, 0, 0, 255),
                                      #Color.new(255, 255, 255, 255), true)
    #chipsize = 80
    #(transition_bmp.height / chipsize).times do |y|
    #  (transition_bmp.width / chipsize).times do |x|
    #    col = Color.new(255,255,255,255)
    #    col.negate! if (x + y) % 2 == 0
    #    transition_bmp.gradient_fill_rect(x * chipsize, y * chipsize,
    #                                      chipsize, chipsize, col, -col, true)
    #  end
    #end
    #transition_bmp.fill_rect(transition_bmp.rect, Color.rgb24(0x7F7F7F))
    #DrawExt.noise(transition_bmp, transition_bmp.rect, 1.0, true)
    #DrawExt.reduce(transition_bmp, transition_bmp.rect, 2)

    #DrawExt.reduce(bmp1, bmp1.rect, 2)
    #DrawExt.reduce(bmp2, bmp2.rect, 2)

    #_tmp = tra.to_texture
    #_tmp.save("checker-vertical-#{chipsize}px.png")
    #_tmp.dispose

    time = 0

    sp = spr(nil)
    sp.bitmap = bitmap

    maxtime = Graphics.frame_rate * 5
    halftime = maxtime / 2

    index = 0

    tran = trans[index % stack.size]
    trg = stack[index.succ % stack.size]
    src = stack[index % stack.size]

    _test_loop_(TEST_TIME_X2LONG) do
      n = Graphics.frame_count % (maxtime + 1)

      #if n < halftime
      #  time = 255 * (n / halftime.to_f)
      #  tran.transition(bitmap.texture, src.texture, trg.texture, time)
      #elsif n < maxtime
      #  time = (255 * ((n - halftime) / halftime.to_f))
      #  tran.transition(bitmap.texture, trg.texture, src.texture, time)
      if n < maxtime
        time = 255 * (n / maxtime.to_f)
        tran.transition(bitmap.texture, src.texture, trg.texture, time)
      else
        index = index.succ % stack.size
        time = 0
        tran = trans[index % stack.size]
        trg = stack[index.succ % stack.size]
        src = stack[index % stack.size]
      end
    end

    trans.each(&:dispose)
    stack.each(&:dispose)
    sp.dispose_all

    return TEST_PASSED
  end

end
