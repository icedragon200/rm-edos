module TestCore

  def self.test_strb_3d
    return TEST_DISABLED unless StarRuby::Texture.method_defined?(:render_in_perspective)
    #{:camera_x=>0.0, :camera_y=>0.0, :camera_height=>96.0, :camera_yaw=>-3.9444441095071845, :camera_pitch=>-1.3788101090755203, :camera_roll=>0.0, :loop=>false}

    options = {
      camera_x: 96,
      camera_y: 96,
      camera_height: 96,
      camera_yaw: 0.degree_to_radian,
      camera_pitch: 0.degree_to_radian,
      camera_roll: 0.degree_to_radian,
      loop: true#,
      #blur: :background
    }

    bitmap = bmp(32, 32)
    DrawExt.draw_gauge_ext_sp5(bitmap, bitmap.rect, 1.0, DrawExt::BLUE_BAR_COLORS)
    screen_bitmap = Bitmap.new(gwidth, gheight)
    screen_bitmap.texture.render_in_perspective(bitmap.texture, options)

    screen_sp = spr(nil)
    screen_sp.bitmap = screen_bitmap

    camera = ThreeDee::Camera3.new
    camera.pos.x = 0
    camera.pos.y = 0
    camera.pos.z = 96

    _test_loop_(TEST_TIME_LONG) do
      Main.update
      screen_sp.update
      if Input.press?(:UP)
        camera.pitch += 1
      elsif Input.press?(:DOWN)
        camera.pitch -= 1
      end
      if Input.press?(:LEFT)
        camera.yaw -= 1
      elsif Input.press?(:RIGHT)
        camera.yaw += 1
      end
      if Input.trigger?(:A)
        options[:loop] = !options[:loop]
      end
      if Input.press?(:L)
        camera.z -= 1
      elsif Input.press?(:R)
        camera.z += 1
      end
      if Input.press?(:B)
        camera.pos.add!(camera.rotation.normalize * 4)
      elsif Input.press?(:Y)
        camera.pos.sub!(camera.rotation.normalize * 4)
      elsif Input.press?(:X)
        camera.pos.sub!(camera.pos.normalize)
      elsif Input.press?(:Z)
        camera.pos.add!(camera.pos.normalize)
      end
      options.merge!(camera.to_options)
      p options.to_s if Input.trigger?(:p)
      screen_bitmap.texture.clear
      screen_bitmap.texture.render_in_perspective(bitmap.texture, options)
      rect = screen_bitmap.rect.dup
      rect.height = 24
      rect.y2 = screen_bitmap.rect.y2
      screen_bitmap.font.size = 16
      screen_bitmap.draw_text(rect, options.to_s, 1)
    end

    screen_sp.dispose
    screen_bitmap.dispose
    bitmap.dispose

    return TEST_PASSED
  end

end
