module TestCore

  def self.test_strb_isometric_3d
    return TEST_DISABLED unless StarRuby::Texture.method_defined?(:render_in_perspective)
    options = { :camera_x=>0,
                :camera_y=>32.0,
                :camera_height=>32.0 * 8,
                :camera_yaw=> -45.degree_to_radian,
                :camera_pitch=> -60.degree_to_radian,
                :camera_roll=> 0.0,
                :view_angle => 45.degree_to_radian,
                :loop=>true
              }

    bitmap = bmp(32, 32)
    DrawExt.draw_gauge_ext_sp5(bitmap, bitmap.rect, 1.0, DrawExt::BLUE_BAR_COLORS)
    #bitmap.texture.transform_in_perspective
    screen_bitmap = bmp(gwidth, gheight)
    screen_bitmap.texture.render_in_perspective(bitmap.texture, options)

    sp = Sprite.new(nil)
    sp.bitmap = screen_bitmap

    _test_loop_(TEST_TIME_LONG) do

    end

    bitmap.dispose
    screen_bitmap.dispose
    sp.dispose

    return TEST_PASSED
  end

end
