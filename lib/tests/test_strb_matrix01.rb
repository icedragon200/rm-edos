module TestCore

  def self.test_strb_matrix01
    canvas = Screen.content
    background_plane = Plane.new
    background_plane.bitmap = Cache.picture("tactile_noise")
    background_plane.z = -100

    title_sp = Sprite::MerioTitle.new(nil, "Matrix Test!")
    title_sp.align_to!(anchor: 2, surface: canvas)

    cube_size = 24

    colors = (1..6).map { |i| DrawExt.quick_bar_colors(Palette["element#{i}"], DrawExt::STYLE_RK4) }

    cube_bmps = Array.new(0xF) do
      cube_bmp = bmp(cube_size, cube_size)
      DrawExt.draw_gauge_ext_sp5(cube_bmp, cube_bmp.rect, 1.0, colors.sample)
      cube_bmp
    end

    shadow_bmp = Bitmap.new(cube_size, cube_size / 2)
    c1 = Color.new(0, 0, 0, 255 * 0.8)
    c2 = c1.dup; c2.alpha = 0
    shadow_bmp.gradient_fill_rect(shadow_bmp.rect.hset(width: shadow_bmp.rect.width * 0.5), c1, c2)

    bitsize = 10
    w = h = 15
    matrix = mtx([w, h], 0)

    brush_size = w / 2
    matrix_brush = mtx([brush_size, brush_size], 0)
    for i in 0...(brush_size / 2)
      add_brus = mtx([brush_size - i * 2, brush_size - i * 2], 1)
      matrix_brush.add_at!([i, i], add_brus, *add_brus.dimnet)
    end
    #viewport = view()
    #viewport.rect.contract!(anchor: 5, amount: 64)

    sps = []
    shdw_sps = []
    tweeners = []

    easer = :back_out
    tick  = 30

    xsize, ysize = matrix.dimensions # // x, y
    sx, sy = 64, (canvas.height - (cube_size * h / 2)) / 2

    shadow_pln = Sprite.new(nil)
    shadow_pln.bitmap = Bitmap.new(w * cube_size, (h + 1) * cube_size / 2)
    shadow_pln.bitmap.fill_rect(shadow_pln.bitmap.rect, Color.new(0, 0, 0, 0x80))
    shadow_pln.x = sx
    shadow_pln.y = sy
    shadow_pln.z = 0

    matrix.datasize.times do
      sp = Sprite.new(nil)#viewport)
      sp.bitmap = cube_bmps.sample
      sps.push(sp)
      shdw_sp = Sprite.new(nil)
      shdw_sp.bitmap = shadow_bmp
      shdw_sps.push(shdw_sp)
      tweeners.push(MACL::Tween2.new(tick, easer, [0.0, 0.0], [0.0, 0.0]))
    end


    matrix_sp = Sprite.new(nil)
    matrix_sp.bitmap = bmp(xsize * bitsize, ysize * bitsize)
    matrix_sp.bitmap.font.size = 14

    #matrix_sp.align_to!(anchor: 3)
    matrix_sp.align_to!(anchor: 3, surface: canvas)
    matrix_sp.y2 = title_sp.y
    #matrix_sp.x = gwidth - matrix_sp.width
    #matrix_sp.y = gheight - matrix_sp.height

    update_pos = true


    for y in 0...ysize
      for x in 0...xsize
        segrect = rct(x * bitsize, y * bitsize, bitsize, bitsize)
        matrix_sp.bitmap.clear_rect(segrect)
        DrawExt.draw_padded_rect_flat(matrix_sp.bitmap, segrect,
                                      [Palette['droid_dark_ui_enb'],
                                       Palette['droid_dark_ui_dis']])
      end
    end

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      sps.each_with_index do |s, i|
        shdw = shdw_sps[i]
        twn = tweeners[i]
        twn.update
        s.update
        s.x, s.y = twn.result
        shdw.x, shdw.y = s.x, s.y
      end

      if Mouse.middle_click?
        matrix.reset!
        update_pos = true
      end

      if Mouse.x.between?(matrix_sp.x, matrix_sp.x2) &&
        Mouse.y.between?(matrix_sp.y, matrix_sp.y2)
        l = Mouse.left_repeat?
        r = Mouse.right_repeat?
        if l || r
          x = ((Mouse.x - matrix_sp.x) / bitsize).round.to_i
          y = ((Mouse.y - matrix_sp.y) / bitsize).round.to_i

          ox = matrix_brush.dimensions[0] / 2
          oy = matrix_brush.dimensions[1] / 2
          pos = [x-ox, y-oy]
          if l
            #matrix[x, y] += 1
            matrix.add_at!(pos, matrix_brush, [0, 0], matrix_brush.dimensions)
          elsif r
            #matrix[x, y] -= 1
            matrix.sub_at!(pos, matrix_brush, [0, 0], matrix_brush.dimensions)
          end
          update_pos = true
        end
      end

      if Input.trigger?(:R)
        matrix.inc!
        update_pos = true
      elsif Input.trigger?(:L)
        matrix.dec!
        update_pos = true
      elsif Input.trigger?(:X)
        matrix.negate!
        update_pos = true
      end

      if Input.trigger?(:B)
        matrix.reset!
        update_pos = true
      end

      if update_pos
        for y in 0...ysize
          for x in 0...xsize
            i = x + y * xsize
            mtv = matrix[x, y]
            sp = sps[i]
            shdw = shdw_sps[i]
            twn = tweeners[i]
            tna = [sx + x * sp.width, sy + (y - mtv) * sp.height / 2]
            if twn.end_values.map(&:to_i) != tna
              twn.setup_pairs(*twn.result.zip(tna))
              twn.reset_tick
            end
            sp.z = y + 1
            shdw.z = sp.z + 1
          end
        end
        for y in 0...ysize
          for x in 0...xsize
            i = x + y * xsize
            mtvL = matrix[x-1, y]
            mtvC = matrix[x, y]
            shdw = shdw_sps[i]
            shdw.opacity = mtvL > mtvC ? 255 : 0
          end
        end
        update_pos = false
      end
    end

    shadow_bmp.dispose
    matrix_sp.dispose_all
    shadow_pln.dispose_all
    background_plane.dispose
    title_sp.dispose
    sps.each(&:dispose)
    shdw_sps.each(&:dispose)
    cube_bmps.each(&:dispose)

    return TEST_PASSED
  end

end
