module TestCore::Scene
  class StarRuby_Texture_CairoBindings < Scene::TestBase

    def start
      return TEST_DISABLED unless StarRuby::Texture.method_defined?(:init_cairo_bind)
      super
      stroke_line = ->(context, x1, y1, x2, y2, line_width, dash_length) do
        context.save do
          context.move_to(x1, y1)
          context.line_to(x2, y2)
          context.set_dash(dash_length) if dash_length > 0
          context.line_width = line_width
          context.line_cap = :round
          context.stroke
        end
      end

      #cr_surface = Cairo::ImageSurface.new(gwidth, gheight)
      #context = Cairo::Context.new(cr_surface)

      texture = StarRuby::Texture.new(gwidth, gheight)
      texture.init_cairo_bind
      cr_surface = texture._cairo_surface
      context = texture._cairo_context

      bitmap = Bitmap.new(texture) #Bitmap.bind_from_cairo(cr_surface)

      sp = spr(nil)
      sp.bitmap = bitmap

      last_x, last_y = 0, 0
      mouse_moved = ->() { last_x != Mouse.x || last_y != Mouse.y }

      _test_loop_(TEST_TIME_LONG) do
        sp.update
        if mouse_moved.()
          last_x, last_y = Mouse.x, Mouse.y
          #texture.fill_rect(0, 0, texture.width, texture.height, StarRuby::Color.new(255, 255, 255, 0))
          context.save do
            context.set_source_color([0.0, 0.0, 0.0, 1.0])
            context.rectangle(0, 0, bitmap.width, bitmap.height)
            context.fill
          end
          context.set_source_color([1.0, 1.0, 1.0, 1.0])
          weight = 32
          for y in [0, gheight / 2, gheight]
            for x in [0, gwidth / 2, gwidth]
              stroke_line.(context, x, y, last_x, last_y, weight, 0)
            end
          end

          #bitmap.texture.undump(cr_surface.data, sr_format)
        end
      end

      sp.bitmap.texture.release_cairo
      sp.bitmap.dispose
      sp.dispose
      cr_surface.destroy
      context.destroy

      return TEST_PASSED
    end

    def title_text
      'Bitmap#draw_gradient_fill_rect 2'
    end

  end
end