module TestCore

  def self.test_strb_texture_render_color
    return TEST_DISABLED
    sp        = spr(nil)
    sp.bitmap = Cache.system('ball')
    sp.color  = Palette['droid_blue']
    ticks     = 0
    loop do
      sp.color.alpha = ticks % 256
      Main.update
      sp.update
      ticks += 1
    end
    return TEST_PASSED
  end

end
