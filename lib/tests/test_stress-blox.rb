#
# test-blox.rb
#
# Blox is a test for Array#reverse_index and DrawExt functions

=begin
"FFFF".hex
("F"*4).hex
0xFFFF
2**16
=
65535
=end
module TestCore

  def self.test_stress_blox
    version = "3.0.0".freeze
    canvas = Screen.content
    viewport = Viewport.new(canvas.x, canvas.y, canvas.width, canvas.height)
    #viewport.rect.y += 24

    title_sp = Sprite::MerioTitle.new(nil, "B.L.O.X vr #{version}")
    title_sp.align_to!(anchor: 2, surface: canvas)
    viewport.rect.height -= title_sp.height
    viewport.rect.height -= 24

    color_variations = 0x80 #32
    block_size = 48 #48 #32
    page_count = 32 #32
    screen_w2  = (viewport.width / block_size) / 4
    total_size = ((viewport.width) / block_size) *
                 (viewport.height / block_size) # // Screen size
    total_size+= viewport.width % block_size
    total_size*= page_count

    sp = Sprite.new(viewport)

    progress_bar = Sprite.new
    progress_bar.bitmap = Bitmap.new(canvas.width, 24)
    DrawExt.draw_gauge_ext_sp5(progress_bar.bitmap,
      progress_bar.bitmap.rect, 1.0,
      DrawExt::BLUE_BAR_COLORS
      #DrawExt.quick_bar_colors(Color.new(0, 178, 212))
    )
    progress_bar.y2 = title_sp.y
    wd = progress_bar.bitmap.width

    color_bmps = []

    fiber_sp = Sprite::MerioTextBox.new(nil, Screen.panel.width, Screen.panel.height)
    fiber_sp.use_background = false
    fiber_sp.align_to!(anchor: 2, surface: Screen.panel)
    fiber_sp.z = 999

  catch(:blox_end) do
    while true do
    #1.times do
      sample_colors = color_variations.times.map{ |i| Color.random }
      sample_colors.sort_by! do |c|
        ColorTool.calc_lumf(c)
      end

      color_bmps = sample_colors.map do |c|
        bmp = Bitmap.new(block_size,block_size)
        DrawExt.draw_gauge_ext_sp6(bmp, bmp.rect, 1.0, DrawExt.quick_bar_colors(c, 0))
        bmp
      end

      total_size += total_size % color_bmps.size
      arrasize    = total_size / color_bmps.size

      colors_ids = color_bmps.size.times.inject([]) do |r, i|
        r + Array.new(arrasize, i)
      end

      random_ids = Array.new(total_size) { colors_ids.sample }

      random_ids.compact!
      sorted_ids = random_ids.sort.reverse

      hg = (random_ids.size / ((viewport.height)/block_size))
      sp.bitmap = Bitmap.new(viewport.width,hg * block_size)
      sp.ox = 0
      sp.oy = 0

      pos_table = Table.new(2, random_ids.size)

      block = Rect.new(0, 0, viewport.width / block_size, hg)
      redraw_block = proc do |index, opacity=255|
        id = random_ids[index]
        cbmp = color_bmps[id]

        #x = (index % block.width * block_size)
        #y = (index / block.width * block_size)
        x = pos_table[0, index]
        y = pos_table[1, index]
        sp.bitmap.clear_rect(x, y, block_size, block_size)
        sp.bitmap.blt(x, y, cbmp, cbmp.rect, opacity)
      end

      for i in 0...random_ids.size
        pos_table[0, i] = (i % block.width * block_size)
        pos_table[1, i] = (i / block.width * block_size)
        redraw_block.call(i)
      end

      tween = MACL::Tween.new([0], [0], :back_out, 1.0)
      last_oy = 0
      fibers = []

      # //
      #Mouse.init
      speed = 1

      begin
        eids = (sorted_ids-[sorted_ids.last])
        size = eids.size
        bench = []
        #updater = Thread.new do
        #  loop do
        #    TestCore.main_update
        #  end
        #end
        oy = 0
        eids.each_with_index do |n, i|
          throw :blox_end if TestCore.main_update
          fiber_sp.set_text(fibers.size, 1)

          #bench.push(Time.now)
          title_sp.update
          if Input.repeat?(:LEFT)
            speed = (speed - 1).max(1)
            puts "speed is now x#{speed}"
          elsif Input.repeat?(:RIGHT)
            speed = (speed + 1).min(30)
            puts "speed is now x#{speed}"
          end
          speed.times do
            fibers.select! do |f|
              f[0] += 1
              redraw_block.call(f[1],(255/30)*f[0])
              redraw_block.call(f[2],(255/30)*f[0])
              if f[0] < 30
                true
              else
                redraw_block.call(f[1])
                redraw_block.call(f[2])
                false
              end
            end
            ## tween settings
            oy = (((i / block.width) - screen_w2).max(0) * block_size)
          end
          if last_oy != oy
            tween = MACL::Tween.new([last_oy],[oy],:back_out,
                                    MACL::Tween.frame_to_sec(40))
            last_oy = oy
          end
          tween.update
          sp.oy = tween.value
          ind = random_ids.rindex(n)
          random_ids[ind], random_ids[i] = random_ids[i], random_ids[ind]
          fibers << [0,ind,i] if random_ids[ind] != random_ids[i]
          #sp.update
          progress_bar.src_rect.width = wd * i / size.to_f
        end
        #prev = bench[0]
        #File.write("bench.log", bench.map { |t| n = t - prev; prev = t; n }.each_with_index.to_a.map { |(x,i)| [i, x].inspect }.join("\n"))
        color_bmps.each(&:dispose)
        color_bmps.clear
        sp.bitmap.dispose
      end
    end
  end

    fiber_sp.dispose
    title_sp.dispose
    sp.dispose_all
    progress_bar.dispose_all
    viewport.dispose
    color_bmps.each(&:dispose)

    return TEST_PASSED
  end

end
