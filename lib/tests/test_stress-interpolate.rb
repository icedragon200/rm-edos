# Interpolate
class BitmapEx

  def self.draw_line(bitmap, x1, y1, x2, y2, color, weight = 1)
    # Bresenham's line algorithm
    a = (y2 - y1).abs
    b = (x2 - x1).abs
    s = (a > b)
    dx = (x2 < x1) ? -1 : 1
    dy = (y2 < y1) ? -1 : 1
    if s
      c = a
      a = b
      b = c
    end
    df1 = ((b - a) << 1)
    df2 = -(a << 1)
    d = b - (a << 1)
    self.set_pxels(bitmap, x1, y1, color, weight)
    if(s)
      while y1 != y2
        y1 += dy
        if d < 0
          x1 += dx
          d += df1
        else
          d += df2
        end
        self.set_pxels(bitmap, x1, y1, color, weight)
      end
    else
      while x1 != x2
        x1 += dx
        if d < 0
          y1 += dy
          d += df1
        else
          d += df2
        end
        self.set_pxels(bitmap, x1, y1, color, weight)
      end
    end
  end

  def self.set_pxels(bitmap, x, y, color, weight = 1)
    even = ((weight % 2) == 0) ? 1 : 0
    half = weight / 2
    for px in (x-half)..(x+half-even)
      for py in (y-half)..(y+half-even)
        bitmap.set_pixel(px, py, color)
      end
    end
  end

end

module TestCore


  def self.test_stress_interpolate
    sp = Sprite.new
    sp.bitmap = Cache.system("ball")
    sp.anchor_oxy!(anchor: 5)
    #sp_points = Array.new(4) do
    #  tsp = Sprite.new; tsp.bitmap = Cache.system("Ball")
    #  tsp.zoom_x = tsp.zoom_y = 0.5; tsp.tone = Palette['green'].hset(:alpha=>0).to_tone
    #  tsp.center_oxy(32,32)
    #  tsp
    #end

    canvas = Screen.content
    mni = Bitmap.new(canvas.width, canvas.height)
    mni_sprite = Sprite.new()
    mni_sprite.bitmap = mni

    1.times do
      mni.clear
      points = []
      sz = 32 #+rand(86)
      points = Array.new(sz) { Point2.new(0, 0) }
      points.each_with_index { |p,i|
        p.x,p.y = Graphics.width*i/sz.to_f,rand(Graphics.height)
      }
      #a = Point[Graphics.width*0.12,100]
      #b = Point[Graphics.width*0.38, 20]
      #c = Point[Graphics.width*0.72,180]
      #d = Point[Graphics.width*0.95,100]
      #points += [a, b, c, d]
      #points.each_with_index{|p,i|
      #  sp_points[i].x,sp_points[i].y = p.to_xy_a
      #}
      for i in 0...(points.size-1)
        p1, p2 = points[i], points[i+1]
        c = Palette['sys1_orange'].lerp(Palette['sys1_red'],i/(points.size-1).to_f)
        BitmapEx.draw_line(mni,p1.x.to_i,p1.y.to_i,p2.x.to_i,p2.y.to_i,c,2)
        Graphics.update
      end
      last_point = nil
      for i in 0...256
        dest = Point2.new(0, 0)
        MACL::Interpolate.bezier(dest,i/256.0,*points)
        #puts dest.to_s
        sp.x, sp.y = dest.to_a
        Main.update #if i % 10 == 0
        if last_point
          BitmapEx.draw_line(mni,last_point.x.to_i,last_point.y.to_i,dest.x.to_i,dest.y.to_i,Palette['white'])
        end
        last_point = dest
      end
    end

    sp.dispose
    mni_sprite.dispose

    mni.dispose

    return TEST_PASSED
  end

end
