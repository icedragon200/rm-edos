module TestCore
  # .-.
  class Node < Point2

    attr_accessor :parents, :children, :sprite

    def initialize(x, y, node, node_c)
      super(x, y)
      @parents = Array(node)
      @children = Array(node_c)
      @ox,@oy = 0,0
    end

    attr_accessor :ox, :oy

    def refresh
      @parents.compact!
      @parents.uniq!
      @children.compact!
      @children.uniq!
    end

    def messages
      @messages ||= []
    end

    def tweeners
      @tweeners ||= []
    end

    def screen_x
      self.x - self.ox
    end

    def screen_y
      self.y - self.oy
    end

    def update
      messages.select! do |msg_a|
        msg_a[0] = msg_a[0].pred.max(0)
        if msg_a[0] == 0
          args,func = msg_a[1]
          instance_exec(*args,&func)
        end
        msg_a[0] > 0
      end
      self.ox,self.oy = 0,0
      tweeners.select! do |tween|
        tween.update
        self.ox,self.oy = tween.values
        !tween.done?
      end
    end

    def send_flash_msg
      args = []
      func = proc do
        sprite.flash Palette['sys_blue1'], 30
        seq = MACL::Tween::Sequencer.new
        seq.add_tween([0,0],[0,BITMAP.height/2],:back_out,MACL::Tween.frame_to_sec(30))
        seq.add_tween([0,BITMAP.height/2],[0,0],:sine_in,MACL::Tween.frame_to_sec(60))
        tweeners << seq
        send_flash_msg
      end
      @children.each do |node|
        msg = [30,[args,func]]
        node.add_msg msg.dup
      end
    end

    def add_msg msg
      messages << msg
    end

  end

  class Sprite_Node < Sprite

    attr_accessor :node

    def initialize node,viewport=nil
      super viewport
      @node = node
      @node.sprite = self
      self.bitmap = Bitmap.new(64, 64)
      self.bitmap.draw_gauge_ext(colors: DrawExt::GREEN_BAR_COLORS)
    end

    def dispose
      dispose_bitmap_safe
      super
    end

    def update
      super
      self.x = @node.screen_x
      self.y = @node.screen_y
    end

  end

  def self.test_stress_node
    return TEST_DISABLED
    node_bitmap = Bitmap.new(48, 48)
    DrawExt.draw_gauge_ext_sp4(node_bitmap)
    linear = []

    7.times do |i|
      linear[i] = Node.new node_bitmap.width+i*node_bitmap.width,64,i > 0 ? linear[i-1] : nil,nil
      linear[i-1].children << linear[i] if i > 0
    end
    nodes = linear

    sprites = Array.new(nodes.size-1) do |i|
      node = nodes[i+1]
      sp = Sprite_Node.new node
      sp
    end

    parent_node = nodes[0]
    nodes.each(&:refresh)

    count = 0

    _test_loop_(TEST_TIME_XLONG) do
      count += 1
      if count % (nodes.size * 32) == 0
        parent_node.send_flash_msg
      end
      sprites.each(&:update)
      #parent_node.update
      nodes.each(&:update)
    end

    sprites.each(&:dispose)

    return TEST_PASSED
  end

end
