module TestCore

  def self.test_stress_rect
    rects = []
    10000.times do |i|
      rects << Rect.new(rand(0xFFFF), rand(0xFFFF), 0xFFFF, 0xFFFF)
    end

    arys = rects.map(&:to_a)

    rects.clear
    arys.clear

    return TEST_PASSED
  end

end
