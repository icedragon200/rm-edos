class Hazel::Shell::SNHud < Hazel::Shell::WindowSelectable

  include Hazel::Shell::Addons::Background

  HP_COLOR = Color.rgb24(0xF80000)
  SP_COLOR = Color.rgb24(0x00F000)

  SN_STYLE = 1 # Style

  def initialize(*args, &block)
    super(*args, &block)
    activate.select(0)
    self.padding_bottom = 0
    update_contents_rect
  end

  def item_max
    5
  end

  def col_max
    5
  end

  def spacing
    8
  end

  def item_width
    return 24
  end

  def item_height
    return 24
  end

  def contents_height
    return self.height - (standard_padding * 2)
  end

  def item_rect(index)
    rect = super(index)
    rect.x += 16
    rect.y += 32
    rect.expand!(anchor: 5, amount: 1)
    return rect
  end

  def draw_sbackground
    bmp = @sbackground_sprite.bitmap
    rect = bmp.rect

    bmp.fill_rect(rect, Palette['droid_dark_ui_enb'])
    bmp.fill_rect(rect.contract(anchor: 5, amount: 1), Palette['droid_light_ui_dis'])
    bmp.fill_rect(rect.contract(anchor: 5, amount: 4), Palette['droid_light_ui_enb'])
  end

  attr_accessor :battler

  def standard_padding
    return Metric.padding
  end

  def refresh
    bmp = self.contents
    bmp.clear
    bmp.font.set_style('simple_brown1')

    back = Cache.system("status_stateborder(window)")
    bmp.blt(16, 0, back, back.rect)

    hp_rate = battler ? battler.hp_rate : 0.0
    mp_rate = battler ? battler.mp_rate : 0.0

    hp_rect = Rect.new(bmp.width / 3, 1 , bmp.width * 0.66, 10)
    hpt_rect = hp_rect.dup
    hpt_rect.width = 64
    hpt_rect.x -= hpt_rect.width

    mp_rect = Rect.new(bmp.width / 3, 13, bmp.width * 0.66, 10)
    mpt_rect = mp_rect.dup
    mpt_rect.width = 64
    mpt_rect.x -= mpt_rect.width

    bmp.draw_gauge_ext(hp_rect, hp_rate, DrawExt.quick_bar_colors(HP_COLOR))
    bmp.draw_gauge_ext(mp_rect, mp_rate, DrawExt.quick_bar_colors(SP_COLOR))

    bmp.draw_text(hpt_rect, battler.hp, 2)
    bmp.draw_text(mpt_rect, battler.mp, 2)

    case SN_STYLE
    when 1 # Summon Night - Swordcraft Story 1
      for x in 0...5
        bmp.blt(16 + (x * 32), 32, back, back.rect)
      end
    when 2 # Summon Night - Swordcraft Story 2
      for x in 0...6
        bmp.blt(16 + (x * 32), 32, back, back.rect)
      end
    end
  end

  #def update
  #  super
  #end

end

module TestCore

  def self.test_summon_night_hud
    actor = $game.actors[1]
    actor.entity.hp = (actor.entity.mhp * 0.5).to_i
    actor.entity.mp = (actor.entity.mmp * 0.7).to_i
    shell = Hazel::Shell::SNHud.new(0, 0, Graphics.width, 70)
    shell.battler = actor.entity
    shell.align_to!(anchor: 2)
    shell.refresh

    _test_loop_(TEST_TIME_LONG) do
      shell.update
    end

    shell.dispose

    return TEST_PASSED
  end

end
