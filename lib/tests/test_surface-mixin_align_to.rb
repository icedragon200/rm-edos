module TestCore

  def self.test_surface_align_to
    window_help = Window::Help.new()

    anchors = (1..9).to_a
    anchor_index = 0
    anchor_index_last = -1

    target_rect = Rect.new(0, 0, 32, 32)

    sp = Sprite.new
    sp.bitmap = Cache.system("ball.png")

    canvas = Graphics.rect.dup
    canvas.height -= window_help.height
    canvas.y += window_help.height

    _test_loop_(TEST_TIME_LONG) do
      if Input.repeat?(:LEFT)
        anchor_index -= 1
      elsif Input.repeat?(:RIGHT)
        anchor_index += 1
      end

      if anchor_index_last != anchor_index
        anchor_index_last = anchor_index % anchors.size
        anchor = anchors[anchor_index_last]
        target_rect.align_to!(anchor: anchor, surface: canvas)
        window_help.set_text("Anchor: #{anchor}")
      end

      #nx = sp.x != target_rect.x ? canvas.width / 30.0 : 0
      #nx = -nx if sp.x > target_rect.x
      #ny = sp.y != target_rect.y ? canvas.height / 30.0 : 0
      #ny = -ny if sp.y > target_rect.y

      sp.x = target_rect.x #(sp.x + nx).clamp(canvas.x, canvas.width)
      sp.y = target_rect.y #(sp.y + ny).clamp(canvas.y, canvas.height)

    end

    sp.dispose_all
    window_help.dispose

    return TEST_PASSED
  end

end
