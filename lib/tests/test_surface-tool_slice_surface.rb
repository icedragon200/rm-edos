module TestCore

  def self.test_surface_tool_slice
    rect = Graphics.rect.contract(anchor: 5, amount: 32)
    enum_x = MACL::Tween.new([0], [rect.width], :quad_in, MACL::Tween.frame_to_sec(32)).to_enum
    enum_y = MACL::Tween.new([0], [rect.height], :expo_inout, MACL::Tween.frame_to_sec(32)).to_enum

    slices_x = enum_x.map(&:value).map(&:round)
    slices_y = enum_y.map(&:value).map(&:round)

    rects = MACL::Surface::Tool.slice_surface(rect, slices_x, slices_y)
    #p rects.map(&:to_s)

    sp = Sprite.new(nil)
    sp.bitmap = Bitmap.new(Graphics.rect.width, Graphics.rect.height)

    rects.each do |r|
      sp.bitmap.fill_rect(r, Color.random)
    end

    _test_loop_(TEST_TIME_LONG) do
      sp.update
    end

    [sp].each do |s|
      s.bitmap.dispose
      s.dispose
    end

    return TEST_PASSED
  end

end
