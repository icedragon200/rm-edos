# Object
class SomeParentObject ; end
class SomeObject < SomeParentObject

  def <<
  end

  def >>
  end

  def +@(val)
    val+1
  end

  def -@(val)
    val-1
  end

  def <(val)
    @val = val
    puts "< #{@val}"
    self
  end

  def >
    puts "> #{@val}"
    self
  end

  def []
    puts "[]"
    self
  end

  def ^
    puts "^ :3"
    self
  end

  def /
    puts "/"
    self
  end

  def ~
    puts "~ Accent Stuff"
    self
  end

  def !
    puts "Exclamation!"
    self
  end

  def &
    puts "&"
    self
  end

  def _
    puts "Underscore_"
    self
  end

end

module TestCore

  def self.test_symbolic_object_func
    obj = SomeObject.new()

    ((obj < 2).>.[].^./.~).!.&._

    return TEST_PASSED
  end

end
