module TestCore

  def self.test_tank_thing
    return TEST_DISABLED # the Tank graphic was removed
    tank_base_sp = spr(nil)
    tank_top_sp = spr(nil)
    tank_base_sp.bitmap = Bitmap.new("spritesheet/tank_base.png")
    tank_top_sp.bitmap  = Bitmap.new("spritesheet/tank_top.png")
    tank_vec = StarRuby::Vector3F.new(0.0, 0.0, 0.0)
    anim_index = 0

    tank_dir = 2
    turret_dir = 2
    turret_lock = 0

    change_tank_dir_abs = ->(new_dir) do
      y = case new_dir
          when 2 then 2 #6
          when 4 then 0
          when 6 then 4
          when 8 then 2
          else raise(ArgumentError, "invalid direction %d" % new_dir)
          end
      tank_base_sp.src_rect.set(anim_index * 32, y * 32, 32, 32)
      tank_dir = new_dir
    end

    change_turret_dir_abs = ->(new_dir) do
      x = case new_dir
          when 2 then 6
          when 4 then 4
          when 6 then 0
          when 8 then 2
          else raise(ArgumentError, "invalid direction %d" % new_dir)
          end
      tank_top_sp.src_rect.set(x * 32, 0, 32, 32)
      turret_dir = new_dir
    end

    change_tank_dir = ->(new_dir) do
      change_tank_dir_abs.(new_dir)
      change_turret_dir_abs.(new_dir) if turret_lock == 1 # turret locked to tank
    end

    change_turret_dir = ->(new_dir) do
      change_tank_dir_abs.(new_dir) if turret_lock == -1 # tank locked to turret
      change_turret_dir_abs.(new_dir)
    end

    ticks = 0
    change_tank_dir.(tank_dir)
    change_turret_dir.(turret_dir)

    ## tank body fixes
    # how much the tank body be visually offseted
    tank_offset = {
      2 => [-2,  0],
      4 => [ 0,  0],
      6 => [ 0,  0],
      8 => [-2,  0]
    }

    # how much to offset the turret based on the Tanks direction
    tank_turret_offset = {
      2 => [ 0,  0],
      4 => [ 0,  0],
      6 => [ 0,  0],
      8 => [ 0,  0]
    }

    # how much to offset the turret based on the its own direction
    turret_offset = {
      2 => [ 0,  0],
      4 => [ 0,  1],
      6 => [ 0,  3],
      8 => [ 0,  2]
    }

    anim_tank_index = {
      2 => [0, 1, 2],
      4 => [0, 1, 2],
      6 => [0, 1, 2],
      8 => [0, 1, 2].reverse,
    }

    _test_loop_(TEST_TIME_LONG) do
      ticks += 1
      anim_index = (anim_index + 1) % 3 if ticks % 20 == 0

      Main.update
      # lock turret to tank
      if Input.trigger?(:t)
        turret_lock = 1
      elsif Input.trigger?(:y)
        turret_lock = 0
      # lock tank to turret
      elsif Input.trigger?(:u)
        turret_lock = -1
      end

      if Input.repeat?(:s)
        change_turret_dir.(2)
      elsif Input.repeat?(:a)
        change_turret_dir.(4)
      elsif Input.repeat?(:d)
        change_turret_dir.(6)
      elsif Input.repeat?(:w)
        change_turret_dir.(8)
      end

      case d=Input.dir4
      when 2
        tank_vec.y += 1
        change_tank_dir.(d)
      when 4
        tank_vec.x -= 1
        change_tank_dir.(d)
      when 6
        tank_vec.x += 1
        change_tank_dir.(d)
      when 8
        tank_vec.y -= 1
        change_tank_dir.(d)
      end

      tnk_off     = tank_offset[tank_dir]
      tnk_tur_off = tank_turret_offset[tank_dir]
      tur_off     = turret_offset[turret_dir]

      tank_base_sp.x = tank_vec.x - tnk_off[0]
      tank_base_sp.y = tank_vec.y - tnk_off[1]
      tank_base_sp.z = tank_vec.z
      tank_top_sp.x = tank_vec.x - tnk_tur_off[0] - tur_off[0]
      tank_top_sp.y = tank_vec.y - tnk_tur_off[1] - tur_off[1]
      tank_top_sp.z = tank_vec.z + 1

      tank_base_sp.src_rect.x = tank_base_sp.src_rect.width * anim_tank_index[tank_dir][anim_index]
    end

    tank_base_sp.dispose_all
    tank_top_sp.dispose_all

    return TEST_PASSED
  end

end
