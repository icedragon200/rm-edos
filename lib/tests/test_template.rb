module TestCore

  def self._test_template
    ### constants
    viewport = nil
    ### functions

    ### Canvas
    canvas = Screen.content
    ### Allocation
    ## Title
    title_sp = Sprite::MerioTitle.new(viewport, "Template", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    ## Help
    help_sp = Sprite::MerioHelp.new(viewport)
    help_sp.y2 = title_sp.y
    ### Main-Loop
    _test_loop_(TEST_TIME_LONG) do

      ##
      title_sp.update
      help_sp.update
    end
    ### De-Allocation
    help_sp.dispose
    title_sp.dispose
    ### Test-Result
    return TEST_PASSED
  end

end