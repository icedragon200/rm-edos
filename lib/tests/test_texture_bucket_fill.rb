module TestCore

  def self.test_texture_bucket_fill
    return TEST_DISABLED unless StarRuby::Texture.method_defined?(:bucket_fill)
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Texture Bucket Fill")
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.set_text("[A]: recolor | [B]: clear | [C]: bucket")

    sp = Sprite.new(nil)
    b = sp.bitmap = Bitmap.new(canvas.width, canvas.height -
                                             title_sp.height - help_sp.height)

    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp.y2 = title_sp.y

    _test_loop_(TEST_TIME_LONG) do
      if Input.trigger?(:A)
        b.fill(Palette['droid_red'])
        b.fill_rect(b.rect/2, Palette['droid_blue'])
        b.texture.recolor(b.rect, Palette['droid_red'], Palette['element3'])
      end
      if Input.trigger?(:B)
        b.clear
      end
      if Input.trigger?(:C)
        b.fill(Palette['droid_blue'])
        b.fill_rect(b.rect.contract(anchor: 5, amount: 1), Palette['droid_red'])
        b.fill_rect(4, 4, 4, 4, Palette['droid_blue'])
        p b.get_pixel(2, 2).to_s
        b.texture.bucket_fill(2, 2, Palette['element3'])
        p b.get_pixel(2, 2).to_s
      end
    end

    sp.dispose_all
    help_sp.dispose
    title_sp.dispose

    return TEST_PASSED
  end

end
