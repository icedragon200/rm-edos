module TestCore

  def self.test_texture_tool_color_blend
    canvas = Screen.content
    sp = Sprite.new
    bmp = sp.bitmap = Bitmap.new(canvas.width, 24)
    font = bmp.font
    font.outline = false
    font.color = Palette['sys1_red']
    bmp.draw_text(bmp.rect, "Quite a string you have there, would be too bad if I had to")
    TextureTool.color_blend(bmp.texture, Palette['sys1_blue'])

    sp.align_to!(anchor: 5)

    _test_loop_(TEST_TIME_LONG) do
      sp.update
    end

    sp.dispose_all

    return TEST_PASSED
  end

end
