module TestCore

  def self.test_tmx_map
    ###
    canvas = Screen.content
    ##
    viewport = Viewport.new(canvas)
    ###
    map = MapManager.load_map(0)
    camera = Camera.new
    layers = map.layers
    layer_maps = layers.size.times.map do |z|
      SRRI::Chipmap.new do |tm|
        layer = layers[z]
        layer_data = layer.data
        data = Table.new(map.width, map.height)
        data.ysize.times do |y|
          data.xsize.times do |x|
            data[x, y] = layer_data[x + y * data.xsize] - 1
          end
        end
        tm.map_data = data
        tm.tile_columns = 1024 / 32
        tm.tile_bitmap = Cache.atlas("terrain_atlas")
        tm.viewport = viewport
      end
    end
    ###
    _test_loop_(TEST_TIME_LONG) do
      case Input.dir4
      when 2
        camera.position += [ 0, 1, 0]
      when 4
        camera.position += [-1, 0, 0]
      when 6
        camera.position += [ 1, 0, 0]
      when 8
        camera.position += [ 0,-1, 0]
      end
      layer_maps.each do |layer|
        layer.ox = camera.x * layer.tilesize
        layer.oy = camera.y * layer.tilesize
        #layer.oz = camera.z # oz is a new feature from the Mythryl core spec.
        layer.update
      end
    end
    ### free
    layer_maps.each(&:dispose)
    ### result
    return TEST_PASSED
  end

end