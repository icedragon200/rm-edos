# Matrix Movement
class Trajectory
  def initialize

  end
end

module TestCore

  def self.test_tracjectory
    angle_sp = Sprite.new
    angle_sp.bitmap = Bitmap.new(gwidth, 24)

    sp = Sprite.new()
    sp.bitmap = Bitmap.new(32, 32)
    sp.bitmap.draw_gauge_ext(
      sp.bitmap.rect, 1.0, DrawExt.quick_bar_colors(Palette['sys1_orange']))

    sp.anchor_oxy!(anchor: 5)

    velo_sp = Sprite.new()
    velo_sp.bitmap = Bitmap.new(128,14)
    velo_sp.bitmap.draw_gauge_ext(
      velo_sp.bitmap.rect, 1.0,
      DrawExt.quick_bar_colors(Palette['sys1_blue']))

    turn_sp = [Sprite.new(), Sprite.new()]
    turn_sp[0].bitmap = Bitmap.new(64,14)
    turn_sp[0].bitmap.draw_gauge_ext(
      turn_sp[0].bitmap.rect, 1.0,
      DrawExt.quick_bar_colors(Palette['sys1_green']))

    turn_sp[0].y += turn_sp[0].height
    # //
    turn_sp[1].bitmap = Bitmap.new(64,14)
    turn_sp[1].bitmap.draw_gauge_ext(
      turn_sp[0].bitmap.rect, 1.0,
      DrawExt.quick_bar_colors(Palette['sys1_red']))

    turn_sp[1].x += turn_sp[1].width
    turn_sp[1].y += turn_sp[1].height

    ang = 0
    last_angle = -1

    box = MACL::Vector2F.new(Graphics.width / 2, Graphics.height / 2)

    vel = MACL::Vector2F.new(0, 0)

    _test_loop_(TEST_TIME_LONG) do

      if Input.press?(:LEFT)
        ang -= 4
      elsif Input.press?(:RIGHT)
        ang += 4
      end

      ang %= 360
      vel.angle = ang
      vel.magnitude = 4

      if Input.press?(:UP) || Input.press?(:DOWN)
        if Input.press?(:UP)
          box += vel
        elsif Input.press?(:DOWN)
          box -= vel
        end
      end

      turn_sp[0].src_rect.width = (vel.radian / Math::PI).min(0).abs * turn_sp[0].bitmap.width
      turn_sp[0].ox = -(turn_sp[0].bitmap.width - turn_sp[0].src_rect.width)
      turn_sp[1].src_rect.width = (vel.radian / Math::PI).max(0) * turn_sp[1].bitmap.width

      sp.angle = ang
      sp.x, sp.y = box.x, box.y

      if ang != last_angle
        last_angle = ang
        angle_sp.bitmap.clear
        angle_sp.bitmap.draw_text(angle_sp.bitmap.rect,
                                  "Angle #{ang} Radian #{vel.radian.round(2)}", 2)
      end
    end

    angle_sp.dispose_all
    sp.dispose_all
    velo_sp.dispose_all
    turn_sp.each(&:dispose_all)

    return TEST_PASSED
  end

end
