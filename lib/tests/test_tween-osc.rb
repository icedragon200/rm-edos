# OscTween Test
# // 01/26/2012
# // 01/26/2012
module TestCore

  def self.test_tween_osc
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Tween | OSC")
    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.y2 = title_sp.y

    sprite = Sprite.new(nil)
    sprite.bitmap = Cache.system("ball")
    sprite.anchor_oxy!(anchor: 5)
    size = sprite.ox.abs * 2

    tesk = MACL::Easer.easers.keys - [:null_in, :null_out]
    ax = tesk.shuffle[0...16] #16.times.map{tesk.pick}
    ay = tesk.shuffle[0...16]
    xt = 16.times.map { |_| 90 + (rand(2) ? rand(60) : -rand(60)) }
    yt = 16.times.map { |_| 90 + (rand(2) ? rand(60) : -rand(60)) }

    tweenerx = MACL::Tween::Osc.new([size],[canvas.width-size],
                                    ax, xt.map { |f| MACL::Tween.frame_to_sec(f) })

    tweenery = MACL::Tween::Osc.new([canvas.height - size - title_sp.height - help_sp.height],[size],
                                    ay, yt.map { |f| MACL::Tween.frame_to_sec(f) })

    _test_loop_(TEST_TIME_X3LONG) do
      title_sp.update
      help_sp.update
      tweenerx.update
      tweenery.update
      r = Graphics.rect
      sprite.x = tweenerx.value.clamp(r.x, r.width)
      sprite.y = tweenery.value.clamp(r.y, r.height)
    end

    sprite.dispose
    title_sp.dispose
    help_sp.dispose

    return TEST_PASSED
  end

end
