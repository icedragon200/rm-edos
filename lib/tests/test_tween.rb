# TweenTest
class MACL::Tween

  def self.run_test
    easers = MACL::Tween::EASERS
    f = Sprite.new(nil)
    f.bitmap = Cache.system( "Ball" )
    @help_window = Window::SmallText.new( 0, 0, Graphics.playwidth, 32 )
    @index = 0
    @tweeners = []
    make_tweeners = proc { |sprite, easer|
      result = MACL::Tween.new(
        [sprite.x, sprite.y],
        [sprite.width+rand(Graphics.width-(sprite.width * 2)),
        sprite.height+rand(Graphics.height-(sprite.height * 2))],
        easer, MACL::Tween.frame_to_sec( 30 )
      )
      #result << MACL::Tween.new( sprite.x, sprite.width+rand(544-(sprite.width * 2)), easer, 1 )
      #result << MACL::Tween.new( sprite.y, sprite.height+rand(416-(sprite.height * 2)), easer, 1 )
      result
    }
    loop {
      Graphics.update
      Input.update
      @tweener.update unless @tweener.done? if @tweener
      #@tweeners.each { |t| t.update }
      f.update
      f.x, f.y = *@tweener.values if @tweener
      if Input.trigger?(:C)
        #@tweeners = make_tweeners.call( f, Tween::EASERS[@index].symbol )
        @tweener = make_tweeners.call( f, easers[@index].symbol )
      elsif Input.trigger?(:LEFT)
        @index -= @index - 1 < 0 ? -(easers.size - 1) : 1
      elsif Input.trigger?(:RIGHT)
        @index += @index + 1 == easers.size ? -@index : 1
      end
      @help_window.set_text(easers[@index].name, 1)
    }
  end

  def self.graph_test
    easers = MACL::Tween::EASERS
    rect = Graphics.rect.contract(anchor: 5, amount: 32)
    @graph = Sprite.new(nil)
    @graph.bitmap = Bitmap.new(rect.width, rect.height)
    @graph.salign!(5)
    @help_window = Window::SmallText.new( 0, 0, Graphics.playwidth, 32 )
    @indexes  = Array.new(2, 0)
    @tweeners = []
    @outsize = 48
    make_tweeners = proc { |sprite, easers|
      result = Tween::Multi.new()
      result.add_tween( [@outsize], [sprite.width-@outsize], easers[0], Tween.frame_to_sec(120) )
      result.add_tween( [@outsize], [sprite.height-@outsize], easers[1], Tween.frame_to_sec(120) )
      result
    }
    @last_point = Point.new(0,0)
    loop do
      Graphics.update
      Input.update
      unless @tweener.done?
        @tweener.update
        point = @tweener.value(0), @tweener.value(1)
        @graph.bitmap.draw_line(@last_point,point,Palette['white'],2)
        @last_point = point
      end if @tweener
      if Input.trigger?(:C)
        @last_point = Point.new(0,0)
        @tweener = make_tweeners.call( @graph, @indexes.map { |i| easers[i].symbol } )
        @graph.bitmap.fill_rect(@graph.bitmap.rect, Palette['sys_orange2'])
        @graph.bitmap.fill_rect(@graph.bitmap.rect.contract(@outsize), Palette['sys_orange1'])
      elsif Input.trigger?(:UP)
        @indexes[0] = @indexes[0].pred.clamp(0,easers.size-1)
      elsif Input.trigger?(:DOWN)
        @indexes[0] = @indexes[0].succ.clamp(0,easers.size-1)
      elsif Input.trigger?(:LEFT)
        @indexes[1] = @indexes[1].pred.clamp(0,easers.size-1)
      elsif Input.trigger?(:RIGHT)
        @indexes[1] = @indexes[1].succ.clamp(0,easers.size-1)
      end
      t = format("Vertical : %s ---- Horizontal : %s", *@indexes.map { |i| easers[i].name })
      @help_window.set_text(t, 1)
    end
  end

end
#MACL::Tween.graph_test
#MACL::Tween.run_test
