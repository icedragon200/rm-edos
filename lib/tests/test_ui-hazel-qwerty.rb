module TestCore

  def self.test_ui_hazel_qwerty
    return TEST_DISABLED unless Hazel::VERSION < "2.0.0"
    canvas = Screen.content
    keyboard = [
      %w(` 1 2 3 4 5 6 7 8 9 0 - =) + [:backspace],
      %w(Q W E R T Y U I O P [ ] \\),
      %w(A S D F G H J K L ; ') + [:return],
      [:shift] + %w(Z X C V B N M , . /) + [:shift],
      [:ctrl, :meta, :alt, :space, :alt, :app, :ctrl]
    ]
    widgets = []
    sprites = []

    background_sp = Sprite.new(nil)
    background_sp.bitmap = Bitmap.new(canvas.width, canvas.height)
    background_sp.bitmap.fill(Palette['clay'])#Palette['droid_dark_ui_enb'])
    background_sp.bitmap.noise(0.12)
    #DrawExt.reduce(background_sp.bitmap, background_sp.bitmap.rect, 16)
    #background_sp.bitmap.blur

    title_sp = Sprite::MerioTitle.new(nil, "Hazel QWERTY")
    title_sp.align_to!(anchor: 2, surface: canvas)

    console_sp = Sprite.new(nil)
    console_sp.bitmap = Bitmap.new(canvas.width, Metric.ui_element / 2)
    console_sp.y2 = title_sp.y

    buffer = ""

    refresh_console = -> do
      console_sp.bitmap.fill(Palette['droid_light_ui_dis'])
      r = console_sp.bitmap.rect.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                                          amount: Metric.contract)
      console_sp.bitmap.draw_text(r, buffer)
    end

    dw = dh = 28
    upcase = false
    keyboard_x = 0
    keyboard_y = 0

    keyboard.each_with_index do |row, y|
      last_rect = Rect.new(keyboard_x + -dw + y * (dw / 2), keyboard_y + y * dh, dw, dh)
      row.each_with_index do |key, x|
        w, h  = case key
                when :ctrl, :alt,
                     :meta, :app  then [(dw * 1.25).to_i, dh]
                when :shift       then [dw * 2, dh]
                when :backspace   then [(dw * 2.75).to_i, dh]
                when :return      then [(dw * 3).to_i, dh]
                when :space       then [dw * 7, dh]
                else                   [dw, dh]
                end

        widget = Hazel::Widget::Button.new(last_rect.x + last_rect.width, last_rect.y, w, h)
        sp = Sprite.new
        bmp = sp.bitmap = Bitmap.new(widget.width * 2, widget.height)
        bmp.font.size = Metric.ui_font_size(:small)

        rect = bmp.rect.dup
        rect.width /= 2

        DrawExt.draw_gauge_ext_sp4(bmp, rect, 1.0, DrawExt::KEYBOARD_BAR_COLORS)
        bmp.draw_text(rect, key.to_s, 1)

        rect.x += rect.width

        DrawExt.draw_gauge_ext_sp4(bmp, rect, 1.0, DrawExt::BLUE_BAR_COLORS)
        bmp.font.color, bmp.font.out_color = bmp.font.out_color.hset(alpha: 255), bmp.font.color.hset(alpha: 72)
        bmp.draw_text(rect, key.to_s, 1)

        sp.x = widget.x
        sp.y = widget.y
        sp.z = widget.z
        sp.src_rect.set(0, 0, rect.width, rect.height)

        widget.add_event_handle(:mouse_left_click) do
          Sound.play_ex('typing')
          case key
          when :backspace   then buffer.chop!
          when :shift       then upcase = true
          when :return      then buffer.clear
          when :space       then buffer << "\s"
          when :ctrl, :alt,
               :meta, :app  then # nothing
          else
            key_s = key.to_s
            buffer << (upcase ? key_s.upcase : key_s.downcase)
            upcase = false
          end

          refresh_console.()
        end

        widget.add_event_handle(:mouse_start_over) do
          sp.src_rect.set(rect.width, 0, rect.width, rect.height)
        end

        widget.add_event_handle(:mouse_stop_over) do
          sp.src_rect.set(0, 0, rect.width, rect.height)
        end

        widgets.push(widget)
        sprites.push(sp)
        last_rect.set(widget.x, widget.y, widget.width, widget.height)

        # This fixes problems with the cursor being able to select 2 adjacent
        # widgets
        widget.surface.contract!(anchor: 5, amount: 1)
      end
    end

    _test_loop_(TEST_TIME_LONG) do
      widgets.each(&:update)
    end

    sprites.each do |s|
      s.dispose_all
    end

    console_sp.dispose_all
    title_sp.dispose
    background_sp.dispose_all

    widgets.each(&:dispose)

    return TEST_PASSED
  end

end
