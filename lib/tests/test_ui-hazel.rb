module TestCore

  class AnimEaser

    def ease(delta, ch)
      dst
    end

    def self.animeaser
      @animeaser ||= new
    end

    def self.ease(delta, ch)
      animeaser.ease(delta, ch)
    end

  end

  class AnimShake < AnimEaser

    attr_accessor :count

    def initialize
      @count = 3
      @easer = MACL::Easer::Linear
    end

    def ease(delta, ch)
      m = 1.0 / @count
      edelta = (delta % m) / m
      ch = -ch unless (delta * @count).to_i % 2 == 0
      if edelta < 0.5
        @easer.ease(edelta * 2, 0, ch, 1.0)
      else
        @easer.ease((edelta - 0.5) * 2, ch, 0, 1.0)
      end
    end

  end

  class Animator

    attr_reader :time
    attr_reader :timemax
    attr_reader :value
    attr_reader :animeaser

    def initialize(ch, time, animeaser)
      @value = 0
      @ch = ch
      @time = @timemax = time
      setup_easer(animeaser)
    end

    def setup_easer(obj)
      @animeaser = case obj
                   when Symbol              then TestCore::AnimEaser[obj]
                   when TestCore::AnimEaser then obj
                   else raise(TypeError,
                              "Expected type AnimEaser or Symbol but received #{obj}")
                   end
    end

    def update
      @time -= 1 if @time > 0
      @value = @animeaser.ease(1 - (@time / @timemax.to_f), @ch)
    end

    def reset
      @time = @timemax
    end

    def finish
      @time = 0
      update
    end

  end

  def self.test_ui_hazel
    return TEST_DISABLED unless Hazel::VERSION < "2.0.0"
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Hazel Test 1", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.y2 = title_sp.y
    help_sp.set_text("[ESC]: finish test | [Mouse over]: Grows box | [Mouse not over]: Shrink box")

    widget = Hazel::Widget.new(0, 0, 32, 32)
    sp = Sprite.new
    bmp = sp.bitmap = Bitmap.new(widget.width, widget.height)
    bmp.fill_rect(bmp.rect, Palette['sys1_orange'])
    sp.align_to!(anchor: 5, surface: canvas)
    sp.anchor_oxy!(anchor: 5)
    widget.x = sp.x
    widget.y = sp.y

    swease = Sweasel.new(1, 15, :back_in)
    shaker = Animator.new(16, 20, AnimShake.animeaser)

    org_x, org_y = sp.x, sp.y

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      widget.update
      sp.update
      swease.update
      shaker.update
      if widget.pos_in_area?(Mouse.x + sp.ox, Mouse.y + sp.oy)
        swease.target_value = 2
        if Mouse.left_press?()
          shaker.reset
        end
      else
        swease.target_value = 1
      end
      sp.zoom_x = sp.zoom_y = swease.value
      sp.x = org_x + shaker.value
      sp.y = org_y
    end

    [sp].each do |s|
      s.bitmap.dispose
      s.dispose
    end

    widget.dispose
    help_sp.dispose
    title_sp.dispose

    return TEST_PASSED
  end

end
