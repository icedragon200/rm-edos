#
# EDOS/core/test_core/test-ui-hazel_and_onyx_01.rb
#   by IceDragon
#   dm 03/04/2013
# vr 1.0.0
module TestCore

  def self.test_ui_hazel_and_onyx_01
    return TEST_DISABLED unless Hazel::VERSION < "2.0.0"
    background_plane = pln()
    background_plane.bitmap = Cache.picture("tactile_noise")
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Hazel and Onyx Test 1", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.y2 = title_sp.y
    help_sp.set_text("[ESC]: finish test")

    bounds = MACL::Surface.new(0, 0, gwidth, gheight - help_sp.height - title_sp.height)

    panel = Hazel::Panel.new(0, 0, 192, 64)
      panel.label = "Dialogue"
      panel.properties[:use_header] = true
    wgt_buttons = []
    spr_buttons = []

    stop_test = false

    button_ok = Hazel::Widget::Button.new(0, 0, 64, 16)
      button_ok.add_event_handle(:toggle,
                                  &Hazel::Default::EV_BUTTON_TOGGLE)

      button_ok.add_event_handle(:mouse_left_click) do
        Sound.play_ex('button')
        #stop_test = true
      end

      button_ok.label = 'OK'

    button_cancel = Hazel::Widget::Button.new(70, 0, 64, 16)
      button_cancel.add_event_handle(:toggle,
                                  &Hazel::Default::EV_BUTTON_TOGGLE)

      button_cancel.add_event_handle(:mouse_left_click,
                                  &Hazel::Default::EV_BUTTON_LEFT_CLICK)

      button_cancel.label = 'Cancel'

    checkbox = Hazel::Widget::Checkbox.new(0, 24, 16, 16)
      checkbox.add_event_handle(
        :mouse_left_click, &Hazel::Default::EV_BUTTON_LEFT_CLICK)

      checkbox.label = 'Some Checkbox'

    buttons = [button_ok, button_cancel]
    button_surf = panel.surface.contract(anchor: MACL::Surface::ANCHOR_MIDDLE_CENTER,
                                         amount: 12)
    anchor_surf = button_surf.dup
    MACL::Surface::Tool.anchor_surfaces(MACL::Surface::ANCHOR_BOTTOM_CENTER,
                                        button_surf, *buttons)
    MACL::Surface::Tool.anchor_surfaces(MACL::Surface::ANCHOR_TOP_LEFT,
                                        anchor_surf, checkbox)

    panel.set_widget(button_ok)
    panel.set_widget(button_cancel)
    panel.set_widget(checkbox)
    panel.refresh_widgets

    # GUI
    widgets = panel.widgets.keys
    components = [panel, *widgets]
    spriteset = Hazel::Onyx::Spriteset_Components.new(components).refresh

    panel.surface.bounds = bounds
    panel.surface.bound_do do |surf|
      surf.align_to!(anchor: MACL::Surface::ANCHOR_MIDDLE_CENTER)
    end

    grabbed = nil

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      help_sp.update
      if Mouse.right_click?
        if grabbed
          grabbed = nil
        elsif panel.pos_in_area?(Mouse.x, Mouse.y)
          surface = panel.surface
          grabbed = [surface.x - Mouse.x, surface.y - Mouse.y]
        end
      end
      panel.update
      spriteset.update
      if grabbed
        panel.x = Mouse.x + grabbed[0]
        panel.y = Mouse.y + grabbed[1]
      end
      break if stop_test
    end

    title_sp.dispose
    help_sp.dispose
    spriteset.dispose
    background_plane.dispose_all

    return TEST_PASSED
  end

end
