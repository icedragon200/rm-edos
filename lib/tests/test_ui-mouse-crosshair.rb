# Crosshair
class Crosshair

  def initialize
    v = Viewport.new(0, 0, 3, Graphics.height)
    @vertical_line   = [Plane.new(v),v]
    @vertical_line[0].bitmap = Bitmap.new(v.rect.width, v.rect.height)
    @vertical_line[0].bitmap.fill_rect(0,0,v.rect.width, v.rect.height, Palette['white'])
    v = Viewport.new(0, 0, Graphics.width, 3)
    @horizontal_line = [Plane.new(v),v]
    @horizontal_line[0].bitmap = Bitmap.new(v.rect.width, v.rect.height)
    @horizontal_line[0].bitmap.fill_rect(0, 0, v.rect.width, v.rect.height, Palette['white'])
  end

  def dispose
    [@horizontal_line[0].bitmap,
     @vertical_line[0].bitmap,
     *(@vertical_line + @horizontal_line)].each(&:dispose)
  end

  def xy
    return @vertical_line[1].rect.x, @horizontal_line[1].rect.y
  end

  def moveto(x, y)
    @vertical_line[1].rect.x = x
    @horizontal_line[1].rect.y = y
  end

  def update
    @vertical_line[1].update
    @horizontal_line[1].update
    #@vertical_line
    #@horizontal_line
  end

end

module TestCore

  def self.test_ui_mouse_crosshair
    canvas = Screen.content
    title_sp = Sprite::MerioTitle.new(nil, "Cross Hair Test", 0)
    title_sp.align_to!(anchor: 2, surface: canvas)
    help_sp = Sprite::MerioHelp.new(nil)
    help_sp.y2 = title_sp.y
    help_sp.set_text("[ESC]: finish test | [Right Click] Move crosshair to mouse")

    ch = Crosshair.new
    tweener = MACL::Tween.new([0, 0], [0, 0], :linear, 1.0)

    _test_loop_(TEST_TIME_LONG) do
      title_sp.update
      help_sp.update
      tweener.update
      if Mouse.right_click?
        tweener.set_and_reset(
          ch.xy, [Mouse.x, Mouse.y], :quad_out, MACL::Tween.frame_to_sec(30))
      end
      ch.moveto(*tweener.values)
    end

    title_sp.dispose
    help_sp.dispose
    ch.dispose

    return TEST_PASSED
  end

end
