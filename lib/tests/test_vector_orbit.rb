module TestCore

  def self.test_vector_orbit
    vec2 = StarRuby::Vector2F.new(0, 0)
    vel = StarRuby::Vector2F.new(2, 0)

    sp = Sprite.new(nil)
    sp.bitmap = Cache.system("ball")
    sp.anchor_oxy!(anchor: 5)

    sp_rad = Sprite::TextStrip.new(nil, Graphics.width, 24)

    trad = 0
    ticks = 0
    orbit = 0

    _test_loop_(TEST_TIME_LONG) do
      vel.radian = Math.atan2(Mouse.y - vec2.y, Mouse.x - vec2.x)
      sp_rad.set_text(vel.radian.round(4), 1) if ticks % 5 == 0
      dist = MACL::Vector::Tool.distance2(Mouse, vec2)
      if dist > 32
        vec2 += vel
        orbit = 0
      elsif dist < 28
        vec2 -= vel
        orbit = 0
      else
        orbit += 1
        ang = (orbit * 2 % 360) / 360 * Math::PI
        vec2.x = Mouse.x + Math.cos(ang) * 32
        vec2.y = Mouse.y + Math.sin(ang) * 32
      end
      sp.x = vec2.x.to_i
      sp.y = vec2.y.to_i
      ticks += 1
    end

    sp.dispose
    sp_rad.dispose

    return TEST_PASSED
  end

end
