#
# EDOS/core/test_core/test-voxel_editor.rb
# vr 1.0.0
module TestCore

  NULL = 0

  def self.test_voxel_editor
    background_plane = pln()
    background_plane.bitmap = Cache.picture("tactile_noise")

    colors = []
    mul = 0xFF / 3
    for r in 0..3
      for g in 0..3
        for b in 0..3
          colors.push(Color.new(r * mul, g * mul, b * mul, 0xFF))
        end
      end
    end
    colors.uniq!

    mtx = StarRuby::MatrixI.new([10, 10, 10], 0)

    pos  = StarRuby::Vector3I.new
    size = StarRuby::Vector3I.new
    size.x, size.y, size.z = mtx.dimensions

    color_limit = colors.size
    bitsize = 8
    plan  = Sprite.new(nil)
    side  = Sprite.new(nil)
    front = Sprite.new(nil)
    pall  = Sprite.new(nil)

    plan.bitmap  = Bitmap.new(size.x * bitsize,
                              size.y * bitsize)
    side.bitmap  = Bitmap.new(size.y * bitsize,
                              size.z * bitsize)
    front.bitmap = Bitmap.new(size.x * bitsize,
                              size.z * bitsize)

    pall.bitmap  = Bitmap.new(10 * bitsize, 10 * bitsize)

    bit_rect = ->(orn, x, y, z) do
      case orn
      when 0 # plan
        Rect.new(x * bitsize, y * bitsize, bitsize, bitsize)
      when 1 # side
        Rect.new(y * bitsize, z * bitsize, bitsize, bitsize)
      when 2 # front
        Rect.new(x * bitsize, z * bitsize, bitsize, bitsize)
      end
    end

    render_plan = -> do
      plan.bitmap.clear
      z = pos.z
      for y in 0...size.y
        for x in 0...size.x
          n = mtx[x, y, z]
          plan.bitmap.fill_rect(bit_rect.(0, x, y, z), colors[n])
        end
      end
    end

    render_side = -> do
      side.bitmap.clear
      x = pos.x
      for z in 0...size.z
        for y in 0...size.y
          n = mtx[x, y, z]
          plan.bitmap.fill_rect(bit_rect.(1, x, y, z), colors[n])
        end
      end
    end

    render_front = -> do
      front.bitmap.clear
      y = pos.y
      for z in 0...size.z
        for x in 0...size.x
          n = mtx[x, y, z]
          plan.bitmap.fill_rect(bit_rect.(2, x, y, z), colors[n])
        end
      end
    end

    render_all = -> do
      render_plan.()
      render_side.()
      render_front.()
    end

    for i in 0...colors.size
      pall.bitmap.fill_rect(bit_rect.(0, i % 10, i / 10, NULL), colors[i])
    end

    pall.align_to!(anchor: MACL::Surface::ANCHOR_TOP_RIGHT)

    plan.align_to!(anchor: MACL::Surface::ANCHOR_TOP_LEFT)
    side.x = plan.x
    side.y = plan.y2
    front.x = plan.x2
    front.y = plan.y

    _test_loop_(TEST_TIME_LONG) do
      render_all.() if Input.trigger?(:C)
    end

    return TEST_PASSED
  end

end
