dir = File.dirname(__FILE__)
Dir.glob(File.join(dir, 'ui', '*.rb')).each do |fn|
  require fn
end

module UI
  class BaseGauge < UI::MerioGauge
    def initialize(*args)
      super(*args)
      self.orientation = :vertical
    end

    def gauge_base_size
      @gauge_base_size ||= Size2.new(4, 32)
    end
  end
  class HpGauge < BaseGauge
    def gauge_colors
      @gauge_colors ||= DrawExt.gauge_palettes['hp']
    end
  end
  class MpGauge < BaseGauge
    def gauge_colors
      @gauge_colors ||= DrawExt.gauge_palettes['mp']
    end
  end
end