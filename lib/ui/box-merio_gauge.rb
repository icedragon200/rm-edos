#
# EDOS/lib/merio/box-merio_gauge.rb
#   by IceDragon
module UI
  class MerioGauge < Lacio::SpaceBox

    ### instance_attributes
    attr_reader :viewport
    attr_reader :rate
    attr_writer :gauge_bar_size
    attr_writer :gauge_base_size
    attr_writer :gauge_bar_offset
    attr_accessor :orientation
    attr_accessor :bar_align

    def initialize(viewport)
      @viewport = viewport
      @orientation = :horizontal
      @bar_align = 2
      @gauge_base = Sprite.new(@viewport)
      @gauge_bar  = Sprite.new(@viewport)
      @gauge_timer = Timer.new(15).zero
      @current_rate = @last_rate = @last_rate_abs = @rate = 0.0
      @gauge_base.rel_z = 0
      @gauge_bar.rel_z = 1
      super([@gauge_base, @gauge_bar])
    end

    def ox
      @gauge_base.ox
    end

    def oy
      @gauge_base.oy
    end

    def trate
      (@last_rate + (@last_rate_abs - @last_rate) * @gauge_timer.rate)
    end

    def rate=(new_rate)
      @rate = [[new_rate.to_f, 1.0].min, 0.0].max
    end

    def viewport=(new_viewport)
      @viewport = new_viewport
      @gauge_base.viewport = @viewport
      @gauge_bar.viewport = @viewport
    end

    def gauge_colors=(new_palette)
      @gauge_colors = Convert.Palette(new_palette)
    end

    def resize(width, height)
      self.gauge_base_size.set!([width, height])
      self.gauge_bar_size = nil
      self.gauge_bar_offset = nil
    end

    def gauge_base_size
      @gauge_base_size ||= Size2.new(128, 6)
    end

    def gauge_bar_size
      @gauge_bar_size ||= gauge_base_size - [2, 2]
    end

    def gauge_bar_offset
      @gauge_bar_offset ||= Point2.new(*((gauge_base_size - gauge_bar_size) / 2))
    end

    def gauge_colors
      @gauge_colors ||= DrawExt.gauge_palettes['default']
    end

    def dispose_gauge_base
      @gauge_base.dispose_all
    end

    def dispose_gauge_bar
      @gauge_bar.dispose_all
    end

    def dispose
      dispose_gauge_base
      dispose_gauge_bar
      super
    end

    def refresh_gauge_base
      DrawExt.draw_gauge_base(@gauge_base.bitmap, colors: gauge_colors)
    end

    def refresh_gauge_bar
      DrawExt.draw_gauge_bar(@gauge_bar.bitmap, rate: 1.0, colors: gauge_colors)
    end

    def refresh_rate
      @current_rate = trate
      case @orientation
      when :horizontal
        @gauge_bar.src_rect.width = @gauge_bar.bitmap.width * @current_rate
      when :vertical
        @gauge_bar.src_rect.height = @gauge_bar.bitmap.height * @current_rate
      end
    end

    def refresh_if_changed
      if @last_rate_abs != r=rate
        @last_rate = @current_rate #@last_rate_abs
        @last_rate_abs = r
        @gauge_timer.reset
      end
    end

    def update_bitmap
      gbs = gauge_base_size
      gbr = gauge_bar_size
      r_bar  = false
      r_base = false
      if !@gauge_base.bitmap || gbs.width != @gauge_base.bitmap.width ||
                                gbs.height != @gauge_base.bitmap.height
        @gauge_base.dispose_bitmap_safe
        @gauge_base.bitmap = Bitmap.new(*gbs)
        r_base = true
      end
      if !@gauge_bar.bitmap || gbr.width != @gauge_bar.bitmap.width ||
                               gbr.height != @gauge_bar.bitmap.height
        @gauge_bar.dispose_bitmap_safe
        @gauge_bar.bitmap  = Bitmap.new(*gbr)
        r_bar = true
      end
      if @last_gauge_colors != gcs = gauge_colors
        @last_gauge_colors = gcs
        r_base = r_bar = true
      end
      refresh_gauge_base if r_base
      refresh_gauge_bar  if r_bar
      recalc_size
    end

    def update_gauge
      refresh_rate unless @gauge_timer.idle?
      @gauge_timer.update
    end

    def update_offset
      gbo = gauge_bar_offset
      case @orientation
      when :horizontal
        @gauge_base.ox = @gauge_base.bitmap.width / 2
        @gauge_bar.ox  = @gauge_base.ox - gbo.x
        @gauge_bar.oy  = -gbo.y
        case @bar_align
        #when 0 then @gauge_bar.ox
        when 1 then @gauge_bar.ox -= (@gauge_bar.bitmap.width - @gauge_bar.width) / 2
        when 2 then @gauge_bar.ox -= @gauge_bar.bitmap.width - @gauge_bar.width
        end
      when :vertical
        @gauge_base.oy = @gauge_base.bitmap.height / 2
        @gauge_bar.oy  = @gauge_base.oy - gbo.y
        @gauge_bar.ox  = -gbo.x
        case @bar_align
        #when 0 then @gauge_bar.oy
        when 1 then @gauge_bar.oy -= (@gauge_bar.bitmap.height - @gauge_bar.height) / 2
        when 2 then @gauge_bar.oy -= @gauge_bar.bitmap.height - @gauge_bar.height
        end
      end
    end

    def update
      super
      update_bitmap
      update_gauge
      update_offset
      refresh_if_changed
    end

  end
end