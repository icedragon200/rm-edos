#
# EDOS/lib/merio/box-merio_scrollbar.rb
#   by IceDragon
module UI
  class MerioScrollBar < Lacio::SpaceBox

    ### instance_attributes
    attr_reader :viewport
    attr_accessor :padding
    attr_accessor :index
    attr_accessor :index_max
    attr_accessor :orientation
    attr_reader :visible

    ##
    # initialize(Viewport viewport, int width, int height, int padding)
    def initialize(viewport, width, height, orientation=0, padding=0)
      @viewport = viewport
      @width_abs, @height_abs = width, height
      @padding = padding
      @visible = true
      @index = -1
      @index_max = 0
      @orientation = orientation
      init_sprites
      init_easer
      refresh_bitmaps
      super([@scrollbar_base_sp, @scrollbar_handle_sp, @scrollbar_index_sp])
      @scrollbar_base_sp.rel_z   = 0
      @scrollbar_handle_sp.rel_z = 1
      @scrollbar_index_sp.rel_z  = 2
      refresh_bodies_pos
      ### refresh internals
      self.viewport = @viewport
      self.visible  = @visible
    end

    def horizontal?
      @orientation == 1
    end

    def vertical?
      @orientation == 0
    end

    ##
    # viewport=(Viewport new_viewport)
    def viewport=(new_viewport)
      @viewport = new_viewport
      @scrollbar_base_sp.viewport   = @viewport
      @scrollbar_handle_sp.viewport = @viewport
      @scrollbar_index_sp.viewport  = @viewport
    end

    ##
    # visible=(Boolean new_visible)
    def visible=(new_visible)
      @visible = !!new_visible
      @scrollbar_base_sp.visible   = @visible
      @scrollbar_handle_sp.visible = @visible
      @scrollbar_index_sp.visible  = @visible
    end

    ##
    # init_sprites
    def init_sprites
      @scrollbar_base_sp   = Sprite.new(@viewport)
      @scrollbar_handle_sp = Sprite.new(@viewport)
      @scrollbar_index_sp  = Sprite.new(@viewport) # bounded to the handle
    end

    ##
    # init_easer
    def init_easer
      tick = Metric::Time.sec_to_frame(0.11666666666666667)
      easer = :sine_in
      @scroll_tween = MACL::Tween2.new(tick, easer, [0, 1])
    end

    ##
    # dispose_bitmaps
    def dispose_bitmaps
      @scrollbar_base_sp.dispose_bitmap_safe
      @scrollbar_handle_sp.dispose_bitmap_safe
      @scrollbar_index_sp.dispose_bitmap_safe
      @scrollbar_base_sp.bitmap   = nil
      @scrollbar_handle_sp.bitmap = nil
      @scrollbar_index_sp.bitmap  = nil
    end

    ##
    # dispose
    def dispose
      dispose_bitmaps
      @scrollbar_base_sp.dispose
      @scrollbar_handle_sp.dispose
      @scrollbar_index_sp.dispose
      super
    end

    ##
    # refresh_bitmaps
    def refresh_bitmaps
      dispose_bitmaps
      pad = @padding
      w1 = @width_abs
      h1 = @height_abs
      if vertical?
        w2 = w1 - pad * 2
        h2 = (h1 - pad * 2) / @index_max.max(1)
        w3 = w2
        h3 = Metric.ui_element_sml
      elsif horizontal?
        w2 = (w1 - pad * 2) / @index_max.max(1)
        h2 = (h1 - pad * 2)
        w3 = Metric.ui_element_sml
        h3 = h2
      end
      b1 = @scrollbar_base_sp.bitmap   = Bitmap.new(w1, h1)
      b2 = @scrollbar_handle_sp.bitmap = Bitmap.new(w2, h2)
      b3 = @scrollbar_index_sp.bitmap  = Bitmap.new(w3, h3)

      b1.merio.draw_dark_rect(b1.rect)
      b2.merio.draw_light_rect(b2.rect)

      @scrollbar_handle_sp.ox = -pad
      @scrollbar_handle_sp.oy = -pad
    end

    ##
    # refresh_index
    def refresh_index
      if @index != @old_index
        @old_index = @index
        @tab_half = @index >= @index_max / 2
        ###
        @scrollbar_index_sp.bitmap.clear
        b3 = @scrollbar_index_sp.bitmap
        b3.merio.draw_dark_rect(b3.rect)
        b3.merio.font_config(:light, :micro, :enb)
        # 1 is added to the @index, since the index start at 0
        # without the 1 added, we would end up with something like 0/1 instead
        # of 1/1
        r = b3.rect.dup
        if vertical?
          r.width /= 3
          b3.draw_text(r, "%d" % (@index + 1), 1)
          b3.merio.draw_light_rect(r.step(6))
          #b3.draw_text(r.step(6), "|", 1)
          b3.draw_text(r.step(6, 2), "%d" % @index_max, 1)
          @scrollbar_index_sp.x = @scrollbar_handle_sp.visx
          old_target = @scrollbar_handle_sp.rel_y
          @scroll_tween.setup_pairs([old_target, @index * @scrollbar_handle_sp.height])
        elsif horizontal?
          r.height /= 3
          b3.draw_text(r, "%d" % (@index + 1), 1)
          b3.merio.draw_light_rect(r.step(2))
          #b3.draw_text(r.step(2), "---", 1)
          b3.draw_text(r.step(2, 2), "%d" % @index_max, 1)
          @scrollbar_index_sp.y = @scrollbar_handle_sp.visy
          old_target = @scrollbar_handle_sp.rel_x
          @scroll_tween.setup_pairs([old_target, @index * @scrollbar_handle_sp.width])
        end
        @scroll_tween.reset_tick
      end
      self
    end

    ##
    # set_index(int new_index)
    def set_index(new_index)
      if @index != new_index
        @index = new_index
        refresh_index
      end
    end

    ##
    # set_index_max(int new_index_max)
    def set_index_max(new_index_max)
      if @index_max != new_index_max
        @index_max = new_index_max
        refresh_bitmaps
        refresh_index
      end
    end

    ##
    # update
    def update
      super
      if @scroll_tween.active?
        @scroll_tween.update
        if vertical?
          @scrollbar_handle_sp.rel_y, = @scroll_tween.result
          refresh_body_pos(@scrollbar_handle_sp)
          @scrollbar_index_sp.rel_x = @scrollbar_handle_sp.rel_x - @scrollbar_handle_sp.ox
          if @tab_half
            @scrollbar_index_sp.rel_y = @scrollbar_handle_sp.rel_y - @scrollbar_handle_sp.oy - @scrollbar_index_sp.height
          else
            @scrollbar_index_sp.rel_y = @scrollbar_handle_sp.rel_y + @scrollbar_handle_sp.height - @scrollbar_handle_sp.oy
          end
          refresh_body_pos(@scrollbar_index_sp)
        elsif horizontal?
          @scrollbar_handle_sp.rel_x, = @scroll_tween.result
          refresh_body_pos(@scrollbar_handle_sp)
          @scrollbar_index_sp.rel_y = @scrollbar_handle_sp.rel_y - @scrollbar_handle_sp.oy
          if @tab_half
            @scrollbar_index_sp.rel_x = @scrollbar_handle_sp.rel_x - @scrollbar_handle_sp.ox - @scrollbar_index_sp.width
          else
            @scrollbar_index_sp.rel_x = @scrollbar_handle_sp.rel_x + @scrollbar_handle_sp.width - @scrollbar_handle_sp.ox
          end
          refresh_body_pos(@scrollbar_index_sp)
        end
      end
      @scrollbar_base_sp.update
      @scrollbar_handle_sp.update
      @scrollbar_index_sp.update
    end

  end
end