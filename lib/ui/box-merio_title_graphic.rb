#
# EDOS/lib/merio/box-merio_title_graphic.rb
#   by IceDragon
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.0.0
module UI
  class MerioTitleGraphic < Lacio::SpaceBox

    ### instance_aatributes
    attr_reader :viewport

    ##
    # initialize(Viewport viewport)
    def initialize(viewport=nil)
      @viewport = nil
      init_graphic
      super([@title_sp, @highlight_sp, @subtitle_sp, @book_sp, @spacer_sp])
      @idle_tick = @highlight_tween.tickmax * 3
      @ticks = 0
    end

    ##
    # init_graphic
    def init_graphic
      ### font_config
      main_title = DrawExt::Merio.new_font(:oqlight, :tera, :enb)
      sub_title  = DrawExt::Merio.new_font(:oqlight, :h6, :enb)
      sub_title.italic = true
      subtext    = DrawExt::Merio.new_font(:light, :small, :enb)
      ### config
      almask = StarRuby::Texture::MASK_ALPHA
      title_str    = "Earthen"
      ## BookI: "Earthen Zero" Palette['droid_red_light']
      ##   Cluster1: "Sunrise", Cluster2: "Noon's Turn"
      ##   Cluster3: "Raeven", Cluster4: "Lunade"
      #book_str     = "Book I"
      #subtitle_str = "Sunrise"
      #sub_color    = Palette['droid_red_light']
      ## BookII: "Earthen" Palette['droid_blue']
      ##   Cluster1: "Dawn of Smiths", Cluster2: "Finite Daybreak"
      ##   Cluster3: "Running Dusk", Cluster4: "Night Sonata"
      book_str     = "Book II"
      subtitle_str = "Dawn of Smiths"
      sub_color    = Palette['droid_blue']
      ## BookIII: "Earthen Tau" Palette['droid_green_light']
      ##   Cluster1: "Wake Up", Cluster2: "Evening Space"
      ##   Cluster3: "Eternal", Cluster4: "Midnight"
      #book_str     = "Book III"
      #subtitle_str = "Wake Up"
      #sub_color    = Palette['droid_green_light']

      sub_title.color = sub_color
      ### bitmaps
      title_bmp     = Bitmap.render_text(main_title, title_str)
      highlight_bmp = Bitmap.new(title_bmp.width, title_bmp.height)
      subtitle_bmp  = Bitmap.render_text(sub_title, subtitle_str)
      book_bmp      = Bitmap.render_text(subtext, book_str)
      spacer_bmp    = Bitmap.new(title_bmp.width, 1)

      ### modify_bitmap
      highlight_bmp.fill(sub_color)
      highlight_bmp.texture.mask(0, 0, almask,
                                 title_bmp.texture, title_bmp.rect, almask)
      spacer_bmp.fill(Palette['white'])

      ### sprites
      @title_sp      = Sprite.new(@viewport)
      @highlight_sp  = Sprite.new(@viewport)
      @subtitle_sp   = Sprite.new(@viewport)
      @book_sp       = Sprite.new(@viewport)
      @spacer_sp     = Sprite.new(@viewport)

      @title_sp.bitmap      = title_bmp
      @highlight_sp.bitmap  = highlight_bmp
      @subtitle_sp.bitmap   = subtitle_bmp
      @book_sp.bitmap       = book_bmp
      @spacer_sp.bitmap     = spacer_bmp

      ### position_setup
      oy = 16
      # x
      @book_sp.x = @title_sp.x
      @subtitle_sp.x2 = @title_sp.x2
      # y
      @book_sp.y      = @title_sp.y2 - oy
      @spacer_sp.y    = @title_sp.y2 - oy
      @subtitle_sp.y  = @title_sp.y2 - oy
      # z
      @highlight_sp.z = 1

      init_highlight_tweener
    end

    ##
    # init_highlight_tweener
    def init_highlight_tweener
      @highlight_sp.src_rect.width *= 0.15
      @hw = @highlight_sp.src_rect.width
      @highlight_sp.src_rect.x -= @highlight_sp.src_rect.width
      pair = [@highlight_sp.src_rect.x, @highlight_sp.bitmap.width] #+ hw]
      t = Graphics.frame_rate * 2
      @highlight_tween = MACL::Tween2.new(t, :linear, pair)
    end

    ##
    # idle -> bool
    def idle
      @idle_tick -= 1
      @idle_tick > 0
    end

    ##
    # dispose
    def dispose
      @title_sp.dispose_all
      @highlight_sp.dispose_all
      @subtitle_sp.dispose_all
      @book_sp.dispose_all
      @spacer_sp.dispose_all
      super
    end

    ##
    # update
    def update
      super
      if @highlight_tween.active?
        @highlight_tween.update
        @highlight_sp.src_rect.x, = @highlight_tween.result
        @highlight_sp.src_rect.width = @hw - (-@highlight_sp.src_rect.x).max(0)
        @highlight_sp.src_rect.x = @highlight_sp.src_rect.x.max(0)
        @highlight_sp.rel_x, = @highlight_sp.src_rect.x
        refresh_body_pos(@highlight_sp)
      end

      @spacer_sp.opacity     = Palila::Util.reflect(@ticks, 128, 256)
      @book_sp.opacity       = Palila::Util.reflect((@ticks + 64) / 4, 128, 256)
      @subtitle_sp.opacity   = Palila::Util.reflect(@ticks / 4, 198, 256)

      if !@highlight_tween.active?
        unless idle
          @highlight_tween.reset_tick
          @idle_tick = @highlight_tween.tickmax * 3
        end
      end

      @title_sp.update
      @highlight_sp.update
      @subtitle_sp.update
      @book_sp.update
      @spacer_sp.update

      @ticks += 1
    end

  end
end