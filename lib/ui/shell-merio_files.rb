#
# EDOS/lib/merio/shell-merio_files.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 23/06/2013
#   dm 23/06/2013
# vr 1.0.0
#   Merio styled File Selection window, inspired by Riviera the promised land
module UI
  class ShellMerioFiles < Hazel::Shell::WindowSelectable

    ##
    # initialize(int x, int y, int width, int height)
    def initialize(x, y, width, height)
      super(x, y + Metric.ui_element_sml, width, height)
      refresh
      activate
    end

    def make_decorations
      deco = add_decoration(:title)
      deco.set_text("Files")
    end

    ##
    # standard_padding
    def standard_padding
      0
    end

    ##
    # spacing
    def spacing
      0
    end

    ##
    # col_max
    def col_max
      2
    end

    ##
    # item_max
    def item_max
      DataManager.savefile_max
    end

    ##
    # item_height
    def item_height
      Metric.ui_element_sml * 5
    end

    ##
    # load_headers
    def load_headers
      @header = []
      item_max.times { |i| @header[i] = DataManager.load_header(i) }
    end

    ##
    # draw_item(int index)
    def draw_item(index)
      rect = item_rect(index)
      header = @header[index]
      art = artist()
      ###
      rect_title = rect.dup
      rect_title.height = Metric.ui_element_sml * 2
      rect_content = rect.dup
      rect_content.height -= rect_title.height
      rect_content.y = rect_title.y2
      rect_file_label = rect_title.contract(anchor: 46, amount: Metric.contract)
      rect_playtime_text = rect_file_label.dup
      rect_character = rect_content.dup
      space_factor = Metric.ui_element
      rect_character.height = 32
      rect_character.width = space_factor * 3 + spacing * 2
      rect_character.align_to!(anchor: 4, surface: rect_content)
      rect_character.x += spacing # push the rectangle over to mimic centering
      rect_character.x += Metric.contract # padding offset
      # character offset
      rect_character.x += 16
      rect_character.y += 32
      ###
      label_s = "File: "
      enabled = !!header

      ###
      merio = DrawExt::Merio::DrawContext.new(contents)
      merio.font_config(:dark, :large, enabled)

      merio.snapshot do |mer|
        merio.active_state = enabled
        merio.draw_light_rect(rect_title)
        merio.draw_dark_rect(rect_content)
      end

      contents.font.snapshot do
        w = contents.text_size(label_s).width
        merio.font_config(:dark, :default, enabled)
        merio.color_state_mod(contents.font.color, enabled)
        contents.draw_text(rect_file_label, label_s, 0)
        rect_file_text = rect_file_label.dup
        rect_file_text.x += w
        rect_file_text.width -= w
        contents.font.color = Palette["droid_blue"]
        merio.color_state_mod(contents.font.color, enabled)
        contents.draw_text(rect_file_text, (index + 1).to_s, 0)
      end

      if header
        playtime_s = header[:playtime_s]
        contents.font.snapshot do
          contents.draw_text(rect_playtime_text, playtime_s, 2)
        end
        rect_playtime_label = rect_playtime_text.dup
        rect_playtime_label.width -= contents.text_size(playtime_s).width
        contents.font.snapshot do
          contents.font.color = Palette["droid_blue"]
          contents.draw_text(rect_playtime_label, "Playtime: ", 2)
        end

        x = rect_character.x
        y = rect_character.y
        header[:characters].each_with_index do |(char_name, char_index), i|
          art.draw_character(char_name, char_index, x + i * space_factor, y)
        end

        if snap = header[:snapshot]
          # implement later
        end
      end
    end

    ##
    # refresh
    def refresh
      load_headers
      contents.clear
      draw_all_items
    end

  end
end