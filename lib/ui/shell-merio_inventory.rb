#
# EDOS/lib/merio/shell-merio_inventory.rb
#   by IceDragon (mistdragon100@gmail.com)
module UI
  class ShellMerioInventory < ShellMerioItemList

    def make_item_list
      @items = @unit ? @unit.all_items : []
    end

    def change_palette_color_by_item(merio, item)
      case ExDatabase.obj_symbol_abs(item)
      when :nil       then merio.change_main_palette('black')
      when :ex_armor  then merio.change_main_palette('purple')
      when :armor     then merio.change_main_palette('blue')
      when :item      then merio.change_main_palette('clay')
      when :ex_weapon then merio.change_main_palette('orange')
      when :weapon    then merio.change_main_palette('red')
      else                 merio.change_main_palette('default')
      end
    end

    ##
    # draw_item(Integer index)
    def draw_item(index)
      ### shorthand
      bmp = contents
      ### rect
      rect = item_rect(index)
      rect_content = rect.contract(anchor: 5, amount: Metric.contract)
      rect_icon = rect_content.dup
      rect_icon.width = rect_icon.height = 24
      rect_num = rect_content.dup
      rect_num.width -= rect_icon.width
      rect_num.height = 24
      rect_num.x += rect_icon.width
      rect_name = rect_content.dup
      if (rect.height - rect_icon.height) > 16
        rect_name.height -= 24
        rect_name.y += 24
      else
        rect_name.x = rect_icon.x2 + 4
        rect_name.width -= (rect_icon.x2 - rect.x) - 8
        rect_name.y = rect_icon.y2 - 16
      end
      ###
      item = items[index]
      ###
      item_number_s = "x %d" % @unit.item_number(item)
      item_name = item ? item.name : ""

      ### drawing
      artist do |art|
        art.merio.snapshot do |mer|
          # preparation
          mer.change_main_palette(mer.main_palette)
          mer.draw_light_rect(rect)
          ## draw_icon
          # this sections draws the small rectangle behind the icon
          mer.snapshot do |sub_mer|
            change_palette_color_by_item(sub_mer, item)
            sub_mer.fill_type = :blend_round
            sub_mer.draw_dark_rect(rect_icon)
          end
          if item
            art.draw_item_icon(item, rect_icon.x, rect_icon.y)
            mer.font_config(:dark, :micro, :enb)
            art.draw_text(rect_num, item_number_s, 2)
            mer.font_config(:dark, :micro, :enb)
            art.draw_text(rect_name, item_name, 0)
          end
        end # merio
      end # artist
    end

  end
end