#
# EDOS/lib/merio/shell-merio_item_list.rb
#   by IceDragon (mistdragon100@gmail.com)
module UI
  class ShellMerioItemList < Hazel::Shell::WindowSelectable

    ### mixins
    #include Hazel::Shell::Addons::Background
    include Mixin::UnitHost

    ##
    # standard_padding
    def standard_padding
      0 #Metric.contract
    end

    ##
    # spacing
    def spacing
      0
    end

    ## Mixin::UnitHost
    # on_unit_change
    def on_unit_change
      refresh
    end

    def items
      @items || []
    end

    def item(index=self.index)
      items[index]
    end

    def item_max
      vc = visible_cols
      wrp = items.size % vc
      [items.size - wrp + (wrp > 0 ? vc : 0), vc * visible_rows].max
    end

    def col_max
      (self.width - padding * 2) / item_width
    end

    def item_width
      Metric.ui_element * 2
    end

    def item_height
      Metric.ui_element_sml * 2
    end

    def make_item_list
      @items = []
    end

    def refresh
      make_item_list
      create_contents
      draw_all_items
      call_update_help
    end

    def change_palette_color_by_item(merio, item)

    end

    ##
    # draw_item(Integer index)
    def draw_item(index)

    end

    def update_help
      super
      @help_window.set_item(item) if @help_window
    end

  end
end