#
# EDOS/lib/merio/shell-merio_menu_status.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc ??/??/2013
#   dm 09/06/2013
# vr 1.1.0
module UI
  class ShellMerioMenuStatus < Hazel::Shell::Window

    include Hazel::Shell::Addons::Background
    include Mixin::UnitHost

    def on_unit_change
      refresh
    end

    def standard_padding
      return Metric.padding
    end

    def dispose
      @transition.dispose if @transition && !@transition.disposed?
      dispose_swap_bitmaps
      super
    end

    def dispose_swap_bitmaps
      @bitmap_swap_source.dispose if @bitmap_swap_source && !@bitmap_swap_source.disposed?
      @bitmap_swap_target.dispose if @bitmap_swap_source && !@bitmap_swap_target.disposed?
      @bitmap_swap_source = nil
      @bitmap_swap_target = nil
    end

    def refresh
      dispose_swap_bitmaps
      @bitmap_swap_source = contents.dup
      bmp = contents
      font = bmp.font

      spacing = 2 #Metric.spacing
      merio = artist.merio
      merio.snapshot
      merio.font_size = :micro

      ### base_rect_setup
      rect_contents = bmp.rect.dup
      rect = Rect.new(0, 0, bmp.width, Metric.ui_element)
      nrect = rect.contract(anchor: MACL::Surface::ANCHOR_CENTER,
                            amount: Metric.contract)

      gw = nrect.width / 2 - spacing #Metric.ui_element_sml * 8 # gauge width
      gh = Metric.ui_element_sml     # gauge height
      pw = gw / 4                    # param width
      ph = Metric.ui_element_sml     # param height
      lw = gw / 4                    # label width
      ### draw_rect_setup
      rect_info_label = Rect.new(nrect.x, nrect.y2, gw, gh)
        rect_info_label.y += Metric.spacing

      rect_element_pair = Rect.new(rect_info_label.x, rect_info_label.y2, gw, gh)
      rect_class_pair = Rect.new(rect_element_pair.x, rect_element_pair.y2, gw, gh)
      rect_level_pair = Rect.new(rect_class_pair.x, rect_class_pair.y2, gw, gh)

      rect_status_label = Rect.new(rect_info_label.x2 + spacing * 2, nrect.y2, gw, gh)
        rect_status_label.y += Metric.spacing

      rect_hp = Rect.new(rect_status_label.x, rect_status_label.y2, gw, gh)
      rect_mp = Rect.new(rect_hp.x, rect_hp.y2, gw, gh)
      rect_xp = Rect.new(rect_mp.x, rect_mp.y2, gw, gh)

      rect_element_label = rect_element_pair.dup
        rect_element_label.width = lw
      rect_elementname_label = rect_element_pair.dup
        rect_elementname_label.width -= rect_element_label.width
        rect_elementname_label.x += rect_element_label.width

      rect_class_label = rect_class_pair.dup
        rect_class_label.width = lw
      rect_classname_label = rect_class_pair.dup
        rect_classname_label.width -= rect_class_label.width
        rect_classname_label.x += rect_class_label.width

      rect_level_label = rect_level_pair.dup
        rect_level_label.width = lw
      rect_level_i_label = rect_level_pair.dup
        rect_level_i_label.width -= rect_level_label.width
        rect_level_i_label.x += rect_level_label.width

      rect_hp_label = rect_hp.dup
      rect_hp_label.width = lw

      rect_mp_label = rect_mp.dup
      rect_mp_label.width = lw

      rect_xp_label = rect_xp.dup
      rect_xp_label.width = lw

      rect_hp.x += lw
      rect_mp.x += lw
      rect_xp.x += lw
      rect_hp.y = rect_hp_label.y = rect_hp_label.y #+ spacing
      rect_mp.y = rect_mp_label.y = rect_mp_label.y #+ spacing
      rect_xp.y = rect_xp_label.y = rect_xp_label.y #+ spacing

      rect_hp.width -= lw - spacing
      rect_mp.width -= lw - spacing
      rect_xp.width -= lw - spacing

      #rect_hp.height /= 2
      #rect_mp.height /= 2
      #rect_xp.height /= 2

      ### icon rect # used to determine the distance to space other elements using icons
      _rct = DrawExt.calc_border_rect([0, 0, 24, 24])

      rect_gauge =
        MACL::Surface::Tool.area_rect(rect_hp, rect_mp, rect_xp,
                                      rect_hp_label, rect_mp_label, rect_xp_label)

      rect_param_label = Rect.new(nrect.x, rect_gauge.y2 + spacing, gw, ph)
      rect_param = Rect.new(nrect.x, rect_param_label.y2, pw, ph)

      rect_arts_label = Rect.new(rect_gauge.x, rect_gauge.y2 + spacing, gw, ph)

      rect_equip_label = Rect.new(nrect.x, rect_param.y2 + rect_param.height * 2 + spacing,
                                  nrect.width, ph)
      rect_state_label = Rect.new(nrect.x, rect_equip_label.y2 + spacing + _rct.height,
                                  rect_equip_label.width, rect_equip_label.height)

      ###
      name = @unit ? @unit.name : ""
      entity = @unit ? @unit.entity : nil

      artist do |art|
        # preparation
        bmp.clear
        font.snapshot

        # setup
        merio.font_config(:light, :h1, :enb)

        # drawing
        bmp.draw_text(nrect, name, 1)

        a_center = MACL::Surface::ALIGN_CENTER

        ## draw_header labels
        merio.snapshot do
          merio.font_size = :medium
          merio.draw_light_label(rect_info_label,   "Info",       a_center)
          merio.draw_light_label(rect_status_label, "Status",     a_center)
          merio.draw_light_label(rect_param_label,  "Parameters", a_center)
          merio.draw_light_label(rect_arts_label,   "Arts",       a_center)
          merio.draw_light_label(rect_equip_label,  "Equips",     a_center)
          merio.draw_light_label(rect_state_label,  "States",     a_center)
        end
        ## draw_sublabel
        merio.snapshot do
          merio.draw_light_label(rect_element_label, "Elem.", a_center)
          merio.draw_light_label(rect_class_label,   "Job",   a_center)
          merio.draw_light_label(rect_level_label,   "Lvl",   a_center)
          merio.draw_light_label(rect_hp_label,      "HP",    a_center)
          merio.draw_light_label(rect_mp_label,      "MP",    a_center)
          merio.draw_light_label(rect_xp_label,      "XP",    a_center)
        end

        ### draw_info
        begin
          element = entity.element
          merio.snapshot do |met|
            ##
            # change the text pallete to an element pallete
            # (it just matches the text color to the element's color)
            #met.txt_palette_set(merio.palette("element#{element.id}"))
            met.draw_dark_label(rect_elementname_label, element.name, a_center)
          end
        end
        merio.draw_dark_label(rect_classname_label,   entity.class.name, a_center)
        merio.draw_dark_label(rect_level_i_label,     entity.level, a_center)

        ### draw_gauges
        should_draw_text = true
        art.draw_entity_hp(entity,  rect_hp, should_draw_text)
        art.draw_entity_mp(entity,  rect_mp, should_draw_text)
        art.draw_entity_exp(entity, rect_xp, should_draw_text)

        ### draw parameters
        for ii in 2...8
          i = ii - 2
          r = rect_param.dup
          r.x += (r.width * 2 + spacing) * (i / 3)
          r.y += (r.height) * (i % 3)
          r.width *= 2
          str = Vocab.param_a(ii)
          stat = entity.param(ii)
          merio.draw_pair_fmt(r, str, stat, "%03d")
        end

        ### draw_arts
        entity.arts.each_with_index do |art, i|
          str = art ? art.name : '----'
          r = rect_arts_label.dup
          r.step!(2, 1).step!(2, i)
          merio.draw_dark_label(r, str, a_center)
        end

        ### draw_equipment
        y = rect_equip_label.y2
        spacer = _rct.width + spacing
        sz = entity.equips.size
        rect_equip = Rect.new(nrect.x + spacing / 2, y, 24, 24)
        r = rect_equip.dup
        DrawExt.enable_border
        sz.times do |i| eq = entity.equips[i]
          orr = art.draw_item_icon(eq, r.x, r.y, true)
          r = rect_equip.dup
          r.x = orr.x
          r.y = orr.y
          r.x += spacer
        end
        DrawExt.restore_border

        ### draw_states
        y = rect_state_label.y2
        spacer = _rct.width + spacing
        sz = 9
        rect_state = Rect.new(nrect.x + spacing / 2, y, 24, 24)
        r = rect_state.dup
        DrawExt.enable_border
        sz.times do |i| st = entity.states[i]
          orr = art.draw_item_icon(st, r.x, r.y, true)
          r = rect_state.dup
          r.x = orr.x
          r.y = orr.y
          r.x += spacer
        end

        ### free
        DrawExt.restore_border
        DrawExt.border_func_set(nil)
        font.restore
      end

      merio.restore

      @bitmap_swap_target = contents.dup
      contents.clear
      contents.blt(0,0, @bitmap_swap_source, @bitmap_swap_source.rect)

      @swap_time = @swap_time_max = 10
    end

    def update
      super
      update_swap if @bitmap_swap_target
    end

    attr_accessor :switch

    def update_swap
      @swap_time -= 1
      bmp = self.contents
      rect = bmp.rect
      #bmp.clear

      # alpha blend
      #bmp.blt(0,0, @bitmap_swap_source, rect, (@swap_time * 255) / 15)
      #bmp.blt(0,0, @bitmap_swap_target, rect, ((15 - @swap_time) * 255) / 15)

      # switch
      #r = ((15 - @swap_time) * rect.width) / 15
      #@switch ||= 0
      #if @switch > 0 # right
      #  sx = 0 - r
      #  tx = rect.width - r
      #else
      #  sx = r
      #  tx = 0 - rect.width + r
      #end
      #bmp.blt(sx, 0, @bitmap_swap_source, rect)
      #bmp.blt(tx, 0, @bitmap_swap_target, rect)

      # transition
      #unless @transition
      #  bp = Bitmap.new(bmp.width, bmp.height)
      #  sbp = Bitmap.new("graphics/transitions/checker-vertical-16px")
      #  bp.blt(0, 0, sbp, bp.rect)
        #bp.gradient_fill_rect(bp.rect, Palette['black'], Palette['white'], true)
        #DrawExt.reduce(bp, bp.rect, 16)
      #  @transition = bp.texture.to_transition
      #  bp.dispose
      #  sbp.dispose
      #end
      r = (@swap_time_max - @swap_time) * 255 / @swap_time_max
      StarRuby::Transition.crossfade(bmp.texture,
                             @bitmap_swap_source.texture,
                             @bitmap_swap_target.texture,
                             r)

      if @swap_time <= 0
        contents.clear
        contents.blt(0, 0, @bitmap_swap_target, @bitmap_swap_target.rect)
        dispose_swap_bitmaps
      end
    end

  end
end