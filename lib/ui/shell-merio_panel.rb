#
# EDOS/lib/merio/shell-merio_panel.rb
#   by IceDragon
#   dc 01/07/2013
#   dm 01/07/2013
# vr 1.0.0
module UI
  class ShellMerioPanel < Hazel::Shell::WindowSelectable

    include Hazel::Shell::Addons::Background

    def initialize(x, y, w, h)
      @list = []
      super(x, y, w, h)
    end

    def spacing
      4
    end

    def col_max
      2
    end

    def item_height
      Metric.ui_element
    end

    def add_widget(symbol, type, label, *data)
      @list.push({ symbol: symbol, type: type, name: label, data: data })
    end

    def item_max
      @list.size
    end

    def draw_component(type, id, rect, data)
      merio = DrawExt::Merio::DrawContext.new(contents)
      case type
      when :switch
        bool_func, l1, l2 = data
        bool = bool_func.call(id)
        merio.draw_switch_w_label(rect, bool, l1, l2)
      when :selector
      when :var_selector
      when :roller
      end
    end

    def draw_item(index)
      art   = artist
      merio = art.merio
      ##
      rect = item_rect(index).contract(anchor: 5, amount: Metric.contract)
      rect_label = rect.dup
      rect_label.width /= 2
      rect_data = rect.dup
      rect_data.x += rect_label.width
      rect_data.width -= rect_label.width
      ###
      ha = @list[index]
      s, t, l, d = ha[:symbol], ha[:type], ha[:name], ha[:data]
      ### draw
      merio.font_config(:light, :h1, :enb)
      art.draw_text(rect, l, 0)
      draw_component(t, s, rect_data, d)
    end

    def refresh
      create_contents
      draw_all_items
    end

    def command_symbol(index=self.index)
      @list[index][:symbol]
    end

  end
end