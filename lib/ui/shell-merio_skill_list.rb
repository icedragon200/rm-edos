#
# EDOS/lib/merio/shell-merio_inventory.rb
#   by IceDragon (mistdragon100@gmail.com)
require_relative 'shell-merio_item_list'
module UI
  class ShellMerioSkillList < ShellMerioItemList

    def make_item_list
      @items = @unit ? @unit.entity.all_skills : []
    end

    def change_palette_color_by_item(merio, item)
      if item.nil?
        merio.change_main_palette('black')
      else
        merio.change_main_palette("element#{item.element_id}")
      end
    end

    ##
    # draw_item(Integer index)
    def draw_item(index)
      ### shorthand
      bmp = contents
      ### rect
      rect = item_rect(index)
      rect_content = rect.contract(anchor: 5, amount: Metric.contract)
      rect_icon = rect_content.dup
      rect_icon.width = rect_icon.height = 24
      rect_name = rect_content.dup
      if (rect.height - rect_icon.height) > 16
        rect_name.height -= 24
        rect_name.y += 24
      else
        rect_name.x = rect_icon.x2 + 4
        rect_name.width -= (rect_icon.x2 - rect.x) - 8
        rect_name.y = rect_icon.y2 - 16
      end
      rect_gauge = Rect.new(rect_icon.x2 + 4, rect_icon.y, rect.width - (rect_icon.x2 - rect.x) - 8, 6)
      ###
      item = items[index]
      ###
      item_name = item ? item.name : ""
      item_r = item ? entity.get_magiclearn_r(item.id) : 0.0
      item_eq = item ? entity.equipped_skill?(item.id) : false
      ### drawing
      artist do |art|
        art.merio.snapshot do |mer|
          # preparation
          if item_r >= 1.0
            mer.change_main_palette("element#{item.element_id}_#{item_eq ? "enb" : "dis"}")
          else
            mer.change_main_palette(item_eq ? 'default' : 'clay')
          end
          mer.txt_palette = mer.main_palette.text_palette_suggestion
          mer.draw_light_rect(rect)
          ## draw_icon
          # this sections draws the small rectangle behind the icon
          mer.snapshot do |sub_mer|
            change_palette_color_by_item(sub_mer, item)
            sub_mer.fill_type = :blend_round
            sub_mer.draw_dark_rect(rect_icon)
          end
          if item
            art.draw_item_icon(item, rect_icon.x, rect_icon.y)
            mer.font_config(:dark, :micro, :enb)
            art.draw_text(rect_name, item_name, 0)
            DrawExt.snapshot do |dext|
              art.draw_gauge_ext(rect: rect_gauge, rate: item_r,
                                 colors: DrawExt.quick_bar_colors(Palette["element#{item.element_id}"]))
            end
          end
        end # merio
      end # artist
    end

  end
end