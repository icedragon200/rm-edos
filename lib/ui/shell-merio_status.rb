#
# EDOS/lib/merio/shell-merio_status.rb
#   by IceDragon
#   dc 05/07/2013
#   dm 05/07/2013
# vr 1.0.0
module UI
  class ShellMerioStatus < Hazel::Shell::WindowSelectable

    ### mixins
    include Hazel::Shell::Addons::Background
    include Mixin::UnitHost

    ##
    # initialize(*args)
    def initialize(*args)
      super(*args)
      activate
    end

    ## Mixin::UnitHost
    # on_unit_change
    def on_unit_change
      refresh
    end

    ##
    # refresh
    def refresh
      ### setup
      bmp     = contents
      font    = bmp.font
      art     = artist      # Artist::Context  (for regularly used drawing)
      merio   = art.merio   # Merio::Context   (for special low level drawing)
      cr      = art.cairo   # Cairo::Context   (its cairo, what more do you need?)
      drawext = art.drawext # DrawExt::Context (if it ever had one 05/07/2013)
      ###
      entity    = unit ? unit.entity : nil
      character = unit ? unit.character : nil
      ### rects
      rect = contents.rect

      rect_name  = rect.dup
      rect_name.height = Metric.ui_element
      rect_name.width /= 2

      rect_title = rect_name.dup
      rect_title.width = rect.width - rect_name.width
      rect_title.x += rect_name.width

      rect_line1 = rect.dup
      rect_line1.height = 1
      rect_line1.y += rect_name.height

      rect_hp = rect.dup
      rect_hp.height = Metric.ui_element_sml
      rect_hp.width = Metric.ui_element * 4
      rect_hp.y = rect_line1.y2 + Metric.spacing
      rect_mp = rect_hp.step(2)
      rect_xp = rect_mp.step(2)

      ### drawing
      merio.snapshot do
        bmp.clear
        ### main strip
        ## entity.name
        merio.font_config(:light, :h1, :enb)
        art.draw_text(rect_name, entity.name, 0)
        ## entity.title
        merio.font_config(:light, :h6, :enb)
        art.draw_text(rect_name, entity.title, 2)
        ## horz_line
        merio.draw_light_rect(rect_line1)
        ###
        art.draw_entity_hp(entity, rect_hp)
        art.draw_entity_mp(entity, rect_mp)
        art.draw_entity_exp(entity, rect_xp)
      end
    end

  end
end