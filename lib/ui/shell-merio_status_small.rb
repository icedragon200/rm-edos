#
# EDOS/lib/merio/shell-merio_status.rb
#   by IceDragon
#   dc 05/07/2013
#   dm 05/07/2013
# vr 1.0.0
module UI
  class ShellMerioStatusSmall < Hazel::Shell::WindowSelectable

    ### mixins
    include Hazel::Shell::Addons::Background
    include Mixin::UnitHost

    ##
    # initialize(x, y)
    def initialize(x, y)
      super(x, y, window_width, window_height)
      activate
    end

    def window_width
      Screen.menu_content.width
    end

    def window_height
      Metric.ui_element
    end

    def contents_height
      window_height - standard_padding * 2
    end

    ## Mixin::UnitHost
    # on_unit_change
    def on_unit_change
      refresh
    end

    ##
    # refresh
    def refresh
      ### setup
      bmp     = contents
      font    = bmp.font
      art     = artist      # Artist::Context  (for regularly used drawing)
      merio   = art.merio   # Merio::Context   (for special low level drawing)
      cr      = art.cairo   # Cairo::Context   (its cairo, what more do you need?)
      drawext = art.drawext # DrawExt::Context (if it ever had one 05/07/2013)
      metric  = Metric
      ###
      entity    = unit ? unit.entity : nil
      character = unit ? unit.character : nil
      ### data
      text_name  = DrawExt::Text.new(entity.name) { |t| merio.font_config_abs(t.font, :light, :large, :enb) }
      text_title = DrawExt::Text.new(entity.title) { |t| merio.font_config_abs(t.font, :light, :small, :enb) }
      ### rects
      rect = contents.rect.contract(anchor: 46, amount: Metric.ui_element)
      rect_name  = rect.dup
      rect_name.height = text_name.text_size.height - 4
      rect_name.width = metric.ui_element * 2

      rect_title = rect_name.dup
      rect_title.y += rect_name.height
      rect_title.width = rect_name.width #text_title.text_size.width.max(text_name.text_size.width)
      rect_title.height = metric.ui_element_sml

      rect_hp = rect.dup
      rect_hp.height = metric.ui_element_sml #- metric.spacing
      rect_hp.width = metric.ui_element * 3
      rect_hp.x = rect_title.x2
      rect_hp.y = rect.y #+ metric.spacing
      rect_mp = rect_hp.step(6)
      rect_xp = rect_mp.step(6)

      ### drawing
      merio.snapshot do |mer|
        bmp.clear
        ### main strip
        art.draw_text(rect_name, text_name, 0)
        art.draw_text(rect_title, text_title, 2)
        ###
        art.draw_entity_hp(entity, rect_hp)
        art.draw_entity_mp(entity, rect_mp)
        art.draw_entity_exp(entity, rect_xp)
      end
    end

  end
end