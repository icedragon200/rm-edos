#
# EDOS/lib/merio/shell-merio_title_command.rb
#   by IceDragon
#   dc 19/06/2013
#   dm 19/06/2013
# vr 1.0.0
module UI
  class ShellMerioTitleCommand < Hazel::Shell::Command

    #include Hazel::Shell::Addons::Background

    ##
    # alignment
    def alignment
      1
    end

    ##
    # window_width
    def window_width
      Graphics.width / 2
    end

    ##
    # item_height
    def item_height
      40
    end

    ##
    # line_height
    def line_height
      item_height
    end

    ##
    # make_command_list
    def make_command_list
      add_command("New Game",  :new_game)
      add_command("Load Game", :load_game)
      add_command("Options",   :options)
      add_command("Extras",    :extras)
      add_command("Quit Game", :shutdown)
    end

    ##
    # draw_item(int index)
    def draw_item(index)
      @command_rect ||= []
      contents.merio.font_config(:oqlight, :h2, :enb)
      com_rect = @command_rect[index] = item_rect(index)
      txt_rect = contents.text_size(command_name(index))
      com_rect.width = txt_rect.width
      com_rect.cx = item_rect(index).cx
      com_rect.expand!(anchor: 46, amount: Metric.contract)
      super(index)
    end

    ##
    # cursor_item_rect(int index)
    def cursor_item_rect(index)
      @command_rect[index] || item_rect(index)
    end

    ##
    # redraw_cont_cursor
    def redraw_cont_cursor
      bmp = @cursor_sprite.bitmap
      bmp.merio.snapshot do
        bmp.merio.active_state = :dis
        bmp.merio.draw_light_rect(bmp.rect)
      end
    end

  end
end