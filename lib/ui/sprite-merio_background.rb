#
# EDOS/lib/merio/sprite-merio_background.rb
#   by IceDragon
#   dc 20/06/2013
#   dm 20/06/2013
# vr 1.0.0
class Sprite::MerioBackground < Sprite

  ##
  # initialize(Viewport viewport, int width, int height)
  def initialize(viewport=nil,
                 width=Graphics.width, height=Graphics.height)
    super(viewport)
    self.bitmap = Bitmap.new(width, height)
    refresh
  end

  ##
  # dispose
  def dispose
    self.bitmap.dispose if self.bitmap
    super
  end

  ##
  # refresh
  def refresh
    bmp = self.bitmap
    bmp.clear
    bmp_fille = Cache.picture("tactile_noise")
    DrawExt.repeat_bmp(bmp, bmp.rect, bmp_fille, bmp_fille.rect)
  end

end