#
# EDOS/lib/merio/sprite-merio_gold.rb
#   by IceDragon
#   dc 02/07/2013
#   dm 02/07/2013
# vr 1.0.0
class Sprite::MerioGold < Sprite

  ### instance_variables
  attr_accessor :inverted

  ##
  # initialize(Viewport viewport)
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Bitmap.new(Metric.ui_element * 4, Metric.ui_element_mid)
    @inverted = true
  end

  ##
  # dispose
  def dispose
    self.bitmap.dispose
    super
  end

  ##
  # refresh
  def refresh
    bmp = self.bitmap
    font = bmp.font
    merio = DrawExt::Merio::DrawContext.new(bmp)
    ###
    back_shade = @inverted ? :light : :dark
    shade      = @inverted ? :dark : :light
    ###
    rect = bmp.rect.contract(anchor: 5, amount: Metric.contract)
    rect_cc  = rect.dup
    rect_cc.width *= 0.75
    rect_lbl = rect.dup
    rect_lbl.width -= rect_cc.width
    rect_lbl.x += rect_cc.width
    ###
    merio.snapshot do |m|
      bmp.clear
      font.snapshot do
        m.font_config(shade, :large, :enb)
        font.color = Palette["droid_yellow"]
        bmp.draw_text(rect_lbl, Vocab.currency_unit, 0)
      end
      merio.font_config(shade, :default, :enb)
      bmp.draw_text(rect_cc, @last_gold_s, 2)
    end
  end

  ##
  # update
  def update
    if @last_gold != (gold = $game.party.gold)
      @last_gold = gold
      @last_gold_s = gold.to_s
      refresh
    end
    super
  end

end
