#
# EDOS/lib/merio/sprite-merio_help.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
# vr 1.0.0
require_relative 'sprite-merio_text_box'
class Sprite::MerioHelp < Sprite::MerioTextBox

  ##
  # initialize(Viewport viewport, Integer width, Integer height)
  def initialize(viewport=nil, width=Graphics.width, height=Metric.ui_element_sml)
    @inverted = true
    super(viewport, width, height)
    @sim_draw = true
    @merio_font_size = :small
    refresh
  end

  def set_item(item)
    if @item != item
      @item = item
      t = @item ? @item.description : ""
      set_text(t)
    end
  end

  def clear
    @item = nil
    super
  end

end
