#
# EDOS/lib/merio/sprite-merio_spacer.rb
#   by IceDragon (mistdragon100@gmail.com)
#   dc 15/06/2013
#   dm 15/06/2013
# vr 1.0.0
class Sprite::MerioSpacer < Sprite

  attr_accessor :inverted

  def initialize(viewport, width=Graphics.width, height=Metric.ui_element)
    super(viewport)
    @inverted = false
    self.bitmap = Bitmap.new(width, height)
    refresh
  end

  ##
  # dispose
  def dispose
    self.bitmap.dispose
    super
  end

  def refresh
    self.bitmap.clear
    if @inverted
      bitmap.merio.draw_dark_rect(bitmap.rect)
    else
      bitmap.merio.draw_light_rect(bitmap.rect)
    end
  end

end