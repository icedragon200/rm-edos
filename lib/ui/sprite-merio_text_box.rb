#
# EDOS/lib/merio/sprite-merio_text_box.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
# vr 1.0.0
class Sprite::MerioTextBox < Sprite

  ### instance_attributes
  attr_accessor :text, :align
  attr_accessor :inverted
  attr_accessor :use_background
  attr_accessor :merio_font_size

  ##
  # initialize(Viewport viewport, Integer width, Integer height)
  def initialize(viewport, width=Graphics.width, height=Metric.ui_element)
    super(viewport)
    set_size(width, height)
    @use_background = true
    @merio_font_size = :default
    @text = ""
    @align = 0
    @cps = 15
    @sim_draw = false # should characters be drawn 1 by 1
  end

  ##
  # dispose
  def dispose
    self.bitmap.dispose
    super
  end

  ##
  # text_rect
  def text_rect
    self.bitmap.rect.contract(anchor: 46, amount: Metric.contract)
  end

  ##
  # draw_background(Bitmap bmp)
  def draw_background(bmp=self.bitmap)
    merio = bmp.merio
    if @inverted
      merio.draw_dark_rect(bmp.rect)
    else
      merio.draw_light_rect(bmp.rect)
    end
  end

  ##
  # draw_content(Bitmap bmp)
  def draw_content(bmp=self.bitmap)
    merio = bmp.merio
    txt_rect = text_rect
    if @inverted
      merio.font_config(:light, @merio_font_size, :enb)
    else
      merio.font_config(:dark, @merio_font_size, :enb)
    end
    if @sim_draw
      @dtext_rect = bmp.text_size(@text)
      @dtext_rect.x += txt_rect.x
      @char_index = 0
      @cc = [@text.size / @cps.to_f, 1].max.to_i
    else
      bmp.draw_text(txt_rect, @text, @align)
    end
  end

  ##
  # update
  def update
    super
    if @sim_draw
      if @dtext_rect
        @cc.times do
          if @char_index < @text.size
            r = @dtext_rect.dup
            r.x += self.bitmap.text_size(text[0...@char_index]).width
            self.bitmap.draw_text(r, @text[@char_index], 0)
            @char_index += 1
          end
        end
      end
    end
  end

  ##
  # refresh
  def refresh
    bmp = self.bitmap
    bmp.clear
    draw_background(bmp) if @use_background
    draw_content(bmp)
  end

  ##
  # set_text
  def set_text(text, align=@align || 0)
    if @text != text || @align != align
      @text  = text
      @align = align
      refresh
    end
  end

  ##
  # clear
  def clear
    set_text("", 0)
  end

  ##
  # set_size(int width, int height)
  def set_size(width, height)
    dispose_bitmap_safe
    self.bitmap = Bitmap.new(width, height)
  end

  ##
  # set_size!(int width, int height)
  def set_size!(width, height)
    set_size(width, height)
    refresh
  end

end
