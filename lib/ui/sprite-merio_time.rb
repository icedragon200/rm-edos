#
# EDOS/lib/merio/sprite-merio_time.rb
#   by IceDragon
#   dc 15/06/2013
#   dm 15/06/2013
# vr 1.1.0
class Sprite::MerioPlaytime < Sprite

  ### instance_variables
  attr_accessor :inverted

  ##
  # initialize(Viewport viewport)
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Bitmap.new(Metric.ui_element * 4, Metric.ui_element_mid)
    @inverted = true
  end

  ##
  # dispose
  def dispose
    self.bitmap.dispose
    super
  end

  ##
  # refresh
  def refresh
    bmp = self.bitmap
    font = bmp.font
    merio = bmp.merio
    ###
    back_shade = @inverted ? :light : :dark
    shade      = @inverted ? :dark : :light
    ###
    rect = bmp.rect.contract(anchor: 5, amount: Metric.contract)
    ###
    merio.snapshot do
      bmp.clear
      font.snapshot do
        merio.font_config(shade, :large, :enb)
        font.color = Palette["droid_yellow"]
        bmp.draw_text(rect, "Playtime", 0)
      end
      merio.font_config(shade, :default, :enb)
      bmp.draw_text(rect, @last_time_s, 2)
    end
  end

  ##
  # update
  def update
    if @last_time != (ply = $game.system.playtime)
      @last_time = ply
      @last_time_s = $game.system.playtime_s
      refresh
    end
    super
  end

end

class Sprite::MerioTime < Sprite

  ### instance_variables
  attr_reader :tick
  attr_accessor :frequency
  attr_accessor :inverted

  ##
  # initialize(Viewport viewport)
  def initialize(viewport=nil)
    super(viewport)
    self.bitmap = Bitmap.new(Screen.content.width, Metric.ui_element)
    @ticks = 0
    @frequency = Graphics.frame_rate # every second
    @inverted = true
    refresh
  end

  def dispose
    self.bitmap.dispose
    super
  end

  def draw_merio_time
    bmp = self.bitmap
    font = bmp.font
    merio = bmp.merio

    time_fmt = "%I:%M"

    back_shade = @inverted ? :light : :dark
    shade      = @inverted ? :dark : :light

    r1 = bmp.rect.contract(anchor: 5, amount: Metric.contract)
    r2 = r1.dup

    font.snapshot do
      font.set_style("merio_#{shade}_mega_enb")
      bignum = (0...9).max_by { |n| font.text_size(n)[0] }
      r1.width -= font.text_size("%1$d%1$d:%1$d%1$d" % bignum)[0]
    end


    merio.snapshot do
      ## draw_time_text setup
      font.set_style("merio_#{shade}_h2_enb")
        a = font.color.alpha;
        font.color = Palette['droid_yellow'];
        font.color.alpha = a
      # draw_phase_text
      bmp.draw_text(r1, @time.strftime("%P"), MACL::Surface::ANCHOR_SE)
      # draw_time_text
      font.set_style("merio_#{shade}_mega_enb")
      bmp.draw_text(r2, @time.strftime(time_fmt), 2)
    end

    return self
  end

  def refresh
    @time = Time.now
    self.bitmap.clear
    draw_merio_time
  end

  def update
    super
    refresh if @ticks % @frequency == 0
    @ticks += 1
  end

end

class Sprite::MerioTimeDate < Sprite::MerioTime

  def draw_merio_date
    bmp = self.bitmap
    font = bmp.font

    back_shade = @inverted ? :light : :dark
    shade      = @inverted ? :dark : :light
    day_fmt   = "%a %d"
    month_fmt = "%h."

    ## rectangles
    r = Rect.new(0, 0, bmp.width, bmp.height)
    sub_r = r.contract(anchor: 5, amount: Metric.contract)
    r1, r2 = MACL::Surface::Tool.split_surface(sub_r, 1, 2)

    ## setup font
    bmp.font.set_style("merio_#{shade}_default_enb")
    # draw_day_text
    bmp.draw_text(r1, @time.strftime(day_fmt), 0)
    # draw_month_text
    bmp.draw_text(r2, @time.strftime(month_fmt), 0)
  end

  def refresh
    super
    draw_merio_date
  end

end
