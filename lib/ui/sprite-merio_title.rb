#
# EDOS/lib/merio/sprite-merio_title.rb
#   by IceDragon
#   dc 14/06/2013
#   dm 14/06/2013
# vr 1.0.0
#   Merio stylized title strip
#   USAGE:
#     text = "My Title"
#     align = 1 # align text to center
#     title_sp = Sprite::MerioTitle.new(viewport, text, align)
#   NOTE:
#     Highlight effect is only available with StarRuby SRRI wrapper
class Sprite::MerioTitle < Sprite::MerioTextBox

  ##
  # initialize(Viewport viewport,
  #            String text, ALIGN align, Integer width, Integer height)
  def initialize(viewport, text="", align=0,
                 width=Graphics.width, height=Metric.ui_element)
    super(viewport, width, height)
    @sim_draw = false
    @merio_font_size = :h1 # MerioTextBox attribute
    set_text(text, align)
    if respond_to?(:post_render)
      @highlight = :horz
      refresh_highlight
    end
    # because of the sim_draw effect, we need to render the text before,
    # call refresh_highlight and then redraw it again this time using sim_draw
    @sim_draw = true
    clear
    set_text(text, align)
  end

### SRRI post_render effect
if defined?(StarRuby) && defined?(SRRI)
  ### instance_attributes
  attr_accessor :highlight
  attr_accessor :highlight_vertical

  ##
  # dispose
  def dispose
    @hightlight_bmp.dispose if @hightlight_bmp && !@hightlight_bmp.disposed?
    super
  end

  def refresh_bitmap
    if @hightlight_bmp && ((@hightlight_bmp.width != self.width) ||
                           (@hightlight_bmp.height != self.height))
      @hightlight_bmp.dispose
      @hightlight_bmp = nil
    end
    if !@hightlight_bmp
      @hightlight_bmp = Bitmap.new(self.width, self.height)
    else
      @hightlight_bmp.clear
    end
  end

  ##
  # refresh_highlight
  def refresh_highlight
    return if @highlight == :none
    merio = bitmap.merio
    refresh_bitmap
    begin
      bmp = @hightlight_bmp
      tone_sym = :blue
      draw_content(bmp)
      bmp.blend_fill_rect(bmp.rect, Palette["droid_#{tone_sym}_light"], :dst_mask)
    end

    ### tween
    ticks = 1
    if @highlight == :vert
      ticks = Graphics.frame_rate * 2 # 2 seconds # y
      @highlight_size = 16 # y
      pair = [self.y - @highlight_size, self.y2] # y
    elsif @highlight == :horz
      ticks = Graphics.frame_rate * 4 # 4 seconds # x
      @highlight_size = 32 # x
      pair = [self.x - @highlight_size, self.x2] # x
    end
    #pair.reverse!
    @tween = MACL::Tween2.new(ticks, :linear, pair)
    @highlight_rel_x = 0
    @highlight_rel_y = 0

    ### texture
    @hightlight_tex = @hightlight_bmp.texture
    update_highlight
  end

  ##
  # update
  def update
    super
    update_highlight if @highlight
  end

  ##
  # update_highlight
  def update_highlight
    @tween.update
    if @highlight == :vert
      @highlight_rel_x = 0
      @highlight_rel_y, = @tween.result
    elsif @highlight == :horz
      @highlight_rel_x, = @tween.result
      @highlight_rel_y = 0
    end
    @tween.reset_tick if !@tween.active?
  end

  ## SRRI
  # post_render(texture, x, y)
  def post_render(texture, x, y)
    return false unless @highlight
    case @highlight
    when :vert
      texture.render_texture(@hightlight_tex,
                             x, y + @highlight_rel_y,
                             src_y: @highlight_rel_y,
                             src_height: @highlight_size,
                             blend_type: :alpha)
    when :horz
      texture.render_texture(@hightlight_tex,
                             x + @highlight_rel_x, y,
                             src_x: @highlight_rel_x,
                             src_width: @highlight_size,
                             blend_type: :alpha)
    end
    #return true
  end

end
end
