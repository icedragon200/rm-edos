
window = Window::NumberInputBase.new(0, 0)
window.digits_max = 1
window.start
window.align_to!(anchor: 5)
window.z = 9999

loop do
  Main.update
  window.update
  break unless window.active
end

n = window.number

window.dispose

$gauge_id = n % 6

module DrawExt::Include

  def draw_gauge_ext(*args)
    send("draw_gauge_ext_sp#{$gauge_id + 1}".to_sym, *args)
  end

end

Main.scene_manager.scene.refresh_windows