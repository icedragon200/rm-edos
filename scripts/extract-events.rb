map = load_data("Data/Map002.rvdata2")

map.events.values.each do |ev|
  next if ev.nil?
  File.open("./extract/event#{ev.id}.txt", "w+") do |f|
    ev.pages[0].list.each do |evc|
      f.puts evc.to_s_exp()
    end
  end
end
