
window = Window::NumberInputBase.new(0, 0)
window.digits_max = 6
window.start
window.align_to!(anchor: 5)
window.z = 9999

loop do
  Main.update
  window.update
  break unless window.active
end

n, id = window.number, -1

window.dispose

$game.party.members.each do |member|
  member.entity.inc_weaponres(n, id)
end

Main.scene_manager.scene.refresh_windows