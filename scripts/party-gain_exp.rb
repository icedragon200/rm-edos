
window = Window::NumberInputBase.new(0, 0)
window.digits_max = 6
window.start
window.align_to!(anchor: 5)
window.z = 9999

loop do
  Main.update
  window.update
  break unless window.active
end

n = window.number

window.dispose

$game.party.members.each do |member|
  member.entity.change_exp(member.entity.exp + n, false)
end

Main.scene_manager.scene.refresh_windows