
window = Window::NumberInputBase.new(0, 0)
window.digits_max = 6
window.start
window.align_to!(anchor: 5)
window.z = 9999

Daemon::DPause.hold_playtime do
  loop do
    Main.update
    window.update
    break unless window.active
  end
end

n = window.number

window.dispose

$game.party.members.each do |member|
  member.entity.inc_weaponexp(n)
end

Main.scene_manager.scene.refresh_windows