class Scene::TestStatus < Scene::MenuUnitBase

  def start
    super
    @status_window = Window::RogueStatus.new(0, 0, nil, nil)
    @status_window.set_unit(@unit)
    @status_window.set_handler(:cancel,   method(:return_scene))
    @status_window.set_handler(:pagedown, method(:next_unit))
    @status_window.set_handler(:pageup,   method(:prev_unit))
    @status_window.set_handler(:ok,       method(:change_refresh))
    @status_window.start_open
    @status_window.activate
    window_manager.add(@status_window)
  end

  def on_unit_change
    @status_window.set_unit(@unit)
    @status_window.activate
  end

  def change_refresh
    @status_window.draw_mode = (@status_window.draw_mode + 1) % 4
    @status_window.activate.refresh
  end

end