try_dispose = proc do |obj|
  unless obj.disposed?
    obj.dispose
  end
end

ObjectSpace.each_object(Sprite, &try_dispose)
ObjectSpace.each_object(Window, &try_dispose)
ObjectSpace.each_object(Plane , &try_dispose)
ObjectSpace.each_object(Bitmap, &try_dispose)

ObjectSpace.each_object(SRRI::SoftSprite, &try_dispose)
