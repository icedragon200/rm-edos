module Graphics
  def self.frame_rate
    60
  end
end

@animator = Animator::Builder.build(File.read("../../data/sequence/animation/walking.seq.rb")).to_animator

loop do
  @animator.update
  p ["--", @animator._list_index, @animator.current_line]
  sleep 0.016
  break if @animator.done?
end